import os
import subprocess as sub
import time

def runTermCmnd(cmnd):  
	if isinstance(cmnd, (list, tuple)):	
		#if input is a list of strings
		try:  
			p = sub.Popen(cmnd, stdout=sub.PIPE, stderr=sub.PIPE)  
			p.wait()  
			if p.returncode:  
				print "Could not run the terminal command, retCode: %s" % str(p.returncode)
			return p.communicate()  
	
		except OSError:  
			print "OSError occured while processing terminal command!"
			return "Failure"
	else:
		#if a input is a string	
		try:
			#print "I am in run"
			p  = sub.Popen(cmnd, universal_newlines=True, shell=True, stdout=sub.PIPE)
			output=p.communicate()
			#print output
			#print "Output in run is: "+output[0]
			return output[0]
			
		except OSError:
			print "OSError occured while processing terminal command"
			return "Failure"

def runCmndTimeOut(cmnd):  
	if isinstance(cmnd, (list, tuple)):	
		#if input is a list of strings
		try:  
			p = sub.Popen(cmnd, stdout=sub.PIPE, stderr=sub.PIPE)  
			p.wait()  
			if p.returncode:  
				print "Could not run the terminal command, retCode: %s" % str(p.returncode)
			return p.communicate()  
	
		except OSError:  
			print "OSError occured while processing terminal command!"
			return "Failure"
	else:
		#if a input is a string	
		try:
			#print "I am in run"
			p  = sub.Popen(cmnd, universal_newlines=True, shell=True, stdout=sub.PIPE)
			output=p.communicate()
			#print output
			#print "Output in run is: "+output[0]
			return output[0]
			
		except OSError:
			print "OSError occured while processing terminal command"
			return "Failure"

	
def runNGetPid(cmnd):
	if isinstance(cmnd, (list, tuple)):	
		try:
			#print "I am in run"
			#p  = sub.Popen(cmnd, universal_newlines=True, shell=True, stdout=sub.PIPE)
			p = sub.Popen(cmnd)
			output=p.pid
			return output
		
		except OSError:
			print "OSError occured while processing terminal command"
			return "Failure"
	else:
		try:
			#print "I am in run"
			p  = sub.Popen(cmnd, universal_newlines=True, shell=True, stdout=sub.PIPE)
			#p = sub.Popen(cmnd)
			output=p.pid
			return output
		
		except OSError:
			print "OSError occured while processing terminal command"
			return "Failure"

def runNGetDetails(cmnd):
	if isinstance(cmnd, (list, tuple)):	
		try:
			#print "I am in run"
			#p  = sub.Popen(cmnd, universal_newlines=True, shell=True, stdout=sub.PIPE)
			p = sub.Popen(cmnd)
			return [p.pid,p.communicate()]
		
		except OSError:
			print "OSError occured while processing terminal command"
			return "Failure"
	else:
		try:
			#print "I am in run"
			p  = sub.Popen(cmnd, universal_newlines=True, shell=True, stdout=sub.PIPE)
			#p = sub.Popen(cmnd)
			return [p.pid,p.communicate()]
		
		except OSError:
			print "OSError occured while processing terminal command"
			return "Failure"

def runNSaveRes(cmnd,outfile):
	if isinstance(cmnd, (list, tuple)):	
		try:
			#print "I am in run"
			#p  = sub.Popen(cmnd, universal_newlines=True, shell=True, stdout=sub.PIPE)
			p = sub.Popen(cmnd,stdout=open(outfile,'w'),stderr=open(outfile,'w'))
			
			output=p.pid
			return output
		
		except OSError:
			print "OSError occured while processing terminal command"
			return "Failure"
	else:
		try:
			#print "I am in run else"
			p  = sub.Popen(cmnd, universal_newlines=True, shell=True, stdout=open(outfile,'w'))
			#p = sub.Popen(cmnd)
			output=p.pid
			return output
		
		except OSError:
			print "OSError occured while processing terminal command"
			return "Failure"

		
def stopProcessId(pid):
	cmnd='kill -9 '+str(pid)
	os.system(cmnd)
	

def runInNewTerminal(cmnd):
	p1 = sub.Popen(args=['gnome-terminal','--command='+cmnd])
	p1.wait()
	time.sleep(5)
	if p1.returncode:  
		print "Could not run the terminal command, retCode: %s" % str(p1.returncode)
	return p1.communicate()[0]
	
	
