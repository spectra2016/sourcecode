import os
import time
import re
import Queue
#import hijacknhook as hh

from Run import runTermCmnd
from Run import runNGetPid
from Run import runNSaveRes
from Run import runInNewTerminal

def adbKill():
	cmnd='pkill -9 -f adb'
	#print 'kill command'+cmnd
	runTermCmnd(cmnd)
	
def createAVD(avdName, androidid, avoidAntiEmu, useExImages):
	if(avoidAntiEmu.lower()=='yes'):
		if(os.path.isdir('.'+os.sep+androidid+os.sep+'NewEmuEssentials'+os.sep+avdName)==False):
			cmnd='mkdir .'+os.sep+androidid+os.sep+'NewEmuEssentials'+os.sep+avdName
			runTermCmnd(cmnd)
	else:
		if(os.path.isdir('.'+os.sep+androidid+os.sep+'EmuEssentials'+os.sep+avdName)==False):
			cmnd='mkdir .'+os.sep+androidid+os.sep+'EmuEssentials'+os.sep+avdName
			runTermCmnd(cmnd)
	snaplockpath='.'+os.sep+androidid+os.sep+'NewEmuEssentials'+os.sep+avdName+os.sep+'snapshots.img.lock'
	
	if(os.path.isfile(snaplockpath)==True):
		cmnd='rm '+snaplockpath
		runTermCmnd(cmnd)
	sdcardlockpath='.'+os.sep+androidid+os.sep+'NewEmuEssentials'+os.sep+avdName+os.sep+'sdcard.img.lock'
	
	if(os.path.isfile(sdcardlockpath)==True):
		cmnd='rm '+sdcardlockpath
		runTermCmnd(cmnd)
	
	
	emString="["+avdName+"] : "
	if(avoidAntiEmu.lower()=='yes'):
		if(useExImages.lower()=='no'):
			
			sdcardpath='.'+os.sep+androidid+os.sep+'NewEmuEssentials'+os.sep+avdName+os.sep+'sdcard.img'
			snapshotpath= '.'+os.sep+androidid+os.sep+'NewEmuEssentials'+os.sep+avdName+os.sep+'snapshots.img'
			sysimgpath='.'+os.sep+androidid+os.sep+'NewEmuEssentials'+os.sep+avdName+os.sep+'system.img'
			
			avddirpath='.'+os.sep+androidid+os.sep+'NewEmuEssentials'+os.sep+avdName
			if(os.path.exists(avddirpath)==True):
				cmnd='rm -rf '+avddirpath+os.sep+"*"
				runTermCmnd(cmnd)
				#print 'Deleting the contents of image directory!'
			cmnd='cp .'+os.sep+androidid+os.sep+'NewEmuEssentials'+os.sep+'Original'+os.sep+'system.img '+sysimgpath
			runTermCmnd(cmnd)
			cmnd='cp .'+os.sep+androidid+os.sep+'NewEmuEssentials'+os.sep+'Original'+os.sep+'sdcard.img '+sdcardpath
			runTermCmnd(cmnd)
			cmnd='cp .'+os.sep+androidid+os.sep+'NewEmuEssentials'+os.sep+'Original'+os.sep+'snapshots.img '+snapshotpath
			runTermCmnd(cmnd)
	elif(os.path.exists('.'+os.sep+androidid+os.sep+'EmuEssentials'+os.sep+avdName+os.sep+'snapshots.img')==False or useExImages.lower()=='yes'):
		cmnd='cp .'+os.sep+androidid+os.sep+'EmuEssentials'+os.sep+'Original'+os.sep+'snapshots.img .'+os.sep+androidid+os.sep+'EmuEssentials'+os.sep+avdName+os.sep+'snapshots.img'
		runTermCmnd(cmnd) 
	
	#print 'Copied sdcard and snapshot images to respective emulator folders!'
	print emString+'Creating AVD, please wait!'
	
	cmnd='echo no | android create avd -n '+avdName+' -t '+androidid+' -c 256M --force -a' 
	runTermCmnd(cmnd)
	
	return


def deleteAVD(avdName):
	cmnd='android delete avd -n '+avdName
	runTermCmnd(cmnd)

def emulatorExists(devName):
	
	cmnd='adb devices | grep '+devName
	output=runTermCmnd(cmnd)
	
	if(output==None or output==''):
		return False
	else:
		return True
		
def killEmulator(devName):
	
	if(emulatorExists(devName)==True):
		cmnd='adb -s '+devName+' emu kill'
		#print 'kill command'+cmnd
		runTermCmnd(cmnd)
		
def getRoot(devName):
	cmnd='adb -s '+devName+' shell chown root ./system/xbin/*'
	runTermCmnd(cmnd)
	
def recBootLog(devName,bootlogfpath):
	cmnd='adb -s '+devName+' logcat -v time > '+bootlogfpath+' &'
	pid=runNGetPid(cmnd)
	if is_integer(pid)==False:
		print 'Error logging boot details!'
		return 'Failure'
	return pid
'''
def enterAdbShell(devName):
	
	cmnd='adb -s '+devName+' shell'
	retCode=os.system(cmnd)
	if retCode<>0:
		print 'Error starting adb shell!'
		return False
	return True
	
	cmnd=['adb','-s',devName,'shell']
	out=runTermCmnd(cmnd)
	if out=='Failure':
		print 'Error starting adb shell'
		return False
	return True
	

def exitShell():
	#cmnd=['adb','shell','exit']
	#runNGetPid(cmnd)
	runAndroidCmnd('exit')
	
	if res=="Failure":
		print 'Error exiting shell'
		
		'''
	
def getFileSize(devName,fPath):
	cmnd='adb -s '+devName+' shell ls -l '+fPath
	return runTermCmnd(cmnd)	



def isFileInAndroid(devName,dpath):
	ppath=''
	cdir=''
	for i in range(len(dpath)):
		
		if dpath[i]=='/':
			ppath=ppath+'/'+cdir
			cdir=''
		else:
			cdir=cdir+dpath[i]
				
	cmnd='adb -s '+devName+' shell ls -l '+ppath
	res=runTermCmnd(cmnd)
	if(res.find('No such file or directory')>-1):
		return False
	try:
		flist=res.split('\n')
		for f in flist:
			fcon=f.split()
			if(f[i]==cdir):
				if(fcon[0][0]=='-'):
					return True
	except:
		return False
	
	return False
	
def deviceScanner():
	devices=[]
	cmnd=['adb','devices']
	termOutput=runTermCmnd(cmnd)
	if termOutput==None:
		return 'Failure'
	
	mylist=termOutput[0].split('\n')
	for i in range(1,len(mylist)):
		if mylist[i]<>'':
			device=[]
			devName=mylist[i].split('\t')[0]
			dtype=''
			if "emulator" in devName:
				dtype="emulator"
			else:
				dtype="Pdevice"
			device.append([devName,dtype])
			devices.append(device)
	return devices
	

def displayDevices(devices):
	if devices==None:
		print 'No Devices Found'
		return
	print '---------------------------'
	print 'Dev Type  |  Device Name'
	print '---------------------------'
	for dev in devices:
		print '%-13s%s' % (dev[0][1],dev[0][0])
	print '---------------------------'
	return devices

def getRunningEmulator(devices):
	numOfDev=getDevCount()
	#print numOfDev
	#print devices
	
	if numOfDev>0:
		for i in range(numOfDev):
			#print devices[0][i][1]
			if devices[0][i][1]=='emulator':
				return devices[0][i][0]
	else:
		print 'No running emulator instances found!'
		return 'Failure'
	
def getDevCount():
	devList=deviceScanner()
	if devList==None:
		numOfDev=0
	else:
		numOfDev=len(devList)
	return numOfDev
'''	
def createAVDforSnapshot(avdname):
	emString="["+avdname+"] : "
	print emString+'Starting Emulator to prepare snapshot..!'
	portnum=avdname[len(avdname)-4:len(avdname)]
	#-no-skin -no-audio -no-window -wipe-data -no-boot-anim
	cmnd="emulator -avd "+avdname+" -port "+portnum+"  -partition-size 1024 &"
	retCode=os.system(cmnd)
	numOfDev=getDevCount()
	if retCode<>0:
		print emString+'Error: Could not launch AVD to create snapshot!'
		return False
	ctime=time.time()

	while numOfDev==getDevCount():
		if time.time()-ctime>120:
			print emString+'Error: Launching emulator is taking more time'
			print emString+'Exiting application'
			return False
		time.sleep(1)
	print emString+'AVD created successfully!'
'''	
def launchAVDfrmSnapshot(avdname, androidid, avoidAntiEmu,invisibleExec):
	emString="["+avdname+"] : "
	print emString+'Starting Emulator from snapshot..!'
	portnum=avdname[len(avdname)-4:len(avdname)]
	# -sdcard ."+os.sep+"EmuEssentials"+os.sep+avdname+os.sep+"sdcard.img
	if(avoidAntiEmu.lower()=='yes'):
		print emString+'Launching device like emulator!'
		if(invisibleExec.lower()=='no'):
			cmnd="echo no | emulator -avd "+avdname+" -port "+portnum+" -no-snapshot-save -no-audio -gpu off -partition-size 256 -snapstorage ."+os.sep+androidid+os.sep+"NewEmuEssentials"+os.sep+avdname+os.sep+"snapshots.img -system ."+os.sep+androidid+os.sep+"NewEmuEssentials"+os.sep+avdname+os.sep+"system.img -sdcard ."+os.sep+androidid+os.sep+"NewEmuEssentials"+os.sep+avdname+os.sep+"sdcard.img &"	
		else:
			cmnd="echo no | emulator -avd "+avdname+" -port "+portnum+" -no-snapshot-save -no-audio -gpu off -no-boot-anim -no-window -partition-size 256 -snapstorage ."+os.sep+androidid+os.sep+"NewEmuEssentials"+os.sep+avdname+os.sep+"snapshots.img -system ."+os.sep+androidid+os.sep+"NewEmuEssentials"+os.sep+avdname+os.sep+"system.img -sdcard ."+os.sep+androidid+os.sep+"NewEmuEssentials"+os.sep+avdname+os.sep+"sdcard.img &"	
	else:
		if(invisibleExec.lower()=='no'):
			cmnd="echo no | emulator -avd "+avdname+" -port "+portnum+" -no-snapshot-save -no-audio -gpu off -partition-size 256 -snapstorage ."+os.sep+androidid+os.sep+"EmuEssentials"+os.sep+avdname+os.sep+"snapshots.img &"
		else:
			cmnd="echo no | emulator -avd "+avdname+" -port "+portnum+" -no-snapshot-save -no-audio -gpu off -no-boot-anim -no-window -partition-size 256 -snapstorage ."+os.sep+androidid+os.sep+"EmuEssentials"+os.sep+avdname+os.sep+"snapshots.img &"
	#print cmnd
	retCode=os.system(cmnd)
	
	'''
	numOfDev=getDevCount()
	
	ctime=time.time()
	
	while numOfDev==getDevCount():
		if time.time()-ctime>30:
			print emString+'Error: Launching emulator from snapshot is taking more time'
			print emString+'Exiting application'
			return False
		time.sleep(1)
	return True
	#print '\n'+emString+'AVD snapshot launched Successfully!'
	'''

	
def isAnyAVDrunning():
	if getDevCount()>0:
		return True
	else:
		return False
	
def isDeviceOnline():
	print 'Yet to implement'
	return True

def sendSwipe(devName):
	emString='['+devName+'] : '
	'''
	#Turning on the screen(Helpful for capturing snapshots)
	cmnd='adb -s '+devName+' shell input keyevent 26'
	retCode=os.system(cmnd)
	'''
	print emString+'Sending swipe gesture to emulator'
	#Unlock the screen by sending the swipe key event
	cmnd='adb -s '+devName+' shell input keyevent 82'
	retCode=os.system(cmnd)
	if retCode<>0:
		print emString+'Error: Could not send unlock the emulator'
		return
	print emString+'Emulator unlocked successfully!'
	print emString+'-----------------------------------------------'

def keyPress(devName,key):
	#emString='['+devName+'] : '
	
	#Pass the keyevent to the device through adb
	cmnd='adb -s '+devName+' shell input keyevent '+str(key)
	retCode=os.system(cmnd)


def flushAdb(devName):
	cmnd='adb -s '+devName+' logcat -c'
	retCode=os.system(cmnd)
	if retCode<>0:
		print 'Could not flush adb messages!'
		return
	
def installApk(devName,appDir,appName,erfname):
	
	cmnd='adb -s '+devName+' install '+appDir+os.sep+appName
	output=runTermCmnd(cmnd)
	
	temp=re.search('(.*)FAIL(.*)',output)
	if temp<>None and temp.groups()[0]<>None:
		print '['+devName+'] : Error: Could not install app: '+appName
		temp='Error while installing : \n'+str(output)
		genErrFile(temp,erfname)
		return False
	
	return True

def launchApp(devName,packName,laName):
	emString='['+devName+'] : '
	cmnd='adb -s '+devName+' shell am start -n '+packName+os.sep+laName
	runTermCmnd(cmnd)
	
def launchApk(devName,packName,laName,erfname,q):
	emString='['+devName+'] : '
	cmnd='adb -s '+devName+' shell am start -W -n '+packName+os.sep+laName
	output=runTermCmnd(cmnd)
	temp=re.search('(.*)rror(.*)',output)
	
	if(q<>''):
		if temp<>None and temp.groups()[0]<>None:
			temp='Error while launching app : \n'+str(output)
			genErrFile(temp,erfname)
			q.put(False)
			return False
		#print ''
	
		if q.empty():
			print emString+'Launched activity: '+packName+os.sep+laName+' successfully!'
			q.put(True)
		#return True 
	
	
def sendRandGestures(devName,packName,numOfGest):
	emString='['+devName+'] : '
	cmnd='adb -s '+devName+' shell monkey -p '+packName+' --throttle 1000 --pct-anyevent 0 --pct-syskeys 0 -v '+numOfGest
	output=runInNewTerminal(cmnd)
	print output
	if(output<>None):
		temp=re.search('monkey aborted',output)
		if temp<>None and temp.groups()[0]<>None:
			print emString+'Error: Could not send random gestures to package: '+packName
			return
		#print ''
		print emString+str(numOfGest)+' Random gestures sent to app: '+packName
		#print ''

def getAvdName(devName):
	devSerNo=devName[len(devName)-4:]
	num=int(devSerNo)
	num=(num-5554)/2
	return "Emulator-"+str(num)
	

def genErrFile(temp,erfname):
	f=open(erfname,'w')
	f.write(temp)
	f.close()
	
def uninstallApk(devName,packName,erfname):
	emString="["+devName+"] : "
	cmnd='adb -s '+devName+' uninstall '+packName
	#print "Command in execution: "+cmnd
	output=runTermCmnd(cmnd)
	#print "output: "+output
	temp=re.search("Fail([^']*)",output)
	#print temp
	if temp<>None and temp.groups()[0]<>None:
		#print emString+'Could not uninstall app: '+packName
		if(erfname<>''):
			temp='Error while uninstalling : \n'+str(output).strip()
			genErrFile(temp,erfname)
		return False
	#print emString+'Uninstalled app: '+packName
	#print ''
	return True

def recRadioLog(devName,logFilePath):
	#cmnd='adb -s '+devName+' logcat -v time > '+logFilePath
	
	aName="["+devName+"] : "
	
	cmnd=['adb','-s',devName,'logcat','-b','radio']
	pid=runNSaveRes(cmnd,logFilePath)
	if is_integer(pid)==False:
		print aName+'Error: could not start adb logging'
		return False
	print aName+'Adb Logging started!'
	
	return pid

def recEventLog(devName,logFilePath):
	#cmnd='adb -s '+devName+' logcat -v time > '+logFilePath
	
	aName="["+devName+"] : "
	
	cmnd=['adb','-s',devName,'logcat','-b', 'events']
	pid=runNSaveRes(cmnd,logFilePath)
	if is_integer(pid)==False:
		print aName+'Error: could not start adb logging'
		return False
	print aName+'Adb Logging started!'
	
	return pid
'''
def recordAdbLog(devName,logFilePath):
	cmnd='./Android-Tools/adb -s '+devName+' logcat -v time > '+logFilePath
	os.system(cmnd)
'''	
def recAdbLog(devName,logFilePath):
	cmnd='adb -s '+devName+' logcat -v time > '+logFilePath
	pid=runNGetPid(cmnd)
	#print logFilePath
	aName="["+devName+"] : "
	'''
	cmnd=['adb','-s',devName,'logcat','-v','time']
	#print cmnd
	pid=runNSaveRes(cmnd,logFilePath)
	#print pid
	if is_integer(pid)==False:
		print aName+'Error: could not start adb logging'
		return False
	'''
	print aName+'Adb Logging started!'
	
	return pid

def pushFile(devName,infpath,pushpath):
	cmnd='adb -s '+devName+' push '+infpath+' '+pushpath
	os.system(cmnd)


#ip.src!=10.0.0.0/16 or ip.dst!=10.0.0.0/16
def recTcpDump(devName,dumpFilePath):
	#print 'Recording tcpdump'
	#cmnd='tcpdump -i any -p -s 0 -w '+dumpFilePath
	cmnd=['adb','-s',devName,'shell','tcpdump','-i','any','-p','-s','0','-w',dumpFilePath] 
	pid=runNGetPid(cmnd)
	if is_integer(pid)==False:
		print 'Error: could not start tcpdump logging'
		return "Failure"
	print '['+devName+'] : Tcpdump Logging started!'
	return pid
	
def recTcpDumpOverHttp(devName,dumpFilePath):
	#print 'Recording tcpdump'
	#cmnd='tcpdump -i any -p -s 0 -w '+dumpFilePath
	cmnd='adb -s '+devName+' shell tcpdump -i any -p -s 0 -w '+dumpFilePath+' port 3128 &' 
	os.system(cmnd)
	'''
	if (output.find('tcpdump: listening on ')<0):
		print 'Error: could not start tcpdump logging'
		return "Failure"
	print '['+devName+'] : Tcpdump Logging started!'
	return pid
'''
def setDateTime(emName):
	cmnd='sh getSysTime.sh '+emName
	runTermCmnd(cmnd)		

def setScreenON(devName):
	cmnd='adb -s '+devName+' shell dumpsys power | grep "mScreenOn="'
	output=runTermCmnd(cmnd)
	screenState=re.search(".*mScreenOn=([^']*)\n",output).groups()[0]
	print screenState
	if(screenState=='false'):
		#Turn On Screen, by pressing power key
		cmnd='adb -s '+devName+' shell input keyevent 26'
		runTermCmnd(cmnd)

def createFile(devName,fPath):
	cmnd='adb -s '+devName+' shell  "> '+fPath+'"'
	#print cmnd
	os.system(cmnd)
	
def startSysTrace(devName,app_pid,rfile):
	#print rfile
	
	emString="["+devName+"] : "
	if is_integer(app_pid)==False:
		cmnd='adb -s '+devName+' shell strace -s 99999 -o '+rfile+' '+app_pid
	else:
		cmnd='adb -s '+devName+' shell strace -s 99999 -o '+rfile+' -p '+str(app_pid)
	strace_pid=runNGetPid(cmnd);
	#print "Strace id is "+str(strace_pid)
	#print "Spid "+str(strace_pid)
	if is_integer(strace_pid)==False:
		print emString+'Error: could not start system call tracing'
		return "Failure"
	print emString+'System call trace started for pid : '+str(app_pid)
	#hh.hook_app_proc(devName,str(app_pid))
	return strace_pid
	
	
def is_integer(mystr):
	try:
		int(mystr)
		return True
	except ValueError:
		return False

def getProcId(devName,procName):
	
	cmnd="adb -s "+devName+" shell ps | grep "+procName+" | awk '{print $2}' | tail -1"
	pid=runTermCmnd(cmnd)
	
	if (pid<>None and pid<>''):
		return pid.strip()
	else:
		return -1

def stopAndProcessId(devName,pid):
	if(int(pid)>0):
		cmnd="adb	-s "+devName+" shell kill "+str(pid)
		#print 'kill command'+cmnd
		os.system(cmnd)
	
def pullFile(devName, sPath,dPath):
	emString="["+devName+"] : "
	print emString+'Pulling file/(s) from '+sPath+' to '+dPath
	cmnd='adb -s '+devName+' pull '+sPath+' '+dPath
	restext=runTermCmnd(cmnd)
	if(restext.find('does not exist')>=0):
		print emString+'File/directory '+sPath+' does not exist'
	
def getPatternfrmFile(fpath,pattern):
	cmnd='grep "'+pattern+'" '+ fpath
	return runTermCmnd(cmnd)
	
def setLocation(latitude,longitude,emport):
	cmnd="echo 'geo fix "+str(latitude)+" "+str(longitude)+" 4433' | telnet localhost "+emport
	os.system(cmnd)

def rmFile(devName,fPath):
	emString="["+devName+"] : "
	print emString+'Removing file: '+fPath+' from device!'
	cmnd='adb -s '+devName+' shell rm '+fPath+' &'
	os.system(cmnd)

def capSnapShot(devName,dPath):
	emString="["+devName+"] : "
	print emString+'Capturing snapshot!'
	cmnd='adb -s '+devName+' shell screencap -p '+dPath
	os.system(cmnd)

def startComponents(devName,actlist,servlist,recvlist,packName):
	for serv in servlist:
		cmnd='adb -s '+devName+' shell am startservice '+packName+'/'+serv
		os.system(cmnd)
		if(emulatorExists(devName)==False):
			return False
		
	for act in actlist:
		cmnd='adb -s '+devName+' shell am start -n '+packName+'/'+act
		os.system(cmnd)
		if(emulatorExists(devName)==False):
			return False
	return True
		
		
def procExists(avdName,procId):
	cmnd='adb -s '+avdName+' shell ps -p '+str(procId)
	resText=runTermCmnd(cmnd)
	#print resText
	resText=resText.strip()
	resText=len(resText.split('\n'))
	if(resText<=1):
		#print 'Proc does not exist'
		#print resText
		return False
	#print resText
	return True

