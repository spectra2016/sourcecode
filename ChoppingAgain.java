
import java.io.File;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChoppingAgain {

    public static void main(String[] args) {

        try {
            File fDeep = new File("/home/jyoti/spectrafinal/results/deep.txt");
            Formatter f = new Formatter("/home/jyoti/spectrafinal/results/Filling.txt");
            if (fDeep.exists()) {

                Scanner scanFromDeep = new Scanner(fDeep);

                int totalNoOfLines = 0;

                int p = 0;
                List<String> totalForm = new ArrayList<String>();
                List<String> apisName = new ArrayList<String>();
                List argsName = new ArrayList<String>();
                int[] freq = {};
                while (scanFromDeep.hasNextLine()) {

                    totalForm.add(scanFromDeep.nextLine().toString());
                    // System.out.printf("%s", totalForm);
                    p++;

                }

                //  System.out.println("P is " + p);
                p = 1;
                String chopedapi = "";
                int indexSpace = 0;

                for (String l : totalForm) {

                    if (l.contains("Ljavax") || l.contains("Ljava")) {

                        if (l.contains("\t\t\t") /*|| l.contains(" ")*/) {

                            StringTokenizer st = new StringTokenizer(l);
                            String apiName, ar, fre;
                            int convertedFre;

                            String next = st.nextToken("\t\t\t");

                            apiName = next.trim();

                            //  System.out.println("Api Name :- " + next);
                            next = st.nextToken("\t\t\t");

                            ar = next.trim();;
                            // System.out.println("Args :- " + next);
                            //  System.out.println(next);
                            next = st.nextToken("\t\t\t");

                            fre = (next.trim());

                            convertedFre = Integer.parseInt(fre);
                            // System.out.println("Frequency  :-   " + next);

                            f.format("%s\n", l);

                            checking(apiName, ar, fre);

                            //System.out.println();

                        }
                    }
                    p++;

                }

            }

            f.close();

        } catch (Exception e) {
            Logger.getLogger(ChoppingAgain.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    private static void checking(String api, String arg, String occurence) {
  
        
        
        //System.out.println("");
        String getapi = api;
        String getarg = arg;
        String getFre = occurence;

        if (getapi.contains("KeyPairGenerator;->initialize")) {
            //System.out.println("Get Api  :- " + api + " " + " get ars :- " + arg + " Get occ :-  " + occurence);

            if (getarg.contains("I=")) {

                int index = getarg.indexOf("I=");

                //System.out.println(index);
                String getTheValue = getarg.substring(index + 2);
                int getTheValueOfInInt = Integer.parseInt(getTheValue);
                if (getTheValueOfInInt < 2048) {

                    System.out.println("RSA ALGORITHM WITH KEY-SIZE < 2148");
                }

            }

        } 
	 if (getapi.contains("MessageDigest;->getInstance")) {

            // System.out.println("This is contains the MessageDigest;-getInstance");
            if (getarg.contains("SHA-1") || getarg.contains("MD4") || getarg.contains("MD5")) {

                System.out.println("MESSAGE DIGEST ALGORITHM IS VULNERABLE....");

            }

        } 

	if (getapi.contains("Cipher;->getInstance")) {

            // System.out.println("This is contains the MessageDigest;-getInstance");
            if (getarg.contains("AES") ) {

                System.out.println("AES WITHOUT MODE SPECIFICATION USES ECB MODE WHICH IS VULNERABLE");

            }
	if (getarg.contains("ECB")) {

                System.out.println("ECB MODE IS VULNERABLE");

            }


	if (getarg.contains("NoPadding")) {

                System.out.println("AES WITHOUT PADDING IS VULNERABLE");

            }


        } 

	if (getapi.contains("SecretKeySpec;-><init>")) {

            if (getarg.contains("Ljavax") || getarg.contains("Ljava")) {
		if (getarg.contains("AES")) {
		System.out.println("AES WITHOUT MODE SPECIFICATION USES ECB MODE WHICH IS VULNERABLE");
		}
                 if (getarg.contains("AES") && getarg.contains("ECB")) {

                    System.out.println("ECB MODE IS VULNERABLE");
                } 
		 if (getarg.contains("AES") && getarg.contains("NoPadding")) {

                    System.out.println("AES WITHOUT PADDING IS VULNERABLE");
                } 
		if (getarg.contains("NoPadding")) {

                    System.out.println("AES WITHOUT PADDING IS VULNERABLE");

                }

            } 
		if (getarg.contains("[B=")) {

                if ((Integer.parseInt(getFre)) > 1) {

                    System.out.println("SAME KEY MATERIAL IS USED MULTIPLE TIMES WHICH MAKES IT VULNERABLE.....");
                }
            }
        } 

	 if (getapi.contains("IvParameterSpec;-><init>")) {

            // System.out.println("This is contains the MessageDigest;-getInstance");
            if (getarg.contains("[B=")) {

                if ((Integer.parseInt(getFre)) > 1) {

                    System.out.println("SAME INITIALIZATION VECTOR IS USED MULTIPLE TIMES");
                }

            }

        }

if (getapi.contains("SecureRandom;->setSeed")) {

            // System.out.println("This is contains the MessageDigest;-getInstance");
            if (getarg.contains("[C=")) {

                if ((Integer.parseInt(getFre)) > 1) {

                    System.out.println("SAME SEED IS USED MULTIPLE TIMES");
                }

            } else if (getarg.contains("[B=")) {

                if ((Integer.parseInt(getFre)) > 1) {

                    System.out.println("SAME SEED IS USED MULTIPLE TIMES");
                }
            }

}





 if (getapi.contains("PBEKeySpec;-><init>")) {

            // System.out.println("This is contains the MessageDigest;-getInstance");
            if (getarg.contains("[C=")) {

                if ((Integer.parseInt(getFre)) > 1) {

                    System.out.println("SAME INITIALIZATION VECTOR IS USED MULTIPLE TIMES");
                }

            } else if (getarg.contains("[B=")) {

                if ((Integer.parseInt(getFre)) > 1) {

                    System.out.println("SAME INITIALIZATION VECTOR IS USED MULTIPLE TIMES");
                }
            } else if (getarg.contains("I=")) {
                int index = getarg.indexOf("I=");

                String getTheValue = getarg.substring(index + 2);
                int getTheValueOfInInt = Integer.parseInt(getTheValue);
                if (getTheValueOfInInt < 1000) {
                    System.out.println("ITERATION COUNT < 1000");

                }
            }
        }

    }
}
