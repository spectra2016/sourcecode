.class public Lcom/example/abhishek/prefinal/MainActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/example/abhishek/prefinal/MainActivity$ViewPagerAdapter;
    }
.end annotation


# instance fields
.field private tabIcons:[I

.field private tabLayout:Landroid/support/design/widget/TabLayout;

.field private toolbar:Landroid/support/v7/widget/Toolbar;

.field private viewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 26
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/example/abhishek/prefinal/MainActivity;->tabIcons:[I

    .line 76
    return-void

    .line 26
    :array_0
    .array-data 4
        0x7f020041
        0x7f020042
        0x7f020040
    .end array-data
.end method

.method private setupTabIcons()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f04001d

    const/4 v5, 0x0

    .line 52
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 53
    .local v0, "tabOne":Landroid/widget/TextView;
    const-string v3, "Help"

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    const v3, 0x7f020041

    invoke-virtual {v0, v5, v3, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 55
    iget-object v3, p0, Lcom/example/abhishek/prefinal/MainActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v3, v5}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/support/design/widget/TabLayout$Tab;->setCustomView(Landroid/view/View;)Landroid/support/design/widget/TabLayout$Tab;

    .line 57
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 58
    .local v2, "tabTwo":Landroid/widget/TextView;
    const-string v3, "Upload Url"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    const v3, 0x7f020042

    invoke-virtual {v2, v5, v3, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 60
    iget-object v3, p0, Lcom/example/abhishek/prefinal/MainActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/design/widget/TabLayout$Tab;->setCustomView(Landroid/view/View;)Landroid/support/design/widget/TabLayout$Tab;

    .line 62
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 63
    .local v1, "tabThree":Landroid/widget/TextView;
    const-string v3, "Statistics"

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    const v3, 0x7f020040

    invoke-virtual {v1, v5, v3, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 65
    iget-object v3, p0, Lcom/example/abhishek/prefinal/MainActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/support/design/widget/TabLayout$Tab;->setCustomView(Landroid/view/View;)Landroid/support/design/widget/TabLayout$Tab;

    .line 66
    return-void
.end method

.method private setupViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 3
    .param p1, "viewPager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 69
    new-instance v0, Lcom/example/abhishek/prefinal/MainActivity$ViewPagerAdapter;

    invoke-virtual {p0}, Lcom/example/abhishek/prefinal/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/example/abhishek/prefinal/MainActivity$ViewPagerAdapter;-><init>(Lcom/example/abhishek/prefinal/MainActivity;Landroid/support/v4/app/FragmentManager;)V

    .line 70
    .local v0, "adapter":Lcom/example/abhishek/prefinal/MainActivity$ViewPagerAdapter;
    new-instance v1, Lcom/example/abhishek/prefinal/Helpactivity;

    invoke-direct {v1}, Lcom/example/abhishek/prefinal/Helpactivity;-><init>()V

    const-string v2, "Help"

    invoke-virtual {v0, v1, v2}, Lcom/example/abhishek/prefinal/MainActivity$ViewPagerAdapter;->addFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 71
    new-instance v1, Lcom/example/abhishek/prefinal/Uploadurlactivity;

    invoke-direct {v1}, Lcom/example/abhishek/prefinal/Uploadurlactivity;-><init>()V

    const-string v2, "Upload Url"

    invoke-virtual {v0, v1, v2}, Lcom/example/abhishek/prefinal/MainActivity$ViewPagerAdapter;->addFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 72
    new-instance v1, Lcom/example/abhishek/prefinal/Graphactivity;

    invoke-direct {v1}, Lcom/example/abhishek/prefinal/Graphactivity;-><init>()V

    const-string v2, "Statistics"

    invoke-virtual {v0, v1, v2}, Lcom/example/abhishek/prefinal/MainActivity$ViewPagerAdapter;->addFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 74
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    const v0, 0x7f04001a

    invoke-virtual {p0, v0}, Lcom/example/abhishek/prefinal/MainActivity;->setContentView(I)V

    .line 38
    const v0, 0x7f0d006a

    invoke-virtual {p0, v0}, Lcom/example/abhishek/prefinal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/example/abhishek/prefinal/MainActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 39
    iget-object v0, p0, Lcom/example/abhishek/prefinal/MainActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/example/abhishek/prefinal/MainActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 41
    invoke-virtual {p0}, Lcom/example/abhishek/prefinal/MainActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 43
    const v0, 0x7f0d006d

    invoke-virtual {p0, v0}, Lcom/example/abhishek/prefinal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/example/abhishek/prefinal/MainActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 44
    iget-object v0, p0, Lcom/example/abhishek/prefinal/MainActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-direct {p0, v0}, Lcom/example/abhishek/prefinal/MainActivity;->setupViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 46
    const v0, 0x7f0d006b

    invoke-virtual {p0, v0}, Lcom/example/abhishek/prefinal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    iput-object v0, p0, Lcom/example/abhishek/prefinal/MainActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    .line 47
    iget-object v0, p0, Lcom/example/abhishek/prefinal/MainActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    iget-object v1, p0, Lcom/example/abhishek/prefinal/MainActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 48
    invoke-direct {p0}, Lcom/example/abhishek/prefinal/MainActivity;->setupTabIcons()V

    .line 49
    return-void
.end method
