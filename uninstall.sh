#!/bin/bash
apkname=$1
new=`aapt list -a $apkname | sed -n "/^Package Group[^s]/s/.*name=//p"`
exist=`adb shell pm list packages| grep $new`
echo "UNINSTALLING APP"
if [ -n "$exist" ]; then
   adb uninstall $new
fi
