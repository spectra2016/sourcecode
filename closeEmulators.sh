ps aux | grep -F "emulator64-arm" | awk '{print $2}' | xargs kill -9
ps aux | grep -F "adb" | awk '{print $2}' | xargs kill -9
echo "Resources released!"
