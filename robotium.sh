#!/bin/bash
apkname=$1
echo $apkname
dir=`pwd`
pwddir="$HOME/spectrafinal"
cd $pwddir
a=$pwddir
echo $a
#testpath is Robotium test project path 
testpath="$a/ExampleApplicationTesting2"
echo "testpath is:"
echo $testpath
echo "robotium shell script started with apk name " $apkname

new=`aapt list -a $apkname | sed -n "/^Package Group[^s]/s/.*name=//p"`
echo "Package = " $new
old=`grep -o -P '(?<=(targetPackage=")).*(?=")' $testpath/AndroidManifest.xml`
echo "Package = " $old
sed -i "s@$old@$new@" $testpath/AndroidManifest.xml
#sed -i "s@$old@$new@" $testpath/bin/AndroidManifest.xml

sed -i "s@$old@$new@" $testpath/src/com/example/any/test/ExampleTest.java
 
olaun=`grep -o -P '(?<=(LAUNCHER_ACTIVITY_FULL_CLASSNAME = ")).*(?=")' $testpath/src/com/example/any/test/ExampleTest.java`
echo "old launcher = " $olaun
nlaun=`aapt d --values badging $apkname | grep "launchable-activity"|cut -d" " -f2|cut -c 7-|rev | cut -c 2- | rev`
echo "New launcher = " $nlaun
sed -i "s@$olaun@$nlaun@" $testpath/src/com/example/any/test/ExampleTest.java
echo "Successfully changed Manifest and script in Robotium Test"

cd $testpath
echo "current directory : "
echo $testpath
echo "Building test project"
android update project --path .
echo "running ant"
ant clean debug
echo "ROBOTIUM BUILD SUCCESSFULLY"
cd $pwddir/results
rm -rf $pwddir/results/signed_$apkname ||true
rm -rf $pwddir/results/ExampleApplicationTesting2-debug.apk|| true
rm -rf $pwddir/results/signed_ExampleApplicationTesting2-debug.apk ||true
cp $testpath/bin/ExampleApplicationTesting2-debug.apk $pwddir/results/ExampleApplicationTesting2-debug.apk
echo "apkname is " $apkname
apkfile=`basename $apkname`
echo "apkfile before sign" 
echo $apkfile
echo "Signing APK to be tested......"
sh $pwddir/results/signapk.sh $pwddir/results/$apkfile $pwddir/results/my-release-key.keystore gajrani alias_name
echo "Signing Robotest App......"
sh $pwddir/results/signapk.sh $pwddir/results/ExampleApplicationTesting2-debug.apk $pwddir/results/my-release-key.keystore gajrani alias_name

echo "Installing test App......"
exist=`adb shell pm list packages| grep $new`
echo "checking it exists "
if [ -n "$exist" ]; then
   adb uninstall $new
fi


a1="signed"
a2="_"
a1="${a1}${a2}"
signapkfile="${a1}${apkfile}"

echo "${signapkfile}"

adb install $pwddir/results/$signapkfile

echo "Installing Robotium App......"

existt=`adb shell pm list packages| grep com.example.any.test`
if [ -n "$existt" ]; then
  adb uninstall com.example.any.test
fi

adb install $pwddir/results/signed_ExampleApplicationTesting2-debug.apk
echo "Starting App Testing......"
cd $dir
echo "base directory"

