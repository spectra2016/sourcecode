import os
from androguard.core.bytecodes import apk
from androguard.core.bytecodes import dvm
from androguard import androlyze  #the androlyze.py file has to be copied in the androguard>androguard directory for the script to work
#import run
import sys                
#import sys
#print "In Static Analysis"
#pwddir = os.getcwd()
pwddir = "$HOME/spectrafinal/"
fileopen = str(sys.argv[1])
outfile = str(sys.argv[1]) + '.txt'
print  "File : " + fileopen
tempfile=pwddir+"temp.txt"
f = open(tempfile, 'w')
sys.stdout = f
print  fileopen
descfile=pwddir+"descriptor.txt"
myfile1 = open(descfile, "w")
myfile = open(outfile, "w")
a, d, dx = androlyze.AnalyzeAPK(fileopen, decompiler = 'dad')
#apk_path = pwddir+"/"+fileopen
print "NAME OF APK FILE :    " + fileopen + "\n"
#print "PATH OF APK FILE :    " + apk_path + "\n"

for current_method in d.get_methods():				
	str1 = current_method.get_descriptor()
	str2 = "crypto"
	str5 = "[B"
	str6 = "String"
        compare1= str1.find(str2)
	compare3 = str1.find(str5)
	compare4 = str1.find(str6)
			
	if (compare1 > -1 or compare3 > -1 or compare4 > -1):
		print "\n\n\n\n"
		myfile.write(current_method.get_class_name()+"\t"+current_method.get_name()                              				+"\t"+current_method.get_descriptor()+"\n")
		#myfile1.write(current_method.get_class_name()+"\t"+current_method.get_name()                              				+"\t"+current_method.get_descriptor()+"\n")

         	method_name = current_method.get_name()
		class_name = current_method.get_class_name()
		count = 0			
		for current_method_2 in d.get_methods():					
			if current_method_2.get_name() == method_name:
		 		if current_method_2.get_class_name() == class_name:	
					count = count + 1
		print "NUMBER OF METHODS WITH NAME:    " + method_name + " -> " + str(count)
		print "\n"+current_method.get_class_name() + "\n"
		class_path = current_method.get_class_name()[1:-1]
		print "CLASS PATH:    " + class_path + "\n"
		complete_class_path = class_path
		print "COMPLETECLASSPATH: "+complete_class_path + "\n"	
		if count == 1:
			class_name = current_method.get_class_name()[:-1]		
			class_name = class_name.replace("$","_")					
			source_code_path = "d.CLASS_"+class_name+".METHOD_"+current_method.get_name()
			source_code_path = source_code_path.replace("/","_")
			source_code_path = source_code_path.replace("(","_")
			source_code_path = source_code_path.replace(")","")
			source_code_path = source_code_path.replace(";","")
			source_code_path = source_code_path.replace(" ","")
			source_code_path = source_code_path.replace("[","")
			source_code_path = source_code_path.replace("<","")
			source_code_path = source_code_path.replace(">","")
			source_code_path = source_code_path.replace("$","")				
			print "SOURCE CODE PATH:    "+source_code_path
			source_code_command = "print "+source_code_path+".get_source()"
			str3 = current_method.get_name()				
			str4 = "$"
			compare2 = str3.find(str4)
			if compare2 < 0: 				
				print "\nSOURCE CODE COMMAND:    "+source_code_command
				print "\n"
				exec source_code_command
		if count > 1:
			class_name = current_method.get_class_name()[:-1]		
			class_name = class_name.replace("$","_")
			source_code_path = "d.CLASS_"+class_name+".METHOD_"+current_method.get_name()+current_method.get_descriptor()
			source_code_path = source_code_path.replace("/","_")
			source_code_path = source_code_path.replace("(","_")
			source_code_path = source_code_path.replace(")","")
			source_code_path = source_code_path.replace(";","")
			source_code_path = source_code_path.replace(" ","")
			source_code_path = source_code_path.replace("[","")
			source_code_path = source_code_path.replace("<","")
			source_code_path = source_code_path.replace(">","")
			source_code_path = source_code_path.replace("$","")				
			print "SOURCE CODE PATH:    "+source_code_path
			source_code_command = "print "+source_code_path+".get_source()"
			str3 = current_method.get_name()				
			str4 = "$"
			compare2 = str3.find(str4)
			if compare2 < 0: 				
				print "\nSOURCE CODE COMMAND:    "+source_code_command
				print "\n"
				exec source_code_command 
				

print "\n\nDONE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
myfile.close()
myfile1.close()
	
        
exit(0)
