#usage sh findurlstatic.sh <appcompletepath>
appfolder=`echo $1 | rev | cut -c 5- | rev`
echo $appfolder
appfol=`echo $appfolder | rev | cut -d'/' -f 1 | rev`
echo $appfol
cd $HOME/spectrafinal/results
rm -rf $appfol || true

apktool d $1 
cd $appfol
pwd
grep -r -i --include \*.smali "http://" . > $HOME/spectrafinal/results/urls.txt
chmod 777 $HOME/spectrafinal/results/urls.txt
a='$HOME/spectrafinal/results/'
b='.apk.urls.txt'
c=$a$appfol$b
grep -Po '".*?"' $HOME/spectrafinal/results/urls.txt|sort|uniq > $c
echo "RESULT OF URL ANALYSIS IN" $c
chmod 777 $c
