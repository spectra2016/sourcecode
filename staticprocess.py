import os
import sys
outfile=str(sys.argv[1])
f = open(outfile, 'w')
sys.stdout = f
pwddir = "$HOME/spectrafinal/"
tempfile=pwddir+"temp.txt"


#opening temp.txt and checking each rule
f1 = open(tempfile,"r") 
for line in f1:
	if 'COMPLETECLASSPATH' in line:
		classname = str(line)
	if 'Cipher.getInstance("DES' in line:
		print classname+line+"\tBroken Encryption Algorithm DES Used!!\n"
	if 'Cipher.getInstance("AES")' in line:
		print classname+line +"\tALGORITHM SHOULD BE AES/CBC!!\n"
	if 'Cipher.getInstance("AES/ECB' in line:
		print classname+line+"\tALGORITHM SHOULD BE AES/CBC!!\n"	
	if '.init(' in line:
		print classname+line+"\tIV SHOULD NOT BE STATIC!!\n"	
	if 'SecretKeySpec' in line:
		print classname+line+"\tKEY (1st Parameter) SHOULD NOT BE STATIC and ALGORITHM SHOULD BE AES/CBC!!\n"	
	if 'PBEKeySpec(' in line:
		print classname+line+"\tITERATION COUNT (3rd Parameter) MUST BE > 1000, SALT SHOULD NOT BE STATIC!!\n"
	if 'PBEParameterSpec(' in line:
		print classname+line+"\tITERATION COUNT MUST BE > 1000, SALT SHOULD NOT BE STATIC, ALGORITHM SHOULD BE AES/CBC!!\n"
	if 'SecureRandom(' in line:
		print classname+line+"\tSEED SHOULD NOT BE STATIC!!\n"
	if 'setSeed(' in line:
		print classname+line+"\tSEED SHOULD NOT BE STATIC!!\n"
	if 'Signature.getInstance("SHA1' in line:
		print classname+line+"\tIT SHOULD BE SHA256 OR HIGHER!!\n"
	if 'Signature.getInstance("SHA-1' in line:
		print classname+line+"\tIT SHOULD BE SHA256 OR HIGHER!!\n"
	if 'Signature.getInstance("MD5' in line:
		print classname+line+"\tIT SHOULD BE SHA256 OR HIGHER!!\n"
sys.stdout = sys.__stdout__
f.close();
print "STATIC ANALYSIS RESULTS ARE IN : " + outfile	
