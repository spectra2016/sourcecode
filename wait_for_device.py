import sys
import os
import logging
import AdbSupport as Adb
import time
from Run import runTermCmnd
from Run import stopProcessId
from Run import runInNewTerminal
#import CreateFeatureFiles as CFF
import urllib2


wDir=os.path.abspath(os.path.dirname(__file__))
os.chdir(wDir)
if os.path.exists(wDir+os.sep+'RunLogs')==False:
	cmnd='mkdir RunLogs'
	os.system(cmnd)

logfilepath=wDir+os.sep+'RunLogs'+os.sep+'runlog_'+str(time.time())+'.lg'
logger = logging.getLogger('myapp')
hdlr = logging.FileHandler(logfilepath)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.DEBUG)  


def waitfordevice(dName,blPath,boot_type):
	emString="["+dName+"] : "
	timeout=10 #In minutes
	flag=0
	res=Adb.recAdbLog(dName,blPath)
	logger.info("%sStarted boot logging!" % emString)
	print emString+"Started boot logging!"
	print "result:"+str(res)
	if(res=="Failure"):
		return False
	pid=int(res)
	bi_time=time.time()
	while(time.time()-bi_time<(timeout*60)):
		#print 'dfsdfsfgsfgs'
		cmnd1='cat '+blPath+'|grep "PowerManagerService.*Boot animation finished."|wc -l'
		termOut1=runTermCmnd(cmnd1)
		cmnd3='cat '+blPath+'|grep "PowerManagerService.*bootCompleted"|wc -l'
		termOut3=runTermCmnd(cmnd3)
		#cmnd3='cat '+blPath+'|grep "PowerManagerService.*Going to sleep due to screen timeout..."|wc -l'
		#termOut3=runTermCmnd(cmnd3)
		if (Adb.is_integer(termOut1)):
			if(int(termOut1)>0):
				#print 'dfsdfsfgsfgs'
				flag=1
				logger.info('%sDevice/Emulator boot successful and its online..' % emString)
				print emString+'Device/Emulator boot successful and its online..'
				break
		if (Adb.is_integer(termOut3)):
			if(int(termOut3)>0):
				#print 'dfsdfsfgsfgsdghdhdfhdfh'
				flag=1
				logger.info('%sDevice/Emulator boot successful and its online..' % emString)
				print emString+'Device/Emulator boot successful and its online..'
				break
	cmnd='kill -9 '+str(pid)
	retCode=os.system(cmnd)

	if retCode<>0:
		logger.info('%sError: Could not stop boot logging!' % emString)
		print emString+'Error: Could not stop boot logging!'
	else:
		logger.info('%sBoot log stopped.' % emString)
		print emString+'Boot log stopped.'
	
	
	if flag==0:
		logger.info('%sError: Device is taking more time to start!' % emString)
		print emString+'Error: Device is taking more time to start!'
		return False
	return True
