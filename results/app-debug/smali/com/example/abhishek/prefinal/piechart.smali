.class public Lcom/example/abhishek/prefinal/piechart;
.super Landroid/view/View;
.source "piechart.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/example/abhishek/prefinal/piechart$OnSelectedLisenter;
    }
.end annotation


# static fields
.field private static final DEGREE_360:I = 0x168

.field public static final ERROR_NOT_EQUAL_TO_100:Ljava/lang/String; = "NOT_EQUAL_TO_100"

.field private static PIE_COLORS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static iColorListSize:I


# instance fields
.field private alPercentage:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field array:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private canvas1:Landroid/graphics/Canvas;

.field private centerCircle:Landroid/graphics/RectF;

.field private fDensity:F

.field private fEndAngle:F

.field private fStartAngle:F

.field fX:F

.field fY:F

.field private iCenterWidth:I

.field private iDataSize:I

.field private iDisplayHeight:I

.field private iDisplayWidth:I

.field private iMargin:I

.field private iSelectedIndex:I

.field private iShift:I

.field private mCenterX:I

.field private mCenterY:I

.field private onSelectedListener:Lcom/example/abhishek/prefinal/piechart$OnSelectedLisenter;

.field private paintCenterCircle:Landroid/graphics/Paint;

.field private paintPieBorder:Landroid/graphics/Paint;

.field private paintPieFill:Landroid/graphics/Paint;

.field private r:Landroid/graphics/RectF;

.field temp:F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 30
    const-class v0, Lcom/example/abhishek/prefinal/piechart;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/example/abhishek/prefinal/piechart;->TAG:Ljava/lang/String;

    .line 34
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "#FF00FF"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "#C0C0C0"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "#808080"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "#FF0000"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "#808000"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "#800080"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "#00FFFF"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "#00FF00"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "#008080"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "#0000FF"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "#000080"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "#000000"

    aput-object v2, v0, v1

    sput-object v0, Lcom/example/abhishek/prefinal/piechart;->PIE_COLORS:[Ljava/lang/String;

    .line 35
    sput v3, Lcom/example/abhishek/prefinal/piechart;->iColorListSize:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    iput-object v3, p0, Lcom/example/abhishek/prefinal/piechart;->onSelectedListener:Lcom/example/abhishek/prefinal/piechart$OnSelectedLisenter;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/example/abhishek/prefinal/piechart;->alPercentage:Ljava/util/ArrayList;

    .line 41
    const/16 v0, 0x140

    iput v0, p0, Lcom/example/abhishek/prefinal/piechart;->mCenterX:I

    .line 42
    const/16 v0, 0x140

    iput v0, p0, Lcom/example/abhishek/prefinal/piechart;->mCenterY:I

    .line 44
    iput v5, p0, Lcom/example/abhishek/prefinal/piechart;->iSelectedIndex:I

    .line 45
    iput v2, p0, Lcom/example/abhishek/prefinal/piechart;->iCenterWidth:I

    .line 46
    iput v2, p0, Lcom/example/abhishek/prefinal/piechart;->iShift:I

    .line 47
    iput v2, p0, Lcom/example/abhishek/prefinal/piechart;->iMargin:I

    .line 48
    iput v2, p0, Lcom/example/abhishek/prefinal/piechart;->iDataSize:I

    .line 50
    iput-object v3, p0, Lcom/example/abhishek/prefinal/piechart;->r:Landroid/graphics/RectF;

    .line 51
    iput-object v3, p0, Lcom/example/abhishek/prefinal/piechart;->centerCircle:Landroid/graphics/RectF;

    .line 52
    iput v1, p0, Lcom/example/abhishek/prefinal/piechart;->fDensity:F

    .line 53
    iput v1, p0, Lcom/example/abhishek/prefinal/piechart;->fStartAngle:F

    .line 54
    iput v1, p0, Lcom/example/abhishek/prefinal/piechart;->fEndAngle:F

    .line 88
    iput v1, p0, Lcom/example/abhishek/prefinal/piechart;->temp:F

    .line 61
    sget-object v0, Lcom/example/abhishek/prefinal/piechart;->PIE_COLORS:[Ljava/lang/String;

    array-length v0, v0

    sput v0, Lcom/example/abhishek/prefinal/piechart;->iColorListSize:I

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/example/abhishek/prefinal/piechart;->array:Ljava/util/ArrayList;

    .line 63
    invoke-direct {p0, p1}, Lcom/example/abhishek/prefinal/piechart;->fnGetDisplayMetrics(Landroid/content/Context;)V

    .line 64
    const/high16 v0, 0x41f00000    # 30.0f

    invoke-direct {p0, v0}, Lcom/example/abhishek/prefinal/piechart;->fnGetRealPxFromDp(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/example/abhishek/prefinal/piechart;->iShift:I

    .line 65
    const/high16 v0, 0x42200000    # 40.0f

    invoke-direct {p0, v0}, Lcom/example/abhishek/prefinal/piechart;->fnGetRealPxFromDp(F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/example/abhishek/prefinal/piechart;->iMargin:I

    .line 66
    new-instance v0, Landroid/graphics/RectF;

    const/high16 v1, 0x43480000    # 200.0f

    const/high16 v2, 0x43480000    # 200.0f

    const/high16 v3, 0x43dc0000    # 440.0f

    const/high16 v4, 0x43dc0000    # 440.0f

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/example/abhishek/prefinal/piechart;->centerCircle:Landroid/graphics/RectF;

    .line 68
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/example/abhishek/prefinal/piechart;->paintPieFill:Landroid/graphics/Paint;

    .line 69
    iget-object v0, p0, Lcom/example/abhishek/prefinal/piechart;->paintPieFill:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 71
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/example/abhishek/prefinal/piechart;->paintCenterCircle:Landroid/graphics/Paint;

    .line 72
    iget-object v0, p0, Lcom/example/abhishek/prefinal/piechart;->paintCenterCircle:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 73
    iget-object v0, p0, Lcom/example/abhishek/prefinal/piechart;->paintCenterCircle:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 75
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/example/abhishek/prefinal/piechart;->paintPieBorder:Landroid/graphics/Paint;

    .line 76
    iget-object v0, p0, Lcom/example/abhishek/prefinal/piechart;->paintPieBorder:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 77
    iget-object v0, p0, Lcom/example/abhishek/prefinal/piechart;->paintPieBorder:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-direct {p0, v1}, Lcom/example/abhishek/prefinal/piechart;->fnGetRealPxFromDp(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 78
    iget-object v0, p0, Lcom/example/abhishek/prefinal/piechart;->paintPieBorder:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 79
    sget-object v0, Lcom/example/abhishek/prefinal/piechart;->TAG:Ljava/lang/String;

    const-string v1, "PieChart init"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    return-void
.end method

.method private fnGetDisplayMetrics(Landroid/content/Context;)V
    .locals 2
    .param p1, "cxt"    # Landroid/content/Context;

    .prologue
    .line 206
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 207
    .local v0, "dm":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/example/abhishek/prefinal/piechart;->fDensity:F

    .line 208
    return-void
.end method

.method private fnGetRealPxFromDp(F)F
    .locals 2
    .param p1, "fDp"    # F

    .prologue
    .line 211
    iget v0, p0, Lcom/example/abhishek/prefinal/piechart;->fDensity:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/example/abhishek/prefinal/piechart;->fDensity:F

    mul-float/2addr p1, v0

    .end local p1    # "fDp":F
    :cond_0
    return p1
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 23
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 92
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 93
    sget-object v2, Lcom/example/abhishek/prefinal/piechart;->TAG:Ljava/lang/String;

    const-string v3, "onDraw"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/example/abhishek/prefinal/piechart;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/example/abhishek/prefinal/piechart;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float v9, v2, v3

    .line 95
    .local v9, "centerX":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/example/abhishek/prefinal/piechart;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/example/abhishek/prefinal/piechart;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float v10, v2, v3

    .line 96
    .local v10, "centerY":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/example/abhishek/prefinal/piechart;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/example/abhishek/prefinal/piechart;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float v18, v2, v3

    .line 97
    .local v18, "radius1":F
    move/from16 v0, v18

    float-to-double v2, v0

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v4

    double-to-float v0, v2

    move/from16 v18, v0

    .line 98
    move-object/from16 v0, p0

    iget v2, v0, Lcom/example/abhishek/prefinal/piechart;->mCenterX:I

    int-to-float v0, v2

    move/from16 v19, v0

    .line 99
    .local v19, "startX":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/example/abhishek/prefinal/piechart;->mCenterY:I

    int-to-float v0, v2

    move/from16 v20, v0

    .line 100
    .local v20, "startY":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/example/abhishek/prefinal/piechart;->mCenterX:I

    int-to-float v0, v2

    move/from16 v17, v0

    .line 101
    .local v17, "radius":F
    const/4 v15, 0x0

    .line 102
    .local v15, "medianAngle":F
    new-instance v16, Landroid/graphics/Path;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Path;-><init>()V

    .line 104
    .local v16, "path":Landroid/graphics/Path;
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/example/abhishek/prefinal/piechart;->iDataSize:I

    if-ge v14, v2, :cond_3

    .line 107
    sget v2, Lcom/example/abhishek/prefinal/piechart;->iColorListSize:I

    if-lt v14, v2, :cond_2

    .line 108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/example/abhishek/prefinal/piechart;->paintPieFill:Landroid/graphics/Paint;

    sget-object v3, Lcom/example/abhishek/prefinal/piechart;->PIE_COLORS:[Ljava/lang/String;

    sget v4, Lcom/example/abhishek/prefinal/piechart;->iColorListSize:I

    rem-int v4, v14, v4

    aget-object v3, v3, v4

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 114
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/example/abhishek/prefinal/piechart;->alPercentage:Ljava/util/ArrayList;

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/example/abhishek/prefinal/piechart;->fEndAngle:F

    .line 117
    move-object/from16 v0, p0

    iget v2, v0, Lcom/example/abhishek/prefinal/piechart;->fEndAngle:F

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/example/abhishek/prefinal/piechart;->fEndAngle:F

    .line 120
    move-object/from16 v0, p0

    iget v2, v0, Lcom/example/abhishek/prefinal/piechart;->iSelectedIndex:I

    if-ne v2, v14, :cond_0

    .line 121
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->save(I)I

    .line 122
    move-object/from16 v0, p0

    iget v2, v0, Lcom/example/abhishek/prefinal/piechart;->fStartAngle:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/example/abhishek/prefinal/piechart;->fEndAngle:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float v11, v2, v3

    .line 123
    .local v11, "fAngle":F
    const/high16 v2, 0x43b40000    # 360.0f

    add-float/2addr v2, v11

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v12

    .line 125
    .local v12, "dxRadius":D
    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/example/abhishek/prefinal/piechart;->fY:F

    .line 126
    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/example/abhishek/prefinal/piechart;->fX:F

    .line 127
    move-object/from16 v0, p0

    iget v2, v0, Lcom/example/abhishek/prefinal/piechart;->fX:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/example/abhishek/prefinal/piechart;->iShift:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/example/abhishek/prefinal/piechart;->fY:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/example/abhishek/prefinal/piechart;->iShift:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 130
    .end local v11    # "fAngle":F
    .end local v12    # "dxRadius":D
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/example/abhishek/prefinal/piechart;->r:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/example/abhishek/prefinal/piechart;->fStartAngle:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/example/abhishek/prefinal/piechart;->fEndAngle:F

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/example/abhishek/prefinal/piechart;->paintPieFill:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 131
    move-object/from16 v0, p0

    iget v2, v0, Lcom/example/abhishek/prefinal/piechart;->fStartAngle:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/example/abhishek/prefinal/piechart;->fEndAngle:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v4

    const-wide v4, 0x4066800000000000L    # 180.0

    div-double/2addr v2, v4

    double-to-float v8, v2

    .line 132
    .local v8, "angle":F
    move/from16 v0, v19

    float-to-double v2, v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v17, v4

    float-to-double v4, v4

    float-to-double v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v0, v2

    move/from16 v21, v0

    .line 133
    .local v21, "stopX":F
    move/from16 v0, v20

    float-to-double v2, v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v17, v4

    float-to-double v4, v4

    float-to-double v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v0, v2

    move/from16 v22, v0

    .line 137
    .local v22, "stopY":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/example/abhishek/prefinal/piechart;->iSelectedIndex:I

    if-ne v2, v14, :cond_1

    .line 138
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/example/abhishek/prefinal/piechart;->r:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/example/abhishek/prefinal/piechart;->fStartAngle:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/example/abhishek/prefinal/piechart;->fEndAngle:F

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/example/abhishek/prefinal/piechart;->paintPieBorder:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 139
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/example/abhishek/prefinal/piechart;->paintPieFill:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    move/from16 v3, v19

    move/from16 v4, v20

    move/from16 v5, v21

    move/from16 v6, v22

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 140
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 142
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/example/abhishek/prefinal/piechart;->fStartAngle:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/example/abhishek/prefinal/piechart;->fEndAngle:F

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/example/abhishek/prefinal/piechart;->fStartAngle:F

    .line 104
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 111
    .end local v8    # "angle":F
    .end local v21    # "stopX":F
    .end local v22    # "stopY":F
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/example/abhishek/prefinal/piechart;->paintPieFill:Landroid/graphics/Paint;

    sget-object v3, Lcom/example/abhishek/prefinal/piechart;->PIE_COLORS:[Ljava/lang/String;

    aget-object v3, v3, v14

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_1

    .line 145
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 149
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 152
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iput v1, p0, Lcom/example/abhishek/prefinal/piechart;->iDisplayWidth:I

    .line 153
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iput v1, p0, Lcom/example/abhishek/prefinal/piechart;->iDisplayHeight:I

    .line 155
    iget v1, p0, Lcom/example/abhishek/prefinal/piechart;->iDisplayWidth:I

    iget v2, p0, Lcom/example/abhishek/prefinal/piechart;->iDisplayHeight:I

    if-le v1, v2, :cond_0

    .line 156
    iget v1, p0, Lcom/example/abhishek/prefinal/piechart;->iDisplayHeight:I

    iput v1, p0, Lcom/example/abhishek/prefinal/piechart;->iDisplayWidth:I

    .line 162
    :cond_0
    iget v1, p0, Lcom/example/abhishek/prefinal/piechart;->iDisplayWidth:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/example/abhishek/prefinal/piechart;->iCenterWidth:I

    .line 163
    iget v1, p0, Lcom/example/abhishek/prefinal/piechart;->iCenterWidth:I

    iget v2, p0, Lcom/example/abhishek/prefinal/piechart;->iMargin:I

    sub-int v0, v1, v2

    .line 164
    .local v0, "iR":I
    iget-object v1, p0, Lcom/example/abhishek/prefinal/piechart;->r:Landroid/graphics/RectF;

    if-nez v1, :cond_1

    .line 165
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, Lcom/example/abhishek/prefinal/piechart;->iCenterWidth:I

    sub-int/2addr v2, v0

    int-to-float v2, v2

    iget v3, p0, Lcom/example/abhishek/prefinal/piechart;->iCenterWidth:I

    sub-int/2addr v3, v0

    int-to-float v3, v3

    iget v4, p0, Lcom/example/abhishek/prefinal/piechart;->iCenterWidth:I

    add-int/2addr v4, v0

    int-to-float v4, v4

    iget v5, p0, Lcom/example/abhishek/prefinal/piechart;->iCenterWidth:I

    add-int/2addr v5, v0

    int-to-float v5, v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lcom/example/abhishek/prefinal/piechart;->r:Landroid/graphics/RectF;

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/example/abhishek/prefinal/piechart;->centerCircle:Landroid/graphics/RectF;

    if-nez v1, :cond_2

    .line 174
    :cond_2
    iget v1, p0, Lcom/example/abhishek/prefinal/piechart;->iDisplayWidth:I

    iget v2, p0, Lcom/example/abhishek/prefinal/piechart;->iDisplayWidth:I

    invoke-virtual {p0, v1, v2}, Lcom/example/abhishek/prefinal/piechart;->setMeasuredDimension(II)V

    .line 175
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v10, 0x43b40000    # 360.0f

    .line 181
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iget v7, p0, Lcom/example/abhishek/prefinal/piechart;->iCenterWidth:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    float-to-double v6, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    iget v9, p0, Lcom/example/abhishek/prefinal/piechart;->iCenterWidth:I

    int-to-float v9, v9

    sub-float/2addr v8, v9

    float-to-double v8, v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    .line 183
    .local v0, "dx":D
    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    div-double v6, v0, v6

    const-wide v8, 0x4076800000000000L    # 360.0

    mul-double/2addr v6, v8

    double-to-float v2, v6

    .line 184
    .local v2, "fDegree":F
    add-float v6, v2, v10

    rem-float v2, v6, v10

    .line 187
    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v6, v2

    div-float v3, v6, v10

    .line 190
    .local v3, "fSelectedPercent":F
    const/4 v4, 0x0

    .line 191
    .local v4, "fTotalPercent":F
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    iget v6, p0, Lcom/example/abhishek/prefinal/piechart;->iDataSize:I

    if-ge v5, v6, :cond_0

    .line 192
    iget-object v6, p0, Lcom/example/abhishek/prefinal/piechart;->alPercentage:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    add-float/2addr v4, v6

    .line 193
    cmpl-float v6, v4, v3

    if-lez v6, :cond_2

    .line 194
    iput v5, p0, Lcom/example/abhishek/prefinal/piechart;->iSelectedIndex:I

    .line 198
    :cond_0
    iget-object v6, p0, Lcom/example/abhishek/prefinal/piechart;->onSelectedListener:Lcom/example/abhishek/prefinal/piechart$OnSelectedLisenter;

    if-eqz v6, :cond_1

    .line 199
    iget-object v6, p0, Lcom/example/abhishek/prefinal/piechart;->onSelectedListener:Lcom/example/abhishek/prefinal/piechart$OnSelectedLisenter;

    iget v7, p0, Lcom/example/abhishek/prefinal/piechart;->iSelectedIndex:I

    invoke-interface {v6, v7}, Lcom/example/abhishek/prefinal/piechart$OnSelectedLisenter;->onSelected(I)V

    .line 201
    :cond_1
    invoke-virtual {p0}, Lcom/example/abhishek/prefinal/piechart;->invalidate()V

    .line 202
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    return v6

    .line 191
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method public setAdapter(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 215
    .local p1, "alPercentage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    iput-object p1, p0, Lcom/example/abhishek/prefinal/piechart;->alPercentage:Ljava/util/ArrayList;

    .line 216
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    iput v2, p0, Lcom/example/abhishek/prefinal/piechart;->iDataSize:I

    .line 217
    const/4 v0, 0x0

    .line 218
    .local v0, "fSum":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/example/abhishek/prefinal/piechart;->iDataSize:I

    if-ge v1, v2, :cond_0

    .line 219
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    add-float/2addr v0, v2

    .line 218
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 221
    :cond_0
    const/high16 v2, 0x42c80000    # 100.0f

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_1

    .line 222
    sget-object v2, Lcom/example/abhishek/prefinal/piechart;->TAG:Ljava/lang/String;

    const-string v3, "NOT_EQUAL_TO_100"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    const/4 v2, 0x0

    iput v2, p0, Lcom/example/abhishek/prefinal/piechart;->iDataSize:I

    .line 224
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "NOT_EQUAL_TO_100"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2

    .line 227
    :cond_1
    return-void
.end method

.method public setOnSelectedListener(Lcom/example/abhishek/prefinal/piechart$OnSelectedLisenter;)V
    .locals 0
    .param p1, "listener"    # Lcom/example/abhishek/prefinal/piechart$OnSelectedLisenter;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/example/abhishek/prefinal/piechart;->onSelectedListener:Lcom/example/abhishek/prefinal/piechart$OnSelectedLisenter;

    .line 86
    return-void
.end method
