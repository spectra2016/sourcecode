.class public Lorg/apache/xml/security/transforms/implementations/TransformXSLT;
.super Lorg/apache/xml/security/transforms/TransformSpi;


# static fields
.field static b:Lorg/apache/commons/logging/Log;

.field static c:Ljava/lang/Class;

.field static d:Ljava/lang/Class;

.field private static e:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const/4 v0, 0x0

    sput-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->e:Ljava/lang/Class;

    :try_start_3
    const-string v0, "javax.xml.XMLConstants"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->e:Ljava/lang/Class;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_b} :catch_25

    :goto_b
    sget-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->c:Ljava/lang/Class;

    if-nez v0, :cond_22

    const-string v0, "org.apache.xml.security.transforms.implementations.TransformXSLT"

    invoke-static {v0}, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->a(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->c:Ljava/lang/Class;

    :goto_17
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->b:Lorg/apache/commons/logging/Log;

    return-void

    :cond_22
    sget-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->c:Ljava/lang/Class;

    goto :goto_17

    :catch_25
    move-exception v0

    goto :goto_b
.end method

.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Lorg/apache/xml/security/transforms/TransformSpi;-><init>()V

    return-void
.end method

.method static a(Ljava/lang/String;)Ljava/lang/Class;
    .registers 3

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    return-object v0

    :catch_5
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method protected a(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;
    .registers 13

    const/4 v8, 0x1

    const/4 v7, 0x0

    sget-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->e:Ljava/lang/Class;

    if-nez v0, :cond_14

    new-array v0, v8, [Ljava/lang/Object;

    const-string v1, "SECURE_PROCESSING_FEATURE not supported"

    aput-object v1, v0, v7

    new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;

    const-string v2, "generic.EmptyMessage"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v1

    :cond_14
    :try_start_14
    invoke-virtual {p3}, Lorg/apache/xml/security/transforms/Transform;->k()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    const-string v1, "http://www.w3.org/1999/XSL/Transform"

    const-string v2, "stylesheet"

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;I)Lorg/w3c/dom/Element;

    move-result-object v1

    if-nez v1, :cond_4d

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "xslt:stylesheet"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Transform"

    aput-object v2, v0, v1

    new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;

    const-string v2, "xml.WrongContent"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v1
    :try_end_3c
    .catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_14 .. :try_end_3c} :catch_3c
    .catch Ljavax/xml/transform/TransformerConfigurationException; {:try_start_14 .. :try_end_3c} :catch_f9
    .catch Ljavax/xml/transform/TransformerException; {:try_start_14 .. :try_end_3c} :catch_11f
    .catch Ljava/lang/NoSuchMethodException; {:try_start_14 .. :try_end_3c} :catch_130
    .catch Ljava/lang/IllegalAccessException; {:try_start_14 .. :try_end_3c} :catch_141
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_14 .. :try_end_3c} :catch_152

    :catch_3c
    move-exception v0

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/apache/xml/security/exceptions/XMLSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    new-instance v2, Lorg/apache/xml/security/transforms/TransformationException;

    const-string v3, "generic.EmptyMessage"

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v2

    :cond_4d
    :try_start_4d
    invoke-static {}, Ljavax/xml/transform/TransformerFactory;->newInstance()Ljavax/xml/transform/TransformerFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "setFeature"

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->d:Ljava/lang/Class;

    if-nez v0, :cond_d8

    const-string v0, "java.lang.String"

    invoke-static {v0}, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->a(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->d:Ljava/lang/Class;

    :goto_67
    aput-object v0, v5, v6

    const/4 v0, 0x1

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v6, v5, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "http://javax.xml.XMLConstants/feature/secure-processing"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Ljavax/xml/transform/stream/StreamSource;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->e()[B

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v3, v0}, Ljavax/xml/transform/stream/StreamSource;-><init>(Ljava/io/InputStream;)V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v2}, Ljavax/xml/transform/TransformerFactory;->newTransformer()Ljavax/xml/transform/Transformer;

    move-result-object v4

    new-instance v5, Ljavax/xml/transform/dom/DOMSource;

    invoke-direct {v5, v1}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    new-instance v1, Ljavax/xml/transform/stream/StreamResult;

    invoke-direct {v1, v0}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v4, v5, v1}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V

    new-instance v1, Ljavax/xml/transform/stream/StreamSource;

    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v4}, Ljavax/xml/transform/stream/StreamSource;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v2, v1}, Ljavax/xml/transform/TransformerFactory;->newTransformer(Ljavax/xml/transform/Source;)Ljavax/xml/transform/Transformer;
    :try_end_b7
    .catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_4d .. :try_end_b7} :catch_3c
    .catch Ljavax/xml/transform/TransformerConfigurationException; {:try_start_4d .. :try_end_b7} :catch_f9
    .catch Ljavax/xml/transform/TransformerException; {:try_start_4d .. :try_end_b7} :catch_11f
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4d .. :try_end_b7} :catch_130
    .catch Ljava/lang/IllegalAccessException; {:try_start_4d .. :try_end_b7} :catch_141
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4d .. :try_end_b7} :catch_152

    move-result-object v1

    :try_start_b8
    const-string v0, "{http://xml.apache.org/xalan}line-separator"

    const-string v2, "\n"

    invoke-virtual {v1, v0, v2}, Ljavax/xml/transform/Transformer;->setOutputProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_bf
    .catch Ljava/lang/Exception; {:try_start_b8 .. :try_end_bf} :catch_db
    .catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_b8 .. :try_end_bf} :catch_3c
    .catch Ljavax/xml/transform/TransformerConfigurationException; {:try_start_b8 .. :try_end_bf} :catch_f9
    .catch Ljavax/xml/transform/TransformerException; {:try_start_b8 .. :try_end_bf} :catch_11f
    .catch Ljava/lang/NoSuchMethodException; {:try_start_b8 .. :try_end_bf} :catch_130
    .catch Ljava/lang/IllegalAccessException; {:try_start_b8 .. :try_end_bf} :catch_141
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_b8 .. :try_end_bf} :catch_152

    :goto_bf
    if-nez p2, :cond_10a

    :try_start_c1
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v0, Ljavax/xml/transform/stream/StreamResult;

    invoke-direct {v0, v2}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v1, v3, v0}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V

    new-instance v0, Lorg/apache/xml/security/signature/XMLSignatureInput;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/xml/security/signature/XMLSignatureInput;-><init>([B)V

    :goto_d7
    return-object v0

    :cond_d8
    sget-object v0, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->d:Ljava/lang/Class;

    goto :goto_67

    :catch_db
    move-exception v0

    sget-object v2, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->b:Lorg/apache/commons/logging/Log;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Unable to set Xalan line-separator property: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V
    :try_end_f8
    .catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_c1 .. :try_end_f8} :catch_3c
    .catch Ljavax/xml/transform/TransformerConfigurationException; {:try_start_c1 .. :try_end_f8} :catch_f9
    .catch Ljavax/xml/transform/TransformerException; {:try_start_c1 .. :try_end_f8} :catch_11f
    .catch Ljava/lang/NoSuchMethodException; {:try_start_c1 .. :try_end_f8} :catch_130
    .catch Ljava/lang/IllegalAccessException; {:try_start_c1 .. :try_end_f8} :catch_141
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_c1 .. :try_end_f8} :catch_152

    goto :goto_bf

    :catch_f9
    move-exception v0

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljavax/xml/transform/TransformerConfigurationException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    new-instance v2, Lorg/apache/xml/security/transforms/TransformationException;

    const-string v3, "generic.EmptyMessage"

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v2

    :cond_10a
    :try_start_10a
    new-instance v0, Ljavax/xml/transform/stream/StreamResult;

    invoke-direct {v0, p2}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v1, v3, v0}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V

    new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureInput;

    const/4 v0, 0x0

    check-cast v0, [B

    invoke-direct {v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureInput;-><init>([B)V

    invoke-virtual {v1, p2}, Lorg/apache/xml/security/signature/XMLSignatureInput;->b(Ljava/io/OutputStream;)V
    :try_end_11d
    .catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_10a .. :try_end_11d} :catch_3c
    .catch Ljavax/xml/transform/TransformerConfigurationException; {:try_start_10a .. :try_end_11d} :catch_f9
    .catch Ljavax/xml/transform/TransformerException; {:try_start_10a .. :try_end_11d} :catch_11f
    .catch Ljava/lang/NoSuchMethodException; {:try_start_10a .. :try_end_11d} :catch_130
    .catch Ljava/lang/IllegalAccessException; {:try_start_10a .. :try_end_11d} :catch_141
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_10a .. :try_end_11d} :catch_152

    move-object v0, v1

    goto :goto_d7

    :catch_11f
    move-exception v0

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljavax/xml/transform/TransformerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    new-instance v2, Lorg/apache/xml/security/transforms/TransformationException;

    const-string v3, "generic.EmptyMessage"

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v2

    :catch_130
    move-exception v0

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    new-instance v2, Lorg/apache/xml/security/transforms/TransformationException;

    const-string v3, "generic.EmptyMessage"

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v2

    :catch_141
    move-exception v0

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    new-instance v2, Lorg/apache/xml/security/transforms/TransformationException;

    const-string v3, "generic.EmptyMessage"

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v2

    :catch_152
    move-exception v0

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    new-instance v2, Lorg/apache/xml/security/transforms/TransformationException;

    const-string v3, "generic.EmptyMessage"

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V

    throw v2
.end method

.method protected a(Lorg/apache/xml/security/signature/XMLSignatureInput;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;
    .registers 4

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/xml/security/transforms/implementations/TransformXSLT;->a(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;

    move-result-object v0

    return-object v0
.end method
