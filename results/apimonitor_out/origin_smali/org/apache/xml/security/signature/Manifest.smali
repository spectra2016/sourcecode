.class public Lorg/apache/xml/security/signature/Manifest;
.super Lorg/apache/xml/security/utils/SignatureElementProxy;


# static fields
.field static a:Lorg/apache/commons/logging/Log;

.field static f:Ljava/lang/Class;


# instance fields
.field b:Ljava/util/List;

.field c:[Lorg/w3c/dom/Element;

.field d:Ljava/util/HashMap;

.field e:Ljava/util/List;

.field private g:[Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    sget-object v0, Lorg/apache/xml/security/signature/Manifest;->f:Ljava/lang/Class;

    if-nez v0, :cond_17

    const-string v0, "org.apache.xml.security.signature.Manifest"

    invoke-static {v0}, Lorg/apache/xml/security/signature/Manifest;->a(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/signature/Manifest;->f:Ljava/lang/Class;

    :goto_c
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/signature/Manifest;->a:Lorg/apache/commons/logging/Log;

    return-void

    :cond_17
    sget-object v0, Lorg/apache/xml/security/signature/Manifest;->f:Ljava/lang/Class;

    goto :goto_c
.end method

.method public constructor <init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
    .registers 7

    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/utils/SignatureElementProxy;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V

    iput-object v3, p0, Lorg/apache/xml/security/signature/Manifest;->g:[Z

    iput-object v3, p0, Lorg/apache/xml/security/signature/Manifest;->d:Ljava/util/HashMap;

    iput-object v3, p0, Lorg/apache/xml/security/signature/Manifest;->e:Ljava/util/List;

    iget-object v1, p0, Lorg/apache/xml/security/signature/Manifest;->k:Lorg/w3c/dom/Element;

    invoke-interface {v1}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v1

    const-string v2, "Reference"

    invoke-static {v1, v2}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Ljava/lang/String;)[Lorg/w3c/dom/Element;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/xml/security/signature/Manifest;->c:[Lorg/w3c/dom/Element;

    iget-object v1, p0, Lorg/apache/xml/security/signature/Manifest;->c:[Lorg/w3c/dom/Element;

    array-length v1, v1

    if-nez v1, :cond_37

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Reference"

    aput-object v2, v1, v0

    const/4 v0, 0x1

    const-string v2, "Manifest"

    aput-object v2, v1, v0

    new-instance v0, Lorg/w3c/dom/DOMException;

    const/4 v2, 0x4

    const-string v3, "xml.WrongContent"

    invoke-static {v3, v1}, Lorg/apache/xml/security/utils/I18n;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lorg/w3c/dom/DOMException;-><init>(SLjava/lang/String;)V

    throw v0

    :cond_37
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lorg/apache/xml/security/signature/Manifest;->b:Ljava/util/List;

    :goto_3e
    if-ge v0, v1, :cond_48

    iget-object v2, p0, Lorg/apache/xml/security/signature/Manifest;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3e

    :cond_48
    return-void
.end method

.method static a(Ljava/lang/String;)Ljava/lang/Class;
    .registers 3

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    return-object v0

    :catch_5
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method private a(IZ)V
    .registers 4

    iget-object v0, p0, Lorg/apache/xml/security/signature/Manifest;->g:[Z

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lorg/apache/xml/security/signature/Manifest;->a()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lorg/apache/xml/security/signature/Manifest;->g:[Z

    :cond_c
    iget-object v0, p0, Lorg/apache/xml/security/signature/Manifest;->g:[Z

    aput-boolean p2, v0, p1

    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    iget-object v0, p0, Lorg/apache/xml/security/signature/Manifest;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(Z)Z
    .registers 14

    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v1, p0, Lorg/apache/xml/security/signature/Manifest;->c:[Lorg/w3c/dom/Element;

    if-nez v1, :cond_15

    iget-object v1, p0, Lorg/apache/xml/security/signature/Manifest;->k:Lorg/w3c/dom/Element;

    invoke-interface {v1}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v1

    const-string v2, "Reference"

    invoke-static {v1, v2}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Ljava/lang/String;)[Lorg/w3c/dom/Element;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/xml/security/signature/Manifest;->c:[Lorg/w3c/dom/Element;

    :cond_15
    sget-object v1, Lorg/apache/xml/security/signature/Manifest;->a:Lorg/apache/commons/logging/Log;

    invoke-interface {v1}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_60

    sget-object v1, Lorg/apache/xml/security/signature/Manifest;->a:Lorg/apache/commons/logging/Log;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "verify "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/xml/security/signature/Manifest;->c:[Lorg/w3c/dom/Element;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " References"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    sget-object v2, Lorg/apache/xml/security/signature/Manifest;->a:Lorg/apache/commons/logging/Log;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "I am "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    if-eqz p1, :cond_6d

    const-string v1, ""

    :goto_4f
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, " requested to follow nested Manifests"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_60
    iget-object v1, p0, Lorg/apache/xml/security/signature/Manifest;->c:[Lorg/w3c/dom/Element;

    array-length v1, v1

    if-nez v1, :cond_70

    new-instance v1, Lorg/apache/xml/security/exceptions/XMLSecurityException;

    const-string v2, "empty"

    invoke-direct {v1, v2}, Lorg/apache/xml/security/exceptions/XMLSecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6d
    const-string v1, "not"

    goto :goto_4f

    :cond_70
    iget-object v1, p0, Lorg/apache/xml/security/signature/Manifest;->c:[Lorg/w3c/dom/Element;

    array-length v1, v1

    new-array v1, v1, [Z

    iput-object v1, p0, Lorg/apache/xml/security/signature/Manifest;->g:[Z

    move v3, v4

    move v1, v5

    :goto_79
    iget-object v2, p0, Lorg/apache/xml/security/signature/Manifest;->c:[Lorg/w3c/dom/Element;

    array-length v2, v2

    if-ge v3, v2, :cond_174

    new-instance v8, Lorg/apache/xml/security/signature/Reference;

    iget-object v2, p0, Lorg/apache/xml/security/signature/Manifest;->c:[Lorg/w3c/dom/Element;

    aget-object v2, v2, v3

    iget-object v6, p0, Lorg/apache/xml/security/signature/Manifest;->l:Ljava/lang/String;

    invoke-direct {v8, v2, v6, p0}, Lorg/apache/xml/security/signature/Reference;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/signature/Manifest;)V

    iget-object v2, p0, Lorg/apache/xml/security/signature/Manifest;->b:Ljava/util/List;

    invoke-interface {v2, v3, v8}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :try_start_8e
    invoke-virtual {v8}, Lorg/apache/xml/security/signature/Reference;->j()Z

    move-result v2

    invoke-direct {p0, v3, v2}, Lorg/apache/xml/security/signature/Manifest;->a(IZ)V

    if-nez v2, :cond_17c

    move v6, v4

    :goto_98
    sget-object v1, Lorg/apache/xml/security/signature/Manifest;->a:Lorg/apache/commons/logging/Log;

    invoke-interface {v1}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_bc

    sget-object v1, Lorg/apache/xml/security/signature/Manifest;->a:Lorg/apache/commons/logging/Log;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "The Reference has Type "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v8}, Lorg/apache/xml/security/signature/Reference;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V

    :cond_bc
    if-eqz v6, :cond_17a

    if-eqz p1, :cond_17a

    invoke-virtual {v8}, Lorg/apache/xml/security/signature/Reference;->f()Z

    move-result v1

    if-eqz v1, :cond_17a

    sget-object v1, Lorg/apache/xml/security/signature/Manifest;->a:Lorg/apache/commons/logging/Log;

    const-string v2, "We have to follow a nested Manifest"

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
    :try_end_cd
    .catch Lorg/apache/xml/security/signature/ReferenceNotInitializedException; {:try_start_8e .. :try_end_cd} :catch_12b

    const/4 v1, 0x0

    :try_start_ce
    invoke-virtual {v8, v1}, Lorg/apache/xml/security/signature/Reference;->a(Ljava/io/OutputStream;)Lorg/apache/xml/security/signature/XMLSignatureInput;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/xml/security/signature/XMLSignatureInput;->b()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_da
    :goto_da
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_178

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Node;

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v2

    if-ne v2, v5, :cond_da

    move-object v0, v1

    check-cast v0, Lorg/w3c/dom/Element;

    move-object v2, v0

    invoke-interface {v2}, Lorg/w3c/dom/Element;->getNamespaceURI()Ljava/lang/String;

    move-result-object v2

    const-string v11, "http://www.w3.org/2000/09/xmldsig#"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_da

    move-object v0, v1

    check-cast v0, Lorg/w3c/dom/Element;

    move-object v2, v0

    invoke-interface {v2}, Lorg/w3c/dom/Element;->getLocalName()Ljava/lang/String;

    move-result-object v2

    const-string v11, "Manifest"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_109
    .catch Ljava/io/IOException; {:try_start_ce .. :try_end_109} :catch_122
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_ce .. :try_end_109} :catch_162
    .catch Lorg/xml/sax/SAXException; {:try_start_ce .. :try_end_109} :catch_16b
    .catch Lorg/apache/xml/security/signature/ReferenceNotInitializedException; {:try_start_ce .. :try_end_109} :catch_12b

    move-result v2

    if-eqz v2, :cond_da

    :try_start_10c
    new-instance v2, Lorg/apache/xml/security/signature/Manifest;

    check-cast v1, Lorg/w3c/dom/Element;

    invoke-virtual {v9}, Lorg/apache/xml/security/signature/XMLSignatureInput;->k()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v2, v1, v11}, Lorg/apache/xml/security/signature/Manifest;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
    :try_end_117
    .catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_10c .. :try_end_117} :catch_175
    .catch Ljava/io/IOException; {:try_start_10c .. :try_end_117} :catch_122
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_10c .. :try_end_117} :catch_162
    .catch Lorg/xml/sax/SAXException; {:try_start_10c .. :try_end_117} :catch_16b
    .catch Lorg/apache/xml/security/signature/ReferenceNotInitializedException; {:try_start_10c .. :try_end_117} :catch_12b

    move-object v1, v2

    :goto_118
    if-nez v1, :cond_13c

    :try_start_11a
    new-instance v1, Lorg/apache/xml/security/signature/MissingResourceFailureException;

    const-string v2, "empty"

    invoke-direct {v1, v2, v8}, Lorg/apache/xml/security/signature/MissingResourceFailureException;-><init>(Ljava/lang/String;Lorg/apache/xml/security/signature/Reference;)V

    throw v1
    :try_end_122
    .catch Ljava/io/IOException; {:try_start_11a .. :try_end_122} :catch_122
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_11a .. :try_end_122} :catch_162
    .catch Lorg/xml/sax/SAXException; {:try_start_11a .. :try_end_122} :catch_16b
    .catch Lorg/apache/xml/security/signature/ReferenceNotInitializedException; {:try_start_11a .. :try_end_122} :catch_12b

    :catch_122
    move-exception v1

    :try_start_123
    new-instance v2, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;

    const-string v3, "empty"

    invoke-direct {v2, v3, v1}, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
    :try_end_12b
    .catch Lorg/apache/xml/security/signature/ReferenceNotInitializedException; {:try_start_123 .. :try_end_12b} :catch_12b

    :catch_12b
    move-exception v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v8}, Lorg/apache/xml/security/signature/Reference;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    new-instance v3, Lorg/apache/xml/security/signature/MissingResourceFailureException;

    const-string v4, "signature.Verification.Reference.NoInput"

    invoke-direct {v3, v4, v2, v1, v8}, Lorg/apache/xml/security/signature/MissingResourceFailureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;Lorg/apache/xml/security/signature/Reference;)V

    throw v3

    :cond_13c
    :try_start_13c
    iget-object v2, p0, Lorg/apache/xml/security/signature/Manifest;->e:Ljava/util/List;

    iput-object v2, v1, Lorg/apache/xml/security/signature/Manifest;->e:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/xml/security/signature/Manifest;->d:Ljava/util/HashMap;

    iput-object v2, v1, Lorg/apache/xml/security/signature/Manifest;->d:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Lorg/apache/xml/security/signature/Manifest;->a(Z)Z

    move-result v1

    if-nez v1, :cond_159

    sget-object v1, Lorg/apache/xml/security/signature/Manifest;->a:Lorg/apache/commons/logging/Log;

    const-string v2, "The nested Manifest was invalid (bad)"

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V

    move v1, v4

    :goto_152
    move v2, v1

    :goto_153
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v2

    goto/16 :goto_79

    :cond_159
    sget-object v1, Lorg/apache/xml/security/signature/Manifest;->a:Lorg/apache/commons/logging/Log;

    const-string v2, "The nested Manifest was valid (good)"

    invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
    :try_end_160
    .catch Ljava/io/IOException; {:try_start_13c .. :try_end_160} :catch_122
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_13c .. :try_end_160} :catch_162
    .catch Lorg/xml/sax/SAXException; {:try_start_13c .. :try_end_160} :catch_16b
    .catch Lorg/apache/xml/security/signature/ReferenceNotInitializedException; {:try_start_13c .. :try_end_160} :catch_12b

    move v1, v6

    goto :goto_152

    :catch_162
    move-exception v1

    :try_start_163
    new-instance v2, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;

    const-string v3, "empty"

    invoke-direct {v2, v3, v1}, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2

    :catch_16b
    move-exception v1

    new-instance v2, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;

    const-string v3, "empty"

    invoke-direct {v2, v3, v1}, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
    :try_end_174
    .catch Lorg/apache/xml/security/signature/ReferenceNotInitializedException; {:try_start_163 .. :try_end_174} :catch_12b

    :cond_174
    return v1

    :catch_175
    move-exception v1

    goto/16 :goto_da

    :cond_178
    move-object v1, v7

    goto :goto_118

    :cond_17a
    move v2, v6

    goto :goto_153

    :cond_17c
    move v6, v1

    goto/16 :goto_98
.end method

.method public e()Ljava/lang/String;
    .registers 2

    const-string v0, "Manifest"

    return-object v0
.end method
