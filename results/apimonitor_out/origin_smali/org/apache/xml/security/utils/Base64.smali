.class public Lorg/apache/xml/security/utils/Base64;
.super Ljava/lang/Object;


# static fields
.field private static final a:[B

.field private static final b:[C


# direct methods
.method static constructor <clinit>()V
    .registers 9

    const/16 v8, 0x3f

    const/16 v7, 0x3e

    const/16 v6, 0x2f

    const/16 v5, 0x2b

    const/4 v0, 0x0

    const/16 v1, 0xff

    new-array v1, v1, [B

    sput-object v1, Lorg/apache/xml/security/utils/Base64;->a:[B

    const/16 v1, 0x40

    new-array v1, v1, [C

    sput-object v1, Lorg/apache/xml/security/utils/Base64;->b:[C

    move v1, v0

    :goto_16
    const/16 v2, 0xff

    if-ge v1, v2, :cond_22

    sget-object v2, Lorg/apache/xml/security/utils/Base64;->a:[B

    const/4 v3, -0x1

    aput-byte v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_16

    :cond_22
    const/16 v1, 0x5a

    :goto_24
    const/16 v2, 0x41

    if-lt v1, v2, :cond_32

    sget-object v2, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v3, v1, -0x41

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    add-int/lit8 v1, v1, -0x1

    goto :goto_24

    :cond_32
    const/16 v1, 0x7a

    :goto_34
    const/16 v2, 0x61

    if-lt v1, v2, :cond_44

    sget-object v2, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v3, v1, -0x61

    add-int/lit8 v3, v3, 0x1a

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    add-int/lit8 v1, v1, -0x1

    goto :goto_34

    :cond_44
    const/16 v1, 0x39

    :goto_46
    const/16 v2, 0x30

    if-lt v1, v2, :cond_56

    sget-object v2, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v3, v1, -0x30

    add-int/lit8 v3, v3, 0x34

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    add-int/lit8 v1, v1, -0x1

    goto :goto_46

    :cond_56
    sget-object v1, Lorg/apache/xml/security/utils/Base64;->a:[B

    aput-byte v7, v1, v5

    sget-object v1, Lorg/apache/xml/security/utils/Base64;->a:[B

    aput-byte v8, v1, v6

    move v1, v0

    :goto_5f
    const/16 v2, 0x19

    if-gt v1, v2, :cond_6d

    sget-object v2, Lorg/apache/xml/security/utils/Base64;->b:[C

    add-int/lit8 v3, v1, 0x41

    int-to-char v3, v3

    aput-char v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5f

    :cond_6d
    const/16 v1, 0x1a

    move v2, v1

    move v1, v0

    :goto_71
    const/16 v3, 0x33

    if-gt v2, v3, :cond_81

    sget-object v3, Lorg/apache/xml/security/utils/Base64;->b:[C

    add-int/lit8 v4, v1, 0x61

    int-to-char v4, v4

    aput-char v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_71

    :cond_81
    const/16 v1, 0x34

    :goto_83
    const/16 v2, 0x3d

    if-gt v1, v2, :cond_93

    sget-object v2, Lorg/apache/xml/security/utils/Base64;->b:[C

    add-int/lit8 v3, v0, 0x30

    int-to-char v3, v3

    aput-char v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_83

    :cond_93
    sget-object v0, Lorg/apache/xml/security/utils/Base64;->b:[C

    aput-char v5, v0, v7

    sget-object v0, Lorg/apache/xml/security/utils/Base64;->b:[C

    aput-char v6, v0, v8

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static final a(Ljava/lang/String;[B)I
    .registers 7

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_7
    if-ge v2, v3, :cond_1d

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    int-to-byte v4, v0

    invoke-static {v4}, Lorg/apache/xml/security/utils/Base64;->a(B)Z

    move-result v0

    if-nez v0, :cond_1e

    add-int/lit8 v0, v1, 0x1

    aput-byte v4, p1, v1

    :goto_18
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_7

    :cond_1d
    return v1

    :cond_1e
    move v0, v1

    goto :goto_18
.end method

.method public static final a([BI)Ljava/lang/String;
    .registers 20

    const/4 v1, 0x4

    move/from16 v0, p1

    if-ge v0, v1, :cond_8

    const p1, 0x7fffffff

    :cond_8
    if-nez p0, :cond_c

    const/4 v1, 0x0

    :goto_b
    return-object v1

    :cond_c
    move-object/from16 v0, p0

    array-length v1, v0

    mul-int/lit8 v1, v1, 0x8

    if-nez v1, :cond_16

    const-string v1, ""

    goto :goto_b

    :cond_16
    rem-int/lit8 v10, v1, 0x18

    div-int/lit8 v2, v1, 0x18

    if-eqz v10, :cond_95

    add-int/lit8 v1, v2, 0x1

    :goto_1e
    div-int/lit8 v3, p1, 0x4

    add-int/lit8 v4, v1, -0x1

    div-int v11, v4, v3

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v1, v11

    new-array v12, v1, [C

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x0

    move v9, v1

    move v1, v6

    :goto_2f
    if-ge v9, v11, :cond_1af

    const/4 v5, 0x0

    move v6, v1

    move v8, v4

    :goto_34
    const/16 v1, 0x13

    if-ge v5, v1, :cond_ab

    add-int/lit8 v1, v3, 0x1

    aget-byte v3, p0, v3

    add-int/lit8 v4, v1, 0x1

    aget-byte v13, p0, v1

    add-int/lit8 v7, v4, 0x1

    aget-byte v14, p0, v4

    and-int/lit8 v1, v13, 0xf

    int-to-byte v15, v1

    and-int/lit8 v1, v3, 0x3

    int-to-byte v0, v1

    move/from16 v16, v0

    and-int/lit8 v1, v3, -0x80

    if-nez v1, :cond_97

    shr-int/lit8 v1, v3, 0x2

    int-to-byte v1, v1

    move v4, v1

    :goto_54
    and-int/lit8 v1, v13, -0x80

    if-nez v1, :cond_9e

    shr-int/lit8 v1, v13, 0x4

    int-to-byte v1, v1

    move v3, v1

    :goto_5c
    and-int/lit8 v1, v14, -0x80

    if-nez v1, :cond_a5

    shr-int/lit8 v1, v14, 0x6

    int-to-byte v1, v1

    :goto_63
    add-int/lit8 v13, v8, 0x1

    sget-object v17, Lorg/apache/xml/security/utils/Base64;->b:[C

    aget-char v4, v17, v4

    aput-char v4, v12, v8

    add-int/lit8 v4, v13, 0x1

    sget-object v8, Lorg/apache/xml/security/utils/Base64;->b:[C

    shl-int/lit8 v16, v16, 0x4

    or-int v3, v3, v16

    aget-char v3, v8, v3

    aput-char v3, v12, v13

    add-int/lit8 v3, v4, 0x1

    sget-object v8, Lorg/apache/xml/security/utils/Base64;->b:[C

    shl-int/lit8 v13, v15, 0x2

    or-int/2addr v1, v13

    aget-char v1, v8, v1

    aput-char v1, v12, v4

    add-int/lit8 v4, v3, 0x1

    sget-object v1, Lorg/apache/xml/security/utils/Base64;->b:[C

    and-int/lit8 v8, v14, 0x3f

    aget-char v1, v1, v8

    aput-char v1, v12, v3

    add-int/lit8 v3, v6, 0x1

    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v6, v3

    move v8, v4

    move v3, v7

    goto :goto_34

    :cond_95
    move v1, v2

    goto :goto_1e

    :cond_97
    shr-int/lit8 v1, v3, 0x2

    xor-int/lit16 v1, v1, 0xc0

    int-to-byte v1, v1

    move v4, v1

    goto :goto_54

    :cond_9e
    shr-int/lit8 v1, v13, 0x4

    xor-int/lit16 v1, v1, 0xf0

    int-to-byte v1, v1

    move v3, v1

    goto :goto_5c

    :cond_a5
    shr-int/lit8 v1, v14, 0x6

    xor-int/lit16 v1, v1, 0xfc

    int-to-byte v1, v1

    goto :goto_63

    :cond_ab
    add-int/lit8 v4, v8, 0x1

    const/16 v1, 0xa

    aput-char v1, v12, v8

    add-int/lit8 v1, v9, 0x1

    move v9, v1

    move v1, v6

    goto/16 :goto_2f

    :goto_b7
    if-ge v5, v2, :cond_124

    add-int/lit8 v3, v1, 0x1

    aget-byte v1, p0, v1

    add-int/lit8 v4, v3, 0x1

    aget-byte v3, p0, v3

    add-int/lit8 v6, v4, 0x1

    aget-byte v8, p0, v4

    and-int/lit8 v4, v3, 0xf

    int-to-byte v9, v4

    and-int/lit8 v4, v1, 0x3

    int-to-byte v11, v4

    and-int/lit8 v4, v1, -0x80

    if-nez v4, :cond_110

    shr-int/lit8 v1, v1, 0x2

    int-to-byte v1, v1

    move v4, v1

    :goto_d3
    and-int/lit8 v1, v3, -0x80

    if-nez v1, :cond_117

    shr-int/lit8 v1, v3, 0x4

    int-to-byte v1, v1

    move v3, v1

    :goto_db
    and-int/lit8 v1, v8, -0x80

    if-nez v1, :cond_11e

    shr-int/lit8 v1, v8, 0x6

    int-to-byte v1, v1

    :goto_e2
    add-int/lit8 v13, v7, 0x1

    sget-object v14, Lorg/apache/xml/security/utils/Base64;->b:[C

    aget-char v4, v14, v4

    aput-char v4, v12, v7

    add-int/lit8 v4, v13, 0x1

    sget-object v7, Lorg/apache/xml/security/utils/Base64;->b:[C

    shl-int/lit8 v11, v11, 0x4

    or-int/2addr v3, v11

    aget-char v3, v7, v3

    aput-char v3, v12, v13

    add-int/lit8 v7, v4, 0x1

    sget-object v3, Lorg/apache/xml/security/utils/Base64;->b:[C

    shl-int/lit8 v9, v9, 0x2

    or-int/2addr v1, v9

    aget-char v1, v3, v1

    aput-char v1, v12, v4

    add-int/lit8 v3, v7, 0x1

    sget-object v1, Lorg/apache/xml/security/utils/Base64;->b:[C

    and-int/lit8 v4, v8, 0x3f

    aget-char v1, v1, v4

    aput-char v1, v12, v7

    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v7, v3

    move v1, v6

    goto :goto_b7

    :cond_110
    shr-int/lit8 v1, v1, 0x2

    xor-int/lit16 v1, v1, 0xc0

    int-to-byte v1, v1

    move v4, v1

    goto :goto_d3

    :cond_117
    shr-int/lit8 v1, v3, 0x4

    xor-int/lit16 v1, v1, 0xf0

    int-to-byte v1, v1

    move v3, v1

    goto :goto_db

    :cond_11e
    shr-int/lit8 v1, v8, 0x6

    xor-int/lit16 v1, v1, 0xfc

    int-to-byte v1, v1

    goto :goto_e2

    :cond_124
    const/16 v2, 0x8

    if-ne v10, v2, :cond_15f

    aget-byte v1, p0, v1

    and-int/lit8 v2, v1, 0x3

    int-to-byte v2, v2

    and-int/lit8 v3, v1, -0x80

    if-nez v3, :cond_159

    shr-int/lit8 v1, v1, 0x2

    int-to-byte v1, v1

    :goto_134
    add-int/lit8 v3, v7, 0x1

    sget-object v4, Lorg/apache/xml/security/utils/Base64;->b:[C

    aget-char v1, v4, v1

    aput-char v1, v12, v7

    add-int/lit8 v1, v3, 0x1

    sget-object v4, Lorg/apache/xml/security/utils/Base64;->b:[C

    shl-int/lit8 v2, v2, 0x4

    aget-char v2, v4, v2

    aput-char v2, v12, v3

    add-int/lit8 v2, v1, 0x1

    const/16 v3, 0x3d

    aput-char v3, v12, v1

    add-int/lit8 v1, v2, 0x1

    const/16 v1, 0x3d

    aput-char v1, v12, v2

    :cond_152
    :goto_152
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v12}, Ljava/lang/String;-><init>([C)V

    goto/16 :goto_b

    :cond_159
    shr-int/lit8 v1, v1, 0x2

    xor-int/lit16 v1, v1, 0xc0

    int-to-byte v1, v1

    goto :goto_134

    :cond_15f
    const/16 v2, 0x10

    if-ne v10, v2, :cond_152

    aget-byte v2, p0, v1

    add-int/lit8 v1, v1, 0x1

    aget-byte v3, p0, v1

    and-int/lit8 v1, v3, 0xf

    int-to-byte v4, v1

    and-int/lit8 v1, v2, 0x3

    int-to-byte v5, v1

    and-int/lit8 v1, v2, -0x80

    if-nez v1, :cond_1a2

    shr-int/lit8 v1, v2, 0x2

    int-to-byte v1, v1

    move v2, v1

    :goto_177
    and-int/lit8 v1, v3, -0x80

    if-nez v1, :cond_1a9

    shr-int/lit8 v1, v3, 0x4

    int-to-byte v1, v1

    :goto_17e
    add-int/lit8 v3, v7, 0x1

    sget-object v6, Lorg/apache/xml/security/utils/Base64;->b:[C

    aget-char v2, v6, v2

    aput-char v2, v12, v7

    add-int/lit8 v2, v3, 0x1

    sget-object v6, Lorg/apache/xml/security/utils/Base64;->b:[C

    shl-int/lit8 v5, v5, 0x4

    or-int/2addr v1, v5

    aget-char v1, v6, v1

    aput-char v1, v12, v3

    add-int/lit8 v1, v2, 0x1

    sget-object v3, Lorg/apache/xml/security/utils/Base64;->b:[C

    shl-int/lit8 v4, v4, 0x2

    aget-char v3, v3, v4

    aput-char v3, v12, v2

    add-int/lit8 v2, v1, 0x1

    const/16 v2, 0x3d

    aput-char v2, v12, v1

    goto :goto_152

    :cond_1a2
    shr-int/lit8 v1, v2, 0x2

    xor-int/lit16 v1, v1, 0xc0

    int-to-byte v1, v1

    move v2, v1

    goto :goto_177

    :cond_1a9
    shr-int/lit8 v1, v3, 0x4

    xor-int/lit16 v1, v1, 0xf0

    int-to-byte v1, v1

    goto :goto_17e

    :cond_1af
    move v5, v1

    move v7, v4

    move v1, v3

    goto/16 :goto_b7
.end method

.method public static final a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .registers 13

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x3

    const/4 v7, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x4

    new-array v3, v0, [B

    move v0, v1

    :cond_9
    :goto_9
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    if-lez v2, :cond_2b

    int-to-byte v4, v2

    invoke-static {v4}, Lorg/apache/xml/security/utils/Base64;->a(B)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-static {v4}, Lorg/apache/xml/security/utils/Base64;->b(B)Z

    move-result v2

    if-eqz v2, :cond_5f

    add-int/lit8 v2, v0, 0x1

    aput-byte v4, v3, v0

    if-ne v2, v8, :cond_2b

    add-int/lit8 v0, v2, 0x1

    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    int-to-byte v0, v0

    aput-byte v0, v3, v2

    :cond_2b
    aget-byte v0, v3, v1

    aget-byte v1, v3, v9

    aget-byte v2, v3, v10

    aget-byte v3, v3, v8

    sget-object v4, Lorg/apache/xml/security/utils/Base64;->a:[B

    aget-byte v0, v4, v0

    sget-object v4, Lorg/apache/xml/security/utils/Base64;->a:[B

    aget-byte v1, v4, v1

    sget-object v4, Lorg/apache/xml/security/utils/Base64;->a:[B

    aget-byte v4, v4, v2

    sget-object v5, Lorg/apache/xml/security/utils/Base64;->a:[B

    aget-byte v5, v5, v3

    if-eq v4, v7, :cond_47

    if-ne v5, v7, :cond_ef

    :cond_47
    invoke-static {v2}, Lorg/apache/xml/security/utils/Base64;->b(B)Z

    move-result v4

    if-eqz v4, :cond_b4

    invoke-static {v3}, Lorg/apache/xml/security/utils/Base64;->b(B)Z

    move-result v4

    if-eqz v4, :cond_b4

    and-int/lit8 v2, v1, 0xf

    if-eqz v2, :cond_aa

    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5f
    add-int/lit8 v2, v0, 0x1

    aput-byte v4, v3, v0

    if-ne v4, v7, :cond_6d

    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6d
    const/4 v0, 0x4

    if-eq v2, v0, :cond_72

    move v0, v2

    goto :goto_9

    :cond_72
    sget-object v0, Lorg/apache/xml/security/utils/Base64;->a:[B

    aget-byte v2, v3, v1

    aget-byte v0, v0, v2

    sget-object v2, Lorg/apache/xml/security/utils/Base64;->a:[B

    aget-byte v4, v3, v9

    aget-byte v2, v2, v4

    sget-object v4, Lorg/apache/xml/security/utils/Base64;->a:[B

    aget-byte v5, v3, v10

    aget-byte v4, v4, v5

    sget-object v5, Lorg/apache/xml/security/utils/Base64;->a:[B

    aget-byte v6, v3, v8

    aget-byte v5, v5, v6

    shl-int/lit8 v0, v0, 0x2

    shr-int/lit8 v6, v2, 0x4

    or-int/2addr v0, v6

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    and-int/lit8 v0, v2, 0xf

    shl-int/lit8 v0, v0, 0x4

    shr-int/lit8 v2, v4, 0x2

    and-int/lit8 v2, v2, 0xf

    or-int/2addr v0, v2

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    shl-int/lit8 v0, v4, 0x6

    or-int/2addr v0, v5

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    move v0, v1

    goto/16 :goto_9

    :cond_aa
    shl-int/lit8 v0, v0, 0x2

    shr-int/lit8 v1, v1, 0x4

    or-int/2addr v0, v1

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    :goto_b3
    return-void

    :cond_b4
    invoke-static {v2}, Lorg/apache/xml/security/utils/Base64;->b(B)Z

    move-result v4

    if-nez v4, :cond_e7

    invoke-static {v3}, Lorg/apache/xml/security/utils/Base64;->b(B)Z

    move-result v3

    if-eqz v3, :cond_e7

    sget-object v3, Lorg/apache/xml/security/utils/Base64;->a:[B

    aget-byte v2, v3, v2

    and-int/lit8 v3, v2, 0x3

    if-eqz v3, :cond_d0

    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d0
    shl-int/lit8 v0, v0, 0x2

    shr-int/lit8 v3, v1, 0x4

    or-int/2addr v0, v3

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    and-int/lit8 v0, v1, 0xf

    shl-int/lit8 v0, v0, 0x4

    shr-int/lit8 v1, v2, 0x2

    and-int/lit8 v1, v1, 0xf

    or-int/2addr v0, v1

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    goto :goto_b3

    :cond_e7
    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_ef
    shl-int/lit8 v0, v0, 0x2

    shr-int/lit8 v2, v1, 0x4

    or-int/2addr v0, v2

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    and-int/lit8 v0, v1, 0xf

    shl-int/lit8 v0, v0, 0x4

    shr-int/lit8 v1, v4, 0x2

    and-int/lit8 v1, v1, 0xf

    or-int/2addr v0, v1

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    shl-int/lit8 v0, v4, 0x6

    or-int/2addr v0, v5

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    goto :goto_b3
.end method

.method public static final a(Ljava/lang/String;Ljava/io/OutputStream;)V
    .registers 4

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    new-array v0, v0, [B

    invoke-static {p0, v0}, Lorg/apache/xml/security/utils/Base64;->a(Ljava/lang/String;[B)I

    move-result v1

    invoke-static {v0, p1, v1}, Lorg/apache/xml/security/utils/Base64;->a([BLjava/io/OutputStream;I)V

    return-void
.end method

.method public static final a([BLjava/io/OutputStream;)V
    .registers 3

    const/4 v0, -0x1

    invoke-static {p0, p1, v0}, Lorg/apache/xml/security/utils/Base64;->a([BLjava/io/OutputStream;I)V

    return-void
.end method

.method protected static final a([BLjava/io/OutputStream;I)V
    .registers 11

    const/4 v7, -0x1

    if-ne p2, v7, :cond_7

    invoke-static {p0}, Lorg/apache/xml/security/utils/Base64;->c([B)I

    move-result p2

    :cond_7
    rem-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_13

    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.divisible.four"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    div-int/lit8 v1, p2, 0x4

    if-nez v1, :cond_18

    :goto_17
    return-void

    :cond_18
    const/4 v0, 0x0

    add-int/lit8 v1, v1, -0x1

    :goto_1b
    if-lez v1, :cond_6d

    sget-object v2, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v3, v0, 0x1

    aget-byte v0, p0, v0

    aget-byte v2, v2, v0

    sget-object v0, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v4, v3, 0x1

    aget-byte v3, p0, v3

    aget-byte v3, v0, v3

    sget-object v0, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v5, v4, 0x1

    aget-byte v4, p0, v4

    aget-byte v4, v0, v4

    sget-object v6, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v0, v5, 0x1

    aget-byte v5, p0, v5

    aget-byte v5, v6, v5

    if-eq v2, v7, :cond_45

    if-eq v3, v7, :cond_45

    if-eq v4, v7, :cond_45

    if-ne v5, v7, :cond_4d

    :cond_45
    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4d
    shl-int/lit8 v2, v2, 0x2

    shr-int/lit8 v6, v3, 0x4

    or-int/2addr v2, v6

    int-to-byte v2, v2

    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write(I)V

    and-int/lit8 v2, v3, 0xf

    shl-int/lit8 v2, v2, 0x4

    shr-int/lit8 v3, v4, 0x2

    and-int/lit8 v3, v3, 0xf

    or-int/2addr v2, v3

    int-to-byte v2, v2

    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write(I)V

    shl-int/lit8 v2, v4, 0x6

    or-int/2addr v2, v5

    int-to-byte v2, v2

    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write(I)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_1b

    :cond_6d
    sget-object v1, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v2, v0, 0x1

    aget-byte v0, p0, v0

    aget-byte v0, v1, v0

    sget-object v1, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v3, v2, 0x1

    aget-byte v2, p0, v2

    aget-byte v1, v1, v2

    if-eq v0, v7, :cond_81

    if-ne v1, v7, :cond_89

    :cond_81
    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_89
    sget-object v2, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v4, v3, 0x1

    aget-byte v3, p0, v3

    aget-byte v2, v2, v3

    sget-object v5, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v6, v4, 0x1

    aget-byte v4, p0, v4

    aget-byte v5, v5, v4

    if-eq v2, v7, :cond_9d

    if-ne v5, v7, :cond_f8

    :cond_9d
    invoke-static {v3}, Lorg/apache/xml/security/utils/Base64;->b(B)Z

    move-result v5

    if-eqz v5, :cond_c0

    invoke-static {v4}, Lorg/apache/xml/security/utils/Base64;->b(B)Z

    move-result v5

    if-eqz v5, :cond_c0

    and-int/lit8 v2, v1, 0xf

    if-eqz v2, :cond_b5

    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b5
    shl-int/lit8 v0, v0, 0x2

    shr-int/lit8 v1, v1, 0x4

    or-int/2addr v0, v1

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    goto/16 :goto_17

    :cond_c0
    invoke-static {v3}, Lorg/apache/xml/security/utils/Base64;->b(B)Z

    move-result v3

    if-nez v3, :cond_f0

    invoke-static {v4}, Lorg/apache/xml/security/utils/Base64;->b(B)Z

    move-result v3

    if-eqz v3, :cond_f0

    and-int/lit8 v3, v2, 0x3

    if-eqz v3, :cond_d8

    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d8
    shl-int/lit8 v0, v0, 0x2

    shr-int/lit8 v3, v1, 0x4

    or-int/2addr v0, v3

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    and-int/lit8 v0, v1, 0xf

    shl-int/lit8 v0, v0, 0x4

    shr-int/lit8 v1, v2, 0x2

    and-int/lit8 v1, v1, 0xf

    or-int/2addr v0, v1

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    goto/16 :goto_17

    :cond_f0
    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f8
    shl-int/lit8 v0, v0, 0x2

    shr-int/lit8 v3, v1, 0x4

    or-int/2addr v0, v3

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    and-int/lit8 v0, v1, 0xf

    shl-int/lit8 v0, v0, 0x4

    shr-int/lit8 v1, v2, 0x2

    and-int/lit8 v1, v1, 0xf

    or-int/2addr v0, v1

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    shl-int/lit8 v0, v2, 0x6

    or-int/2addr v0, v5

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    goto/16 :goto_17
.end method

.method protected static final a(B)Z
    .registers 2

    const/16 v0, 0x20

    if-eq p0, v0, :cond_10

    const/16 v0, 0xd

    if-eq p0, v0, :cond_10

    const/16 v0, 0xa

    if-eq p0, v0, :cond_10

    const/16 v0, 0x9

    if-ne p0, v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public static final a(Ljava/lang/String;)[B
    .registers 3

    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    new-array v0, v0, [B

    invoke-static {p0, v0}, Lorg/apache/xml/security/utils/Base64;->a(Ljava/lang/String;[B)I

    move-result v1

    invoke-static {v0, v1}, Lorg/apache/xml/security/utils/Base64;->b([BI)[B

    move-result-object v0

    goto :goto_3
.end method

.method public static final a(Lorg/w3c/dom/Element;)[B
    .registers 5

    invoke-interface {p0}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    :goto_9
    if-eqz v1, :cond_21

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1c

    move-object v0, v1

    check-cast v0, Lorg/w3c/dom/Text;

    invoke-interface {v0}, Lorg/w3c/dom/Text;->getData()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1c
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v1

    goto :goto_9

    :cond_21
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/xml/security/utils/Base64;->a(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public static final a([B)[B
    .registers 2

    const/4 v0, -0x1

    invoke-static {p0, v0}, Lorg/apache/xml/security/utils/Base64;->b([BI)[B

    move-result-object v0

    return-object v0
.end method

.method public static final b([B)Ljava/lang/String;
    .registers 2

    invoke-static {}, Lorg/apache/xml/security/utils/XMLUtils;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    const v0, 0x7fffffff

    invoke-static {p0, v0}, Lorg/apache/xml/security/utils/Base64;->a([BI)Ljava/lang/String;

    move-result-object v0

    :goto_d
    return-object v0

    :cond_e
    const/16 v0, 0x4c

    invoke-static {p0, v0}, Lorg/apache/xml/security/utils/Base64;->a([BI)Ljava/lang/String;

    move-result-object v0

    goto :goto_d
.end method

.method protected static final b(B)Z
    .registers 2

    const/16 v0, 0x3d

    if-ne p0, v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected static final b([BI)[B
    .registers 13

    const/4 v1, 0x0

    const/4 v10, -0x1

    if-ne p1, v10, :cond_8

    invoke-static {p0}, Lorg/apache/xml/security/utils/Base64;->c([B)I

    move-result p1

    :cond_8
    rem-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_14

    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.divisible.four"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_14
    div-int/lit8 v2, p1, 0x4

    if-nez v2, :cond_1b

    new-array v0, v1, [B

    :cond_1a
    return-object v0

    :cond_1b
    add-int/lit8 v0, v2, -0x1

    mul-int/lit8 v0, v0, 0x4

    add-int/lit8 v3, v2, -0x1

    mul-int/lit8 v3, v3, 0x3

    sget-object v4, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v5, v0, 0x1

    aget-byte v0, p0, v0

    aget-byte v4, v4, v0

    sget-object v0, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v6, v5, 0x1

    aget-byte v5, p0, v5

    aget-byte v5, v0, v5

    if-eq v4, v10, :cond_37

    if-ne v5, v10, :cond_3f

    :cond_37
    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3f
    sget-object v0, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v7, v6, 0x1

    aget-byte v6, p0, v6

    aget-byte v8, v0, v6

    sget-object v0, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v9, v7, 0x1

    aget-byte v7, p0, v7

    aget-byte v9, v0, v7

    if-eq v8, v10, :cond_53

    if-ne v9, v10, :cond_e8

    :cond_53
    invoke-static {v6}, Lorg/apache/xml/security/utils/Base64;->b(B)Z

    move-result v0

    if-eqz v0, :cond_ad

    invoke-static {v7}, Lorg/apache/xml/security/utils/Base64;->b(B)Z

    move-result v0

    if-eqz v0, :cond_ad

    and-int/lit8 v0, v5, 0xf

    if-eqz v0, :cond_6b

    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6b
    add-int/lit8 v0, v3, 0x1

    new-array v0, v0, [B

    shl-int/lit8 v4, v4, 0x2

    shr-int/lit8 v5, v5, 0x4

    or-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    :goto_77
    add-int/lit8 v2, v2, -0x1

    move v3, v2

    move v2, v1

    :goto_7b
    if-lez v3, :cond_1a

    sget-object v4, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v5, v1, 0x1

    aget-byte v1, p0, v1

    aget-byte v4, v4, v1

    sget-object v1, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v6, v5, 0x1

    aget-byte v5, p0, v5

    aget-byte v5, v1, v5

    sget-object v1, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v7, v6, 0x1

    aget-byte v6, p0, v6

    aget-byte v6, v1, v6

    sget-object v8, Lorg/apache/xml/security/utils/Base64;->a:[B

    add-int/lit8 v1, v7, 0x1

    aget-byte v7, p0, v7

    aget-byte v7, v8, v7

    if-eq v4, v10, :cond_a5

    if-eq v5, v10, :cond_a5

    if-eq v6, v10, :cond_a5

    if-ne v7, v10, :cond_10e

    :cond_a5
    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_ad
    invoke-static {v6}, Lorg/apache/xml/security/utils/Base64;->b(B)Z

    move-result v0

    if-nez v0, :cond_e0

    invoke-static {v7}, Lorg/apache/xml/security/utils/Base64;->b(B)Z

    move-result v0

    if-eqz v0, :cond_e0

    and-int/lit8 v0, v8, 0x3

    if-eqz v0, :cond_c5

    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c5
    add-int/lit8 v0, v3, 0x2

    new-array v0, v0, [B

    add-int/lit8 v6, v3, 0x1

    shl-int/lit8 v4, v4, 0x2

    shr-int/lit8 v7, v5, 0x4

    or-int/2addr v4, v7

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    and-int/lit8 v3, v5, 0xf

    shl-int/lit8 v3, v3, 0x4

    shr-int/lit8 v4, v8, 0x2

    and-int/lit8 v4, v4, 0xf

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v0, v6

    goto :goto_77

    :cond_e0
    new-instance v0, Lorg/apache/xml/security/exceptions/Base64DecodingException;

    const-string v1, "decoding.general"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/exceptions/Base64DecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e8
    add-int/lit8 v0, v3, 0x3

    new-array v0, v0, [B

    add-int/lit8 v6, v3, 0x1

    shl-int/lit8 v4, v4, 0x2

    shr-int/lit8 v7, v5, 0x4

    or-int/2addr v4, v7

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    add-int/lit8 v3, v6, 0x1

    and-int/lit8 v4, v5, 0xf

    shl-int/lit8 v4, v4, 0x4

    shr-int/lit8 v5, v8, 0x2

    and-int/lit8 v5, v5, 0xf

    or-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v0, v6

    add-int/lit8 v4, v3, 0x1

    shl-int/lit8 v4, v8, 0x6

    or-int/2addr v4, v9

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    goto/16 :goto_77

    :cond_10e
    add-int/lit8 v8, v2, 0x1

    shl-int/lit8 v4, v4, 0x2

    shr-int/lit8 v9, v5, 0x4

    or-int/2addr v4, v9

    int-to-byte v4, v4

    aput-byte v4, v0, v2

    add-int/lit8 v4, v8, 0x1

    and-int/lit8 v2, v5, 0xf

    shl-int/lit8 v2, v2, 0x4

    shr-int/lit8 v5, v6, 0x2

    and-int/lit8 v5, v5, 0xf

    or-int/2addr v2, v5

    int-to-byte v2, v2

    aput-byte v2, v0, v8

    add-int/lit8 v2, v4, 0x1

    shl-int/lit8 v5, v6, 0x6

    or-int/2addr v5, v7

    int-to-byte v5, v5

    aput-byte v5, v0, v4

    add-int/lit8 v3, v3, -0x1

    goto/16 :goto_7b
.end method

.method protected static final c([B)I
    .registers 6

    const/4 v1, 0x0

    if-nez p0, :cond_4

    :cond_3
    return v1

    :cond_4
    array-length v3, p0

    move v2, v1

    :goto_6
    if-ge v2, v3, :cond_3

    aget-byte v4, p0, v2

    invoke-static {v4}, Lorg/apache/xml/security/utils/Base64;->a(B)Z

    move-result v0

    if-nez v0, :cond_19

    add-int/lit8 v0, v1, 0x1

    aput-byte v4, p0, v1

    :goto_14
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_6

    :cond_19
    move v0, v1

    goto :goto_14
.end method
