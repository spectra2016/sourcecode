.class Lorg/apache/xml/security/keys/storage/StorageResolver$StorageResolverIterator;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field a:Ljava/util/Iterator;

.field b:Ljava/util/Iterator;


# direct methods
.method private a()Ljava/util/Iterator;
    .registers 3

    :cond_0
    iget-object v0, p0, Lorg/apache/xml/security/keys/storage/StorageResolver$StorageResolverIterator;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lorg/apache/xml/security/keys/storage/StorageResolver$StorageResolverIterator;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/xml/security/keys/storage/StorageResolverSpi;

    invoke-virtual {v0}, Lorg/apache/xml/security/keys/storage/StorageResolverSpi;->a()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1a
    return-object v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method


# virtual methods
.method public hasNext()Z
    .registers 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/xml/security/keys/storage/StorageResolver$StorageResolverIterator;->b:Ljava/util/Iterator;

    if-nez v2, :cond_8

    move v0, v1

    :cond_7
    :goto_7
    return v0

    :cond_8
    iget-object v2, p0, Lorg/apache/xml/security/keys/storage/StorageResolver$StorageResolverIterator;->b:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_7

    invoke-direct {p0}, Lorg/apache/xml/security/keys/storage/StorageResolver$StorageResolverIterator;->a()Ljava/util/Iterator;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/xml/security/keys/storage/StorageResolver$StorageResolverIterator;->b:Ljava/util/Iterator;

    iget-object v2, p0, Lorg/apache/xml/security/keys/storage/StorageResolver$StorageResolverIterator;->b:Ljava/util/Iterator;

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_7
.end method

.method public next()Ljava/lang/Object;
    .registers 2

    invoke-virtual {p0}, Lorg/apache/xml/security/keys/storage/StorageResolver$StorageResolverIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lorg/apache/xml/security/keys/storage/StorageResolver$StorageResolverIterator;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_d
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public remove()V
    .registers 3

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can\'t remove keys from KeyStore"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
