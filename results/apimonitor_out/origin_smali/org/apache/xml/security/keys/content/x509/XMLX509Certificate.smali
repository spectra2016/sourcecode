.class public Lorg/apache/xml/security/keys/content/x509/XMLX509Certificate;
.super Lorg/apache/xml/security/utils/SignatureElementProxy;

# interfaces
.implements Lorg/apache/xml/security/keys/content/x509/XMLX509DataContent;


# virtual methods
.method public a()[B
    .registers 2

    invoke-virtual {p0}, Lorg/apache/xml/security/keys/content/x509/XMLX509Certificate;->n()[B

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .registers 2

    const-string v0, "X509Certificate"

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5

    const/4 v0, 0x0

    instance-of v1, p1, Lorg/apache/xml/security/keys/content/x509/XMLX509Certificate;

    if-nez v1, :cond_6

    :goto_5
    return v0

    :cond_6
    check-cast p1, Lorg/apache/xml/security/keys/content/x509/XMLX509Certificate;

    :try_start_8
    invoke-virtual {p1}, Lorg/apache/xml/security/keys/content/x509/XMLX509Certificate;->a()[B

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/xml/security/keys/content/x509/XMLX509Certificate;->a()[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_13
    .catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_8 .. :try_end_13} :catch_15

    move-result v0

    goto :goto_5

    :catch_15
    move-exception v1

    goto :goto_5
.end method

.method public hashCode()I
    .registers 5

    const/16 v0, 0x11

    :try_start_2
    invoke-virtual {p0}, Lorg/apache/xml/security/keys/content/x509/XMLX509Certificate;->a()[B

    move-result-object v3

    const/4 v1, 0x0

    :goto_7
    array-length v2, v3

    if-ge v1, v2, :cond_15

    mul-int/lit8 v2, v0, 0x1f

    aget-byte v0, v3, v1
    :try_end_e
    .catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_2 .. :try_end_e} :catch_14

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_7

    :catch_14
    move-exception v1

    :cond_15
    return v0
.end method
