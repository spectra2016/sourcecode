.class public abstract Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/String;
.end method

.method protected abstract a(B)V
.end method

.method protected abstract a(Ljava/security/Key;)V
.end method

.method protected a(Lorg/w3c/dom/Element;)V
    .registers 2

    return-void
.end method

.method protected abstract a([B)V
.end method

.method protected abstract a([BII)V
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method protected abstract b([B)Z
.end method

.method public c()V
    .registers 1

    return-void
.end method
