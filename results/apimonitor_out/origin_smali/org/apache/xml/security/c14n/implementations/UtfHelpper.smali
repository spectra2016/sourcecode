.class public Lorg/apache/xml/security/c14n/implementations/UtfHelpper;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static final a(CLjava/io/OutputStream;)V
    .registers 6

    const/16 v1, 0x80

    const/16 v2, 0x3f

    if-ge p0, v1, :cond_a

    invoke-virtual {p1, p0}, Ljava/io/OutputStream;->write(I)V

    :goto_9
    return-void

    :cond_a
    const v0, 0xd800

    if-lt p0, v0, :cond_14

    const v0, 0xdbff

    if-le p0, v0, :cond_1e

    :cond_14
    const v0, 0xdc00

    if-lt p0, v0, :cond_22

    const v0, 0xdfff

    if-gt p0, v0, :cond_22

    :cond_1e
    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write(I)V

    goto :goto_9

    :cond_22
    const/16 v0, 0x7ff

    if-le p0, v0, :cond_47

    ushr-int/lit8 v0, p0, 0xc

    int-to-char v3, v0

    const/16 v0, 0xe0

    if-lez v3, :cond_30

    and-int/lit8 v3, v3, 0xf

    or-int/2addr v0, v3

    :cond_30
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    move v0, v1

    move v1, v2

    :goto_35
    ushr-int/lit8 v2, p0, 0x6

    int-to-char v2, v2

    if-lez v2, :cond_3c

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    :cond_3c
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    and-int/lit8 v0, p0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    goto :goto_9

    :cond_47
    const/16 v0, 0xc0

    const/16 v1, 0x1f

    goto :goto_35
.end method

.method static final a(Ljava/lang/String;Ljava/io/OutputStream;)V
    .registers 10

    const/16 v1, 0x80

    const/16 v3, 0x3f

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v0, 0x0

    :goto_9
    if-ge v0, v5, :cond_5c

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ge v6, v1, :cond_18

    invoke-virtual {p1, v6}, Ljava/io/OutputStream;->write(I)V

    move v0, v4

    goto :goto_9

    :cond_18
    const v0, 0xd800

    if-lt v6, v0, :cond_22

    const v0, 0xdbff

    if-le v6, v0, :cond_2c

    :cond_22
    const v0, 0xdc00

    if-lt v6, v0, :cond_31

    const v0, 0xdfff

    if-gt v6, v0, :cond_31

    :cond_2c
    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    move v0, v4

    goto :goto_9

    :cond_31
    const/16 v0, 0x7ff

    if-le v6, v0, :cond_57

    ushr-int/lit8 v0, v6, 0xc

    int-to-char v2, v0

    const/16 v0, 0xe0

    if-lez v2, :cond_3f

    and-int/lit8 v2, v2, 0xf

    or-int/2addr v0, v2

    :cond_3f
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    move v0, v1

    move v2, v3

    :goto_44
    ushr-int/lit8 v7, v6, 0x6

    int-to-char v7, v7

    if-lez v7, :cond_4b

    and-int/2addr v2, v7

    or-int/2addr v0, v2

    :cond_4b
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    and-int/lit8 v0, v6, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    move v0, v4

    goto :goto_9

    :cond_57
    const/16 v0, 0xc0

    const/16 v2, 0x1f

    goto :goto_44

    :cond_5c
    return-void
.end method

.method static final a(Ljava/lang/String;Ljava/io/OutputStream;Ljava/util/Map;)V
    .registers 4

    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    if-nez v0, :cond_f

    invoke-static {p0}, Lorg/apache/xml/security/c14n/implementations/UtfHelpper;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-interface {p2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_f
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public static final a(Ljava/lang/String;)[B
    .registers 12

    const/16 v2, 0x3f

    const/4 v6, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    new-array v1, v8, [B

    move v3, v6

    move v4, v6

    move v0, v6

    :goto_c
    if-ge v4, v8, :cond_7c

    add-int/lit8 v7, v4, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v4, 0x80

    if-ge v9, v4, :cond_20

    add-int/lit8 v4, v3, 0x1

    int-to-byte v5, v9

    aput-byte v5, v1, v3

    move v3, v4

    move v4, v7

    goto :goto_c

    :cond_20
    const v4, 0xd800

    if-lt v9, v4, :cond_2a

    const v4, 0xdbff

    if-le v9, v4, :cond_34

    :cond_2a
    const v4, 0xdc00

    if-lt v9, v4, :cond_3b

    const v4, 0xdfff

    if-gt v9, v4, :cond_3b

    :cond_34
    add-int/lit8 v4, v3, 0x1

    aput-byte v2, v1, v3

    move v3, v4

    move v4, v7

    goto :goto_c

    :cond_3b
    if-nez v0, :cond_86

    mul-int/lit8 v0, v8, 0x3

    new-array v0, v0, [B

    invoke-static {v1, v6, v0, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v1, 0x1

    move-object v4, v0

    move v5, v1

    :goto_47
    const/16 v0, 0x7ff

    if-le v9, v0, :cond_77

    ushr-int/lit8 v0, v9, 0xc

    int-to-char v1, v0

    const/16 v0, -0x20

    if-lez v1, :cond_56

    and-int/lit8 v1, v1, 0xf

    or-int/2addr v0, v1

    int-to-byte v0, v0

    :cond_56
    add-int/lit8 v1, v3, 0x1

    aput-byte v0, v4, v3

    const/16 v0, -0x80

    move v3, v1

    move v1, v2

    :goto_5e
    ushr-int/lit8 v10, v9, 0x6

    int-to-char v10, v10

    if-lez v10, :cond_66

    and-int/2addr v1, v10

    or-int/2addr v0, v1

    int-to-byte v0, v0

    :cond_66
    add-int/lit8 v1, v3, 0x1

    aput-byte v0, v4, v3

    add-int/lit8 v3, v1, 0x1

    and-int/lit8 v0, v9, 0x3f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    aput-byte v0, v4, v1

    move-object v1, v4

    move v0, v5

    move v4, v7

    goto :goto_c

    :cond_77
    const/16 v0, -0x40

    const/16 v1, 0x1f

    goto :goto_5e

    :cond_7c
    if-eqz v0, :cond_84

    new-array v0, v3, [B

    invoke-static {v1, v6, v0, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_83
    return-object v0

    :cond_84
    move-object v0, v1

    goto :goto_83

    :cond_86
    move-object v4, v1

    move v5, v0

    goto :goto_47
.end method
