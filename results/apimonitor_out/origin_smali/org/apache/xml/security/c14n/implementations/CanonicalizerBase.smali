.class public abstract Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;
.super Lorg/apache/xml/security/c14n/CanonicalizerSpi;


# static fields
.field private static final b:[B

.field private static final c:[B

.field private static final d:[B

.field private static final e:[B

.field private static final f:[B

.field static final g:Lorg/apache/xml/security/c14n/helper/AttrCompare;

.field static final h:[B

.field protected static final i:Lorg/w3c/dom/Attr;

.field private static final o:[B

.field private static final p:[B

.field private static final q:[B

.field private static final r:[B

.field private static final s:[B

.field private static final t:[B

.field private static final u:[B


# instance fields
.field j:Ljava/util/List;

.field k:Z

.field l:Ljava/util/Set;

.field m:Lorg/w3c/dom/Node;

.field n:Ljava/io/OutputStream;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    const/4 v3, 0x4

    const/4 v2, 0x5

    const/4 v1, 0x2

    new-array v0, v1, [B

    fill-array-data v0, :array_a0

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->b:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_a6

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->c:[B

    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_ac

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->d:[B

    new-array v0, v3, [B

    fill-array-data v0, :array_b2

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->e:[B

    new-array v0, v2, [B

    fill-array-data v0, :array_b8

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->f:[B

    new-array v0, v2, [B

    fill-array-data v0, :array_c0

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->o:[B

    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_c8

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->p:[B

    new-array v0, v2, [B

    fill-array-data v0, :array_d0

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->q:[B

    new-array v0, v3, [B

    fill-array-data v0, :array_d8

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->r:[B

    new-array v0, v3, [B

    fill-array-data v0, :array_de

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->s:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_e4

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->t:[B

    new-array v0, v2, [B

    fill-array-data v0, :array_ea

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->u:[B

    new-instance v0, Lorg/apache/xml/security/c14n/helper/AttrCompare;

    invoke-direct {v0}, Lorg/apache/xml/security/c14n/helper/AttrCompare;-><init>()V

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->g:Lorg/apache/xml/security/c14n/helper/AttrCompare;

    new-array v0, v1, [B

    fill-array-data v0, :array_f2

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->h:[B

    :try_start_67
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    const-string v1, "http://www.w3.org/2000/xmlns/"

    const-string v2, "xmlns"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Document;->createAttributeNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Attr;

    move-result-object v0

    sput-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->i:Lorg/w3c/dom/Attr;

    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->i:Lorg/w3c/dom/Attr;

    const-string v1, ""

    invoke-interface {v0, v1}, Lorg/w3c/dom/Attr;->setValue(Ljava/lang/String;)V
    :try_end_84
    .catch Ljava/lang/Exception; {:try_start_67 .. :try_end_84} :catch_85

    return-void

    :catch_85
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Unable to create nullNode"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :array_a0
    .array-data 0x1
        0x3ft
        0x3et
    .end array-data

    nop

    :array_a6
    .array-data 0x1
        0x3ct
        0x3ft
    .end array-data

    nop

    :array_ac
    .array-data 0x1
        0x2dt
        0x2dt
        0x3et
    .end array-data

    :array_b2
    .array-data 0x1
        0x3ct
        0x21t
        0x2dt
        0x2dt
    .end array-data

    :array_b8
    .array-data 0x1
        0x26t
        0x23t
        0x78t
        0x41t
        0x3bt
    .end array-data

    nop

    :array_c0
    .array-data 0x1
        0x26t
        0x23t
        0x78t
        0x39t
        0x3bt
    .end array-data

    nop

    :array_c8
    .array-data 0x1
        0x26t
        0x71t
        0x75t
        0x6ft
        0x74t
        0x3bt
    .end array-data

    nop

    :array_d0
    .array-data 0x1
        0x26t
        0x23t
        0x78t
        0x44t
        0x3bt
    .end array-data

    nop

    :array_d8
    .array-data 0x1
        0x26t
        0x67t
        0x74t
        0x3bt
    .end array-data

    :array_de
    .array-data 0x1
        0x26t
        0x6ct
        0x74t
        0x3bt
    .end array-data

    :array_e4
    .array-data 0x1
        0x3ct
        0x2ft
    .end array-data

    nop

    :array_ea
    .array-data 0x1
        0x26t
        0x61t
        0x6dt
        0x70t
        0x3bt
    .end array-data

    nop

    :array_f2
    .array-data 0x1
        0x3dt
        0x22t
    .end array-data
.end method

.method public constructor <init>(Z)V
    .registers 3

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/xml/security/c14n/CanonicalizerSpi;-><init>()V

    iput-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->l:Ljava/util/Set;

    iput-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->m:Lorg/w3c/dom/Node;

    new-instance v0, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;

    invoke-direct {v0}, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    iput-boolean p1, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->k:Z

    return-void
.end method

.method static final a(Ljava/lang/String;Ljava/io/OutputStream;)V
    .registers 6

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-ge v1, v2, :cond_2d

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    sparse-switch v0, :sswitch_data_2e

    const/16 v3, 0x80

    if-ge v0, v3, :cond_29

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    :goto_16
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :sswitch_1a
    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->u:[B

    :goto_1c
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    goto :goto_16

    :sswitch_20
    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->s:[B

    goto :goto_1c

    :sswitch_23
    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->r:[B

    goto :goto_1c

    :sswitch_26
    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->q:[B

    goto :goto_1c

    :cond_29
    invoke-static {v0, p1}, Lorg/apache/xml/security/c14n/implementations/UtfHelpper;->a(CLjava/io/OutputStream;)V

    goto :goto_16

    :cond_2d
    return-void

    :sswitch_data_2e
    .sparse-switch
        0xd -> :sswitch_26
        0x26 -> :sswitch_1a
        0x3c -> :sswitch_20
        0x3e -> :sswitch_23
    .end sparse-switch
.end method

.method static final a(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Ljava/util/Map;)V
    .registers 8

    const/16 v0, 0x20

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write(I)V

    invoke-static {p0, p2, p3}, Lorg/apache/xml/security/c14n/implementations/UtfHelpper;->a(Ljava/lang/String;Ljava/io/OutputStream;Ljava/util/Map;)V

    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->h:[B

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v0, 0x0

    :goto_12
    if-ge v0, v2, :cond_41

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    sparse-switch v0, :sswitch_data_48

    const/16 v3, 0x80

    if-ge v0, v3, :cond_3c

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write(I)V

    move v0, v1

    goto :goto_12

    :sswitch_26
    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->u:[B

    :goto_28
    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write([B)V

    move v0, v1

    goto :goto_12

    :sswitch_2d
    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->s:[B

    goto :goto_28

    :sswitch_30
    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->p:[B

    goto :goto_28

    :sswitch_33
    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->o:[B

    goto :goto_28

    :sswitch_36
    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->f:[B

    goto :goto_28

    :sswitch_39
    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->q:[B

    goto :goto_28

    :cond_3c
    invoke-static {v0, p2}, Lorg/apache/xml/security/c14n/implementations/UtfHelpper;->a(CLjava/io/OutputStream;)V

    move v0, v1

    goto :goto_12

    :cond_41
    const/16 v0, 0x22

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write(I)V

    return-void

    nop

    :sswitch_data_48
    .sparse-switch
        0x9 -> :sswitch_33
        0xa -> :sswitch_36
        0xd -> :sswitch_39
        0x22 -> :sswitch_30
        0x26 -> :sswitch_26
        0x3c -> :sswitch_2d
    .end sparse-switch
.end method

.method static final a(Lorg/w3c/dom/Comment;Ljava/io/OutputStream;I)V
    .registers 9

    const/16 v5, 0xa

    const/4 v0, 0x1

    if-ne p2, v0, :cond_8

    invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write(I)V

    :cond_8
    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->e:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-interface {p0}, Lorg/w3c/dom/Comment;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v0, 0x0

    :goto_16
    if-ge v0, v2, :cond_34

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0xd

    if-ne v3, v4, :cond_28

    sget-object v3, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->q:[B

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write([B)V

    :goto_25
    add-int/lit8 v0, v0, 0x1

    goto :goto_16

    :cond_28
    const/16 v4, 0x80

    if-ge v3, v4, :cond_30

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    goto :goto_25

    :cond_30
    invoke-static {v3, p1}, Lorg/apache/xml/security/c14n/implementations/UtfHelpper;->a(CLjava/io/OutputStream;)V

    goto :goto_25

    :cond_34
    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->d:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    const/4 v0, -0x1

    if-ne p2, v0, :cond_3f

    invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write(I)V

    :cond_3f
    return-void
.end method

.method static final a(Lorg/w3c/dom/ProcessingInstruction;Ljava/io/OutputStream;I)V
    .registers 11

    const/16 v7, 0xd

    const/16 v6, 0xa

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p2, v1, :cond_b

    invoke-virtual {p1, v6}, Ljava/io/OutputStream;->write(I)V

    :cond_b
    sget-object v1, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->c:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-interface {p0}, Lorg/w3c/dom/ProcessingInstruction;->getTarget()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    move v1, v0

    :goto_19
    if-ge v1, v3, :cond_35

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v4, v7, :cond_29

    sget-object v4, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->q:[B

    invoke-virtual {p1, v4}, Ljava/io/OutputStream;->write([B)V

    :goto_26
    add-int/lit8 v1, v1, 0x1

    goto :goto_19

    :cond_29
    const/16 v5, 0x80

    if-ge v4, v5, :cond_31

    invoke-virtual {p1, v4}, Ljava/io/OutputStream;->write(I)V

    goto :goto_26

    :cond_31
    invoke-static {v4, p1}, Lorg/apache/xml/security/c14n/implementations/UtfHelpper;->a(CLjava/io/OutputStream;)V

    goto :goto_26

    :cond_35
    invoke-interface {p0}, Lorg/w3c/dom/ProcessingInstruction;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_58

    const/16 v3, 0x20

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    :goto_44
    if-ge v0, v2, :cond_58

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v7, :cond_54

    sget-object v3, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->q:[B

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write([B)V

    :goto_51
    add-int/lit8 v0, v0, 0x1

    goto :goto_44

    :cond_54
    invoke-static {v3, p1}, Lorg/apache/xml/security/c14n/implementations/UtfHelpper;->a(CLjava/io/OutputStream;)V

    goto :goto_51

    :cond_58
    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->b:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    const/4 v0, -0x1

    if-ne p2, v0, :cond_63

    invoke-virtual {p1, v6}, Ljava/io/OutputStream;->write(I)V

    :cond_63
    return-void
.end method

.method private d(Lorg/w3c/dom/Node;)[B
    .registers 5

    :try_start_0
    invoke-virtual {p0, p1, p1}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->b(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)V

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    instance-of v0, v0, Ljava/io/ByteArrayOutputStream;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    check-cast v0, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    iget-boolean v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a:Z

    if-eqz v0, :cond_21

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    check-cast v0, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    :cond_21
    move-object v0, v1

    :goto_22
    return-object v0

    :cond_23
    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    instance-of v0, v0, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;

    if-eqz v0, :cond_3e

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    check-cast v0, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;

    invoke-virtual {v0}, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;->a()[B

    move-result-object v1

    iget-boolean v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a:Z

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    check-cast v0, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;

    invoke-virtual {v0}, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;->b()V
    :try_end_3c
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_3c} :catch_40
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_3c} :catch_49

    :cond_3c
    move-object v0, v1

    goto :goto_22

    :cond_3e
    const/4 v0, 0x0

    goto :goto_22

    :catch_40
    move-exception v0

    new-instance v1, Lorg/apache/xml/security/c14n/CanonicalizationException;

    const-string v2, "empty"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :catch_49
    move-exception v0

    new-instance v1, Lorg/apache/xml/security/c14n/CanonicalizationException;

    const-string v2, "empty"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method


# virtual methods
.method a(Lorg/w3c/dom/Node;I)I
    .registers 6

    const/4 v1, 0x1

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->j:Ljava/util/List;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/xml/security/signature/NodeFilter;

    invoke-interface {v0, p1, p2}, Lorg/apache/xml/security/signature/NodeFilter;->a(Lorg/w3c/dom/Node;I)I

    move-result v0

    if-eq v0, v1, :cond_b

    :goto_1d
    return v0

    :cond_1e
    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->l:Ljava/util/Set;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->l:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2c

    const/4 v0, 0x0

    goto :goto_1d

    :cond_2c
    move v0, v1

    goto :goto_1d
.end method

.method abstract a(Lorg/w3c/dom/Element;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;)Ljava/util/Iterator;
.end method

.method public a(Ljava/io/OutputStream;)V
    .registers 2

    iput-object p1, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    return-void
.end method

.method abstract a(Lorg/apache/xml/security/signature/XMLSignatureInput;)V
.end method

.method final a(Lorg/w3c/dom/Node;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;Lorg/w3c/dom/Node;I)V
    .registers 16

    invoke-virtual {p0, p1}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->b(Lorg/w3c/dom/Node;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_8

    :cond_7
    return-void

    :cond_8
    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v4, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    iget-object v5, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->m:Lorg/w3c/dom/Node;

    iget-boolean v6, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->k:Z

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    move-object v3, v1

    move-object v1, v0

    move-object v0, p1

    :goto_18
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v2

    packed-switch v2, :pswitch_data_f2

    :pswitch_1f
    move-object p1, v1

    move-object v1, v3

    :cond_21
    :goto_21
    if-nez p1, :cond_dc

    if-eqz v1, :cond_dc

    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->t:[B

    invoke-virtual {v4, v0}, Ljava/io/OutputStream;->write([B)V

    move-object v0, v1

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-interface {v0}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4, v7}, Lorg/apache/xml/security/c14n/implementations/UtfHelpper;->a(Ljava/lang/String;Ljava/io/OutputStream;Ljava/util/Map;)V

    const/16 v0, 0x3e

    invoke-virtual {v4, v0}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p2}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->b()V

    if-eq v1, p3, :cond_7

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object p1

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v3

    if-eqz v3, :cond_4f

    const/4 v0, 0x1

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v1

    if-eq v0, v1, :cond_ee

    :cond_4f
    const/4 p4, 0x1

    const/4 v3, 0x0

    move-object v1, v3

    goto :goto_21

    :pswitch_53
    new-instance v0, Lorg/apache/xml/security/c14n/CanonicalizationException;

    const-string v1, "empty"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_5b
    invoke-virtual {p2}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->a()V

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v1

    move-object p1, v1

    move-object v1, v3

    goto :goto_21

    :pswitch_65
    if-eqz v6, :cond_ea

    check-cast v0, Lorg/w3c/dom/Comment;

    invoke-static {v0, v4, p4}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Lorg/w3c/dom/Comment;Ljava/io/OutputStream;I)V

    move-object p1, v1

    move-object v1, v3

    goto :goto_21

    :pswitch_6f
    check-cast v0, Lorg/w3c/dom/ProcessingInstruction;

    invoke-static {v0, v4, p4}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Lorg/w3c/dom/ProcessingInstruction;Ljava/io/OutputStream;I)V

    move-object p1, v1

    move-object v1, v3

    goto :goto_21

    :pswitch_77
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Ljava/lang/String;Ljava/io/OutputStream;)V

    move-object p1, v1

    move-object v1, v3

    goto :goto_21

    :pswitch_81
    const/4 p4, 0x0

    if-ne v0, v5, :cond_87

    move-object p1, v1

    move-object v1, v3

    goto :goto_21

    :cond_87
    move-object v1, v0

    check-cast v1, Lorg/w3c/dom/Element;

    invoke-virtual {p2}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->a()V

    const/16 v2, 0x3c

    invoke-virtual {v4, v2}, Ljava/io/OutputStream;->write(I)V

    invoke-interface {v1}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v4, v7}, Lorg/apache/xml/security/c14n/implementations/UtfHelpper;->a(Ljava/lang/String;Ljava/io/OutputStream;Ljava/util/Map;)V

    invoke-virtual {p0, v1, p2}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Lorg/w3c/dom/Element;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;)Ljava/util/Iterator;

    move-result-object v9

    if-eqz v9, :cond_b7

    :goto_9f
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/w3c/dom/Attr;

    invoke-interface {v2}, Lorg/w3c/dom/Attr;->getNodeName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v2}, Lorg/w3c/dom/Attr;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v10, v2, v4, v7}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Ljava/util/Map;)V

    goto :goto_9f

    :cond_b7
    const/16 v2, 0x3e

    invoke-virtual {v4, v2}, Ljava/io/OutputStream;->write(I)V

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object p1

    if-nez p1, :cond_21

    sget-object v1, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->t:[B

    invoke-virtual {v4, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-static {v8, v4}, Lorg/apache/xml/security/c14n/implementations/UtfHelpper;->a(Ljava/lang/String;Ljava/io/OutputStream;)V

    const/16 v1, 0x3e

    invoke-virtual {v4, v1}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p2}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->b()V

    if-eqz v3, :cond_e7

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v1

    move-object p1, v1

    move-object v1, v3

    goto/16 :goto_21

    :cond_dc
    if-eqz p1, :cond_7

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v0

    move-object v3, v1

    move-object v1, v0

    move-object v0, p1

    goto/16 :goto_18

    :cond_e7
    move-object v1, v3

    goto/16 :goto_21

    :cond_ea
    move-object p1, v1

    move-object v1, v3

    goto/16 :goto_21

    :cond_ee
    move-object v1, v3

    goto/16 :goto_21

    nop

    :pswitch_data_f2
    .packed-switch 0x1
        :pswitch_81
        :pswitch_53
        :pswitch_77
        :pswitch_77
        :pswitch_1f
        :pswitch_53
        :pswitch_6f
        :pswitch_65
        :pswitch_5b
        :pswitch_1f
        :pswitch_5b
        :pswitch_53
    .end packed-switch
.end method

.method public a(Ljava/util/Set;)[B
    .registers 3

    iput-object p1, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->l:Ljava/util/Set;

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->l:Ljava/util/Set;

    invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->a(Ljava/util/Set;)Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->d(Lorg/w3c/dom/Node;)[B

    move-result-object v0

    return-object v0
.end method

.method public a(Lorg/w3c/dom/Node;)[B
    .registers 3

    const/4 v0, 0x0

    check-cast v0, Lorg/w3c/dom/Node;

    invoke-virtual {p0, p1, v0}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)[B

    move-result-object v0

    return-object v0
.end method

.method a(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)[B
    .registers 8

    iput-object p2, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->m:Lorg/w3c/dom/Node;

    :try_start_2
    new-instance v2, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;

    invoke-direct {v2}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;-><init>()V

    const/4 v1, -0x1

    if-eqz p1, :cond_19

    const/4 v3, 0x1

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v4

    if-ne v3, v4, :cond_19

    move-object v0, p1

    check-cast v0, Lorg/w3c/dom/Element;

    move-object v1, v0

    invoke-virtual {p0, v1, v2}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->d(Lorg/w3c/dom/Element;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;)V

    const/4 v1, 0x0

    :cond_19
    invoke-virtual {p0, p1, v2, p1, v1}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Lorg/w3c/dom/Node;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;Lorg/w3c/dom/Node;I)V

    iget-object v1, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    iget-object v1, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    instance-of v1, v1, Ljava/io/ByteArrayOutputStream;

    if-eqz v1, :cond_3c

    iget-object v1, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    check-cast v1, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    iget-boolean v1, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a:Z

    if-eqz v1, :cond_3a

    iget-object v1, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    check-cast v1, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    :cond_3a
    move-object v1, v2

    :goto_3b
    return-object v1

    :cond_3c
    iget-object v1, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    instance-of v1, v1, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;

    if-eqz v1, :cond_57

    iget-object v1, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    check-cast v1, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;

    invoke-virtual {v1}, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;->a()[B

    move-result-object v2

    iget-boolean v1, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a:Z

    if-eqz v1, :cond_55

    iget-object v1, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    check-cast v1, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;

    invoke-virtual {v1}, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;->b()V
    :try_end_55
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_55} :catch_59
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_55} :catch_62

    :cond_55
    move-object v1, v2

    goto :goto_3b

    :cond_57
    const/4 v1, 0x0

    goto :goto_3b

    :catch_59
    move-exception v1

    new-instance v2, Lorg/apache/xml/security/c14n/CanonicalizationException;

    const-string v3, "empty"

    invoke-direct {v2, v3, v1}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2

    :catch_62
    move-exception v1

    new-instance v2, Lorg/apache/xml/security/c14n/CanonicalizationException;

    const-string v3, "empty"

    invoke-direct {v2, v3, v1}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
.end method

.method b(Lorg/w3c/dom/Node;)I
    .registers 5

    const/4 v1, 0x1

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->j:Ljava/util/List;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/xml/security/signature/NodeFilter;

    invoke-interface {v0, p1}, Lorg/apache/xml/security/signature/NodeFilter;->a(Lorg/w3c/dom/Node;)I

    move-result v0

    if-eq v0, v1, :cond_b

    :goto_1d
    return v0

    :cond_1e
    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->l:Ljava/util/Set;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->l:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2c

    const/4 v0, 0x0

    goto :goto_1d

    :cond_2c
    move v0, v1

    goto :goto_1d
.end method

.method abstract b(Lorg/w3c/dom/Element;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;)Ljava/util/Iterator;
.end method

.method final b(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)V
    .registers 15

    invoke-virtual {p0, p1}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->b(Lorg/w3c/dom/Node;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_8

    :cond_7
    return-void

    :cond_8
    new-instance v7, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;

    invoke-direct {v7}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;-><init>()V

    if-eqz p1, :cond_1c

    const/4 v0, 0x1

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v1

    if-ne v0, v1, :cond_1c

    move-object v0, p1

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-virtual {p0, v0, v7}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->d(Lorg/w3c/dom/Element;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;)V

    :cond_1c
    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v8, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->n:Ljava/io/OutputStream;

    const/4 v0, -0x1

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    move-object v3, v1

    move v1, v0

    move-object v0, p1

    :goto_29
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v4

    packed-switch v4, :pswitch_data_17e

    :pswitch_30
    move-object p1, v2

    move v2, v1

    move-object v1, v3

    :goto_33
    if-nez p1, :cond_164

    if-eqz v1, :cond_164

    invoke-virtual {p0, v1}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->c(Lorg/w3c/dom/Node;)Z

    move-result v0

    if-eqz v0, :cond_15f

    sget-object v0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->t:[B

    invoke-virtual {v8, v0}, Ljava/io/OutputStream;->write([B)V

    move-object v0, v1

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-interface {v0}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v8, v9}, Lorg/apache/xml/security/c14n/implementations/UtfHelpper;->a(Ljava/lang/String;Ljava/io/OutputStream;Ljava/util/Map;)V

    const/16 v0, 0x3e

    invoke-virtual {v8, v0}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {v7}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->b()V

    :goto_54
    if-eq v1, p2, :cond_7

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object p1

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v3

    if-eqz v3, :cond_67

    const/4 v0, 0x1

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v1

    if-eq v0, v1, :cond_17a

    :cond_67
    const/4 v3, 0x0

    const/4 v1, 0x1

    move v2, v1

    move-object v1, v3

    goto :goto_33

    :pswitch_6c
    new-instance v0, Lorg/apache/xml/security/c14n/CanonicalizationException;

    const-string v1, "empty"

    invoke-direct {v0, v1}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_74
    invoke-virtual {v7}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->a()V

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v2

    move-object p1, v2

    move v2, v1

    move-object v1, v3

    goto :goto_33

    :pswitch_7f
    iget-boolean v4, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->k:Z

    if-eqz v4, :cond_175

    invoke-virtual {v7}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->f()I

    move-result v4

    invoke-virtual {p0, v0, v4}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Lorg/w3c/dom/Node;I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_175

    check-cast v0, Lorg/w3c/dom/Comment;

    invoke-static {v0, v8, v1}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Lorg/w3c/dom/Comment;Ljava/io/OutputStream;I)V

    move-object p1, v2

    move v2, v1

    move-object v1, v3

    goto :goto_33

    :pswitch_97
    invoke-virtual {p0, v0}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->c(Lorg/w3c/dom/Node;)Z

    move-result v4

    if-eqz v4, :cond_175

    check-cast v0, Lorg/w3c/dom/ProcessingInstruction;

    invoke-static {v0, v8, v1}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Lorg/w3c/dom/ProcessingInstruction;Ljava/io/OutputStream;I)V

    move-object p1, v2

    move v2, v1

    move-object v1, v3

    goto :goto_33

    :pswitch_a6
    invoke-virtual {p0, v0}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->c(Lorg/w3c/dom/Node;)Z

    move-result v4

    if-eqz v4, :cond_175

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v8}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Ljava/lang/String;Ljava/io/OutputStream;)V

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v0

    :goto_b7
    if-eqz v0, :cond_175

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_c7

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_175

    :cond_c7
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v8}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Ljava/lang/String;Ljava/io/OutputStream;)V

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v0

    goto :goto_b7

    :pswitch_d7
    const/4 v6, 0x0

    move-object v1, v0

    check-cast v1, Lorg/w3c/dom/Element;

    const/4 v2, 0x0

    invoke-virtual {v7}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->f()I

    move-result v4

    invoke-virtual {p0, v0, v4}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Lorg/w3c/dom/Node;I)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_f0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v2

    move-object v1, v3

    move-object p1, v2

    move v2, v6

    goto/16 :goto_33

    :cond_f0
    const/4 v5, 0x1

    if-ne v4, v5, :cond_125

    const/4 v4, 0x1

    move v5, v4

    :goto_f5
    if-eqz v5, :cond_128

    invoke-virtual {v7}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->a()V

    const/16 v2, 0x3c

    invoke-virtual {v8, v2}, Ljava/io/OutputStream;->write(I)V

    invoke-interface {v1}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v8, v9}, Lorg/apache/xml/security/c14n/implementations/UtfHelpper;->a(Ljava/lang/String;Ljava/io/OutputStream;Ljava/util/Map;)V

    move-object v4, v2

    :goto_107
    invoke-virtual {p0, v1, v7}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->b(Lorg/w3c/dom/Element;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;)Ljava/util/Iterator;

    move-result-object v10

    if-eqz v10, :cond_12d

    :goto_10d
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12d

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/w3c/dom/Attr;

    invoke-interface {v2}, Lorg/w3c/dom/Attr;->getNodeName()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v2}, Lorg/w3c/dom/Attr;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v2, v8, v9}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Ljava/util/Map;)V

    goto :goto_10d

    :cond_125
    const/4 v4, 0x0

    move v5, v4

    goto :goto_f5

    :cond_128
    invoke-virtual {v7}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->c()V

    move-object v4, v2

    goto :goto_107

    :cond_12d
    if-eqz v5, :cond_134

    const/16 v2, 0x3e

    invoke-virtual {v8, v2}, Ljava/io/OutputStream;->write(I)V

    :cond_134
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v2

    if-nez v2, :cond_15b

    if-eqz v5, :cond_157

    sget-object v1, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->t:[B

    invoke-virtual {v8, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-static {v4, v8, v9}, Lorg/apache/xml/security/c14n/implementations/UtfHelpper;->a(Ljava/lang/String;Ljava/io/OutputStream;Ljava/util/Map;)V

    const/16 v1, 0x3e

    invoke-virtual {v8, v1}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {v7}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->b()V

    :goto_14c
    if-eqz v3, :cond_170

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v2

    move-object v1, v3

    move-object p1, v2

    move v2, v6

    goto/16 :goto_33

    :cond_157
    invoke-virtual {v7}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->d()V

    goto :goto_14c

    :cond_15b
    move-object p1, v2

    move v2, v6

    goto/16 :goto_33

    :cond_15f
    invoke-virtual {v7}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->d()V

    goto/16 :goto_54

    :cond_164
    if-eqz p1, :cond_7

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v0

    move-object v3, v1

    move v1, v2

    move-object v2, v0

    move-object v0, p1

    goto/16 :goto_29

    :cond_170
    move-object v1, v3

    move-object p1, v2

    move v2, v6

    goto/16 :goto_33

    :cond_175
    move-object p1, v2

    move v2, v1

    move-object v1, v3

    goto/16 :goto_33

    :cond_17a
    move-object v1, v3

    goto/16 :goto_33

    nop

    :pswitch_data_17e
    .packed-switch 0x1
        :pswitch_d7
        :pswitch_6c
        :pswitch_a6
        :pswitch_a6
        :pswitch_30
        :pswitch_6c
        :pswitch_97
        :pswitch_7f
        :pswitch_74
        :pswitch_30
        :pswitch_74
        :pswitch_6c
    .end packed-switch
.end method

.method public b(Lorg/apache/xml/security/signature/XMLSignatureInput;)[B
    .registers 5

    :try_start_0
    invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->n()Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->k:Z

    :cond_9
    invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->h()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->e()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a([B)[B

    move-result-object v0

    :goto_17
    return-object v0

    :cond_18
    invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->g()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->m()Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->l()Lorg/w3c/dom/Node;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)[B

    move-result-object v0

    goto :goto_17

    :cond_2b
    invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->f()Z

    move-result v0

    if-eqz v0, :cond_52

    invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->p()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->j:Ljava/util/List;

    invoke-virtual {p0, p1}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Lorg/apache/xml/security/signature/XMLSignatureInput;)V

    invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->m()Lorg/w3c/dom/Node;

    move-result-object v0

    if-eqz v0, :cond_49

    invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->m()Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->d(Lorg/w3c/dom/Node;)[B

    move-result-object v0

    goto :goto_17

    :cond_49
    invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->b()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->a(Ljava/util/Set;)[B
    :try_end_50
    .catch Lorg/apache/xml/security/c14n/CanonicalizationException; {:try_start_0 .. :try_end_50} :catch_54
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_50} :catch_5d
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_50} :catch_66
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_50} :catch_6f

    move-result-object v0

    goto :goto_17

    :cond_52
    const/4 v0, 0x0

    goto :goto_17

    :catch_54
    move-exception v0

    new-instance v1, Lorg/apache/xml/security/c14n/CanonicalizationException;

    const-string v2, "empty"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :catch_5d
    move-exception v0

    new-instance v1, Lorg/apache/xml/security/c14n/CanonicalizationException;

    const-string v2, "empty"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :catch_66
    move-exception v0

    new-instance v1, Lorg/apache/xml/security/c14n/CanonicalizationException;

    const-string v2, "empty"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :catch_6f
    move-exception v0

    new-instance v1, Lorg/apache/xml/security/c14n/CanonicalizationException;

    const-string v2, "empty"

    invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method c(Lorg/w3c/dom/Element;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;)V
    .registers 10

    invoke-interface {p1}, Lorg/w3c/dom/Element;->hasAttributes()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    return-void

    :cond_7
    invoke-interface {p1}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_11
    if-ge v1, v3, :cond_6

    invoke-interface {v2, v1}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Attr;

    const-string v4, "http://www.w3.org/2000/xmlns/"

    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getNamespaceURI()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_29

    :cond_25
    :goto_25
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_11

    :cond_29
    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getLocalName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getNodeValue()Ljava/lang/String;

    move-result-object v5

    const-string v6, "xml"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_41

    const-string v6, "http://www.w3.org/XML/1998/namespace"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_25

    :cond_41
    invoke-virtual {p2, v4, v5, v0}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->a(Ljava/lang/String;Ljava/lang/String;Lorg/w3c/dom/Attr;)Z

    goto :goto_25
.end method

.method c(Lorg/w3c/dom/Node;)Z
    .registers 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->j:Ljava/util/List;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/xml/security/signature/NodeFilter;

    invoke-interface {v0, p1}, Lorg/apache/xml/security/signature/NodeFilter;->a(Lorg/w3c/dom/Node;)I

    move-result v0

    if-eq v0, v2, :cond_c

    move v0, v1

    :goto_1f
    return v0

    :cond_20
    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->l:Ljava/util/Set;

    if-eqz v0, :cond_2e

    iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->l:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2e

    move v0, v1

    goto :goto_1f

    :cond_2e
    move v0, v2

    goto :goto_1f
.end method

.method final d(Lorg/w3c/dom/Element;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;)V
    .registers 7

    const/4 v3, 0x1

    new-instance v1, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Lorg/w3c/dom/Element;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v0

    if-eqz v0, :cond_14

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v2

    if-eq v3, v2, :cond_15

    :cond_14
    :goto_14
    return-void

    :cond_15
    check-cast v0, Lorg/w3c/dom/Element;

    :goto_17
    if-eqz v0, :cond_28

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Lorg/w3c/dom/Element;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v0

    if-eqz v0, :cond_28

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v2

    if-eq v3, v2, :cond_40

    :cond_28
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v1

    :goto_30
    invoke-interface {v1}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-interface {v1}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-virtual {p0, v0, p2}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->c(Lorg/w3c/dom/Element;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;)V

    goto :goto_30

    :cond_40
    check-cast v0, Lorg/w3c/dom/Element;

    goto :goto_17

    :cond_43
    const-string v0, "xmlns"

    invoke-virtual {p2, v0}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->b(Ljava/lang/String;)Lorg/w3c/dom/Attr;

    move-result-object v0

    if-eqz v0, :cond_14

    const-string v1, ""

    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const-string v0, "xmlns"

    const-string v1, ""

    sget-object v2, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;->i:Lorg/w3c/dom/Attr;

    invoke-virtual {p2, v0, v1, v2}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->b(Ljava/lang/String;Ljava/lang/String;Lorg/w3c/dom/Attr;)Lorg/w3c/dom/Node;

    goto :goto_14
.end method
