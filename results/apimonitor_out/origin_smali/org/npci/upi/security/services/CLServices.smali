.class public Lorg/npci/upi/security/services/CLServices;
.super Ljava/lang/Object;
.source "CLServices.java"


# static fields
.field private static final GET_CHALLENGE_URI:Landroid/net/Uri; = null

.field private static final GET_CREDENTIAL_URI:Landroid/net/Uri; = null

.field private static final PROVIDER_NAME:Ljava/lang/String; = "org.npci.upi.security.pinactivitycomponent.clservices"

.field private static final REGISTER_APP_URI:Landroid/net/Uri;

.field private static clServices:Lorg/npci/upi/security/services/CLServices;


# instance fields
.field private clRemoteService:Lorg/npci/upi/security/services/a;

.field private mContext:Landroid/content/Context;

.field private notifier:Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;

.field private serviceConnection:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-string v0, "content://org.npci.upi.security.pinactivitycomponent.clservices/getChallenge"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/npci/upi/security/services/CLServices;->GET_CHALLENGE_URI:Landroid/net/Uri;

    const-string v0, "content://org.npci.upi.security.pinactivitycomponent.clservices/registerApp"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/npci/upi/security/services/CLServices;->REGISTER_APP_URI:Landroid/net/Uri;

    const-string v0, "content://org.npci.upi.security.pinactivitycomponent.clservices/getCredential"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lorg/npci/upi/security/services/CLServices;->GET_CREDENTIAL_URI:Landroid/net/Uri;

    const/4 v0, 0x0

    sput-object v0, Lorg/npci/upi/security/services/CLServices;->clServices:Lorg/npci/upi/security/services/CLServices;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;)V
    .registers 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/npci/upi/security/services/CLServices;->clRemoteService:Lorg/npci/upi/security/services/a;

    new-instance v0, Lorg/npci/upi/security/services/CLServices$1;

    invoke-direct {v0, p0}, Lorg/npci/upi/security/services/CLServices$1;-><init>(Lorg/npci/upi/security/services/CLServices;)V

    iput-object v0, p0, Lorg/npci/upi/security/services/CLServices;->serviceConnection:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lorg/npci/upi/security/services/CLServices;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lorg/npci/upi/security/services/CLServices;->notifier:Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "org.npci.upi.security.services.CLRemoteService"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lorg/npci/upi/security/services/CLServices;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lorg/npci/upi/security/services/CLServices;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lorg/npci/upi/security/services/CLServices;->serviceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method static synthetic access$002(Lorg/npci/upi/security/services/CLServices;Lorg/npci/upi/security/services/a;)Lorg/npci/upi/security/services/a;
    .registers 2

    iput-object p1, p0, Lorg/npci/upi/security/services/CLServices;->clRemoteService:Lorg/npci/upi/security/services/a;

    return-object p1
.end method

.method static synthetic access$100()Lorg/npci/upi/security/services/CLServices;
    .registers 1

    sget-object v0, Lorg/npci/upi/security/services/CLServices;->clServices:Lorg/npci/upi/security/services/CLServices;

    return-object v0
.end method

.method static synthetic access$200(Lorg/npci/upi/security/services/CLServices;)Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/services/CLServices;->notifier:Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;

    return-object v0
.end method

.method public static initService(Landroid/content/Context;Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;)V
    .registers 4

    sget-object v0, Lorg/npci/upi/security/services/CLServices;->clServices:Lorg/npci/upi/security/services/CLServices;

    if-eqz v0, :cond_c

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Service already initiated"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    new-instance v0, Lorg/npci/upi/security/services/CLServices;

    invoke-direct {v0, p0, p1}, Lorg/npci/upi/security/services/CLServices;-><init>(Landroid/content/Context;Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;)V

    sput-object v0, Lorg/npci/upi/security/services/CLServices;->clServices:Lorg/npci/upi/security/services/CLServices;

    return-void
.end method


# virtual methods
.method public getChallenge(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6

    const/4 v0, 0x0

    const-class v1, Lorg/npci/upi/security/services/CLServices;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GetChallenge called"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_24

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_24

    if-eqz p2, :cond_24

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_30

    :cond_24
    const-class v1, Lorg/npci/upi/security/services/CLServices;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "In-sufficient arguments provided"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2f
    return-object v0

    :cond_30
    :try_start_30
    iget-object v1, p0, Lorg/npci/upi/security/services/CLServices;->clRemoteService:Lorg/npci/upi/security/services/a;

    invoke-interface {v1, p1, p2}, Lorg/npci/upi/security/services/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_35
    .catch Landroid/os/RemoteException; {:try_start_30 .. :try_end_35} :catch_37

    move-result-object v0

    goto :goto_2f

    :catch_37
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2f
.end method

.method public getCredential(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/npci/upi/security/services/CLRemoteResultReceiver;)V
    .registers 20

    const-class v0, Lorg/npci/upi/security/services/CLServices;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Get Credential called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_b
    iget-object v0, p0, Lorg/npci/upi/security/services/CLServices;->clRemoteService:Lorg/npci/upi/security/services/a;

    invoke-virtual/range {p9 .. p9}, Lorg/npci/upi/security/services/CLRemoteResultReceiver;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lorg/npci/upi/security/services/e;->a(Landroid/os/IBinder;)Lorg/npci/upi/security/services/d;

    move-result-object v9

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-interface/range {v0 .. v9}, Lorg/npci/upi/security/services/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/npci/upi/security/services/d;)V
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_23} :catch_24

    :goto_23
    return-void

    :catch_24
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_23
.end method

.method public registerApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 8

    const/4 v0, 0x0

    const-class v1, Lorg/npci/upi/security/services/CLServices;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Register App called"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_3c

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3c

    if-eqz p2, :cond_3c

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3c

    if-eqz p3, :cond_3c

    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3c

    if-eqz p4, :cond_3c

    invoke-virtual {p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_48

    :cond_3c
    const-class v1, Lorg/npci/upi/security/services/CLServices;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "In-sufficient arguments provided"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_47
    return v0

    :cond_48
    :try_start_48
    iget-object v1, p0, Lorg/npci/upi/security/services/CLServices;->clRemoteService:Lorg/npci/upi/security/services/a;

    invoke-interface {v1, p1, p2, p3, p4}, Lorg/npci/upi/security/services/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_4d
    .catch Landroid/os/RemoteException; {:try_start_48 .. :try_end_4d} :catch_4f

    move-result v0

    goto :goto_47

    :catch_4f
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_47
.end method

.method public unbindService()V
    .registers 3

    iget-object v0, p0, Lorg/npci/upi/security/services/CLServices;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lorg/npci/upi/security/services/CLServices;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    return-void
.end method
