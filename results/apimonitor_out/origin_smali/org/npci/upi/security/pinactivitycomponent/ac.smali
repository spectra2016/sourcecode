.class public Lorg/npci/upi/security/pinactivitycomponent/ac;
.super Ljava/lang/Object;


# instance fields
.field private a:Lin/org/npci/commonlibrary/e;

.field private b:Ljava/lang/String;

.field private c:Lorg/npci/upi/security/pinactivitycomponent/ab;


# direct methods
.method public constructor <init>(Lin/org/npci/commonlibrary/e;Lorg/npci/upi/security/pinactivitycomponent/ab;Ljava/lang/String;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/ac;->a:Lin/org/npci/commonlibrary/e;

    iput-object p3, p0, Lorg/npci/upi/security/pinactivitycomponent/ac;->b:Ljava/lang/String;

    iput-object p2, p0, Lorg/npci/upi/security/pinactivitycomponent/ac;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lin/org/npci/commonlibrary/Message;
    .registers 14

    const/4 v6, 0x0

    :try_start_1
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/ac;->a:Lin/org/npci/commonlibrary/e;

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/ac;->b:Ljava/lang/String;

    move-object v2, p4

    move-object v3, p5

    move-object v4, p1

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lin/org/npci/commonlibrary/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lin/org/npci/commonlibrary/Message;
    :try_end_c
    .catch Lin/org/npci/commonlibrary/f; {:try_start_1 .. :try_end_c} :catch_36

    move-result-object v0

    :try_start_d
    invoke-virtual {v0, p2}, Lin/org/npci/commonlibrary/Message;->setType(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Lin/org/npci/commonlibrary/Message;->setSubType(Ljava/lang/String;)V

    invoke-virtual {v0}, Lin/org/npci/commonlibrary/Message;->getData()Lin/org/npci/commonlibrary/Data;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2.0|"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lin/org/npci/commonlibrary/Message;->getData()Lin/org/npci/commonlibrary/Data;

    move-result-object v3

    invoke-virtual {v3}, Lin/org/npci/commonlibrary/Data;->getEncryptedBase64String()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lin/org/npci/commonlibrary/Data;->setEncryptedBase64String(Ljava/lang/String;)V
    :try_end_35
    .catch Lin/org/npci/commonlibrary/f; {:try_start_d .. :try_end_35} :catch_3d

    :goto_35
    return-object v0

    :catch_36
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    :goto_39
    invoke-virtual {v1}, Lin/org/npci/commonlibrary/f;->printStackTrace()V

    goto :goto_35

    :catch_3d
    move-exception v1

    goto :goto_39
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Lin/org/npci/commonlibrary/Message;
    .registers 14

    const/4 v1, 0x0

    :try_start_1
    const-string v0, "txnId"

    invoke-virtual {p4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "credential"

    invoke-virtual {p4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "appId"

    invoke-virtual {p4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "deviceId"

    invoke-virtual {p4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "mobileNumber"

    invoke-virtual {p4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1e
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1e} :catch_a6

    move-result-object v6

    const-string v7, "DBH in encryptor"

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/ac;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    if-nez v0, :cond_ac

    const-string v0, "null"

    :goto_27
    invoke-static {v7, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/ac;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    invoke-virtual {v0, v2, v3, v6}, Lorg/npci/upi/security/pinactivitycomponent/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "K0 in encryptor"

    invoke-static {v0, v6}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "\\{([^}]*)\\}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_b4

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lorg/npci/upi/security/pinactivitycomponent/ac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lin/org/npci/commonlibrary/Message;

    move-result-object v0

    invoke-virtual {v0}, Lin/org/npci/commonlibrary/Message;->getData()Lin/org/npci/commonlibrary/Data;

    move-result-object v1

    invoke-virtual {v1}, Lin/org/npci/commonlibrary/Data;->getEncryptedBase64String()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\n"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v8, v1}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    :goto_77
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-lez v1, :cond_80

    invoke-virtual {v7, v8}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    :cond_80
    if-eqz v0, :cond_a5

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CommonLibrary"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Encrypted Data: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lin/org/npci/commonlibrary/Message;->getData()Lin/org/npci/commonlibrary/Data;

    move-result-object v2

    invoke-virtual {v2, v1}, Lin/org/npci/commonlibrary/Data;->setEncryptedBase64String(Ljava/lang/String;)V

    :cond_a5
    :goto_a5
    return-object v0

    :catch_a6
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    move-object v0, v1

    goto :goto_a5

    :cond_ac
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/ac;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_27

    :cond_b4
    move-object v0, v1

    goto :goto_77
.end method
