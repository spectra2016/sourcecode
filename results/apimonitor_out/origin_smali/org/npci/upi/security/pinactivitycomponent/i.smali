.class Lorg/npci/upi/security/pinactivitycomponent/i;
.super Ljava/util/TimerTask;


# instance fields
.field a:J

.field final synthetic b:Lorg/npci/upi/security/pinactivitycomponent/q;

.field final synthetic c:I

.field final synthetic d:Lorg/npci/upi/security/pinactivitycomponent/h;


# direct methods
.method constructor <init>(Lorg/npci/upi/security/pinactivitycomponent/h;Lorg/npci/upi/security/pinactivitycomponent/q;I)V
    .registers 8

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/i;->d:Lorg/npci/upi/security/pinactivitycomponent/h;

    iput-object p2, p0, Lorg/npci/upi/security/pinactivitycomponent/i;->b:Lorg/npci/upi/security/pinactivitycomponent/q;

    iput p3, p0, Lorg/npci/upi/security/pinactivitycomponent/i;->c:I

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0xafc8

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lorg/npci/upi/security/pinactivitycomponent/i;->a:J

    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/i;->b:Lorg/npci/upi/security/pinactivitycomponent/q;

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/i;->c:I

    iget-wide v2, p0, Lorg/npci/upi/security/pinactivitycomponent/i;->a:J

    const-wide/16 v4, 0x7d0

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lorg/npci/upi/security/pinactivitycomponent/q;->a(IJ)Lorg/npci/upi/security/pinactivitycomponent/p;

    move-result-object v0

    if-eqz v0, :cond_20

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lorg/npci/upi/security/pinactivitycomponent/j;

    invoke-direct {v2, p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/j;-><init>(Lorg/npci/upi/security/pinactivitycomponent/i;Lorg/npci/upi/security/pinactivitycomponent/p;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_20
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/npci/upi/security/pinactivitycomponent/i;->a:J

    return-void
.end method
