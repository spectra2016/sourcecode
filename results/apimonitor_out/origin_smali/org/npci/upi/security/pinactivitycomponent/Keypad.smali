.class public Lorg/npci/upi/security/pinactivitycomponent/Keypad;
.super Landroid/widget/TableLayout;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:F

.field private e:Lorg/npci/upi/security/pinactivitycomponent/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    invoke-direct {p0, p1, p2}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v0, 0x3d

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->a:I

    invoke-direct {p0, p2}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->a(Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->a()V

    return-void
.end method

.method private a(F)I
    .registers 3

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    div-int/lit16 v0, v0, 0xa0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    return v0
.end method

.method static synthetic a(Lorg/npci/upi/security/pinactivitycomponent/Keypad;)Lorg/npci/upi/security/pinactivitycomponent/f;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->e:Lorg/npci/upi/security/pinactivitycomponent/f;

    return-object v0
.end method

.method private a()V
    .registers 13

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/high16 v9, 0x4040

    const/4 v1, 0x0

    const/4 v4, 0x1

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->b:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->setBackgroundColor(I)V

    new-instance v5, Landroid/widget/TableLayout$LayoutParams;

    const/4 v0, -0x1

    const/high16 v2, 0x3f80

    invoke-direct {v5, v0, v1, v2}, Landroid/widget/TableLayout$LayoutParams;-><init>(IIF)V

    move v3, v1

    move v0, v4

    :goto_15
    if-ge v3, v11, :cond_6e

    new-instance v6, Landroid/widget/TableRow;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v6, v2}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v5}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v6, v9}, Landroid/widget/TableRow;->setWeightSum(F)V

    move v2, v0

    move v0, v1

    :goto_28
    if-ge v0, v11, :cond_66

    new-instance v7, Landroid/widget/TextView;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/16 v8, 0x11

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setGravity(I)V

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getItemParams()Landroid/widget/TableRow$LayoutParams;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v8, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->c:I

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    iget v8, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->d:F

    invoke-virtual {v7, v10, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setClickable(Z)V

    invoke-direct {p0, v7}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->setClickFeedback(Landroid/view/View;)V

    new-instance v8, Lorg/npci/upi/security/pinactivitycomponent/aq;

    invoke-direct {v8, p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/aq;-><init>(Lorg/npci/upi/security/pinactivitycomponent/Keypad;I)V

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v6, v7}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_28

    :cond_66
    invoke-virtual {p0, v6}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_15

    :cond_6e
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$c;->ic_action_backspace:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getItemParams()Landroid/widget/TableRow$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    invoke-direct {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->setClickFeedback(Landroid/view/View;)V

    new-instance v2, Lorg/npci/upi/security/pinactivitycomponent/ar;

    invoke-direct {v2, p0}, Lorg/npci/upi/security/pinactivitycomponent/ar;-><init>(Lorg/npci/upi/security/pinactivitycomponent/Keypad;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getItemParams()Landroid/widget/TableRow$LayoutParams;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->c:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->d:F

    invoke-virtual {v2, v10, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setClickable(Z)V

    invoke-direct {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->setClickFeedback(Landroid/view/View;)V

    new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/c;

    invoke-direct {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/c;-><init>(Lorg/npci/upi/security/pinactivitycomponent/Keypad;)V

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$c;->ic_action_submit:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getItemParams()Landroid/widget/TableRow$LayoutParams;

    move-result-object v3

    iget v6, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->a:I

    int-to-float v6, v6

    invoke-direct {p0, v6}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->a(F)I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x3fa0

    mul-float/2addr v6, v7

    float-to-int v6, v6

    iput v6, v3, Landroid/widget/TableRow$LayoutParams;->height:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    invoke-direct {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->setClickFeedback(Landroid/view/View;)V

    new-instance v3, Lorg/npci/upi/security/pinactivitycomponent/e;

    invoke-direct {v3, p0}, Lorg/npci/upi/security/pinactivitycomponent/e;-><init>(Lorg/npci/upi/security/pinactivitycomponent/Keypad;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v3, Landroid/widget/TableRow;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v5}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v9}, Landroid/widget/TableRow;->setWeightSum(F)V

    invoke-virtual {v3, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    invoke-virtual {v3, v2}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    invoke-virtual {v3, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    invoke-virtual {p0, v3}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->addView(Landroid/view/View;)V

    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .registers 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lorg/npci/upi/security/pinactivitycomponent/a$h;->Keypad:[I

    invoke-virtual {v0, p1, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$h;->Keypad_keypad_bg_color:I

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$b;->npci_keypad_bg_color:I

    invoke-static {v2, v3}, Landroid/support/v4/a/a;->c(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->b:I

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$h;->Keypad_key_digit_color:I

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$b;->npci_key_digit_color:I

    invoke-static {v2, v3}, Landroid/support/v4/a/a;->c(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->c:I

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$h;->Keypad_key_digit_size:I

    const/16 v2, 0x24

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->d:F

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$h;->Keypad_key_digit_height:I

    iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->a:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private getItemParams()Landroid/widget/TableRow$LayoutParams;
    .registers 5

    new-instance v0, Landroid/widget/TableRow$LayoutParams;

    const/4 v1, 0x0

    iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->a:I

    int-to-float v2, v2

    invoke-direct {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->a(F)I

    move-result v2

    const/high16 v3, 0x3f80

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/TableRow$LayoutParams;-><init>(IIF)V

    return-object v0
.end method

.method private setClickFeedback(Landroid/view/View;)V
    .registers 6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$a;->selectableItemBackground:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    return-void
.end method


# virtual methods
.method public setOnKeyPressCallback(Lorg/npci/upi/security/pinactivitycomponent/f;)V
    .registers 2

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->e:Lorg/npci/upi/security/pinactivitycomponent/f;

    return-void
.end method
