.class public Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
.super Landroid/support/v7/a/b;


# static fields
.field private static final m:Ljava/lang/String;


# instance fields
.field private A:Landroid/graphics/drawable/TransitionDrawable;

.field private B:Landroid/widget/ImageView;

.field private C:I

.field private D:Z

.field private E:I

.field private F:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private n:Lorg/json/JSONObject;

.field private o:Lorg/json/JSONObject;

.field private p:Lorg/json/JSONObject;

.field private q:Lorg/json/JSONArray;

.field private r:Lorg/json/JSONArray;

.field private s:Ljava/lang/String;

.field private t:Lorg/npci/upi/security/pinactivitycomponent/an;

.field private u:Lorg/npci/upi/security/pinactivitycomponent/w;

.field private v:Lorg/npci/upi/security/pinactivitycomponent/h;

.field private final w:Landroid/content/Context;

.field private x:Lorg/npci/upi/security/pinactivitycomponent/am;

.field private y:Landroid/view/View;

.field private z:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 4

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/support/v7/a/b;-><init>()V

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->n:Lorg/json/JSONObject;

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o:Lorg/json/JSONObject;

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->p:Lorg/json/JSONObject;

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->q:Lorg/json/JSONArray;

    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->r:Lorg/json/JSONArray;

    const-string v0, "en_US"

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->s:Ljava/lang/String;

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->v:Lorg/npci/upi/security/pinactivitycomponent/h;

    iput-object p0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->w:Landroid/content/Context;

    iput-boolean v2, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->D:Z

    iput v2, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->E:I

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->F:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-void
.end method

.method private a(F)I
    .registers 3

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    div-int/lit16 v0, v0, 0xa0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    return v0
.end method

.method static synthetic a(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)V
    .registers 1

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->x()V

    return-void
.end method

.method static synthetic a(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;Z)Z
    .registers 2

    iput-boolean p1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->D:Z

    return p1
.end method

.method static synthetic b(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Lorg/npci/upi/security/pinactivitycomponent/h;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->v:Lorg/npci/upi/security/pinactivitycomponent/h;

    return-object v0
.end method

.method static synthetic b(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;Z)V
    .registers 2

    invoke-direct {p0, p1}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .registers 7

    const/16 v3, 0x12c

    const/high16 v1, 0x4334

    const/4 v2, 0x0

    if-eqz p1, :cond_4e

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->B:Landroid/widget/ImageView;

    invoke-virtual {p0, v2, v1, v3, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->a(FFILandroid/view/View;)V

    :goto_c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-le v0, v1, :cond_59

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->y:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    if-nez v0, :cond_1c

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->C:I

    :cond_1c
    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->y:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->y:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    if-eqz p1, :cond_54

    move v1, v2

    :goto_2a
    invoke-virtual {v3, v1}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    if-eqz p1, :cond_32

    const/high16 v2, 0x3f80

    :cond_32
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lorg/npci/upi/security/pinactivitycomponent/al;

    invoke-direct {v2, p0, p1, v0}, Lorg/npci/upi/security/pinactivitycomponent/al;-><init>(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;ZI)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    :goto_4d
    return-void

    :cond_4e
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->B:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v2, v3, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->a(FFILandroid/view/View;)V

    goto :goto_c

    :cond_54
    const/high16 v1, -0x4080

    int-to-float v4, v0

    mul-float/2addr v1, v4

    goto :goto_2a

    :cond_59
    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->y:Landroid/view/View;

    if-eqz p1, :cond_62

    const/4 v0, 0x0

    :goto_5e
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4d

    :cond_62
    const/16 v0, 0x8

    goto :goto_5e
.end method

.method static synthetic c(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Z
    .registers 2

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->t()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/graphics/drawable/TransitionDrawable;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->A:Landroid/graphics/drawable/TransitionDrawable;

    return-object v0
.end method

.method static synthetic e(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/view/View;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->y:Landroid/view/View;

    return-object v0
.end method

.method static synthetic f(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/view/View;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->z:Landroid/view/View;

    return-object v0
.end method

.method static synthetic g(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/content/Context;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->w:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic h(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)I
    .registers 2

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->E:I

    return v0
.end method

.method static synthetic p()Ljava/lang/String;
    .registers 1

    sget-object v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->m:Ljava/lang/String;

    return-object v0
.end method

.method private q()Z
    .registers 12

    const/4 v3, 0x2

    const/4 v1, 0x3

    const/4 v6, 0x1

    const/4 v2, 0x0

    new-array v7, v1, [Ljava/lang/String;

    const-string v0, "ATMPIN"

    aput-object v0, v7, v2

    const-string v0, "SMS|EMAIL|HOTP|TOTP"

    aput-object v0, v7, v6

    const-string v0, "MPIN"

    aput-object v0, v7, v3

    new-array v8, v1, [Ljava/lang/String;

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->q:Lorg/json/JSONArray;

    if-eqz v0, :cond_74

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->q:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ne v0, v1, :cond_74

    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    :goto_24
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->q:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_6d

    :try_start_2c
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->q:Lorg/json/JSONArray;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    const-string v9, "subtype"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v1

    aget-object v0, v8, v1

    const/4 v9, 0x0

    aget-object v9, v7, v9

    invoke-virtual {v0, v9}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    move v5, v6

    :cond_4a
    aget-object v0, v8, v1

    const/4 v9, 0x1

    aget-object v9, v7, v9

    invoke-virtual {v0, v9}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_56

    move v4, v6

    :cond_56
    aget-object v0, v8, v1

    const/4 v9, 0x2

    aget-object v9, v7, v9

    invoke-virtual {v0, v9}, Ljava/lang/String;->matches(Ljava/lang/String;)Z
    :try_end_5e
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_5e} :catch_66

    move-result v0

    if-eqz v0, :cond_62

    move v3, v6

    :cond_62
    :goto_62
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_24

    :catch_66
    move-exception v0

    sget-object v9, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->m:Ljava/lang/String;

    invoke-static {v9, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_62

    :cond_6d
    if-eqz v5, :cond_74

    if-eqz v4, :cond_74

    if-eqz v3, :cond_74

    :goto_73
    return v6

    :cond_74
    move v6, v2

    goto :goto_73
.end method

.method private r()V
    .registers 5

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_a4

    :try_start_a
    const-string v1, "configuration"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_19

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->n:Lorg/json/JSONObject;

    :cond_19
    const-string v1, "controls"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3d

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o:Lorg/json/JSONObject;

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o:Lorg/json/JSONObject;

    if-eqz v1, :cond_3d

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o:Lorg/json/JSONObject;

    const-string v2, "CredAllowed"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3d

    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->q:Lorg/json/JSONArray;

    :cond_3d
    const-string v1, "salt"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4c

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->p:Lorg/json/JSONObject;

    :cond_4c
    const-string v1, "payInfo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5b

    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->r:Lorg/json/JSONArray;

    :cond_5b
    const-string v1, "languagePref"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a4

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->s:Ljava/lang/String;

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->s:Ljava/lang/String;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/util/Locale;

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->s:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    array-length v2, v1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_83

    new-instance v0, Ljava/util/Locale;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    const/4 v3, 0x1

    aget-object v1, v1, v3

    invoke-direct {v0, v2, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :cond_83
    invoke-static {v0}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1}, Landroid/content/res/Configuration;-><init>()V

    iput-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V
    :try_end_a4
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a4} :catch_a5

    :cond_a4
    :goto_a4
    return-void

    :catch_a5
    move-exception v0

    sget-object v1, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->m:Ljava/lang/String;

    invoke-static {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_a4
.end method

.method private s()V
    .registers 3

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->fragmentTelKeyboard:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/Keypad;

    if-eqz v0, :cond_12

    new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/ah;

    invoke-direct {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/ah;-><init>(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)V

    invoke-virtual {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/Keypad;->setOnKeyPressCallback(Lorg/npci/upi/security/pinactivitycomponent/f;)V

    :cond_12
    return-void
.end method

.method private t()Z
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->y:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private u()V
    .registers 4

    new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/an;

    invoke-direct {v0}, Lorg/npci/upi/security/pinactivitycomponent/an;-><init>()V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->t:Lorg/npci/upi/security/pinactivitycomponent/an;

    :try_start_7
    new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/w;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->t:Lorg/npci/upi/security/pinactivitycomponent/an;

    invoke-direct {v0, v1, v2, p0}, Lorg/npci/upi/security/pinactivitycomponent/w;-><init>(Landroid/content/Context;Lorg/npci/upi/security/pinactivitycomponent/an;Landroid/app/Activity;)V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->u:Lorg/npci/upi/security/pinactivitycomponent/w;

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->t:Lorg/npci/upi/security/pinactivitycomponent/an;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/an;->a(Landroid/os/Bundle;Landroid/content/Context;)V
    :try_end_21
    .catch Lorg/npci/upi/security/pinactivitycomponent/d; {:try_start_7 .. :try_end_21} :catch_22
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_21} :catch_2d

    :goto_21
    return-void

    :catch_22
    move-exception v0

    sget-object v1, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->m:Ljava/lang/String;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/d;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_21

    :catch_2d
    move-exception v0

    sget-object v1, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->m:Ljava/lang/String;

    const-string v2, "Error parsing and validating arguments to CL"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_21
.end method

.method private v()V
    .registers 3

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    :try_start_5
    const-string v1, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const/16 v1, 0x3e7

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->x:Lorg/npci/upi/security/pinactivitycomponent/am;

    invoke-virtual {p0, v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_14} :catch_15

    :goto_14
    return-void

    :catch_15
    move-exception v0

    sget-object v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->m:Ljava/lang/String;

    const-string v1, "Failed to register SMS broadcast receiver (Ignoring)"

    invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_14
.end method

.method private w()V
    .registers 3

    :try_start_0
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->x:Lorg/npci/upi/security/pinactivitycomponent/am;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->x:Lorg/npci/upi/security/pinactivitycomponent/am;

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->x:Lorg/npci/upi/security/pinactivitycomponent/am;
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_c} :catch_d

    :cond_c
    :goto_c
    return-void

    :catch_d
    move-exception v0

    sget-object v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->m:Ljava/lang/String;

    const-string v1, "Failed to unregister SMS receiver (Ignoring)"

    invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c
.end method

.method private x()V
    .registers 4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "error"

    const-string v2, "USER_ABORTED"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o()Lorg/npci/upi/security/pinactivitycomponent/w;

    move-result-object v1

    invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/w;->d()Landroid/os/ResultReceiver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->finish()V

    return-void
.end method


# virtual methods
.method public a(FFILandroid/view/View;)V
    .registers 12

    const/high16 v4, 0x3f00

    const/4 v3, 0x1

    new-instance v0, Landroid/view/animation/RotateAnimation;

    move v1, p1

    move v2, p2

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    int-to-long v4, p3

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    invoke-virtual {v0, v3}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    invoke-virtual {p4, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public a(Lorg/npci/upi/security/pinactivitycomponent/h;)V
    .registers 2

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->v:Lorg/npci/upi/security/pinactivitycomponent/h;

    return-void
.end method

.method public a(Lorg/npci/upi/security/pinactivitycomponent/h;Landroid/os/Bundle;Z)V
    .registers 6

    :try_start_0
    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->f()Landroid/support/v4/app/p;

    move-result-object v0

    if-eqz p2, :cond_9

    invoke-virtual {p1, p2}, Lorg/npci/upi/security/pinactivitycomponent/h;->g(Landroid/os/Bundle;)V

    :cond_9
    invoke-virtual {v0}, Landroid/support/v4/app/p;->a()Landroid/support/v4/app/r;

    move-result-object v0

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$d;->main_inner_layout:I

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/r;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/r;

    if-eqz p3, :cond_1f

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/r;->a(Ljava/lang/String;)Landroid/support/v4/app/r;

    :cond_1f
    invoke-virtual {v0}, Landroid/support/v4/app/r;->b()I

    invoke-virtual {p0, p1}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->a(Lorg/npci/upi/security/pinactivitycomponent/h;)V
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_25} :catch_26

    :goto_25
    return-void

    :catch_26
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_25
.end method

.method public b(I)V
    .registers 2

    iput p1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->E:I

    return-void
.end method

.method k()V
    .registers 9

    const-string v0, ""

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->n:Lorg/json/JSONObject;

    if-eqz v1, :cond_184

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->n:Lorg/json/JSONObject;

    const-string v1, "payerBankName"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    :goto_f
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->p:Lorg/json/JSONObject;

    if-nez v0, :cond_1d

    new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/d;

    const-string v1, "l12"

    const-string v2, "l12.message"

    invoke-direct {v0, p0, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1c
    return-void

    :cond_1d
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->p:Lorg/json/JSONObject;

    const-string v1, "txnAmount"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v2, ""

    const-string v0, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_181

    const/4 v0, 0x0

    move v1, v0

    :goto_31
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->r:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_181

    :try_start_39
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->r:Lorg/json/JSONArray;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    const-string v3, "name"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "payeeName"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_118

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->r:Lorg/json/JSONArray;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    const-string v3, "value"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_60
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_60} :catch_16e

    move-result-object v0

    move-object v6, v0

    :goto_62
    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->transaction_bar_root:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$d;->tv_acc_or_payee:I

    invoke-virtual {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$d;->transaction_bar_title:I

    invoke-virtual {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$d;->transaction_bar_info:I

    invoke-virtual {p0, v3}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget v4, Lorg/npci/upi/security/pinactivitycomponent/a$d;->transaction_bar_arrow:I

    invoke-virtual {p0, v4}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->B:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v2, ""

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9a

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_9a
    const-string v1, ""

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\u20b9 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_b8
    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xd

    if-lt v3, v4, :cond_179

    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v1, v2, Landroid/graphics/Point;->y:I

    iput v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->C:I

    :goto_d2
    new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/ai;

    invoke-direct {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/ai;-><init>(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->transaction_details_scroller:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->y:Landroid/view/View;

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->transaction_details_expanded_space:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->z:Landroid/view/View;

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->y:Landroid/view/View;

    new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/aj;

    invoke-direct {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/aj;-><init>(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->z:Landroid/view/View;

    if-eqz v0, :cond_102

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->z:Landroid/view/View;

    new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/ak;

    invoke-direct {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/ak;-><init>(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_102
    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->transaction_info_root:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->A:Landroid/graphics/drawable/TransitionDrawable;

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->A:Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    goto/16 :goto_1c

    :cond_118
    :try_start_118
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->r:Lorg/json/JSONArray;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    const-string v3, "name"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "account"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_143

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->r:Lorg/json/JSONArray;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    const-string v3, "value"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    goto/16 :goto_62

    :cond_143
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->r:Lorg/json/JSONArray;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    const-string v3, "name"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "mobileNumber"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_174

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->r:Lorg/json/JSONArray;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    const-string v3, "payinfo.mobilenumber.label"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_16a
    .catch Ljava/lang/Exception; {:try_start_118 .. :try_end_16a} :catch_16e

    move-result-object v0

    move-object v6, v0

    goto/16 :goto_62

    :catch_16e
    move-exception v0

    sget-object v3, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->m:Ljava/lang/String;

    invoke-static {v3, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_174
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_31

    :cond_179
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    iput v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->C:I

    goto/16 :goto_d2

    :cond_181
    move-object v6, v2

    goto/16 :goto_62

    :cond_184
    move-object v5, v0

    goto/16 :goto_f
.end method

.method l()V
    .registers 9

    const/4 v5, 0x0

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->transaction_details_root:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    move v4, v5

    :goto_a
    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->r:Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v4, v1, :cond_51

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$e;->layout_transaction_details_item:I

    invoke-virtual {v1, v2, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$d;->transaction_details_item_name:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$d;->transaction_details_item_value:I

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v6, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->r:Lorg/json/JSONArray;

    invoke-virtual {v6, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "name"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v2, "value"

    invoke-virtual {v6, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_a

    :cond_51
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/high16 v4, 0x4040

    invoke-direct {p0, v4}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->a(F)I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/high16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    const v2, 0x3ea8f5c3

    invoke-static {v1, v2}, Landroid/support/v4/f/af;->b(Landroid/view/View;F)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public m()Z
    .registers 2

    const-string v0, "android.permission.RECEIVE_SMS"

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public n()Z
    .registers 2

    const-string v0, "android.permission.READ_SMS"

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public o()Lorg/npci/upi/security/pinactivitycomponent/w;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->u:Lorg/npci/upi/security/pinactivitycomponent/w;

    return-object v0
.end method

.method public onBackPressed()V
    .registers 5

    const/4 v3, 0x0

    iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->D:Z

    if-eqz v0, :cond_20

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "error"

    const-string v2, "USER_ABORTED"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o()Lorg/npci/upi/security/pinactivitycomponent/w;

    move-result-object v1

    invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/w;->d()Landroid/os/ResultReceiver;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    invoke-super {p0}, Landroid/support/v7/a/b;->onBackPressed()V

    :goto_1f
    return-void

    :cond_20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->D:Z

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$f;->back_button_exit_message:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/ag;

    invoke-direct {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/ag;-><init>(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1f
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2

    invoke-super {p0, p1}, Landroid/support/v7/a/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/support/v7/a/b;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->F:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/ad;

    invoke-direct {v1}, Lorg/npci/upi/security/pinactivitycomponent/ad;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->r()V

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$e;->activity_pin_activity_component:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->setContentView(I)V

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->s()V

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->u()V

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->k()V

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->l()V

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->q()Z

    move-result v0

    if-eqz v0, :cond_57

    new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/b;

    invoke-direct {v0}, Lorg/npci/upi/security/pinactivitycomponent/b;-><init>()V

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->a(Lorg/npci/upi/security/pinactivitycomponent/h;Landroid/os/Bundle;Z)V

    :goto_44
    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->go_back:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_56

    new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/af;

    invoke-direct {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/af;-><init>(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_56
    return-void

    :cond_57
    new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/r;

    invoke-direct {v0}, Lorg/npci/upi/security/pinactivitycomponent/r;-><init>()V

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->a(Lorg/npci/upi/security/pinactivitycomponent/h;Landroid/os/Bundle;Z)V

    goto :goto_44
.end method

.method protected onDestroy()V
    .registers 3

    invoke-super {p0}, Landroid/support/v7/a/b;->onDestroy()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->F:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    return-void
.end method

.method protected onPause()V
    .registers 1

    invoke-super {p0}, Landroid/support/v7/a/b;->onPause()V

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->w()V

    return-void
.end method

.method protected onResume()V
    .registers 3

    invoke-super {p0}, Landroid/support/v7/a/b;->onResume()V

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->m()Z

    move-result v0

    if-eqz v0, :cond_15

    new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/am;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/am;-><init>(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;Lorg/npci/upi/security/pinactivitycomponent/af;)V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->x:Lorg/npci/upi/security/pinactivitycomponent/am;

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->v()V

    :goto_14
    return-void

    :cond_15
    sget-object v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->m:Ljava/lang/String;

    const-string v1, "RECEIVE_SMS permission not provided by the App. This will affect Auto OTP detection feature of Common Library"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_14
.end method
