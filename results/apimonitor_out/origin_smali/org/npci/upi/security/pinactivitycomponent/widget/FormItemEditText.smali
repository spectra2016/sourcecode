.class public Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;
.super Landroid/widget/EditText;


# instance fields
.field private A:[[I

.field private B:[I

.field private C:Landroid/content/res/ColorStateList;

.field private a:Ljava/lang/String;

.field private b:Ljava/lang/StringBuilder;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:I

.field private j:[Landroid/graphics/RectF;

.field private k:[F

.field private l:Landroid/graphics/Paint;

.field private m:Landroid/graphics/Paint;

.field private n:Landroid/graphics/Paint;

.field private o:Landroid/graphics/drawable/Drawable;

.field private p:Landroid/graphics/Rect;

.field private q:Z

.field private r:Landroid/view/View$OnClickListener;

.field private s:Lorg/npci/upi/security/pinactivitycomponent/widget/k;

.field private t:Z

.field private u:F

.field private v:F

.field private w:Landroid/graphics/Paint;

.field private x:Z

.field private y:Z

.field private z:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 9

    const/4 v6, 0x4

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a:Ljava/lang/String;

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->c:Ljava/lang/String;

    iput v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->d:I

    const/high16 v0, 0x41c0

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->e:F

    const/high16 v0, 0x4080

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->g:F

    const/high16 v0, 0x4100

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->h:F

    iput v6, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->i:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->p:Landroid/graphics/Rect;

    iput-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->q:Z

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->s:Lorg/npci/upi/security/pinactivitycomponent/widget/k;

    const/high16 v0, 0x3f80

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->u:F

    const/high16 v0, 0x4000

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->v:F

    iput-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->x:Z

    iput-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->y:Z

    new-array v0, v6, [[I

    new-array v1, v5, [I

    const v2, 0x10100a1

    aput v2, v1, v4

    aput-object v1, v0, v4

    new-array v1, v5, [I

    const v2, 0x10100a2

    aput v2, v1, v4

    aput-object v1, v0, v5

    const/4 v1, 0x2

    new-array v2, v5, [I

    const v3, 0x101009c

    aput v3, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-array v2, v5, [I

    const v3, -0x101009c

    aput v3, v2, v4

    aput-object v2, v0, v1

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->A:[[I

    new-array v0, v6, [I

    fill-array-data v0, :array_72

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->B:[I

    new-instance v0, Landroid/content/res/ColorStateList;

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->A:[[I

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->B:[I

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->C:Landroid/content/res/ColorStateList;

    return-void

    nop

    :array_72
    .array-data 0x4
        0x0t 0xfft 0x0t 0xfft
        0x0t 0x0t 0xfft 0xfft
        0x0t 0x0t 0x0t 0xfft
        0x88t 0x88t 0x88t 0xfft
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 10

    const/4 v6, 0x4

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a:Ljava/lang/String;

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->c:Ljava/lang/String;

    iput v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->d:I

    const/high16 v0, 0x41c0

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->e:F

    const/high16 v0, 0x4080

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->g:F

    const/high16 v0, 0x4100

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->h:F

    iput v6, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->i:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->p:Landroid/graphics/Rect;

    iput-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->q:Z

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->s:Lorg/npci/upi/security/pinactivitycomponent/widget/k;

    const/high16 v0, 0x3f80

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->u:F

    const/high16 v0, 0x4000

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->v:F

    iput-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->x:Z

    iput-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->y:Z

    new-array v0, v6, [[I

    new-array v1, v5, [I

    const v2, 0x10100a1

    aput v2, v1, v4

    aput-object v1, v0, v4

    new-array v1, v5, [I

    const v2, 0x10100a2

    aput v2, v1, v4

    aput-object v1, v0, v5

    const/4 v1, 0x2

    new-array v2, v5, [I

    const v3, 0x101009c

    aput v3, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-array v2, v5, [I

    const v3, -0x101009c

    aput v3, v2, v4

    aput-object v2, v0, v1

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->A:[[I

    new-array v0, v6, [I

    fill-array-data v0, :array_74

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->B:[I

    new-instance v0, Landroid/content/res/ColorStateList;

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->A:[[I

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->B:[I

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->C:Landroid/content/res/ColorStateList;

    invoke-direct {p0, p1, p2}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void

    :array_74
    .array-data 0x4
        0x0t 0xfft 0x0t 0xfft
        0x0t 0x0t 0xfft 0xfft
        0x0t 0x0t 0x0t 0xfft
        0x88t 0x88t 0x88t 0xfft
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 11

    const/4 v6, 0x4

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a:Ljava/lang/String;

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->c:Ljava/lang/String;

    iput v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->d:I

    const/high16 v0, 0x41c0

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->e:F

    const/high16 v0, 0x4080

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->g:F

    const/high16 v0, 0x4100

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->h:F

    iput v6, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->i:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->p:Landroid/graphics/Rect;

    iput-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->q:Z

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->s:Lorg/npci/upi/security/pinactivitycomponent/widget/k;

    const/high16 v0, 0x3f80

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->u:F

    const/high16 v0, 0x4000

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->v:F

    iput-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->x:Z

    iput-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->y:Z

    new-array v0, v6, [[I

    new-array v1, v5, [I

    const v2, 0x10100a1

    aput v2, v1, v4

    aput-object v1, v0, v4

    new-array v1, v5, [I

    const v2, 0x10100a2

    aput v2, v1, v4

    aput-object v1, v0, v5

    const/4 v1, 0x2

    new-array v2, v5, [I

    const v3, 0x101009c

    aput v3, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-array v2, v5, [I

    const v3, -0x101009c

    aput v3, v2, v4

    aput-object v2, v0, v1

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->A:[[I

    new-array v0, v6, [I

    fill-array-data v0, :array_74

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->B:[I

    new-instance v0, Landroid/content/res/ColorStateList;

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->A:[[I

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->B:[I

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->C:Landroid/content/res/ColorStateList;

    invoke-direct {p0, p1, p2}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void

    :array_74
    .array-data 0x4
        0x0t 0xfft 0x0t 0xfft
        0x0t 0x0t 0xfft 0xfft
        0x0t 0x0t 0x0t 0xfft
        0x88t 0x88t 0x88t 0xfft
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .registers 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    const/4 v6, 0x4

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a:Ljava/lang/String;

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->c:Ljava/lang/String;

    iput v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->d:I

    const/high16 v0, 0x41c0

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->e:F

    const/high16 v0, 0x4080

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->g:F

    const/high16 v0, 0x4100

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->h:F

    iput v6, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->i:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->p:Landroid/graphics/Rect;

    iput-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->q:Z

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->s:Lorg/npci/upi/security/pinactivitycomponent/widget/k;

    const/high16 v0, 0x3f80

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->u:F

    const/high16 v0, 0x4000

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->v:F

    iput-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->x:Z

    iput-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->y:Z

    new-array v0, v6, [[I

    new-array v1, v5, [I

    const v2, 0x10100a1

    aput v2, v1, v4

    aput-object v1, v0, v4

    new-array v1, v5, [I

    const v2, 0x10100a2

    aput v2, v1, v4

    aput-object v1, v0, v5

    const/4 v1, 0x2

    new-array v2, v5, [I

    const v3, 0x101009c

    aput v3, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-array v2, v5, [I

    const v3, -0x101009c

    aput v3, v2, v4

    aput-object v2, v0, v1

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->A:[[I

    new-array v0, v6, [I

    fill-array-data v0, :array_74

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->B:[I

    new-instance v0, Landroid/content/res/ColorStateList;

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->A:[[I

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->B:[I

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->C:Landroid/content/res/ColorStateList;

    invoke-direct {p0, p1, p2}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void

    :array_74
    .array-data 0x4
        0x0t 0xfft 0x0t 0xfft
        0x0t 0x0t 0xfft 0xfft
        0x0t 0x0t 0x0t 0xfft
        0x88t 0x88t 0x88t 0xfft
    .end array-data
.end method

.method private varargs a([I)I
    .registers 4

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->C:Landroid/content/res/ColorStateList;

    const v1, -0x777778

    invoke-virtual {v0, p1, v1}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;)Landroid/view/View$OnClickListener;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->r:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 9

    const v5, -0x777778

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->u:F

    invoke-virtual {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a(F)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->u:F

    iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->v:F

    invoke-virtual {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a(F)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->v:F

    iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->e:F

    invoke-virtual {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a(F)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->e:F

    iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->h:F

    invoke-virtual {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a(F)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->h:F

    sget-object v2, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemEditText:[I

    invoke-virtual {p1, p2, v2, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    :try_start_2f
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    sget v4, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemEditText_pinAnimationType:I

    invoke-virtual {v2, v4, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    iget v3, v3, Landroid/util/TypedValue;->data:I

    iput v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->d:I

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemEditText_pinCharacterMask:I

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a:Ljava/lang/String;

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemEditText_pinRepeatedHint:I

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->c:Ljava/lang/String;

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemEditText_pinLineStroke:I

    iget v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->u:F

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->u:F

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemEditText_pinLineStrokeSelected:I

    iget v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->v:F

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->v:F

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemEditText_pinLineStrokeCentered:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->t:Z

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemEditText_pinCharacterSize:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->f:F

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemEditText_pinCharacterSpacing:I

    iget v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->e:F

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->e:F

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemEditText_pinTextBottomPadding:I

    iget v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->h:F

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->h:F

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemEditText_pinBackgroundIsSquare:I

    iget-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->q:Z

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->q:Z

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemEditText_pinBackgroundDrawable:I

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->o:Landroid/graphics/drawable/Drawable;

    sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemEditText_pinLineColors:I

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    if-eqz v3, :cond_a3

    iput-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->C:Landroid/content/res/ColorStateList;
    :try_end_a3
    .catchall {:try_start_2f .. :try_end_a3} :catchall_155

    :cond_a3
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v2, Landroid/graphics/Paint;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->l:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->m:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->n:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->w:Landroid/graphics/Paint;

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->w:Landroid/graphics/Paint;

    iget v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->u:F

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->f:F

    invoke-virtual {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setFontSize(F)V

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    sget v4, Lorg/npci/upi/security/pinactivitycomponent/a$a;->colorControlActivated:I

    invoke-virtual {v3, v4, v2, v0}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v2, v2, Landroid/util/TypedValue;->data:I

    iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->B:[I

    aput v2, v3, v1

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->B:[I

    aput v5, v2, v0

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->B:[I

    const/4 v3, 0x2

    aput v5, v2, v3

    invoke-virtual {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setBackgroundResource(I)V

    const-string v2, "http://schemas.android.com/apk/res/android"

    const-string v3, "maxLength"

    const/4 v4, 0x4

    invoke-interface {p2, v2, v3, v4}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->i:I

    iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->i:I

    int-to-float v2, v2

    iput v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->g:F

    new-instance v2, Lorg/npci/upi/security/pinactivitycomponent/widget/d;

    invoke-direct {v2, p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/d;-><init>(Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;)V

    invoke-super {p0, v2}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Lorg/npci/upi/security/pinactivitycomponent/widget/e;

    invoke-direct {v2, p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/e;-><init>(Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;)V

    invoke-super {p0, v2}, Landroid/widget/EditText;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getInputType()I

    move-result v2

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_15a

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_15a

    const-string v2, "\u25cf"

    iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a:Ljava/lang/String;

    :cond_134
    :goto_134
    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_142

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getMaskChars()Ljava/lang/StringBuilder;

    move-result-object v2

    iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b:Ljava/lang/StringBuilder;

    :cond_142
    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    const-string v3, "|"

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->p:Landroid/graphics/Rect;

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->d:I

    const/4 v3, -0x1

    if-le v2, v3, :cond_171

    :goto_152
    iput-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->x:Z

    return-void

    :catchall_155
    move-exception v0

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    :cond_15a
    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getInputType()I

    move-result v2

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_134

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_134

    const-string v2, "\u25cf"

    iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a:Ljava/lang/String;

    goto :goto_134

    :cond_171
    move v0, v1

    goto :goto_152
.end method

.method private a(Ljava/lang/CharSequence;)V
    .registers 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->m:Landroid/graphics/Paint;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_44

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/widget/f;

    invoke-direct {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/f;-><init>(Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    iget v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->i:I

    if-ne v2, v3, :cond_37

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->s:Lorg/npci/upi/security/pinactivitycomponent/widget/k;

    if-eqz v2, :cond_37

    new-instance v2, Lorg/npci/upi/security/pinactivitycomponent/widget/g;

    invoke-direct {v2, p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/g;-><init>(Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;)V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_37
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/animation/Animator;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    return-void

    :array_44
    .array-data 0x4
        0x7dt 0x0t 0x0t 0x0t
        0xfft 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private a(Ljava/lang/CharSequence;I)V
    .registers 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const-wide/16 v8, 0x12c

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->k:[F

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v1, v1, p2

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->h:F

    sub-float/2addr v1, v2

    aput v1, v0, p2

    new-array v0, v5, [F

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->k:[F

    aget v1, v1, p2

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/TextPaint;->getTextSize()F

    move-result v2

    add-float/2addr v1, v2

    aput v1, v0, v6

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->k:[F

    aget v1, v1, p2

    aput v1, v0, v7

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v1}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/widget/h;

    invoke-direct {v1, p0, p2}, Lorg/npci/upi/security/pinactivitycomponent/widget/h;-><init>(Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;I)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->m:Landroid/graphics/Paint;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    new-array v1, v5, [I

    fill-array-data v1, :array_82

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v2, Lorg/npci/upi/security/pinactivitycomponent/widget/i;

    invoke-direct {v2, p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/i;-><init>(Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    iget v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->i:I

    if-ne v3, v4, :cond_74

    iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->s:Lorg/npci/upi/security/pinactivitycomponent/widget/k;

    if-eqz v3, :cond_74

    new-instance v3, Lorg/npci/upi/security/pinactivitycomponent/widget/j;

    invoke-direct {v3, p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/j;-><init>(Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;)V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_74
    new-array v3, v5, [Landroid/animation/Animator;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    return-void

    nop

    :array_82
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xfft 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private a(ZZ)V
    .registers 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->y:Z

    if-eqz v0, :cond_17

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->w:Landroid/graphics/Paint;

    new-array v1, v2, [I

    const v2, 0x10100a2

    aput v2, v1, v3

    invoke-direct {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a([I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    :goto_16
    return-void

    :cond_17
    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->w:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_37

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->v:F

    :goto_21
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    if-eqz p1, :cond_3a

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->w:Landroid/graphics/Paint;

    new-array v1, v2, [I

    const v2, 0x10100a1

    aput v2, v1, v3

    invoke-direct {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a([I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_16

    :cond_37
    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->u:F

    goto :goto_21

    :cond_3a
    if-eqz p2, :cond_5f

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_53

    new-array v0, v2, [I

    const v1, 0x10100a6

    aput v1, v0, v3

    invoke-direct {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a([I)I

    move-result v0

    :goto_4d
    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->w:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_16

    :cond_53
    new-array v0, v2, [I

    const v1, -0x10100a6

    aput v1, v0, v3

    invoke-direct {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a([I)I

    move-result v0

    goto :goto_4d

    :cond_5f
    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_76

    new-array v0, v2, [I

    const v1, 0x101009c

    aput v1, v0, v3

    invoke-direct {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a([I)I

    move-result v0

    :goto_70
    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->w:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_16

    :cond_76
    new-array v0, v2, [I

    const v1, -0x101009c

    aput v1, v0, v3

    invoke-direct {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a([I)I

    move-result v0

    goto :goto_70
.end method

.method static synthetic b(Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;)Landroid/graphics/Paint;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->m:Landroid/graphics/Paint;

    return-object v0
.end method

.method private b(ZZ)V
    .registers 8

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->y:Z

    if-eqz v0, :cond_14

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->o:Landroid/graphics/drawable/Drawable;

    new-array v1, v1, [I

    const v2, 0x10100a2

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_13
    :goto_13
    return-void

    :cond_14
    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_40

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->o:Landroid/graphics/drawable/Drawable;

    new-array v1, v1, [I

    const v2, 0x101009c

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    if-eqz p2, :cond_33

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->o:Landroid/graphics/drawable/Drawable;

    new-array v1, v4, [I

    fill-array-data v1, :array_4e

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_13

    :cond_33
    if-eqz p1, :cond_13

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->o:Landroid/graphics/drawable/Drawable;

    new-array v1, v4, [I

    fill-array-data v1, :array_56

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_13

    :cond_40
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->o:Landroid/graphics/drawable/Drawable;

    new-array v1, v1, [I

    const v2, -0x101009c

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_13

    nop

    :array_4e
    .array-data 0x4
        0x9ct 0x0t 0x1t 0x1t
        0xa1t 0x0t 0x1t 0x1t
    .end array-data

    :array_56
    .array-data 0x4
        0x9ct 0x0t 0x1t 0x1t
        0xa0t 0x0t 0x1t 0x1t
    .end array-data
.end method

.method static synthetic c(Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;)Lorg/npci/upi/security/pinactivitycomponent/widget/k;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->s:Lorg/npci/upi/security/pinactivitycomponent/widget/k;

    return-object v0
.end method

.method static synthetic d(Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;)[F
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->k:[F

    return-object v0
.end method

.method private getFullText()Ljava/lang/CharSequence;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a:Ljava/lang/String;

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    :goto_8
    return-object v0

    :cond_9
    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getMaskChars()Ljava/lang/StringBuilder;

    move-result-object v0

    goto :goto_8
.end method

.method private getMaskChars()Ljava/lang/StringBuilder;
    .registers 4

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b:Ljava/lang/StringBuilder;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b:Ljava/lang/StringBuilder;

    :cond_b
    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    :goto_13
    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eq v1, v0, :cond_39

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-ge v1, v0, :cond_2b

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_13

    :cond_2b
    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto :goto_13

    :cond_39
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b:Ljava/lang/StringBuilder;

    return-object v0
.end method


# virtual methods
.method a(F)I
    .registers 3

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    div-int/lit16 v0, v0, 0xa0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 14

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getFullText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v10

    new-array v11, v10, [F

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v10, v11}, Landroid/text/TextPaint;->getTextWidths(Ljava/lang/CharSequence;II[F)I

    const/4 v0, 0x0

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->c:Ljava/lang/String;

    if-eqz v2, :cond_34

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->c:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    new-array v4, v2, [F

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/text/TextPaint;->getTextWidths(Ljava/lang/String;[F)I

    array-length v5, v4

    const/4 v2, 0x0

    :goto_2a
    if-ge v2, v5, :cond_34

    aget v3, v4, v2

    add-float/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_2a

    :cond_34
    move v9, v0

    const/4 v2, 0x0

    :goto_36
    int-to-float v0, v2

    iget v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->g:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_ff

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->o:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_71

    if-ge v2, v10, :cond_ca

    const/4 v0, 0x1

    move v3, v0

    :goto_45
    if-ne v2, v10, :cond_ce

    const/4 v0, 0x1

    :goto_48
    invoke-direct {p0, v3, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->b(ZZ)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->o:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v3, v3, v2

    iget v3, v3, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v4, v4, v2

    iget v4, v4, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    iget-object v5, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v5, v5, v2

    iget v5, v5, Landroid/graphics/RectF;->right:F

    float-to-int v5, v5

    iget-object v6, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v6, v6, v2

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    float-to-int v6, v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_71
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->f:F

    const/high16 v4, 0x4000

    div-float/2addr v3, v4

    add-float/2addr v0, v3

    if-le v10, v2, :cond_e5

    iget-boolean v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->x:Z

    if-eqz v3, :cond_87

    add-int/lit8 v3, v10, -0x1

    if-eq v2, v3, :cond_d1

    :cond_87
    add-int/lit8 v3, v2, 0x1

    aget v4, v11, v2

    const/high16 v5, 0x4000

    div-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->k:[F

    aget v5, v0, v2

    iget-object v6, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->l:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    :cond_9a
    :goto_9a
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->o:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_c6

    if-ge v2, v10, :cond_fa

    const/4 v0, 0x1

    move v3, v0

    :goto_a2
    if-ne v2, v10, :cond_fd

    const/4 v0, 0x1

    :goto_a5
    invoke-direct {p0, v3, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a(ZZ)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    iget v4, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    iget v5, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    iget v6, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v8, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->w:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_c6
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_36

    :cond_ca
    const/4 v0, 0x0

    move v3, v0

    goto/16 :goto_45

    :cond_ce
    const/4 v0, 0x0

    goto/16 :goto_48

    :cond_d1
    add-int/lit8 v3, v2, 0x1

    aget v4, v11, v2

    const/high16 v5, 0x4000

    div-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->k:[F

    aget v5, v0, v2

    iget-object v6, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->m:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    goto :goto_9a

    :cond_e5
    iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->c:Ljava/lang/String;

    if-eqz v3, :cond_9a

    iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->c:Ljava/lang/String;

    const/high16 v4, 0x4000

    div-float v4, v9, v4

    sub-float/2addr v0, v4

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->k:[F

    aget v4, v4, v2

    iget-object v5, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v0, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_9a

    :cond_fa
    const/4 v0, 0x0

    move v3, v0

    goto :goto_a2

    :cond_fd
    const/4 v0, 0x0

    goto :goto_a5

    :cond_ff
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .registers 16

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->onSizeChanged(IIII)V

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->z:Landroid/content/res/ColorStateList;

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->z:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->m:Landroid/graphics/Paint;

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->z:Landroid/content/res/ColorStateList;

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->l:Landroid/graphics/Paint;

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->z:Landroid/content/res/ColorStateList;

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->n:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getCurrentHintTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    :cond_2c
    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getWidth()I

    move-result v0

    invoke-static {p0}, Landroid/support/v4/f/af;->h(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {p0}, Landroid/support/v4/f/af;->g(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->e:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_108

    int-to-float v0, v0

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->g:F

    const/high16 v2, 0x4000

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f80

    sub-float/2addr v1, v2

    div-float/2addr v0, v1

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->f:F

    :cond_4d
    :goto_4d
    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->g:F

    float-to-int v0, v0

    new-array v0, v0, [Landroid/graphics/RectF;

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->g:F

    float-to-int v0, v0

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->k:[F

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getPaddingTop()I

    move-result v1

    sub-int v3, v0, v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/d/d;->a(Ljava/util/Locale;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_120

    const/4 v0, 0x1

    :goto_76
    if-eqz v0, :cond_123

    const/4 v0, -0x1

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getWidth()I

    move-result v1

    invoke-static {p0}, Landroid/support/v4/f/af;->g(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->f:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    :goto_87
    const/4 v2, 0x0

    move v10, v2

    move v2, v1

    move v1, v10

    :goto_8b
    int-to-float v4, v1

    iget v5, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->g:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_14c

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    new-instance v5, Landroid/graphics/RectF;

    int-to-float v6, v2

    int-to-float v7, v3

    int-to-float v8, v2

    iget v9, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->f:F

    add-float/2addr v8, v9

    int-to-float v9, v3

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    aput-object v5, v4, v1

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->o:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_c5

    iget-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->q:Z

    if-eqz v4, :cond_12a

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v4, v4, v1

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getPaddingTop()I

    move-result v5

    int-to-float v5, v5

    iput v5, v4, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v4, v4, v1

    int-to-float v5, v2

    iget-object v6, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    add-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->right:F

    :cond_c5
    :goto_c5
    iget v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->e:F

    const/4 v5, 0x0

    cmpg-float v4, v4, v5

    if-gez v4, :cond_141

    int-to-float v2, v2

    int-to-float v4, v0

    iget v5, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->f:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x4000

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    float-to-int v2, v2

    :goto_d6
    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->k:[F

    iget-object v5, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    iget v6, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->h:F

    sub-float/2addr v5, v6

    aput v5, v4, v1

    iget-boolean v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->t:Z

    if-eqz v4, :cond_105

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->top:F

    const/high16 v6, 0x4000

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v4, v4, v1

    iget-object v5, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    const/high16 v6, 0x4000

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->bottom:F

    :cond_105
    add-int/lit8 v1, v1, 0x1

    goto :goto_8b

    :cond_108
    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->f:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_4d

    int-to-float v0, v0

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->e:F

    iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->g:F

    const/high16 v3, 0x3f80

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->g:F

    div-float/2addr v0, v1

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->f:F

    goto/16 :goto_4d

    :cond_120
    const/4 v0, 0x0

    goto/16 :goto_76

    :cond_123
    const/4 v0, 0x1

    invoke-static {p0}, Landroid/support/v4/f/af;->g(Landroid/view/View;)I

    move-result v1

    goto/16 :goto_87

    :cond_12a
    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    aget-object v4, v4, v1

    iget v5, v4, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->p:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->h:F

    const/high16 v8, 0x4000

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    sub-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/RectF;->top:F

    goto :goto_c5

    :cond_141
    int-to-float v2, v2

    int-to-float v4, v0

    iget v5, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->f:F

    iget v6, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->e:F

    add-float/2addr v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    float-to-int v2, v2

    goto :goto_d6

    :cond_14c
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 7

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setError(Z)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->j:[Landroid/graphics/RectF;

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->x:Z

    if-nez v0, :cond_1e

    :cond_c
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->s:Lorg/npci/upi/security/pinactivitycomponent/widget/k;

    if-eqz v0, :cond_1d

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->i:I

    if-ne v0, v1, :cond_1d

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->s:Lorg/npci/upi/security/pinactivitycomponent/widget/k;

    invoke-interface {v0, p1}, Lorg/npci/upi/security/pinactivitycomponent/widget/k;->a(Ljava/lang/CharSequence;)V

    :cond_1d
    :goto_1d
    return-void

    :cond_1e
    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_27

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->invalidate()V

    goto :goto_1d

    :cond_27
    if-le p4, p3, :cond_1d

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->d:I

    if-nez v0, :cond_31

    invoke-direct {p0, p1}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a(Ljava/lang/CharSequence;)V

    goto :goto_1d

    :cond_31
    invoke-direct {p0, p1, p2}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->a(Ljava/lang/CharSequence;I)V

    goto :goto_1d
.end method

.method public setAnimateText(Z)V
    .registers 2

    iput-boolean p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->x:Z

    return-void
.end method

.method public setCharSize(F)V
    .registers 2

    iput p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->f:F

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->invalidate()V

    return-void
.end method

.method public setColorStates(Landroid/content/res/ColorStateList;)V
    .registers 2

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->C:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->invalidate()V

    return-void
.end method

.method public setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V
    .registers 4

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "setCustomSelectionActionModeCallback() not supported."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setError(Z)V
    .registers 2

    iput-boolean p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->y:Z

    return-void
.end method

.method public setFontSize(F)V
    .registers 3

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->m:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->n:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    return-void
.end method

.method public setLineStroke(F)V
    .registers 2

    iput p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->u:F

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->invalidate()V

    return-void
.end method

.method public setLineStrokeCentered(Z)V
    .registers 2

    iput-boolean p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->t:Z

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->invalidate()V

    return-void
.end method

.method public setLineStrokeSelected(F)V
    .registers 2

    iput p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->v:F

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->invalidate()V

    return-void
.end method

.method public setMargin([I)V
    .registers 7

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v1, 0x0

    aget v1, p1, v1

    const/4 v2, 0x1

    aget v2, p1, v2

    const/4 v3, 0x2

    aget v3, p1, v3

    const/4 v4, 0x3

    aget v4, p1, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setMaxLength(I)V
    .registers 5

    iput p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->i:I

    int-to-float v0, p1

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->g:F

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v2, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .registers 2

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->r:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setOnPinEnteredListener(Lorg/npci/upi/security/pinactivitycomponent/widget/k;)V
    .registers 2

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->s:Lorg/npci/upi/security/pinactivitycomponent/widget/k;

    return-void
.end method

.method public setSpace(F)V
    .registers 2

    iput p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->e:F

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->invalidate()V

    return-void
.end method
