.class public Lorg/npci/upi/security/pinactivitycomponent/q;
.super Ljava/lang/Object;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Lorg/json/JSONArray;

.field private c:Landroid/content/Context;

.field private d:Lorg/npci/upi/security/pinactivitycomponent/ap;

.field private e:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lorg/npci/upi/security/pinactivitycomponent/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/npci/upi/security/pinactivitycomponent/q;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->a:Lorg/json/JSONArray;

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->c:Landroid/content/Context;

    new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/ap;

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/ap;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->d:Lorg/npci/upi/security/pinactivitycomponent/ap;

    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->a:Lorg/json/JSONArray;

    const-string v0, "npci_otp_rules.json"

    invoke-static {v0, p1}, Lorg/npci/upi/security/pinactivitycomponent/ae;->a(Ljava/lang/String;Landroid/content/Context;)[B

    move-result-object v0

    if-eqz v0, :cond_2c

    :try_start_20
    new-instance v1, Lorg/json/JSONArray;

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v1, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->a:Lorg/json/JSONArray;
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_2c} :catch_2d

    :cond_2c
    :goto_2c
    return-void

    :catch_2d
    move-exception v0

    sget-object v1, Lorg/npci/upi/security/pinactivitycomponent/q;->b:Ljava/lang/String;

    invoke-static {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2c
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 5

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    :try_start_4
    const-string v1, "MD5"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v2, v3, v0}, Ljava/security/MessageDigest;->update([BII)V

    new-instance v0, Ljava/math/BigInteger;

    const/4 v2, 0x1

    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    invoke-direct {v0, v2, v1}, Ljava/math/BigInteger;-><init>(I[B)V

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    :goto_26
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x20

    if-ge v1, v2, :cond_49

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_40
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_40} :catch_42

    move-result-object v0

    goto :goto_26

    :catch_42
    move-exception v0

    sget-object v1, Lorg/npci/upi/security/pinactivitycomponent/q;->b:Ljava/lang/String;

    invoke-static {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    const/4 v0, 0x0

    :cond_49
    return-object v0
.end method

.method private a(Ljava/util/ArrayList;)Ljava/lang/String;
    .registers 6

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_8

    const/4 v0, 0x0

    :cond_7
    return-object v0

    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x1

    move v3, v0

    move-object v0, v1

    move v1, v3

    :goto_26
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_26
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/npci/upi/security/pinactivitycomponent/p;
    .registers 10

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_2
    const-string v0, "sender"

    invoke-virtual {p4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-direct {p0, p3, v0}, Lorg/npci/upi/security/pinactivitycomponent/q;->a(Ljava/lang/String;Lorg/json/JSONArray;)Z

    move-result v0

    if-nez v0, :cond_10

    move-object v0, v1

    :goto_f
    return-object v0

    :cond_10
    const-string v0, "message"

    invoke-virtual {p4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lorg/npci/upi/security/pinactivitycomponent/q;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1e

    move-object v0, v1

    goto :goto_f

    :cond_1e
    if-eqz p1, :cond_60

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\\d{"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "}"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_39
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/p;

    invoke-direct {v0}, Lorg/npci/upi/security/pinactivitycomponent/p;-><init>()V

    invoke-virtual {v0, p2}, Lorg/npci/upi/security/pinactivitycomponent/p;->a(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_69

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v4

    if-gt v2, v4, :cond_69

    invoke-virtual {v3, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/npci/upi/security/pinactivitycomponent/p;->b(Ljava/lang/String;)V

    goto :goto_f

    :catch_5d
    move-exception v0

    move-object v0, v1

    goto :goto_f

    :cond_60
    const-string v0, "otp"

    invoke-virtual {p4, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_68
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_68} :catch_5d

    goto :goto_39

    :cond_69
    move-object v0, v1

    goto :goto_f
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4

    const/4 v0, 0x2

    invoke-static {p2, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method private a(Ljava/lang/String;Lorg/json/JSONArray;)Z
    .registers 7

    const/4 v1, 0x0

    move v0, v1

    :goto_2
    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_1c

    invoke-virtual {p2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_1d

    const/4 v1, 0x1

    :cond_1c
    return v1

    :cond_1d
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private b(Ljava/lang/String;)Z
    .registers 5

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->e:Ljava/util/List;

    if-nez v0, :cond_1a

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->d:Lorg/npci/upi/security/pinactivitycomponent/ap;

    const-string v1, "msgID"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/ap;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->e:Ljava/util/List;

    :cond_1a
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private b(Lorg/npci/upi/security/pinactivitycomponent/p;)Z
    .registers 3

    invoke-virtual {p1}, Lorg/npci/upi/security/pinactivitycomponent/p;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/q;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    invoke-direct {p0, p1}, Lorg/npci/upi/security/pinactivitycomponent/q;->c(Lorg/npci/upi/security/pinactivitycomponent/p;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/q;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private c(Lorg/npci/upi/security/pinactivitycomponent/p;)Ljava/lang/String;
    .registers 3

    invoke-virtual {p1}, Lorg/npci/upi/security/pinactivitycomponent/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .registers 5

    if-nez p1, :cond_3

    :cond_2
    :goto_2
    return-void

    :cond_3
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->d:Lorg/npci/upi/security/pinactivitycomponent/ap;

    const-string v1, "msgID"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/ap;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v2, 0xa

    if-lt v0, v2, :cond_2e

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_2e
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/q;->a(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->d:Lorg/npci/upi/security/pinactivitycomponent/ap;

    const-string v2, "msgID"

    invoke-virtual {v1, v2, v0}, Lorg/npci/upi/security/pinactivitycomponent/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public a(IJ)Lorg/npci/upi/security/pinactivitycomponent/p;
    .registers 14

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v0, "content://sms/inbox"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v4

    const-string v0, "address"

    aput-object v0, v2, v3

    const-string v0, "body"

    aput-object v0, v2, v5

    const/4 v0, 0x3

    const-string v3, "date"

    aput-object v3, v2, v0

    const-string v0, "date > ?"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "date DESC"

    :try_start_28
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4a
    .catchall {:try_start_28 .. :try_end_4a} :catchall_a1
    .catch Ljava/lang/SecurityException; {:try_start_28 .. :try_end_4a} :catch_8c
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_4a} :catch_94

    move-result-object v1

    :cond_4b
    :try_start_4b
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_85

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v0, v2}, Lorg/npci/upi/security/pinactivitycomponent/q;->a(ILjava/lang/String;Ljava/lang/String;)Lorg/npci/upi/security/pinactivitycomponent/p;

    move-result-object v0

    if-eqz v0, :cond_4b

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/npci/upi/security/pinactivitycomponent/p;->c(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/q;->b(Lorg/npci/upi/security/pinactivitycomponent/p;)Z

    move-result v2

    if-eqz v2, :cond_4b

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/q;->a(Lorg/npci/upi/security/pinactivitycomponent/p;)V
    :try_end_7f
    .catchall {:try_start_4b .. :try_end_7f} :catchall_a9
    .catch Ljava/lang/SecurityException; {:try_start_4b .. :try_end_7f} :catch_ad
    .catch Ljava/lang/Exception; {:try_start_4b .. :try_end_7f} :catch_ab

    if-eqz v1, :cond_84

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_84
    :goto_84
    return-object v0

    :cond_85
    if-eqz v1, :cond_8a

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_8a
    :goto_8a
    move-object v0, v6

    goto :goto_84

    :catch_8c
    move-exception v0

    move-object v0, v6

    :goto_8e
    if-eqz v0, :cond_8a

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_8a

    :catch_94
    move-exception v0

    move-object v1, v6

    :goto_96
    :try_start_96
    sget-object v2, Lorg/npci/upi/security/pinactivitycomponent/q;->b:Ljava/lang/String;

    invoke-static {v2, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_9b
    .catchall {:try_start_96 .. :try_end_9b} :catchall_a9

    if-eqz v1, :cond_8a

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_8a

    :catchall_a1
    move-exception v0

    move-object v1, v6

    :goto_a3
    if-eqz v1, :cond_a8

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_a8
    throw v0

    :catchall_a9
    move-exception v0

    goto :goto_a3

    :catch_ab
    move-exception v0

    goto :goto_96

    :catch_ad
    move-exception v0

    move-object v0, v1

    goto :goto_8e
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;)Lorg/npci/upi/security/pinactivitycomponent/p;
    .registers 7

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v2, v0

    :goto_3
    :try_start_3
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->a:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_24

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/q;->a:Lorg/json/JSONArray;

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {p0, p1, p3, p2, v0}, Lorg/npci/upi/security/pinactivitycomponent/q;->a(ILjava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/npci/upi/security/pinactivitycomponent/p;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_14} :catch_1c

    move-result-object v0

    if-eqz v0, :cond_18

    :goto_17
    return-object v0

    :cond_18
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :catch_1c
    move-exception v0

    sget-object v2, Lorg/npci/upi/security/pinactivitycomponent/q;->b:Ljava/lang/String;

    invoke-static {v2, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v0, v1

    goto :goto_17

    :cond_24
    move-object v0, v1

    goto :goto_17
.end method

.method public a(Lorg/npci/upi/security/pinactivitycomponent/p;)V
    .registers 3

    invoke-virtual {p1}, Lorg/npci/upi/security/pinactivitycomponent/p;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lorg/npci/upi/security/pinactivitycomponent/p;->c()Ljava/lang/String;

    move-result-object v0

    :goto_a
    invoke-direct {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/q;->c(Ljava/lang/String;)V

    return-void

    :cond_e
    invoke-direct {p0, p1}, Lorg/npci/upi/security/pinactivitycomponent/q;->c(Lorg/npci/upi/security/pinactivitycomponent/p;)Ljava/lang/String;

    move-result-object v0

    goto :goto_a
.end method
