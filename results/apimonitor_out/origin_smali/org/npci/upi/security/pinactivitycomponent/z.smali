.class public Lorg/npci/upi/security/pinactivitycomponent/z;
.super Ljava/lang/Object;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Lorg/npci/upi/security/pinactivitycomponent/ab;

.field private d:Lorg/npci/upi/security/pinactivitycomponent/aa;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    const-string v0, ""

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->e:Ljava/lang/String;

    :try_start_e
    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->a:Landroid/content/Context;

    new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/ab;

    invoke-direct {v0, p1}, Lorg/npci/upi/security/pinactivitycomponent/ab;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/aa;

    invoke-direct {v0}, Lorg/npci/upi/security/pinactivitycomponent/aa;-><init>()V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->d:Lorg/npci/upi/security/pinactivitycomponent/aa;
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_1e} :catch_1f

    return-void

    :catch_1f
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    throw v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 9

    const/4 v0, 0x0

    :try_start_1
    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/ab;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Token in CL"

    invoke-static {v2, v1}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->d:Lorg/npci/upi/security/pinactivitycomponent/aa;

    invoke-virtual {v2, v1}, Lorg/npci/upi/security/pinactivitycomponent/aa;->b(Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->d:Lorg/npci/upi/security/pinactivitycomponent/aa;

    invoke-virtual {v3, v2, v1}, Lorg/npci/upi/security/pinactivitycomponent/aa;->b([B[B)[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CL Hmac Msg"

    invoke-static {v3, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->d:Lorg/npci/upi/security/pinactivitycomponent/aa;

    invoke-virtual {v3, v2}, Lorg/npci/upi/security/pinactivitycomponent/aa;->a(Ljava/lang/String;)[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    const-string v3, "CL Hash"

    invoke-static {v3, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_5b
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_5b} :catch_60
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1 .. :try_end_5b} :catch_65
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1 .. :try_end_5b} :catch_6a
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_1 .. :try_end_5b} :catch_6f
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_5b} :catch_74
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_5b} :catch_79
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_5b} :catch_7e

    move-result v1

    if-eqz v1, :cond_5f

    const/4 v0, 0x1

    :cond_5f
    :goto_5f
    return v0

    :catch_60
    move-exception v1

    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_5f

    :catch_65
    move-exception v1

    invoke-virtual {v1}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V

    goto :goto_5f

    :catch_6a
    move-exception v1

    invoke-virtual {v1}, Ljavax/crypto/BadPaddingException;->printStackTrace()V

    goto :goto_5f

    :catch_6f
    move-exception v1

    invoke-virtual {v1}, Ljava/security/InvalidAlgorithmParameterException;->printStackTrace()V

    goto :goto_5f

    :catch_74
    move-exception v1

    invoke-virtual {v1}, Ljava/security/InvalidKeyException;->printStackTrace()V

    goto :goto_5f

    :catch_79
    move-exception v1

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_5f

    :catch_7e
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5f
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 3

    const-string v0, ""

    :try_start_2
    const-string v0, "AES"

    invoke-static {v0}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v0

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Ljavax/crypto/KeyGenerator;->init(I)V

    invoke-virtual {v0}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v0

    invoke-interface {v0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v0

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->d:Lorg/npci/upi/security/pinactivitycomponent/aa;

    invoke-virtual {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/aa;->a([B)Ljava/lang/String;
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_1a} :catch_1c

    move-result-object v0

    :goto_1b
    return-object v0

    :catch_1c
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 11

    const-string v2, ""

    const-string v1, ""

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v3, "dd/MM/yyyy"

    invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/sql/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    :try_start_18
    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/z;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/z;->a()Ljava/lang/String;
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_21} :catch_85

    move-result-object v0

    move-object v2, v0

    :goto_23
    :try_start_23
    new-instance v4, Lorg/npci/upi/security/pinactivitycomponent/aa;

    invoke-direct {v4}, Lorg/npci/upi/security/pinactivitycomponent/aa;-><init>()V

    const-string v0, "initial"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9f

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/ab;->c()V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/ab;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8a

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    new-instance v5, Lorg/npci/upi/security/pinactivitycomponent/u;

    iget-object v6, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->b:Ljava/lang/String;

    invoke-direct {v5, v2, v6, v3}, Lorg/npci/upi/security/pinactivitycomponent/u;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lorg/npci/upi/security/pinactivitycomponent/ab;->b(Lorg/npci/upi/security/pinactivitycomponent/u;)I

    :goto_4d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "|"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "|"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lin/org/npci/commonlibrary/l;->a()Ljava/security/PublicKey;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/aa;->a(Ljava/lang/String;Ljava/security/PublicKey;)Ljava/lang/String;
    :try_end_77
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_77} :catch_97

    move-result-object v0

    :try_start_78
    const-string v1, "K0 in Challenge"

    invoke-static {v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "token in Challenge"

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_84
    .catch Ljava/lang/Exception; {:try_start_78 .. :try_end_84} :catch_102

    :goto_84
    return-object v0

    :catch_85
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_23

    :cond_8a
    :try_start_8a
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    new-instance v5, Lorg/npci/upi/security/pinactivitycomponent/u;

    iget-object v6, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->b:Ljava/lang/String;

    invoke-direct {v5, v2, v6, v3}, Lorg/npci/upi/security/pinactivitycomponent/u;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lorg/npci/upi/security/pinactivitycomponent/ab;->a(Lorg/npci/upi/security/pinactivitycomponent/u;)V
    :try_end_96
    .catch Ljava/lang/Exception; {:try_start_8a .. :try_end_96} :catch_97

    goto :goto_4d

    :catch_97
    move-exception v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    :goto_9b
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_84

    :cond_9f
    :try_start_9f
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/ab;->a()Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/u;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/u;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "K0 in Challenge"

    invoke-static {v5, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "token in Challenge"

    iget-object v6, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->b:Ljava/lang/String;

    invoke-static {v5, v6}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Lorg/npci/upi/security/pinactivitycomponent/aa;->b(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Lorg/npci/upi/security/pinactivitycomponent/aa;->a([B[B)[B

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v0, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_ef
    .catch Ljava/lang/Exception; {:try_start_9f .. :try_end_ef} :catch_97

    move-result-object v0

    :try_start_f0
    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/ab;->c()V

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->c:Lorg/npci/upi/security/pinactivitycomponent/ab;

    new-instance v4, Lorg/npci/upi/security/pinactivitycomponent/u;

    iget-object v5, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->b:Ljava/lang/String;

    invoke-direct {v4, v2, v5, v3}, Lorg/npci/upi/security/pinactivitycomponent/u;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Lorg/npci/upi/security/pinactivitycomponent/ab;->a(Lorg/npci/upi/security/pinactivitycomponent/u;)V
    :try_end_101
    .catch Ljava/lang/Exception; {:try_start_f0 .. :try_end_101} :catch_102

    goto :goto_84

    :catch_102
    move-exception v1

    goto :goto_9b
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6

    const-string v0, "hmac"

    invoke-static {v0, p4}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p4, p1, p2, p3}, Lorg/npci/upi/security/pinactivitycomponent/z;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public b()Ljava/lang/String;
    .registers 3

    const-string v0, ""

    :try_start_2
    const-string v0, "AES"

    invoke-static {v0}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v0

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Ljavax/crypto/KeyGenerator;->init(I)V

    invoke-virtual {v0}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v0

    invoke-interface {v0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v0

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/z;->d:Lorg/npci/upi/security/pinactivitycomponent/aa;

    invoke-virtual {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/aa;->a([B)Ljava/lang/String;
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_1a} :catch_1c

    move-result-object v0

    :goto_1b
    return-object v0

    :catch_1c
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_1b
.end method
