.class Lorg/npci/upi/security/pinactivitycomponent/widget/l;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;


# direct methods
.method constructor <init>(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)V
    .registers 2

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 7

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->e(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Lorg/npci/upi/security/pinactivitycomponent/widget/b;Ljava/lang/String;)Ljava/lang/String;

    :goto_11
    return-void

    :cond_12
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->f(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Lorg/npci/upi/security/pinactivitycomponent/widget/b;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_11

    :cond_24
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_32

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    const-string v1, ""

    invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Lorg/npci/upi/security/pinactivitycomponent/widget/b;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_11

    :cond_32
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->g(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-le v0, v1, :cond_5f

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->g(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v3}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->g(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Lorg/npci/upi/security/pinactivitycomponent/widget/b;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_11

    :cond_5f
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x25cf

    if-eq v0, v1, :cond_ac

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v2}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->g(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Lorg/npci/upi/security/pinactivitycomponent/widget/b;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->g(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    const-string v2, "\u25cf"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->b(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_11

    :cond_ac
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->b(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    move-result-object v0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setSelection(I)V

    goto/16 :goto_11
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 8

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Lorg/npci/upi/security/pinactivitycomponent/widget/o;

    move-result-object v0

    if-nez v0, :cond_9

    :cond_8
    :goto_8
    return-void

    :cond_9
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->b(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    move-result-object v0

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->b(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    move-result-object v0

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->c(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)I

    move-result v1

    if-lt v0, v1, :cond_8

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Lorg/npci/upi/security/pinactivitycomponent/widget/o;

    move-result-object v0

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->d(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)I

    move-result v1

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/l;->a:Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-static {v2}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->b(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    move-result-object v2

    invoke-virtual {v2}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/widget/o;->a(ILjava/lang/String;)V

    goto :goto_8
.end method
