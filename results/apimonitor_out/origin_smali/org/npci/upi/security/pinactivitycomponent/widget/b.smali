.class public Lorg/npci/upi/security/pinactivitycomponent/widget/b;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Lorg/npci/upi/security/pinactivitycomponent/widget/c;


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Landroid/widget/TextView;

.field private f:Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

.field private g:Lorg/npci/upi/security/pinactivitycomponent/widget/o;

.field private h:I

.field private i:Ljava/lang/Object;

.field private j:Landroid/widget/LinearLayout;

.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/ProgressBar;

.field private m:Landroid/widget/ImageView;

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-boolean v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a:Z

    const-string v0, ""

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->n:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->o:Z

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic a(Lorg/npci/upi/security/pinactivitycomponent/widget/b;Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->n:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Lorg/npci/upi/security/pinactivitycomponent/widget/o;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->g:Lorg/npci/upi/security/pinactivitycomponent/widget/o;

    return-object v0
.end method

.method static synthetic b(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->f:Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    return-object v0
.end method

.method static synthetic c(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)I
    .registers 2

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->d:I

    return v0
.end method

.method static synthetic d(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)I
    .registers 2

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->h:I

    return v0
.end method

.method static synthetic e(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Z
    .registers 2

    iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a:Z

    return v0
.end method

.method static synthetic f(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Z
    .registers 2

    iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->o:Z

    return v0
.end method

.method static synthetic g(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->n:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;Z)Landroid/support/v4/f/au;
    .registers 7

    const/4 v2, 0x0

    const/high16 v1, 0x3f80

    invoke-static {p1}, Landroid/support/v4/f/af;->k(Landroid/view/View;)Landroid/support/v4/f/au;

    move-result-object v3

    if-eqz p2, :cond_2e

    move v0, v1

    :goto_a
    invoke-virtual {v3, v0}, Landroid/support/v4/f/au;->d(F)Landroid/support/v4/f/au;

    move-result-object v0

    if-eqz p2, :cond_11

    move v2, v1

    :cond_11
    invoke-virtual {v0, v2}, Landroid/support/v4/f/au;->c(F)Landroid/support/v4/f/au;

    move-result-object v0

    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/support/v4/f/au;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/f/au;

    move-result-object v0

    new-instance v2, Lorg/npci/upi/security/pinactivitycomponent/widget/n;

    invoke-direct {v2, p0, p2}, Lorg/npci/upi/security/pinactivitycomponent/widget/n;-><init>(Lorg/npci/upi/security/pinactivitycomponent/widget/b;Z)V

    invoke-virtual {v0, v2}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/ay;)Landroid/support/v4/f/au;

    move-result-object v0

    if-eqz p2, :cond_30

    :goto_29
    invoke-virtual {v0, v1}, Landroid/support/v4/f/au;->a(F)Landroid/support/v4/f/au;

    move-result-object v0

    return-object v0

    :cond_2e
    move v0, v2

    goto :goto_a

    :cond_30
    const/high16 v1, 0x3f00

    goto :goto_29
.end method

.method public a()V
    .registers 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a:Z

    return-void
.end method

.method public a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7

    const/4 v3, 0x0

    sget-object v0, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    if-eqz v0, :cond_2d

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemView_formTitle:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->b:Ljava/lang/String;

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemView_formValidationError:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->c:Ljava/lang/String;

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemView_formInputLength:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->d:I

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$h;->FormItemView_formActionOnTop:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->p:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_2d
    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$e;->layout_form_item:I

    invoke-static {p1, v0, p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->form_item_root:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->r:Landroid/widget/RelativeLayout;

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->form_item_action_bar:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->j:Landroid/widget/LinearLayout;

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->form_item_title:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->e:Landroid/widget/TextView;

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->form_item_input:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->f:Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->form_item_button:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->k:Landroid/widget/Button;

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->form_item_progress:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->l:Landroid/widget/ProgressBar;

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->form_item_image:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->m:Landroid/widget/ImageView;

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->f:Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    invoke-virtual {v0, v3}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setInputType(I)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setTitle(Ljava/lang/String;)V

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->d:I

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setInputLength(I)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->f:Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/widget/l;

    invoke-direct {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/l;-><init>(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)V

    invoke-virtual {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->f:Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/widget/m;

    invoke-direct {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/m;-><init>(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)V

    invoke-virtual {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->p:Z

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setActionBarPositionTop(Z)V

    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;Z)V
    .registers 4

    if-eqz p1, :cond_7

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_7
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->m:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p2}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Landroid/view/View;Z)Landroid/support/v4/f/au;

    return-void
.end method

.method public a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;IZZ)V
    .registers 13

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->k:Landroid/widget/Button;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_c
    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->k:Landroid/widget/Button;

    if-nez p4, :cond_2f

    move-object v3, p2

    :goto_11
    const/4 v1, 0x1

    if-ne p4, v1, :cond_31

    move-object v2, p2

    :goto_15
    const/4 v1, 0x2

    if-ne p4, v1, :cond_33

    move-object v1, p2

    :goto_19
    const/4 v5, 0x3

    if-ne p4, v5, :cond_35

    :goto_1c
    invoke-virtual {v4, v3, v2, v1, p2}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->k:Landroid/widget/Button;

    invoke-virtual {v0, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->k:Landroid/widget/Button;

    invoke-virtual {v0, p6}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->k:Landroid/widget/Button;

    invoke-virtual {p0, v0, p5}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Landroid/view/View;Z)Landroid/support/v4/f/au;

    return-void

    :cond_2f
    move-object v3, v0

    goto :goto_11

    :cond_31
    move-object v2, v0

    goto :goto_15

    :cond_33
    move-object v1, v0

    goto :goto_19

    :cond_35
    move-object p2, v0

    goto :goto_1c
.end method

.method public a(Ljava/lang/String;Landroid/view/View$OnClickListener;ZZ)V
    .registers 6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->k:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_b
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->k:Landroid/widget/Button;

    invoke-virtual {p0, v0, p3}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Landroid/view/View;Z)Landroid/support/v4/f/au;

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->k:Landroid/widget/Button;

    invoke-virtual {v0, p4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->k:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public a(Z)V
    .registers 4

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->l:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0, p1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Landroid/view/View;Z)Landroid/support/v4/f/au;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/support/v4/f/au;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/f/au;

    invoke-virtual {v0}, Landroid/support/v4/f/au;->c()V

    return-void
.end method

.method public c()Z
    .registers 4

    iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->o:Z

    if-nez v0, :cond_f

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->o:Z

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->n:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setText(Ljava/lang/String;)V

    :goto_c
    iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->o:Z

    return v0

    :cond_f
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->o:Z

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->n:Ljava/lang/String;

    const-string v1, "."

    const-string v2, "\u25cf"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->f:Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    invoke-virtual {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_c
.end method

.method public d()Z
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->f:Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->requestFocus()Z

    const/4 v0, 0x1

    return v0
.end method

.method public getFormDataTag()Ljava/lang/Object;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->i:Ljava/lang/Object;

    return-object v0
.end method

.method public getFormInputView()Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->f:Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    return-object v0
.end method

.method public getFormItemListener()Lorg/npci/upi/security/pinactivitycomponent/widget/o;
    .registers 2

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->g:Lorg/npci/upi/security/pinactivitycomponent/widget/o;

    return-object v0
.end method

.method public getInputLength()I
    .registers 2

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->d:I

    return v0
.end method

.method public getInputValue()Ljava/lang/String;
    .registers 2

    iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->o:Z

    if-eqz v0, :cond_13

    :cond_8
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->f:Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_12
    return-object v0

    :cond_13
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->n:Ljava/lang/String;

    goto :goto_12
.end method

.method public setActionBarPositionTop(Z)V
    .registers 7

    const/16 v4, 0xa

    const/16 v3, 0x8

    const/4 v2, 0x0

    iput-boolean p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->q:Z

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-boolean v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->q:Z

    if-eqz v1, :cond_1f

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v0, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    :goto_19
    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_1f
    invoke-virtual {v0, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$d;->form_item_input:I

    invoke-virtual {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_19
.end method

.method public setFormDataTag(Ljava/lang/Object;)V
    .registers 2

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->i:Ljava/lang/Object;

    return-void
.end method

.method public setFormItemListener(Lorg/npci/upi/security/pinactivitycomponent/widget/o;)V
    .registers 2

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->g:Lorg/npci/upi/security/pinactivitycomponent/widget/o;

    return-void
.end method

.method public setFormItemTag(I)V
    .registers 2

    iput p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->h:I

    return-void
.end method

.method public setInputLength(I)V
    .registers 3

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->f:Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    invoke-virtual {v0, p1}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setMaxLength(I)V

    iput p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->d:I

    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .registers 4

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->f:Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    invoke-virtual {v0, p1}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->f:Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setSelection(I)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .registers 3

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->b:Ljava/lang/String;

    return-void
.end method
