.class Lorg/npci/upi/security/pinactivitycomponent/am;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;


# direct methods
.method private constructor <init>(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)V
    .registers 2

    iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/am;->a:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;Lorg/npci/upi/security/pinactivitycomponent/af;)V
    .registers 3

    invoke-direct {p0, p1}, Lorg/npci/upi/security/pinactivitycomponent/am;-><init>(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)V

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .registers 10

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_67

    const-string v1, "pdus"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    array-length v1, v0

    new-array v3, v1, [Landroid/telephony/SmsMessage;

    const/4 v1, 0x0

    move v2, v1

    :goto_15
    array-length v1, v3

    if-ge v2, v1, :cond_67

    aget-object v1, v0, v2

    check-cast v1, [B

    check-cast v1, [B

    invoke-static {v1}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v1

    aput-object v1, v3, v2

    aget-object v1, v3, v2

    invoke-virtual {v1}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    aget-object v4, v3, v2

    invoke-virtual {v4}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    aget-object v6, v3, v2

    invoke-virtual {v6}, Landroid/telephony/SmsMessage;->getTimestampMillis()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    new-instance v5, Lorg/npci/upi/security/pinactivitycomponent/q;

    iget-object v6, p0, Lorg/npci/upi/security/pinactivitycomponent/am;->a:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;

    invoke-static {v6}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->g(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/npci/upi/security/pinactivitycomponent/q;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lorg/npci/upi/security/pinactivitycomponent/am;->a:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;

    invoke-static {v6}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->h(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)I

    move-result v6

    invoke-virtual {v5, v6, v1, v4}, Lorg/npci/upi/security/pinactivitycomponent/q;->a(ILjava/lang/String;Ljava/lang/String;)Lorg/npci/upi/security/pinactivitycomponent/p;

    move-result-object v1

    if-eqz v1, :cond_63

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/am;->a:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;

    invoke-static {v4}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->b(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Lorg/npci/upi/security/pinactivitycomponent/h;

    move-result-object v4

    invoke-virtual {v4, v1}, Lorg/npci/upi/security/pinactivitycomponent/h;->b(Lorg/npci/upi/security/pinactivitycomponent/p;)V

    :cond_63
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_15

    :cond_67
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 5

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-direct {p0, p2}, Lorg/npci/upi/security/pinactivitycomponent/am;->a(Landroid/content/Intent;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_f} :catch_10

    :cond_f
    :goto_f
    return-void

    :catch_10
    move-exception v0

    invoke-static {}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_f
.end method
