.class public Lorg/npci/upi/security/pinactivitycomponent/b;
.super Lorg/npci/upi/security/pinactivitycomponent/h;

# interfaces
.implements Lorg/npci/upi/security/pinactivitycomponent/widget/o;


# static fields
.field private static final ap:Ljava/lang/String;


# instance fields
.field private aq:Ljava/util/HashMap;

.field private ar:I

.field private as:Z

.field private at:Landroid/widget/ViewSwitcher;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lorg/npci/upi/security/pinactivitycomponent/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/npci/upi/security/pinactivitycomponent/b;->ap:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->aq:Ljava/util/HashMap;

    iput v1, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ar:I

    iput-boolean v1, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->as:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->at:Landroid/widget/ViewSwitcher;

    return-void
.end method

.method private M()V
    .registers 8

    const/4 v6, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_37

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    if-eqz v0, :cond_37

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2d

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputLength()I

    move-result v3

    if-eq v1, v3, :cond_37

    :cond_2d
    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->invalid_otp:I

    invoke-virtual {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/b;->b(Landroid/view/View;Ljava/lang/String;)V

    :cond_36
    :goto_36
    return-void

    :cond_37
    move v1, v2

    :goto_38
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6e

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    if-eqz v0, :cond_6a

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputLength()I

    move-result v4

    if-eq v3, v4, :cond_6a

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->componentMessage:I

    invoke-virtual {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/b;->b(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_36

    :cond_6a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_38

    :cond_6e
    iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->as:Z

    if-nez v0, :cond_36

    iput-boolean v6, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->as:Z

    :goto_74
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_e2

    :try_start_7c
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/c;

    invoke-interface {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/c;->getFormDataTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    const-string v1, "type"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "subtype"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->b:Lorg/json/JSONObject;

    const-string v5, "credential"

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/c;

    invoke-interface {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/c;->getInputValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ao:Landroid/content/Context;

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o()Lorg/npci/upi/security/pinactivitycomponent/w;

    move-result-object v0

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/w;->a()Lorg/npci/upi/security/pinactivitycomponent/t;

    move-result-object v0

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->b:Lorg/json/JSONObject;

    invoke-virtual {v0, v4}, Lorg/npci/upi/security/pinactivitycomponent/t;->a(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ao:Landroid/content/Context;

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o()Lorg/npci/upi/security/pinactivitycomponent/w;

    move-result-object v0

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/w;->b()Lorg/npci/upi/security/pinactivitycomponent/ac;

    move-result-object v0

    iget-object v5, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->b:Lorg/json/JSONObject;

    invoke-virtual {v0, v4, v1, v3, v5}, Lorg/npci/upi/security/pinactivitycomponent/ac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Lin/org/npci/commonlibrary/Message;

    move-result-object v0

    if-eqz v0, :cond_d8

    iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->aq:Ljava/util/HashMap;

    invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/ao;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_d8
    .catch Ljava/lang/Exception; {:try_start_7c .. :try_end_d8} :catch_db

    :cond_d8
    :goto_d8
    add-int/lit8 v2, v2, 0x1

    goto :goto_74

    :catch_db
    move-exception v0

    sget-object v1, Lorg/npci/upi/security/pinactivitycomponent/b;->ap:Ljava/lang/String;

    invoke-static {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_d8

    :cond_e2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "credBlocks"

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->aq:Ljava/util/HashMap;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ao:Landroid/content/Context;

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o()Lorg/npci/upi/security/pinactivitycomponent/w;

    move-result-object v0

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/w;->d()Landroid/os/ResultReceiver;

    move-result-object v0

    invoke-virtual {v0, v6, v1}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ao:Landroid/content/Context;

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o()Lorg/npci/upi/security/pinactivitycomponent/w;

    move-result-object v0

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/w;->c()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_36
.end method

.method private N()V
    .registers 18

    move-object/from16 v0, p0

    iget v1, v0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2d

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v2, v0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    if-eqz v1, :cond_2d

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v2, v0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)V

    invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a()V

    :cond_2d
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v16

    move v15, v1

    :goto_37
    move/from16 v0, v16

    if-ge v15, v0, :cond_84

    move-object/from16 v0, p0

    iget v1, v0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    if-eq v15, v1, :cond_80

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/npci/upi/security/pinactivitycomponent/widget/c;

    invoke-virtual/range {p0 .. p0}, Lorg/npci/upi/security/pinactivitycomponent/b;->h()Landroid/support/v4/app/l;

    move-result-object v1

    sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$c;->ic_visibility_on:I

    invoke-static {v1, v2}, Landroid/support/v4/a/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lorg/npci/upi/security/pinactivitycomponent/b;->h()Landroid/support/v4/app/l;

    move-result-object v1

    sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$c;->ic_visibility_off:I

    invoke-static {v1, v2}, Landroid/support/v4/a/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->action_hide:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(I)Ljava/lang/String;

    move-result-object v4

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->action_show:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(I)Ljava/lang/String;

    move-result-object v5

    new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/v;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v7}, Lorg/npci/upi/security/pinactivitycomponent/v;-><init>(Lorg/npci/upi/security/pinactivitycomponent/b;Lorg/npci/upi/security/pinactivitycomponent/widget/c;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x1

    move-object v8, v3

    move-object v9, v5

    move-object v10, v7

    move-object v11, v1

    invoke-interface/range {v8 .. v14}, Lorg/npci/upi/security/pinactivitycomponent/widget/c;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;IZZ)V

    :cond_80
    add-int/lit8 v1, v15, 0x1

    move v15, v1

    goto :goto_37

    :cond_84
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 10

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->switcherLayout1:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$d;->switcherLayout2:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$d;->view_switcher:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ViewSwitcher;

    iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->at:Landroid/widget/ViewSwitcher;

    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->c:Lorg/json/JSONArray;

    if-eqz v2, :cond_fa

    const/4 v2, 0x0

    move v3, v2

    :goto_20
    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->c:Lorg/json/JSONArray;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v3, v2, :cond_fa

    :try_start_28
    iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->c:Lorg/json/JSONArray;

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v2, "subtype"

    invoke-virtual {v6, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v2, "dLength"

    invoke-virtual {v6, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_80

    const/4 v2, 0x6

    move v5, v2

    :goto_3e
    const-string v2, "MPIN"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_88

    sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$f;->npci_set_mpin_title:I

    invoke-virtual {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v3, v5}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(Ljava/lang/String;II)Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    move-result-object v2

    sget v4, Lorg/npci/upi/security/pinactivitycomponent/a$f;->npci_confirm_mpin_title:I

    invoke-virtual {p0, v4}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v3, v5}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(Ljava/lang/String;II)Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/npci/upi/security/pinactivitycomponent/widget/a;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/b;->h()Landroid/support/v4/app/l;

    move-result-object v4

    invoke-direct {v2, v4}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v5, p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a(Ljava/util/ArrayList;Lorg/npci/upi/security/pinactivitycomponent/widget/o;)V

    invoke-virtual {v2, v6}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->setFormDataTag(Ljava/lang/Object;)V

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :goto_7c
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_20

    :cond_80
    const-string v2, "dLength"

    invoke-virtual {v6, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    move v5, v2

    goto :goto_3e

    :cond_88
    const-string v2, ""

    const-string v7, "ATMPIN"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b1

    sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$f;->npci_atm_title:I

    invoke-virtual {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(I)Ljava/lang/String;

    move-result-object v2

    :cond_98
    :goto_98
    invoke-virtual {p0, v2, v3, v5}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(Ljava/lang/String;II)Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    move-result-object v2

    invoke-virtual {v2, v6}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setFormDataTag(Ljava/lang/Object;)V

    iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V
    :try_end_a7
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_a7} :catch_a8

    goto :goto_7c

    :catch_a8
    move-exception v2

    sget-object v4, Lorg/npci/upi/security/pinactivitycomponent/b;->ap:Ljava/lang/String;

    const-string v5, "Error while inflating Layout"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_7c

    :cond_b1
    :try_start_b1
    const-string v7, "OTP"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d9

    const-string v7, "SMS"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d9

    const-string v7, "EMAIL"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d9

    const-string v7, "HOTP"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d9

    const-string v7, "TOTP"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_98

    :cond_d9
    sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$f;->npci_otp_title:I

    invoke-virtual {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(I)Ljava/lang/String;

    move-result-object v4

    iput v3, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/b;->h()Landroid/support/v4/app/l;

    move-result-object v2

    instance-of v2, v2, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;

    if-eqz v2, :cond_f8

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/b;->h()Landroid/support/v4/app/l;

    move-result-object v2

    check-cast v2, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;

    invoke-virtual {v2}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->n()Z

    move-result v2

    if-eqz v2, :cond_f8

    invoke-virtual {p0, v5}, Lorg/npci/upi/security/pinactivitycomponent/b;->c(I)V
    :try_end_f8
    .catch Ljava/lang/Exception; {:try_start_b1 .. :try_end_f8} :catch_a8

    :cond_f8
    move-object v2, v4

    goto :goto_98

    :cond_fa
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6

    sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$e;->fragment_atmpin:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 6

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ar:I

    if-nez v0, :cond_1d

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ar:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/c;

    invoke-interface {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/c;->d()Z

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ar:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ar:I

    :cond_1c
    :goto_1c
    return-void

    :cond_1d
    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ar:I

    if-ne v0, v2, :cond_8d

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputLength()I

    move-result v1

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/c;

    invoke-interface {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/c;->getInputValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eq v1, v0, :cond_51

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->npci_otp_title:I

    invoke-virtual {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/b;->b(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_1c

    :cond_51
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputLength()I

    move-result v1

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/c;

    invoke-interface {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/c;->getInputValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eq v1, v0, :cond_81

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->npci_atm_title:I

    invoke-virtual {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/b;->b(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_1c

    :cond_81
    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->at:Landroid/widget/ViewSwitcher;

    if-eqz v0, :cond_8d

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->at:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showNext()V

    iput v4, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ar:I

    goto :goto_1c

    :cond_8d
    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ar:I

    if-ne v0, v4, :cond_a6

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ar:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/c;

    invoke-interface {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/c;->d()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/b;->M()V

    goto/16 :goto_1c

    :cond_a6
    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/b;->M()V

    goto/16 :goto_1c
.end method

.method public a(ILjava/lang/String;)V
    .registers 7

    const/4 v3, 0x0

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_50

    iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    if-ne v0, p1, :cond_50

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    if-eqz v0, :cond_50

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->i:Ljava/util/Timer;

    invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(Ljava/util/Timer;)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-virtual {v0, v3}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Z)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3, v3}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;ZZ)V

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->g:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/b;->h()Landroid/support/v4/app/l;

    move-result-object v1

    sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$c;->ic_tick_ok:I

    invoke-static {v1, v2}, Landroid/support/v4/a/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Landroid/graphics/drawable/Drawable;Z)V

    :cond_50
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 3

    invoke-super {p0, p1, p2}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/b;->K()V

    invoke-direct {p0, p1}, Lorg/npci/upi/security/pinactivitycomponent/b;->a(Landroid/view/View;)V

    invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/b;->N()V

    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/String;)V
    .registers 3

    invoke-virtual {p0, p1, p2}, Lorg/npci/upi/security/pinactivitycomponent/b;->b(Landroid/view/View;Ljava/lang/String;)V

    return-void
.end method

.method public b(I)V
    .registers 3

    iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;

    if-nez v0, :cond_c

    iput p1, p0, Lorg/npci/upi/security/pinactivitycomponent/b;->ar:I

    :cond_c
    return-void
.end method
