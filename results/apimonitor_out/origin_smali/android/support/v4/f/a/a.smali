.class public final Landroid/support/v4/f/a/a;
.super Ljava/lang/Object;
.source "AccessibilityEventCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/f/a/a$b;,
        Landroid/support/v4/f/a/a$a;,
        Landroid/support/v4/f/a/a$c;,
        Landroid/support/v4/f/a/a$d;
    }
.end annotation


# static fields
.field private static final a:Landroid/support/v4/f/a/a$d;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_e

    new-instance v0, Landroid/support/v4/f/a/a$b;

    invoke-direct {v0}, Landroid/support/v4/f/a/a$b;-><init>()V

    sput-object v0, Landroid/support/v4/f/a/a;->a:Landroid/support/v4/f/a/a$d;

    :goto_d
    return-void

    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1c

    new-instance v0, Landroid/support/v4/f/a/a$a;

    invoke-direct {v0}, Landroid/support/v4/f/a/a$a;-><init>()V

    sput-object v0, Landroid/support/v4/f/a/a;->a:Landroid/support/v4/f/a/a$d;

    goto :goto_d

    :cond_1c
    new-instance v0, Landroid/support/v4/f/a/a$c;

    invoke-direct {v0}, Landroid/support/v4/f/a/a$c;-><init>()V

    sput-object v0, Landroid/support/v4/f/a/a;->a:Landroid/support/v4/f/a/a$d;

    goto :goto_d
.end method

.method public static a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/f/a/h;
    .registers 2

    new-instance v0, Landroid/support/v4/f/a/h;

    invoke-direct {v0, p0}, Landroid/support/v4/f/a/h;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method
