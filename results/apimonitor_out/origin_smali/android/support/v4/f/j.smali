.class Landroid/support/v4/f/j;
.super Ljava/lang/Object;
.source "LayoutInflaterCompatBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/f/j$a;
    }
.end annotation


# direct methods
.method static a(Landroid/view/LayoutInflater;)Landroid/support/v4/f/m;
    .registers 3

    invoke-virtual {p0}, Landroid/view/LayoutInflater;->getFactory()Landroid/view/LayoutInflater$Factory;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v4/f/j$a;

    if-eqz v1, :cond_d

    check-cast v0, Landroid/support/v4/f/j$a;

    iget-object v0, v0, Landroid/support/v4/f/j$a;->a:Landroid/support/v4/f/m;

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method static a(Landroid/view/LayoutInflater;Landroid/support/v4/f/m;)V
    .registers 3

    if-eqz p1, :cond_b

    new-instance v0, Landroid/support/v4/f/j$a;

    invoke-direct {v0, p1}, Landroid/support/v4/f/j$a;-><init>(Landroid/support/v4/f/m;)V

    :goto_7
    invoke-virtual {p0, v0}, Landroid/view/LayoutInflater;->setFactory(Landroid/view/LayoutInflater$Factory;)V

    return-void

    :cond_b
    const/4 v0, 0x0

    goto :goto_7
.end method
