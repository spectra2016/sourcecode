.class public final Landroid/support/v4/f/as;
.super Ljava/lang/Object;
.source "ViewParentCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/f/as$d;,
        Landroid/support/v4/f/as$c;,
        Landroid/support/v4/f/as$a;,
        Landroid/support/v4/f/as$e;,
        Landroid/support/v4/f/as$b;
    }
.end annotation


# static fields
.field static final a:Landroid/support/v4/f/as$b;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_e

    new-instance v0, Landroid/support/v4/f/as$d;

    invoke-direct {v0}, Landroid/support/v4/f/as$d;-><init>()V

    sput-object v0, Landroid/support/v4/f/as;->a:Landroid/support/v4/f/as$b;

    :goto_d
    return-void

    :cond_e
    const/16 v1, 0x13

    if-lt v0, v1, :cond_1a

    new-instance v0, Landroid/support/v4/f/as$c;

    invoke-direct {v0}, Landroid/support/v4/f/as$c;-><init>()V

    sput-object v0, Landroid/support/v4/f/as;->a:Landroid/support/v4/f/as$b;

    goto :goto_d

    :cond_1a
    const/16 v1, 0xe

    if-lt v0, v1, :cond_26

    new-instance v0, Landroid/support/v4/f/as$a;

    invoke-direct {v0}, Landroid/support/v4/f/as$a;-><init>()V

    sput-object v0, Landroid/support/v4/f/as;->a:Landroid/support/v4/f/as$b;

    goto :goto_d

    :cond_26
    new-instance v0, Landroid/support/v4/f/as$e;

    invoke-direct {v0}, Landroid/support/v4/f/as$e;-><init>()V

    sput-object v0, Landroid/support/v4/f/as;->a:Landroid/support/v4/f/as$b;

    goto :goto_d
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;)V
    .registers 3

    sget-object v0, Landroid/support/v4/f/as;->a:Landroid/support/v4/f/as$b;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/f/as$b;->a(Landroid/view/ViewParent;Landroid/view/View;)V

    return-void
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;IIII)V
    .registers 13

    sget-object v0, Landroid/support/v4/f/as;->a:Landroid/support/v4/f/as$b;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/f/as$b;->a(Landroid/view/ViewParent;Landroid/view/View;IIII)V

    return-void
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;II[I)V
    .registers 11

    sget-object v0, Landroid/support/v4/f/as;->a:Landroid/support/v4/f/as$b;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/f/as$b;->a(Landroid/view/ViewParent;Landroid/view/View;II[I)V

    return-void
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;FF)Z
    .registers 5

    sget-object v0, Landroid/support/v4/f/as;->a:Landroid/support/v4/f/as$b;

    invoke-interface {v0, p0, p1, p2, p3}, Landroid/support/v4/f/as$b;->a(Landroid/view/ViewParent;Landroid/view/View;FF)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z
    .registers 11

    sget-object v0, Landroid/support/v4/f/as;->a:Landroid/support/v4/f/as$b;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/f/as$b;->a(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z
    .registers 5

    sget-object v0, Landroid/support/v4/f/as;->a:Landroid/support/v4/f/as$b;

    invoke-interface {v0, p0, p1, p2, p3}, Landroid/support/v4/f/as$b;->a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V
    .registers 5

    sget-object v0, Landroid/support/v4/f/as;->a:Landroid/support/v4/f/as$b;

    invoke-interface {v0, p0, p1, p2, p3}, Landroid/support/v4/f/as$b;->b(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V

    return-void
.end method
