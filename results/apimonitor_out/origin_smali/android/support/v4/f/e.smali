.class public final Landroid/support/v4/f/e;
.super Ljava/lang/Object;
.source "GravityCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/f/e$c;,
        Landroid/support/v4/f/e$b;,
        Landroid/support/v4/f/e$a;
    }
.end annotation


# static fields
.field static final a:Landroid/support/v4/f/e$a;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_e

    new-instance v0, Landroid/support/v4/f/e$c;

    invoke-direct {v0}, Landroid/support/v4/f/e$c;-><init>()V

    sput-object v0, Landroid/support/v4/f/e;->a:Landroid/support/v4/f/e$a;

    :goto_d
    return-void

    :cond_e
    new-instance v0, Landroid/support/v4/f/e$b;

    invoke-direct {v0}, Landroid/support/v4/f/e$b;-><init>()V

    sput-object v0, Landroid/support/v4/f/e;->a:Landroid/support/v4/f/e$a;

    goto :goto_d
.end method

.method public static a(II)I
    .registers 3

    sget-object v0, Landroid/support/v4/f/e;->a:Landroid/support/v4/f/e$a;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/f/e$a;->a(II)I

    move-result v0

    return v0
.end method
