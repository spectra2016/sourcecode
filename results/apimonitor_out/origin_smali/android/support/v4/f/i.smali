.class public final Landroid/support/v4/f/i;
.super Ljava/lang/Object;
.source "LayoutInflaterCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/f/i$d;,
        Landroid/support/v4/f/i$c;,
        Landroid/support/v4/f/i$b;,
        Landroid/support/v4/f/i$a;
    }
.end annotation


# static fields
.field static final a:Landroid/support/v4/f/i$a;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_e

    new-instance v0, Landroid/support/v4/f/i$d;

    invoke-direct {v0}, Landroid/support/v4/f/i$d;-><init>()V

    sput-object v0, Landroid/support/v4/f/i;->a:Landroid/support/v4/f/i$a;

    :goto_d
    return-void

    :cond_e
    const/16 v1, 0xb

    if-lt v0, v1, :cond_1a

    new-instance v0, Landroid/support/v4/f/i$c;

    invoke-direct {v0}, Landroid/support/v4/f/i$c;-><init>()V

    sput-object v0, Landroid/support/v4/f/i;->a:Landroid/support/v4/f/i$a;

    goto :goto_d

    :cond_1a
    new-instance v0, Landroid/support/v4/f/i$b;

    invoke-direct {v0}, Landroid/support/v4/f/i$b;-><init>()V

    sput-object v0, Landroid/support/v4/f/i;->a:Landroid/support/v4/f/i$a;

    goto :goto_d
.end method

.method public static a(Landroid/view/LayoutInflater;)Landroid/support/v4/f/m;
    .registers 2

    sget-object v0, Landroid/support/v4/f/i;->a:Landroid/support/v4/f/i$a;

    invoke-interface {v0, p0}, Landroid/support/v4/f/i$a;->a(Landroid/view/LayoutInflater;)Landroid/support/v4/f/m;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/support/v4/f/m;)V
    .registers 3

    sget-object v0, Landroid/support/v4/f/i;->a:Landroid/support/v4/f/i$a;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/f/i$a;->a(Landroid/view/LayoutInflater;Landroid/support/v4/f/m;)V

    return-void
.end method
