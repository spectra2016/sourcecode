.class Landroid/support/v4/f/au$b;
.super Landroid/support/v4/f/au$a;
.source "ViewPropertyAnimatorCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/f/au;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/f/au$b$a;
    }
.end annotation


# instance fields
.field b:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 2

    invoke-direct {p0}, Landroid/support/v4/f/au$a;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/f/au$b;->b:Ljava/util/WeakHashMap;

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/f/au;Landroid/view/View;)J
    .registers 5

    invoke-static {p2}, Landroid/support/v4/f/av;->a(Landroid/view/View;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Landroid/support/v4/f/au;Landroid/view/View;F)V
    .registers 4

    invoke-static {p2, p3}, Landroid/support/v4/f/av;->a(Landroid/view/View;F)V

    return-void
.end method

.method public a(Landroid/support/v4/f/au;Landroid/view/View;J)V
    .registers 6

    invoke-static {p2, p3, p4}, Landroid/support/v4/f/av;->a(Landroid/view/View;J)V

    return-void
.end method

.method public a(Landroid/support/v4/f/au;Landroid/view/View;Landroid/support/v4/f/ay;)V
    .registers 5

    const/high16 v0, 0x7e00

    invoke-virtual {p2, v0, p3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    new-instance v0, Landroid/support/v4/f/au$b$a;

    invoke-direct {v0, p1}, Landroid/support/v4/f/au$b$a;-><init>(Landroid/support/v4/f/au;)V

    invoke-static {p2, v0}, Landroid/support/v4/f/av;->a(Landroid/view/View;Landroid/support/v4/f/ay;)V

    return-void
.end method

.method public a(Landroid/support/v4/f/au;Landroid/view/View;Landroid/view/animation/Interpolator;)V
    .registers 4

    invoke-static {p2, p3}, Landroid/support/v4/f/av;->a(Landroid/view/View;Landroid/view/animation/Interpolator;)V

    return-void
.end method

.method public b(Landroid/support/v4/f/au;Landroid/view/View;)V
    .registers 3

    invoke-static {p2}, Landroid/support/v4/f/av;->b(Landroid/view/View;)V

    return-void
.end method

.method public b(Landroid/support/v4/f/au;Landroid/view/View;F)V
    .registers 4

    invoke-static {p2, p3}, Landroid/support/v4/f/av;->b(Landroid/view/View;F)V

    return-void
.end method

.method public b(Landroid/support/v4/f/au;Landroid/view/View;J)V
    .registers 6

    invoke-static {p2, p3, p4}, Landroid/support/v4/f/av;->b(Landroid/view/View;J)V

    return-void
.end method

.method public c(Landroid/support/v4/f/au;Landroid/view/View;)V
    .registers 3

    invoke-static {p2}, Landroid/support/v4/f/av;->c(Landroid/view/View;)V

    return-void
.end method

.method public c(Landroid/support/v4/f/au;Landroid/view/View;F)V
    .registers 4

    invoke-static {p2, p3}, Landroid/support/v4/f/av;->c(Landroid/view/View;F)V

    return-void
.end method

.method public d(Landroid/support/v4/f/au;Landroid/view/View;F)V
    .registers 4

    invoke-static {p2, p3}, Landroid/support/v4/f/av;->d(Landroid/view/View;F)V

    return-void
.end method
