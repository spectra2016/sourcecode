.class public final Landroid/support/v4/f/aq;
.super Ljava/lang/Object;
.source "ViewConfigurationCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/f/aq$d;,
        Landroid/support/v4/f/aq$c;,
        Landroid/support/v4/f/aq$b;,
        Landroid/support/v4/f/aq$a;,
        Landroid/support/v4/f/aq$e;
    }
.end annotation


# static fields
.field static final a:Landroid/support/v4/f/aq$e;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_e

    new-instance v0, Landroid/support/v4/f/aq$d;

    invoke-direct {v0}, Landroid/support/v4/f/aq$d;-><init>()V

    sput-object v0, Landroid/support/v4/f/aq;->a:Landroid/support/v4/f/aq$e;

    :goto_d
    return-void

    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1c

    new-instance v0, Landroid/support/v4/f/aq$c;

    invoke-direct {v0}, Landroid/support/v4/f/aq$c;-><init>()V

    sput-object v0, Landroid/support/v4/f/aq;->a:Landroid/support/v4/f/aq$e;

    goto :goto_d

    :cond_1c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_2a

    new-instance v0, Landroid/support/v4/f/aq$b;

    invoke-direct {v0}, Landroid/support/v4/f/aq$b;-><init>()V

    sput-object v0, Landroid/support/v4/f/aq;->a:Landroid/support/v4/f/aq$e;

    goto :goto_d

    :cond_2a
    new-instance v0, Landroid/support/v4/f/aq$a;

    invoke-direct {v0}, Landroid/support/v4/f/aq$a;-><init>()V

    sput-object v0, Landroid/support/v4/f/aq;->a:Landroid/support/v4/f/aq$e;

    goto :goto_d
.end method

.method public static a(Landroid/view/ViewConfiguration;)Z
    .registers 2

    sget-object v0, Landroid/support/v4/f/aq;->a:Landroid/support/v4/f/aq$e;

    invoke-interface {v0, p0}, Landroid/support/v4/f/aq$e;->a(Landroid/view/ViewConfiguration;)Z

    move-result v0

    return v0
.end method
