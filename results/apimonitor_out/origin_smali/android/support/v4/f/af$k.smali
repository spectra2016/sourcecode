.class Landroid/support/v4/f/af$k;
.super Landroid/support/v4/f/af$j;
.source "ViewCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/f/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "k"
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    invoke-direct {p0}, Landroid/support/v4/f/af$j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/support/v4/f/bb;)Landroid/support/v4/f/bb;
    .registers 4

    invoke-static {p1, p2}, Landroid/support/v4/f/ao;->a(Landroid/view/View;Landroid/support/v4/f/bb;)Landroid/support/v4/f/bb;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
    .registers 3

    invoke-static {p1, p2}, Landroid/support/v4/f/ao;->a(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
    .registers 3

    invoke-static {p1, p2}, Landroid/support/v4/f/ao;->a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V

    return-void
.end method

.method public a(Landroid/view/View;Landroid/support/v4/f/aa;)V
    .registers 3

    invoke-static {p1, p2}, Landroid/support/v4/f/ao;->a(Landroid/view/View;Landroid/support/v4/f/aa;)V

    return-void
.end method

.method public d(Landroid/view/View;F)V
    .registers 3

    invoke-static {p1, p2}, Landroid/support/v4/f/ao;->a(Landroid/view/View;F)V

    return-void
.end method

.method public o(Landroid/view/View;)V
    .registers 2

    invoke-static {p1}, Landroid/support/v4/f/ao;->a(Landroid/view/View;)V

    return-void
.end method

.method public q(Landroid/view/View;)Landroid/content/res/ColorStateList;
    .registers 3

    invoke-static {p1}, Landroid/support/v4/f/ao;->b(Landroid/view/View;)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method public r(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;
    .registers 3

    invoke-static {p1}, Landroid/support/v4/f/ao;->c(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    return-object v0
.end method

.method public s(Landroid/view/View;)V
    .registers 2

    invoke-static {p1}, Landroid/support/v4/f/ao;->d(Landroid/view/View;)V

    return-void
.end method
