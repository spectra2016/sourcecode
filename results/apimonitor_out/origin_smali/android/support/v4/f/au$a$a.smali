.class Landroid/support/v4/f/au$a$a;
.super Ljava/lang/Object;
.source "ViewPropertyAnimatorCompat.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/f/au$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field b:Landroid/support/v4/f/au;

.field final synthetic c:Landroid/support/v4/f/au$a;


# direct methods
.method private constructor <init>(Landroid/support/v4/f/au$a;Landroid/support/v4/f/au;Landroid/view/View;)V
    .registers 5

    iput-object p1, p0, Landroid/support/v4/f/au$a$a;->c:Landroid/support/v4/f/au$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/v4/f/au$a$a;->a:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Landroid/support/v4/f/au$a$a;->b:Landroid/support/v4/f/au;

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v4/f/au$a;Landroid/support/v4/f/au;Landroid/view/View;Landroid/support/v4/f/au$1;)V
    .registers 5

    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/f/au$a$a;-><init>(Landroid/support/v4/f/au$a;Landroid/support/v4/f/au;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    iget-object v0, p0, Landroid/support/v4/f/au$a$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_11

    iget-object v1, p0, Landroid/support/v4/f/au$a$a;->c:Landroid/support/v4/f/au$a;

    iget-object v2, p0, Landroid/support/v4/f/au$a$a;->b:Landroid/support/v4/f/au;

    invoke-static {v1, v2, v0}, Landroid/support/v4/f/au$a;->a(Landroid/support/v4/f/au$a;Landroid/support/v4/f/au;Landroid/view/View;)V

    :cond_11
    return-void
.end method
