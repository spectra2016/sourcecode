.class Landroid/support/v4/f/af$f;
.super Landroid/support/v4/f/af$d;
.source "ViewCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/f/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "f"
.end annotation


# static fields
.field static b:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const/4 v0, 0x0

    sput-boolean v0, Landroid/support/v4/f/af$f;->b:Z

    return-void
.end method

.method constructor <init>()V
    .registers 1

    invoke-direct {p0}, Landroid/support/v4/f/af$d;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/support/v4/f/a;)V
    .registers 4

    if-nez p2, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-static {p1, v0}, Landroid/support/v4/f/aj;->a(Landroid/view/View;Ljava/lang/Object;)V

    return-void

    :cond_7
    invoke-virtual {p2}, Landroid/support/v4/f/a;->a()Ljava/lang/Object;

    move-result-object v0

    goto :goto_3
.end method

.method public a(Landroid/view/View;I)Z
    .registers 4

    invoke-static {p1, p2}, Landroid/support/v4/f/aj;->a(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public m(Landroid/view/View;)Landroid/support/v4/f/au;
    .registers 4

    iget-object v0, p0, Landroid/support/v4/f/af$f;->a:Ljava/util/WeakHashMap;

    if-nez v0, :cond_b

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Landroid/support/v4/f/af$f;->a:Ljava/util/WeakHashMap;

    :cond_b
    iget-object v0, p0, Landroid/support/v4/f/af$f;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/f/au;

    if-nez v0, :cond_1f

    new-instance v0, Landroid/support/v4/f/au;

    invoke-direct {v0, p1}, Landroid/support/v4/f/au;-><init>(Landroid/view/View;)V

    iget-object v1, p0, Landroid/support/v4/f/af$f;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1f
    return-object v0
.end method
