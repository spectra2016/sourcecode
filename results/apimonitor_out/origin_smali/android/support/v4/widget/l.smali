.class public final Landroid/support/v4/widget/l;
.super Ljava/lang/Object;
.source "ListViewCompat.java"


# direct methods
.method public static a(Landroid/widget/ListView;I)V
    .registers 4

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_a

    invoke-static {p0, p1}, Landroid/support/v4/widget/n;->a(Landroid/widget/ListView;I)V

    :goto_9
    return-void

    :cond_a
    invoke-static {p0, p1}, Landroid/support/v4/widget/m;->a(Landroid/widget/ListView;I)V

    goto :goto_9
.end method
