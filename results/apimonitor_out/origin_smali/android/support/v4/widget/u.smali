.class public final Landroid/support/v4/widget/u;
.super Ljava/lang/Object;
.source "ScrollerCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/widget/u$d;,
        Landroid/support/v4/widget/u$c;,
        Landroid/support/v4/widget/u$b;,
        Landroid/support/v4/widget/u$a;
    }
.end annotation


# instance fields
.field a:Ljava/lang/Object;

.field b:Landroid/support/v4/widget/u$a;


# direct methods
.method private constructor <init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xe

    if-lt p1, v0, :cond_17

    new-instance v0, Landroid/support/v4/widget/u$d;

    invoke-direct {v0}, Landroid/support/v4/widget/u$d;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    :goto_e
    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    invoke-interface {v0, p2, p3}, Landroid/support/v4/widget/u$a;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/u;->a:Ljava/lang/Object;

    return-void

    :cond_17
    const/16 v0, 0x9

    if-lt p1, v0, :cond_23

    new-instance v0, Landroid/support/v4/widget/u$c;

    invoke-direct {v0}, Landroid/support/v4/widget/u$c;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    goto :goto_e

    :cond_23
    new-instance v0, Landroid/support/v4/widget/u$b;

    invoke-direct {v0}, Landroid/support/v4/widget/u$b;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    goto :goto_e
.end method

.method public static a(Landroid/content/Context;)Landroid/support/v4/widget/u;
    .registers 2

    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/support/v4/widget/u;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/u;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/u;
    .registers 4

    new-instance v0, Landroid/support/v4/widget/u;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {v0, v1, p0, p1}, Landroid/support/v4/widget/u;-><init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V

    return-object v0
.end method


# virtual methods
.method public a(IIII)V
    .registers 11

    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    iget-object v1, p0, Landroid/support/v4/widget/u;->a:Ljava/lang/Object;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/widget/u$a;->a(Ljava/lang/Object;IIII)V

    return-void
.end method

.method public a(IIIIIIII)V
    .registers 19

    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    iget-object v1, p0, Landroid/support/v4/widget/u;->a:Ljava/lang/Object;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-interface/range {v0 .. v9}, Landroid/support/v4/widget/u$a;->a(Ljava/lang/Object;IIIIIIII)V

    return-void
.end method

.method public a(IIIIIIIIII)V
    .registers 23

    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    iget-object v1, p0, Landroid/support/v4/widget/u;->a:Ljava/lang/Object;

    move v2, p1

    move v3, p2

    move v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    invoke-interface/range {v0 .. v11}, Landroid/support/v4/widget/u$a;->a(Ljava/lang/Object;IIIIIIIIII)V

    return-void
.end method

.method public a()Z
    .registers 3

    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    iget-object v1, p0, Landroid/support/v4/widget/u;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/u$a;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(IIIIII)Z
    .registers 15

    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    iget-object v1, p0, Landroid/support/v4/widget/u;->a:Ljava/lang/Object;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-interface/range {v0 .. v7}, Landroid/support/v4/widget/u$a;->a(Ljava/lang/Object;IIIIII)Z

    move-result v0

    return v0
.end method

.method public b()I
    .registers 3

    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    iget-object v1, p0, Landroid/support/v4/widget/u;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/u$a;->b(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public c()I
    .registers 3

    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    iget-object v1, p0, Landroid/support/v4/widget/u;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/u$a;->c(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public d()I
    .registers 3

    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    iget-object v1, p0, Landroid/support/v4/widget/u;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/u$a;->g(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public e()F
    .registers 3

    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    iget-object v1, p0, Landroid/support/v4/widget/u;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/u$a;->d(Ljava/lang/Object;)F

    move-result v0

    return v0
.end method

.method public f()Z
    .registers 3

    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    iget-object v1, p0, Landroid/support/v4/widget/u;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/u$a;->e(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public g()V
    .registers 3

    iget-object v0, p0, Landroid/support/v4/widget/u;->b:Landroid/support/v4/widget/u$a;

    iget-object v1, p0, Landroid/support/v4/widget/u;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/u$a;->f(Ljava/lang/Object;)V

    return-void
.end method
