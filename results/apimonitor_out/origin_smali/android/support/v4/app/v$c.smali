.class Landroid/support/v4/app/v$c;
.super Landroid/support/v4/app/v$b;
.source "NavUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/app/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "c"
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    invoke-direct {p0}, Landroid/support/v4/app/v$b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;)Landroid/content/Intent;
    .registers 3

    invoke-static {p1}, Landroid/support/v4/app/w;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_a

    invoke-virtual {p0, p1}, Landroid/support/v4/app/v$c;->b(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    :cond_a
    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/content/pm/ActivityInfo;)Ljava/lang/String;
    .registers 4

    invoke-static {p2}, Landroid/support/v4/app/w;->a(Landroid/content/pm/ActivityInfo;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_a

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/v$b;->a(Landroid/content/Context;Landroid/content/pm/ActivityInfo;)Ljava/lang/String;

    move-result-object v0

    :cond_a
    return-object v0
.end method

.method public a(Landroid/app/Activity;Landroid/content/Intent;)Z
    .registers 4

    invoke-static {p1, p2}, Landroid/support/v4/app/w;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method b(Landroid/app/Activity;)Landroid/content/Intent;
    .registers 3

    invoke-super {p0, p1}, Landroid/support/v4/app/v$b;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/app/Activity;Landroid/content/Intent;)V
    .registers 3

    invoke-static {p1, p2}, Landroid/support/v4/app/w;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method
