.class public Landroid/support/v4/app/aa;
.super Ljava/lang/Object;
.source "NotificationCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/app/aa$a;,
        Landroid/support/v4/app/aa$f;,
        Landroid/support/v4/app/aa$c;,
        Landroid/support/v4/app/aa$b;,
        Landroid/support/v4/app/aa$p;,
        Landroid/support/v4/app/aa$d;,
        Landroid/support/v4/app/aa$i;,
        Landroid/support/v4/app/aa$h;,
        Landroid/support/v4/app/aa$o;,
        Landroid/support/v4/app/aa$n;,
        Landroid/support/v4/app/aa$m;,
        Landroid/support/v4/app/aa$l;,
        Landroid/support/v4/app/aa$k;,
        Landroid/support/v4/app/aa$j;,
        Landroid/support/v4/app/aa$e;,
        Landroid/support/v4/app/aa$g;
    }
.end annotation


# static fields
.field private static final a:Landroid/support/v4/app/aa$g;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_e

    new-instance v0, Landroid/support/v4/app/aa$i;

    invoke-direct {v0}, Landroid/support/v4/app/aa$i;-><init>()V

    sput-object v0, Landroid/support/v4/app/aa;->a:Landroid/support/v4/app/aa$g;

    :goto_d
    return-void

    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_1c

    new-instance v0, Landroid/support/v4/app/aa$h;

    invoke-direct {v0}, Landroid/support/v4/app/aa$h;-><init>()V

    sput-object v0, Landroid/support/v4/app/aa;->a:Landroid/support/v4/app/aa$g;

    goto :goto_d

    :cond_1c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2a

    new-instance v0, Landroid/support/v4/app/aa$o;

    invoke-direct {v0}, Landroid/support/v4/app/aa$o;-><init>()V

    sput-object v0, Landroid/support/v4/app/aa;->a:Landroid/support/v4/app/aa$g;

    goto :goto_d

    :cond_2a
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_38

    new-instance v0, Landroid/support/v4/app/aa$n;

    invoke-direct {v0}, Landroid/support/v4/app/aa$n;-><init>()V

    sput-object v0, Landroid/support/v4/app/aa;->a:Landroid/support/v4/app/aa$g;

    goto :goto_d

    :cond_38
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_46

    new-instance v0, Landroid/support/v4/app/aa$m;

    invoke-direct {v0}, Landroid/support/v4/app/aa$m;-><init>()V

    sput-object v0, Landroid/support/v4/app/aa;->a:Landroid/support/v4/app/aa$g;

    goto :goto_d

    :cond_46
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_54

    new-instance v0, Landroid/support/v4/app/aa$l;

    invoke-direct {v0}, Landroid/support/v4/app/aa$l;-><init>()V

    sput-object v0, Landroid/support/v4/app/aa;->a:Landroid/support/v4/app/aa$g;

    goto :goto_d

    :cond_54
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_62

    new-instance v0, Landroid/support/v4/app/aa$k;

    invoke-direct {v0}, Landroid/support/v4/app/aa$k;-><init>()V

    sput-object v0, Landroid/support/v4/app/aa;->a:Landroid/support/v4/app/aa$g;

    goto :goto_d

    :cond_62
    new-instance v0, Landroid/support/v4/app/aa$j;

    invoke-direct {v0}, Landroid/support/v4/app/aa$j;-><init>()V

    sput-object v0, Landroid/support/v4/app/aa;->a:Landroid/support/v4/app/aa$g;

    goto :goto_d
.end method

.method static synthetic a()Landroid/support/v4/app/aa$g;
    .registers 1

    sget-object v0, Landroid/support/v4/app/aa;->a:Landroid/support/v4/app/aa$g;

    return-object v0
.end method

.method static synthetic a(Landroid/support/v4/app/y;Ljava/util/ArrayList;)V
    .registers 2

    invoke-static {p0, p1}, Landroid/support/v4/app/aa;->b(Landroid/support/v4/app/y;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic a(Landroid/support/v4/app/z;Landroid/support/v4/app/aa$p;)V
    .registers 2

    invoke-static {p0, p1}, Landroid/support/v4/app/aa;->b(Landroid/support/v4/app/z;Landroid/support/v4/app/aa$p;)V

    return-void
.end method

.method private static b(Landroid/support/v4/app/y;Ljava/util/ArrayList;)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/y;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/aa$a;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/aa$a;

    invoke-interface {p0, v0}, Landroid/support/v4/app/y;->a(Landroid/support/v4/app/ad$a;)V

    goto :goto_4

    :cond_14
    return-void
.end method

.method private static b(Landroid/support/v4/app/z;Landroid/support/v4/app/aa$p;)V
    .registers 9

    if-eqz p1, :cond_13

    instance-of v0, p1, Landroid/support/v4/app/aa$c;

    if-eqz v0, :cond_14

    check-cast p1, Landroid/support/v4/app/aa$c;

    iget-object v0, p1, Landroid/support/v4/app/aa$c;->e:Ljava/lang/CharSequence;

    iget-boolean v1, p1, Landroid/support/v4/app/aa$c;->g:Z

    iget-object v2, p1, Landroid/support/v4/app/aa$c;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, Landroid/support/v4/app/aa$c;->a:Ljava/lang/CharSequence;

    invoke-static {p0, v0, v1, v2, v3}, Landroid/support/v4/app/ah;->a(Landroid/support/v4/app/z;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_13
    :goto_13
    return-void

    :cond_14
    instance-of v0, p1, Landroid/support/v4/app/aa$f;

    if-eqz v0, :cond_26

    check-cast p1, Landroid/support/v4/app/aa$f;

    iget-object v0, p1, Landroid/support/v4/app/aa$f;->e:Ljava/lang/CharSequence;

    iget-boolean v1, p1, Landroid/support/v4/app/aa$f;->g:Z

    iget-object v2, p1, Landroid/support/v4/app/aa$f;->f:Ljava/lang/CharSequence;

    iget-object v3, p1, Landroid/support/v4/app/aa$f;->a:Ljava/util/ArrayList;

    invoke-static {p0, v0, v1, v2, v3}, Landroid/support/v4/app/ah;->a(Landroid/support/v4/app/z;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V

    goto :goto_13

    :cond_26
    instance-of v0, p1, Landroid/support/v4/app/aa$b;

    if-eqz v0, :cond_13

    check-cast p1, Landroid/support/v4/app/aa$b;

    iget-object v1, p1, Landroid/support/v4/app/aa$b;->e:Ljava/lang/CharSequence;

    iget-boolean v2, p1, Landroid/support/v4/app/aa$b;->g:Z

    iget-object v3, p1, Landroid/support/v4/app/aa$b;->f:Ljava/lang/CharSequence;

    iget-object v4, p1, Landroid/support/v4/app/aa$b;->a:Landroid/graphics/Bitmap;

    iget-object v5, p1, Landroid/support/v4/app/aa$b;->b:Landroid/graphics/Bitmap;

    iget-boolean v6, p1, Landroid/support/v4/app/aa$b;->c:Z

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/support/v4/app/ah;->a(Landroid/support/v4/app/z;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    goto :goto_13
.end method
