.class final Landroid/support/v4/app/u$a;
.super Ljava/lang/Object;
.source "LoaderManager.java"

# interfaces
.implements Landroid/support/v4/a/h$a;
.implements Landroid/support/v4/a/h$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/app/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/a/h$a",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Landroid/support/v4/a/h$b",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final a:I

.field final b:Landroid/os/Bundle;

.field c:Landroid/support/v4/app/t$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/t$a",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field d:Landroid/support/v4/a/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/a/h",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field e:Z

.field f:Z

.field g:Ljava/lang/Object;

.field h:Z

.field i:Z

.field j:Z

.field k:Z

.field l:Z

.field m:Z

.field n:Landroid/support/v4/app/u$a;

.field final synthetic o:Landroid/support/v4/app/u;


# virtual methods
.method a()V
    .registers 5

    const/4 v3, 0x1

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->i:Z

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->j:Z

    if-eqz v0, :cond_c

    iput-boolean v3, p0, Landroid/support/v4/app/u$a;->h:Z

    :cond_b
    :goto_b
    return-void

    :cond_c
    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->h:Z

    if-nez v0, :cond_b

    iput-boolean v3, p0, Landroid/support/v4/app/u$a;->h:Z

    sget-boolean v0, Landroid/support/v4/app/u;->a:Z

    if-eqz v0, :cond_2e

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  Starting: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2e
    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    if-nez v0, :cond_42

    iget-object v0, p0, Landroid/support/v4/app/u$a;->c:Landroid/support/v4/app/t$a;

    if-eqz v0, :cond_42

    iget-object v0, p0, Landroid/support/v4/app/u$a;->c:Landroid/support/v4/app/t$a;

    iget v1, p0, Landroid/support/v4/app/u$a;->a:I

    iget-object v2, p0, Landroid/support/v4/app/u$a;->b:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2}, Landroid/support/v4/app/t$a;->a(ILandroid/os/Bundle;)Landroid/support/v4/a/h;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    :cond_42
    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isMemberClass()Z

    move-result v0

    if-eqz v0, :cond_7d

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    if-nez v0, :cond_7d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Object returned from onCreateLoader must not be a non-static inner member class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7d
    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->m:Z

    if-nez v0, :cond_8f

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    iget v1, p0, Landroid/support/v4/app/u$a;->a:I

    invoke-virtual {v0, v1, p0}, Landroid/support/v4/a/h;->a(ILandroid/support/v4/a/h$b;)V

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-virtual {v0, p0}, Landroid/support/v4/a/h;->a(Landroid/support/v4/a/h$a;)V

    iput-boolean v3, p0, Landroid/support/v4/app/u$a;->m:Z

    :cond_8f
    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-virtual {v0}, Landroid/support/v4/a/h;->a()V

    goto/16 :goto_b
.end method

.method a(Landroid/support/v4/a/h;Ljava/lang/Object;)V
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/a/h",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Landroid/support/v4/app/u$a;->c:Landroid/support/v4/app/t$a;

    if-eqz v0, :cond_68

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v1}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v1

    if-eqz v1, :cond_7d

    iget-object v0, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v0}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v4/app/o;->d:Landroid/support/v4/app/q;

    iget-object v0, v0, Landroid/support/v4/app/q;->v:Ljava/lang/String;

    iget-object v1, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v1}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v1

    iget-object v1, v1, Landroid/support/v4/app/o;->d:Landroid/support/v4/app/q;

    const-string v2, "onLoadFinished"

    iput-object v2, v1, Landroid/support/v4/app/q;->v:Ljava/lang/String;

    move-object v1, v0

    :goto_24
    :try_start_24
    sget-boolean v0, Landroid/support/v4/app/u;->a:Z

    if-eqz v0, :cond_4e

    const-string v0, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  onLoadFinished in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, p2}, Landroid/support/v4/a/h;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4e
    iget-object v0, p0, Landroid/support/v4/app/u$a;->c:Landroid/support/v4/app/t$a;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/app/t$a;->a(Landroid/support/v4/a/h;Ljava/lang/Object;)V
    :try_end_53
    .catchall {:try_start_24 .. :try_end_53} :catchall_69

    iget-object v0, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v0}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v0

    if-eqz v0, :cond_65

    iget-object v0, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v0}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v4/app/o;->d:Landroid/support/v4/app/q;

    iput-object v1, v0, Landroid/support/v4/app/q;->v:Ljava/lang/String;

    :cond_65
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/u$a;->f:Z

    :cond_68
    return-void

    :catchall_69
    move-exception v0

    iget-object v2, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v2}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v2

    if-eqz v2, :cond_7c

    iget-object v2, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v2}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v2

    iget-object v2, v2, Landroid/support/v4/app/o;->d:Landroid/support/v4/app/q;

    iput-object v1, v2, Landroid/support/v4/app/q;->v:Ljava/lang/String;

    :cond_7c
    throw v0

    :cond_7d
    move-object v1, v0

    goto :goto_24
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mId="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/u$a;->a:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, " mArgs="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/u$a;->b:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCallbacks="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/u$a;->c:Landroid/support/v4/app/t$a;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mLoader="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    if-eqz v0, :cond_4d

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/support/v4/a/h;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :cond_4d
    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->e:Z

    if-nez v0, :cond_55

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->f:Z

    if-eqz v0, :cond_79

    :cond_55
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHaveData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->e:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, "  mDeliveredData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->f:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mData="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/u$a;->g:Ljava/lang/Object;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_79
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->h:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mReportNextStart="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->k:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->l:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mRetaining="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->i:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mRetainingStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->j:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mListenerRegistered="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->m:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    iget-object v0, p0, Landroid/support/v4/app/u$a;->n:Landroid/support/v4/app/u$a;

    if-eqz v0, :cond_e9

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Pending Loader "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/u$a;->n:Landroid/support/v4/app/u$a;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/u$a;->n:Landroid/support/v4/app/u$a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/support/v4/app/u$a;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :cond_e9
    return-void
.end method

.method b()V
    .registers 4

    sget-boolean v0, Landroid/support/v4/app/u;->a:Z

    if-eqz v0, :cond_1c

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  Retaining: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1c
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/u$a;->i:Z

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->h:Z

    iput-boolean v0, p0, Landroid/support/v4/app/u$a;->j:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/u$a;->h:Z

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/u$a;->c:Landroid/support/v4/app/t$a;

    return-void
.end method

.method c()V
    .registers 4

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->i:Z

    if-eqz v0, :cond_30

    sget-boolean v0, Landroid/support/v4/app/u;->a:Z

    if-eqz v0, :cond_20

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  Finished Retaining: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_20
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/u$a;->i:Z

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->h:Z

    iget-boolean v1, p0, Landroid/support/v4/app/u$a;->j:Z

    if-eq v0, v1, :cond_30

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->h:Z

    if-nez v0, :cond_30

    invoke-virtual {p0}, Landroid/support/v4/app/u$a;->e()V

    :cond_30
    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->h:Z

    if-eqz v0, :cond_43

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->e:Z

    if-eqz v0, :cond_43

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->k:Z

    if-nez v0, :cond_43

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    iget-object v1, p0, Landroid/support/v4/app/u$a;->g:Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/u$a;->a(Landroid/support/v4/a/h;Ljava/lang/Object;)V

    :cond_43
    return-void
.end method

.method d()V
    .registers 3

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->h:Z

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->k:Z

    if-eqz v0, :cond_16

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/u$a;->k:Z

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->e:Z

    if-eqz v0, :cond_16

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    iget-object v1, p0, Landroid/support/v4/app/u$a;->g:Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/u$a;->a(Landroid/support/v4/a/h;Ljava/lang/Object;)V

    :cond_16
    return-void
.end method

.method e()V
    .registers 5

    const/4 v3, 0x0

    sget-boolean v0, Landroid/support/v4/app/u;->a:Z

    if-eqz v0, :cond_1d

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  Stopping: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1d
    iput-boolean v3, p0, Landroid/support/v4/app/u$a;->h:Z

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->i:Z

    if-nez v0, :cond_3c

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    if-eqz v0, :cond_3c

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->m:Z

    if-eqz v0, :cond_3c

    iput-boolean v3, p0, Landroid/support/v4/app/u$a;->m:Z

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-virtual {v0, p0}, Landroid/support/v4/a/h;->a(Landroid/support/v4/a/h$b;)V

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-virtual {v0, p0}, Landroid/support/v4/a/h;->b(Landroid/support/v4/a/h$a;)V

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-virtual {v0}, Landroid/support/v4/a/h;->c()V

    :cond_3c
    return-void
.end method

.method f()V
    .registers 6

    const/4 v2, 0x0

    const/4 v4, 0x0

    sget-boolean v0, Landroid/support/v4/app/u;->a:Z

    if-eqz v0, :cond_1e

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  Destroying: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1e
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/u$a;->l:Z

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->f:Z

    iput-boolean v4, p0, Landroid/support/v4/app/u$a;->f:Z

    iget-object v1, p0, Landroid/support/v4/app/u$a;->c:Landroid/support/v4/app/t$a;

    if-eqz v1, :cond_87

    iget-object v1, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    if-eqz v1, :cond_87

    iget-boolean v1, p0, Landroid/support/v4/app/u$a;->e:Z

    if-eqz v1, :cond_87

    if-eqz v0, :cond_87

    sget-boolean v0, Landroid/support/v4/app/u;->a:Z

    if-eqz v0, :cond_4f

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  Reseting: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4f
    iget-object v0, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v0}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v0

    if-eqz v0, :cond_c4

    iget-object v0, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v0}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v4/app/o;->d:Landroid/support/v4/app/q;

    iget-object v0, v0, Landroid/support/v4/app/q;->v:Ljava/lang/String;

    iget-object v1, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v1}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v1

    iget-object v1, v1, Landroid/support/v4/app/o;->d:Landroid/support/v4/app/q;

    const-string v3, "onLoaderReset"

    iput-object v3, v1, Landroid/support/v4/app/q;->v:Ljava/lang/String;

    move-object v1, v0

    :goto_6e
    :try_start_6e
    iget-object v0, p0, Landroid/support/v4/app/u$a;->c:Landroid/support/v4/app/t$a;

    iget-object v3, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-interface {v0, v3}, Landroid/support/v4/app/t$a;->a(Landroid/support/v4/a/h;)V
    :try_end_75
    .catchall {:try_start_6e .. :try_end_75} :catchall_b0

    iget-object v0, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v0}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v0

    if-eqz v0, :cond_87

    iget-object v0, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v0}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v4/app/o;->d:Landroid/support/v4/app/q;

    iput-object v1, v0, Landroid/support/v4/app/q;->v:Ljava/lang/String;

    :cond_87
    iput-object v2, p0, Landroid/support/v4/app/u$a;->c:Landroid/support/v4/app/t$a;

    iput-object v2, p0, Landroid/support/v4/app/u$a;->g:Ljava/lang/Object;

    iput-boolean v4, p0, Landroid/support/v4/app/u$a;->e:Z

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    if-eqz v0, :cond_a6

    iget-boolean v0, p0, Landroid/support/v4/app/u$a;->m:Z

    if-eqz v0, :cond_a1

    iput-boolean v4, p0, Landroid/support/v4/app/u$a;->m:Z

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-virtual {v0, p0}, Landroid/support/v4/a/h;->a(Landroid/support/v4/a/h$b;)V

    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-virtual {v0, p0}, Landroid/support/v4/a/h;->b(Landroid/support/v4/a/h$a;)V

    :cond_a1
    iget-object v0, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-virtual {v0}, Landroid/support/v4/a/h;->e()V

    :cond_a6
    iget-object v0, p0, Landroid/support/v4/app/u$a;->n:Landroid/support/v4/app/u$a;

    if-eqz v0, :cond_af

    iget-object v0, p0, Landroid/support/v4/app/u$a;->n:Landroid/support/v4/app/u$a;

    invoke-virtual {v0}, Landroid/support/v4/app/u$a;->f()V

    :cond_af
    return-void

    :catchall_b0
    move-exception v0

    iget-object v2, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v2}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v2

    if-eqz v2, :cond_c3

    iget-object v2, p0, Landroid/support/v4/app/u$a;->o:Landroid/support/v4/app/u;

    invoke-static {v2}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/u;)Landroid/support/v4/app/o;

    move-result-object v2

    iget-object v2, v2, Landroid/support/v4/app/o;->d:Landroid/support/v4/app/q;

    iput-object v1, v2, Landroid/support/v4/app/q;->v:Ljava/lang/String;

    :cond_c3
    throw v0

    :cond_c4
    move-object v1, v2

    goto :goto_6e
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "LoaderInfo{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Landroid/support/v4/app/u$a;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroid/support/v4/app/u$a;->d:Landroid/support/v4/a/h;

    invoke-static {v1, v0}, Landroid/support/v4/e/c;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
