.class Landroid/support/v7/a/j;
.super Landroid/support/v7/a/i;
.source "AppCompatDelegateImplV14.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v7/a/j$a;
    }
.end annotation


# static fields
.field private static r:Landroid/support/v7/a/q;


# instance fields
.field private s:I

.field private t:Z

.field private u:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/Window;Landroid/support/v7/a/f;)V
    .registers 5

    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/a/i;-><init>(Landroid/content/Context;Landroid/view/Window;Landroid/support/v7/a/f;)V

    const/16 v0, -0x64

    iput v0, p0, Landroid/support/v7/a/j;->s:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/a/j;->u:Z

    return-void
.end method

.method private e(I)Z
    .registers 6

    iget-object v0, p0, Landroid/support/v7/a/j;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v0, v2, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v3, v0, 0x30

    const/4 v0, 0x2

    if-ne p1, v0, :cond_27

    const/16 v0, 0x20

    :goto_13
    if-eq v3, v0, :cond_2a

    new-instance v3, Landroid/content/res/Configuration;

    invoke-direct {v3, v2}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iget v2, v3, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v2, v2, -0x31

    or-int/2addr v0, v2

    iput v0, v3, Landroid/content/res/Configuration;->uiMode:I

    const/4 v0, 0x0

    invoke-virtual {v1, v3, v0}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    const/4 v0, 0x1

    :goto_26
    return v0

    :cond_27
    const/16 v0, 0x10

    goto :goto_13

    :cond_2a
    const/4 v0, 0x0

    goto :goto_26
.end method

.method private s()Landroid/support/v7/a/q;
    .registers 3

    sget-object v0, Landroid/support/v7/a/j;->r:Landroid/support/v7/a/q;

    if-nez v0, :cond_11

    new-instance v0, Landroid/support/v7/a/q;

    iget-object v1, p0, Landroid/support/v7/a/j;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/a/q;-><init>(Landroid/content/Context;)V

    sput-object v0, Landroid/support/v7/a/j;->r:Landroid/support/v7/a/q;

    :cond_11
    sget-object v0, Landroid/support/v7/a/j;->r:Landroid/support/v7/a/q;

    return-object v0
.end method


# virtual methods
.method a(Landroid/view/Window$Callback;)Landroid/view/Window$Callback;
    .registers 3

    new-instance v0, Landroid/support/v7/a/j$a;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/a/j$a;-><init>(Landroid/support/v7/a/j;Landroid/view/Window$Callback;)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4

    const/16 v1, -0x64

    invoke-super {p0, p1}, Landroid/support/v7/a/i;->a(Landroid/os/Bundle;)V

    if-eqz p1, :cond_13

    iget v0, p0, Landroid/support/v7/a/j;->s:I

    if-ne v0, v1, :cond_13

    const-string v0, "appcompat:local_night_mode"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/a/j;->s:I

    :cond_13
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 4

    invoke-super {p0, p1}, Landroid/support/v7/a/i;->c(Landroid/os/Bundle;)V

    iget v0, p0, Landroid/support/v7/a/j;->s:I

    const/16 v1, -0x64

    if-eq v0, v1, :cond_10

    const-string v0, "appcompat:local_night_mode"

    iget v1, p0, Landroid/support/v7/a/j;->s:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_10
    return-void
.end method

.method d(I)I
    .registers 3

    sparse-switch p1, :sswitch_data_16

    move v0, p1

    :goto_4
    return v0

    :sswitch_5
    invoke-direct {p0}, Landroid/support/v7/a/j;->s()Landroid/support/v7/a/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/a/q;->a()Z

    move-result v0

    if-eqz v0, :cond_11

    const/4 v0, 0x2

    goto :goto_4

    :cond_11
    const/4 v0, 0x1

    goto :goto_4

    :sswitch_13
    const/4 v0, -0x1

    goto :goto_4

    nop

    :sswitch_data_16
    .sparse-switch
        -0x64 -> :sswitch_13
        0x0 -> :sswitch_5
    .end sparse-switch
.end method

.method public h()Z
    .registers 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/a/j;->t:Z

    iget v0, p0, Landroid/support/v7/a/j;->s:I

    const/16 v1, -0x64

    if-ne v0, v1, :cond_19

    invoke-static {}, Landroid/support/v7/a/j;->i()I

    move-result v0

    :goto_d
    invoke-virtual {p0, v0}, Landroid/support/v7/a/j;->d(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1c

    invoke-direct {p0, v0}, Landroid/support/v7/a/j;->e(I)Z

    move-result v0

    :goto_18
    return v0

    :cond_19
    iget v0, p0, Landroid/support/v7/a/j;->s:I

    goto :goto_d

    :cond_1c
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public n()Z
    .registers 2

    iget-boolean v0, p0, Landroid/support/v7/a/j;->u:Z

    return v0
.end method
