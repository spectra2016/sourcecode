.class Landroid/support/v7/a/q;
.super Ljava/lang/Object;
.source "TwilightManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v7/a/q$1;,
        Landroid/support/v7/a/q$a;
    }
.end annotation


# static fields
.field private static final a:Landroid/support/v7/a/q$a;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/location/LocationManager;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    new-instance v0, Landroid/support/v7/a/q$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/support/v7/a/q$a;-><init>(Landroid/support/v7/a/q$1;)V

    sput-object v0, Landroid/support/v7/a/q;->a:Landroid/support/v7/a/q$a;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v7/a/q;->b:Landroid/content/Context;

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Landroid/support/v7/a/q;->c:Landroid/location/LocationManager;

    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/location/Location;
    .registers 5

    iget-object v0, p0, Landroid/support/v7/a/q;->c:Landroid/location/LocationManager;

    if-eqz v0, :cond_1b

    :try_start_4
    iget-object v0, p0, Landroid/support/v7/a/q;->c:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Landroid/support/v7/a/q;->c:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_11} :catch_13

    move-result-object v0

    :goto_12
    return-object v0

    :catch_13
    move-exception v0

    const-string v1, "TwilightManager"

    const-string v2, "Failed to get last known location"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1b
    const/4 v0, 0x0

    goto :goto_12
.end method

.method private a(Landroid/location/Location;)V
    .registers 22

    sget-object v10, Landroid/support/v7/a/q;->a:Landroid/support/v7/a/q$a;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {}, Landroid/support/v7/a/p;->a()Landroid/support/v7/a/p;

    move-result-object v3

    const-wide/32 v4, 0x5265c00

    sub-long v4, v12, v4

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-virtual/range {v3 .. v9}, Landroid/support/v7/a/p;->a(JDD)V

    iget-wide v14, v3, Landroid/support/v7/a/p;->a:J

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    move-wide v4, v12

    invoke-virtual/range {v3 .. v9}, Landroid/support/v7/a/p;->a(JDD)V

    iget v2, v3, Landroid/support/v7/a/p;->c:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_6a

    const/4 v2, 0x1

    :goto_2e
    iget-wide v0, v3, Landroid/support/v7/a/p;->b:J

    move-wide/from16 v16, v0

    iget-wide v0, v3, Landroid/support/v7/a/p;->a:J

    move-wide/from16 v18, v0

    const-wide/32 v4, 0x5265c00

    add-long/2addr v4, v12

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-virtual/range {v3 .. v9}, Landroid/support/v7/a/p;->a(JDD)V

    iget-wide v6, v3, Landroid/support/v7/a/p;->b:J

    const-wide/16 v4, 0x0

    const-wide/16 v8, -0x1

    cmp-long v3, v16, v8

    if-eqz v3, :cond_55

    const-wide/16 v8, -0x1

    cmp-long v3, v18, v8

    if-nez v3, :cond_6c

    :cond_55
    const-wide/32 v4, 0x2932e00

    add-long/2addr v4, v12

    :goto_59
    iput-boolean v2, v10, Landroid/support/v7/a/q$a;->a:Z

    iput-wide v14, v10, Landroid/support/v7/a/q$a;->b:J

    move-wide/from16 v0, v16

    iput-wide v0, v10, Landroid/support/v7/a/q$a;->c:J

    move-wide/from16 v0, v18

    iput-wide v0, v10, Landroid/support/v7/a/q$a;->d:J

    iput-wide v6, v10, Landroid/support/v7/a/q$a;->e:J

    iput-wide v4, v10, Landroid/support/v7/a/q$a;->f:J

    return-void

    :cond_6a
    const/4 v2, 0x0

    goto :goto_2e

    :cond_6c
    cmp-long v3, v12, v18

    if-lez v3, :cond_76

    add-long/2addr v4, v6

    :goto_71
    const-wide/32 v8, 0xea60

    add-long/2addr v4, v8

    goto :goto_59

    :cond_76
    cmp-long v3, v12, v16

    if-lez v3, :cond_7d

    add-long v4, v4, v18

    goto :goto_71

    :cond_7d
    add-long v4, v4, v16

    goto :goto_71
.end method

.method private a(Landroid/support/v7/a/q$a;)Z
    .registers 6

    if-eqz p1, :cond_e

    iget-wide v0, p1, Landroid/support/v7/a/q$a;->f:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private b()Landroid/location/Location;
    .registers 7

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/a/q;->b:Landroid/content/Context;

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-static {v0, v2}, Landroid/support/v4/a/j;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_39

    const-string v0, "network"

    invoke-direct {p0, v0}, Landroid/support/v7/a/q;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    :goto_11
    iget-object v2, p0, Landroid/support/v7/a/q;->b:Landroid/content/Context;

    const-string v3, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v2, v3}, Landroid/support/v4/a/j;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_21

    const-string v1, "gps"

    invoke-direct {p0, v1}, Landroid/support/v7/a/q;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    :cond_21
    if-eqz v1, :cond_33

    if-eqz v0, :cond_33

    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_32

    move-object v0, v1

    :cond_32
    :goto_32
    return-object v0

    :cond_33
    if-eqz v1, :cond_37

    :goto_35
    move-object v0, v1

    goto :goto_32

    :cond_37
    move-object v1, v0

    goto :goto_35

    :cond_39
    move-object v0, v1

    goto :goto_11
.end method


# virtual methods
.method a()Z
    .registers 3

    sget-object v0, Landroid/support/v7/a/q;->a:Landroid/support/v7/a/q$a;

    invoke-direct {p0, v0}, Landroid/support/v7/a/q;->a(Landroid/support/v7/a/q$a;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-boolean v0, v0, Landroid/support/v7/a/q$a;->a:Z

    :goto_a
    return v0

    :cond_b
    invoke-direct {p0}, Landroid/support/v7/a/q;->b()Landroid/location/Location;

    move-result-object v1

    if-eqz v1, :cond_17

    invoke-direct {p0, v1}, Landroid/support/v7/a/q;->a(Landroid/location/Location;)V

    iget-boolean v0, v0, Landroid/support/v7/a/q$a;->a:Z

    goto :goto_a

    :cond_17
    const-string v0, "TwilightManager"

    const-string v1, "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x6

    if-lt v0, v1, :cond_2f

    const/16 v1, 0x16

    if-lt v0, v1, :cond_31

    :cond_2f
    const/4 v0, 0x1

    goto :goto_a

    :cond_31
    const/4 v0, 0x0

    goto :goto_a
.end method
