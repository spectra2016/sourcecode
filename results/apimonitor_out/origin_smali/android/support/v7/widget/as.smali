.class public Landroid/support/v7/widget/as;
.super Ljava/lang/Object;
.source "ToolbarWidgetWrapper.java"

# interfaces
.implements Landroid/support/v7/widget/ac;


# instance fields
.field private a:Landroid/support/v7/widget/Toolbar;

.field private b:I

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:Z

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;

.field private k:Ljava/lang/CharSequence;

.field private l:Landroid/view/Window$Callback;

.field private m:Z

.field private n:Landroid/support/v7/widget/d;

.field private o:I

.field private final p:Landroid/support/v7/widget/l;

.field private q:I

.field private r:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/Toolbar;Z)V
    .registers 5

    sget v0, Landroid/support/v7/b/a$i;->abc_action_bar_up_description:I

    sget v1, Landroid/support/v7/b/a$e;->abc_ic_ab_back_mtrl_am_alpha:I

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/support/v7/widget/as;-><init>(Landroid/support/v7/widget/Toolbar;ZII)V

    return-void
.end method

.method public constructor <init>(Landroid/support/v7/widget/Toolbar;ZII)V
    .registers 11

    const/4 v5, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Landroid/support/v7/widget/as;->o:I

    iput v1, p0, Landroid/support/v7/widget/as;->q:I

    iput-object p1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/as;->i:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getSubtitle()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/as;->j:Ljava/lang/CharSequence;

    iget-object v0, p0, Landroid/support/v7/widget/as;->i:Ljava/lang/CharSequence;

    if-eqz v0, :cond_132

    const/4 v0, 0x1

    :goto_1c
    iput-boolean v0, p0, Landroid/support/v7/widget/as;->h:Z

    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/as;->g:Landroid/graphics/drawable/Drawable;

    if-eqz p2, :cond_135

    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    sget-object v3, Landroid/support/v7/b/a$k;->ActionBar:[I

    sget v4, Landroid/support/v7/b/a$a;->actionBarStyle:I

    invoke-static {v0, v2, v3, v4, v1}, Landroid/support/v7/widget/ar;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/widget/ar;

    move-result-object v0

    sget v2, Landroid/support/v7/b/a$k;->ActionBar_title:I

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ar;->c(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_42

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/as;->b(Ljava/lang/CharSequence;)V

    :cond_42
    sget v2, Landroid/support/v7/b/a$k;->ActionBar_subtitle:I

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ar;->c(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_51

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/as;->c(Ljava/lang/CharSequence;)V

    :cond_51
    sget v2, Landroid/support/v7/b/a$k;->ActionBar_logo:I

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ar;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_5c

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/as;->c(Landroid/graphics/drawable/Drawable;)V

    :cond_5c
    sget v2, Landroid/support/v7/b/a$k;->ActionBar_icon:I

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ar;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v7/widget/as;->g:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_6b

    if-eqz v2, :cond_6b

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/as;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_6b
    sget v2, Landroid/support/v7/b/a$k;->ActionBar_homeAsUpIndicator:I

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ar;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_76

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/as;->d(Landroid/graphics/drawable/Drawable;)V

    :cond_76
    sget v2, Landroid/support/v7/b/a$k;->ActionBar_displayOptions:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/widget/ar;->a(II)I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/as;->c(I)V

    sget v2, Landroid/support/v7/b/a$k;->ActionBar_customNavigationLayout:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/widget/ar;->g(II)I

    move-result v2

    if-eqz v2, :cond_a1

    iget-object v3, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3, v2, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/as;->a(Landroid/view/View;)V

    iget v2, p0, Landroid/support/v7/widget/as;->b:I

    or-int/lit8 v2, v2, 0x10

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/as;->c(I)V

    :cond_a1
    sget v2, Landroid/support/v7/b/a$k;->ActionBar_height:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/widget/ar;->f(II)I

    move-result v2

    if-lez v2, :cond_b6

    iget-object v3, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3}, Landroid/support/v7/widget/Toolbar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v2, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/Toolbar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_b6
    sget v2, Landroid/support/v7/b/a$k;->ActionBar_contentInsetStart:I

    invoke-virtual {v0, v2, v5}, Landroid/support/v7/widget/ar;->d(II)I

    move-result v2

    sget v3, Landroid/support/v7/b/a$k;->ActionBar_contentInsetEnd:I

    invoke-virtual {v0, v3, v5}, Landroid/support/v7/widget/ar;->d(II)I

    move-result v3

    if-gez v2, :cond_c6

    if-ltz v3, :cond_d3

    :cond_c6
    iget-object v4, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {v4, v2, v3}, Landroid/support/v7/widget/Toolbar;->a(II)V

    :cond_d3
    sget v2, Landroid/support/v7/b/a$k;->ActionBar_titleTextStyle:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/widget/ar;->g(II)I

    move-result v2

    if-eqz v2, :cond_e6

    iget-object v3, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/content/Context;I)V

    :cond_e6
    sget v2, Landroid/support/v7/b/a$k;->ActionBar_subtitleTextStyle:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/widget/ar;->g(II)I

    move-result v2

    if-eqz v2, :cond_f9

    iget-object v3, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/support/v7/widget/Toolbar;->b(Landroid/content/Context;I)V

    :cond_f9
    sget v2, Landroid/support/v7/b/a$k;->ActionBar_popupTheme:I

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/widget/ar;->g(II)I

    move-result v1

    if-eqz v1, :cond_106

    iget-object v2, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/Toolbar;->setPopupTheme(I)V

    :cond_106
    invoke-virtual {v0}, Landroid/support/v7/widget/ar;->a()V

    :goto_109
    invoke-static {}, Landroid/support/v7/widget/l;->a()Landroid/support/v7/widget/l;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/as;->p:Landroid/support/v7/widget/l;

    invoke-virtual {p0, p3}, Landroid/support/v7/widget/as;->d(I)V

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getNavigationContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/as;->k:Ljava/lang/CharSequence;

    iget-object v0, p0, Landroid/support/v7/widget/as;->p:Landroid/support/v7/widget/l;

    invoke-virtual {p0}, Landroid/support/v7/widget/as;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/as;->b(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Landroid/support/v7/widget/as$1;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/as$1;-><init>(Landroid/support/v7/widget/as;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_132
    move v0, v1

    goto/16 :goto_1c

    :cond_135
    invoke-direct {p0}, Landroid/support/v7/widget/as;->s()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/as;->b:I

    goto :goto_109
.end method

.method static synthetic a(Landroid/support/v7/widget/as;)Landroid/support/v7/widget/Toolbar;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method static synthetic b(Landroid/support/v7/widget/as;)Ljava/lang/CharSequence;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic c(Landroid/support/v7/widget/as;)Landroid/view/Window$Callback;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->l:Landroid/view/Window$Callback;

    return-object v0
.end method

.method static synthetic d(Landroid/support/v7/widget/as;)Z
    .registers 2

    iget-boolean v0, p0, Landroid/support/v7/widget/as;->m:Z

    return v0
.end method

.method private e(Ljava/lang/CharSequence;)V
    .registers 3

    iput-object p1, p0, Landroid/support/v7/widget/as;->i:Ljava/lang/CharSequence;

    iget v0, p0, Landroid/support/v7/widget/as;->b:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_d

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    :cond_d
    return-void
.end method

.method private s()I
    .registers 3

    const/16 v0, 0xb

    iget-object v1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_c

    const/16 v0, 0xf

    :cond_c
    return v0
.end method

.method private t()V
    .registers 3

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v7/widget/as;->b:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_13

    iget v0, p0, Landroid/support/v7/widget/as;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1c

    iget-object v0, p0, Landroid/support/v7/widget/as;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_19

    iget-object v0, p0, Landroid/support/v7/widget/as;->f:Landroid/graphics/drawable/Drawable;

    :cond_13
    :goto_13
    iget-object v1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setLogo(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_19
    iget-object v0, p0, Landroid/support/v7/widget/as;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_13

    :cond_1c
    iget-object v0, p0, Landroid/support/v7/widget/as;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_13
.end method

.method private u()V
    .registers 3

    iget v0, p0, Landroid/support/v7/widget/as;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_15

    iget-object v0, p0, Landroid/support/v7/widget/as;->k:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    iget v1, p0, Landroid/support/v7/widget/as;->q:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(I)V

    :cond_15
    :goto_15
    return-void

    :cond_16
    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/widget/as;->k:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_15
.end method

.method private v()V
    .registers 3

    iget v0, p0, Landroid/support/v7/widget/as;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_11

    iget-object v1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v0, p0, Landroid/support/v7/widget/as;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_12

    iget-object v0, p0, Landroid/support/v7/widget/as;->g:Landroid/graphics/drawable/Drawable;

    :goto_e
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_11
    return-void

    :cond_12
    iget-object v0, p0, Landroid/support/v7/widget/as;->r:Landroid/graphics/drawable/Drawable;

    goto :goto_e
.end method


# virtual methods
.method public a(IJ)Landroid/support/v4/f/au;
    .registers 6

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, Landroid/support/v4/f/af;->k(Landroid/view/View;)Landroid/support/v4/f/au;

    move-result-object v1

    if-nez p1, :cond_1c

    const/high16 v0, 0x3f80

    :goto_a
    invoke-virtual {v1, v0}, Landroid/support/v4/f/au;->a(F)Landroid/support/v4/f/au;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/support/v4/f/au;->a(J)Landroid/support/v4/f/au;

    move-result-object v0

    new-instance v1, Landroid/support/v7/widget/as$2;

    invoke-direct {v1, p0, p1}, Landroid/support/v7/widget/as$2;-><init>(Landroid/support/v7/widget/as;I)V

    invoke-virtual {v0, v1}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/ay;)Landroid/support/v4/f/au;

    move-result-object v0

    return-object v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public a()Landroid/view/ViewGroup;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method public a(I)V
    .registers 4

    if-eqz p1, :cond_10

    iget-object v0, p0, Landroid/support/v7/widget/as;->p:Landroid/support/v7/widget/l;

    invoke-virtual {p0}, Landroid/support/v7/widget/as;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_c
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/as;->a(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_10
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    iput-object p1, p0, Landroid/support/v7/widget/as;->e:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Landroid/support/v7/widget/as;->t()V

    return-void
.end method

.method public a(Landroid/support/v7/view/menu/l$a;Landroid/support/v7/view/menu/f$a;)V
    .registers 4

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/support/v7/view/menu/l$a;Landroid/support/v7/view/menu/f$a;)V

    return-void
.end method

.method public a(Landroid/support/v7/widget/ak;)V
    .registers 6

    const/4 v3, -0x2

    iget-object v0, p0, Landroid/support/v7/widget/as;->c:Landroid/view/View;

    if-eqz v0, :cond_16

    iget-object v0, p0, Landroid/support/v7/widget/as;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    if-ne v0, v1, :cond_16

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/widget/as;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    :cond_16
    iput-object p1, p0, Landroid/support/v7/widget/as;->c:Landroid/view/View;

    if-eqz p1, :cond_3c

    iget v0, p0, Landroid/support/v7/widget/as;->o:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3c

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/widget/as;->c:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;I)V

    iget-object v0, p0, Landroid/support/v7/widget/as;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar$b;

    iput v3, v0, Landroid/support/v7/widget/Toolbar$b;->width:I

    iput v3, v0, Landroid/support/v7/widget/Toolbar$b;->height:I

    const v1, 0x800053

    iput v1, v0, Landroid/support/v7/widget/Toolbar$b;->a:I

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/ak;->setAllowCollapse(Z)V

    :cond_3c
    return-void
.end method

.method public a(Landroid/view/Menu;Landroid/support/v7/view/menu/l$a;)V
    .registers 5

    iget-object v0, p0, Landroid/support/v7/widget/as;->n:Landroid/support/v7/widget/d;

    if-nez v0, :cond_18

    new-instance v0, Landroid/support/v7/widget/d;

    iget-object v1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/as;->n:Landroid/support/v7/widget/d;

    iget-object v0, p0, Landroid/support/v7/widget/as;->n:Landroid/support/v7/widget/d;

    sget v1, Landroid/support/v7/b/a$f;->action_menu_presenter:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/d;->a(I)V

    :cond_18
    iget-object v0, p0, Landroid/support/v7/widget/as;->n:Landroid/support/v7/widget/d;

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/d;->a(Landroid/support/v7/view/menu/l$a;)V

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    check-cast p1, Landroid/support/v7/view/menu/f;

    iget-object v1, p0, Landroid/support/v7/widget/as;->n:Landroid/support/v7/widget/d;

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/support/v7/view/menu/f;Landroid/support/v7/widget/d;)V

    return-void
.end method

.method public a(Landroid/view/View;)V
    .registers 4

    iget-object v0, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    if-eqz v0, :cond_11

    iget v0, p0, Landroid/support/v7/widget/as;->b:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_11

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    :cond_11
    iput-object p1, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    if-eqz p1, :cond_22

    iget v0, p0, Landroid/support/v7/widget/as;->b:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_22

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    :cond_22
    return-void
.end method

.method public a(Landroid/view/Window$Callback;)V
    .registers 2

    iput-object p1, p0, Landroid/support/v7/widget/as;->l:Landroid/view/Window$Callback;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .registers 3

    iget-boolean v0, p0, Landroid/support/v7/widget/as;->h:Z

    if-nez v0, :cond_7

    invoke-direct {p0, p1}, Landroid/support/v7/widget/as;->e(Ljava/lang/CharSequence;)V

    :cond_7
    return-void
.end method

.method public a(Z)V
    .registers 3

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setCollapsible(Z)V

    return-void
.end method

.method public b()Landroid/content/Context;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .registers 4

    if-eqz p1, :cond_10

    iget-object v0, p0, Landroid/support/v7/widget/as;->p:Landroid/support/v7/widget/l;

    invoke-virtual {p0}, Landroid/support/v7/widget/as;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_c
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/as;->c(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_10
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    iget-object v0, p0, Landroid/support/v7/widget/as;->r:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_9

    iput-object p1, p0, Landroid/support/v7/widget/as;->r:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Landroid/support/v7/widget/as;->v()V

    :cond_9
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .registers 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/as;->h:Z

    invoke-direct {p0, p1}, Landroid/support/v7/widget/as;->e(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public b(Z)V
    .registers 2

    return-void
.end method

.method public c(I)V
    .registers 5

    const/4 v2, 0x0

    iget v0, p0, Landroid/support/v7/widget/as;->b:I

    xor-int/2addr v0, p1

    iput p1, p0, Landroid/support/v7/widget/as;->b:I

    if-eqz v0, :cond_46

    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_16

    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_47

    invoke-direct {p0}, Landroid/support/v7/widget/as;->v()V

    invoke-direct {p0}, Landroid/support/v7/widget/as;->u()V

    :cond_16
    :goto_16
    and-int/lit8 v1, v0, 0x3

    if-eqz v1, :cond_1d

    invoke-direct {p0}, Landroid/support/v7/widget/as;->t()V

    :cond_1d
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_33

    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_4d

    iget-object v1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Landroid/support/v7/widget/as;->i:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Landroid/support/v7/widget/as;->j:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    :cond_33
    :goto_33
    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_46

    iget-object v0, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    if-eqz v0, :cond_46

    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_58

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    :cond_46
    :goto_46
    return-void

    :cond_47
    iget-object v1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_16

    :cond_4d
    iget-object v1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_33

    :cond_58
    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/widget/as;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_46
.end method

.method public c(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    iput-object p1, p0, Landroid/support/v7/widget/as;->f:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Landroid/support/v7/widget/as;->t()V

    return-void
.end method

.method public c(Ljava/lang/CharSequence;)V
    .registers 3

    iput-object p1, p0, Landroid/support/v7/widget/as;->j:Ljava/lang/CharSequence;

    iget v0, p0, Landroid/support/v7/widget/as;->b:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_d

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V

    :cond_d
    return-void
.end method

.method public c()Z
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->g()Z

    move-result v0

    return v0
.end method

.method public d()V
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->h()V

    return-void
.end method

.method public d(I)V
    .registers 3

    iget v0, p0, Landroid/support/v7/widget/as;->q:I

    if-ne p1, v0, :cond_5

    :cond_4
    :goto_4
    return-void

    :cond_5
    iput p1, p0, Landroid/support/v7/widget/as;->q:I

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getNavigationContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Landroid/support/v7/widget/as;->q:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/as;->e(I)V

    goto :goto_4
.end method

.method public d(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    iput-object p1, p0, Landroid/support/v7/widget/as;->g:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Landroid/support/v7/widget/as;->v()V

    return-void
.end method

.method public d(Ljava/lang/CharSequence;)V
    .registers 2

    iput-object p1, p0, Landroid/support/v7/widget/as;->k:Ljava/lang/CharSequence;

    invoke-direct {p0}, Landroid/support/v7/widget/as;->u()V

    return-void
.end method

.method public e()Ljava/lang/CharSequence;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public e(I)V
    .registers 3

    if-nez p1, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/as;->d(Ljava/lang/CharSequence;)V

    return-void

    :cond_7
    invoke-virtual {p0}, Landroid/support/v7/widget/as;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public f()V
    .registers 3

    const-string v0, "ToolbarWidgetWrapper"

    const-string v1, "Progress display unsupported"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public g()V
    .registers 3

    const-string v0, "ToolbarWidgetWrapper"

    const-string v1, "Progress display unsupported"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public h()Z
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->a()Z

    move-result v0

    return v0
.end method

.method public i()Z
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->b()Z

    move-result v0

    return v0
.end method

.method public j()Z
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->c()Z

    move-result v0

    return v0
.end method

.method public k()Z
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->d()Z

    move-result v0

    return v0
.end method

.method public l()Z
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->e()Z

    move-result v0

    return v0
.end method

.method public m()V
    .registers 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/as;->m:Z

    return-void
.end method

.method public n()V
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->f()V

    return-void
.end method

.method public o()I
    .registers 2

    iget v0, p0, Landroid/support/v7/widget/as;->b:I

    return v0
.end method

.method public p()I
    .registers 2

    iget v0, p0, Landroid/support/v7/widget/as;->o:I

    return v0
.end method

.method public q()I
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getVisibility()I

    move-result v0

    return v0
.end method

.method public r()Landroid/view/Menu;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/as;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    return-object v0
.end method
