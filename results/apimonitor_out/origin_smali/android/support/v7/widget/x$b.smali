.class Landroid/support/v7/widget/x$b;
.super Landroid/support/v7/widget/ag;
.source "AppCompatSpinner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Landroid/support/v7/widget/x;

.field private c:Ljava/lang/CharSequence;

.field private d:Landroid/widget/ListAdapter;

.field private final e:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/x;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6

    iput-object p1, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-direct {p0, p2, p3, p4}, Landroid/support/v7/widget/ag;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/x$b;->e:Landroid/graphics/Rect;

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/x$b;->a(Landroid/view/View;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/x$b;->a(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/x$b;->a(I)V

    new-instance v0, Landroid/support/v7/widget/x$b$1;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/widget/x$b$1;-><init>(Landroid/support/v7/widget/x$b;Landroid/support/v7/widget/x;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/x$b;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/x$b;)Landroid/widget/ListAdapter;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/x$b;->d:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method static synthetic a(Landroid/support/v7/widget/x$b;Landroid/view/View;)Z
    .registers 3

    invoke-direct {p0, p1}, Landroid/support/v7/widget/x$b;->b(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Landroid/support/v7/widget/x$b;)V
    .registers 1

    invoke-super {p0}, Landroid/support/v7/widget/ag;->c()V

    return-void
.end method

.method private b(Landroid/view/View;)Z
    .registers 3

    invoke-static {p1}, Landroid/support/v4/f/af;->u(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Landroid/support/v7/widget/x$b;->e:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method


# virtual methods
.method public a()Ljava/lang/CharSequence;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/x$b;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .registers 2

    invoke-super {p0, p1}, Landroid/support/v7/widget/ag;->a(Landroid/widget/ListAdapter;)V

    iput-object p1, p0, Landroid/support/v7/widget/x$b;->d:Landroid/widget/ListAdapter;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .registers 2

    iput-object p1, p0, Landroid/support/v7/widget/x$b;->c:Ljava/lang/CharSequence;

    return-void
.end method

.method b()V
    .registers 8

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/x$b;->d()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_96

    iget-object v0, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-static {v0}, Landroid/support/v7/widget/x;->b(Landroid/support/v7/widget/x;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-static {v0}, Landroid/support/v7/widget/au;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_8c

    iget-object v0, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-static {v0}, Landroid/support/v7/widget/x;->b(Landroid/support/v7/widget/x;)Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->right:I

    :goto_20
    move v1, v0

    :goto_21
    iget-object v0, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->getPaddingLeft()I

    move-result v3

    iget-object v0, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->getPaddingRight()I

    move-result v4

    iget-object v0, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->getWidth()I

    move-result v5

    iget-object v0, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-static {v0}, Landroid/support/v7/widget/x;->c(Landroid/support/v7/widget/x;)I

    move-result v0

    const/4 v2, -0x2

    if-ne v0, v2, :cond_a9

    iget-object v2, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    iget-object v0, p0, Landroid/support/v7/widget/x$b;->d:Landroid/widget/ListAdapter;

    check-cast v0, Landroid/widget/SpinnerAdapter;

    invoke-virtual {p0}, Landroid/support/v7/widget/x$b;->d()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-static {v2, v0, v6}, Landroid/support/v7/widget/x;->a(Landroid/support/v7/widget/x;Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I

    move-result v2

    iget-object v0, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v6, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-static {v6}, Landroid/support/v7/widget/x;->b(Landroid/support/v7/widget/x;)Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v6

    iget-object v6, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-static {v6}, Landroid/support/v7/widget/x;->b(Landroid/support/v7/widget/x;)Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v6

    if-le v2, v0, :cond_c6

    :goto_6e
    sub-int v2, v5, v3

    sub-int/2addr v2, v4

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/x$b;->f(I)V

    :goto_78
    iget-object v0, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-static {v0}, Landroid/support/v7/widget/au;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_c3

    sub-int v0, v5, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/x$b;->h()I

    move-result v2

    sub-int/2addr v0, v2

    add-int/2addr v0, v1

    :goto_88
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/x$b;->b(I)V

    return-void

    :cond_8c
    iget-object v0, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-static {v0}, Landroid/support/v7/widget/x;->b(Landroid/support/v7/widget/x;)Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    neg-int v0, v0

    goto :goto_20

    :cond_96
    iget-object v1, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-static {v1}, Landroid/support/v7/widget/x;->b(Landroid/support/v7/widget/x;)Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-static {v2}, Landroid/support/v7/widget/x;->b(Landroid/support/v7/widget/x;)Landroid/graphics/Rect;

    move-result-object v2

    iput v0, v2, Landroid/graphics/Rect;->right:I

    iput v0, v1, Landroid/graphics/Rect;->left:I

    move v1, v0

    goto/16 :goto_21

    :cond_a9
    iget-object v0, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-static {v0}, Landroid/support/v7/widget/x;->c(Landroid/support/v7/widget/x;)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_b9

    sub-int v0, v5, v3

    sub-int/2addr v0, v4

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/x$b;->f(I)V

    goto :goto_78

    :cond_b9
    iget-object v0, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-static {v0}, Landroid/support/v7/widget/x;->c(Landroid/support/v7/widget/x;)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/x$b;->f(I)V

    goto :goto_78

    :cond_c3
    add-int v0, v1, v3

    goto :goto_88

    :cond_c6
    move v0, v2

    goto :goto_6e
.end method

.method public c()V
    .registers 4

    invoke-virtual {p0}, Landroid/support/v7/widget/x$b;->k()Z

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/x$b;->b()V

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/x$b;->g(I)V

    invoke-super {p0}, Landroid/support/v7/widget/ag;->c()V

    invoke-virtual {p0}, Landroid/support/v7/widget/x$b;->m()Landroid/widget/ListView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    iget-object v1, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-virtual {v1}, Landroid/support/v7/widget/x;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/x$b;->h(I)V

    if-eqz v0, :cond_22

    :cond_21
    :goto_21
    return-void

    :cond_22
    iget-object v0, p0, Landroid/support/v7/widget/x$b;->a:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    if-eqz v0, :cond_21

    new-instance v1, Landroid/support/v7/widget/x$b$2;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/x$b$2;-><init>(Landroid/support/v7/widget/x$b;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    new-instance v0, Landroid/support/v7/widget/x$b$3;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/x$b$3;-><init>(Landroid/support/v7/widget/x$b;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/x$b;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    goto :goto_21
.end method
