.class public Landroid/support/v7/widget/ak;
.super Landroid/widget/HorizontalScrollView;
.source "ScrollingTabContainerView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v7/widget/ak$b;,
        Landroid/support/v7/widget/ak$a;,
        Landroid/support/v7/widget/ak$c;
    }
.end annotation


# static fields
.field private static final j:Landroid/view/animation/Interpolator;


# instance fields
.field a:Ljava/lang/Runnable;

.field b:I

.field c:I

.field private d:Landroid/support/v7/widget/ak$b;

.field private e:Landroid/support/v7/widget/af;

.field private f:Landroid/widget/Spinner;

.field private g:Z

.field private h:I

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Landroid/support/v7/widget/ak;->j:Landroid/view/animation/Interpolator;

    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/ak;)Landroid/support/v7/widget/af;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/ak;->e:Landroid/support/v7/widget/af;

    return-object v0
.end method

.method private a(Landroid/support/v7/a/a$c;Z)Landroid/support/v7/widget/ak$c;
    .registers 7

    const/4 v2, 0x0

    new-instance v0, Landroid/support/v7/widget/ak$c;

    invoke-virtual {p0}, Landroid/support/v7/widget/ak;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Landroid/support/v7/widget/ak$c;-><init>(Landroid/support/v7/widget/ak;Landroid/content/Context;Landroid/support/v7/a/a$c;Z)V

    if-eqz p2, :cond_1b

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ak$c;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Landroid/support/v7/widget/ak;->h:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ak$c;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_1a
    return-object v0

    :cond_1b
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ak$c;->setFocusable(Z)V

    iget-object v1, p0, Landroid/support/v7/widget/ak;->d:Landroid/support/v7/widget/ak$b;

    if-nez v1, :cond_2a

    new-instance v1, Landroid/support/v7/widget/ak$b;

    invoke-direct {v1, p0, v2}, Landroid/support/v7/widget/ak$b;-><init>(Landroid/support/v7/widget/ak;Landroid/support/v7/widget/ak$1;)V

    iput-object v1, p0, Landroid/support/v7/widget/ak;->d:Landroid/support/v7/widget/ak$b;

    :cond_2a
    iget-object v1, p0, Landroid/support/v7/widget/ak;->d:Landroid/support/v7/widget/ak$b;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ak$c;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1a
.end method

.method static synthetic a(Landroid/support/v7/widget/ak;Landroid/support/v7/a/a$c;Z)Landroid/support/v7/widget/ak$c;
    .registers 4

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ak;->a(Landroid/support/v7/a/a$c;Z)Landroid/support/v7/widget/ak$c;

    move-result-object v0

    return-object v0
.end method

.method private a()Z
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/ak;->f:Landroid/widget/Spinner;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/ak;->f:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private b()V
    .registers 6

    const/4 v4, 0x0

    invoke-direct {p0}, Landroid/support/v7/widget/ak;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    :goto_7
    return-void

    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/ak;->f:Landroid/widget/Spinner;

    if-nez v0, :cond_12

    invoke-direct {p0}, Landroid/support/v7/widget/ak;->d()Landroid/widget/Spinner;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/ak;->f:Landroid/widget/Spinner;

    :cond_12
    iget-object v0, p0, Landroid/support/v7/widget/ak;->e:Landroid/support/v7/widget/af;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ak;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Landroid/support/v7/widget/ak;->f:Landroid/widget/Spinner;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/ak;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Landroid/support/v7/widget/ak;->f:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-nez v0, :cond_35

    iget-object v0, p0, Landroid/support/v7/widget/ak;->f:Landroid/widget/Spinner;

    new-instance v1, Landroid/support/v7/widget/ak$a;

    invoke-direct {v1, p0, v4}, Landroid/support/v7/widget/ak$a;-><init>(Landroid/support/v7/widget/ak;Landroid/support/v7/widget/ak$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_35
    iget-object v0, p0, Landroid/support/v7/widget/ak;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_40

    iget-object v0, p0, Landroid/support/v7/widget/ak;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ak;->removeCallbacks(Ljava/lang/Runnable;)Z

    iput-object v4, p0, Landroid/support/v7/widget/ak;->a:Ljava/lang/Runnable;

    :cond_40
    iget-object v0, p0, Landroid/support/v7/widget/ak;->f:Landroid/widget/Spinner;

    iget v1, p0, Landroid/support/v7/widget/ak;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_7
.end method

.method private c()Z
    .registers 6

    const/4 v4, 0x0

    invoke-direct {p0}, Landroid/support/v7/widget/ak;->a()Z

    move-result v0

    if-nez v0, :cond_8

    :goto_7
    return v4

    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/ak;->f:Landroid/widget/Spinner;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ak;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Landroid/support/v7/widget/ak;->e:Landroid/support/v7/widget/af;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/ak;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Landroid/support/v7/widget/ak;->f:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ak;->setTabSelected(I)V

    goto :goto_7
.end method

.method private d()Landroid/widget/Spinner;
    .registers 5

    new-instance v0, Landroid/support/v7/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/ak;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    sget v3, Landroid/support/v7/b/a$a;->actionDropDownStyle:I

    invoke-direct {v0, v1, v2, v3}, Landroid/support/v7/widget/x;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Landroid/support/v7/widget/af$a;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/support/v7/widget/af$a;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .registers 4

    iget-object v0, p0, Landroid/support/v7/widget/ak;->e:Landroid/support/v7/widget/af;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/af;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/ak;->a:Ljava/lang/Runnable;

    if-eqz v1, :cond_f

    iget-object v1, p0, Landroid/support/v7/widget/ak;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ak;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_f
    new-instance v1, Landroid/support/v7/widget/ak$1;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/widget/ak$1;-><init>(Landroid/support/v7/widget/ak;Landroid/view/View;)V

    iput-object v1, p0, Landroid/support/v7/widget/ak;->a:Ljava/lang/Runnable;

    iget-object v0, p0, Landroid/support/v7/widget/ak;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ak;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onAttachedToWindow()V
    .registers 2

    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onAttachedToWindow()V

    iget-object v0, p0, Landroid/support/v7/widget/ak;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/v7/widget/ak;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ak;->post(Ljava/lang/Runnable;)Z

    :cond_c
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_9

    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    :cond_9
    invoke-virtual {p0}, Landroid/support/v7/widget/ak;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/view/a;->a(Landroid/content/Context;)Landroid/support/v7/view/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/view/a;->e()I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ak;->setContentHeight(I)V

    invoke-virtual {v0}, Landroid/support/v7/view/a;->g()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/ak;->c:I

    return-void
.end method

.method public onDetachedFromWindow()V
    .registers 2

    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onDetachedFromWindow()V

    iget-object v0, p0, Landroid/support/v7/widget/ak;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/v7/widget/ak;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ak;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_c
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    check-cast p2, Landroid/support/v7/widget/ak$c;

    invoke-virtual {p2}, Landroid/support/v7/widget/ak$c;->b()Landroid/support/v7/a/a$c;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/a/a$c;->d()V

    return-void
.end method

.method public onMeasure(II)V
    .registers 10

    const/high16 v6, 0x4000

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    if-ne v3, v6, :cond_6c

    move v0, v1

    :goto_b
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ak;->setFillViewport(Z)V

    iget-object v4, p0, Landroid/support/v7/widget/ak;->e:Landroid/support/v7/widget/af;

    invoke-virtual {v4}, Landroid/support/v7/widget/af;->getChildCount()I

    move-result v4

    if-le v4, v1, :cond_77

    if-eq v3, v6, :cond_1c

    const/high16 v5, -0x8000

    if-ne v3, v5, :cond_77

    :cond_1c
    const/4 v3, 0x2

    if-le v4, v3, :cond_6e

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3ecccccd

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Landroid/support/v7/widget/ak;->b:I

    :goto_2b
    iget v3, p0, Landroid/support/v7/widget/ak;->b:I

    iget v4, p0, Landroid/support/v7/widget/ak;->c:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Landroid/support/v7/widget/ak;->b:I

    :goto_35
    iget v3, p0, Landroid/support/v7/widget/ak;->h:I

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    if-nez v0, :cond_7b

    iget-boolean v4, p0, Landroid/support/v7/widget/ak;->g:Z

    if-eqz v4, :cond_7b

    :goto_41
    if-eqz v1, :cond_81

    iget-object v1, p0, Landroid/support/v7/widget/ak;->e:Landroid/support/v7/widget/af;

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/widget/af;->measure(II)V

    iget-object v1, p0, Landroid/support/v7/widget/ak;->e:Landroid/support/v7/widget/af;

    invoke-virtual {v1}, Landroid/support/v7/widget/af;->getMeasuredWidth()I

    move-result v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    if-le v1, v2, :cond_7d

    invoke-direct {p0}, Landroid/support/v7/widget/ak;->b()V

    :goto_57
    invoke-virtual {p0}, Landroid/support/v7/widget/ak;->getMeasuredWidth()I

    move-result v1

    invoke-super {p0, p1, v3}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    invoke-virtual {p0}, Landroid/support/v7/widget/ak;->getMeasuredWidth()I

    move-result v2

    if-eqz v0, :cond_6b

    if-eq v1, v2, :cond_6b

    iget v0, p0, Landroid/support/v7/widget/ak;->i:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ak;->setTabSelected(I)V

    :cond_6b
    return-void

    :cond_6c
    move v0, v2

    goto :goto_b

    :cond_6e
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iput v3, p0, Landroid/support/v7/widget/ak;->b:I

    goto :goto_2b

    :cond_77
    const/4 v3, -0x1

    iput v3, p0, Landroid/support/v7/widget/ak;->b:I

    goto :goto_35

    :cond_7b
    move v1, v2

    goto :goto_41

    :cond_7d
    invoke-direct {p0}, Landroid/support/v7/widget/ak;->c()Z

    goto :goto_57

    :cond_81
    invoke-direct {p0}, Landroid/support/v7/widget/ak;->c()Z

    goto :goto_57
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method

.method public setAllowCollapse(Z)V
    .registers 2

    iput-boolean p1, p0, Landroid/support/v7/widget/ak;->g:Z

    return-void
.end method

.method public setContentHeight(I)V
    .registers 2

    iput p1, p0, Landroid/support/v7/widget/ak;->h:I

    invoke-virtual {p0}, Landroid/support/v7/widget/ak;->requestLayout()V

    return-void
.end method

.method public setTabSelected(I)V
    .registers 7

    const/4 v1, 0x0

    iput p1, p0, Landroid/support/v7/widget/ak;->i:I

    iget-object v0, p0, Landroid/support/v7/widget/ak;->e:Landroid/support/v7/widget/af;

    invoke-virtual {v0}, Landroid/support/v7/widget/af;->getChildCount()I

    move-result v3

    move v2, v1

    :goto_a
    if-ge v2, v3, :cond_23

    iget-object v0, p0, Landroid/support/v7/widget/ak;->e:Landroid/support/v7/widget/af;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/af;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-ne v2, p1, :cond_21

    const/4 v0, 0x1

    :goto_15
    invoke-virtual {v4, v0}, Landroid/view/View;->setSelected(Z)V

    if-eqz v0, :cond_1d

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/ak;->a(I)V

    :cond_1d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_a

    :cond_21
    move v0, v1

    goto :goto_15

    :cond_23
    iget-object v0, p0, Landroid/support/v7/widget/ak;->f:Landroid/widget/Spinner;

    if-eqz v0, :cond_2e

    if-ltz p1, :cond_2e

    iget-object v0, p0, Landroid/support/v7/widget/ak;->f:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_2e
    return-void
.end method
