.class Landroid/support/v7/widget/ag$a;
.super Landroid/support/v7/widget/ah;
.source "ListPopupWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/ag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Landroid/support/v4/f/au;

.field private k:Landroid/support/v4/widget/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .registers 5

    const/4 v0, 0x0

    sget v1, Landroid/support/v7/b/a$a;->dropDownListViewStyle:I

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/widget/ah;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean p2, p0, Landroid/support/v7/widget/ag$a;->h:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ag$a;->setCacheColorHint(I)V

    return-void
.end method

.method private a(Landroid/view/View;I)V
    .registers 5

    invoke-virtual {p0, p2}, Landroid/support/v7/widget/ag$a;->getItemIdAtPosition(I)J

    move-result-wide v0

    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/support/v7/widget/ag$a;->performItemClick(Landroid/view/View;IJ)Z

    return-void
.end method

.method private a(Landroid/view/View;IFF)V
    .registers 11

    const/16 v5, 0x15

    const/4 v4, 0x0

    const/4 v3, 0x1

    iput-boolean v3, p0, Landroid/support/v7/widget/ag$a;->i:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v5, :cond_d

    invoke-virtual {p0, p3, p4}, Landroid/support/v7/widget/ag$a;->drawableHotspotChanged(FF)V

    :cond_d
    invoke-virtual {p0}, Landroid/support/v7/widget/ag$a;->isPressed()Z

    move-result v0

    if-nez v0, :cond_16

    invoke-virtual {p0, v3}, Landroid/support/v7/widget/ag$a;->setPressed(Z)V

    :cond_16
    invoke-virtual {p0}, Landroid/support/v7/widget/ag$a;->layoutChildren()V

    iget v0, p0, Landroid/support/v7/widget/ag$a;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_36

    iget v0, p0, Landroid/support/v7/widget/ag$a;->f:I

    invoke-virtual {p0}, Landroid/support/v7/widget/ag$a;->getFirstVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ag$a;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_36

    if-eq v0, p1, :cond_36

    invoke-virtual {v0}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_36

    invoke-virtual {v0, v4}, Landroid/view/View;->setPressed(Z)V

    :cond_36
    iput p2, p0, Landroid/support/v7/widget/ag$a;->f:I

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p3, v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    sub-float v1, p4, v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v5, :cond_4d

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->drawableHotspotChanged(FF)V

    :cond_4d
    invoke-virtual {p1}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-nez v0, :cond_56

    invoke-virtual {p1, v3}, Landroid/view/View;->setPressed(Z)V

    :cond_56
    invoke-virtual {p0, p2, p1, p3, p4}, Landroid/support/v7/widget/ag$a;->a(ILandroid/view/View;FF)V

    invoke-virtual {p0, v4}, Landroid/support/v7/widget/ag$a;->setSelectorEnabled(Z)V

    invoke-virtual {p0}, Landroid/support/v7/widget/ag$a;->refreshDrawableState()V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/ag$a;Z)Z
    .registers 2

    iput-boolean p1, p0, Landroid/support/v7/widget/ag$a;->g:Z

    return p1
.end method

.method private d()V
    .registers 4

    const/4 v2, 0x0

    iput-boolean v2, p0, Landroid/support/v7/widget/ag$a;->i:Z

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ag$a;->setPressed(Z)V

    invoke-virtual {p0}, Landroid/support/v7/widget/ag$a;->drawableStateChanged()V

    iget v0, p0, Landroid/support/v7/widget/ag$a;->f:I

    invoke-virtual {p0}, Landroid/support/v7/widget/ag$a;->getFirstVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ag$a;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_19

    invoke-virtual {v0, v2}, Landroid/view/View;->setPressed(Z)V

    :cond_19
    iget-object v0, p0, Landroid/support/v7/widget/ag$a;->j:Landroid/support/v4/f/au;

    if-eqz v0, :cond_25

    iget-object v0, p0, Landroid/support/v7/widget/ag$a;->j:Landroid/support/v4/f/au;

    invoke-virtual {v0}, Landroid/support/v4/f/au;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/ag$a;->j:Landroid/support/v4/f/au;

    :cond_25
    return-void
.end method


# virtual methods
.method protected a()Z
    .registers 2

    iget-boolean v0, p0, Landroid/support/v7/widget/ag$a;->i:Z

    if-nez v0, :cond_a

    invoke-super {p0}, Landroid/support/v7/widget/ah;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public a(Landroid/view/MotionEvent;I)Z
    .registers 11

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/support/v4/f/s;->a(Landroid/view/MotionEvent;)I

    move-result v3

    packed-switch v3, :pswitch_data_6c

    :cond_9
    :goto_9
    move v0, v1

    move v3, v2

    :goto_b
    if-eqz v3, :cond_f

    if-eqz v0, :cond_12

    :cond_f
    invoke-direct {p0}, Landroid/support/v7/widget/ag$a;->d()V

    :cond_12
    if-eqz v3, :cond_60

    iget-object v0, p0, Landroid/support/v7/widget/ag$a;->k:Landroid/support/v4/widget/k;

    if-nez v0, :cond_1f

    new-instance v0, Landroid/support/v4/widget/k;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/k;-><init>(Landroid/widget/ListView;)V

    iput-object v0, p0, Landroid/support/v7/widget/ag$a;->k:Landroid/support/v4/widget/k;

    :cond_1f
    iget-object v0, p0, Landroid/support/v7/widget/ag$a;->k:Landroid/support/v4/widget/k;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/k;->a(Z)Landroid/support/v4/widget/a;

    iget-object v0, p0, Landroid/support/v7/widget/ag$a;->k:Landroid/support/v4/widget/k;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/widget/k;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    :cond_29
    :goto_29
    return v3

    :pswitch_2a
    move v0, v1

    move v3, v1

    goto :goto_b

    :pswitch_2d
    move v0, v1

    :goto_2e
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v4

    if-gez v4, :cond_37

    move v0, v1

    move v3, v1

    goto :goto_b

    :cond_37
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p0, v5, v4}, Landroid/support/v7/widget/ag$a;->pointToPosition(II)I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_4b

    move v3, v0

    move v0, v2

    goto :goto_b

    :cond_4b
    invoke-virtual {p0}, Landroid/support/v7/widget/ag$a;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, v6, v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ag$a;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    int-to-float v5, v5

    int-to-float v4, v4

    invoke-direct {p0, v0, v6, v5, v4}, Landroid/support/v7/widget/ag$a;->a(Landroid/view/View;IFF)V

    if-ne v3, v2, :cond_9

    invoke-direct {p0, v0, v6}, Landroid/support/v7/widget/ag$a;->a(Landroid/view/View;I)V

    goto :goto_9

    :cond_60
    iget-object v0, p0, Landroid/support/v7/widget/ag$a;->k:Landroid/support/v4/widget/k;

    if-eqz v0, :cond_29

    iget-object v0, p0, Landroid/support/v7/widget/ag$a;->k:Landroid/support/v4/widget/k;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/k;->a(Z)Landroid/support/v4/widget/a;

    goto :goto_29

    :pswitch_6a
    move v0, v2

    goto :goto_2e

    :pswitch_data_6c
    .packed-switch 0x1
        :pswitch_2d
        :pswitch_6a
        :pswitch_2a
    .end packed-switch
.end method

.method public hasFocus()Z
    .registers 2

    iget-boolean v0, p0, Landroid/support/v7/widget/ag$a;->h:Z

    if-nez v0, :cond_a

    invoke-super {p0}, Landroid/support/v7/widget/ah;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public hasWindowFocus()Z
    .registers 2

    iget-boolean v0, p0, Landroid/support/v7/widget/ag$a;->h:Z

    if-nez v0, :cond_a

    invoke-super {p0}, Landroid/support/v7/widget/ah;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public isFocused()Z
    .registers 2

    iget-boolean v0, p0, Landroid/support/v7/widget/ag$a;->h:Z

    if-nez v0, :cond_a

    invoke-super {p0}, Landroid/support/v7/widget/ah;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public isInTouchMode()Z
    .registers 2

    iget-boolean v0, p0, Landroid/support/v7/widget/ag$a;->h:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Landroid/support/v7/widget/ag$a;->g:Z

    if-nez v0, :cond_e

    :cond_8
    invoke-super {p0}, Landroid/support/v7/widget/ah;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method
