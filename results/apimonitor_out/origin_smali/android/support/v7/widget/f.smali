.class public Landroid/support/v7/widget/f;
.super Landroid/widget/AutoCompleteTextView;
.source "AppCompatAutoCompleteTextView.java"

# interfaces
.implements Landroid/support/v4/f/ac;


# static fields
.field private static final a:[I


# instance fields
.field private b:Landroid/support/v7/widget/l;

.field private c:Landroid/support/v7/widget/g;

.field private d:Landroid/support/v7/widget/y;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x1010176

    aput v2, v0, v1

    sput-object v0, Landroid/support/v7/widget/f;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/f;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    sget v0, Landroid/support/v7/b/a$a;->autoCompleteTextViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/f;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/support/v7/widget/ao;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {}, Landroid/support/v7/widget/l;->a()Landroid/support/v7/widget/l;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/f;->b:Landroid/support/v7/widget/l;

    invoke-virtual {p0}, Landroid/support/v7/widget/f;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/widget/f;->a:[I

    invoke-static {v0, p2, v1, p3, v2}, Landroid/support/v7/widget/ar;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/widget/ar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ar;->f(I)Z

    move-result v1

    if-eqz v1, :cond_25

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ar;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/f;->setDropDownBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_25
    invoke-virtual {v0}, Landroid/support/v7/widget/ar;->a()V

    new-instance v0, Landroid/support/v7/widget/g;

    iget-object v1, p0, Landroid/support/v7/widget/f;->b:Landroid/support/v7/widget/l;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/g;-><init>(Landroid/view/View;Landroid/support/v7/widget/l;)V

    iput-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/g;->a(Landroid/util/AttributeSet;I)V

    invoke-static {p0}, Landroid/support/v7/widget/y;->a(Landroid/widget/TextView;)Landroid/support/v7/widget/y;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/f;->d:Landroid/support/v7/widget/y;

    iget-object v0, p0, Landroid/support/v7/widget/f;->d:Landroid/support/v7/widget/y;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/y;->a(Landroid/util/AttributeSet;I)V

    iget-object v0, p0, Landroid/support/v7/widget/f;->d:Landroid/support/v7/widget/y;

    invoke-virtual {v0}, Landroid/support/v7/widget/y;->a()V

    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .registers 2

    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->drawableStateChanged()V

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    invoke-virtual {v0}, Landroid/support/v7/widget/g;->c()V

    :cond_c
    iget-object v0, p0, Landroid/support/v7/widget/f;->d:Landroid/support/v7/widget/y;

    if-eqz v0, :cond_15

    iget-object v0, p0, Landroid/support/v7/widget/f;->d:Landroid/support/v7/widget/y;

    invoke-virtual {v0}, Landroid/support/v7/widget/y;->a()V

    :cond_15
    return-void
.end method

.method public getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    invoke-virtual {v0}, Landroid/support/v7/widget/g;->a()Landroid/content/res/ColorStateList;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    invoke-virtual {v0}, Landroid/support/v7/widget/g;->b()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/g;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_c
    return-void
.end method

.method public setBackgroundResource(I)V
    .registers 3

    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->setBackgroundResource(I)V

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/g;->a(I)V

    :cond_c
    return-void
.end method

.method public setDropDownBackgroundResource(I)V
    .registers 4

    iget-object v0, p0, Landroid/support/v7/widget/f;->b:Landroid/support/v7/widget/l;

    if-eqz v0, :cond_12

    iget-object v0, p0, Landroid/support/v7/widget/f;->b:Landroid/support/v7/widget/l;

    invoke-virtual {p0}, Landroid/support/v7/widget/f;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/f;->setDropDownBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_11
    return-void

    :cond_12
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->setDropDownBackgroundResource(I)V

    goto :goto_11
.end method

.method public setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .registers 3

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/g;->a(Landroid/content/res/ColorStateList;)V

    :cond_9
    return-void
.end method

.method public setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .registers 3

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/f;->c:Landroid/support/v7/widget/g;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/g;->a(Landroid/graphics/PorterDuff$Mode;)V

    :cond_9
    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .registers 4

    invoke-super {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v0, p0, Landroid/support/v7/widget/f;->d:Landroid/support/v7/widget/y;

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/v7/widget/f;->d:Landroid/support/v7/widget/y;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/y;->a(Landroid/content/Context;I)V

    :cond_c
    return-void
.end method
