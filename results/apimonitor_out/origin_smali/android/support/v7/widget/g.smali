.class Landroid/support/v7/widget/g;
.super Ljava/lang/Object;
.source "AppCompatBackgroundHelper.java"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/support/v7/widget/l;

.field private c:Landroid/support/v7/widget/ap;

.field private d:Landroid/support/v7/widget/ap;

.field private e:Landroid/support/v7/widget/ap;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/support/v7/widget/l;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v7/widget/g;->a:Landroid/view/View;

    iput-object p2, p0, Landroid/support/v7/widget/g;->b:Landroid/support/v7/widget/l;

    return-void
.end method

.method private b(Landroid/graphics/drawable/Drawable;)Z
    .registers 5

    const/4 v0, 0x1

    iget-object v1, p0, Landroid/support/v7/widget/g;->e:Landroid/support/v7/widget/ap;

    if-nez v1, :cond_c

    new-instance v1, Landroid/support/v7/widget/ap;

    invoke-direct {v1}, Landroid/support/v7/widget/ap;-><init>()V

    iput-object v1, p0, Landroid/support/v7/widget/g;->e:Landroid/support/v7/widget/ap;

    :cond_c
    iget-object v1, p0, Landroid/support/v7/widget/g;->e:Landroid/support/v7/widget/ap;

    invoke-virtual {v1}, Landroid/support/v7/widget/ap;->a()V

    iget-object v2, p0, Landroid/support/v7/widget/g;->a:Landroid/view/View;

    invoke-static {v2}, Landroid/support/v4/f/af;->q(Landroid/view/View;)Landroid/content/res/ColorStateList;

    move-result-object v2

    if-eqz v2, :cond_1d

    iput-boolean v0, v1, Landroid/support/v7/widget/ap;->d:Z

    iput-object v2, v1, Landroid/support/v7/widget/ap;->a:Landroid/content/res/ColorStateList;

    :cond_1d
    iget-object v2, p0, Landroid/support/v7/widget/g;->a:Landroid/view/View;

    invoke-static {v2}, Landroid/support/v4/f/af;->r(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v2

    if-eqz v2, :cond_29

    iput-boolean v0, v1, Landroid/support/v7/widget/ap;->c:Z

    iput-object v2, v1, Landroid/support/v7/widget/ap;->b:Landroid/graphics/PorterDuff$Mode;

    :cond_29
    iget-boolean v2, v1, Landroid/support/v7/widget/ap;->d:Z

    if-nez v2, :cond_31

    iget-boolean v2, v1, Landroid/support/v7/widget/ap;->c:Z

    if-eqz v2, :cond_3b

    :cond_31
    iget-object v2, p0, Landroid/support/v7/widget/g;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getDrawableState()[I

    move-result-object v2

    invoke-static {p1, v1, v2}, Landroid/support/v7/widget/l;->a(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/ap;[I)V

    :goto_3a
    return v0

    :cond_3b
    const/4 v0, 0x0

    goto :goto_3a
.end method


# virtual methods
.method a()Landroid/content/res/ColorStateList;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    iget-object v0, v0, Landroid/support/v7/widget/ap;->a:Landroid/content/res/ColorStateList;

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method a(I)V
    .registers 4

    iget-object v0, p0, Landroid/support/v7/widget/g;->b:Landroid/support/v7/widget/l;

    if-eqz v0, :cond_14

    iget-object v0, p0, Landroid/support/v7/widget/g;->b:Landroid/support/v7/widget/l;

    iget-object v1, p0, Landroid/support/v7/widget/g;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/l;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    :goto_10
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/g;->b(Landroid/content/res/ColorStateList;)V

    return-void

    :cond_14
    const/4 v0, 0x0

    goto :goto_10
.end method

.method a(Landroid/content/res/ColorStateList;)V
    .registers 4

    iget-object v0, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    if-nez v0, :cond_b

    new-instance v0, Landroid/support/v7/widget/ap;

    invoke-direct {v0}, Landroid/support/v7/widget/ap;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    :cond_b
    iget-object v0, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    iput-object p1, v0, Landroid/support/v7/widget/ap;->a:Landroid/content/res/ColorStateList;

    iget-object v0, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/ap;->d:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/g;->c()V

    return-void
.end method

.method a(Landroid/graphics/PorterDuff$Mode;)V
    .registers 4

    iget-object v0, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    if-nez v0, :cond_b

    new-instance v0, Landroid/support/v7/widget/ap;

    invoke-direct {v0}, Landroid/support/v7/widget/ap;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    :cond_b
    iget-object v0, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    iput-object p1, v0, Landroid/support/v7/widget/ap;->b:Landroid/graphics/PorterDuff$Mode;

    iget-object v0, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/ap;->c:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/g;->c()V

    return-void
.end method

.method a(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/g;->b(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method a(Landroid/util/AttributeSet;I)V
    .registers 8

    iget-object v0, p0, Landroid/support/v7/widget/g;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/b/a$k;->ViewBackgroundHelper:[I

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    :try_start_d
    sget v0, Landroid/support/v7/b/a$k;->ViewBackgroundHelper_android_background:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_2d

    iget-object v0, p0, Landroid/support/v7/widget/g;->b:Landroid/support/v7/widget/l;

    iget-object v2, p0, Landroid/support/v7/widget/g;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Landroid/support/v7/b/a$k;->ViewBackgroundHelper_android_background:I

    const/4 v4, -0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/widget/l;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    if-eqz v0, :cond_2d

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/g;->b(Landroid/content/res/ColorStateList;)V

    :cond_2d
    sget v0, Landroid/support/v7/b/a$k;->ViewBackgroundHelper_backgroundTint:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_40

    iget-object v0, p0, Landroid/support/v7/widget/g;->a:Landroid/view/View;

    sget v2, Landroid/support/v7/b/a$k;->ViewBackgroundHelper_backgroundTint:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v4/f/af;->a(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    :cond_40
    sget v0, Landroid/support/v7/b/a$k;->ViewBackgroundHelper_backgroundTintMode:I

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_59

    iget-object v0, p0, Landroid/support/v7/widget/g;->a:Landroid/view/View;

    sget v2, Landroid/support/v7/b/a$k;->ViewBackgroundHelper_backgroundTintMode:I

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/support/v7/widget/ad;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v4/f/af;->a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
    :try_end_59
    .catchall {:try_start_d .. :try_end_59} :catchall_5d

    :cond_59
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_5d
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method b()Landroid/graphics/PorterDuff$Mode;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    iget-object v0, v0, Landroid/support/v7/widget/ap;->b:Landroid/graphics/PorterDuff$Mode;

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method b(Landroid/content/res/ColorStateList;)V
    .registers 4

    if-eqz p1, :cond_1a

    iget-object v0, p0, Landroid/support/v7/widget/g;->c:Landroid/support/v7/widget/ap;

    if-nez v0, :cond_d

    new-instance v0, Landroid/support/v7/widget/ap;

    invoke-direct {v0}, Landroid/support/v7/widget/ap;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/g;->c:Landroid/support/v7/widget/ap;

    :cond_d
    iget-object v0, p0, Landroid/support/v7/widget/g;->c:Landroid/support/v7/widget/ap;

    iput-object p1, v0, Landroid/support/v7/widget/ap;->a:Landroid/content/res/ColorStateList;

    iget-object v0, p0, Landroid/support/v7/widget/g;->c:Landroid/support/v7/widget/ap;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/ap;->d:Z

    :goto_16
    invoke-virtual {p0}, Landroid/support/v7/widget/g;->c()V

    return-void

    :cond_1a
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/g;->c:Landroid/support/v7/widget/ap;

    goto :goto_16
.end method

.method c()V
    .registers 4

    iget-object v0, p0, Landroid/support/v7/widget/g;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_14

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ne v1, v2, :cond_15

    invoke-direct {p0, v0}, Landroid/support/v7/widget/g;->b(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-eqz v1, :cond_15

    :cond_14
    :goto_14
    return-void

    :cond_15
    iget-object v1, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    if-eqz v1, :cond_25

    iget-object v1, p0, Landroid/support/v7/widget/g;->d:Landroid/support/v7/widget/ap;

    iget-object v2, p0, Landroid/support/v7/widget/g;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getDrawableState()[I

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/l;->a(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/ap;[I)V

    goto :goto_14

    :cond_25
    iget-object v1, p0, Landroid/support/v7/widget/g;->c:Landroid/support/v7/widget/ap;

    if-eqz v1, :cond_14

    iget-object v1, p0, Landroid/support/v7/widget/g;->c:Landroid/support/v7/widget/ap;

    iget-object v2, p0, Landroid/support/v7/widget/g;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getDrawableState()[I

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/l;->a(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/ap;[I)V

    goto :goto_14
.end method
