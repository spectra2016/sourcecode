.class Landroid/support/v7/widget/e;
.super Landroid/database/DataSetObservable;
.source "ActivityChooserModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v7/widget/e$1;,
        Landroid/support/v7/widget/e$e;,
        Landroid/support/v7/widget/e$a;,
        Landroid/support/v7/widget/e$c;,
        Landroid/support/v7/widget/e$d;,
        Landroid/support/v7/widget/e$b;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/Object;

.field private static final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/support/v7/widget/e;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Ljava/lang/Object;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v7/widget/e$a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v7/widget/e$c;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/content/Context;

.field private final h:Ljava/lang/String;

.field private i:Landroid/content/Intent;

.field private j:Landroid/support/v7/widget/e$b;

.field private k:I

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Landroid/support/v7/widget/e$d;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Landroid/support/v7/widget/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/e;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/support/v7/widget/e;->b:Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Landroid/support/v7/widget/e;->c:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/e;)Landroid/content/Context;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/e;->g:Landroid/content/Context;

    return-object v0
.end method

.method private a(Landroid/support/v7/widget/e$c;)Z
    .registers 4

    iget-object v0, p0, Landroid/support/v7/widget/e;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v7/widget/e;->n:Z

    invoke-direct {p0}, Landroid/support/v7/widget/e;->i()V

    invoke-direct {p0}, Landroid/support/v7/widget/e;->d()V

    invoke-direct {p0}, Landroid/support/v7/widget/e;->f()Z

    invoke-virtual {p0}, Landroid/support/v7/widget/e;->notifyChanged()V

    :cond_17
    return v0
.end method

.method static synthetic a(Landroid/support/v7/widget/e;Z)Z
    .registers 2

    iput-boolean p1, p0, Landroid/support/v7/widget/e;->l:Z

    return p1
.end method

.method static synthetic b(Landroid/support/v7/widget/e;)Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Landroid/support/v7/widget/e;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .registers 1

    sget-object v0, Landroid/support/v7/widget/e;->a:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .registers 6

    const/4 v4, 0x0

    iget-boolean v0, p0, Landroid/support/v7/widget/e;->m:Z

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No preceding call to #readHistoricalData"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iget-boolean v0, p0, Landroid/support/v7/widget/e;->n:Z

    if-nez v0, :cond_12

    :cond_11
    :goto_11
    return-void

    :cond_12
    iput-boolean v4, p0, Landroid/support/v7/widget/e;->n:Z

    iget-object v0, p0, Landroid/support/v7/widget/e;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    new-instance v0, Landroid/support/v7/widget/e$e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/e$e;-><init>(Landroid/support/v7/widget/e;Landroid/support/v7/widget/e$1;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Landroid/support/v7/widget/e;->f:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    aput-object v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p0, Landroid/support/v7/widget/e;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Landroid/support/v4/os/a;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_11
.end method

.method private e()V
    .registers 3

    invoke-direct {p0}, Landroid/support/v7/widget/e;->g()Z

    move-result v0

    invoke-direct {p0}, Landroid/support/v7/widget/e;->h()Z

    move-result v1

    or-int/2addr v0, v1

    invoke-direct {p0}, Landroid/support/v7/widget/e;->i()V

    if-eqz v0, :cond_14

    invoke-direct {p0}, Landroid/support/v7/widget/e;->f()Z

    invoke-virtual {p0}, Landroid/support/v7/widget/e;->notifyChanged()V

    :cond_14
    return-void
.end method

.method private f()Z
    .registers 5

    iget-object v0, p0, Landroid/support/v7/widget/e;->j:Landroid/support/v7/widget/e$b;

    if-eqz v0, :cond_29

    iget-object v0, p0, Landroid/support/v7/widget/e;->i:Landroid/content/Intent;

    if-eqz v0, :cond_29

    iget-object v0, p0, Landroid/support/v7/widget/e;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_29

    iget-object v0, p0, Landroid/support/v7/widget/e;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_29

    iget-object v0, p0, Landroid/support/v7/widget/e;->j:Landroid/support/v7/widget/e$b;

    iget-object v1, p0, Landroid/support/v7/widget/e;->i:Landroid/content/Intent;

    iget-object v2, p0, Landroid/support/v7/widget/e;->e:Ljava/util/List;

    iget-object v3, p0, Landroid/support/v7/widget/e;->f:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Landroid/support/v7/widget/e$b;->a(Landroid/content/Intent;Ljava/util/List;Ljava/util/List;)V

    const/4 v0, 0x1

    :goto_28
    return v0

    :cond_29
    const/4 v0, 0x0

    goto :goto_28
.end method

.method private g()Z
    .registers 7

    const/4 v0, 0x0

    iget-boolean v1, p0, Landroid/support/v7/widget/e;->o:Z

    if-eqz v1, :cond_38

    iget-object v1, p0, Landroid/support/v7/widget/e;->i:Landroid/content/Intent;

    if-eqz v1, :cond_38

    iput-boolean v0, p0, Landroid/support/v7/widget/e;->o:Z

    iget-object v1, p0, Landroid/support/v7/widget/e;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Landroid/support/v7/widget/e;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/e;->i:Landroid/content/Intent;

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_21
    if-ge v1, v3, :cond_37

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v4, p0, Landroid/support/v7/widget/e;->e:Ljava/util/List;

    new-instance v5, Landroid/support/v7/widget/e$a;

    invoke-direct {v5, p0, v0}, Landroid/support/v7/widget/e$a;-><init>(Landroid/support/v7/widget/e;Landroid/content/pm/ResolveInfo;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_21

    :cond_37
    const/4 v0, 0x1

    :cond_38
    return v0
.end method

.method private h()Z
    .registers 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Landroid/support/v7/widget/e;->l:Z

    if-eqz v2, :cond_1a

    iget-boolean v2, p0, Landroid/support/v7/widget/e;->n:Z

    if-eqz v2, :cond_1a

    iget-object v2, p0, Landroid/support/v7/widget/e;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1a

    iput-boolean v1, p0, Landroid/support/v7/widget/e;->l:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/e;->m:Z

    invoke-direct {p0}, Landroid/support/v7/widget/e;->j()V

    :goto_19
    return v0

    :cond_1a
    move v0, v1

    goto :goto_19
.end method

.method private i()V
    .registers 5

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/e;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Landroid/support/v7/widget/e;->k:I

    sub-int v3, v0, v1

    if-gtz v3, :cond_e

    :cond_d
    return-void

    :cond_e
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/e;->n:Z

    move v1, v2

    :goto_12
    if-ge v1, v3, :cond_d

    iget-object v0, p0, Landroid/support/v7/widget/e;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/e$c;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12
.end method

.method private j()V
    .registers 10

    const/4 v8, 0x1

    :try_start_1
    iget-object v0, p0, Landroid/support/v7/widget/e;->g:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v7/widget/e;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_8} :catch_d3

    move-result-object v1

    :try_start_9
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    const-string v0, "UTF-8"

    invoke-interface {v2, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_13
    if-eq v0, v8, :cond_1d

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1d

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_13

    :cond_1d
    const-string v0, "historical-records"

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_52

    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "Share records file does not start with historical-records tag."

    invoke-direct {v0, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_31
    .catchall {:try_start_9 .. :try_end_31} :catchall_c8
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_9 .. :try_end_31} :catch_31
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_31} :catch_7f

    :catch_31
    move-exception v0

    :try_start_32
    sget-object v2, Landroid/support/v7/widget/e;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error reading historical recrod file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v7/widget/e;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4c
    .catchall {:try_start_32 .. :try_end_4c} :catchall_c8

    if-eqz v1, :cond_51

    :try_start_4e
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_51
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_51} :catch_cf

    :cond_51
    :goto_51
    return-void

    :cond_52
    :try_start_52
    iget-object v0, p0, Landroid/support/v7/widget/e;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_57
    :goto_57
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_5a
    .catchall {:try_start_52 .. :try_end_5a} :catchall_c8
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_52 .. :try_end_5a} :catch_31
    .catch Ljava/io/IOException; {:try_start_52 .. :try_end_5a} :catch_7f

    move-result v3

    if-ne v3, v8, :cond_65

    if-eqz v1, :cond_51

    :try_start_5f
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_62
    .catch Ljava/io/IOException; {:try_start_5f .. :try_end_62} :catch_63

    goto :goto_51

    :catch_63
    move-exception v0

    goto :goto_51

    :cond_65
    const/4 v4, 0x3

    if-eq v3, v4, :cond_57

    const/4 v4, 0x4

    if-eq v3, v4, :cond_57

    :try_start_6b
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "historical-record"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a2

    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v2, "Share records file not well-formed."

    invoke-direct {v0, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7f
    .catchall {:try_start_6b .. :try_end_7f} :catchall_c8
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_6b .. :try_end_7f} :catch_31
    .catch Ljava/io/IOException; {:try_start_6b .. :try_end_7f} :catch_7f

    :catch_7f
    move-exception v0

    :try_start_80
    sget-object v2, Landroid/support/v7/widget/e;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error reading historical recrod file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v7/widget/e;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9a
    .catchall {:try_start_80 .. :try_end_9a} :catchall_c8

    if-eqz v1, :cond_51

    :try_start_9c
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_9f
    .catch Ljava/io/IOException; {:try_start_9c .. :try_end_9f} :catch_a0

    goto :goto_51

    :catch_a0
    move-exception v0

    goto :goto_51

    :cond_a2
    const/4 v3, 0x0

    :try_start_a3
    const-string v4, "activity"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "time"

    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const/4 v6, 0x0

    const-string v7, "weight"

    invoke-interface {v2, v6, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    new-instance v7, Landroid/support/v7/widget/e$c;

    invoke-direct {v7, v3, v4, v5, v6}, Landroid/support/v7/widget/e$c;-><init>(Ljava/lang/String;JF)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_c7
    .catchall {:try_start_a3 .. :try_end_c7} :catchall_c8
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_a3 .. :try_end_c7} :catch_31
    .catch Ljava/io/IOException; {:try_start_a3 .. :try_end_c7} :catch_7f

    goto :goto_57

    :catchall_c8
    move-exception v0

    if-eqz v1, :cond_ce

    :try_start_cb
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_ce
    .catch Ljava/io/IOException; {:try_start_cb .. :try_end_ce} :catch_d1

    :cond_ce
    :goto_ce
    throw v0

    :catch_cf
    move-exception v0

    goto :goto_51

    :catch_d1
    move-exception v1

    goto :goto_ce

    :catch_d3
    move-exception v0

    goto/16 :goto_51
.end method


# virtual methods
.method public a()I
    .registers 3

    iget-object v1, p0, Landroid/support/v7/widget/e;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    invoke-direct {p0}, Landroid/support/v7/widget/e;->e()V

    iget-object v0, p0, Landroid/support/v7/widget/e;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    monitor-exit v1

    return v0

    :catchall_e
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    throw v0
.end method

.method public a(Landroid/content/pm/ResolveInfo;)I
    .registers 7

    iget-object v2, p0, Landroid/support/v7/widget/e;->d:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    invoke-direct {p0}, Landroid/support/v7/widget/e;->e()V

    iget-object v3, p0, Landroid/support/v7/widget/e;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    const/4 v1, 0x0

    :goto_d
    if-ge v1, v4, :cond_1f

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/e$a;

    iget-object v0, v0, Landroid/support/v7/widget/e$a;->a:Landroid/content/pm/ResolveInfo;

    if-ne v0, p1, :cond_1c

    monitor-exit v2

    move v0, v1

    :goto_1b
    return v0

    :cond_1c
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    :cond_1f
    const/4 v0, -0x1

    monitor-exit v2

    goto :goto_1b

    :catchall_22
    move-exception v0

    monitor-exit v2
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_22

    throw v0
.end method

.method public a(I)Landroid/content/pm/ResolveInfo;
    .registers 4

    iget-object v1, p0, Landroid/support/v7/widget/e;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    invoke-direct {p0}, Landroid/support/v7/widget/e;->e()V

    iget-object v0, p0, Landroid/support/v7/widget/e;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/e$a;

    iget-object v0, v0, Landroid/support/v7/widget/e$a;->a:Landroid/content/pm/ResolveInfo;

    monitor-exit v1

    return-object v0

    :catchall_12
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    throw v0
.end method

.method public b(I)Landroid/content/Intent;
    .registers 9

    const/4 v1, 0x0

    iget-object v2, p0, Landroid/support/v7/widget/e;->d:Ljava/lang/Object;

    monitor-enter v2

    :try_start_4
    iget-object v0, p0, Landroid/support/v7/widget/e;->i:Landroid/content/Intent;

    if-nez v0, :cond_b

    monitor-exit v2

    move-object v0, v1

    :goto_a
    return-object v0

    :cond_b
    invoke-direct {p0}, Landroid/support/v7/widget/e;->e()V

    iget-object v0, p0, Landroid/support/v7/widget/e;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/e$a;

    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, v0, Landroid/support/v7/widget/e$a;->a:Landroid/content/pm/ResolveInfo;

    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/support/v7/widget/e$a;->a:Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v4, p0, Landroid/support/v7/widget/e;->i:Landroid/content/Intent;

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v4, p0, Landroid/support/v7/widget/e;->p:Landroid/support/v7/widget/e$d;

    if-eqz v4, :cond_45

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iget-object v5, p0, Landroid/support/v7/widget/e;->p:Landroid/support/v7/widget/e$d;

    invoke-interface {v5, p0, v4}, Landroid/support/v7/widget/e$d;->a(Landroid/support/v7/widget/e;Landroid/content/Intent;)Z

    move-result v4

    if-eqz v4, :cond_45

    monitor-exit v2

    move-object v0, v1

    goto :goto_a

    :cond_45
    new-instance v1, Landroid/support/v7/widget/e$c;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/high16 v6, 0x3f80

    invoke-direct {v1, v3, v4, v5, v6}, Landroid/support/v7/widget/e$c;-><init>(Landroid/content/ComponentName;JF)V

    invoke-direct {p0, v1}, Landroid/support/v7/widget/e;->a(Landroid/support/v7/widget/e$c;)Z

    monitor-exit v2

    goto :goto_a

    :catchall_55
    move-exception v0

    monitor-exit v2
    :try_end_57
    .catchall {:try_start_4 .. :try_end_57} :catchall_55

    throw v0
.end method

.method public b()Landroid/content/pm/ResolveInfo;
    .registers 4

    iget-object v1, p0, Landroid/support/v7/widget/e;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    invoke-direct {p0}, Landroid/support/v7/widget/e;->e()V

    iget-object v0, p0, Landroid/support/v7/widget/e;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1b

    iget-object v0, p0, Landroid/support/v7/widget/e;->e:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/e$a;

    iget-object v0, v0, Landroid/support/v7/widget/e$a;->a:Landroid/content/pm/ResolveInfo;

    monitor-exit v1

    :goto_1a
    return-object v0

    :cond_1b
    monitor-exit v1

    const/4 v0, 0x0

    goto :goto_1a

    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    throw v0
.end method

.method public c(I)V
    .registers 8

    iget-object v2, p0, Landroid/support/v7/widget/e;->d:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    invoke-direct {p0}, Landroid/support/v7/widget/e;->e()V

    iget-object v0, p0, Landroid/support/v7/widget/e;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/e$a;

    iget-object v1, p0, Landroid/support/v7/widget/e;->e:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/e$a;

    if-eqz v1, :cond_40

    iget v1, v1, Landroid/support/v7/widget/e$a;->b:F

    iget v3, v0, Landroid/support/v7/widget/e$a;->b:F

    sub-float/2addr v1, v3

    const/high16 v3, 0x40a0

    add-float/2addr v1, v3

    :goto_21
    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, v0, Landroid/support/v7/widget/e$a;->a:Landroid/content/pm/ResolveInfo;

    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/support/v7/widget/e$a;->a:Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/support/v7/widget/e$c;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v0, v3, v4, v5, v1}, Landroid/support/v7/widget/e$c;-><init>(Landroid/content/ComponentName;JF)V

    invoke-direct {p0, v0}, Landroid/support/v7/widget/e;->a(Landroid/support/v7/widget/e$c;)Z

    monitor-exit v2

    return-void

    :cond_40
    const/high16 v1, 0x3f80

    goto :goto_21

    :catchall_43
    move-exception v0

    monitor-exit v2
    :try_end_45
    .catchall {:try_start_3 .. :try_end_45} :catchall_43

    throw v0
.end method
