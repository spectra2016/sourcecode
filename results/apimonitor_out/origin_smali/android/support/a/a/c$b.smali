.class public Landroid/support/a/a/c$b;
.super Ljava/lang/Object;
.source "PathParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/a/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field a:C

.field b:[F


# direct methods
.method private constructor <init>(C[F)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-char p1, p0, Landroid/support/a/a/c$b;->a:C

    iput-object p2, p0, Landroid/support/a/a/c$b;->b:[F

    return-void
.end method

.method synthetic constructor <init>(C[FLandroid/support/a/a/c$1;)V
    .registers 4

    invoke-direct {p0, p1, p2}, Landroid/support/a/a/c$b;-><init>(C[F)V

    return-void
.end method

.method private constructor <init>(Landroid/support/a/a/c$b;)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-char v0, p1, Landroid/support/a/a/c$b;->a:C

    iput-char v0, p0, Landroid/support/a/a/c$b;->a:C

    iget-object v0, p1, Landroid/support/a/a/c$b;->b:[F

    const/4 v1, 0x0

    iget-object v2, p1, Landroid/support/a/a/c$b;->b:[F

    array-length v2, v2

    invoke-static {v0, v1, v2}, Landroid/support/a/a/c;->a([FII)[F

    move-result-object v0

    iput-object v0, p0, Landroid/support/a/a/c$b;->b:[F

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/a/a/c$b;Landroid/support/a/a/c$1;)V
    .registers 3

    invoke-direct {p0, p1}, Landroid/support/a/a/c$b;-><init>(Landroid/support/a/a/c$b;)V

    return-void
.end method

.method private static a(Landroid/graphics/Path;DDDDDDDDD)V
    .registers 54

    const-wide/high16 v2, 0x4010

    mul-double v2, v2, p17

    const-wide v4, 0x400921fb54442d18L

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    move/from16 v20, v0

    invoke-static/range {p13 .. p14}, Ljava/lang/Math;->cos(D)D

    move-result-wide v22

    invoke-static/range {p13 .. p14}, Ljava/lang/Math;->sin(D)D

    move-result-wide v24

    invoke-static/range {p15 .. p16}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    invoke-static/range {p15 .. p16}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    move-wide/from16 v0, p5

    neg-double v6, v0

    mul-double v6, v6, v22

    mul-double/2addr v6, v4

    mul-double v8, p7, v24

    mul-double/2addr v8, v2

    sub-double/2addr v6, v8

    move-wide/from16 v0, p5

    neg-double v8, v0

    mul-double v8, v8, v24

    mul-double/2addr v4, v8

    mul-double v8, p7, v22

    mul-double/2addr v2, v8

    add-double/2addr v4, v2

    move/from16 v0, v20

    int-to-double v2, v0

    div-double v26, p17, v2

    const/4 v2, 0x0

    move v9, v2

    move-wide v2, v4

    move-wide v4, v6

    :goto_42
    move/from16 v0, v20

    if-ge v9, v0, :cond_d6

    add-double v14, p15, v26

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double v12, p5, v22

    mul-double/2addr v12, v10

    add-double v12, v12, p1

    mul-double v16, p7, v24

    mul-double v16, v16, v6

    sub-double v18, v12, v16

    mul-double v12, p5, v24

    mul-double/2addr v12, v10

    add-double v12, v12, p3

    mul-double v16, p7, v22

    mul-double v16, v16, v6

    add-double v16, v16, v12

    move-wide/from16 v0, p5

    neg-double v12, v0

    mul-double v12, v12, v22

    mul-double/2addr v12, v6

    mul-double v28, p7, v24

    mul-double v28, v28, v10

    sub-double v12, v12, v28

    move-wide/from16 v0, p5

    neg-double v0, v0

    move-wide/from16 v28, v0

    mul-double v28, v28, v24

    mul-double v6, v6, v28

    mul-double v28, p7, v22

    mul-double v10, v10, v28

    add-double/2addr v10, v6

    sub-double v6, v14, p15

    const-wide/high16 v28, 0x4000

    div-double v6, v6, v28

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v6

    sub-double v28, v14, p15

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sin(D)D

    move-result-wide v28

    const-wide/high16 v30, 0x4010

    const-wide/high16 v32, 0x4008

    mul-double v32, v32, v6

    mul-double v6, v6, v32

    add-double v6, v6, v30

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    const-wide/high16 v30, 0x3ff0

    sub-double v6, v6, v30

    mul-double v6, v6, v28

    const-wide/high16 v28, 0x4008

    div-double v6, v6, v28

    mul-double/2addr v4, v6

    add-double v4, v4, p9

    mul-double/2addr v2, v6

    add-double v28, p11, v2

    mul-double v2, v6, v12

    sub-double v30, v18, v2

    mul-double v2, v6, v10

    sub-double v6, v16, v2

    double-to-float v3, v4

    move-wide/from16 v0, v28

    double-to-float v4, v0

    move-wide/from16 v0, v30

    double-to-float v5, v0

    double-to-float v6, v6

    move-wide/from16 v0, v18

    double-to-float v7, v0

    move-wide/from16 v0, v16

    double-to-float v8, v0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    add-int/lit8 v2, v9, 0x1

    move v9, v2

    move-wide v4, v12

    move-wide/from16 p15, v14

    move-wide/from16 p11, v16

    move-wide/from16 p9, v18

    move-wide v2, v10

    goto/16 :goto_42

    :cond_d6
    return-void
.end method

.method private static a(Landroid/graphics/Path;FFFFFFFZZ)V
    .registers 40

    move/from16 v0, p7

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    move/from16 v0, p1

    float-to-double v2, v0

    mul-double/2addr v2, v8

    move/from16 v0, p2

    float-to-double v4, v0

    mul-double/2addr v4, v10

    add-double/2addr v2, v4

    move/from16 v0, p5

    float-to-double v4, v0

    div-double v6, v2, v4

    move/from16 v0, p1

    neg-float v2, v0

    float-to-double v2, v2

    mul-double/2addr v2, v10

    move/from16 v0, p2

    float-to-double v4, v0

    mul-double/2addr v4, v8

    add-double/2addr v2, v4

    move/from16 v0, p6

    float-to-double v4, v0

    div-double v12, v2, v4

    move/from16 v0, p3

    float-to-double v2, v0

    mul-double/2addr v2, v8

    move/from16 v0, p4

    float-to-double v4, v0

    mul-double/2addr v4, v10

    add-double/2addr v2, v4

    move/from16 v0, p5

    float-to-double v4, v0

    div-double v14, v2, v4

    move/from16 v0, p3

    neg-float v2, v0

    float-to-double v2, v2

    mul-double/2addr v2, v10

    move/from16 v0, p4

    float-to-double v4, v0

    mul-double/2addr v4, v8

    add-double/2addr v2, v4

    move/from16 v0, p6

    float-to-double v4, v0

    div-double v20, v2, v4

    sub-double v2, v6, v14

    sub-double v4, v12, v20

    add-double v18, v6, v14

    const-wide/high16 v22, 0x4000

    div-double v18, v18, v22

    add-double v22, v12, v20

    const-wide/high16 v24, 0x4000

    div-double v22, v22, v24

    mul-double v24, v2, v2

    mul-double v26, v4, v4

    add-double v24, v24, v26

    const-wide/16 v26, 0x0

    cmpl-double v26, v24, v26

    if-nez v26, :cond_6d

    const-string v2, "PathParser"

    const-string v3, " Points are coincident"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_6c
    return-void

    :cond_6d
    const-wide/high16 v26, 0x3ff0

    div-double v26, v26, v24

    const-wide/high16 v28, 0x3fd0

    sub-double v26, v26, v28

    const-wide/16 v28, 0x0

    cmpg-double v28, v26, v28

    if-gez v28, :cond_b8

    const-string v2, "PathParser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Points are too far apart "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v24

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    const-wide v4, 0x3ffffff583a53b8eL

    div-double/2addr v2, v4

    double-to-float v2, v2

    mul-float v7, p5, v2

    mul-float v8, p6, v2

    move-object/from16 v2, p0

    move/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    invoke-static/range {v2 .. v11}, Landroid/support/a/a/c$b;->a(Landroid/graphics/Path;FFFFFFFZZ)V

    goto :goto_6c

    :cond_b8
    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v24

    mul-double v2, v2, v24

    mul-double v4, v4, v24

    move/from16 v0, p8

    move/from16 v1, p9

    if-ne v0, v1, :cond_115

    sub-double v4, v18, v4

    add-double v2, v2, v22

    :goto_ca
    sub-double/2addr v12, v2

    sub-double/2addr v6, v4

    invoke-static {v12, v13, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v18

    sub-double v6, v20, v2

    sub-double v12, v14, v4

    invoke-static {v6, v7, v12, v13}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    sub-double v20, v6, v18

    const-wide/16 v6, 0x0

    cmpl-double v6, v20, v6

    if-ltz v6, :cond_11a

    const/4 v6, 0x1

    :goto_e1
    move/from16 v0, p9

    if-eq v0, v6, :cond_f2

    const-wide/16 v6, 0x0

    cmpl-double v6, v20, v6

    if-lez v6, :cond_11c

    const-wide v6, 0x401921fb54442d18L

    sub-double v20, v20, v6

    :cond_f2
    :goto_f2
    move/from16 v0, p5

    float-to-double v6, v0

    mul-double/2addr v6, v4

    move/from16 v0, p6

    float-to-double v4, v0

    mul-double/2addr v2, v4

    mul-double v4, v6, v8

    mul-double v12, v2, v10

    sub-double/2addr v4, v12

    mul-double/2addr v6, v10

    mul-double/2addr v2, v8

    add-double/2addr v6, v2

    move/from16 v0, p5

    float-to-double v8, v0

    move/from16 v0, p6

    float-to-double v10, v0

    move/from16 v0, p1

    float-to-double v12, v0

    move/from16 v0, p2

    float-to-double v14, v0

    move-object/from16 v3, p0

    invoke-static/range {v3 .. v21}, Landroid/support/a/a/c$b;->a(Landroid/graphics/Path;DDDDDDDDD)V

    goto/16 :goto_6c

    :cond_115
    add-double v4, v4, v18

    sub-double v2, v22, v2

    goto :goto_ca

    :cond_11a
    const/4 v6, 0x0

    goto :goto_e1

    :cond_11c
    const-wide v6, 0x401921fb54442d18L

    add-double v20, v20, v6

    goto :goto_f2
.end method

.method private static a(Landroid/graphics/Path;[FCC[F)V
    .registers 24

    const/4 v9, 0x2

    const/4 v3, 0x0

    aget v8, p1, v3

    const/4 v3, 0x1

    aget v7, p1, v3

    const/4 v3, 0x2

    aget v5, p1, v3

    const/4 v3, 0x3

    aget v3, p1, v3

    const/4 v4, 0x4

    aget v6, p1, v4

    const/4 v4, 0x5

    aget v4, p1, v4

    sparse-switch p3, :sswitch_data_428

    move v13, v9

    :goto_17
    const/4 v9, 0x0

    move v14, v9

    move v15, v4

    move/from16 v16, v6

    move/from16 v17, v7

    move/from16 v18, v8

    :goto_20
    move-object/from16 v0, p4

    array-length v4, v0

    if-ge v14, v4, :cond_406

    sparse-switch p3, :sswitch_data_47a

    move v4, v15

    move/from16 v6, v16

    move v7, v5

    move/from16 v8, v17

    move/from16 v9, v18

    move v5, v3

    :goto_31
    add-int v3, v14, v13

    move v14, v3

    move v15, v4

    move/from16 v16, v6

    move/from16 v17, v8

    move/from16 v18, v9

    move/from16 p2, p3

    move v3, v5

    move v5, v7

    goto :goto_20

    :sswitch_40
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Path;->close()V

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v4}, Landroid/graphics/Path;->moveTo(FF)V

    move v3, v4

    move v5, v6

    move v7, v4

    move v8, v6

    move v13, v9

    goto :goto_17

    :sswitch_4e
    const/4 v9, 0x2

    move v13, v9

    goto :goto_17

    :sswitch_51
    const/4 v9, 0x1

    move v13, v9

    goto :goto_17

    :sswitch_54
    const/4 v9, 0x6

    move v13, v9

    goto :goto_17

    :sswitch_57
    const/4 v9, 0x4

    move v13, v9

    goto :goto_17

    :sswitch_5a
    const/4 v9, 0x7

    move v13, v9

    goto :goto_17

    :sswitch_5d
    add-int/lit8 v4, v14, 0x0

    aget v4, p4, v4

    add-float v6, v18, v4

    add-int/lit8 v4, v14, 0x1

    aget v4, p4, v4

    add-float v4, v4, v17

    if-lez v14, :cond_80

    add-int/lit8 v7, v14, 0x0

    aget v7, p4, v7

    add-int/lit8 v8, v14, 0x1

    aget v8, p4, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v8}, Landroid/graphics/Path;->rLineTo(FF)V

    move v7, v5

    move v8, v4

    move v9, v6

    move v4, v15

    move/from16 v6, v16

    move v5, v3

    goto :goto_31

    :cond_80
    add-int/lit8 v7, v14, 0x0

    aget v7, p4, v7

    add-int/lit8 v8, v14, 0x1

    aget v8, p4, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v8}, Landroid/graphics/Path;->rMoveTo(FF)V

    move v7, v5

    move v8, v4

    move v9, v6

    move v5, v3

    goto :goto_31

    :sswitch_92
    add-int/lit8 v4, v14, 0x0

    aget v6, p4, v4

    add-int/lit8 v4, v14, 0x1

    aget v4, p4, v4

    if-lez v14, :cond_b1

    add-int/lit8 v7, v14, 0x0

    aget v7, p4, v7

    add-int/lit8 v8, v14, 0x1

    aget v8, p4, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    move v7, v5

    move v8, v4

    move v9, v6

    move v4, v15

    move/from16 v6, v16

    move v5, v3

    goto :goto_31

    :cond_b1
    add-int/lit8 v7, v14, 0x0

    aget v7, p4, v7

    add-int/lit8 v8, v14, 0x1

    aget v8, p4, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v8}, Landroid/graphics/Path;->moveTo(FF)V

    move v7, v5

    move v8, v4

    move v9, v6

    move v5, v3

    goto/16 :goto_31

    :sswitch_c4
    add-int/lit8 v4, v14, 0x0

    aget v4, p4, v4

    add-int/lit8 v6, v14, 0x1

    aget v6, p4, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Landroid/graphics/Path;->rLineTo(FF)V

    add-int/lit8 v4, v14, 0x0

    aget v4, p4, v4

    add-float v6, v18, v4

    add-int/lit8 v4, v14, 0x1

    aget v4, p4, v4

    add-float v4, v4, v17

    move v7, v5

    move v8, v4

    move v9, v6

    move v4, v15

    move/from16 v6, v16

    move v5, v3

    goto/16 :goto_31

    :sswitch_e6
    add-int/lit8 v4, v14, 0x0

    aget v4, p4, v4

    add-int/lit8 v6, v14, 0x1

    aget v6, p4, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v4, v14, 0x0

    aget v6, p4, v4

    add-int/lit8 v4, v14, 0x1

    aget v4, p4, v4

    move v7, v5

    move v8, v4

    move v9, v6

    move v4, v15

    move/from16 v6, v16

    move v5, v3

    goto/16 :goto_31

    :sswitch_104
    add-int/lit8 v4, v14, 0x0

    aget v4, p4, v4

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Landroid/graphics/Path;->rLineTo(FF)V

    add-int/lit8 v4, v14, 0x0

    aget v4, p4, v4

    add-float v6, v18, v4

    move v4, v15

    move v7, v5

    move/from16 v8, v17

    move v9, v6

    move v5, v3

    move/from16 v6, v16

    goto/16 :goto_31

    :sswitch_11e
    add-int/lit8 v4, v14, 0x0

    aget v4, p4, v4

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v4, v14, 0x0

    aget v6, p4, v4

    move v4, v15

    move v7, v5

    move/from16 v8, v17

    move v9, v6

    move v5, v3

    move/from16 v6, v16

    goto/16 :goto_31

    :sswitch_137
    const/4 v4, 0x0

    add-int/lit8 v6, v14, 0x0

    aget v6, p4, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Landroid/graphics/Path;->rLineTo(FF)V

    add-int/lit8 v4, v14, 0x0

    aget v4, p4, v4

    add-float v4, v4, v17

    move/from16 v6, v16

    move v7, v5

    move v8, v4

    move/from16 v9, v18

    move v5, v3

    move v4, v15

    goto/16 :goto_31

    :sswitch_151
    add-int/lit8 v4, v14, 0x0

    aget v4, p4, v4

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v4, v14, 0x0

    aget v4, p4, v4

    move/from16 v6, v16

    move v7, v5

    move v8, v4

    move/from16 v9, v18

    move v5, v3

    move v4, v15

    goto/16 :goto_31

    :sswitch_16a
    add-int/lit8 v3, v14, 0x0

    aget v4, p4, v3

    add-int/lit8 v3, v14, 0x1

    aget v5, p4, v3

    add-int/lit8 v3, v14, 0x2

    aget v6, p4, v3

    add-int/lit8 v3, v14, 0x3

    aget v7, p4, v3

    add-int/lit8 v3, v14, 0x4

    aget v8, p4, v3

    add-int/lit8 v3, v14, 0x5

    aget v9, p4, v3

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    add-int/lit8 v3, v14, 0x2

    aget v3, p4, v3

    add-float v5, v18, v3

    add-int/lit8 v3, v14, 0x3

    aget v3, p4, v3

    add-float v3, v3, v17

    add-int/lit8 v4, v14, 0x4

    aget v4, p4, v4

    add-float v6, v18, v4

    add-int/lit8 v4, v14, 0x5

    aget v4, p4, v4

    add-float v4, v4, v17

    move v7, v5

    move v8, v4

    move v9, v6

    move v4, v15

    move/from16 v6, v16

    move v5, v3

    goto/16 :goto_31

    :sswitch_1a8
    add-int/lit8 v3, v14, 0x0

    aget v4, p4, v3

    add-int/lit8 v3, v14, 0x1

    aget v5, p4, v3

    add-int/lit8 v3, v14, 0x2

    aget v6, p4, v3

    add-int/lit8 v3, v14, 0x3

    aget v7, p4, v3

    add-int/lit8 v3, v14, 0x4

    aget v8, p4, v3

    add-int/lit8 v3, v14, 0x5

    aget v9, p4, v3

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    add-int/lit8 v3, v14, 0x4

    aget v6, p4, v3

    add-int/lit8 v3, v14, 0x5

    aget v4, p4, v3

    add-int/lit8 v3, v14, 0x2

    aget v5, p4, v3

    add-int/lit8 v3, v14, 0x3

    aget v3, p4, v3

    move v7, v5

    move v8, v4

    move v9, v6

    move v4, v15

    move/from16 v6, v16

    move v5, v3

    goto/16 :goto_31

    :sswitch_1de
    const/4 v6, 0x0

    const/4 v4, 0x0

    const/16 v7, 0x63

    move/from16 v0, p2

    if-eq v0, v7, :cond_1f8

    const/16 v7, 0x73

    move/from16 v0, p2

    if-eq v0, v7, :cond_1f8

    const/16 v7, 0x43

    move/from16 v0, p2

    if-eq v0, v7, :cond_1f8

    const/16 v7, 0x53

    move/from16 v0, p2

    if-ne v0, v7, :cond_423

    :cond_1f8
    sub-float v4, v18, v5

    sub-float v5, v17, v3

    :goto_1fc
    add-int/lit8 v3, v14, 0x0

    aget v6, p4, v3

    add-int/lit8 v3, v14, 0x1

    aget v7, p4, v3

    add-int/lit8 v3, v14, 0x2

    aget v8, p4, v3

    add-int/lit8 v3, v14, 0x3

    aget v9, p4, v3

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    add-int/lit8 v3, v14, 0x0

    aget v3, p4, v3

    add-float v5, v18, v3

    add-int/lit8 v3, v14, 0x1

    aget v3, p4, v3

    add-float v3, v3, v17

    add-int/lit8 v4, v14, 0x2

    aget v4, p4, v4

    add-float v6, v18, v4

    add-int/lit8 v4, v14, 0x3

    aget v4, p4, v4

    add-float v4, v4, v17

    move v7, v5

    move v8, v4

    move v9, v6

    move v4, v15

    move/from16 v6, v16

    move v5, v3

    goto/16 :goto_31

    :sswitch_232
    const/16 v4, 0x63

    move/from16 v0, p2

    if-eq v0, v4, :cond_24a

    const/16 v4, 0x73

    move/from16 v0, p2

    if-eq v0, v4, :cond_24a

    const/16 v4, 0x43

    move/from16 v0, p2

    if-eq v0, v4, :cond_24a

    const/16 v4, 0x53

    move/from16 v0, p2

    if-ne v0, v4, :cond_41d

    :cond_24a
    const/high16 v4, 0x4000

    mul-float v4, v4, v18

    sub-float/2addr v4, v5

    const/high16 v5, 0x4000

    mul-float v5, v5, v17

    sub-float/2addr v5, v3

    :goto_254
    add-int/lit8 v3, v14, 0x0

    aget v6, p4, v3

    add-int/lit8 v3, v14, 0x1

    aget v7, p4, v3

    add-int/lit8 v3, v14, 0x2

    aget v8, p4, v3

    add-int/lit8 v3, v14, 0x3

    aget v9, p4, v3

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    add-int/lit8 v3, v14, 0x0

    aget v5, p4, v3

    add-int/lit8 v3, v14, 0x1

    aget v3, p4, v3

    add-int/lit8 v4, v14, 0x2

    aget v6, p4, v4

    add-int/lit8 v4, v14, 0x3

    aget v4, p4, v4

    move v7, v5

    move v8, v4

    move v9, v6

    move v4, v15

    move/from16 v6, v16

    move v5, v3

    goto/16 :goto_31

    :sswitch_282
    add-int/lit8 v3, v14, 0x0

    aget v3, p4, v3

    add-int/lit8 v4, v14, 0x1

    aget v4, p4, v4

    add-int/lit8 v5, v14, 0x2

    aget v5, p4, v5

    add-int/lit8 v6, v14, 0x3

    aget v6, p4, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->rQuadTo(FFFF)V

    add-int/lit8 v3, v14, 0x0

    aget v3, p4, v3

    add-float v5, v18, v3

    add-int/lit8 v3, v14, 0x1

    aget v3, p4, v3

    add-float v3, v3, v17

    add-int/lit8 v4, v14, 0x2

    aget v4, p4, v4

    add-float v6, v18, v4

    add-int/lit8 v4, v14, 0x3

    aget v4, p4, v4

    add-float v4, v4, v17

    move v7, v5

    move v8, v4

    move v9, v6

    move v4, v15

    move/from16 v6, v16

    move v5, v3

    goto/16 :goto_31

    :sswitch_2b8
    add-int/lit8 v3, v14, 0x0

    aget v3, p4, v3

    add-int/lit8 v4, v14, 0x1

    aget v4, p4, v4

    add-int/lit8 v5, v14, 0x2

    aget v5, p4, v5

    add-int/lit8 v6, v14, 0x3

    aget v6, p4, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    add-int/lit8 v3, v14, 0x0

    aget v5, p4, v3

    add-int/lit8 v3, v14, 0x1

    aget v3, p4, v3

    add-int/lit8 v4, v14, 0x2

    aget v6, p4, v4

    add-int/lit8 v4, v14, 0x3

    aget v4, p4, v4

    move v7, v5

    move v8, v4

    move v9, v6

    move v4, v15

    move/from16 v6, v16

    move v5, v3

    goto/16 :goto_31

    :sswitch_2e6
    const/4 v6, 0x0

    const/4 v4, 0x0

    const/16 v7, 0x71

    move/from16 v0, p2

    if-eq v0, v7, :cond_300

    const/16 v7, 0x74

    move/from16 v0, p2

    if-eq v0, v7, :cond_300

    const/16 v7, 0x51

    move/from16 v0, p2

    if-eq v0, v7, :cond_300

    const/16 v7, 0x54

    move/from16 v0, p2

    if-ne v0, v7, :cond_419

    :cond_300
    sub-float v4, v18, v5

    sub-float v3, v17, v3

    :goto_304
    add-int/lit8 v5, v14, 0x0

    aget v5, p4, v5

    add-int/lit8 v6, v14, 0x1

    aget v6, p4, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/graphics/Path;->rQuadTo(FFFF)V

    add-float v5, v18, v4

    add-float v3, v3, v17

    add-int/lit8 v4, v14, 0x0

    aget v4, p4, v4

    add-float v6, v18, v4

    add-int/lit8 v4, v14, 0x1

    aget v4, p4, v4

    add-float v4, v4, v17

    move v7, v5

    move v8, v4

    move v9, v6

    move v4, v15

    move/from16 v6, v16

    move v5, v3

    goto/16 :goto_31

    :sswitch_32a
    const/16 v4, 0x71

    move/from16 v0, p2

    if-eq v0, v4, :cond_342

    const/16 v4, 0x74

    move/from16 v0, p2

    if-eq v0, v4, :cond_342

    const/16 v4, 0x51

    move/from16 v0, p2

    if-eq v0, v4, :cond_342

    const/16 v4, 0x54

    move/from16 v0, p2

    if-ne v0, v4, :cond_34e

    :cond_342
    const/high16 v4, 0x4000

    mul-float v4, v4, v18

    sub-float v18, v4, v5

    const/high16 v4, 0x4000

    mul-float v4, v4, v17

    sub-float v17, v4, v3

    :cond_34e
    add-int/lit8 v3, v14, 0x0

    aget v3, p4, v3

    add-int/lit8 v4, v14, 0x1

    aget v4, p4, v4

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    add-int/lit8 v3, v14, 0x0

    aget v6, p4, v3

    add-int/lit8 v3, v14, 0x1

    aget v4, p4, v3

    move/from16 v5, v17

    move/from16 v7, v18

    move v8, v4

    move v9, v6

    move v4, v15

    move/from16 v6, v16

    goto/16 :goto_31

    :sswitch_372
    add-int/lit8 v3, v14, 0x5

    aget v3, p4, v3

    add-float v6, v3, v18

    add-int/lit8 v3, v14, 0x6

    aget v3, p4, v3

    add-float v7, v3, v17

    add-int/lit8 v3, v14, 0x0

    aget v8, p4, v3

    add-int/lit8 v3, v14, 0x1

    aget v9, p4, v3

    add-int/lit8 v3, v14, 0x2

    aget v10, p4, v3

    add-int/lit8 v3, v14, 0x3

    aget v3, p4, v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_3bc

    const/4 v11, 0x1

    :goto_394
    add-int/lit8 v3, v14, 0x4

    aget v3, p4, v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_3be

    const/4 v12, 0x1

    :goto_39e
    move-object/from16 v3, p0

    move/from16 v4, v18

    move/from16 v5, v17

    invoke-static/range {v3 .. v12}, Landroid/support/a/a/c$b;->a(Landroid/graphics/Path;FFFFFFFZZ)V

    add-int/lit8 v3, v14, 0x5

    aget v3, p4, v3

    add-float v5, v18, v3

    add-int/lit8 v3, v14, 0x6

    aget v3, p4, v3

    add-float v3, v3, v17

    move v4, v15

    move/from16 v6, v16

    move v7, v5

    move v8, v3

    move v9, v5

    move v5, v3

    goto/16 :goto_31

    :cond_3bc
    const/4 v11, 0x0

    goto :goto_394

    :cond_3be
    const/4 v12, 0x0

    goto :goto_39e

    :sswitch_3c0
    add-int/lit8 v3, v14, 0x5

    aget v6, p4, v3

    add-int/lit8 v3, v14, 0x6

    aget v7, p4, v3

    add-int/lit8 v3, v14, 0x0

    aget v8, p4, v3

    add-int/lit8 v3, v14, 0x1

    aget v9, p4, v3

    add-int/lit8 v3, v14, 0x2

    aget v10, p4, v3

    add-int/lit8 v3, v14, 0x3

    aget v3, p4, v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_402

    const/4 v11, 0x1

    :goto_3de
    add-int/lit8 v3, v14, 0x4

    aget v3, p4, v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_404

    const/4 v12, 0x1

    :goto_3e8
    move-object/from16 v3, p0

    move/from16 v4, v18

    move/from16 v5, v17

    invoke-static/range {v3 .. v12}, Landroid/support/a/a/c$b;->a(Landroid/graphics/Path;FFFFFFFZZ)V

    add-int/lit8 v3, v14, 0x5

    aget v5, p4, v3

    add-int/lit8 v3, v14, 0x6

    aget v3, p4, v3

    move v4, v15

    move/from16 v6, v16

    move v7, v5

    move v8, v3

    move v9, v5

    move v5, v3

    goto/16 :goto_31

    :cond_402
    const/4 v11, 0x0

    goto :goto_3de

    :cond_404
    const/4 v12, 0x0

    goto :goto_3e8

    :cond_406
    const/4 v4, 0x0

    aput v18, p1, v4

    const/4 v4, 0x1

    aput v17, p1, v4

    const/4 v4, 0x2

    aput v5, p1, v4

    const/4 v4, 0x3

    aput v3, p1, v4

    const/4 v3, 0x4

    aput v16, p1, v3

    const/4 v3, 0x5

    aput v15, p1, v3

    return-void

    :cond_419
    move v3, v4

    move v4, v6

    goto/16 :goto_304

    :cond_41d
    move/from16 v5, v17

    move/from16 v4, v18

    goto/16 :goto_254

    :cond_423
    move v5, v4

    move v4, v6

    goto/16 :goto_1fc

    nop

    :sswitch_data_428
    .sparse-switch
        0x41 -> :sswitch_5a
        0x43 -> :sswitch_54
        0x48 -> :sswitch_51
        0x4c -> :sswitch_4e
        0x4d -> :sswitch_4e
        0x51 -> :sswitch_57
        0x53 -> :sswitch_57
        0x54 -> :sswitch_4e
        0x56 -> :sswitch_51
        0x5a -> :sswitch_40
        0x61 -> :sswitch_5a
        0x63 -> :sswitch_54
        0x68 -> :sswitch_51
        0x6c -> :sswitch_4e
        0x6d -> :sswitch_4e
        0x71 -> :sswitch_57
        0x73 -> :sswitch_57
        0x74 -> :sswitch_4e
        0x76 -> :sswitch_51
        0x7a -> :sswitch_40
    .end sparse-switch

    :sswitch_data_47a
    .sparse-switch
        0x41 -> :sswitch_3c0
        0x43 -> :sswitch_1a8
        0x48 -> :sswitch_11e
        0x4c -> :sswitch_e6
        0x4d -> :sswitch_92
        0x51 -> :sswitch_2b8
        0x53 -> :sswitch_232
        0x54 -> :sswitch_32a
        0x56 -> :sswitch_151
        0x61 -> :sswitch_372
        0x63 -> :sswitch_16a
        0x68 -> :sswitch_104
        0x6c -> :sswitch_c4
        0x6d -> :sswitch_5d
        0x71 -> :sswitch_282
        0x73 -> :sswitch_1de
        0x74 -> :sswitch_2e6
        0x76 -> :sswitch_137
    .end sparse-switch
.end method

.method public static a([Landroid/support/a/a/c$b;Landroid/graphics/Path;)V
    .registers 7

    const/4 v0, 0x6

    new-array v2, v0, [F

    const/16 v1, 0x6d

    const/4 v0, 0x0

    :goto_6
    array-length v3, p0

    if-ge v0, v3, :cond_1b

    aget-object v3, p0, v0

    iget-char v3, v3, Landroid/support/a/a/c$b;->a:C

    aget-object v4, p0, v0

    iget-object v4, v4, Landroid/support/a/a/c$b;->b:[F

    invoke-static {p1, v2, v1, v3, v4}, Landroid/support/a/a/c$b;->a(Landroid/graphics/Path;[FCC[F)V

    aget-object v1, p0, v0

    iget-char v1, v1, Landroid/support/a/a/c$b;->a:C

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_1b
    return-void
.end method
