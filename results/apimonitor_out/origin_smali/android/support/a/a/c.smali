.class Landroid/support/a/a/c;
.super Ljava/lang/Object;
.source "PathParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/a/a/c$1;,
        Landroid/support/a/a/c$b;,
        Landroid/support/a/a/c$a;
    }
.end annotation


# direct methods
.method private static a(Ljava/lang/String;I)I
    .registers 5

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p1, v0, :cond_20

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v1, v0, -0x41

    add-int/lit8 v2, v0, -0x5a

    mul-int/2addr v1, v2

    if-lez v1, :cond_18

    add-int/lit8 v1, v0, -0x61

    add-int/lit8 v2, v0, -0x7a

    mul-int/2addr v1, v2

    if-gtz v1, :cond_21

    :cond_18
    const/16 v1, 0x65

    if-eq v0, v1, :cond_21

    const/16 v1, 0x45

    if-eq v0, v1, :cond_21

    :cond_20
    return p1

    :cond_21
    add-int/lit8 p1, p1, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;ILandroid/support/a/a/c$a;)V
    .registers 10

    const/4 v1, 0x0

    const/4 v5, 0x1

    iput-boolean v1, p2, Landroid/support/a/a/c$a;->b:Z

    move v0, v1

    move v2, v1

    move v3, v1

    move v4, p1

    :goto_8
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v4, v6, :cond_18

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v6

    sparse-switch v6, :sswitch_data_36

    :cond_15
    move v0, v1

    :goto_16
    if-eqz v3, :cond_33

    :cond_18
    iput v4, p2, Landroid/support/a/a/c$a;->a:I

    return-void

    :sswitch_1b
    move v0, v1

    move v3, v5

    goto :goto_16

    :sswitch_1e
    if-eq v4, p1, :cond_15

    if-nez v0, :cond_15

    iput-boolean v5, p2, Landroid/support/a/a/c$a;->b:Z

    move v0, v1

    move v3, v5

    goto :goto_16

    :sswitch_27
    if-nez v2, :cond_2c

    move v0, v1

    move v2, v5

    goto :goto_16

    :cond_2c
    iput-boolean v5, p2, Landroid/support/a/a/c$a;->b:Z

    move v0, v1

    move v3, v5

    goto :goto_16

    :sswitch_31
    move v0, v5

    goto :goto_16

    :cond_33
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    :sswitch_data_36
    .sparse-switch
        0x20 -> :sswitch_1b
        0x2c -> :sswitch_1b
        0x2d -> :sswitch_1e
        0x2e -> :sswitch_27
        0x45 -> :sswitch_31
        0x65 -> :sswitch_31
    .end sparse-switch
.end method

.method private static a(Ljava/util/ArrayList;C[F)V
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/a/a/c$b;",
            ">;C[F)V"
        }
    .end annotation

    new-instance v0, Landroid/support/a/a/c$b;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Landroid/support/a/a/c$b;-><init>(C[FLandroid/support/a/a/c$1;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic a([FII)[F
    .registers 4

    invoke-static {p0, p1, p2}, Landroid/support/a/a/c;->b([FII)[F

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)[Landroid/support/a/a/c$b;
    .registers 7

    const/4 v1, 0x1

    const/4 v3, 0x0

    if-nez p0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    move v2, v3

    :goto_d
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_34

    invoke-static {p0, v0}, Landroid/support/a/a/c;->a(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {p0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_30

    invoke-static {v0}, Landroid/support/a/a/c;->b(Ljava/lang/String;)[F

    move-result-object v2

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v5, v0, v2}, Landroid/support/a/a/c;->a(Ljava/util/ArrayList;C[F)V

    :cond_30
    add-int/lit8 v0, v4, 0x1

    move v2, v4

    goto :goto_d

    :cond_34
    sub-int/2addr v0, v2

    if-ne v0, v1, :cond_46

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v2, v0, :cond_46

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    new-array v1, v3, [F

    invoke-static {v5, v0, v1}, Landroid/support/a/a/c;->a(Ljava/util/ArrayList;C[F)V

    :cond_46
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/support/a/a/c$b;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/support/a/a/c$b;

    goto :goto_5
.end method

.method public static a([Landroid/support/a/a/c$b;)[Landroid/support/a/a/c$b;
    .registers 6

    const/4 v1, 0x0

    if-nez p0, :cond_5

    move-object v0, v1

    :goto_4
    return-object v0

    :cond_5
    array-length v0, p0

    new-array v2, v0, [Landroid/support/a/a/c$b;

    const/4 v0, 0x0

    :goto_9
    array-length v3, p0

    if-ge v0, v3, :cond_18

    new-instance v3, Landroid/support/a/a/c$b;

    aget-object v4, p0, v0

    invoke-direct {v3, v4, v1}, Landroid/support/a/a/c$b;-><init>(Landroid/support/a/a/c$b;Landroid/support/a/a/c$1;)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_18
    move-object v0, v2

    goto :goto_4
.end method

.method private static b(Ljava/lang/String;)[F
    .registers 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v3, 0x7a

    if-ne v0, v3, :cond_1a

    move v0, v1

    :goto_b
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x5a

    if-ne v3, v4, :cond_1c

    move v3, v1

    :goto_14
    or-int/2addr v0, v3

    if-eqz v0, :cond_1e

    new-array v0, v2, [F

    :goto_19
    return-object v0

    :cond_1a
    move v0, v2

    goto :goto_b

    :cond_1c
    move v3, v2

    goto :goto_14

    :cond_1e
    :try_start_1e
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    new-array v4, v0, [F

    new-instance v5, Landroid/support/a/a/c$a;

    const/4 v0, 0x0

    invoke-direct {v5, v0}, Landroid/support/a/a/c$a;-><init>(Landroid/support/a/a/c$1;)V

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    move v7, v1

    move v1, v2

    move v2, v7

    :goto_31
    if-ge v2, v6, :cond_52

    invoke-static {p0, v2, v5}, Landroid/support/a/a/c;->a(Ljava/lang/String;ILandroid/support/a/a/c$a;)V

    iget v3, v5, Landroid/support/a/a/c$a;->a:I

    if-ge v2, v3, :cond_78

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    aput v2, v4, v1

    :goto_46
    iget-boolean v1, v5, Landroid/support/a/a/c$a;->b:Z

    if-eqz v1, :cond_4d

    move v2, v3

    move v1, v0

    goto :goto_31

    :cond_4d
    add-int/lit8 v1, v3, 0x1

    move v2, v1

    move v1, v0

    goto :goto_31

    :cond_52
    const/4 v0, 0x0

    invoke-static {v4, v0, v1}, Landroid/support/a/a/c;->b([FII)[F
    :try_end_56
    .catch Ljava/lang/NumberFormatException; {:try_start_1e .. :try_end_56} :catch_58

    move-result-object v0

    goto :goto_19

    :catch_58
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error in parsing \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_78
    move v0, v1

    goto :goto_46
.end method

.method private static b([FII)[F
    .registers 6

    if-le p1, p2, :cond_8

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_8
    array-length v0, p0

    if-ltz p1, :cond_d

    if-le p1, v0, :cond_13

    :cond_d
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_13
    sub-int v1, p2, p1

    sub-int/2addr v0, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-array v1, v1, [F

    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1
.end method
