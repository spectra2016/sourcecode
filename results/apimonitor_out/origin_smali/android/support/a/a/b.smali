.class public Landroid/support/a/a/b;
.super Landroid/support/a/a/e;
.source "AnimatedVectorDrawableCompat.java"

# interfaces
.implements Landroid/graphics/drawable/Animatable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/a/a/b$a;,
        Landroid/support/a/a/b$b;
    }
.end annotation


# instance fields
.field private b:Landroid/support/a/a/b$a;

.field private c:Landroid/content/Context;

.field private d:Landroid/animation/ArgbEvaluator;

.field private final e:Landroid/graphics/drawable/Drawable$Callback;


# direct methods
.method private constructor <init>()V
    .registers 2

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, v0}, Landroid/support/a/a/b;-><init>(Landroid/content/Context;Landroid/support/a/a/b$a;Landroid/content/res/Resources;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0}, Landroid/support/a/a/b;-><init>(Landroid/content/Context;Landroid/support/a/a/b$a;Landroid/content/res/Resources;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/support/a/a/b$a;Landroid/content/res/Resources;)V
    .registers 6

    invoke-direct {p0}, Landroid/support/a/a/e;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/a/a/b;->d:Landroid/animation/ArgbEvaluator;

    new-instance v0, Landroid/support/a/a/b$1;

    invoke-direct {v0, p0}, Landroid/support/a/a/b$1;-><init>(Landroid/support/a/a/b;)V

    iput-object v0, p0, Landroid/support/a/a/b;->e:Landroid/graphics/drawable/Drawable$Callback;

    iput-object p1, p0, Landroid/support/a/a/b;->c:Landroid/content/Context;

    if-eqz p2, :cond_14

    iput-object p2, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    :goto_13
    return-void

    :cond_14
    new-instance v0, Landroid/support/a/a/b$a;

    iget-object v1, p0, Landroid/support/a/a/b;->e:Landroid/graphics/drawable/Drawable$Callback;

    invoke-direct {v0, p1, p2, v1, p3}, Landroid/support/a/a/b$a;-><init>(Landroid/content/Context;Landroid/support/a/a/b$a;Landroid/graphics/drawable/Drawable$Callback;Landroid/content/res/Resources;)V

    iput-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    goto :goto_13
.end method

.method synthetic constructor <init>(Landroid/support/a/a/b$1;)V
    .registers 2

    invoke-direct {p0}, Landroid/support/a/a/b;-><init>()V

    return-void
.end method

.method static a(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    .registers 5

    const/4 v0, 0x0

    if-nez p1, :cond_8

    invoke-virtual {p0, p2, p3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    :goto_7
    return-object v0

    :cond_8
    invoke-virtual {p1, p2, p3, v0, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    goto :goto_7
.end method

.method static synthetic a(Landroid/support/a/a/b;)Landroid/graphics/drawable/Drawable$Callback;
    .registers 2

    iget-object v0, p0, Landroid/support/a/a/b;->e:Landroid/graphics/drawable/Drawable$Callback;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/support/a/a/b;
    .registers 6

    new-instance v0, Landroid/support/a/a/b;

    invoke-direct {v0, p0}, Landroid/support/a/a/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/a/a/b;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    return-object v0
.end method

.method private a(Landroid/animation/Animator;)V
    .registers 5

    instance-of v0, p1, Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_22

    move-object v0, p1

    check-cast v0, Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_22

    const/4 v0, 0x0

    move v1, v0

    :goto_f
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_22

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-direct {p0, v0}, Landroid/support/a/a/b;->a(Landroid/animation/Animator;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f

    :cond_22
    instance-of v0, p1, Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_4c

    check-cast p1, Landroid/animation/ObjectAnimator;

    invoke-virtual {p1}, Landroid/animation/ObjectAnimator;->getPropertyName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "fillColor"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3c

    const-string v1, "strokeColor"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c

    :cond_3c
    iget-object v0, p0, Landroid/support/a/a/b;->d:Landroid/animation/ArgbEvaluator;

    if-nez v0, :cond_47

    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Landroid/support/a/a/b;->d:Landroid/animation/ArgbEvaluator;

    :cond_47
    iget-object v0, p0, Landroid/support/a/a/b;->d:Landroid/animation/ArgbEvaluator;

    invoke-virtual {p1, v0}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    :cond_4c
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/animation/Animator;)V
    .registers 5

    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0, p1}, Landroid/support/a/a/f;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_14

    invoke-direct {p0, p2}, Landroid/support/a/a/b;->a(Landroid/animation/Animator;)V

    :cond_14
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_2c

    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Landroid/support/a/a/b$a;->c:Ljava/util/ArrayList;

    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    new-instance v1, Landroid/support/v4/e/a;

    invoke-direct {v1}, Landroid/support/v4/e/a;-><init>()V

    iput-object v1, v0, Landroid/support/a/a/b$a;->d:Landroid/support/v4/e/a;

    :cond_2c
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->d:Landroid/support/v4/e/a;

    invoke-virtual {v0, p2, p1}, Landroid/support/v4/e/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private a()Z
    .registers 6

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v3, v0, Landroid/support/a/a/b$a;->c:Ljava/util/ArrayList;

    if-nez v3, :cond_9

    move v0, v1

    :goto_8
    return v0

    :cond_9
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_e
    if-ge v2, v4, :cond_22

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1e

    const/4 v0, 0x1

    goto :goto_8

    :cond_1e
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_e

    :cond_22
    move v0, v1

    goto :goto_8
.end method


# virtual methods
.method public applyTheme(Landroid/content/res/Resources$Theme;)V
    .registers 3

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/Resources$Theme;)V

    :cond_9
    return-void
.end method

.method public canApplyTheme()Z
    .registers 2

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Landroid/support/v4/b/a/a;->d(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public bridge synthetic clearColorFilter()V
    .registers 1

    invoke-super {p0}, Landroid/support/a/a/e;->clearColorFilter()V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 3

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_9
    :goto_9
    return-void

    :cond_a
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0, p1}, Landroid/support/a/a/f;->draw(Landroid/graphics/Canvas;)V

    invoke-direct {p0}, Landroid/support/a/a/b;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Landroid/support/a/a/b;->invalidateSelf()V

    goto :goto_9
.end method

.method public getAlpha()I
    .registers 2

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Landroid/support/v4/b/a/a;->c(Landroid/graphics/drawable/Drawable;)I

    move-result v0

    :goto_a
    return v0

    :cond_b
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0}, Landroid/support/a/a/f;->getAlpha()I

    move-result v0

    goto :goto_a
.end method

.method public getChangingConfigurations()I
    .registers 3

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    invoke-super {p0}, Landroid/support/a/a/e;->getChangingConfigurations()I

    move-result v0

    iget-object v1, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget v1, v1, Landroid/support/a/a/b$a;->a:I

    or-int/2addr v0, v1

    goto :goto_a
.end method

.method public bridge synthetic getColorFilter()Landroid/graphics/ColorFilter;
    .registers 2

    invoke-super {p0}, Landroid/support/a/a/e;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v0

    return-object v0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .registers 3

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_10

    new-instance v0, Landroid/support/a/a/b$b;

    iget-object v1, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/a/a/b$b;-><init>(Landroid/graphics/drawable/Drawable$ConstantState;)V

    :goto_f
    return-object v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public bridge synthetic getCurrent()Landroid/graphics/drawable/Drawable;
    .registers 2

    invoke-super {p0}, Landroid/support/a/a/e;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getIntrinsicHeight()I
    .registers 2

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0}, Landroid/support/a/a/f;->getIntrinsicHeight()I

    move-result v0

    goto :goto_a
.end method

.method public getIntrinsicWidth()I
    .registers 2

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0}, Landroid/support/a/a/f;->getIntrinsicWidth()I

    move-result v0

    goto :goto_a
.end method

.method public bridge synthetic getLayoutDirection()I
    .registers 2

    invoke-super {p0}, Landroid/support/a/a/e;->getLayoutDirection()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getMinimumHeight()I
    .registers 2

    invoke-super {p0}, Landroid/support/a/a/e;->getMinimumHeight()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getMinimumWidth()I
    .registers 2

    invoke-super {p0}, Landroid/support/a/a/e;->getMinimumWidth()I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .registers 2

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0}, Landroid/support/a/a/f;->getOpacity()I

    move-result v0

    goto :goto_a
.end method

.method public bridge synthetic getPadding(Landroid/graphics/Rect;)Z
    .registers 3

    invoke-super {p0, p1}, Landroid/support/a/a/e;->getPadding(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getState()[I
    .registers 2

    invoke-super {p0}, Landroid/support/a/a/e;->getState()[I

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getTransparentRegion()Landroid/graphics/Region;
    .registers 2

    invoke-super {p0}, Landroid/support/a/a/e;->getTransparentRegion()Landroid/graphics/Region;

    move-result-object v0

    return-object v0
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .registers 5

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/support/a/a/b;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    return-void
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
    .registers 11

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1, p2, p3, p4}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    :cond_b
    return-void

    :cond_c
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    :goto_10
    if-eq v0, v5, :cond_b

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4e

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "animated-vector"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_53

    sget-object v0, Landroid/support/a/a/a;->e:[I

    invoke-static {p1, p4, p3, v0}, Landroid/support/a/a/b;->a(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    if-eqz v1, :cond_4b

    invoke-static {p1, v1, p4}, Landroid/support/a/a/f;->a(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/support/a/a/f;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/a/a/f;->a(Z)V

    iget-object v2, p0, Landroid/support/a/a/b;->e:Landroid/graphics/drawable/Drawable$Callback;

    invoke-virtual {v1, v2}, Landroid/support/a/a/f;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v2, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v2, v2, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    if-eqz v2, :cond_47

    iget-object v2, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v2, v2, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/a/a/f;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_47
    iget-object v2, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iput-object v1, v2, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    :cond_4b
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_4e
    :goto_4e
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_10

    :cond_53
    const-string v1, "target"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4e

    sget-object v0, Landroid/support/a/a/a;->f:[I

    invoke-virtual {p1, p3, v0}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    if-eqz v2, :cond_78

    iget-object v3, p0, Landroid/support/a/a/b;->c:Landroid/content/Context;

    if-eqz v3, :cond_7c

    iget-object v3, p0, Landroid/support/a/a/b;->c:Landroid/content/Context;

    invoke-static {v3, v2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Landroid/support/a/a/b;->a(Ljava/lang/String;Landroid/animation/Animator;)V

    :cond_78
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_4e

    :cond_7c
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Context can\'t be null when inflating animators"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic isAutoMirrored()Z
    .registers 2

    invoke-super {p0}, Landroid/support/a/a/e;->isAutoMirrored()Z

    move-result v0

    return v0
.end method

.method public isRunning()Z
    .registers 6

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->isRunning()Z

    move-result v0

    :goto_d
    return v0

    :cond_e
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v3, v0, Landroid/support/a/a/b$a;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_17
    if-ge v2, v4, :cond_2b

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_27

    const/4 v0, 0x1

    goto :goto_d

    :cond_27
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_17

    :cond_2b
    move v0, v1

    goto :goto_d
.end method

.method public isStateful()Z
    .registers 2

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0}, Landroid/support/a/a/f;->isStateful()Z

    move-result v0

    goto :goto_a
.end method

.method public bridge synthetic jumpToCurrentState()V
    .registers 1

    invoke-super {p0}, Landroid/support/a/a/e;->jumpToCurrentState()V

    return-void
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .registers 3

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    return-object p0

    :cond_a
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Mutate() is not supported for older platform"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .registers 3

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    :goto_9
    return-void

    :cond_a
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0, p1}, Landroid/support/a/a/f;->setBounds(Landroid/graphics/Rect;)V

    goto :goto_9
.end method

.method protected onLevelChange(I)Z
    .registers 3

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0, p1}, Landroid/support/a/a/f;->setLevel(I)Z

    move-result v0

    goto :goto_a
.end method

.method protected onStateChange([I)Z
    .registers 3

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0, p1}, Landroid/support/a/a/f;->setState([I)Z

    move-result v0

    goto :goto_a
.end method

.method public setAlpha(I)V
    .registers 3

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :goto_9
    return-void

    :cond_a
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0, p1}, Landroid/support/a/a/f;->setAlpha(I)V

    goto :goto_9
.end method

.method public bridge synthetic setAutoMirrored(Z)V
    .registers 2

    invoke-super {p0, p1}, Landroid/support/a/a/e;->setAutoMirrored(Z)V

    return-void
.end method

.method public bridge synthetic setChangingConfigurations(I)V
    .registers 2

    invoke-super {p0, p1}, Landroid/support/a/a/e;->setChangingConfigurations(I)V

    return-void
.end method

.method public bridge synthetic setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
    .registers 3

    invoke-super {p0, p1, p2}, Landroid/support/a/a/e;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 3

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :goto_9
    return-void

    :cond_a
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0, p1}, Landroid/support/a/a/f;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_9
.end method

.method public bridge synthetic setFilterBitmap(Z)V
    .registers 2

    invoke-super {p0, p1}, Landroid/support/a/a/e;->setFilterBitmap(Z)V

    return-void
.end method

.method public bridge synthetic setHotspot(FF)V
    .registers 3

    invoke-super {p0, p1, p2}, Landroid/support/a/a/e;->setHotspot(FF)V

    return-void
.end method

.method public bridge synthetic setHotspotBounds(IIII)V
    .registers 5

    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/a/a/e;->setHotspotBounds(IIII)V

    return-void
.end method

.method public bridge synthetic setState([I)Z
    .registers 3

    invoke-super {p0, p1}, Landroid/support/a/a/e;->setState([I)Z

    move-result v0

    return v0
.end method

.method public setTint(I)V
    .registers 3

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;I)V

    :goto_9
    return-void

    :cond_a
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0, p1}, Landroid/support/a/a/f;->setTint(I)V

    goto :goto_9
.end method

.method public setTintList(Landroid/content/res/ColorStateList;)V
    .registers 3

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    :goto_9
    return-void

    :cond_a
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0, p1}, Landroid/support/a/a/f;->setTintList(Landroid/content/res/ColorStateList;)V

    goto :goto_9
.end method

.method public setTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .registers 3

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    :goto_9
    return-void

    :cond_a
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0, p1}, Landroid/support/a/a/f;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    goto :goto_9
.end method

.method public setVisible(ZZ)Z
    .registers 4

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v0, v0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;

    invoke-virtual {v0, p1, p2}, Landroid/support/a/a/f;->setVisible(ZZ)Z

    invoke-super {p0, p1, p2}, Landroid/support/a/a/e;->setVisible(ZZ)Z

    move-result v0

    goto :goto_a
.end method

.method public start()V
    .registers 5

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->start()V

    :cond_b
    :goto_b
    return-void

    :cond_c
    invoke-direct {p0}, Landroid/support/a/a/b;->a()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v2, v0, Landroid/support/a/a/b$a;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1c
    if-ge v1, v3, :cond_2b

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1c

    :cond_2b
    invoke-virtual {p0}, Landroid/support/a/a/b;->invalidateSelf()V

    goto :goto_b
.end method

.method public stop()V
    .registers 5

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->stop()V

    :cond_b
    return-void

    :cond_c
    iget-object v0, p0, Landroid/support/a/a/b;->b:Landroid/support/a/a/b$a;

    iget-object v2, v0, Landroid/support/a/a/b$a;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_16
    if-ge v1, v3, :cond_b

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_16
.end method
