.class public final Lin/juspay/widget/qrscanner/com/google/zxing/g;
.super Ljava/lang/Object;
.source "MultiFormatReader.java"

# interfaces
.implements Lin/juspay/widget/qrscanner/com/google/zxing/i;


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lin/juspay/widget/qrscanner/com/google/zxing/d;",
            "*>;"
        }
    .end annotation
.end field

.field private b:[Lin/juspay/widget/qrscanner/com/google/zxing/i;


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private c(Lin/juspay/widget/qrscanner/com/google/zxing/c;)Lin/juspay/widget/qrscanner/com/google/zxing/j;
    .registers 7

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/g;->b:[Lin/juspay/widget/qrscanner/com/google/zxing/i;

    if-eqz v0, :cond_17

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/g;->b:[Lin/juspay/widget/qrscanner/com/google/zxing/i;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_17

    aget-object v3, v1, v0

    :try_start_c
    iget-object v4, p0, Lin/juspay/widget/qrscanner/com/google/zxing/g;->a:Ljava/util/Map;

    invoke-interface {v3, p1, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/i;->a(Lin/juspay/widget/qrscanner/com/google/zxing/c;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/j;
    :try_end_11
    .catch Lin/juspay/widget/qrscanner/com/google/zxing/ReaderException; {:try_start_c .. :try_end_11} :catch_13

    move-result-object v0

    return-object v0

    :catch_13
    move-exception v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_17
    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/c;)Lin/juspay/widget/qrscanner/com/google/zxing/j;
    .registers 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/g;->a(Ljava/util/Map;)V

    invoke-direct {p0, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/g;->c(Lin/juspay/widget/qrscanner/com/google/zxing/c;)Lin/juspay/widget/qrscanner/com/google/zxing/j;

    move-result-object v0

    return-object v0
.end method

.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/c;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/j;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lin/juspay/widget/qrscanner/com/google/zxing/c;",
            "Ljava/util/Map",
            "<",
            "Lin/juspay/widget/qrscanner/com/google/zxing/d;",
            "*>;)",
            "Lin/juspay/widget/qrscanner/com/google/zxing/j;"
        }
    .end annotation

    invoke-virtual {p0, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/g;->a(Ljava/util/Map;)V

    invoke-direct {p0, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/g;->c(Lin/juspay/widget/qrscanner/com/google/zxing/c;)Lin/juspay/widget/qrscanner/com/google/zxing/j;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 5

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/g;->b:[Lin/juspay/widget/qrscanner/com/google/zxing/i;

    if-eqz v0, :cond_12

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/g;->b:[Lin/juspay/widget/qrscanner/com/google/zxing/i;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_12

    aget-object v3, v1, v0

    invoke-interface {v3}, Lin/juspay/widget/qrscanner/com/google/zxing/i;->a()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_12
    return-void
.end method

.method public a(Ljava/util/Map;)V
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lin/juspay/widget/qrscanner/com/google/zxing/d;",
            "*>;)V"
        }
    .end annotation

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/g;->a:Ljava/util/Map;

    if-eqz p1, :cond_c

    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->d:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_c
    if-nez p1, :cond_43

    const/4 v0, 0x0

    :goto_f
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_26

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->l:Lin/juspay/widget/qrscanner/com/google/zxing/a;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a;-><init>()V

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_26
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_34

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a;-><init>()V

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_34
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Lin/juspay/widget/qrscanner/com/google/zxing/i;

    invoke-interface {v1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lin/juspay/widget/qrscanner/com/google/zxing/i;

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/g;->b:[Lin/juspay/widget/qrscanner/com/google/zxing/i;

    return-void

    :cond_43
    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->c:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_f
.end method

.method public b(Lin/juspay/widget/qrscanner/com/google/zxing/c;)Lin/juspay/widget/qrscanner/com/google/zxing/j;
    .registers 3

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/g;->b:[Lin/juspay/widget/qrscanner/com/google/zxing/i;

    if-nez v0, :cond_8

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/g;->a(Ljava/util/Map;)V

    :cond_8
    invoke-direct {p0, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/g;->c(Lin/juspay/widget/qrscanner/com/google/zxing/c;)Lin/juspay/widget/qrscanner/com/google/zxing/j;

    move-result-object v0

    return-object v0
.end method
