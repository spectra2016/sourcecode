.class public final Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a/a;
.super Ljava/lang/Object;
.source "OpenCameraInterface.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .registers 7

    const/4 v0, -0x1

    const/4 v2, 0x0

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v4

    if-nez v4, :cond_10

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a/a;->a:Ljava/lang/String;

    const-string v2, "No cameras!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    :goto_f
    return v0

    :cond_10
    if-ltz p0, :cond_29

    const/4 v1, 0x1

    move v3, v1

    :goto_14
    if-nez v3, :cond_32

    move v1, v2

    :goto_17
    if-ge v1, v4, :cond_25

    new-instance v5, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v5}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {v1, v5}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v5, v5, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-nez v5, :cond_2b

    :cond_25
    :goto_25
    if-ge v1, v4, :cond_2e

    move v0, v1

    goto :goto_f

    :cond_29
    move v3, v2

    goto :goto_14

    :cond_2b
    add-int/lit8 v1, v1, 0x1

    goto :goto_17

    :cond_2e
    if-nez v3, :cond_f

    move v0, v2

    goto :goto_f

    :cond_32
    move v1, p0

    goto :goto_25
.end method

.method public static b(I)Landroid/hardware/Camera;
    .registers 3

    invoke-static {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a/a;->a(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x0

    :goto_8
    return-object v0

    :cond_9
    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    goto :goto_8
.end method
