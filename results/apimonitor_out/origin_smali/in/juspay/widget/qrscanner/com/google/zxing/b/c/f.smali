.class public final Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;
.super Ljava/lang/Object;
.source "QRCode.java"


# instance fields
.field private a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

.field private b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

.field private c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;

.field private d:I

.field private e:Lin/juspay/widget/qrscanner/com/google/zxing/b/c/b;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;->d:I

    return-void
.end method

.method public static b(I)Z
    .registers 2

    if-ltz p0, :cond_8

    const/16 v0, 0x8

    if-ge p0, v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method


# virtual methods
.method public a()Lin/juspay/widget/qrscanner/com/google/zxing/b/c/b;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;->e:Lin/juspay/widget/qrscanner/com/google/zxing/b/c/b;

    return-object v0
.end method

.method public a(I)V
    .registers 2

    iput p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;->d:I

    return-void
.end method

.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    return-void
.end method

.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    return-void
.end method

.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;

    return-void
.end method

.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/b/c/b;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;->e:Lin/juspay/widget/qrscanner/com/google/zxing/b/c/b;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "<<\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " mode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n ecLevel: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n version: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n maskPattern: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;->e:Lin/juspay/widget/qrscanner/com/google/zxing/b/c/b;

    if-nez v1, :cond_47

    const-string v1, "\n matrix: null\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3d
    const-string v1, ">>\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_47
    const-string v1, "\n matrix:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/c/f;->e:Lin/juspay/widget/qrscanner/com/google/zxing/b/c/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_3d
.end method
