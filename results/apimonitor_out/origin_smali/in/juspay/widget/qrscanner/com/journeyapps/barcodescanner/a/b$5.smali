.class Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$5;
.super Ljava/lang/Object;
.source "CameraInstance.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;


# direct methods
.method constructor <init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$5;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    :try_start_0
    invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Starting preview"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$5;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$5;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-static {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->d(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$5;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->c()V
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_21} :catch_22

    :goto_21
    return-void

    :catch_22
    move-exception v0

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$5;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-static {v1, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;Ljava/lang/Exception;)V

    invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to start preview"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_21
.end method
