.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
.super Ljava/lang/Object;
.source "CameraSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;


# direct methods
.method public constructor <init>()V
    .registers 3

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->a:I

    iput-boolean v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->b:Z

    iput-boolean v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->c:Z

    iput-boolean v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->d:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->e:Z

    iput-boolean v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->f:Z

    iput-boolean v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->g:Z

    iput-boolean v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->h:Z

    sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->i:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;

    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->a:I

    return v0
.end method

.method public b()Z
    .registers 2

    iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->b:Z

    return v0
.end method

.method public c()Z
    .registers 2

    iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->c:Z

    return v0
.end method

.method public d()Z
    .registers 2

    iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->g:Z

    return v0
.end method

.method public e()Z
    .registers 2

    iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->d:Z

    return v0
.end method

.method public f()Z
    .registers 2

    iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->e:Z

    return v0
.end method

.method public g()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->i:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;

    return-object v0
.end method

.method public h()Z
    .registers 2

    iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->h:Z

    return v0
.end method
