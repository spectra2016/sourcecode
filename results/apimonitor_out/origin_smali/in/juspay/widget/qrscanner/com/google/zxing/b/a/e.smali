.class public final Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;
.super Ljava/lang/Object;
.source "Decoder.java"


# instance fields
.field private final a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;


# direct methods
.method public constructor <init>()V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->e:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-direct {v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;

    return-void
.end method

.method private a(Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/common/e;
    .registers 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;",
            "Ljava/util/Map",
            "<",
            "Lin/juspay/widget/qrscanner/com/google/zxing/d;",
            "*>;)",
            "Lin/juspay/widget/qrscanner/com/google/zxing/common/e;"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->b()Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;

    move-result-object v5

    invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a()Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;->a()Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    move-result-object v6

    invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->c()[B

    move-result-object v0

    invoke-static {v0, v5, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->a([BLin/juspay/widget/qrscanner/com/google/zxing/b/a/j;Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;)[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;

    move-result-object v7

    array-length v3, v7

    move v0, v1

    move v2, v1

    :goto_18
    if-ge v0, v3, :cond_24

    aget-object v4, v7, v0

    invoke-virtual {v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->a()I

    move-result v4

    add-int/2addr v2, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    :cond_24
    new-array v8, v2, [B

    array-length v9, v7

    move v4, v1

    move v0, v1

    :goto_29
    if-ge v4, v9, :cond_4b

    aget-object v2, v7, v4

    invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->b()[B

    move-result-object v10

    invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->a()I

    move-result v11

    invoke-direct {p0, v10, v11}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;->a([BI)V

    move v2, v0

    move v0, v1

    :goto_3a
    if-ge v0, v11, :cond_46

    add-int/lit8 v3, v2, 0x1

    aget-byte v12, v10, v0

    aput-byte v12, v8, v2

    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_3a

    :cond_46
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v0, v2

    goto :goto_29

    :cond_4b
    invoke-static {v8, v5, v6, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a([BLin/juspay/widget/qrscanner/com/google/zxing/b/a/j;Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/common/e;

    move-result-object v0

    return-object v0
.end method

.method private a([BI)V
    .registers 8

    const/4 v0, 0x0

    array-length v2, p1

    new-array v3, v2, [I

    move v1, v0

    :goto_5
    if-ge v1, v2, :cond_10

    aget-byte v4, p1, v1

    and-int/lit16 v4, v4, 0xff

    aput v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_10
    :try_start_10
    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;

    array-length v2, p1

    sub-int/2addr v2, p2

    invoke-virtual {v1, v3, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a([II)V
    :try_end_17
    .catch Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException; {:try_start_10 .. :try_end_17} :catch_21

    :goto_17
    if-ge v0, p2, :cond_27

    aget v1, v3, v0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_17

    :catch_21
    move-exception v0

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    :cond_27
    return-void
.end method


# virtual methods
.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/common/e;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lin/juspay/widget/qrscanner/com/google/zxing/common/b;",
            "Ljava/util/Map",
            "<",
            "Lin/juspay/widget/qrscanner/com/google/zxing/d;",
            "*>;)",
            "Lin/juspay/widget/qrscanner/com/google/zxing/common/e;"
        }
    .end annotation

    const/4 v0, 0x0

    new-instance v3, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;

    invoke-direct {v3, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;)V

    :try_start_6
    invoke-direct {p0, v3, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;->a(Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/common/e;
    :try_end_9
    .catch Lin/juspay/widget/qrscanner/com/google/zxing/FormatException; {:try_start_6 .. :try_end_9} :catch_b
    .catch Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException; {:try_start_6 .. :try_end_9} :catch_30

    move-result-object v0

    :goto_a
    return-object v0

    :catch_b
    move-exception v1

    move-object v2, v1

    move-object v1, v0

    :goto_e
    :try_start_e
    invoke-virtual {v3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->d()V

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a(Z)V

    invoke-virtual {v3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->b()Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;

    invoke-virtual {v3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a()Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;

    invoke-virtual {v3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->e()V

    invoke-direct {p0, v3, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;->a(Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/common/e;

    move-result-object v0

    new-instance v3, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/i;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/i;-><init>(Z)V

    invoke-virtual {v0, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->a(Ljava/lang/Object;)V
    :try_end_2b
    .catch Lin/juspay/widget/qrscanner/com/google/zxing/FormatException; {:try_start_e .. :try_end_2b} :catch_2c
    .catch Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException; {:try_start_e .. :try_end_2b} :catch_37

    goto :goto_a

    :catch_2c
    move-exception v0

    :goto_2d
    if-eqz v2, :cond_33

    throw v2

    :catch_30
    move-exception v1

    move-object v2, v0

    goto :goto_e

    :cond_33
    if-eqz v1, :cond_36

    throw v1

    :cond_36
    throw v0

    :catch_37
    move-exception v0

    goto :goto_2d
.end method
