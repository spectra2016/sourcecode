.class public final enum Lin/juspay/widget/qrscanner/com/google/zxing/k;
.super Ljava/lang/Enum;
.source "ResultMetadataType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lin/juspay/widget/qrscanner/com/google/zxing/k;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lin/juspay/widget/qrscanner/com/google/zxing/k;

.field public static final enum b:Lin/juspay/widget/qrscanner/com/google/zxing/k;

.field public static final enum c:Lin/juspay/widget/qrscanner/com/google/zxing/k;

.field public static final enum d:Lin/juspay/widget/qrscanner/com/google/zxing/k;

.field public static final enum e:Lin/juspay/widget/qrscanner/com/google/zxing/k;

.field public static final enum f:Lin/juspay/widget/qrscanner/com/google/zxing/k;

.field public static final enum g:Lin/juspay/widget/qrscanner/com/google/zxing/k;

.field public static final enum h:Lin/juspay/widget/qrscanner/com/google/zxing/k;

.field public static final enum i:Lin/juspay/widget/qrscanner/com/google/zxing/k;

.field public static final enum j:Lin/juspay/widget/qrscanner/com/google/zxing/k;

.field public static final enum k:Lin/juspay/widget/qrscanner/com/google/zxing/k;

.field private static final synthetic l:[Lin/juspay/widget/qrscanner/com/google/zxing/k;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;->a:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;

    const-string v1, "ORIENTATION"

    invoke-direct {v0, v1, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;->b:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;

    const-string v1, "BYTE_SEGMENTS"

    invoke-direct {v0, v1, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;->c:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;

    const-string v1, "ERROR_CORRECTION_LEVEL"

    invoke-direct {v0, v1, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;->d:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;

    const-string v1, "ISSUE_NUMBER"

    invoke-direct {v0, v1, v7}, Lin/juspay/widget/qrscanner/com/google/zxing/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;->e:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;

    const-string v1, "SUGGESTED_PRICE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;->f:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;

    const-string v1, "POSSIBLE_COUNTRY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;->g:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;

    const-string v1, "UPC_EAN_EXTENSION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;->h:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;

    const-string v1, "PDF417_EXTRA_METADATA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;->i:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;

    const-string v1, "STRUCTURED_APPEND_SEQUENCE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;->j:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;

    const-string v1, "STRUCTURED_APPEND_PARITY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;->k:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    const/16 v0, 0xb

    new-array v0, v0, [Lin/juspay/widget/qrscanner/com/google/zxing/k;

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/k;->a:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    aput-object v1, v0, v3

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/k;->b:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    aput-object v1, v0, v4

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/k;->c:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    aput-object v1, v0, v5

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/k;->d:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    aput-object v1, v0, v6

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/k;->e:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/k;->f:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/k;->g:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/k;->h:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/k;->i:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/k;->j:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/k;->k:Lin/juspay/widget/qrscanner/com/google/zxing/k;

    aput-object v2, v0, v1

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;->l:[Lin/juspay/widget/qrscanner/com/google/zxing/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lin/juspay/widget/qrscanner/com/google/zxing/k;
    .registers 2

    const-class v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;

    return-object v0
.end method

.method public static values()[Lin/juspay/widget/qrscanner/com/google/zxing/k;
    .registers 1

    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/k;->l:[Lin/juspay/widget/qrscanner/com/google/zxing/k;

    invoke-virtual {v0}, [Lin/juspay/widget/qrscanner/com/google/zxing/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lin/juspay/widget/qrscanner/com/google/zxing/k;

    return-object v0
.end method
