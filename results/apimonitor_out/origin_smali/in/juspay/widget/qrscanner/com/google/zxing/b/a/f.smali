.class public final enum Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;
.super Ljava/lang/Enum;
.source "ErrorCorrectionLevel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

.field public static final enum b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

.field public static final enum c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

.field public static final enum d:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

.field private static final e:[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

.field private static final synthetic g:[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    const-string v1, "L"

    invoke-direct {v0, v1, v2, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    const-string v1, "M"

    invoke-direct {v0, v1, v3, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    const-string v1, "Q"

    invoke-direct {v0, v1, v4, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    const-string v1, "H"

    invoke-direct {v0, v1, v5, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->d:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    new-array v0, v6, [Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    aput-object v1, v0, v2

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    aput-object v1, v0, v3

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    aput-object v1, v0, v4

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->d:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    aput-object v1, v0, v5

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->g:[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    new-array v0, v6, [Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    aput-object v1, v0, v2

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    aput-object v1, v0, v3

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->d:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    aput-object v1, v0, v4

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    aput-object v1, v0, v5

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->e:[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->f:I

    return-void
.end method

.method public static a(I)Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;
    .registers 2

    if-ltz p0, :cond_7

    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->e:[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    array-length v0, v0

    if-lt p0, v0, :cond_d

    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_d
    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->e:[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;
    .registers 2

    const-class v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    return-object v0
.end method

.method public static values()[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;
    .registers 1

    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->g:[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    invoke-virtual {v0}, [Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    iget v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->f:I

    return v0
.end method
