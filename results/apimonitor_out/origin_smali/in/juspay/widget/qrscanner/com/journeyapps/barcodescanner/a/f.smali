.class Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
.super Ljava/lang/Object;
.source "CameraThread.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;


# instance fields
.field private c:Landroid/os/Handler;

.field private d:Landroid/os/HandlerThread;

.field private e:I

.field private final f:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->e:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->f:Ljava/lang/Object;

    return-void
.end method

.method public static a()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
    .registers 1

    sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;

    if-nez v0, :cond_b

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;-><init>()V

    sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;

    :cond_b
    sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;

    return-object v0
.end method

.method private c()V
    .registers 4

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->c:Landroid/os/Handler;

    if-nez v0, :cond_31

    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->e:I

    if-gtz v0, :cond_16

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "CameraThread is not open"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    throw v0

    :cond_16
    :try_start_16
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "CameraThread"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->d:Landroid/os/HandlerThread;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->d:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->d:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->c:Landroid/os/Handler;

    :cond_31
    monitor-exit v1
    :try_end_32
    .catchall {:try_start_16 .. :try_end_32} :catchall_13

    return-void
.end method

.method private d()V
    .registers 3

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->d:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    const/4 v0, 0x0

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->d:Landroid/os/HandlerThread;

    const/4 v0, 0x0

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->c:Landroid/os/Handler;

    monitor-exit v1

    return-void

    :catchall_10
    move-exception v0

    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    throw v0
.end method


# virtual methods
.method protected a(Ljava/lang/Runnable;)V
    .registers 4

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->c()V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->c:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    monitor-exit v1

    return-void

    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method protected b()V
    .registers 3

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->e:I

    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->e:I

    if-nez v0, :cond_10

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->d()V

    :cond_10
    monitor-exit v1

    return-void

    :catchall_12
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    throw v0
.end method

.method protected b(Ljava/lang/Runnable;)V
    .registers 4

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->e:I

    invoke-virtual {p0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->a(Ljava/lang/Runnable;)V

    monitor-exit v1

    return-void

    :catchall_e
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    throw v0
.end method
