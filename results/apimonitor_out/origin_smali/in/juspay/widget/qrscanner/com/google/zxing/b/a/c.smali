.class abstract enum Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;
.super Ljava/lang/Enum;
.source "DataMask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

.field public static final enum b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

.field public static final enum c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

.field public static final enum d:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

.field public static final enum e:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

.field public static final enum f:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

.field public static final enum g:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

.field public static final enum h:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

.field private static final synthetic i:[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$1;

    const-string v1, "DATA_MASK_000"

    invoke-direct {v0, v1, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$2;

    const-string v1, "DATA_MASK_001"

    invoke-direct {v0, v1, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$3;

    const-string v1, "DATA_MASK_010"

    invoke-direct {v0, v1, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$4;

    const-string v1, "DATA_MASK_011"

    invoke-direct {v0, v1, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->d:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$5;

    const-string v1, "DATA_MASK_100"

    invoke-direct {v0, v1, v7}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$5;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->e:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$6;

    const-string v1, "DATA_MASK_101"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$6;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->f:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$7;

    const-string v1, "DATA_MASK_110"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$7;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->g:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$8;

    const-string v1, "DATA_MASK_111"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c$8;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->h:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    const/16 v0, 0x8

    new-array v0, v0, [Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    aput-object v1, v0, v3

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    aput-object v1, v0, v4

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    aput-object v1, v0, v5

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->d:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    aput-object v1, v0, v6

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->e:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->f:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->g:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->h:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    aput-object v2, v0, v1

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->i:[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILin/juspay/widget/qrscanner/com/google/zxing/b/a/c$1;)V
    .registers 4

    invoke-direct {p0, p1, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;
    .registers 2

    const-class v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    return-object v0
.end method

.method public static values()[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;
    .registers 1

    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->i:[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    invoke-virtual {v0}, [Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;

    return-object v0
.end method


# virtual methods
.method final a(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;I)V
    .registers 7

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, p2, :cond_17

    move v0, v1

    :goto_5
    if-ge v0, p2, :cond_13

    invoke-virtual {p0, v2, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->a(II)Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-virtual {p1, v0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c(II)V

    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_13
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_17
    return-void
.end method

.method abstract a(II)Z
.end method
