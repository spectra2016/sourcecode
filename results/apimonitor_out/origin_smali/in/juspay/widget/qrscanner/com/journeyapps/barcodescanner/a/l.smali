.class public abstract Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;
.super Ljava/lang/Object;
.source "PreviewScalingStrategy.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)F
    .registers 4

    const/high16 v0, 0x3f00

    return v0
.end method

.method public a(Ljava/util/List;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;",
            ">;",
            "Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;",
            ")",
            "Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;->b(Ljava/util/List;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Viewfinder size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Preview in order of preference: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    return-object v0
.end method

.method public abstract b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Landroid/graphics/Rect;
.end method

.method public b(Ljava/util/List;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;",
            ">;",
            "Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;",
            ")",
            "Ljava/util/List",
            "<",
            "Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;",
            ">;"
        }
    .end annotation

    if-nez p2, :cond_3

    :goto_2
    return-object p1

    :cond_3
    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l$1;

    invoke-direct {v0, p0, p2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l$1;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_2
.end method
