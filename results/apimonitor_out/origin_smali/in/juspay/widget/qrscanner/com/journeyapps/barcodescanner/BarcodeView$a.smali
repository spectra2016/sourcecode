.class final enum Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
.super Ljava/lang/Enum;
.source "BarcodeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

.field public static final enum b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

.field public static final enum c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

.field private static final synthetic d:[Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    const-string v1, "SINGLE"

    invoke-direct {v0, v1, v3}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    const-string v1, "CONTINUOUS"

    invoke-direct {v0, v1, v4}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    const/4 v0, 0x3

    new-array v0, v0, [Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    aput-object v1, v0, v2

    sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    aput-object v1, v0, v3

    sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    aput-object v1, v0, v4

    sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->d:[Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
    .registers 2

    const-class v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    return-object v0
.end method

.method public static values()[Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
    .registers 1

    sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->d:[Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    invoke-virtual {v0}, [Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    return-object v0
.end method
