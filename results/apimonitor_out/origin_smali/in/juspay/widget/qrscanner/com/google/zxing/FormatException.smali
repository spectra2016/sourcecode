.class public final Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;
.super Lin/juspay/widget/qrscanner/com/google/zxing/ReaderException;
.source "FormatException.java"


# static fields
.field private static final c:Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;-><init>()V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->c:Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->c:Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->b:[Ljava/lang/StackTraceElement;

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/ReaderException;-><init>()V

    return-void
.end method

.method public static a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;
    .registers 1

    sget-boolean v0, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a:Z

    if-eqz v0, :cond_a

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;-><init>()V

    :goto_9
    return-object v0

    :cond_a
    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->c:Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    goto :goto_9
.end method
