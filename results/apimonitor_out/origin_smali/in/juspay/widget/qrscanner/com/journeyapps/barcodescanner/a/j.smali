.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/j;
.super Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;
.source "FitXYStrategy.java"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/j;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;-><init>()V

    return-void
.end method

.method private static a(F)F
    .registers 3

    const/high16 v1, 0x3f80

    cmpg-float v0, p0, v1

    if-gez v0, :cond_8

    div-float p0, v1, p0

    :cond_8
    return p0
.end method


# virtual methods
.method protected a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)F
    .registers 8

    const/high16 v4, 0x3f80

    iget v0, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    if-lez v0, :cond_a

    iget v0, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    if-gtz v0, :cond_c

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0

    :cond_c
    iget v0, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    int-to-float v0, v0

    mul-float/2addr v0, v4

    iget v1, p2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/j;->a(F)F

    move-result v0

    iget v1, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    int-to-float v1, v1

    mul-float/2addr v1, v4

    iget v2, p2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/j;->a(F)F

    move-result v1

    div-float v0, v4, v0

    div-float/2addr v0, v1

    iget v1, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    int-to-float v1, v1

    mul-float/2addr v1, v4

    iget v2, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    iget v3, p2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    div-float/2addr v1, v2

    invoke-static {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/j;->a(F)F

    move-result v1

    div-float v2, v4, v1

    div-float/2addr v2, v1

    div-float v1, v2, v1

    mul-float/2addr v0, v1

    goto :goto_b
.end method

.method public b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Landroid/graphics/Rect;
    .registers 7

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/Rect;

    iget v1, p2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    iget v2, p2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method
