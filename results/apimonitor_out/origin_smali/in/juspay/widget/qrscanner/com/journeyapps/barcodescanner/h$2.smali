.class Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$2;
.super Ljava/lang/Object;
.source "DecoderThread.java"

# interfaces
.implements Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;


# direct methods
.method constructor <init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$2;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;)V
    .registers 5

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$2;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;

    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_7
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$2;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;

    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->c(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;)Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$2;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;

    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->d(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;)Landroid/os/Handler;

    move-result-object v0

    sget v2, Lin/juspay/widget/qrscanner/a$b;->zxing_decode:I

    invoke-virtual {v0, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_1e
    monitor-exit v1

    return-void

    :catchall_20
    move-exception v0

    monitor-exit v1
    :try_end_22
    .catchall {:try_start_7 .. :try_end_22} :catchall_20

    throw v0
.end method

.method public a(Ljava/lang/Exception;)V
    .registers 5

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$2;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;

    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_7
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$2;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;

    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->c(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;)Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$2;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;

    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->d(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;)Landroid/os/Handler;

    move-result-object v0

    sget v2, Lin/juspay/widget/qrscanner/a$b;->zxing_preview_failed:I

    invoke-virtual {v0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_1e
    monitor-exit v1

    return-void

    :catchall_20
    move-exception v0

    monitor-exit v1
    :try_end_22
    .catchall {:try_start_7 .. :try_end_22} :catchall_20

    throw v0
.end method
