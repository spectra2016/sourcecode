.class public final Lin/juspay/widget/qrscanner/com/google/zxing/common/e;
.super Ljava/lang/Object;
.source "DecoderResult.java"


# instance fields
.field private final a:[B

.field private b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/String;

.field private f:Ljava/lang/Object;

.field private final g:I

.field private final h:I


# direct methods
.method public constructor <init>([BLjava/lang/String;Ljava/util/List;Ljava/lang/String;II)V
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->a:[B

    if-nez p1, :cond_15

    const/4 v0, 0x0

    :goto_8
    iput v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->b:I

    iput-object p2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->c:Ljava/lang/String;

    iput-object p3, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->d:Ljava/util/List;

    iput-object p4, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->e:Ljava/lang/String;

    iput p6, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->g:I

    iput p5, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->h:I

    return-void

    :cond_15
    array-length v0, p1

    mul-int/lit8 v0, v0, 0x8

    goto :goto_8
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->f:Ljava/lang/Object;

    return-void
.end method

.method public a()[B
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->a:[B

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->d:Ljava/util/List;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/Object;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->f:Ljava/lang/Object;

    return-object v0
.end method

.method public f()Z
    .registers 2

    iget v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->g:I

    if-ltz v0, :cond_a

    iget v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->h:I

    if-ltz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public g()I
    .registers 2

    iget v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->g:I

    return v0
.end method

.method public h()I
    .registers 2

    iget v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->h:I

    return v0
.end method
