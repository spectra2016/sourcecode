.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;
.super Ljava/lang/Object;
.source "SourceData.java"


# instance fields
.field private a:[B

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>([BIIII)V
    .registers 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->a:[B

    iput p2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->b:I

    iput p3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->c:I

    iput p5, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->e:I

    iput p4, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->d:I

    mul-int v0, p2, p3

    array-length v1, p1

    if-le v0, v1, :cond_40

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Image data does not match the resolution. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_40
    return-void
.end method

.method public static a(I[BII)[B
    .registers 4

    sparse-switch p0, :sswitch_data_14

    :goto_3
    :sswitch_3
    return-object p1

    :sswitch_4
    invoke-static {p1, p2, p3}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->a([BII)[B

    move-result-object p1

    goto :goto_3

    :sswitch_9
    invoke-static {p1, p2, p3}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->b([BII)[B

    move-result-object p1

    goto :goto_3

    :sswitch_e
    invoke-static {p1, p2, p3}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->c([BII)[B

    move-result-object p1

    goto :goto_3

    nop

    :sswitch_data_14
    .sparse-switch
        0x0 -> :sswitch_3
        0x5a -> :sswitch_4
        0xb4 -> :sswitch_9
        0x10e -> :sswitch_e
    .end sparse-switch
.end method

.method public static a([BII)[B
    .registers 8

    const/4 v0, 0x0

    mul-int v1, p1, p2

    new-array v3, v1, [B

    move v2, v0

    move v1, v0

    :goto_7
    if-ge v2, p1, :cond_1d

    add-int/lit8 v0, p2, -0x1

    :goto_b
    if-ltz v0, :cond_19

    mul-int v4, v0, p1

    add-int/2addr v4, v2

    aget-byte v4, p0, v4

    aput-byte v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_b

    :cond_19
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    :cond_1d
    return-object v3
.end method

.method public static b([BII)[B
    .registers 8

    mul-int v2, p1, p2

    new-array v3, v2, [B

    add-int/lit8 v1, v2, -0x1

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v2, :cond_12

    aget-byte v4, p0, v0

    aput-byte v4, v3, v1

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_12
    return-object v3
.end method

.method public static c([BII)[B
    .registers 8

    mul-int v0, p1, p2

    new-array v3, v0, [B

    add-int/lit8 v1, v0, -0x1

    const/4 v0, 0x0

    move v2, v0

    :goto_8
    if-ge v2, p1, :cond_1e

    add-int/lit8 v0, p2, -0x1

    :goto_c
    if-ltz v0, :cond_1a

    mul-int v4, v0, p1

    add-int/2addr v4, v2

    aget-byte v4, p0, v4

    aput-byte v4, v3, v1

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_c

    :cond_1a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    :cond_1e
    return-object v3
.end method


# virtual methods
.method public a(Landroid/graphics/Rect;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->f:Landroid/graphics/Rect;

    return-void
.end method

.method public a()Z
    .registers 2

    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->e:I

    rem-int/lit16 v0, v0, 0xb4

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public b()Lin/juspay/widget/qrscanner/com/google/zxing/h;
    .registers 10

    const/4 v8, 0x0

    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->e:I

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->a:[B

    iget v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->b:I

    iget v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->c:I

    invoke-static {v0, v1, v2, v3}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->a(I[BII)[B

    move-result-object v1

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->a()Z

    move-result v0

    if-eqz v0, :cond_31

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/h;

    iget v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->c:I

    iget v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->b:I

    iget-object v4, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->f:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->f:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->f:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    iget-object v7, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->f:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    invoke-direct/range {v0 .. v8}, Lin/juspay/widget/qrscanner/com/google/zxing/h;-><init>([BIIIIIIZ)V

    :goto_30
    return-object v0

    :cond_31
    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/h;

    iget v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->b:I

    iget v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->c:I

    iget-object v4, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->f:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->f:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->f:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    iget-object v7, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->f:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    invoke-direct/range {v0 .. v8}, Lin/juspay/widget/qrscanner/com/google/zxing/h;-><init>([BIIIIIIZ)V

    goto :goto_30
.end method
