.class public final Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;
.super Ljava/lang/Object;
.source "ReedSolomonDecoder.java"


# instance fields
.field private final a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;


# direct methods
.method public constructor <init>(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;)V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    return-void
.end method

.method private a(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;)[I
    .registers 7

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b()I

    move-result v3

    if-ne v3, v0, :cond_12

    new-array v2, v0, [I

    invoke-virtual {p1, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->a(I)I

    move-result v0

    aput v0, v2, v1

    move-object v0, v2

    :goto_11
    return-object v0

    :cond_12
    new-array v2, v3, [I

    :goto_14
    iget-object v4, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v4}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->c()I

    move-result v4

    if-ge v0, v4, :cond_31

    if-ge v1, v3, :cond_31

    invoke-virtual {p1, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b(I)I

    move-result v4

    if-nez v4, :cond_2e

    iget-object v4, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v4, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->c(I)I

    move-result v4

    aput v4, v2, v1

    add-int/lit8 v1, v1, 0x1

    :cond_2e
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    :cond_31
    if-eq v1, v3, :cond_3b

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException;

    const-string v1, "Error locator degree does not match number of roots"

    invoke-direct {v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3b
    move-object v0, v2

    goto :goto_11
.end method

.method private a(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;[I)[I
    .registers 12

    const/4 v3, 0x0

    array-length v5, p2

    new-array v6, v5, [I

    move v4, v3

    :goto_5
    if-ge v4, v5, :cond_59

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    aget v1, p2, v4

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->c(I)I

    move-result v7

    const/4 v1, 0x1

    move v2, v3

    :goto_11
    if-ge v2, v5, :cond_31

    if-eq v4, v2, :cond_5a

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    aget v8, p2, v2

    invoke-virtual {v0, v8, v7}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->c(II)I

    move-result v0

    and-int/lit8 v8, v0, 0x1

    if-nez v8, :cond_2e

    or-int/lit8 v0, v0, 0x1

    :goto_23
    iget-object v8, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v8, v1, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->c(II)I

    move-result v0

    :goto_29
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_11

    :cond_2e
    and-int/lit8 v0, v0, -0x2

    goto :goto_23

    :cond_31
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {p1, v7}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b(I)I

    move-result v2

    iget-object v8, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v8, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->c(I)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->c(II)I

    move-result v0

    aput v0, v6, v4

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->d()I

    move-result v0

    if-eqz v0, :cond_55

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    aget v1, v6, v4

    invoke-virtual {v0, v1, v7}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->c(II)I

    move-result v0

    aput v0, v6, v4

    :cond_55
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_5

    :cond_59
    return-object v6

    :cond_5a
    move v0, v1

    goto :goto_29
.end method

.method private a(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;I)[Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;
    .registers 14

    const/4 v8, 0x0

    invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b()I

    move-result v0

    invoke-virtual {p2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b()I

    move-result v1

    if-ge v0, v1, :cond_c3

    :goto_b
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->a()Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    move-result-object v1

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->b()Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    move-result-object v0

    :goto_17
    invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b()I

    move-result v2

    div-int/lit8 v3, p3, 0x2

    if-lt v2, v3, :cond_9e

    invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->c()Z

    move-result v2

    if-eqz v2, :cond_2d

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException;

    const-string v1, "r_{i-1} was zero"

    invoke-direct {v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2d
    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->a()Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    move-result-object v2

    invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->a(I)I

    move-result v3

    iget-object v4, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v4, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->c(I)I

    move-result v4

    move-object v3, v2

    move-object v2, p2

    :goto_43
    invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b()I

    move-result v5

    invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b()I

    move-result v6

    if-lt v5, v6, :cond_7d

    invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->c()Z

    move-result v5

    if-nez v5, :cond_7d

    invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b()I

    move-result v5

    invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b()I

    move-result v7

    invoke-virtual {v2, v7}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->a(I)I

    move-result v7

    invoke-virtual {v6, v7, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->c(II)I

    move-result v6

    iget-object v7, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v7, v5, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->a(II)Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    move-result-object v7

    invoke-virtual {v3, v7}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;)Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    move-result-object v3

    invoke-virtual {p1, v5, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->a(II)Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    move-result-object v5

    invoke-virtual {v2, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;)Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    move-result-object v2

    goto :goto_43

    :cond_7d
    invoke-virtual {v3, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;)Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    move-result-object v3

    invoke-virtual {v3, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;)Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    move-result-object v1

    invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b()I

    move-result v3

    invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b()I

    move-result v4

    if-lt v3, v4, :cond_97

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Division algorithm failed to reduce polynomial?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_97
    move-object p2, p1

    move-object p1, v2

    move-object v9, v0

    move-object v0, v1

    move-object v1, v9

    goto/16 :goto_17

    :cond_9e
    invoke-virtual {v0, v8}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->a(I)I

    move-result v1

    if-nez v1, :cond_ac

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException;

    const-string v1, "sigmaTilde(0) was zero"

    invoke-direct {v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_ac
    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v2, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->c(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->c(I)Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    move-result-object v0

    invoke-virtual {p1, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->c(I)Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    aput-object v0, v2, v8

    const/4 v0, 0x1

    aput-object v1, v2, v0

    return-object v2

    :cond_c3
    move-object v9, p2

    move-object p2, p1

    move-object p1, v9

    goto/16 :goto_b
.end method


# virtual methods
.method public a([II)V
    .registers 11

    const/4 v3, 0x1

    const/4 v1, 0x0

    new-instance v4, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-direct {v4, v0, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;[I)V

    new-array v5, p2, [I

    move v2, v1

    move v0, v3

    :goto_d
    if-ge v2, p2, :cond_2c

    iget-object v6, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    iget-object v7, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v7}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->d()I

    move-result v7

    add-int/2addr v7, v2

    invoke-virtual {v6, v7}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->a(I)I

    move-result v6

    invoke-virtual {v4, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;->b(I)I

    move-result v6

    array-length v7, v5

    add-int/lit8 v7, v7, -0x1

    sub-int/2addr v7, v2

    aput v6, v5, v7

    if-eqz v6, :cond_29

    move v0, v1

    :cond_29
    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    :cond_2c
    if-eqz v0, :cond_2f

    :cond_2e
    return-void

    :cond_2f
    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-direct {v0, v2, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;[I)V

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    invoke-virtual {v2, p2, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->a(II)Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    move-result-object v2

    invoke-direct {p0, v2, v0, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;I)[Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;

    move-result-object v0

    aget-object v2, v0, v1

    aget-object v0, v0, v3

    invoke-direct {p0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;)[I

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;[I)[I

    move-result-object v0

    :goto_4c
    array-length v3, v2

    if-ge v1, v3, :cond_2e

    array-length v3, p1

    add-int/lit8 v3, v3, -0x1

    iget-object v4, p0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;

    aget v5, v2, v1

    invoke-virtual {v4, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->b(I)I

    move-result v4

    sub-int/2addr v3, v4

    if-gez v3, :cond_65

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException;

    const-string v1, "Bad error location"

    invoke-direct {v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_65
    aget v4, p1, v3

    aget v5, v0, v1

    invoke-static {v4, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;->b(II)I

    move-result v4

    aput v4, p1, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_4c
.end method
