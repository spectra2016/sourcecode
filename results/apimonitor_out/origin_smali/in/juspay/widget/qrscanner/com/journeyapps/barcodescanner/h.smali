.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;
.super Ljava/lang/Object;
.source "DecoderThread.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

.field private c:Landroid/os/HandlerThread;

.field private d:Landroid/os/Handler;

.field private e:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;

.field private f:Landroid/os/Handler;

.field private g:Landroid/graphics/Rect;

.field private h:Z

.field private final i:Ljava/lang/Object;

.field private final j:Landroid/os/Handler$Callback;

.field private final k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;Landroid/os/Handler;)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->h:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->i:Ljava/lang/Object;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$1;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$1;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->j:Landroid/os/Handler$Callback;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$2;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$2;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;

    invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/n;->a()V

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    iput-object p2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->e:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;

    iput-object p3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->f:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;)V
    .registers 1

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->c()V

    return-void
.end method

.method static synthetic a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;)V
    .registers 2

    invoke-direct {p0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;)V

    return-void
.end method

.method static synthetic b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;)Ljava/lang/Object;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->i:Ljava/lang/Object;

    return-object v0
.end method

.method private b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;)V
    .registers 10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v0, 0x0

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->g:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->a(Landroid/graphics/Rect;)V

    invoke-virtual {p0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;)Lin/juspay/widget/qrscanner/com/google/zxing/f;

    move-result-object v1

    if-eqz v1, :cond_16

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->e:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->a(Lin/juspay/widget/qrscanner/com/google/zxing/f;)Lin/juspay/widget/qrscanner/com/google/zxing/j;

    move-result-object v0

    :cond_16
    if-eqz v0, :cond_71

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Found barcode in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sub-long v2, v4, v2

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->f:Landroid/os/Handler;

    if-eqz v1, :cond_58

    new-instance v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/b;

    invoke-direct {v1, v0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/b;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/j;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->f:Landroid/os/Handler;

    sget v2, Lin/juspay/widget/qrscanner/a$b;->zxing_decode_succeeded:I

    invoke-static {v0, v2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_58
    :goto_58
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->f:Landroid/os/Handler;

    if-eqz v0, :cond_6d

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->e:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->a()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->f:Landroid/os/Handler;

    sget v2, Lin/juspay/widget/qrscanner/a$b;->zxing_possible_result_points:I

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_6d
    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->c()V

    return-void

    :cond_71
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->f:Landroid/os/Handler;

    if-eqz v0, :cond_58

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->f:Landroid/os/Handler;

    sget v1, Lin/juspay/widget/qrscanner/a$b;->zxing_decode_failed:I

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_58
.end method

.method private c()V
    .registers 3

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->f()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;)V

    :cond_f
    return-void
.end method

.method static synthetic c(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;)Z
    .registers 2

    iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->h:Z

    return v0
.end method

.method static synthetic d(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;)Landroid/os/Handler;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->d:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;)Lin/juspay/widget/qrscanner/com/google/zxing/f;
    .registers 3

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->g:Landroid/graphics/Rect;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;->b()Lin/juspay/widget/qrscanner/com/google/zxing/h;

    move-result-object v0

    goto :goto_5
.end method

.method public a()V
    .registers 4

    invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/n;->a()V

    new-instance v0, Landroid/os/HandlerThread;

    sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->c:Landroid/os/HandlerThread;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->c:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->j:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->d:Landroid/os/Handler;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->h:Z

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->c()V

    return-void
.end method

.method public a(Landroid/graphics/Rect;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->g:Landroid/graphics/Rect;

    return-void
.end method

.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->e:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;

    return-void
.end method

.method public b()V
    .registers 4

    invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/n;->a()V

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->i:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_7
    iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->h:Z

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->d:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    monitor-exit v1

    return-void

    :catchall_16
    move-exception v0

    monitor-exit v1
    :try_end_18
    .catchall {:try_start_7 .. :try_end_18} :catchall_16

    throw v0
.end method
