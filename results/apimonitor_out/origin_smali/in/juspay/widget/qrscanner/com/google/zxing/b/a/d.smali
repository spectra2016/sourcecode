.class final Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;
.super Ljava/lang/Object;
.source "DecodedBitStreamParser.java"


# static fields
.field private static final a:[C


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-string v0, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a:[C

    return-void
.end method

.method private static a(I)C
    .registers 2

    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a:[C

    array-length v0, v0

    if-lt p0, v0, :cond_a

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_a
    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a:[C

    aget-char v0, v0, p0

    return v0
.end method

.method private static a(Lin/juspay/widget/qrscanner/com/google/zxing/common/c;)I
    .registers 5

    const/16 v3, 0x8

    invoke-virtual {p0, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v0

    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_d

    and-int/lit8 v0, v0, 0x7f

    :goto_c
    return v0

    :cond_d
    and-int/lit16 v1, v0, 0xc0

    const/16 v2, 0x80

    if-ne v1, v2, :cond_1d

    invoke-virtual {p0, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v1

    and-int/lit8 v0, v0, 0x3f

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    goto :goto_c

    :cond_1d
    and-int/lit16 v1, v0, 0xe0

    const/16 v2, 0xc0

    if-ne v1, v2, :cond_2f

    const/16 v1, 0x10

    invoke-virtual {p0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v1

    and-int/lit8 v0, v0, 0x1f

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v1

    goto :goto_c

    :cond_2f
    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0
.end method

.method static a([BLin/juspay/widget/qrscanner/com/google/zxing/b/a/j;Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/common/e;
    .registers 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;",
            "Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;",
            "Ljava/util/Map",
            "<",
            "Lin/juspay/widget/qrscanner/com/google/zxing/d;",
            "*>;)",
            "Lin/juspay/widget/qrscanner/com/google/zxing/common/e;"
        }
    .end annotation

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;-><init>([B)V

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x32

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    new-instance v4, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v5, -0x1

    const/4 v6, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    move v8, v6

    move v9, v5

    move v6, v2

    :goto_19
    :try_start_19
    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a()I

    move-result v2

    const/4 v5, 0x4

    if-ge v2, v5, :cond_4c

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    move-object v7, v2

    :goto_23
    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    if-eq v7, v2, :cond_f7

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->h:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    if-eq v7, v2, :cond_2f

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->i:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    if-ne v7, v2, :cond_57

    :cond_2f
    const/4 v6, 0x1

    move v2, v6

    move v5, v9

    move v6, v8

    :goto_33
    sget-object v8, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
    :try_end_35
    .catch Ljava/lang/IllegalArgumentException; {:try_start_19 .. :try_end_35} :catch_68

    if-ne v7, v8, :cond_fc

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_ee

    const/4 v3, 0x0

    :goto_44
    if-nez p2, :cond_f1

    const/4 v4, 0x0

    :goto_47
    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;-><init>([BLjava/lang/String;Ljava/util/List;Ljava/lang/String;II)V

    return-object v0

    :cond_4c
    const/4 v2, 0x4

    :try_start_4d
    invoke-virtual {v0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v2

    invoke-static {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->a(I)Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    move-result-object v2

    move-object v7, v2

    goto :goto_23

    :cond_57
    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->d:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    if-ne v7, v2, :cond_7e

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a()I

    move-result v2

    const/16 v5, 0x10

    if-ge v2, v5, :cond_6e

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0
    :try_end_68
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4d .. :try_end_68} :catch_68

    :catch_68
    move-exception v0

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_6e
    const/16 v2, 0x8

    :try_start_70
    invoke-virtual {v0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v9

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v8

    move v2, v6

    move v5, v9

    move v6, v8

    goto :goto_33

    :cond_7e
    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->f:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    if-ne v7, v2, :cond_95

    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/c;)I

    move-result v2

    invoke-static {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/d;->a(I)Lin/juspay/widget/qrscanner/com/google/zxing/common/d;

    move-result-object v3

    if-nez v3, :cond_91

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_91
    move v2, v6

    move v5, v9

    move v6, v8

    goto :goto_33

    :cond_95
    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->j:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    if-ne v7, v2, :cond_b0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v2

    invoke-virtual {v7, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->a(Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;)I

    move-result v5

    invoke-virtual {v0, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v5

    const/4 v10, 0x1

    if-ne v2, v10, :cond_ac

    invoke-static {v0, v1, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/c;Ljava/lang/StringBuilder;I)V

    :cond_ac
    move v2, v6

    move v5, v9

    move v6, v8

    goto :goto_33

    :cond_b0
    invoke-virtual {v7, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->a(Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;)I

    move-result v2

    invoke-virtual {v0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v2

    sget-object v5, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    if-ne v7, v5, :cond_c4

    invoke-static {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->c(Lin/juspay/widget/qrscanner/com/google/zxing/common/c;Ljava/lang/StringBuilder;I)V

    move v2, v6

    move v5, v9

    move v6, v8

    goto/16 :goto_33

    :cond_c4
    sget-object v5, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    if-ne v7, v5, :cond_d0

    invoke-static {v0, v1, v2, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/c;Ljava/lang/StringBuilder;IZ)V

    move v2, v6

    move v5, v9

    move v6, v8

    goto/16 :goto_33

    :cond_d0
    sget-object v5, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->e:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    if-ne v7, v5, :cond_dd

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/c;Ljava/lang/StringBuilder;ILin/juspay/widget/qrscanner/com/google/zxing/common/d;Ljava/util/Collection;Ljava/util/Map;)V

    move v2, v6

    move v5, v9

    move v6, v8

    goto/16 :goto_33

    :cond_dd
    sget-object v5, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->g:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;

    if-ne v7, v5, :cond_e9

    invoke-static {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->b(Lin/juspay/widget/qrscanner/com/google/zxing/common/c;Ljava/lang/StringBuilder;I)V

    move v2, v6

    move v5, v9

    move v6, v8

    goto/16 :goto_33

    :cond_e9
    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0
    :try_end_ee
    .catch Ljava/lang/IllegalArgumentException; {:try_start_70 .. :try_end_ee} :catch_68

    :cond_ee
    move-object v3, v4

    goto/16 :goto_44

    :cond_f1
    invoke-virtual {p2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_47

    :cond_f7
    move v2, v6

    move v5, v9

    move v6, v8

    goto/16 :goto_33

    :cond_fc
    move v8, v6

    move v9, v5

    move v6, v2

    goto/16 :goto_19
.end method

.method private static a(Lin/juspay/widget/qrscanner/com/google/zxing/common/c;Ljava/lang/StringBuilder;I)V
    .registers 7

    mul-int/lit8 v0, p2, 0xd

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a()I

    move-result v1

    if-le v0, v1, :cond_d

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_d
    mul-int/lit8 v0, p2, 0x2

    new-array v2, v0, [B

    const/4 v0, 0x0

    move v1, v0

    :goto_13
    if-lez p2, :cond_43

    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v0

    div-int/lit8 v3, v0, 0x60

    shl-int/lit8 v3, v3, 0x8

    rem-int/lit8 v0, v0, 0x60

    or-int/2addr v0, v3

    const/16 v3, 0x3bf

    if-ge v0, v3, :cond_3e

    const v3, 0xa1a1

    add-int/2addr v0, v3

    :goto_2a
    shr-int/lit8 v3, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    add-int/lit8 v3, v1, 0x1

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    add-int/lit8 v0, v1, 0x2

    add-int/lit8 p2, p2, -0x1

    move v1, v0

    goto :goto_13

    :cond_3e
    const v3, 0xa6a1

    add-int/2addr v0, v3

    goto :goto_2a

    :cond_43
    :try_start_43
    new-instance v0, Ljava/lang/String;

    const-string v1, "GB2312"

    invoke-direct {v0, v2, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4d
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_43 .. :try_end_4d} :catch_4e

    return-void

    :catch_4e
    move-exception v0

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0
.end method

.method private static a(Lin/juspay/widget/qrscanner/com/google/zxing/common/c;Ljava/lang/StringBuilder;ILin/juspay/widget/qrscanner/com/google/zxing/common/d;Ljava/util/Collection;Ljava/util/Map;)V
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lin/juspay/widget/qrscanner/com/google/zxing/common/c;",
            "Ljava/lang/StringBuilder;",
            "I",
            "Lin/juspay/widget/qrscanner/com/google/zxing/common/d;",
            "Ljava/util/Collection",
            "<[B>;",
            "Ljava/util/Map",
            "<",
            "Lin/juspay/widget/qrscanner/com/google/zxing/d;",
            "*>;)V"
        }
    .end annotation

    mul-int/lit8 v0, p2, 0x8

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a()I

    move-result v1

    if-le v0, v1, :cond_d

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_d
    new-array v1, p2, [B

    const/4 v0, 0x0

    :goto_10
    if-ge v0, p2, :cond_1e

    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    :cond_1e
    if-nez p3, :cond_30

    invoke-static {v1, p5}, Lin/juspay/widget/qrscanner/com/google/zxing/common/l;->a([BLjava/util/Map;)Ljava/lang/String;

    move-result-object v0

    :goto_24
    :try_start_24
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2c
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_24 .. :try_end_2c} :catch_35

    invoke-interface {p4, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void

    :cond_30
    invoke-virtual {p3}, Lin/juspay/widget/qrscanner/com/google/zxing/common/d;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_24

    :catch_35
    move-exception v0

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0
.end method

.method private static a(Lin/juspay/widget/qrscanner/com/google/zxing/common/c;Ljava/lang/StringBuilder;IZ)V
    .registers 11

    const/16 v6, 0x25

    const/16 v5, 0xb

    const/4 v4, 0x6

    const/4 v3, 0x1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    :goto_a
    if-le p2, v3, :cond_30

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a()I

    move-result v1

    if-ge v1, v5, :cond_17

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_17
    invoke-virtual {p0, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v1

    div-int/lit8 v2, v1, 0x2d

    invoke-static {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a(I)C

    move-result v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    rem-int/lit8 v1, v1, 0x2d

    invoke-static {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 p2, p2, -0x2

    goto :goto_a

    :cond_30
    if-ne p2, v3, :cond_48

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a()I

    move-result v1

    if-ge v1, v4, :cond_3d

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_3d
    invoke-virtual {p0, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v1

    invoke-static {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_48
    if-eqz p3, :cond_74

    :goto_4a
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-ge v0, v1, :cond_74

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v6, :cond_6b

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6e

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v6, :cond_6e

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    :cond_6b
    :goto_6b
    add-int/lit8 v0, v0, 0x1

    goto :goto_4a

    :cond_6e
    const/16 v1, 0x1d

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_6b

    :cond_74
    return-void
.end method

.method private static b(Lin/juspay/widget/qrscanner/com/google/zxing/common/c;Ljava/lang/StringBuilder;I)V
    .registers 7

    mul-int/lit8 v0, p2, 0xd

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a()I

    move-result v1

    if-le v0, v1, :cond_d

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_d
    mul-int/lit8 v0, p2, 0x2

    new-array v2, v0, [B

    const/4 v0, 0x0

    move v1, v0

    :goto_13
    if-lez p2, :cond_3f

    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v0

    div-int/lit16 v3, v0, 0xc0

    shl-int/lit8 v3, v3, 0x8

    rem-int/lit16 v0, v0, 0xc0

    or-int/2addr v0, v3

    const/16 v3, 0x1f00

    if-ge v0, v3, :cond_3a

    const v3, 0x8140

    add-int/2addr v0, v3

    :goto_2a
    shr-int/lit8 v3, v0, 0x8

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    add-int/lit8 v3, v1, 0x1

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    add-int/lit8 v0, v1, 0x2

    add-int/lit8 p2, p2, -0x1

    move v1, v0

    goto :goto_13

    :cond_3a
    const v3, 0xc140

    add-int/2addr v0, v3

    goto :goto_2a

    :cond_3f
    :try_start_3f
    new-instance v0, Ljava/lang/String;

    const-string v1, "SJIS"

    invoke-direct {v0, v2, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_49
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3f .. :try_end_49} :catch_4a

    return-void

    :catch_4a
    move-exception v0

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0
.end method

.method private static c(Lin/juspay/widget/qrscanner/com/google/zxing/common/c;Ljava/lang/StringBuilder;I)V
    .registers 8

    const/4 v4, 0x7

    const/4 v3, 0x4

    const/16 v2, 0xa

    :goto_4
    const/4 v0, 0x3

    if-lt p2, v0, :cond_3f

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a()I

    move-result v0

    if-ge v0, v2, :cond_12

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_12
    invoke-virtual {p0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v0

    const/16 v1, 0x3e8

    if-lt v0, v1, :cond_1f

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_1f
    div-int/lit8 v1, v0, 0x64

    invoke-static {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    div-int/lit8 v1, v0, 0xa

    rem-int/lit8 v1, v1, 0xa

    invoke-static {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    rem-int/lit8 v0, v0, 0xa

    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a(I)C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 p2, p2, -0x3

    goto :goto_4

    :cond_3f
    const/4 v0, 0x2

    if-ne p2, v0, :cond_6d

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a()I

    move-result v0

    if-ge v0, v4, :cond_4d

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_4d
    invoke-virtual {p0, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_5a

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_5a
    div-int/lit8 v1, v0, 0xa

    invoke-static {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a(I)C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    rem-int/lit8 v0, v0, 0xa

    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a(I)C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_6c
    :goto_6c
    return-void

    :cond_6d
    const/4 v0, 0x1

    if-ne p2, v0, :cond_6c

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a()I

    move-result v0

    if-ge v0, v3, :cond_7b

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_7b
    invoke-virtual {p0, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/common/c;->a(I)I

    move-result v0

    if-lt v0, v2, :cond_86

    invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_86
    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/d;->a(I)C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_6c
.end method
