.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;
.super Landroid/view/View;
.source "ViewfinderView.java"


# static fields
.field protected static final a:Ljava/lang/String;

.field protected static final b:[I


# instance fields
.field protected final c:Landroid/graphics/Paint;

.field protected d:Landroid/graphics/Bitmap;

.field protected final e:I

.field protected final f:I

.field protected final g:I

.field protected final h:I

.field protected i:I

.field protected j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lin/juspay/widget/qrscanner/com/google/zxing/l;",
            ">;"
        }
    .end annotation
.end field

.field protected k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lin/juspay/widget/qrscanner/com/google/zxing/l;",
            ">;"
        }
    .end annotation
.end field

.field protected l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;

.field protected m:Landroid/graphics/Rect;

.field protected n:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->a:Ljava/lang/String;

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_12

    sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->b:[I

    return-void

    :array_12
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x40t 0x0t 0x0t 0x0t
        0x80t 0x0t 0x0t 0x0t
        0xc0t 0x0t 0x0t 0x0t
        0xfft 0x0t 0x0t 0x0t
        0xc0t 0x0t 0x0t 0x0t
        0x80t 0x0t 0x0t 0x0t
        0x40t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lin/juspay/widget/qrscanner/a$f;->zxing_finder:[I

    invoke-virtual {v1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    sget v2, Lin/juspay/widget/qrscanner/a$f;->zxing_finder_zxing_viewfinder_mask:I

    sget v3, Lin/juspay/widget/qrscanner/a$a;->zxing_viewfinder_mask:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->e:I

    sget v2, Lin/juspay/widget/qrscanner/a$f;->zxing_finder_zxing_result_view:I

    sget v3, Lin/juspay/widget/qrscanner/a$a;->zxing_result_view:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->f:I

    sget v2, Lin/juspay/widget/qrscanner/a$f;->zxing_finder_zxing_viewfinder_laser:I

    sget v3, Lin/juspay/widget/qrscanner/a$a;->zxing_viewfinder_laser:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->g:I

    sget v2, Lin/juspay/widget/qrscanner/a$f;->zxing_finder_zxing_possible_result_points:I

    sget v3, Lin/juspay/widget/qrscanner/a$a;->zxing_possible_result_points:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->h:I

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    const/4 v0, 0x0

    iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->i:I

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->j:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->k:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected a()V
    .registers 3

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;

    if-nez v0, :cond_5

    :cond_4
    :goto_4
    return-void

    :cond_5
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getFramingRect()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;

    invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getPreviewFramingRect()Landroid/graphics/Rect;

    move-result-object v1

    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->m:Landroid/graphics/Rect;

    iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->n:Landroid/graphics/Rect;

    goto :goto_4
.end method

.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/l;)V
    .registers 5

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0x14

    if-le v1, v2, :cond_17

    const/4 v2, 0x0

    add-int/lit8 v1, v1, -0xa

    invoke-interface {v0, v2, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_17
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .registers 16
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    const/4 v13, 0x0

    const/16 v12, 0xa0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->a()V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->m:Landroid/graphics/Rect;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->n:Landroid/graphics/Rect;

    if-nez v0, :cond_10

    :cond_f
    :goto_f
    return-void

    :cond_10
    iget-object v8, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->m:Landroid/graphics/Rect;

    iget-object v9, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->n:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v10

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v11

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->d:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_75

    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->f:I

    :goto_24
    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v3, v10

    iget v0, v8, Landroid/graphics/Rect;->top:I

    int-to-float v4, v0

    iget-object v5, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget v0, v8, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget v0, v8, Landroid/graphics/Rect;->left:I

    int-to-float v3, v0

    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v0, v0, 0x1

    int-to-float v4, v0

    iget-object v5, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget v0, v8, Landroid/graphics/Rect;->right:I

    add-int/lit8 v0, v0, 0x1

    int-to-float v3, v0

    iget v0, v8, Landroid/graphics/Rect;->top:I

    int-to-float v4, v0

    int-to-float v5, v10

    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v0, v0, 0x1

    int-to-float v6, v0

    iget-object v7, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v0, v0, 0x1

    int-to-float v2, v0

    int-to-float v3, v10

    int-to-float v4, v11

    iget-object v5, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->d:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_78

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v12}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->d:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v13, v8, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_f

    :cond_75
    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->e:I

    goto :goto_24

    :cond_78
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    iget v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->g:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->b:[I

    iget v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->i:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->i:I

    add-int/lit8 v0, v0, 0x1

    sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->b:[I

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->i:I

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget v1, v8, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v1

    iget v1, v8, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-int/lit8 v2, v0, -0x1

    int-to-float v2, v2

    iget v3, v8, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    add-int/lit8 v0, v0, 0x2

    int-to-float v4, v0

    iget-object v5, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    div-float v1, v0, v1

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float v2, v0, v2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->j:Ljava/util/List;

    iget-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->k:Ljava/util/List;

    iget v4, v8, Landroid/graphics/Rect;->left:I

    iget v5, v8, Landroid/graphics/Rect;->top:I

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_113

    iput-object v13, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->k:Ljava/util/List;

    :cond_db
    if-eqz v3, :cond_151

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    const/16 v6, 0x50

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    iget v6, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->h:I

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v6, 0x4040

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_f1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_151

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/l;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F

    move-result v7

    mul-float/2addr v7, v1

    float-to-int v7, v7

    add-int/2addr v7, v4

    int-to-float v7, v7

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F

    move-result v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    add-int/2addr v0, v5

    int-to-float v0, v0

    iget-object v9, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v0, v6, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_f1

    :cond_113
    new-instance v6, Ljava/util/ArrayList;

    const/4 v7, 0x5

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v6, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->j:Ljava/util/List;

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->k:Ljava/util/List;

    iget-object v6, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    invoke-virtual {v6, v12}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v6, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    iget v7, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->h:I

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_12d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_db

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/l;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F

    move-result v7

    mul-float/2addr v7, v1

    float-to-int v7, v7

    add-int/2addr v7, v4

    int-to-float v7, v7

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F

    move-result v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    add-int/2addr v0, v5

    int-to-float v0, v0

    const/high16 v9, 0x40c0

    iget-object v10, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v0, v9, v10}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_12d

    :cond_151
    const-wide/16 v2, 0x50

    iget v0, v8, Landroid/graphics/Rect;->left:I

    add-int/lit8 v4, v0, -0x6

    iget v0, v8, Landroid/graphics/Rect;->top:I

    add-int/lit8 v5, v0, -0x6

    iget v0, v8, Landroid/graphics/Rect;->right:I

    add-int/lit8 v6, v0, 0x6

    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v7, v0, 0x6

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->postInvalidateDelayed(JIIII)V

    goto/16 :goto_f
.end method

.method public setCameraPreview(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V
    .registers 3

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView$1;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView$1;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;)V

    invoke-virtual {p1, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;)V

    return-void
.end method
