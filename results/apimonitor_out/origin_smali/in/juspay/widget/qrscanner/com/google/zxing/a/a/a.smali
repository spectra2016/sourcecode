.class public final Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;
.super Ljava/lang/Object;
.source "AmbientLightManager.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;

.field private b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;

.field private c:Landroid/hardware/Sensor;

.field private d:Landroid/content/Context;

.field private e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->d:Landroid/content/Context;

    iput-object p2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;

    iput-object p3, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->e:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;

    return-object v0
.end method

.method private a(Z)V
    .registers 4

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->e:Landroid/os/Handler;

    new-instance v1, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a$1;

    invoke-direct {v1, p0, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a$1;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public a()V
    .registers 4

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->h()Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->d:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->c:Landroid/hardware/Sensor;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->c:Landroid/hardware/Sensor;

    if-eqz v1, :cond_23

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->c:Landroid/hardware/Sensor;

    const/4 v2, 0x3

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_23
    return-void
.end method

.method public b()V
    .registers 3

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->c:Landroid/hardware/Sensor;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->d:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->c:Landroid/hardware/Sensor;

    :cond_14
    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .registers 3

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .registers 5

    const/4 v2, 0x0

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v2

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;

    if-eqz v1, :cond_13

    const/high16 v1, 0x4234

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_14

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->a(Z)V

    :cond_13
    :goto_13
    return-void

    :cond_14
    const/high16 v1, 0x43e1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_13

    invoke-direct {p0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->a(Z)V

    goto :goto_13
.end method
