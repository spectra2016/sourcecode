.class public final Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;
.super Ljava/lang/Object;
.source "BeepManager.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Z

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;->c:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;->d:Z

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setVolumeControlStream(I)V

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;->b:Landroid/content/Context;

    return-void
.end method

.method static synthetic b()Ljava/lang/String;
    .registers 1

    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/media/MediaPlayer;
    .registers 8

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    new-instance v1, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b$1;

    invoke-direct {v1, p0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b$1;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    new-instance v1, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b$2;

    invoke-direct {v1, p0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b$2;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    :try_start_19
    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lin/juspay/widget/qrscanner/a$d;->zxing_beep:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_24} :catch_4c

    move-result-object v6

    :try_start_25
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_34
    .catchall {:try_start_25 .. :try_end_34} :catchall_47

    :try_start_34
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    const v1, 0x3dcccccd

    const v2, 0x3dcccccd

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    :goto_46
    return-object v0

    :catchall_47
    move-exception v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    throw v1
    :try_end_4c
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_4c} :catch_4c

    :catch_4c
    move-exception v1

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;->a:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    goto :goto_46
.end method

.method public a(Z)V
    .registers 2

    iput-boolean p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;->c:Z

    return-void
.end method
