.class public final enum Lin/juspay/widget/qrscanner/com/google/zxing/d;
.super Ljava/lang/Enum;
.source "DecodeHintType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lin/juspay/widget/qrscanner/com/google/zxing/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lin/juspay/widget/qrscanner/com/google/zxing/d;

.field public static final enum b:Lin/juspay/widget/qrscanner/com/google/zxing/d;

.field public static final enum c:Lin/juspay/widget/qrscanner/com/google/zxing/d;

.field public static final enum d:Lin/juspay/widget/qrscanner/com/google/zxing/d;

.field public static final enum e:Lin/juspay/widget/qrscanner/com/google/zxing/d;

.field public static final enum f:Lin/juspay/widget/qrscanner/com/google/zxing/d;

.field public static final enum g:Lin/juspay/widget/qrscanner/com/google/zxing/d;

.field public static final enum h:Lin/juspay/widget/qrscanner/com/google/zxing/d;

.field public static final enum i:Lin/juspay/widget/qrscanner/com/google/zxing/d;

.field public static final enum j:Lin/juspay/widget/qrscanner/com/google/zxing/d;

.field public static final enum k:Lin/juspay/widget/qrscanner/com/google/zxing/d;

.field private static final synthetic m:[Lin/juspay/widget/qrscanner/com/google/zxing/d;


# instance fields
.field private final l:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;

    const-string v1, "OTHER"

    const-class v2, Ljava/lang/Object;

    invoke-direct {v0, v1, v4, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/d;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->a:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;

    const-string v1, "PURE_BARCODE"

    const-class v2, Ljava/lang/Void;

    invoke-direct {v0, v1, v5, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/d;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->b:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;

    const-string v1, "POSSIBLE_FORMATS"

    const-class v2, Ljava/util/List;

    invoke-direct {v0, v1, v6, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/d;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->c:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;

    const-string v1, "TRY_HARDER"

    const-class v2, Ljava/lang/Void;

    invoke-direct {v0, v1, v7, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/d;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->d:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;

    const-string v1, "CHARACTER_SET"

    const-class v2, Ljava/lang/String;

    invoke-direct {v0, v1, v8, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/d;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->e:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;

    const-string v1, "ALLOWED_LENGTHS"

    const/4 v2, 0x5

    const-class v3, [I

    invoke-direct {v0, v1, v2, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/d;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->f:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;

    const-string v1, "ASSUME_CODE_39_CHECK_DIGIT"

    const/4 v2, 0x6

    const-class v3, Ljava/lang/Void;

    invoke-direct {v0, v1, v2, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/d;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->g:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;

    const-string v1, "ASSUME_GS1"

    const/4 v2, 0x7

    const-class v3, Ljava/lang/Void;

    invoke-direct {v0, v1, v2, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/d;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->h:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;

    const-string v1, "RETURN_CODABAR_START_END"

    const/16 v2, 0x8

    const-class v3, Ljava/lang/Void;

    invoke-direct {v0, v1, v2, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/d;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->i:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;

    const-string v1, "NEED_RESULT_POINT_CALLBACK"

    const/16 v2, 0x9

    const-class v3, Lin/juspay/widget/qrscanner/com/google/zxing/m;

    invoke-direct {v0, v1, v2, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/d;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->j:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;

    const-string v1, "ALLOWED_EAN_EXTENSIONS"

    const/16 v2, 0xa

    const-class v3, [I

    invoke-direct {v0, v1, v2, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/d;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->k:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    const/16 v0, 0xb

    new-array v0, v0, [Lin/juspay/widget/qrscanner/com/google/zxing/d;

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/d;->a:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    aput-object v1, v0, v4

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/d;->b:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    aput-object v1, v0, v5

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/d;->c:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    aput-object v1, v0, v6

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/d;->d:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    aput-object v1, v0, v7

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/d;->e:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/d;->f:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/d;->g:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/d;->h:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/d;->i:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/d;->j:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/d;->k:Lin/juspay/widget/qrscanner/com/google/zxing/d;

    aput-object v2, v0, v1

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->m:[Lin/juspay/widget/qrscanner/com/google/zxing/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->l:Ljava/lang/Class;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lin/juspay/widget/qrscanner/com/google/zxing/d;
    .registers 2

    const-class v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;

    return-object v0
.end method

.method public static values()[Lin/juspay/widget/qrscanner/com/google/zxing/d;
    .registers 1

    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->m:[Lin/juspay/widget/qrscanner/com/google/zxing/d;

    invoke-virtual {v0}, [Lin/juspay/widget/qrscanner/com/google/zxing/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lin/juspay/widget/qrscanner/com/google/zxing/d;

    return-object v0
.end method
