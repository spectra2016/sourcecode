.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;
.super Ljava/lang/Object;
.source "RotationListener.java"


# instance fields
.field private a:I

.field private b:Landroid/view/WindowManager;

.field private c:Landroid/view/OrientationEventListener;

.field private d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/j;


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;I)I
    .registers 2

    iput p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->a:I

    return p1
.end method

.method static synthetic a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;)Landroid/view/WindowManager;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->b:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/j;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/j;

    return-object v0
.end method

.method static synthetic c(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;)I
    .registers 2

    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->a:I

    return v0
.end method


# virtual methods
.method public a()V
    .registers 3

    const/4 v1, 0x0

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->c:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->c:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    :cond_a
    iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->c:Landroid/view/OrientationEventListener;

    iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->b:Landroid/view/WindowManager;

    iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/j;

    return-void
.end method

.method public a(Landroid/content/Context;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/j;)V
    .registers 6

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->a()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object p2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/j;

    const-string v0, "window"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->b:Landroid/view/WindowManager;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k$1;

    const/4 v2, 0x3

    invoke-direct {v0, p0, v1, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k$1;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;Landroid/content/Context;I)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->c:Landroid/view/OrientationEventListener;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->c:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->b:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->a:I

    return-void
.end method
