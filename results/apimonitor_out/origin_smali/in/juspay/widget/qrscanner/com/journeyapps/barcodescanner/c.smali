.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;
.super Landroid/view/ViewGroup;
.source "CameraPreview.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final A:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;

.field private b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

.field private c:Landroid/view/WindowManager;

.field private d:Landroid/os/Handler;

.field private e:Z

.field private f:Landroid/view/SurfaceView;

.field private g:Landroid/view/TextureView;

.field private h:Z

.field private i:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;

.field private j:I

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;

.field private m:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;

.field private n:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

.field private o:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

.field private p:Landroid/graphics/Rect;

.field private q:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

.field private r:Landroid/graphics/Rect;

.field private s:Landroid/graphics/Rect;

.field private t:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

.field private u:D

.field private v:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

.field private w:Z

.field private final x:Landroid/view/SurfaceHolder$Callback;

.field private final y:Landroid/os/Handler$Callback;

.field private z:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/j;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    iput-boolean v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->e:Z

    iput-boolean v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->h:Z

    const/4 v0, -0x1

    iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->j:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->k:Ljava/util/List;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;-><init>()V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->m:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->r:Landroid/graphics/Rect;

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->s:Landroid/graphics/Rect;

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->t:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    const-wide v0, 0x3fb999999999999aL

    iput-wide v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->u:D

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->v:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    iput-boolean v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->w:Z

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$2;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$2;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->x:Landroid/view/SurfaceHolder$Callback;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->y:Landroid/os/Handler$Callback;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$4;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$4;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->z:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/j;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$5;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$5;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->A:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;

    invoke-direct {p0, p1, v3, v2, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->e:Z

    iput-boolean v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->h:Z

    const/4 v0, -0x1

    iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->j:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->k:Ljava/util/List;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;-><init>()V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->m:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->r:Landroid/graphics/Rect;

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->s:Landroid/graphics/Rect;

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->t:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    const-wide v0, 0x3fb999999999999aL

    iput-wide v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->u:D

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->v:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    iput-boolean v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->w:Z

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$2;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$2;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->x:Landroid/view/SurfaceHolder$Callback;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->y:Landroid/os/Handler$Callback;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$4;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$4;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->z:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/j;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$5;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$5;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->A:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;

    invoke-direct {p0, p1, p2, v2, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->e:Z

    iput-boolean v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->h:Z

    const/4 v0, -0x1

    iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->j:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->k:Ljava/util/List;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;-><init>()V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->m:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->r:Landroid/graphics/Rect;

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->s:Landroid/graphics/Rect;

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->t:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    const-wide v0, 0x3fb999999999999aL

    iput-wide v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->u:D

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->v:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    iput-boolean v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->w:Z

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$2;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$2;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->x:Landroid/view/SurfaceHolder$Callback;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->y:Landroid/os/Handler$Callback;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$4;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$4;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->z:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/j;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$5;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$5;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->A:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;

    invoke-direct {p0, p1, p2, p3, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method private a()Landroid/view/TextureView$SurfaceTextureListener;
    .registers 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$1;

    invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$1;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    return-object v0
.end method

.method static synthetic a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->q:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    return-object p1
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .registers 7

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_b

    const/high16 v0, -0x100

    invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->setBackgroundColor(I)V

    :cond_b
    invoke-virtual {p0, p2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a(Landroid/util/AttributeSet;)V

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->c:Landroid/view/WindowManager;

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->y:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->d:Landroid/os/Handler;

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;-><init>()V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->i:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;

    return-void
.end method

.method private a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;)V
    .registers 4

    iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->h:Z

    if-nez v0, :cond_24

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    if-eqz v0, :cond_24

    sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a:Ljava/lang/String;

    const-string v1, "Starting preview"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-virtual {v0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->d()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->h:Z

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->c()V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->A:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;

    invoke-interface {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;->b()V

    :cond_24
    return-void
.end method

.method static synthetic a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V
    .registers 1

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->l()V

    return-void
.end method

.method private a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)V
    .registers 4

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->n:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    if-eqz v0, :cond_39

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;

    move-result-object v0

    if-nez v0, :cond_39

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getDisplayRotation()I

    move-result v1

    invoke-direct {v0, v1, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;-><init>(ILin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getPreviewScalingStrategy()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->c()V

    iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->w:Z

    if-eqz v0, :cond_39

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    iget-boolean v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->w:Z

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Z)V

    :cond_39
    return-void
.end method

.method static synthetic b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->A:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;

    return-object v0
.end method

.method private b()V
    .registers 3

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getDisplayRotation()I

    move-result v0

    iget v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->j:I

    if-eq v0, v1, :cond_14

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->d()V

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->e()V

    :cond_14
    return-void
.end method

.method static synthetic b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)V
    .registers 2

    invoke-direct {p0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)V

    return-void
.end method

.method private b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)V
    .registers 3

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->o:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->n:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    if-eqz v0, :cond_f

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->k()V

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->requestLayout()V

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->l()V

    :cond_f
    return-void
.end method

.method static synthetic c(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V
    .registers 1

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b()V

    return-void
.end method

.method static synthetic d(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)Landroid/os/Handler;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->d:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)Ljava/util/List;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->k:Ljava/util/List;

    return-object v0
.end method

.method private getDisplayRotation()I
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->c:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    return v0
.end method

.method static synthetic i()Ljava/lang/String;
    .registers 1

    sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method private j()V
    .registers 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewAPI"
        }
    .end annotation

    iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->e:Z

    if-eqz v0, :cond_24

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_24

    new-instance v0, Landroid/view/TextureView;

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->addView(Landroid/view/View;)V

    :goto_23
    return-void

    :cond_24
    new-instance v0, Landroid/view/SurfaceView;

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f:Landroid/view/SurfaceView;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_3f

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    :cond_3f
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->x:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f:Landroid/view/SurfaceView;

    invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->addView(Landroid/view/View;)V

    goto :goto_23
.end method

.method private k()V
    .registers 9

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->n:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->o:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;

    if-nez v0, :cond_1c

    :cond_e
    iput-object v7, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->s:Landroid/graphics/Rect;

    iput-object v7, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->r:Landroid/graphics/Rect;

    iput-object v7, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "containerSize or previewSize is not set yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1c
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->o:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget v0, v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->o:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget v1, v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->n:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget v2, v2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    iget-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->n:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget v3, v3, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    iget-object v4, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;

    iget-object v5, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->o:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    invoke-virtual {v4, v5}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Landroid/graphics/Rect;

    move-result-object v4

    iput-object v4, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v6, v6, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    invoke-virtual {p0, v4, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    iput-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->r:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->r:Landroid/graphics/Rect;

    invoke-direct {v2, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    neg-int v3, v3

    iget-object v4, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    neg-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    new-instance v3, Landroid/graphics/Rect;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    mul-int/2addr v4, v0

    iget-object v5, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    div-int/2addr v4, v5

    iget v5, v2, Landroid/graphics/Rect;->top:I

    mul-int/2addr v5, v1

    iget-object v6, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    div-int/2addr v5, v6

    iget v6, v2, Landroid/graphics/Rect;->right:I

    mul-int/2addr v0, v6

    iget-object v6, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    div-int/2addr v0, v6

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    mul-int/2addr v1, v2

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/2addr v1, v2

    invoke-direct {v3, v4, v5, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->s:Landroid/graphics/Rect;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->s:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_96

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->s:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-gtz v0, :cond_a2

    :cond_96
    iput-object v7, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->s:Landroid/graphics/Rect;

    iput-object v7, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->r:Landroid/graphics/Rect;

    sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a:Ljava/lang/String;

    const-string v1, "Preview frame is too small"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_a1
    return-void

    :cond_a2
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->A:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;

    invoke-interface {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;->a()V

    goto :goto_a1
.end method

.method private l()V
    .registers 5

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->q:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    if-eqz v0, :cond_37

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->o:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    if-eqz v0, :cond_37

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    if-eqz v0, :cond_37

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f:Landroid/view/SurfaceView;

    if-eqz v0, :cond_38

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->q:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    new-instance v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;-><init>(II)V

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-direct {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;-><init>(Landroid/view/SurfaceHolder;)V

    invoke-direct {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;)V

    :cond_37
    :goto_37
    return-void

    :cond_38
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    if-eqz v0, :cond_37

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_37

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    if-eqz v0, :cond_37

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->o:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    if-eqz v0, :cond_6a

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getWidth()I

    move-result v1

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-virtual {v2}, Landroid/view/TextureView;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;-><init>(II)V

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->o:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    invoke-virtual {p0, v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-virtual {v1, v0}, Landroid/view/TextureView;->setTransform(Landroid/graphics/Matrix;)V

    :cond_6a
    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    invoke-direct {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-direct {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;)V

    goto :goto_37
.end method

.method private m()V
    .registers 3

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    if-eqz v0, :cond_c

    sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a:Ljava/lang/String;

    const-string v1, "initCamera called twice"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_b
    return-void

    :cond_c
    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    move-result-object v0

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Landroid/os/Handler;)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->b()V

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getDisplayRotation()I

    move-result v0

    iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->j:I

    goto :goto_b
.end method


# virtual methods
.method protected a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Landroid/graphics/Matrix;
    .registers 9

    const/high16 v4, 0x4000

    const/high16 v0, 0x3f80

    iget v1, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    int-to-float v1, v1

    iget v2, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    int-to-float v2, v2

    iget v3, p2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    cmpg-float v3, v1, v2

    if-gez v3, :cond_38

    div-float v1, v2, v1

    :goto_18
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    iget v3, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    int-to-float v3, v3

    mul-float/2addr v1, v3

    iget v3, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    iget v3, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    int-to-float v3, v3

    sub-float v1, v3, v1

    div-float/2addr v1, v4

    iget v3, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    int-to-float v3, v3

    sub-float v0, v3, v0

    div-float/2addr v0, v4

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-object v2

    :cond_38
    div-float/2addr v1, v2

    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_18
.end method

.method protected a(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .registers 12

    const/4 v8, 0x0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->t:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    if-eqz v1, :cond_2f

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->t:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget v2, v2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->t:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget v3, v3, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    invoke-static {v8, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    :cond_2e
    :goto_2e
    return-object v0

    :cond_2f
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-double v2, v1

    iget-wide v4, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->u:D

    mul-double/2addr v2, v4

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-double v4, v1

    iget-wide v6, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->u:D

    mul-double/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Rect;->inset(II)V

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-le v1, v2, :cond_2e

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v8, v1}, Landroid/graphics/Rect;->inset(II)V

    goto :goto_2e
.end method

.method protected a(Landroid/util/AttributeSet;)V
    .registers 7

    const/4 v4, 0x1

    const/high16 v3, -0x4080

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lin/juspay/widget/qrscanner/a$f;->zxing_camera_preview:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lin/juspay/widget/qrscanner/a$f;->zxing_camera_preview_zxing_framing_rect_width:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    sget v2, Lin/juspay/widget/qrscanner/a$f;->zxing_camera_preview_zxing_framing_rect_height:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    if-lez v1, :cond_26

    if-lez v2, :cond_26

    new-instance v3, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    invoke-direct {v3, v1, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;-><init>(II)V

    iput-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->t:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    :cond_26
    sget v1, Lin/juspay/widget/qrscanner/a$f;->zxing_camera_preview_zxing_use_texture_view:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->e:Z

    sget v1, Lin/juspay/widget/qrscanner/a$f;->zxing_camera_preview_zxing_preview_scaling_strategy:I

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    if-ne v1, v4, :cond_42

    new-instance v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/g;

    invoke-direct {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/g;-><init>()V

    iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->v:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    :cond_3e
    :goto_3e
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_42
    const/4 v2, 0x2

    if-ne v1, v2, :cond_4d

    new-instance v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/i;

    invoke-direct {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/i;-><init>()V

    iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->v:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    goto :goto_3e

    :cond_4d
    const/4 v2, 0x3

    if-ne v1, v2, :cond_3e

    new-instance v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/j;

    invoke-direct {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/j;-><init>()V

    iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->v:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    goto :goto_3e
.end method

.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;)V
    .registers 3

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected c()V
    .registers 1

    return-void
.end method

.method public d()V
    .registers 4

    const/4 v2, 0x0

    invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/n;->a()V

    sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a:Ljava/lang/String;

    const-string v1, "pause()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->j:I

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    if-eqz v0, :cond_23

    sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a:Ljava/lang/String;

    const-string v1, "cameraInstance NOT NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->e()V

    iput-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->h:Z

    :cond_23
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->q:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    if-nez v0, :cond_36

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f:Landroid/view/SurfaceView;

    if-eqz v0, :cond_36

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->x:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    :cond_36
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->q:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    if-nez v0, :cond_49

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    if-eqz v0, :cond_49

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_49

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-virtual {v0, v2}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    :cond_49
    iput-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->n:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iput-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->o:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iput-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->s:Landroid/graphics/Rect;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->i:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->a()V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->A:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;

    invoke-interface {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;->c()V

    return-void
.end method

.method public e()V
    .registers 5

    invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/n;->a()V

    sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a:Ljava/lang/String;

    const-string v1, "resume()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->m()V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->q:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    if-eqz v0, :cond_23

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->l()V

    :cond_14
    :goto_14
    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->requestLayout()V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->i:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->z:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/j;

    invoke-virtual {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;->a(Landroid/content/Context;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/j;)V

    return-void

    :cond_23
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f:Landroid/view/SurfaceView;

    if-eqz v0, :cond_33

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->x:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    goto :goto_14

    :cond_33
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    if-eqz v0, :cond_14

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_14

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_5f

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-virtual {v2}, Landroid/view/TextureView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-virtual {v3}, Landroid/view/TextureView;->getHeight()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V

    goto :goto_14

    :cond_5f
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    goto :goto_14
.end method

.method protected f()Z
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected g()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
    .registers 3

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->m:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;)V

    return-object v0
.end method

.method public getCameraInstance()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    return-object v0
.end method

.method public getCameraSettings()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->m:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;

    return-object v0
.end method

.method public getFramingRect()Landroid/graphics/Rect;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->r:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getFramingRectSize()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->t:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    return-object v0
.end method

.method public getMarginFraction()D
    .registers 3

    iget-wide v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->u:D

    return-wide v0
.end method

.method public getPreviewFramingRect()Landroid/graphics/Rect;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->s:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getPreviewScalingStrategy()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->v:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->v:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    :goto_6
    return-object v0

    :cond_7
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    if-eqz v0, :cond_11

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/g;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/g;-><init>()V

    goto :goto_6

    :cond_11
    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/i;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/i;-><init>()V

    goto :goto_6
.end method

.method public h()Z
    .registers 2

    iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->h:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->j()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    const/4 v3, 0x0

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;-><init>(II)V

    invoke-direct {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f:Landroid/view/SurfaceView;

    if-eqz v0, :cond_39

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    if-nez v0, :cond_23

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f:Landroid/view/SurfaceView;

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/SurfaceView;->layout(IIII)V

    :cond_22
    :goto_22
    return-void

    :cond_23
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f:Landroid/view/SurfaceView;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->p:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/SurfaceView;->layout(IIII)V

    goto :goto_22

    :cond_39
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    if-eqz v0, :cond_22

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_22

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->g:Landroid/view/TextureView;

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/TextureView;->layout(IIII)V

    goto :goto_22
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 3

    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_8

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_7
    return-void

    :cond_8
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "super"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "torch"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->setTorch(Z)V

    goto :goto_7
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "super"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "torch"

    iget-boolean v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->w:Z

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v1
.end method

.method public setCameraSettings(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->m:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;

    return-void
.end method

.method public setFramingRectSize(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->t:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    return-void
.end method

.method public setMarginFraction(D)V
    .registers 6

    const-wide/high16 v0, 0x3fe0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_e

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The margin fraction must be less than 0.5"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    iput-wide p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->u:D

    return-void
.end method

.method public setPreviewScalingStrategy(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->v:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    return-void
.end method

.method public setTorch(Z)V
    .registers 3

    iput-boolean p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->w:Z

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;

    invoke-virtual {v0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Z)V

    :cond_b
    return-void
.end method

.method public setUseTextureView(Z)V
    .registers 2

    iput-boolean p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->e:Z

    return-void
.end method
