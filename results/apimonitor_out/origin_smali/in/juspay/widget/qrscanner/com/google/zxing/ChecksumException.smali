.class public final Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;
.super Lin/juspay/widget/qrscanner/com/google/zxing/ReaderException;
.source "ChecksumException.java"


# static fields
.field private static final c:Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;-><init>()V

    sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;->c:Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;

    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;->c:Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;

    sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;->b:[Ljava/lang/StackTraceElement;

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/ReaderException;-><init>()V

    return-void
.end method

.method public static a()Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;
    .registers 1

    sget-boolean v0, Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;->a:Z

    if-eqz v0, :cond_a

    new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;-><init>()V

    :goto_9
    return-object v0

    :cond_a
    sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;->c:Lin/juspay/widget/qrscanner/com/google/zxing/ChecksumException;

    goto :goto_9
.end method
