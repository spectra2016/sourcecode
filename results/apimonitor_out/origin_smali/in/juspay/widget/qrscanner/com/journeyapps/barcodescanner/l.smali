.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
.super Ljava/lang/Object;
.source "Size.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method public constructor <init>(II)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    iput p2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    return-void
.end method


# virtual methods
.method public a()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
    .registers 4

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    iget v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;-><init>(II)V

    return-object v0
.end method

.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
    .registers 6

    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    iget v1, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    mul-int/2addr v0, v1

    iget v1, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    iget v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    mul-int/2addr v1, v2

    if-lt v0, v1, :cond_1c

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget v1, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    iget v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    iget v3, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    mul-int/2addr v2, v3

    iget v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    div-int/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;-><init>(II)V

    :goto_1b
    return-object v0

    :cond_1c
    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    iget v2, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    mul-int/2addr v1, v2

    iget v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    div-int/2addr v1, v2

    iget v2, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;-><init>(II)V

    goto :goto_1b
.end method

.method public b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
    .registers 6

    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    iget v1, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    mul-int/2addr v0, v1

    iget v1, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    iget v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    mul-int/2addr v1, v2

    if-gt v0, v1, :cond_1c

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget v1, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    iget v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    iget v3, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    mul-int/2addr v2, v3

    iget v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    div-int/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;-><init>(II)V

    :goto_1b
    return-object v0

    :cond_1c
    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    iget v2, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    mul-int/2addr v1, v2

    iget v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    div-int/2addr v1, v2

    iget v2, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;-><init>(II)V

    goto :goto_1b
.end method

.method public c(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)I
    .registers 5

    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    iget v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    mul-int/2addr v0, v1

    iget v1, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    iget v2, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    mul-int/2addr v1, v2

    if-ge v1, v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    if-le v1, v0, :cond_12

    const/4 v0, -0x1

    goto :goto_d

    :cond_12
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3

    check-cast p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    invoke-virtual {p0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->c(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_5

    :cond_4
    :goto_4
    return v0

    :cond_5
    if-eqz p1, :cond_11

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_13

    :cond_11
    move v0, v1

    goto :goto_4

    :cond_13
    check-cast p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    iget v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    iget v3, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    if-ne v2, v3, :cond_21

    iget v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    iget v3, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    if-eq v2, v3, :cond_4

    :cond_21
    move v0, v1

    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
