.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;
.super Landroid/widget/FrameLayout;
.source "DecoratedBarcodeView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$a;,
        Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$b;
    }
.end annotation


# instance fields
.field private a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

.field private b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;

.field private c:Landroid/widget/TextView;

.field private d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->e()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a(Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;

    return-object v0
.end method

.method private a(Landroid/util/AttributeSet;)V
    .registers 5

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lin/juspay/widget/qrscanner/a$f;->zxing_view:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lin/juspay/widget/qrscanner/a$f;->zxing_view_zxing_scanner_layout:I

    sget v2, Lin/juspay/widget/qrscanner/a$c;->zxing_barcode_scanner:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    sget v0, Lin/juspay/widget/qrscanner/a$b;->zxing_barcode_surface:I

    invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    if-nez v0, :cond_32

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "There is no a com.journeyapps.barcodescanner.BarcodeView on provided layout with the id \"zxing_barcode_surface\"."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_32
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    invoke-virtual {v0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a(Landroid/util/AttributeSet;)V

    sget v0, Lin/juspay/widget/qrscanner/a$b;->zxing_viewfinder_view:I

    invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;

    if-nez v0, :cond_4d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "There is no a com.journeyapps.barcodescanner.ViewfinderView on provided layout with the id \"zxing_viewfinder_view\"."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4d
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;->setCameraPreview(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V

    sget v0, Lin/juspay/widget/qrscanner/a$b;->zxing_status_view:I

    invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->c:Landroid/widget/TextView;

    return-void
.end method

.method private e()V
    .registers 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a(Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->d()V

    return-void
.end method

.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;)V
    .registers 4

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    new-instance v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$b;

    invoke-direct {v1, p0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$b;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;)V

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;)V

    return-void
.end method

.method public b()V
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->e()V

    return-void
.end method

.method public c()V
    .registers 3

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->setTorch(Z)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$a;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$a;

    invoke-interface {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$a;->a()V

    :cond_f
    return-void
.end method

.method public d()V
    .registers 3

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->setTorch(Z)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$a;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$a;

    invoke-interface {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$a;->b()V

    :cond_f
    return-void
.end method

.method public getBarcodeView()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;
    .registers 2

    sget v0, Lin/juspay/widget/qrscanner/a$b;->zxing_barcode_surface:I

    invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    return-object v0
.end method

.method public getStatusView()Landroid/widget/TextView;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method public getViewFinder()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;
    .registers 2

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView;

    return-object v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_12

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_8
    :sswitch_8
    return v0

    :sswitch_9
    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->d()V

    goto :goto_8

    :sswitch_d
    invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->c()V

    goto :goto_8

    nop

    :sswitch_data_12
    .sparse-switch
        0x18 -> :sswitch_d
        0x19 -> :sswitch_9
        0x1b -> :sswitch_8
        0x50 -> :sswitch_8
    .end sparse-switch
.end method

.method public setStatusText(Ljava/lang/String;)V
    .registers 3

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_9
    return-void
.end method

.method public setTorchListener(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$a;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$a;

    return-void
.end method
