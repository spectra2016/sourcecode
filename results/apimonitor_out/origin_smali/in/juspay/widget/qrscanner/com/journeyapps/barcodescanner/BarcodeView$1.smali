.class Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;
.super Ljava/lang/Object;
.source "BarcodeView.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;


# direct methods
.method constructor <init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .registers 6

    const/4 v1, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    sget v2, Lin/juspay/widget/qrscanner/a$b;->zxing_decode_succeeded:I

    if-ne v0, v2, :cond_39

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/b;

    if-eqz v0, :cond_37

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    invoke-static {v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;

    move-result-object v2

    if-eqz v2, :cond_37

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    invoke-static {v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    move-result-object v2

    sget-object v3, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    if-eq v2, v3, :cond_37

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    invoke-static {v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;

    move-result-object v2

    invoke-interface {v2, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/b;)V

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    move-result-object v0

    sget-object v2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    if-ne v0, v2, :cond_37

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a()V

    :cond_37
    move v0, v1

    :goto_38
    return v0

    :cond_39
    iget v0, p1, Landroid/os/Message;->what:I

    sget v2, Lin/juspay/widget/qrscanner/a$b;->zxing_decode_failed:I

    if-ne v0, v2, :cond_41

    move v0, v1

    goto :goto_38

    :cond_41
    iget v0, p1, Landroid/os/Message;->what:I

    sget v2, Lin/juspay/widget/qrscanner/a$b;->zxing_possible_result_points:I

    if-ne v0, v2, :cond_68

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    invoke-static {v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;

    move-result-object v2

    if-eqz v2, :cond_66

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    invoke-static {v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    move-result-object v2

    sget-object v3, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;

    if-eq v2, v3, :cond_66

    iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;

    invoke-static {v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;

    move-result-object v2

    invoke-interface {v2, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;->a(Ljava/util/List;)V

    :cond_66
    move v0, v1

    goto :goto_38

    :cond_68
    const/4 v0, 0x0

    goto :goto_38
.end method
