.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;
.super Ljava/lang/Object;
.source "DisplayConfiguration.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

.field private c:I

.field private d:Z

.field private e:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->d:Z

    new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/i;

    invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/i;-><init>()V

    iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->e:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    iput p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->c:I

    iput-object p2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->c:I

    return v0
.end method

.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Landroid/graphics/Rect;
    .registers 4

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->e:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    invoke-virtual {v0, p1, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;Z)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;",
            ">;Z)",
            "Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;"
        }
    .end annotation

    invoke-virtual {p0, p2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->a(Z)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->e:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    invoke-virtual {v1, p1, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;->a(Ljava/util/List;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
    .registers 3

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    if-eqz p1, :cond_f

    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    move-result-object v0

    goto :goto_5

    :cond_f
    iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    goto :goto_5
.end method

.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->e:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;

    return-void
.end method
