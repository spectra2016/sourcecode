.class Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;
.super Ljava/lang/Object;
.source "CameraPreview.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;


# direct methods
.method constructor <init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .registers 4

    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lin/juspay/widget/qrscanner/a$b;->zxing_prewiew_size_ready:I

    if-ne v0, v1, :cond_11

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;

    invoke-static {v1, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)V

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lin/juspay/widget/qrscanner/a$b;->zxing_camera_error:I

    if-ne v0, v1, :cond_31

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Exception;

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;

    invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->f()Z

    move-result v1

    if-eqz v1, :cond_31

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;

    invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->d()V

    iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;

    invoke-static {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;

    move-result-object v1

    invoke-interface {v1, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;->a(Ljava/lang/Exception;)V

    :cond_31
    const/4 v0, 0x0

    goto :goto_10
.end method
