.class public Lin/juspay/tracker/a;
.super Ljava/lang/Object;
.source "EncryptionHelper.java"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:[B

.field private static c:Lin/juspay/tracker/a;

.field private static final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-string v0, "AES"

    sput-object v0, Lin/juspay/tracker/a;->a:Ljava/lang/String;

    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_16

    sput-object v0, Lin/juspay/tracker/a;->b:[B

    const-class v0, Lin/juspay/tracker/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/juspay/tracker/a;->d:Ljava/lang/String;

    return-void

    :array_16
    .array-data 0x1
        0xcct
        0x33t
        0xbct
        0x87t
        0xd4t
        0x8et
        0xc5t
        0xect
        0xb1t
        0x16t
        0x22t
        0xb3t
        0xd0t
        0xb5t
        0x2dt
        0x5dt
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lin/juspay/tracker/a;
    .registers 2

    const-class v1, Lin/juspay/tracker/a;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lin/juspay/tracker/a;->c:Lin/juspay/tracker/a;

    if-nez v0, :cond_e

    new-instance v0, Lin/juspay/tracker/a;

    invoke-direct {v0}, Lin/juspay/tracker/a;-><init>()V

    sput-object v0, Lin/juspay/tracker/a;->c:Lin/juspay/tracker/a;

    :cond_e
    sget-object v0, Lin/juspay/tracker/a;->c:Lin/juspay/tracker/a;

    monitor-exit v1

    return-object v0

    :catchall_12
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    throw v0
.end method

.method private b()Ljava/security/Key;
    .registers 4

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    sget-object v1, Lin/juspay/tracker/a;->b:[B

    sget-object v2, Lin/juspay/tracker/a;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v0
.end method

.method private f([B)Ljava/lang/String;
    .registers 7

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    :goto_6
    array-length v2, p1

    if-ge v0, v2, :cond_23

    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1d

    const/16 v3, 0x30

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_23
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .registers 6

    :try_start_0
    const-string v0, "SHA-256"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lin/juspay/tracker/a;->f([B)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lin/juspay/tracker/a;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "result is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lin/juspay/tracker/f;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2d
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_2d} :catch_2e

    :goto_2d
    return-object v0

    :catch_2e
    move-exception v0

    sget-object v1, Lin/juspay/tracker/a;->d:Ljava/lang/String;

    const-string v2, "Exception caught trying to SHA-256 hash"

    invoke-static {v1, v2, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_2d
.end method

.method public a([B)Ljava/lang/String;
    .registers 9

    const-string v0, "MD5"

    :try_start_2
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update([B)V

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_17
    if-ge v1, v4, :cond_43

    aget-byte v0, v2, v1

    and-int/lit16 v0, v0, 0xff

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    :goto_21
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x2

    if-ge v5, v6, :cond_3c

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_21

    :cond_3c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_17

    :cond_43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_46
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_46} :catch_48

    move-result-object v0

    :goto_47
    return-object v0

    :catch_48
    move-exception v0

    sget-object v1, Lin/juspay/tracker/a;->d:Ljava/lang/String;

    const-string v2, "Exception trying to calculate md5sum from given string"

    invoke-static {v1, v2, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_47
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .registers 3

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lin/juspay/tracker/a;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b([B)[B
    .registers 5

    invoke-direct {p0}, Lin/juspay/tracker/a;->b()Ljava/security/Key;

    move-result-object v0

    sget-object v1, Lin/juspay/tracker/a;->a:Ljava/lang/String;

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    return-object v0
.end method

.method public c([B)[B
    .registers 11

    const/16 v8, 0x8

    const/4 v0, 0x0

    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    new-array v3, v8, [B

    invoke-virtual {v1, v3}, Ljava/security/SecureRandom;->nextBytes([B)V

    array-length v4, p1

    add-int v1, v4, v8

    new-array v5, v1, [B

    move v2, v0

    move v1, v0

    :goto_14
    if-ge v0, v4, :cond_3a

    add-int v6, v4, v8

    if-ge v2, v6, :cond_3a

    if-lez v2, :cond_2d

    rem-int/lit8 v6, v2, 0xa

    const/16 v7, 0x9

    if-ne v6, v7, :cond_2d

    if-ge v1, v8, :cond_2d

    aget-byte v6, v3, v1

    aput-byte v6, v5, v2

    add-int/lit8 v1, v1, 0x1

    :goto_2a
    add-int/lit8 v2, v2, 0x1

    goto :goto_14

    :cond_2d
    aget-byte v6, p1, v0

    rem-int v7, v0, v8

    aget-byte v7, v3, v7

    xor-int/2addr v6, v7

    int-to-byte v6, v6

    aput-byte v6, v5, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_2a

    :cond_3a
    return-object v5
.end method

.method public d([B)[B
    .registers 3

    :try_start_0
    invoke-static {p1}, Lin/juspay/tracker/d;->a([B)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lin/juspay/tracker/a;->b([B)[B
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_7} :catch_9
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_7} :catch_f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_14

    move-result-object v0

    :goto_8
    return-object v0

    :catch_9
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    :goto_d
    const/4 v0, 0x0

    goto :goto_8

    :catch_f
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    :catch_14
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_d
.end method

.method public e([B)[B
    .registers 3

    :try_start_0
    invoke-static {p1}, Lin/juspay/tracker/d;->a([B)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lin/juspay/tracker/a;->c([B)[B
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_7} :catch_9
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_7} :catch_f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_14

    move-result-object v0

    :goto_8
    return-object v0

    :catch_9
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    :goto_d
    const/4 v0, 0x0

    goto :goto_8

    :catch_f
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    :catch_14
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_d
.end method
