.class public Lin/juspay/tracker/j;
.super Ljava/lang/Object;
.source "SessionInfo.java"


# static fields
.field private static a:Ljava/lang/String;

.field private static f:Lin/juspay/tracker/j;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Landroid/util/DisplayMetrics;

.field private e:Ljava/lang/String;

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lin/juspay/tracker/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/juspay/tracker/j;->a:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lin/juspay/tracker/j;->f:Lin/juspay/tracker/j;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lin/juspay/tracker/j;->g:Ljava/util/Map;

    return-void
.end method

.method public static a()Lin/juspay/tracker/j;
    .registers 2

    const-class v1, Lin/juspay/tracker/j;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lin/juspay/tracker/j;->f:Lin/juspay/tracker/j;

    if-nez v0, :cond_e

    new-instance v0, Lin/juspay/tracker/j;

    invoke-direct {v0}, Lin/juspay/tracker/j;-><init>()V

    sput-object v0, Lin/juspay/tracker/j;->f:Lin/juspay/tracker/j;

    :cond_e
    sget-object v0, Lin/juspay/tracker/j;->f:Lin/juspay/tracker/j;

    monitor-exit v1

    return-object v0

    :catchall_12
    move-exception v0

    monitor-exit v1
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_12

    throw v0
.end method

.method public static e()Z
    .registers 3

    const/4 v0, 0x1

    sget-object v1, Landroid/os/Build;->TAGS:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v2, "test-keys"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    :cond_d
    :goto_d
    return v0

    :cond_e
    :try_start_e
    new-instance v1, Ljava/io/File;

    const-string v2, "/system/app/Superuser.apk"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_18} :catch_1d

    move-result v1

    if-nez v1, :cond_d

    :goto_1b
    const/4 v0, 0x0

    goto :goto_d

    :catch_1d
    move-exception v0

    goto :goto_1b
.end method

.method private i(Landroid/content/Context;)Landroid/util/DisplayMetrics;
    .registers 5

    :try_start_0
    iget-object v0, p0, Lin/juspay/tracker/j;->d:Landroid/util/DisplayMetrics;

    if-nez v0, :cond_e

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lin/juspay/tracker/j;->d:Landroid/util/DisplayMetrics;

    :cond_e
    iget-object v0, p0, Lin/juspay/tracker/j;->d:Landroid/util/DisplayMetrics;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_10} :catch_11

    :goto_10
    return-object v0

    :catch_11
    move-exception v0

    sget-object v1, Lin/juspay/tracker/j;->a:Ljava/lang/String;

    const-string v2, "Exception caught trying to get display metrics"

    invoke-static {v1, v2, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_10
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .registers 5

    :try_start_0
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1b

    const-string v0, "wifi"

    :goto_1a
    return-object v0

    :cond_1b
    const-string v0, "cellular"
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1d} :catch_1e

    goto :goto_1a

    :catch_1e
    move-exception v0

    sget-object v1, Lin/juspay/tracker/j;->a:Ljava/lang/String;

    const-string v2, "Exception trying to get network info"

    invoke-static {v1, v2, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public a(Ljava/lang/String;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/tracker/j;->b:Ljava/lang/String;

    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 8

    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    sget-object v1, Lin/juspay/tracker/j;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Permission not found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_29
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_29} :catch_2a

    goto :goto_10

    :catch_2a
    move-exception v1

    sget-object v2, Lin/juspay/tracker/j;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception trying to fetch permission info: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " returning FALSE"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_10
.end method

.method public b(Landroid/content/Context;)I
    .registers 5

    :try_start_0
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_b} :catch_d

    move-result v0

    :goto_c
    return v0

    :catch_d
    move-exception v0

    sget-object v1, Lin/juspay/tracker/j;->a:Ljava/lang/String;

    const-string v2, "Exception trying to get network type"

    invoke-static {v1, v2, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, -0x1

    goto :goto_c
.end method

.method public b()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lin/juspay/tracker/j;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/tracker/j;->c:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lin/juspay/tracker/j;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c(Landroid/content/Context;)Ljava/lang/String;
    .registers 5

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_f} :catch_10

    :goto_f
    return-object v0

    :catch_10
    move-exception v0

    sget-object v1, Lin/juspay/tracker/j;->a:Ljava/lang/String;

    const-string v2, "Exception trying to getVersionName"

    invoke-static {v1, v2, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_f
.end method

.method public c(Ljava/lang/String;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/tracker/j;->e:Ljava/lang/String;

    return-void
.end method

.method public d()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lin/juspay/tracker/j;->e:Ljava/lang/String;

    return-object v0
.end method

.method public d(Landroid/content/Context;)Ljava/lang/String;
    .registers 3

    invoke-direct {p0, p1}, Lin/juspay/tracker/j;->i(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    if-eqz v0, :cond_d

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public d(Ljava/lang/String;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/tracker/j;->h:Ljava/lang/String;

    return-void
.end method

.method public e(Landroid/content/Context;)Ljava/lang/String;
    .registers 3

    invoke-direct {p0, p1}, Lin/juspay/tracker/j;->i(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    if-eqz v0, :cond_d

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public f(Landroid/content/Context;)Ljava/lang/String;
    .registers 3

    invoke-direct {p0, p1}, Lin/juspay/tracker/j;->i(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    if-eqz v0, :cond_d

    iget v0, v0, Landroid/util/DisplayMetrics;->xdpi:F

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public g(Landroid/content/Context;)Ljava/lang/String;
    .registers 6

    const/4 v1, 0x0

    :try_start_1
    const-string v0, "android.permission.READ_PHONE_STATE"

    invoke-virtual {p0, p1, v0}, Lin/juspay/tracker/j;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-static {}, Lin/juspay/tracker/a;->a()Lin/juspay/tracker/a;

    move-result-object v2

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lin/juspay/tracker/a;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1c} :catch_20

    move-result-object v0

    :goto_1d
    return-object v0

    :cond_1e
    move-object v0, v1

    goto :goto_1d

    :catch_20
    move-exception v0

    sget-object v2, Lin/juspay/tracker/j;->a:Ljava/lang/String;

    const-string v3, "Exception trying to get device id"

    invoke-static {v2, v3, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_1d
.end method

.method public h(Landroid/content/Context;)Z
    .registers 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_1c

    :try_start_4
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "development_settings_enabled"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_e} :catch_14

    move-result v2

    if-ne v2, v0, :cond_12

    :goto_11
    return v0

    :cond_12
    move v0, v1

    goto :goto_11

    :catch_14
    move-exception v0

    sget-object v2, Lin/juspay/tracker/j;->a:Ljava/lang/String;

    const-string v3, "Exception while getting dev options enabled"

    invoke-static {v2, v3, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1c
    move v0, v1

    goto :goto_11
.end method
