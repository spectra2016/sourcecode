.class public final enum Lin/juspay/tracker/b$b;
.super Ljava/lang/Enum;
.source "Event.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lin/juspay/tracker/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lin/juspay/tracker/b$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lin/juspay/tracker/b$b;

.field public static final enum b:Lin/juspay/tracker/b$b;

.field public static final enum c:Lin/juspay/tracker/b$b;

.field public static final enum d:Lin/juspay/tracker/b$b;

.field public static final enum e:Lin/juspay/tracker/b$b;

.field public static final enum f:Lin/juspay/tracker/b$b;

.field private static final synthetic h:[Lin/juspay/tracker/b$b;


# instance fields
.field private final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lin/juspay/tracker/b$b;

    const-string v1, "UI"

    const-string v2, "ui"

    invoke-direct {v0, v1, v4, v2}, Lin/juspay/tracker/b$b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$b;->a:Lin/juspay/tracker/b$b;

    new-instance v0, Lin/juspay/tracker/b$b;

    const-string v1, "GODEL"

    const-string v2, "godel"

    invoke-direct {v0, v1, v5, v2}, Lin/juspay/tracker/b$b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$b;->b:Lin/juspay/tracker/b$b;

    new-instance v0, Lin/juspay/tracker/b$b;

    const-string v1, "ACS"

    const-string v2, "acs"

    invoke-direct {v0, v1, v6, v2}, Lin/juspay/tracker/b$b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$b;->c:Lin/juspay/tracker/b$b;

    new-instance v0, Lin/juspay/tracker/b$b;

    const-string v1, "CONFIG"

    const-string v2, "config"

    invoke-direct {v0, v1, v7, v2}, Lin/juspay/tracker/b$b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$b;->d:Lin/juspay/tracker/b$b;

    new-instance v0, Lin/juspay/tracker/b$b;

    const-string v1, "UBER"

    const-string v2, "uber"

    invoke-direct {v0, v1, v8, v2}, Lin/juspay/tracker/b$b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$b;->e:Lin/juspay/tracker/b$b;

    new-instance v0, Lin/juspay/tracker/b$b;

    const-string v1, "DUI"

    const/4 v2, 0x5

    const-string v3, "dui"

    invoke-direct {v0, v1, v2, v3}, Lin/juspay/tracker/b$b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$b;->f:Lin/juspay/tracker/b$b;

    const/4 v0, 0x6

    new-array v0, v0, [Lin/juspay/tracker/b$b;

    sget-object v1, Lin/juspay/tracker/b$b;->a:Lin/juspay/tracker/b$b;

    aput-object v1, v0, v4

    sget-object v1, Lin/juspay/tracker/b$b;->b:Lin/juspay/tracker/b$b;

    aput-object v1, v0, v5

    sget-object v1, Lin/juspay/tracker/b$b;->c:Lin/juspay/tracker/b$b;

    aput-object v1, v0, v6

    sget-object v1, Lin/juspay/tracker/b$b;->d:Lin/juspay/tracker/b$b;

    aput-object v1, v0, v7

    sget-object v1, Lin/juspay/tracker/b$b;->e:Lin/juspay/tracker/b$b;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lin/juspay/tracker/b$b;->f:Lin/juspay/tracker/b$b;

    aput-object v2, v0, v1

    sput-object v0, Lin/juspay/tracker/b$b;->h:[Lin/juspay/tracker/b$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lin/juspay/tracker/b$b;->g:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lin/juspay/tracker/b$b;)Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lin/juspay/tracker/b$b;->g:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lin/juspay/tracker/b$b;
    .registers 2

    const-class v0, Lin/juspay/tracker/b$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lin/juspay/tracker/b$b;

    return-object v0
.end method

.method public static values()[Lin/juspay/tracker/b$b;
    .registers 1

    sget-object v0, Lin/juspay/tracker/b$b;->h:[Lin/juspay/tracker/b$b;

    invoke-virtual {v0}, [Lin/juspay/tracker/b$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lin/juspay/tracker/b$b;

    return-object v0
.end method
