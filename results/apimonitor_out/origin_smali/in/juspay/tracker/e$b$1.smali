.class Lin/juspay/tracker/e$b$1;
.super Ljava/lang/Object;
.source "GodelTracker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lin/juspay/tracker/e$b;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lin/juspay/tracker/e$b;


# direct methods
.method constructor <init>(Lin/juspay/tracker/e$b;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/tracker/e$b$1;->a:Lin/juspay/tracker/e$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    const/4 v1, 0x0

    const/4 v0, 0x1

    :try_start_2
    invoke-static {v0}, Lin/juspay/tracker/e;->b(Z)Z

    iget-object v0, p0, Lin/juspay/tracker/e$b$1;->a:Lin/juspay/tracker/e$b;

    iget-object v0, v0, Lin/juspay/tracker/e$b;->a:Lin/juspay/tracker/e;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lin/juspay/tracker/e;->a(Z)V

    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lin/juspay/tracker/e$b$1;->a:Lin/juspay/tracker/e$b;

    iget-object v3, v3, Lin/juspay/tracker/e$b;->a:Lin/juspay/tracker/e;

    invoke-static {v3}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/e;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/residueLogs.log"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lin/juspay/tracker/e$b$1;->a:Lin/juspay/tracker/e$b;

    iget-object v4, v4, Lin/juspay/tracker/e$b;->a:Lin/juspay/tracker/e;

    invoke-static {v4}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/e;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/sendingLogs"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".log"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_71

    invoke-virtual {v0, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    :cond_71
    iget-object v0, p0, Lin/juspay/tracker/e$b$1;->a:Lin/juspay/tracker/e$b;

    iget-object v0, v0, Lin/juspay/tracker/e$b;->a:Lin/juspay/tracker/e;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lin/juspay/tracker/e;->a(Z)V

    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lin/juspay/tracker/e$b$1;->a:Lin/juspay/tracker/e$b;

    iget-object v3, v3, Lin/juspay/tracker/e$b;->a:Lin/juspay/tracker/e;

    invoke-static {v3}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/e;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Lin/juspay/tracker/e$b$1$1;

    invoke-direct {v2, p0}, Lin/juspay/tracker/e$b$1$1;-><init>(Lin/juspay/tracker/e$b$1;)V

    invoke-virtual {v0, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v2

    if-lez v3, :cond_14d

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_149

    iget-object v0, p0, Lin/juspay/tracker/e$b$1;->a:Lin/juspay/tracker/e$b;

    invoke-static {v0, v2}, Lin/juspay/tracker/e$b;->a(Lin/juspay/tracker/e$b;Ljava/io/File;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object v3, v2

    move-object v2, v0

    :goto_c1
    const/4 v0, 0x0

    const-string v4, "["

    invoke-virtual {v2, v0, v4}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "{}]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "data is "

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_d5} :catch_13e

    :try_start_d5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "[{}]"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_126

    new-instance v0, Lorg/json/JSONArray;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_ea
    .catch Lorg/json/JSONException; {:try_start_d5 .. :try_end_ea} :catch_122
    .catch Ljava/lang/Exception; {:try_start_d5 .. :try_end_ea} :catch_13e

    :goto_ea
    :try_start_ea
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "data"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_128

    if-eqz v3, :cond_128

    new-instance v0, Lin/juspay/tracker/e$b$1$2;

    invoke-direct {v0, p0, v1, v3}, Lin/juspay/tracker/e$b$1$2;-><init>(Lin/juspay/tracker/e$b$1;Ljava/lang/String;Ljava/io/File;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lin/juspay/tracker/e$b$1$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_107
    invoke-static {}, Lin/juspay/tracker/e;->i()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Post data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lin/juspay/tracker/f;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_121
    .catch Lorg/json/JSONException; {:try_start_ea .. :try_end_121} :catch_12d
    .catch Ljava/lang/Exception; {:try_start_ea .. :try_end_121} :catch_13e

    :goto_121
    return-void

    :catch_122
    move-exception v0

    :try_start_123
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_126
    .catch Ljava/lang/Exception; {:try_start_123 .. :try_end_126} :catch_13e

    :cond_126
    move-object v0, v1

    goto :goto_ea

    :cond_128
    const/4 v0, 0x0

    :try_start_129
    invoke-static {v0}, Lin/juspay/tracker/e;->b(Z)Z
    :try_end_12c
    .catch Lorg/json/JSONException; {:try_start_129 .. :try_end_12c} :catch_12d
    .catch Ljava/lang/Exception; {:try_start_129 .. :try_end_12c} :catch_13e

    goto :goto_107

    :catch_12d
    move-exception v0

    :try_start_12e
    invoke-static {}, Lin/juspay/tracker/e;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    invoke-static {v0}, Lin/juspay/tracker/e;->b(Z)Z
    :try_end_13d
    .catch Ljava/lang/Exception; {:try_start_12e .. :try_end_13d} :catch_13e

    goto :goto_121

    :catch_13e
    move-exception v0

    invoke-static {}, Lin/juspay/tracker/e;->i()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception posting logs"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_121

    :cond_149
    move-object v3, v2

    move-object v2, v0

    goto/16 :goto_c1

    :cond_14d
    move-object v2, v0

    move-object v3, v1

    goto/16 :goto_c1
.end method
