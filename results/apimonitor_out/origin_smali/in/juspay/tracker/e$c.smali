.class Lin/juspay/tracker/e$c;
.super Ljava/util/TimerTask;
.source "GodelTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lin/juspay/tracker/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lin/juspay/tracker/e;


# direct methods
.method constructor <init>(Lin/juspay/tracker/e;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/tracker/e$c;->a:Lin/juspay/tracker/e;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 9

    const/4 v2, 0x0

    invoke-static {}, Lin/juspay/tracker/e;->h()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_a7

    invoke-static {}, Lin/juspay/tracker/e;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_a7

    iget-object v0, p0, Lin/juspay/tracker/e$c;->a:Lin/juspay/tracker/e;

    invoke-virtual {v0}, Lin/juspay/tracker/e;->c()Z

    move-result v0

    if-nez v0, :cond_a7

    iget-object v0, p0, Lin/juspay/tracker/e$c;->a:Lin/juspay/tracker/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lin/juspay/tracker/e;->a(Z)V

    :try_start_1f
    invoke-static {}, Lin/juspay/tracker/e;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move v1, v2

    :goto_2f
    if-ge v1, v3, :cond_71

    invoke-static {}, Lin/juspay/tracker/e;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v6, Lorg/json/JSONObject;

    invoke-static {}, Lin/juspay/tracker/e;->h()Ljava/util/List;

    move-result-object v0

    const/4 v7, 0x0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {v6, v0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ","

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lin/juspay/tracker/e;->h()Ljava/util/List;

    move-result-object v0

    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_6d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2f

    :cond_71
    iget-object v0, p0, Lin/juspay/tracker/e$c;->a:Lin/juspay/tracker/e;

    invoke-static {v0}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/e;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "residueLogs.log"

    const v3, 0x8000

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_91
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_91} :catch_a8

    :goto_91
    iget-object v0, p0, Lin/juspay/tracker/e$c;->a:Lin/juspay/tracker/e;

    invoke-virtual {v0, v2}, Lin/juspay/tracker/e;->a(Z)V

    iget-object v0, p0, Lin/juspay/tracker/e$c;->a:Lin/juspay/tracker/e;

    invoke-static {v0}, Lin/juspay/tracker/e;->b(Lin/juspay/tracker/e;)Z

    move-result v0

    if-eqz v0, :cond_a7

    iget-object v0, p0, Lin/juspay/tracker/e$c;->a:Lin/juspay/tracker/e;

    invoke-static {v0}, Lin/juspay/tracker/e;->c(Lin/juspay/tracker/e;)Ljava/util/TimerTask;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimerTask;->run()V

    :cond_a7
    return-void

    :catch_a8
    move-exception v0

    invoke-static {}, Lin/juspay/tracker/e;->i()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Exception writing logs to a file"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_91
.end method
