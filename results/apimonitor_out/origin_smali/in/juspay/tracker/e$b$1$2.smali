.class Lin/juspay/tracker/e$b$1$2;
.super Landroid/os/AsyncTask;
.source "GodelTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lin/juspay/tracker/e$b$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/io/File;

.field final synthetic c:Lin/juspay/tracker/e$b$1;


# direct methods
.method constructor <init>(Lin/juspay/tracker/e$b$1;Ljava/lang/String;Ljava/io/File;)V
    .registers 4

    iput-object p1, p0, Lin/juspay/tracker/e$b$1$2;->c:Lin/juspay/tracker/e$b$1;

    iput-object p2, p0, Lin/juspay/tracker/e$b$1$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lin/juspay/tracker/e$b$1$2;->b:Ljava/io/File;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    :try_start_0
    iget-object v0, p0, Lin/juspay/tracker/e$b$1$2;->c:Lin/juspay/tracker/e$b$1;

    iget-object v0, v0, Lin/juspay/tracker/e$b$1;->a:Lin/juspay/tracker/e$b;

    iget-object v0, v0, Lin/juspay/tracker/e$b;->a:Lin/juspay/tracker/e;

    invoke-static {v0}, Lin/juspay/tracker/e;->d(Lin/juspay/tracker/e;)Lin/juspay/tracker/e$a;

    move-result-object v0

    sget-object v1, Lin/juspay/tracker/e$a;->a:Lin/juspay/tracker/e$a;

    if-ne v0, v1, :cond_26

    invoke-static {}, Lin/juspay/tracker/i;->a()Lin/juspay/tracker/i;

    move-result-object v0

    invoke-virtual {v0}, Lin/juspay/tracker/i;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/tracker/e$b$1$2;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lin/juspay/tracker/RestClient;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1b
    :goto_1b
    iget-object v0, p0, Lin/juspay/tracker/e$b$1$2;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_20} :catch_42

    :goto_20
    const/4 v0, 0x0

    invoke-static {v0}, Lin/juspay/tracker/e;->b(Z)Z

    const/4 v0, 0x0

    return-object v0

    :cond_26
    :try_start_26
    iget-object v0, p0, Lin/juspay/tracker/e$b$1$2;->c:Lin/juspay/tracker/e$b$1;

    iget-object v0, v0, Lin/juspay/tracker/e$b$1;->a:Lin/juspay/tracker/e$b;

    iget-object v0, v0, Lin/juspay/tracker/e$b;->a:Lin/juspay/tracker/e;

    invoke-static {v0}, Lin/juspay/tracker/e;->d(Lin/juspay/tracker/e;)Lin/juspay/tracker/e$a;

    move-result-object v0

    sget-object v1, Lin/juspay/tracker/e$a;->b:Lin/juspay/tracker/e$a;

    if-ne v0, v1, :cond_4d

    invoke-static {}, Lin/juspay/tracker/i;->a()Lin/juspay/tracker/i;

    move-result-object v0

    invoke-virtual {v0}, Lin/juspay/tracker/i;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/tracker/e$b$1$2;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lin/juspay/tracker/RestClient;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_41
    .catch Ljava/lang/Throwable; {:try_start_26 .. :try_end_41} :catch_42

    goto :goto_1b

    :catch_42
    move-exception v0

    invoke-static {}, Lin/juspay/tracker/e;->i()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception trying to post analytics data as JSON "

    invoke-static {v1, v2, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_20

    :cond_4d
    :try_start_4d
    iget-object v0, p0, Lin/juspay/tracker/e$b$1$2;->c:Lin/juspay/tracker/e$b$1;

    iget-object v0, v0, Lin/juspay/tracker/e$b$1;->a:Lin/juspay/tracker/e$b;

    iget-object v0, v0, Lin/juspay/tracker/e$b;->a:Lin/juspay/tracker/e;

    invoke-static {v0}, Lin/juspay/tracker/e;->d(Lin/juspay/tracker/e;)Lin/juspay/tracker/e$a;

    move-result-object v0

    sget-object v1, Lin/juspay/tracker/e$a;->c:Lin/juspay/tracker/e$a;

    if-ne v0, v1, :cond_1b

    invoke-static {}, Lin/juspay/tracker/i;->a()Lin/juspay/tracker/i;

    move-result-object v0

    invoke-virtual {v0}, Lin/juspay/tracker/i;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/tracker/e$b$1$2;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lin/juspay/tracker/RestClient;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_68
    .catch Ljava/lang/Throwable; {:try_start_4d .. :try_end_68} :catch_42

    goto :goto_1b
.end method
