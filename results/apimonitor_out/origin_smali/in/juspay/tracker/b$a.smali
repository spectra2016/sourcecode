.class public final enum Lin/juspay/tracker/b$a;
.super Ljava/lang/Enum;
.source "Event.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lin/juspay/tracker/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lin/juspay/tracker/b$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lin/juspay/tracker/b$a;

.field public static final enum b:Lin/juspay/tracker/b$a;

.field public static final enum c:Lin/juspay/tracker/b$a;

.field public static final enum d:Lin/juspay/tracker/b$a;

.field public static final enum e:Lin/juspay/tracker/b$a;

.field public static final enum f:Lin/juspay/tracker/b$a;

.field public static final enum g:Lin/juspay/tracker/b$a;

.field public static final enum h:Lin/juspay/tracker/b$a;

.field public static final enum i:Lin/juspay/tracker/b$a;

.field private static final synthetic k:[Lin/juspay/tracker/b$a;


# instance fields
.field private final j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lin/juspay/tracker/b$a;

    const-string v1, "CLICK"

    const-string v2, "click"

    invoke-direct {v0, v1, v4, v2}, Lin/juspay/tracker/b$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$a;->a:Lin/juspay/tracker/b$a;

    new-instance v0, Lin/juspay/tracker/b$a;

    const-string v1, "ASYNC"

    const-string v2, "async"

    invoke-direct {v0, v1, v5, v2}, Lin/juspay/tracker/b$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$a;->b:Lin/juspay/tracker/b$a;

    new-instance v0, Lin/juspay/tracker/b$a;

    const-string v1, "PREFERENCES"

    const-string v2, "prefs"

    invoke-direct {v0, v1, v6, v2}, Lin/juspay/tracker/b$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$a;->c:Lin/juspay/tracker/b$a;

    new-instance v0, Lin/juspay/tracker/b$a;

    const-string v1, "CHECK"

    const-string v2, "check"

    invoke-direct {v0, v1, v7, v2}, Lin/juspay/tracker/b$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$a;->d:Lin/juspay/tracker/b$a;

    new-instance v0, Lin/juspay/tracker/b$a;

    const-string v1, "WEBLAB"

    const-string v2, "weblab"

    invoke-direct {v0, v1, v8, v2}, Lin/juspay/tracker/b$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$a;->e:Lin/juspay/tracker/b$a;

    new-instance v0, Lin/juspay/tracker/b$a;

    const-string v1, "FALLBACK"

    const/4 v2, 0x5

    const-string v3, "fallback"

    invoke-direct {v0, v1, v2, v3}, Lin/juspay/tracker/b$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$a;->f:Lin/juspay/tracker/b$a;

    new-instance v0, Lin/juspay/tracker/b$a;

    const-string v1, "ERROR"

    const/4 v2, 0x6

    const-string v3, "error"

    invoke-direct {v0, v1, v2, v3}, Lin/juspay/tracker/b$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$a;->g:Lin/juspay/tracker/b$a;

    new-instance v0, Lin/juspay/tracker/b$a;

    const-string v1, "INFO"

    const/4 v2, 0x7

    const-string v3, "info"

    invoke-direct {v0, v1, v2, v3}, Lin/juspay/tracker/b$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$a;->h:Lin/juspay/tracker/b$a;

    new-instance v0, Lin/juspay/tracker/b$a;

    const-string v1, "OTHER"

    const/16 v2, 0x8

    const-string v3, "other"

    invoke-direct {v0, v1, v2, v3}, Lin/juspay/tracker/b$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lin/juspay/tracker/b$a;->i:Lin/juspay/tracker/b$a;

    const/16 v0, 0x9

    new-array v0, v0, [Lin/juspay/tracker/b$a;

    sget-object v1, Lin/juspay/tracker/b$a;->a:Lin/juspay/tracker/b$a;

    aput-object v1, v0, v4

    sget-object v1, Lin/juspay/tracker/b$a;->b:Lin/juspay/tracker/b$a;

    aput-object v1, v0, v5

    sget-object v1, Lin/juspay/tracker/b$a;->c:Lin/juspay/tracker/b$a;

    aput-object v1, v0, v6

    sget-object v1, Lin/juspay/tracker/b$a;->d:Lin/juspay/tracker/b$a;

    aput-object v1, v0, v7

    sget-object v1, Lin/juspay/tracker/b$a;->e:Lin/juspay/tracker/b$a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lin/juspay/tracker/b$a;->f:Lin/juspay/tracker/b$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lin/juspay/tracker/b$a;->g:Lin/juspay/tracker/b$a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lin/juspay/tracker/b$a;->h:Lin/juspay/tracker/b$a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lin/juspay/tracker/b$a;->i:Lin/juspay/tracker/b$a;

    aput-object v2, v0, v1

    sput-object v0, Lin/juspay/tracker/b$a;->k:[Lin/juspay/tracker/b$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lin/juspay/tracker/b$a;->j:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lin/juspay/tracker/b$a;
    .registers 2

    const-class v0, Lin/juspay/tracker/b$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lin/juspay/tracker/b$a;

    return-object v0
.end method

.method public static values()[Lin/juspay/tracker/b$a;
    .registers 1

    sget-object v0, Lin/juspay/tracker/b$a;->k:[Lin/juspay/tracker/b$a;

    invoke-virtual {v0}, [Lin/juspay/tracker/b$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lin/juspay/tracker/b$a;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lin/juspay/tracker/b$a;->j:Ljava/lang/String;

    return-object v0
.end method
