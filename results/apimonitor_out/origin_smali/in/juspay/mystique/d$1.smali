.class Lin/juspay/mystique/d$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lin/juspay/mystique/d;


# direct methods
.method constructor <init>(Lin/juspay/mystique/d;Ljava/lang/String;)V
    .registers 3

    iput-object p1, p0, Lin/juspay/mystique/d$1;->b:Lin/juspay/mystique/d;

    iput-object p2, p0, Lin/juspay/mystique/d$1;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    :try_start_0
    iget-object v0, p0, Lin/juspay/mystique/d$1;->b:Lin/juspay/mystique/d;

    invoke-static {v0}, Lin/juspay/mystique/d;->a(Lin/juspay/mystique/d;)Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_27

    iget-object v0, p0, Lin/juspay/mystique/d$1;->b:Lin/juspay/mystique/d;

    invoke-static {v0}, Lin/juspay/mystique/d;->a(Lin/juspay/mystique/d;)Landroid/webkit/WebView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "javascript:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lin/juspay/mystique/d$1;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :goto_26
    return-void

    :cond_27
    invoke-static {}, Lin/juspay/mystique/d;->d()Lin/juspay/mystique/c;

    move-result-object v0

    const-string v1, "DynamicUI"

    const-string v2, "browser null, call start first"

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_32
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_32} :catch_33
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_32} :catch_7b

    goto :goto_26

    :catch_33
    move-exception v0

    invoke-static {}, Lin/juspay/mystique/d;->d()Lin/juspay/mystique/c;

    move-result-object v1

    const-string v2, "DynamicUI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OutOfMemoryError :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lin/juspay/mystique/d$1;->b:Lin/juspay/mystique/d;

    invoke-static {v4, v0}, Lin/juspay/mystique/d;->a(Lin/juspay/mystique/d;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lin/juspay/mystique/d$1;->b:Lin/juspay/mystique/d;

    invoke-static {v1}, Lin/juspay/mystique/d;->b(Lin/juspay/mystique/d;)Lin/juspay/mystique/e;

    move-result-object v1

    const-string v2, "addJsToWebView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lin/juspay/mystique/d$1;->b:Lin/juspay/mystique/d;

    invoke-static {v4, v0}, Lin/juspay/mystique/d;->a(Lin/juspay/mystique/d;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_26

    :catch_7b
    move-exception v0

    invoke-static {}, Lin/juspay/mystique/d;->d()Lin/juspay/mystique/c;

    move-result-object v1

    const-string v2, "DynamicUI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lin/juspay/mystique/d$1;->b:Lin/juspay/mystique/d;

    invoke-static {v4, v0}, Lin/juspay/mystique/d;->a(Lin/juspay/mystique/d;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lin/juspay/mystique/d$1;->b:Lin/juspay/mystique/d;

    invoke-static {v1}, Lin/juspay/mystique/d;->b(Lin/juspay/mystique/d;)Lin/juspay/mystique/e;

    move-result-object v1

    const-string v2, "addJsToWebView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lin/juspay/mystique/d$1;->b:Lin/juspay/mystique/d;

    invoke-static {v4, v0}, Lin/juspay/mystique/d;->a(Lin/juspay/mystique/d;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_26
.end method
