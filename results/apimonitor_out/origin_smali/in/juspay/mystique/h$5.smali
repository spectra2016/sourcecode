.class Lin/juspay/mystique/h$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lin/juspay/mystique/h;->runInUI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lin/juspay/mystique/h;


# direct methods
.method constructor <init>(Lin/juspay/mystique/h;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    iput-object p1, p0, Lin/juspay/mystique/h$5;->e:Lin/juspay/mystique/h;

    iput-object p2, p0, Lin/juspay/mystique/h$5;->a:Ljava/lang/String;

    iput-object p3, p0, Lin/juspay/mystique/h$5;->b:Ljava/lang/String;

    iput-object p4, p0, Lin/juspay/mystique/h$5;->c:Ljava/lang/String;

    iput-object p5, p0, Lin/juspay/mystique/h$5;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    :try_start_0
    iget-object v0, p0, Lin/juspay/mystique/h$5;->e:Lin/juspay/mystique/h;

    invoke-static {v0}, Lin/juspay/mystique/h;->b(Lin/juspay/mystique/h;)Lin/juspay/mystique/j;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/mystique/h$5;->e:Lin/juspay/mystique/h;

    invoke-static {v1}, Lin/juspay/mystique/h;->f(Lin/juspay/mystique/h;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lin/juspay/mystique/h$5;->a:Ljava/lang/String;

    iget-object v3, p0, Lin/juspay/mystique/h$5;->b:Ljava/lang/String;

    iget-object v4, p0, Lin/juspay/mystique/h$5;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lin/juspay/mystique/j;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v0, p0, Lin/juspay/mystique/h$5;->d:Ljava/lang/String;

    if-eqz v0, :cond_3d

    iget-object v0, p0, Lin/juspay/mystique/h$5;->e:Lin/juspay/mystique/h;

    invoke-static {v0}, Lin/juspay/mystique/h;->c(Lin/juspay/mystique/h;)Lin/juspay/mystique/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window.callUICallback("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lin/juspay/mystique/h$5;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",\'success\');"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3d} :catch_3e

    :cond_3d
    :goto_3d
    return-void

    :catch_3e
    move-exception v0

    if-eqz v0, :cond_ad

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/mystique/h$5;->e:Lin/juspay/mystique/h;

    invoke-static {v1}, Lin/juspay/mystique/h;->d(Lin/juspay/mystique/h;)Lin/juspay/mystique/c;

    move-result-object v1

    const-string v2, "ERROR"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " excep: fn__runInUI  - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lin/juspay/mystique/h$5;->e:Lin/juspay/mystique/h;

    invoke-static {v4}, Lin/juspay/mystique/h;->b(Lin/juspay/mystique/h;)Lin/juspay/mystique/j;

    move-result-object v4

    invoke-virtual {v4}, Lin/juspay/mystique/j;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lin/juspay/mystique/h$5;->e:Lin/juspay/mystique/h;

    invoke-static {v1}, Lin/juspay/mystique/h;->e(Lin/juspay/mystique/h;)Lin/juspay/mystique/e;

    move-result-object v1

    const-string v2, "ERROR"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " excep: fn__runInUI  - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " - "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lin/juspay/mystique/h$5;->e:Lin/juspay/mystique/h;

    invoke-static {v3}, Lin/juspay/mystique/h;->b(Lin/juspay/mystique/h;)Lin/juspay/mystique/j;

    move-result-object v3

    invoke-virtual {v3}, Lin/juspay/mystique/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_ad
    iget-object v0, p0, Lin/juspay/mystique/h$5;->d:Ljava/lang/String;

    if-eqz v0, :cond_3d

    iget-object v0, p0, Lin/juspay/mystique/h$5;->e:Lin/juspay/mystique/h;

    invoke-static {v0}, Lin/juspay/mystique/h;->c(Lin/juspay/mystique/h;)Lin/juspay/mystique/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window.callUICallback("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lin/juspay/mystique/h$5;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",\'failure\');"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V

    goto/16 :goto_3d
.end method
