.class public Lin/juspay/mystique/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lin/juspay/mystique/g;

.field private b:Landroid/app/Activity;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/ViewGroup;

.field private f:Lin/juspay/mystique/e;

.field private g:Lin/juspay/mystique/d;

.field private h:Lin/juspay/mystique/c;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lin/juspay/mystique/d;)V
    .registers 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lin/juspay/mystique/j;->g:Lin/juspay/mystique/d;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    iput-object p1, p0, Lin/juspay/mystique/j;->b:Landroid/app/Activity;

    invoke-virtual {p2}, Lin/juspay/mystique/d;->c()Lin/juspay/mystique/e;

    move-result-object v0

    iput-object v0, p0, Lin/juspay/mystique/j;->f:Lin/juspay/mystique/e;

    invoke-static {}, Lin/juspay/mystique/d;->b()Lin/juspay/mystique/c;

    move-result-object v0

    iput-object v0, p0, Lin/juspay/mystique/j;->h:Lin/juspay/mystique/c;

    new-instance v0, Lin/juspay/mystique/g;

    iget-object v1, p0, Lin/juspay/mystique/j;->b:Landroid/app/Activity;

    iget-object v2, p0, Lin/juspay/mystique/j;->h:Lin/juspay/mystique/c;

    iget-object v3, p0, Lin/juspay/mystique/j;->f:Lin/juspay/mystique/e;

    invoke-direct {v0, v1, v2, v3, p2}, Lin/juspay/mystique/g;-><init>(Landroid/app/Activity;Lin/juspay/mystique/c;Lin/juspay/mystique/e;Lin/juspay/mystique/d;)V

    iput-object v0, p0, Lin/juspay/mystique/j;->a:Lin/juspay/mystique/g;

    return-void
.end method

.method private a(Landroid/view/View;)Landroid/view/View;
    .registers 6

    if-eqz p1, :cond_a

    iget-object v0, p0, Lin/juspay/mystique/j;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :goto_7
    iget-object v0, p0, Lin/juspay/mystique/j;->e:Landroid/view/ViewGroup;

    return-object v0

    :cond_a
    iget-object v0, p0, Lin/juspay/mystique/j;->f:Lin/juspay/mystique/e;

    const-string v1, "ERROR"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__Render -  instance null "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7
.end method

.method private b(Landroid/view/View;)V
    .registers 4

    iget-object v0, p0, Lin/juspay/mystique/j;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Lin/juspay/mystique/j;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeViewAt(I)V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)Landroid/view/View;
    .registers 12

    const/4 v9, 0x1

    const/4 v2, 0x0

    const-string v0, "type"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "props"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v1, "props"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-virtual {p0, v0, v3}, Lin/juspay/mystique/j;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_19
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    new-array v0, v9, [Ljava/lang/Class;

    const-class v1, Landroid/content/Context;

    aput-object v1, v0, v2

    invoke-virtual {v4, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    new-array v1, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lin/juspay/mystique/j;->b:Landroid/app/Activity;

    aput-object v5, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v5

    :goto_35
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_47

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v6, p0, Lin/juspay/mystique/j;->a:Lin/juspay/mystique/g;

    invoke-virtual {v6, v0, v3, v1}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/Object;)V

    goto :goto_35

    :cond_47
    const-string v0, "children"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    if-eqz v3, :cond_7c

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-eqz v0, :cond_7c

    move v0, v2

    :goto_56
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v0, v5, :cond_7c

    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {p0, v5}, Lin/juspay/mystique/j;->a(Lorg/json/JSONObject;)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_79

    const-string v6, "addView"

    new-array v7, v9, [Ljava/lang/Class;

    const-class v8, Landroid/view/View;

    aput-object v8, v7, v2

    invoke-virtual {v4, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v5, v7, v2

    invoke-virtual {v6, v1, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_79
    add-int/lit8 v0, v0, 0x1

    goto :goto_56

    :cond_7c
    move-object v0, v1

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .registers 8

    iget-object v0, p0, Lin/juspay/mystique/j;->a:Lin/juspay/mystique/g;

    const-string v1, "modifyDom"

    invoke-virtual {v0, v1}, Lin/juspay/mystique/g;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/j;->a:Lin/juspay/mystique/g;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lin/juspay/mystique/g;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/j;->a:Lin/juspay/mystique/g;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ln: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/mystique/g;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/j;->a:Lin/juspay/mystique/g;

    invoke-virtual {v0, p1, p2}, Lin/juspay/mystique/g;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lin/juspay/mystique/j;->a:Lin/juspay/mystique/g;

    invoke-virtual {v0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Landroid/view/ViewGroup;)V
    .registers 5

    iput-object p2, p0, Lin/juspay/mystique/j;->e:Landroid/view/ViewGroup;

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lin/juspay/mystique/j;->a(Lorg/json/JSONObject;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lin/juspay/mystique/j;->c:Landroid/view/View;

    iget-object v0, p0, Lin/juspay/mystique/j;->d:Landroid/view/View;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lin/juspay/mystique/j;->d:Landroid/view/View;

    iget-object v1, p0, Lin/juspay/mystique/j;->c:Landroid/view/View;

    if-eq v0, v1, :cond_1c

    iget-object v0, p0, Lin/juspay/mystique/j;->d:Landroid/view/View;

    invoke-direct {p0, v0}, Lin/juspay/mystique/j;->b(Landroid/view/View;)V

    :cond_1c
    iget-object v0, p0, Lin/juspay/mystique/j;->c:Landroid/view/View;

    invoke-direct {p0, v0}, Lin/juspay/mystique/j;->a(Landroid/view/View;)Landroid/view/View;

    iget-object v0, p0, Lin/juspay/mystique/j;->c:Landroid/view/View;

    iput-object v0, p0, Lin/juspay/mystique/j;->d:Landroid/view/View;

    return-void
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .registers 5

    iget-object v0, p0, Lin/juspay/mystique/j;->a:Lin/juspay/mystique/g;

    invoke-virtual {v0, p1}, Lin/juspay/mystique/g;->b(Ljava/lang/String;)V

    const-string v0, "node_id"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    const-string v0, "node_id"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/mystique/j;->a:Lin/juspay/mystique/g;

    invoke-virtual {v1, v0}, Lin/juspay/mystique/g;->a(Ljava/lang/String;)V

    :cond_18
    const-string v0, "__filename"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    const-string v0, "__filename"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/mystique/j;->a:Lin/juspay/mystique/g;

    invoke-virtual {v1, v0}, Lin/juspay/mystique/g;->c(Ljava/lang/String;)V

    :cond_2b
    return-void
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;IZ)V
    .registers 9

    iget-object v0, p0, Lin/juspay/mystique/j;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lin/juspay/mystique/j;->b:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-ltz p3, :cond_4a

    iget-object v1, p0, Lin/juspay/mystique/j;->b:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz p4, :cond_21

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_21
    invoke-virtual {p0, p2}, Lin/juspay/mystique/j;->a(Lorg/json/JSONObject;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2b

    invoke-virtual {v0, v1, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :goto_2a
    return-void

    :cond_2b
    iget-object v0, p0, Lin/juspay/mystique/j;->f:Lin/juspay/mystique/e;

    const-string v1, "ERROR"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__addViewToParent - child null "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2a

    :cond_4a
    const-string v0, "props"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_61

    const-string v0, "type"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "props"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lin/juspay/mystique/j;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    :cond_61
    iget-object v0, p0, Lin/juspay/mystique/j;->f:Lin/juspay/mystique/e;

    const-string v1, "ERROR"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__addViewToParent - negative index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2a
.end method
