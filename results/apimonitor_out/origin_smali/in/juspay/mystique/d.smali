.class public final Lin/juspay/mystique/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:Lin/juspay/mystique/c;


# instance fields
.field private a:Landroid/webkit/WebView;

.field private c:Landroid/app/Activity;

.field private d:Lin/juspay/mystique/e;

.field private e:Lin/juspay/mystique/h;

.field private f:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/widget/FrameLayout;Landroid/os/Bundle;Lin/juspay/mystique/e;)V
    .registers 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lin/juspay/mystique/a;

    invoke-direct {v0}, Lin/juspay/mystique/a;-><init>()V

    sput-object v0, Lin/juspay/mystique/d;->b:Lin/juspay/mystique/c;

    iput-object p4, p0, Lin/juspay/mystique/d;->d:Lin/juspay/mystique/e;

    if-eqz p1, :cond_4b

    if-eqz p2, :cond_4b

    iput-object p1, p0, Lin/juspay/mystique/d;->c:Landroid/app/Activity;

    iput-object p2, p0, Lin/juspay/mystique/d;->f:Landroid/widget/FrameLayout;

    new-instance v0, Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    invoke-direct {p0}, Lin/juspay/mystique/d;->e()V

    iget-object v0, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    iget-object v0, p0, Lin/juspay/mystique/d;->f:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    new-instance v0, Lin/juspay/mystique/h;

    invoke-direct {v0, p1, p2, p0}, Lin/juspay/mystique/h;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lin/juspay/mystique/d;)V

    iput-object v0, p0, Lin/juspay/mystique/d;->e:Lin/juspay/mystique/h;

    iget-object v0, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    iget-object v1, p0, Lin/juspay/mystique/d;->e:Lin/juspay/mystique/h;

    const-string v2, "Android"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_4a
    return-void

    :cond_4b
    sget-object v0, Lin/juspay/mystique/d;->b:Lin/juspay/mystique/c;

    const-string v1, "DynamicUI"

    const-string v2, "container or activity null"

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4a
.end method

.method static synthetic a(Lin/juspay/mystique/d;)Landroid/webkit/WebView;
    .registers 2

    iget-object v0, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic a(Lin/juspay/mystique/d;Ljava/lang/Object;)Ljava/lang/String;
    .registers 3

    invoke-direct {p0, p1}, Lin/juspay/mystique/d;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/String;
    .registers 6

    const-string v1, ""

    check-cast p1, Ljava/lang/Exception;

    invoke-virtual {p1}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v0, 0x0

    :goto_9
    array-length v3, v2

    if-ge v0, v3, :cond_2c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v3, v2, v0

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_2c
    return-object v1
.end method

.method public static b()Lin/juspay/mystique/c;
    .registers 1

    sget-object v0, Lin/juspay/mystique/d;->b:Lin/juspay/mystique/c;

    return-object v0
.end method

.method static synthetic b(Lin/juspay/mystique/d;)Lin/juspay/mystique/e;
    .registers 2

    iget-object v0, p0, Lin/juspay/mystique/d;->d:Lin/juspay/mystique/e;

    return-object v0
.end method

.method static synthetic d()Lin/juspay/mystique/c;
    .registers 1

    sget-object v0, Lin/juspay/mystique/d;->b:Lin/juspay/mystique/c;

    return-object v0
.end method

.method private e()V
    .registers 6

    const/4 v4, 0x1

    iget-object v0, p0, Lin/juspay/mystique/d;->c:Landroid/app/Activity;

    if-eqz v0, :cond_60

    iget-object v0, p0, Lin/juspay/mystique/d;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/mystique/d;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "is_dui_debuggable"

    const-string v3, "string"

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_48

    iget-object v1, p0, Lin/juspay/mystique/d;->c:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_48

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_48

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_34

    invoke-static {v4}, Landroid/webkit/WebView;->setWebContentsDebuggingEnabled(Z)V

    :cond_34
    iget-object v0, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    new-instance v1, Landroid/webkit/WebChromeClient;

    invoke-direct {v1}, Landroid/webkit/WebChromeClient;-><init>()V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    new-instance v0, Landroid/webkit/WebViewClient;

    invoke-direct {v0}, Landroid/webkit/WebViewClient;-><init>()V

    iget-object v1, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    :cond_48
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_61

    iget-object v0, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    iget-object v0, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    :cond_60
    :goto_60
    return-void

    :cond_61
    const-string v0, "DUI"

    const-string v1, "Will throw cors error if less than version < 16"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_60
.end method


# virtual methods
.method public a()V
    .registers 7

    const/4 v5, 0x0

    iget-object v0, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    const-string v1, "http://juspay.in"

    const-string v2, "<html></html>"

    const-string v3, "text/html"

    const-string v4, "utf-8"

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/d;->f:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->removeAllViews()V

    iget-object v0, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    iput-object v5, p0, Lin/juspay/mystique/d;->c:Landroid/app/Activity;

    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "JavascriptInterface"
        }
    .end annotation

    iget-object v0, p0, Lin/juspay/mystique/d;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4

    iget-object v0, p0, Lin/juspay/mystique/d;->c:Landroid/app/Activity;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lin/juspay/mystique/d;->c:Landroid/app/Activity;

    new-instance v1, Lin/juspay/mystique/d$1;

    invoke-direct {v1, p0, p1}, Lin/juspay/mystique/d$1;-><init>(Lin/juspay/mystique/d;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_e
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 4

    iget-object v0, p0, Lin/juspay/mystique/d;->c:Landroid/app/Activity;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lin/juspay/mystique/d;->c:Landroid/app/Activity;

    new-instance v1, Lin/juspay/mystique/d$2;

    invoke-direct {v1, p0, p1}, Lin/juspay/mystique/d$2;-><init>(Lin/juspay/mystique/d;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_e
    return-void
.end method

.method public c()Lin/juspay/mystique/e;
    .registers 2

    iget-object v0, p0, Lin/juspay/mystique/d;->d:Lin/juspay/mystique/e;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .registers 4

    iget-object v0, p0, Lin/juspay/mystique/d;->e:Lin/juspay/mystique/h;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lin/juspay/mystique/d;->e:Lin/juspay/mystique/h;

    invoke-virtual {v0, p1}, Lin/juspay/mystique/h;->setState(Ljava/lang/String;)V

    return-void

    :cond_a
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "JS-Interface not initailised"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d(Ljava/lang/String;)V
    .registers 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "window.onActivityLifeCycleEvent(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V

    return-void
.end method
