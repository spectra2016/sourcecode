.class public Lin/juspay/mystique/g;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lin/juspay/mystique/g$a;
    }
.end annotation


# static fields
.field static a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lin/juspay/mystique/g$a;",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field

.field private static b:F

.field private static c:F

.field private static d:F

.field private static e:F

.field private static h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/lang/String;

.field private static final v:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private f:Landroid/app/Activity;

.field private g:Lin/juspay/mystique/e;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Lin/juspay/mystique/d;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/util/regex/Pattern;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/util/regex/Pattern;

.field private w:Lin/juspay/mystique/c;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lin/juspay/mystique/g;->h:Ljava/util/HashMap;

    const-class v0, Lin/juspay/mystique/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/juspay/mystique/g;->i:Ljava/lang/String;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lin/juspay/mystique/g;->v:Ljava/util/Map;

    sget-object v0, Lin/juspay/mystique/g;->v:Ljava/util/Map;

    const-class v1, Ljava/lang/Boolean;

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lin/juspay/mystique/g;->v:Ljava/util/Map;

    const-class v1, Ljava/lang/Character;

    sget-object v2, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lin/juspay/mystique/g;->v:Ljava/util/Map;

    const-class v1, Ljava/lang/Byte;

    sget-object v2, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lin/juspay/mystique/g;->v:Ljava/util/Map;

    const-class v1, Ljava/lang/Short;

    sget-object v2, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lin/juspay/mystique/g;->v:Ljava/util/Map;

    const-class v1, Ljava/lang/Integer;

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lin/juspay/mystique/g;->v:Ljava/util/Map;

    const-class v1, Ljava/lang/Long;

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lin/juspay/mystique/g;->v:Ljava/util/Map;

    const-class v1, Ljava/lang/Float;

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lin/juspay/mystique/g;->v:Ljava/util/Map;

    const-class v1, Ljava/lang/Double;

    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lin/juspay/mystique/g;->v:Ljava/util/Map;

    const-class v1, Ljava/lang/Void;

    sget-object v2, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lin/juspay/mystique/g;->a:Ljava/util/HashMap;

    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Lin/juspay/mystique/c;Lin/juspay/mystique/e;Lin/juspay/mystique/d;)V
    .registers 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "-1"

    iput-object v0, p0, Lin/juspay/mystique/g;->j:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lin/juspay/mystique/g;->k:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lin/juspay/mystique/g;->l:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lin/juspay/mystique/g;->m:Ljava/lang/String;

    const-string v0, ":"

    iput-object v0, p0, Lin/juspay/mystique/g;->o:Ljava/lang/String;

    const-string v0, ","

    iput-object v0, p0, Lin/juspay/mystique/g;->p:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "(?<!\\\\)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lin/juspay/mystique/g;->q:Ljava/util/regex/Pattern;

    const-string v0, "->"

    iput-object v0, p0, Lin/juspay/mystique/g;->r:Ljava/lang/String;

    const-string v0, "_"

    iput-object v0, p0, Lin/juspay/mystique/g;->s:Ljava/lang/String;

    const-string v0, "="

    iput-object v0, p0, Lin/juspay/mystique/g;->t:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "(?<!\\\\)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lin/juspay/mystique/g;->u:Ljava/util/regex/Pattern;

    iput-object p4, p0, Lin/juspay/mystique/g;->n:Lin/juspay/mystique/d;

    iput-object p1, p0, Lin/juspay/mystique/g;->f:Landroid/app/Activity;

    iput-object p2, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    iput-object p3, p0, Lin/juspay/mystique/g;->g:Lin/juspay/mystique/e;

    sget-object v0, Lin/juspay/mystique/g;->h:Ljava/util/HashMap;

    const-string v1, "duiObj"

    invoke-virtual {v0, v1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)I
    .registers 8

    const/4 v3, -0x1

    invoke-virtual {p1, p3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v3, :cond_2a

    if-eqz v0, :cond_2a

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_2a

    add-int v1, v0, p3

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x5c

    if-ne v1, v2, :cond_2a

    add-int/2addr v0, p3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {p0, p1, p2, v0}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    :cond_29
    :goto_29
    return v0

    :cond_2a
    if-eq v0, v3, :cond_29

    add-int/2addr v0, p3

    goto :goto_29
.end method

.method static synthetic a(Lin/juspay/mystique/g;)Lin/juspay/mystique/d;
    .registers 2

    iget-object v0, p0, Lin/juspay/mystique/g;->n:Lin/juspay/mystique/d;

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .registers 14

    const/4 v8, -0x1

    const/4 v1, 0x0

    const/4 v9, 0x1

    const/4 v6, 0x0

    iput-object p3, p0, Lin/juspay/mystique/g;->k:Ljava/lang/String;

    iget-object v0, p0, Lin/juspay/mystique/g;->r:Ljava/lang/String;

    invoke-direct {p0, p3, v0, v6}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v8, :cond_440

    iget-object v0, p0, Lin/juspay/mystique/g;->r:Ljava/lang/String;

    invoke-direct {p0, p3, v0}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v6

    iget-object v2, p0, Lin/juspay/mystique/g;->s:Ljava/lang/String;

    invoke-direct {p0, v0, v2, v6}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v8, :cond_4a2

    const/4 v2, 0x3

    invoke-virtual {v0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "get"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4a2

    iget-object v2, p0, Lin/juspay/mystique/g;->s:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v0, v2, v9

    aget-object v2, v2, v6

    :goto_35
    iget-object v3, p0, Lin/juspay/mystique/g;->o:Ljava/lang/String;

    invoke-direct {p0, p3, v3, v6}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    if-eq v3, v8, :cond_85

    iget-object v3, p0, Lin/juspay/mystique/g;->r:Ljava/lang/String;

    invoke-direct {p0, p3, v3}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v4, v3, v9

    iget-object v3, p0, Lin/juspay/mystique/g;->o:Ljava/lang/String;

    invoke-direct {p0, v4, v3}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v3, v5, v6

    aget-object v5, v5, v9

    :goto_4f
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_4a6

    :cond_56
    move v7, v8

    :goto_57
    packed-switch v7, :pswitch_data_4b4

    const-string v0, "var_"

    invoke-direct {p0, v3, v0, v6}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v8, :cond_3a1

    iget-object v0, p0, Lin/juspay/mystique/g;->s:Ljava/lang/String;

    invoke-direct {p0, v3, v0}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v9

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    iget-object v2, p0, Lin/juspay/mystique/g;->o:Ljava/lang/String;

    invoke-direct {p0, v4, v2}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v9

    invoke-direct {p0, v2}, Lin/juspay/mystique/g;->g(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_84
    :goto_84
    return-object p2

    :cond_85
    iget-object v3, p0, Lin/juspay/mystique/g;->r:Ljava/lang/String;

    invoke-direct {p0, p3, v3}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v9

    move-object v4, v3

    move-object v5, v1

    goto :goto_4f

    :sswitch_90
    const-string v7, "this"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_56

    move v7, v6

    goto :goto_57

    :sswitch_9a
    const-string v7, "ctx"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_56

    move v7, v9

    goto :goto_57

    :sswitch_a4
    const-string v7, "get"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_56

    const/4 v7, 0x2

    goto :goto_57

    :pswitch_ae
    if-eqz v5, :cond_115

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_c3

    invoke-direct {p0, v5}, Lin/juspay/mystique/g;->d(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto :goto_84

    :cond_c3
    iget-object v0, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__runCommand - classMethodDetails  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/g;->g:Lin/juspay/mystique/e;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__runCommand - classMethodDetails  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_84

    :cond_115
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_125

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/16 :goto_84

    :cond_125
    iget-object v0, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__runCommand - this  classMethodDetails "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/g;->g:Lin/juspay/mystique/e;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__runCommand - this  classMethodDetails "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_84

    :pswitch_177
    if-eqz v5, :cond_1e3

    iget-object v0, p0, Lin/juspay/mystique/g;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_191

    iget-object v1, p0, Lin/juspay/mystique/g;->f:Landroid/app/Activity;

    invoke-direct {p0, v5}, Lin/juspay/mystique/g;->d(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/16 :goto_84

    :cond_191
    iget-object v0, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__runCommand - ctx  classMethodDetails "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/g;->g:Lin/juspay/mystique/e;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__runCommand - ctx  classMethodDetails "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_84

    :cond_1e3
    iget-object v0, p0, Lin/juspay/mystique/g;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_1f7

    iget-object v2, p0, Lin/juspay/mystique/g;->f:Landroid/app/Activity;

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/16 :goto_84

    :cond_1f7
    iget-object v0, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__runCommand - ctx classMethodDetails  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/g;->g:Lin/juspay/mystique/e;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__runCommand - ctx classMethodDetails  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_84

    :pswitch_249
    if-eqz v0, :cond_84

    sget-object v2, Lin/juspay/mystique/g;->h:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    iget-object v7, p0, Lin/juspay/mystique/g;->s:Ljava/lang/String;

    invoke-direct {p0, v3, v7, v6}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v8, :cond_325

    if-eqz v2, :cond_325

    if-eqz v5, :cond_2c3

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_271

    invoke-direct {p0, v5}, Lin/juspay/mystique/g;->d(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/16 :goto_84

    :cond_271
    iget-object v0, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__runCommand - get classMethodDetails "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/g;->g:Lin/juspay/mystique/e;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__runCommand - get classMethodDetails "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_84

    :cond_2c3
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_2d3

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/16 :goto_84

    :cond_2d3
    iget-object v0, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__runCommand - get classMethodDetails : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/g;->g:Lin/juspay/mystique/e;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__runCommand - get classMethodDetails : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_84

    :cond_325
    if-eqz v2, :cond_343

    iget-object v1, p0, Lin/juspay/mystique/g;->s:Ljava/lang/String;

    invoke-direct {p0, v3, v1}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v9

    sget-object v2, Lin/juspay/mystique/g;->h:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lin/juspay/mystique/g;->o:Ljava/lang/String;

    invoke-direct {p0, v4, v2}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v9

    invoke-direct {p0, v0, v1, v2}, Lin/juspay/mystique/g;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    goto/16 :goto_84

    :cond_343
    iget-object v1, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    const-string v2, "WARNING"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " isNull : fn__runCommand - get_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lin/juspay/mystique/g;->g:Lin/juspay/mystique/e;

    const-string v2, "WARNING"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " isNull : fn__runCommand - get_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " is null"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_84

    :cond_3a1
    const-string v0, "new"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3b9

    iget-object v0, p0, Lin/juspay/mystique/g;->o:Ljava/lang/String;

    invoke-direct {p0, v4, v0}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v6

    const-string v3, "new"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_414

    :cond_3b9
    if-eqz v5, :cond_40a

    const-string v0, "in.juspay.mystique.DuiInvocationHandler"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3d2

    invoke-direct {p0, v5}, Lin/juspay/mystique/g;->d(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    new-instance p2, Lin/juspay/mystique/b;

    aget-object v0, v0, v6

    iget-object v1, p0, Lin/juspay/mystique/g;->n:Lin/juspay/mystique/d;

    invoke-direct {p2, v0, v1}, Lin/juspay/mystique/b;-><init>(Ljava/lang/Object;Lin/juspay/mystique/d;)V

    goto/16 :goto_84

    :cond_3d2
    invoke-direct {p0, v5}, Lin/juspay/mystique/g;->e(Ljava/lang/String;)[Ljava/lang/Class;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v2

    move v0, v6

    :goto_3df
    array-length v3, v2

    if-ge v0, v3, :cond_84

    aget-object v3, v2, v0

    invoke-virtual {v3}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v3

    array-length v3, v3

    invoke-direct {p0, v5}, Lin/juspay/mystique/g;->h(Ljava/lang/String;)I

    move-result v4

    if-ne v3, v4, :cond_407

    aget-object v3, v2, v0

    invoke-virtual {v3}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v3

    invoke-direct {p0, v3, v1}, Lin/juspay/mystique/g;->a([Ljava/lang/Class;[Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_407

    aget-object v0, v2, v0

    invoke-direct {p0, v5}, Lin/juspay/mystique/g;->d(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/16 :goto_84

    :cond_407
    add-int/lit8 v0, v0, 0x1

    goto :goto_3df

    :cond_40a
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object p2

    goto/16 :goto_84

    :cond_414
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_84

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "forName"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_436

    invoke-direct {p0, v5}, Lin/juspay/mystique/g;->g(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p2

    goto/16 :goto_84

    :cond_436
    invoke-direct {p0, v5}, Lin/juspay/mystique/g;->d(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/16 :goto_84

    :cond_440
    if-nez p2, :cond_472

    iget-object v0, p0, Lin/juspay/mystique/g;->o:Ljava/lang/String;

    invoke-direct {p0, p3, v0, v6}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v8, :cond_464

    iget-object v0, p0, Lin/juspay/mystique/g;->o:Ljava/lang/String;

    invoke-direct {p0, p3, v0}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v9

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {p0, v1, p3}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-direct {p0, v0}, Lin/juspay/mystique/g;->d(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/16 :goto_84

    :cond_464
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/16 :goto_84

    :cond_472
    iget-object v0, p0, Lin/juspay/mystique/g;->o:Ljava/lang/String;

    invoke-direct {p0, p3, v0, v6}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v8, :cond_494

    iget-object v0, p0, Lin/juspay/mystique/g;->o:Ljava/lang/String;

    invoke-direct {p0, p3, v0}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v9

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {p0, v1, p3}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-direct {p0, v0}, Lin/juspay/mystique/g;->d(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, p2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/16 :goto_84

    :cond_494
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, p2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/16 :goto_84

    :cond_4a2
    move-object v2, v0

    move-object v0, v1

    goto/16 :goto_35

    :sswitch_data_4a6
    .sparse-switch
        0x18227 -> :sswitch_9a
        0x18f56 -> :sswitch_a4
        0x364e9e -> :sswitch_90
    .end sparse-switch

    :pswitch_data_4b4
    .packed-switch 0x0
        :pswitch_ae
        :pswitch_177
        :pswitch_249
    .end packed-switch
.end method

.method private a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .registers 10

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_8
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_8} :catch_13

    move-result-object v1

    :cond_9
    if-eqz v1, :cond_32

    invoke-direct {p0, p3}, Lin/juspay/mystique/g;->g(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_12
    return-object p1

    :catch_13
    move-exception v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1f
    if-ge v2, v4, :cond_9

    aget-object v0, v3, v2

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4d

    :goto_2d
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1f

    :cond_32
    iget-object v0, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    sget-object v1, Lin/juspay/mystique/g;->i:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t set field for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_12

    :cond_4d
    move-object v0, v1

    goto :goto_2d
.end method

.method private a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;
    .registers 8

    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    if-nez p1, :cond_6

    :goto_5
    return-object v0

    :cond_6
    iget-object v1, p0, Lin/juspay/mystique/g;->o:Ljava/lang/String;

    invoke-direct {p0, p2, v1, v3}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_35

    iget-object v1, p0, Lin/juspay/mystique/g;->o:Ljava/lang/String;

    invoke-direct {p0, p2, v1}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object p2, v1, v3

    aget-object v1, v1, v4

    :goto_19
    if-eqz v1, :cond_1f

    invoke-direct {p0, v1}, Lin/juspay/mystique/g;->e(Ljava/lang/String;)[Ljava/lang/Class;

    move-result-object v0

    :cond_1f
    new-instance v1, Lin/juspay/mystique/g$a;

    invoke-direct {v1, p1, p2, v0}, Lin/juspay/mystique/g$a;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sget-object v2, Lin/juspay/mystique/g;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_37

    sget-object v0, Lin/juspay/mystique/g;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Method;

    goto :goto_5

    :cond_35
    move-object v1, v0

    goto :goto_19

    :cond_37
    :try_start_37
    invoke-direct {p0, p1, p2, v0}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_3a
    .catch Ljava/lang/NoSuchMethodException; {:try_start_37 .. :try_end_3a} :catch_41

    move-result-object v0

    :goto_3b
    sget-object v2, Lin/juspay/mystique/g;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :catch_41
    move-exception v2

    if-eqz v0, :cond_4e

    array-length v2, v0

    if-ne v2, v4, :cond_4e

    aget-object v0, v0, v3

    invoke-direct {p0, p1, p2, v0}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    goto :goto_3b

    :cond_4e
    invoke-direct {p0, p1, p2, v0}, Lin/juspay/mystique/g;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    goto :goto_3b
.end method

.method private a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .registers 11

    const/4 v1, 0x0

    invoke-static {p3}, Lin/juspay/mystique/g;->a(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1b

    const/4 v0, 0x1

    :try_start_8
    new-array v2, v0, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v0, Lin/juspay/mystique/g;->v:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    aput-object v0, v2, v3

    invoke-virtual {p1, p2, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_18
    .catch Ljava/lang/NoSuchMethodException; {:try_start_8 .. :try_end_18} :catch_1a

    move-result-object v0

    :goto_19
    return-object v0

    :catch_1a
    move-exception v0

    :cond_1b
    invoke-virtual {p3}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_21
    if-ge v0, v3, :cond_34

    aget-object v4, v2, v0

    const/4 v5, 0x1

    :try_start_26
    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    invoke-virtual {p1, p2, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2e
    .catch Ljava/lang/NoSuchMethodException; {:try_start_26 .. :try_end_2e} :catch_30

    move-result-object v0

    goto :goto_19

    :catch_30
    move-exception v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_21

    :cond_34
    const/4 v0, 0x1

    :try_start_35
    new-array v0, v0, [Ljava/lang/Class;

    const/4 v2, 0x0

    aput-object p3, v0, v2

    invoke-virtual {p1, p2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_3d
    .catch Ljava/lang/NoSuchMethodException; {:try_start_35 .. :try_end_3d} :catch_3f

    move-result-object v0

    goto :goto_19

    :catch_3f
    move-exception v0

    invoke-virtual {p3}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p3

    if-nez p3, :cond_1b

    iget-object v0, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    sget-object v1, Lin/juspay/mystique/g;->i:Ljava/lang/String;

    const-string v2, "Never reach here"

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_19
.end method

.method private a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .registers 5

    invoke-virtual {p1, p2, p3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Class;)Z
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    sget-object v0, Lin/juspay/mystique/g;->v:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private a([Ljava/lang/Class;[Ljava/lang/Class;)Z
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v2, 0x0

    move v1, v2

    :goto_3
    array-length v0, p1

    if-ge v1, v0, :cond_92

    aget-object v0, p2, v1

    if-eqz v0, :cond_5f

    aget-object v0, p1, v1

    if-eqz v0, :cond_5f

    aget-object v0, p1, v1

    const-class v4, Ljava/lang/Object;

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    aget-object v0, p2, v1

    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v0

    if-eqz v0, :cond_5f

    :cond_20
    aget-object v0, p1, v1

    aget-object v4, p2, v1

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5f

    aget-object v0, p1, v1

    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v0

    if-eqz v0, :cond_63

    aget-object v0, p2, v1

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_63

    :try_start_3a
    aget-object v0, p2, v1

    const-string v4, "TYPE"

    invoke-virtual {v0, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    aget-object v4, p1, v1

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_4e
    .catch Ljava/lang/Exception; {:try_start_3a .. :try_end_4e} :catch_52

    move-result v0

    if-nez v0, :cond_5f

    :cond_51
    :goto_51
    return v2

    :catch_52
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v4, "java.lang.NoSuchFieldException"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_51

    :cond_5f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_63
    aget-object v0, p1, v1

    const-class v4, Ljava/lang/ClassLoader;

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7d

    aget-object v0, p2, v1

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v4, "dalvik.system.PathClassLoader"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5f

    move v2, v3

    goto :goto_51

    :cond_7d
    aget-object v0, p1, v1

    aget-object v4, p2, v1

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_51

    aget-object v0, p1, v1

    aget-object v4, p2, v1

    invoke-virtual {v0, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_5f

    goto :goto_51

    :cond_92
    move v2, v3

    goto :goto_51
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .registers 8

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, v3}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    const/4 v0, -0x1

    if-ne v1, v0, :cond_e

    new-array v0, v4, [Ljava/lang/String;

    aput-object p1, v0, v3

    :goto_d
    return-object v0

    :cond_e
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    goto :goto_d
.end method

.method static synthetic b()F
    .registers 1

    sget v0, Lin/juspay/mystique/g;->c:F

    return v0
.end method

.method static synthetic b(F)F
    .registers 1

    sput p0, Lin/juspay/mystique/g;->b:F

    return p0
.end method

.method private b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .registers 10

    invoke-virtual {p1}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_2e

    aget-object v0, v2, v1

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2a

    if-eqz p3, :cond_2a

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v4

    array-length v4, v4

    array-length v5, p3

    if-ne v4, v5, :cond_2a

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v4

    invoke-direct {p0, v4, p3}, Lin/juspay/mystique/g;->a([Ljava/lang/Class;[Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_2a

    :goto_29
    return-object v0

    :cond_2a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_2e
    const/4 v0, 0x0

    goto :goto_29
.end method

.method static synthetic c()F
    .registers 1

    sget v0, Lin/juspay/mystique/g;->b:F

    return v0
.end method

.method static synthetic c(F)F
    .registers 1

    sput p0, Lin/juspay/mystique/g;->d:F

    return p0
.end method

.method static synthetic d()F
    .registers 1

    sget v0, Lin/juspay/mystique/g;->e:F

    return v0
.end method

.method static synthetic d(F)F
    .registers 1

    sput p0, Lin/juspay/mystique/g;->c:F

    return p0
.end method

.method private d(Ljava/lang/String;)[Ljava/lang/Object;
    .registers 7

    const/4 v0, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lin/juspay/mystique/g;->p:Ljava/lang/String;

    invoke-direct {p0, p1, v2, v0}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1b

    invoke-direct {p0, p1}, Lin/juspay/mystique/g;->g(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_16
    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_1b
    iget-object v2, p0, Lin/juspay/mystique/g;->q:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    :goto_22
    if-ge v0, v3, :cond_16

    aget-object v4, v2, v0

    invoke-direct {p0, v4}, Lin/juspay/mystique/g;->g(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_22
.end method

.method static synthetic e()F
    .registers 1

    sget v0, Lin/juspay/mystique/g;->d:F

    return v0
.end method

.method static synthetic e(F)F
    .registers 1

    sput p0, Lin/juspay/mystique/g;->e:F

    return p0
.end method

.method private e(Ljava/lang/String;)[Ljava/lang/Class;
    .registers 7

    const/4 v4, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lin/juspay/mystique/g;->p:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2e

    iget-object v0, p0, Lin/juspay/mystique/g;->q:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v3

    array-length v0, v3

    if-le v0, v4, :cond_2e

    array-length v0, v3

    new-array v2, v0, [Ljava/lang/Class;

    :goto_1b
    array-length v0, v3

    if-ge v1, v0, :cond_2c

    aget-object v0, v3, v1

    invoke-direct {p0, v0}, Lin/juspay/mystique/g;->f(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1b

    :cond_2c
    move-object v0, v2

    goto :goto_5

    :cond_2e
    new-array v2, v4, [Ljava/lang/Class;

    invoke-direct {p0, p1}, Lin/juspay/mystique/g;->f(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    aput-object v0, v2, v1

    move-object v0, v2

    goto :goto_5
.end method

.method private f(Ljava/lang/String;)Ljava/lang/Object;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Any:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TAny;"
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_131

    iget-object v1, p0, Lin/juspay/mystique/g;->s:Ljava/lang/String;

    invoke-direct {p0, p1, v1}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v4, v3, v0

    const/4 v1, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_170

    :cond_14
    move v0, v1

    :goto_15
    packed-switch v0, :pswitch_data_1a6

    :goto_18
    const-class v0, Ljava/lang/String;

    :goto_1a
    return-object v0

    :sswitch_1b
    const-string v5, "i"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    goto :goto_15

    :sswitch_24
    const-string v0, "b"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    move v0, v2

    goto :goto_15

    :sswitch_2e
    const-string v0, "cs"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x2

    goto :goto_15

    :sswitch_38
    const-string v0, "f"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x3

    goto :goto_15

    :sswitch_42
    const-string v0, "dp"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x4

    goto :goto_15

    :sswitch_4c
    const-string v0, "sp"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x5

    goto :goto_15

    :sswitch_56
    const-string v0, "l"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x6

    goto :goto_15

    :sswitch_60
    const-string v0, "get"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x7

    goto :goto_15

    :sswitch_6a
    const-string v0, "dpf"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/16 v0, 0x8

    goto :goto_15

    :sswitch_75
    const-string v0, "strget"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/16 v0, 0x9

    goto :goto_15

    :sswitch_80
    const-string v0, "ctx"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/16 v0, 0xa

    goto :goto_15

    :sswitch_8b
    const-string v0, "s"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/16 v0, 0xb

    goto :goto_15

    :sswitch_96
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/16 v0, 0xc

    goto/16 :goto_15

    :pswitch_a2
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    goto/16 :goto_1a

    :pswitch_a6
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    goto/16 :goto_1a

    :pswitch_aa
    const-class v0, Ljava/lang/CharSequence;

    goto/16 :goto_1a

    :pswitch_ae
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    goto/16 :goto_1a

    :pswitch_b2
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    goto/16 :goto_1a

    :pswitch_b6
    const-class v0, Ljava/lang/Float;

    goto/16 :goto_1a

    :pswitch_ba
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    goto/16 :goto_1a

    :pswitch_be
    sget-object v0, Lin/juspay/mystique/g;->h:Ljava/util/HashMap;

    aget-object v1, v3, v2

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_ce

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto/16 :goto_1a

    :cond_ce
    iget-object v0, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__getClassType - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/g;->g:Lin/juspay/mystique/e;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__getClassType - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :pswitch_11e
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    goto/16 :goto_1a

    :pswitch_122
    const-class v0, Ljava/lang/CharSequence;

    goto/16 :goto_1a

    :pswitch_126
    const-class v0, Landroid/content/Context;

    goto/16 :goto_1a

    :pswitch_12a
    const-class v0, Ljava/lang/String;

    goto/16 :goto_1a

    :pswitch_12e
    const/4 v0, 0x0

    goto/16 :goto_1a

    :cond_131
    iget-object v0, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__getClassType -  toConvert "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/g;->g:Lin/juspay/mystique/e;

    const-string v1, "WARNING"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " isNull : fn__getClassType -  toConvert "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_18

    nop

    :sswitch_data_170
    .sparse-switch
        -0x352aa87b -> :sswitch_75
        0x62 -> :sswitch_24
        0x66 -> :sswitch_38
        0x69 -> :sswitch_1b
        0x6c -> :sswitch_56
        0x73 -> :sswitch_8b
        0xc70 -> :sswitch_2e
        0xc8c -> :sswitch_42
        0xe5d -> :sswitch_4c
        0x18227 -> :sswitch_80
        0x1855a -> :sswitch_6a
        0x18f56 -> :sswitch_60
        0x33c587 -> :sswitch_96
    .end sparse-switch

    :pswitch_data_1a6
    .packed-switch 0x0
        :pswitch_a2
        :pswitch_a6
        :pswitch_aa
        :pswitch_ae
        :pswitch_b2
        :pswitch_b6
        :pswitch_ba
        :pswitch_be
        :pswitch_11e
        :pswitch_122
        :pswitch_126
        :pswitch_12a
        :pswitch_12e
    .end packed-switch
.end method

.method private g(Ljava/lang/String;)Ljava/lang/Object;
    .registers 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Any:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TAny;"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/16 v8, 0x5c

    const/4 v3, -0x1

    if-eqz p1, :cond_1d3

    iget-object v0, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    const-string v5, "getValue!"

    invoke-interface {v0, v5, p1}, Lin/juspay/mystique/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/g;->s:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v5, v0, v2

    aget-object v0, v0, v4

    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v3, :cond_2f

    const-string v6, ";"

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-eq v6, v3, :cond_2f

    const-string v6, "\\\\;"

    const-string v7, ";"

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    :cond_2f
    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v3, :cond_45

    const-string v6, "_"

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-eq v6, v3, :cond_45

    const-string v6, "\\\\_"

    const-string v7, "_"

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    :cond_45
    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v3, :cond_5b

    const-string v6, ":"

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-eq v6, v3, :cond_5b

    const-string v6, "\\\\:"

    const-string v7, ":"

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    :cond_5b
    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v3, :cond_71

    const-string v6, ","

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-eq v6, v3, :cond_71

    const-string v6, "\\\\,"

    const-string v7, ","

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    :cond_71
    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v3, :cond_87

    const-string v6, "="

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-eq v6, v3, :cond_87

    const-string v6, "\\\\="

    const-string v7, "="

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    :cond_87
    if-eqz v0, :cond_195

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_212

    :cond_90
    move v2, v3

    :goto_91
    packed-switch v2, :pswitch_data_244

    :goto_94
    :pswitch_94
    return-object v0

    :sswitch_95
    const-string v4, "i"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_90

    goto :goto_91

    :sswitch_9e
    const-string v2, "b"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_90

    move v2, v4

    goto :goto_91

    :sswitch_a8
    const-string v2, "f"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_90

    const/4 v2, 0x2

    goto :goto_91

    :sswitch_b2
    const-string v2, "s"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_90

    const/4 v2, 0x3

    goto :goto_91

    :sswitch_bc
    const-string v2, "sp"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_90

    const/4 v2, 0x4

    goto :goto_91

    :sswitch_c6
    const-string v2, "dp"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_90

    const/4 v2, 0x5

    goto :goto_91

    :sswitch_d0
    const-string v2, "dpf"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_90

    const/4 v2, 0x6

    goto :goto_91

    :sswitch_da
    const-string v2, "l"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_90

    const/4 v2, 0x7

    goto :goto_91

    :sswitch_e4
    const-string v2, "get"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_90

    const/16 v2, 0x8

    goto :goto_91

    :sswitch_ef
    const-string v2, "ctx"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_90

    const/16 v2, 0x9

    goto :goto_91

    :sswitch_fa
    const-string v2, "null"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_90

    const/16 v2, 0xa

    goto :goto_91

    :sswitch_105
    const-string v2, "strget"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_90

    const/16 v2, 0xb

    goto :goto_91

    :pswitch_110
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_94

    :pswitch_11a
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_94

    :pswitch_124
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto/16 :goto_94

    :pswitch_12e
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iget-object v1, p0, Lin/juspay/mystique/g;->f:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->scaledDensity:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto/16 :goto_94

    :pswitch_145
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lin/juspay/mystique/g;->a(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_94

    :pswitch_153
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {p0, v0}, Lin/juspay/mystique/g;->a(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto/16 :goto_94

    :pswitch_161
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_94

    :pswitch_16b
    sget-object v1, Lin/juspay/mystique/g;->h:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_94

    :pswitch_173
    iget-object v0, p0, Lin/juspay/mystique/g;->f:Landroid/app/Activity;

    goto/16 :goto_94

    :pswitch_177
    move-object v0, v1

    goto/16 :goto_94

    :pswitch_17a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lin/juspay/mystique/g;->h:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_94

    :cond_195
    iget-object v1, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    const-string v2, "WARNING"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " isNull : fn__getValue - value "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lin/juspay/mystique/g;->g:Lin/juspay/mystique/e;

    const-string v2, "WARNING"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " isNull : fn__getValue - value "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_94

    :cond_1d3
    iget-object v0, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    const-string v2, "WARNING"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " isNull : fn__getValue - value "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lin/juspay/mystique/g;->g:Lin/juspay/mystique/e;

    const-string v2, "WARNING"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " isNull : fn__getValue - value "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_94

    :sswitch_data_212
    .sparse-switch
        -0x352aa87b -> :sswitch_105
        0x62 -> :sswitch_9e
        0x66 -> :sswitch_a8
        0x69 -> :sswitch_95
        0x6c -> :sswitch_da
        0x73 -> :sswitch_b2
        0xc8c -> :sswitch_c6
        0xe5d -> :sswitch_bc
        0x18227 -> :sswitch_ef
        0x1855a -> :sswitch_d0
        0x18f56 -> :sswitch_e4
        0x33c587 -> :sswitch_fa
    .end sparse-switch

    :pswitch_data_244
    .packed-switch 0x0
        :pswitch_110
        :pswitch_11a
        :pswitch_124
        :pswitch_94
        :pswitch_12e
        :pswitch_145
        :pswitch_153
        :pswitch_161
        :pswitch_16b
        :pswitch_173
        :pswitch_177
        :pswitch_17a
    .end packed-switch
.end method

.method private h(Ljava/lang/String;)I
    .registers 3

    iget-object v0, p0, Lin/juspay/mystique/g;->q:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    return v0
.end method


# virtual methods
.method public a(F)F
    .registers 4

    const/4 v0, 0x0

    cmpl-float v1, p1, v0

    if-lez v1, :cond_17

    iget-object v0, p0, Lin/juspay/mystique/g;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    :cond_17
    return v0
.end method

.method public a(I)I
    .registers 4

    if-lez p1, :cond_15

    iget-object v0, p0, Lin/juspay/mystique/g;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    int-to-float v1, p1

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method public a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .registers 15

    const/4 v11, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lin/juspay/mystique/g;->u:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    array-length v4, v3

    move v1, v2

    :goto_b
    if-ge v1, v4, :cond_67

    aget-object v5, v3, v1

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5f

    iget-object v6, p0, Lin/juspay/mystique/g;->t:Ljava/lang/String;

    invoke-direct {p0, v5, v6, v2}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_62

    iget-object v6, p0, Lin/juspay/mystique/g;->t:Ljava/lang/String;

    invoke-direct {p0, v5, v6}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v6, v5, v2

    iget-object v7, p0, Lin/juspay/mystique/g;->s:Ljava/lang/String;

    invoke-direct {p0, v6, v7}, Lin/juspay/mystique/g;->a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v11

    aget-object v5, v5, v11

    invoke-direct {p0, p1, v0, v5}, Lin/juspay/mystique/g;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    sget-object v7, Lin/juspay/mystique/g;->h:Ljava/util/HashMap;

    invoke-virtual {v7, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v7, p0, Lin/juspay/mystique/g;->w:Lin/juspay/mystique/c;

    sget-object v8, Lin/juspay/mystique/g;->i:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setting "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " to "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v7, v8, v5}, Lin/juspay/mystique/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5f
    :goto_5f
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_62
    invoke-direct {p0, p1, v0, v5}, Lin/juspay/mystique/g;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_5f

    :cond_67
    return-object p1
.end method

.method public a()Ljava/lang/String;
    .registers 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lin/juspay/mystique/g;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/mystique/g;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/mystique/g;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/mystique/g;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/mystique/g;->j:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/Object;)V
    .registers 16

    const/4 v3, 0x0

    const/16 v9, 0xb

    const/4 v11, -0x1

    const/4 v8, 0x1

    const/4 v2, 0x0

    :try_start_6
    const-string v1, "pattern"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_50

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v4, "setFilters"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, [Landroid/text/InputFilter;

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const-string v1, "pattern"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    aget-object v5, v1, v5

    array-length v6, v1

    if-ne v6, v8, :cond_251

    const/16 v1, 0x2710

    :goto_34
    new-instance v6, Lin/juspay/mystique/g$1;

    invoke-direct {v6, p0, v5}, Lin/juspay/mystique/g$1;-><init>(Lin/juspay/mystique/g;Ljava/lang/String;)V

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/text/InputFilter;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    const/4 v6, 0x1

    new-instance v7, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v7, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v7, v5, v6

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v5, v1, v6

    invoke-virtual {v4, p3, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_50
    const-string v1, "onKeyUp"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7e

    const-string v1, "onKeyUp"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "setOnKeyListener"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/view/View$OnKeyListener;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Lin/juspay/mystique/g$5;

    invoke-direct {v7, p0, v1}, Lin/juspay/mystique/g$5;-><init>(Lin/juspay/mystique/g;Ljava/lang/String;)V

    aput-object v7, v5, v6

    invoke-virtual {v4, p3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7e
    const-string v1, "onLongPress"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_ac

    const-string v1, "onLongPress"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "setOnLongClickListener"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/view/View$OnLongClickListener;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Lin/juspay/mystique/g$6;

    invoke-direct {v7, p0, v1}, Lin/juspay/mystique/g$6;-><init>(Lin/juspay/mystique/g;Ljava/lang/String;)V

    aput-object v7, v5, v6

    invoke-virtual {v4, p3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_ac
    const-string v1, "onClick"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_da

    const-string v1, "onClick"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "setOnClickListener"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/view/View$OnClickListener;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Lin/juspay/mystique/g$7;

    invoke-direct {v7, p0, v1}, Lin/juspay/mystique/g$7;-><init>(Lin/juspay/mystique/g;Ljava/lang/String;)V

    aput-object v7, v5, v6

    invoke-virtual {v4, p3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_da
    const-string v1, "onItemClick"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_108

    const-string v1, "onItemClick"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "setOnItemSelectedListener"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/widget/AdapterView$OnItemSelectedListener;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Lin/juspay/mystique/g$8;

    invoke-direct {v7, p0, v1}, Lin/juspay/mystique/g$8;-><init>(Lin/juspay/mystique/g;Ljava/lang/String;)V

    aput-object v7, v5, v6

    invoke-virtual {v4, p3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_108
    const-string v1, "onChange"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_136

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v4, "addTextChangedListener"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/text/TextWatcher;

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const-string v4, "onChange"

    invoke-virtual {p2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Lin/juspay/mystique/g$9;

    invoke-direct {v7, p0, v4}, Lin/juspay/mystique/g$9;-><init>(Lin/juspay/mystique/g;Ljava/lang/String;)V

    aput-object v7, v5, v6

    invoke-virtual {v1, p3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_136
    const-string v1, "onFocus"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_164

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v4, "setOnFocusChangeListener"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/view/View$OnFocusChangeListener;

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const-string v4, "onFocus"

    invoke-virtual {p2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Lin/juspay/mystique/g$10;

    invoke-direct {v7, p0, v4}, Lin/juspay/mystique/g$10;-><init>(Lin/juspay/mystique/g;Ljava/lang/String;)V

    aput-object v7, v5, v6

    invoke-virtual {v1, p3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_164
    const-string v1, "onTouch"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_192

    const-string v1, "onTouch"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "setOnTouchListener"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/view/View$OnTouchListener;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Lin/juspay/mystique/g$11;

    invoke-direct {v7, p0, v1}, Lin/juspay/mystique/g$11;-><init>(Lin/juspay/mystique/g;Ljava/lang/String;)V

    aput-object v7, v5, v6

    invoke-virtual {v4, p3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_192
    const-string v1, "onDateChange"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c4

    const-string v1, "onDateChange"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "setOnDateChangeListener"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/widget/CalendarView$OnDateChangeListener;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v9, :cond_1c4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Lin/juspay/mystique/g$12;

    invoke-direct {v7, p0, v1}, Lin/juspay/mystique/g$12;-><init>(Lin/juspay/mystique/g;Ljava/lang/String;)V

    aput-object v7, v5, v6

    invoke-virtual {v4, p3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1c4
    const-string v1, "onSwipe"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f2

    const-string v1, "onSwipe"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "setOnTouchListener"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/view/View$OnTouchListener;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Lin/juspay/mystique/g$2;

    invoke-direct {v7, p0, v1}, Lin/juspay/mystique/g$2;-><init>(Lin/juspay/mystique/g;Ljava/lang/String;)V

    aput-object v7, v5, v6

    invoke-virtual {v4, p3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1f2
    const-string v1, "popupMenu"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_272

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v9, :cond_272

    const-string v1, "popupMenu"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lin/juspay/mystique/g;->q:Ljava/util/regex/Pattern;

    invoke-virtual {v4}, Ljava/util/regex/Pattern;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const-string v1, "onMenuItemClick"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Landroid/widget/PopupMenu;

    iget-object v7, p0, Lin/juspay/mystique/g;->f:Landroid/app/Activity;

    move-object v0, p3

    check-cast v0, Landroid/view/View;

    move-object v1, v0

    invoke-direct {v6, v7, v1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    move v1, v2

    :goto_220
    array-length v7, v4

    if-ge v1, v7, :cond_25e

    aget-object v7, v4, v1

    const-string v8, "\\"

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    if-eq v7, v11, :cond_243

    aget-object v7, v4, v1

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    if-eq v7, v11, :cond_243

    aget-object v7, v4, v1

    const-string v8, "\\\\,"

    const-string v9, ","

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v1

    :cond_243
    invoke-virtual {v6}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    aget-object v10, v4, v1

    invoke-interface {v7, v8, v1, v9, v10}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    add-int/lit8 v1, v1, 0x1

    goto :goto_220

    :cond_251
    const/4 v6, 0x1

    aget-object v1, v1, v6

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_34

    :cond_25e
    new-instance v1, Lin/juspay/mystique/g$3;

    invoke-direct {v1, p0, v5}, Lin/juspay/mystique/g$3;-><init>(Lin/juspay/mystique/g;Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    move-object v0, p3

    check-cast v0, Landroid/view/View;

    move-object v1, v0

    new-instance v4, Lin/juspay/mystique/g$4;

    invoke-direct {v4, p0, v6}, Lin/juspay/mystique/g$4;-><init>(Lin/juspay/mystique/g;Landroid/widget/PopupMenu;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_272
    const-string v1, "localImage"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_29d

    const-string v1, "localImage"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    move v1, v2

    move-object v4, v3

    :goto_288
    array-length v6, v5

    if-ge v1, v6, :cond_296

    if-nez v4, :cond_296

    aget-object v4, v5, v1

    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_288

    :cond_296
    move-object v0, p3

    check-cast v0, Landroid/widget/ImageView;

    move-object v1, v0

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_29d
    const-string v1, "localBackgoundImage"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2d9

    const-string v1, "localImage"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    move v1, v2

    move-object v2, v3

    :goto_2b3
    array-length v3, v4

    if-ge v1, v3, :cond_2c1

    if-nez v2, :cond_2c1

    aget-object v2, v4, v1

    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_2b3

    :cond_2c1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v1, v3, :cond_325

    move-object v0, p3

    check-cast v0, Landroid/view/View;

    move-object v1, v0

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lin/juspay/mystique/g;->f:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_2d9
    :goto_2d9
    const-string v1, "runInUI"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e8

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p3, v1}, Lin/juspay/mystique/g;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    :cond_2e8
    const-string v1, "afterRender"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_324

    const-string v1, "id"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "javascript:window.callUICallback(\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "afterRender"

    invoke-virtual {p2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\', \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\');"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lin/juspay/mystique/g;->n:Lin/juspay/mystique/d;

    invoke-virtual {v2, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V

    :cond_324
    :goto_324
    return-void

    :cond_325
    move-object v0, p3

    check-cast v0, Landroid/view/View;

    move-object v1, v0

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lin/juspay/mystique/g;->f:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_337
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_337} :catch_338

    goto :goto_2d9

    :catch_338
    move-exception v1

    if-eqz v1, :cond_324

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lin/juspay/mystique/g;->g:Lin/juspay/mystique/e;

    const-string v3, "WARNING"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " excep: fn__parseKeys  - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " - "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lin/juspay/mystique/g;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_324
.end method

.method public b(Ljava/lang/String;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/mystique/g;->l:Ljava/lang/String;

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .registers 2

    iput-object p1, p0, Lin/juspay/mystique/g;->m:Ljava/lang/String;

    return-void
.end method
