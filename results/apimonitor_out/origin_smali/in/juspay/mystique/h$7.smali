.class Lin/juspay/mystique/h$7;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lin/juspay/mystique/h;->setImage(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lin/juspay/mystique/h;


# direct methods
.method constructor <init>(Lin/juspay/mystique/h;ILjava/lang/String;)V
    .registers 4

    iput-object p1, p0, Lin/juspay/mystique/h$7;->c:Lin/juspay/mystique/h;

    iput p2, p0, Lin/juspay/mystique/h$7;->a:I

    iput-object p3, p0, Lin/juspay/mystique/h$7;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    :try_start_0
    iget-object v0, p0, Lin/juspay/mystique/h$7;->c:Lin/juspay/mystique/h;

    invoke-static {v0}, Lin/juspay/mystique/h;->f(Lin/juspay/mystique/h;)Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lin/juspay/mystique/h$7;->a:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lin/juspay/mystique/h$7;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_20} :catch_21

    :cond_20
    :goto_20
    return-void

    :catch_21
    move-exception v0

    if-eqz v0, :cond_20

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/mystique/h$7;->c:Lin/juspay/mystique/h;

    invoke-static {v1}, Lin/juspay/mystique/h;->d(Lin/juspay/mystique/h;)Lin/juspay/mystique/c;

    move-result-object v1

    const-string v2, "ERROR"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " excep: fn__setImage  - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lin/juspay/mystique/h$7;->c:Lin/juspay/mystique/h;

    invoke-static {v4}, Lin/juspay/mystique/h;->b(Lin/juspay/mystique/h;)Lin/juspay/mystique/j;

    move-result-object v4

    invoke-virtual {v4}, Lin/juspay/mystique/j;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lin/juspay/mystique/h$7;->c:Lin/juspay/mystique/h;

    invoke-static {v1}, Lin/juspay/mystique/h;->e(Lin/juspay/mystique/h;)Lin/juspay/mystique/e;

    move-result-object v1

    const-string v2, "ERROR"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " excep: fn__setImage  - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " - "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lin/juspay/mystique/h$7;->c:Lin/juspay/mystique/h;

    invoke-static {v3}, Lin/juspay/mystique/h;->b(Lin/juspay/mystique/h;)Lin/juspay/mystique/j;

    move-result-object v3

    invoke-virtual {v3}, Lin/juspay/mystique/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_20
.end method
