.class public Lin/juspay/mystique/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final h:Ljava/lang/String;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Lin/juspay/mystique/j;

.field private c:Landroid/view/ViewGroup;

.field private d:Lin/juspay/mystique/c;

.field private e:Lin/juspay/mystique/e;

.field private f:Ljava/lang/String;

.field private g:Lin/juspay/mystique/d;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lin/juspay/mystique/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/juspay/mystique/h;->h:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Landroid/view/ViewGroup;Lin/juspay/mystique/d;)V
    .registers 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    iput-object p3, p0, Lin/juspay/mystique/h;->g:Lin/juspay/mystique/d;

    new-instance v0, Lin/juspay/mystique/j;

    iget-object v1, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    invoke-direct {v0, v1, p3}, Lin/juspay/mystique/j;-><init>(Landroid/app/Activity;Lin/juspay/mystique/d;)V

    iput-object v0, p0, Lin/juspay/mystique/h;->b:Lin/juspay/mystique/j;

    iput-object p2, p0, Lin/juspay/mystique/h;->c:Landroid/view/ViewGroup;

    invoke-static {}, Lin/juspay/mystique/d;->b()Lin/juspay/mystique/c;

    move-result-object v0

    iput-object v0, p0, Lin/juspay/mystique/h;->d:Lin/juspay/mystique/c;

    invoke-virtual {p3}, Lin/juspay/mystique/d;->c()Lin/juspay/mystique/e;

    move-result-object v0

    iput-object v0, p0, Lin/juspay/mystique/h;->e:Lin/juspay/mystique/e;

    return-void
.end method

.method static synthetic a(Lin/juspay/mystique/h;)Landroid/view/ViewGroup;
    .registers 2

    iget-object v0, p0, Lin/juspay/mystique/h;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .registers 1

    sget-object v0, Lin/juspay/mystique/h;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lin/juspay/mystique/h;)Lin/juspay/mystique/j;
    .registers 2

    iget-object v0, p0, Lin/juspay/mystique/h;->b:Lin/juspay/mystique/j;

    return-object v0
.end method

.method static synthetic c(Lin/juspay/mystique/h;)Lin/juspay/mystique/d;
    .registers 2

    iget-object v0, p0, Lin/juspay/mystique/h;->g:Lin/juspay/mystique/d;

    return-object v0
.end method

.method static synthetic d(Lin/juspay/mystique/h;)Lin/juspay/mystique/c;
    .registers 2

    iget-object v0, p0, Lin/juspay/mystique/h;->d:Lin/juspay/mystique/c;

    return-object v0
.end method

.method static synthetic e(Lin/juspay/mystique/h;)Lin/juspay/mystique/e;
    .registers 2

    iget-object v0, p0, Lin/juspay/mystique/h;->e:Lin/juspay/mystique/e;

    return-object v0
.end method

.method static synthetic f(Lin/juspay/mystique/h;)Landroid/app/Activity;
    .registers 2

    iget-object v0, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public Render(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    new-instance v1, Lin/juspay/mystique/h$1;

    invoke-direct {v1, p0, p1, p2}, Lin/juspay/mystique/h$1;-><init>(Lin/juspay/mystique/h;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Landroid/view/View;[Ljava/lang/String;Ljava/lang/String;)V
    .registers 10

    const/4 v5, 0x0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_40

    new-instance v1, Landroid/widget/PopupMenu;

    iget-object v0, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    invoke-direct {v1, v0, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_12
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    array-length v3, p2

    if-ge v2, v3, :cond_35

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aget-object v4, p2, v4

    invoke-interface {v2, v5, v3, v5, v4}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_12

    :cond_35
    new-instance v0, Lin/juspay/mystique/h$10;

    invoke-direct {v0, p0, p3}, Lin/juspay/mystique/h$10;-><init>(Lin/juspay/mystique/h;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    :cond_40
    return-void
.end method

.method public addViewToParent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .registers 11
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lin/juspay/mystique/h;->addViewToParent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    return-void
.end method

.method public addViewToParent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .registers 14
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v7, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    new-instance v0, Lin/juspay/mystique/h$4;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p5

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lin/juspay/mystique/h$4;-><init>(Lin/juspay/mystique/h;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;)V

    invoke-virtual {v7, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public callAPI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Lin/juspay/mystique/h$3;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lin/juspay/mystique/h$3;-><init>(Lin/juspay/mystique/h;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lin/juspay/mystique/h$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public downloadFile(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Lin/juspay/mystique/h$11;

    invoke-direct {v0, p0, p1, p2}, Lin/juspay/mystique/h$11;-><init>(Lin/juspay/mystique/h;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lin/juspay/mystique/h$11;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public fetchData(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    const-string v1, "DUI"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "null"

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public generateUIElement(Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v6, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    new-instance v0, Lin/juspay/mystique/h$9;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lin/juspay/mystique/h$9;-><init>(Lin/juspay/mystique/h;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getAssetBaseFilePath()Ljava/lang/String;
    .registers 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "/android_asset"

    return-object v0
.end method

.method public getExternalStorageBaseFilePath()Ljava/lang/String;
    .registers 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInternalStorageBaseFilePath()Ljava/lang/String;
    .registers 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    const-string v1, "juspay"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreenDimensions()Ljava/lang/String;
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iget-object v1, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "{\"width\":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ",\"height\":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .registers 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lin/juspay/mystique/h;->f:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lin/juspay/mystique/h;->f:Ljava/lang/String;

    :goto_6
    return-object v0

    :cond_7
    const-string v0, "{}"

    goto :goto_6
.end method

.method public isFilePresent(Ljava/lang/String;)Z
    .registers 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public loadFile(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    :try_start_0
    iget-object v0, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    invoke-static {v0, p1}, Lin/juspay/mystique/f;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_b

    const-string v0, ""

    :goto_a
    return-object v0

    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_10} :catch_11

    goto :goto_a

    :catch_11
    move-exception v0

    const-string v0, ""

    goto :goto_a
.end method

.method public loadFileFromExternalStorage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-static {p1, p2}, Lin/juspay/mystique/f;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_9} :catch_a

    :goto_9
    return-object v0

    :catch_a
    move-exception v0

    const-string v0, ""

    goto :goto_9
.end method

.method public run(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    :try_start_0
    iget-object v0, p0, Lin/juspay/mystique/h;->b:Lin/juspay/mystique/j;

    iget-object v1, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    const-string v2, ""

    const-string v3, ""

    invoke-virtual {v0, v1, p1, v2, v3}, Lin/juspay/mystique/j;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    if-eqz p2, :cond_2b

    iget-object v0, p0, Lin/juspay/mystique/h;->g:Lin/juspay/mystique/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window.callUICallback("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",\'success\');"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_2b} :catch_2c

    :cond_2b
    :goto_2b
    return-void

    :catch_2c
    move-exception v0

    if-eqz v0, :cond_62

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lin/juspay/mystique/h;->d:Lin/juspay/mystique/c;

    const-string v2, "runInUI"

    invoke-interface {v1, v2, v0}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lin/juspay/mystique/h;->e:Lin/juspay/mystique/e;

    const-string v2, "runInUI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " - "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lin/juspay/mystique/h;->b:Lin/juspay/mystique/j;

    invoke-virtual {v3}, Lin/juspay/mystique/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_62
    if-eqz p2, :cond_2b

    iget-object v0, p0, Lin/juspay/mystique/h;->g:Lin/juspay/mystique/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window.callUICallback("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",\'failure\');"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V

    goto :goto_2b
.end method

.method public runInUI(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    new-instance v1, Lin/juspay/mystique/h$6;

    invoke-direct {v1, p0, p1, p2}, Lin/juspay/mystique/h$6;-><init>(Lin/juspay/mystique/h;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public runInUI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v6, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    new-instance v0, Lin/juspay/mystique/h$5;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lin/juspay/mystique/h$5;-><init>(Lin/juspay/mystique/h;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public saveData(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    const-string v1, "DUI"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public saveFileToInternalStorage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    :try_start_0
    iget-object v0, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v0, p1, v1}, Lin/juspay/mystique/f;->a(Landroid/content/Context;Ljava/lang/String;[B)V

    const-string v0, "SUCCESS"
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_b} :catch_c

    :goto_b
    return-object v0

    :catch_c
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FAILURE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_b
.end method

.method public saveState(Ljava/lang/String;)V
    .registers 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iput-object p1, p0, Lin/juspay/mystique/h;->f:Ljava/lang/String;

    return-void
.end method

.method public setImage(ILjava/lang/String;)V
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    new-instance v1, Lin/juspay/mystique/h$7;

    invoke-direct {v1, p0, p1, p2}, Lin/juspay/mystique/h$7;-><init>(Lin/juspay/mystique/h;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .registers 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iput-object p1, p0, Lin/juspay/mystique/h;->f:Ljava/lang/String;

    return-void
.end method

.method public showLoading()V
    .registers 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    new-instance v1, Lin/juspay/mystique/h$2;

    invoke-direct {v1, p0}, Lin/juspay/mystique/h$2;-><init>(Lin/juspay/mystique/h;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public throwError(Ljava/lang/String;)V
    .registers 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lin/juspay/mystique/h;->d:Lin/juspay/mystique/c;

    const-string v1, "throwError"

    invoke-interface {v0, v1, p1}, Lin/juspay/mystique/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public toggleKeyboard(ILjava/lang/String;)V
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lin/juspay/mystique/h;->a:Landroid/app/Activity;

    new-instance v1, Lin/juspay/mystique/h$8;

    invoke-direct {v1, p0, p1, p2}, Lin/juspay/mystique/h$8;-><init>(Lin/juspay/mystique/h;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method
