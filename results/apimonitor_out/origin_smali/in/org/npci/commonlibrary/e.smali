.class public Lin/org/npci/commonlibrary/e;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/util/List;


# instance fields
.field private b:Lin/org/npci/commonlibrary/i;

.field private c:Lin/org/npci/commonlibrary/k;

.field private d:Lin/org/npci/commonlibrary/a/b;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lin/org/npci/commonlibrary/e;->e:Ljava/lang/String;

    invoke-static {}, Lorg/apache/xml/security/Init;->b()V

    :try_start_a
    new-instance v0, Lin/org/npci/commonlibrary/a/b;

    invoke-direct {v0}, Lin/org/npci/commonlibrary/a/b;-><init>()V

    iput-object v0, p0, Lin/org/npci/commonlibrary/e;->d:Lin/org/npci/commonlibrary/a/b;

    iget-object v0, p0, Lin/org/npci/commonlibrary/e;->d:Lin/org/npci/commonlibrary/a/b;

    invoke-virtual {v0, p1}, Lin/org/npci/commonlibrary/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "XML Validated"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v0, Lin/org/npci/commonlibrary/k;

    invoke-direct {v0, p1}, Lin/org/npci/commonlibrary/k;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lin/org/npci/commonlibrary/e;->c:Lin/org/npci/commonlibrary/k;

    iget-object v0, p0, Lin/org/npci/commonlibrary/e;->c:Lin/org/npci/commonlibrary/k;

    invoke-virtual {v0}, Lin/org/npci/commonlibrary/k;->a()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lin/org/npci/commonlibrary/e;->a:Ljava/util/List;
    :try_end_2f
    .catch Lin/org/npci/commonlibrary/f; {:try_start_a .. :try_end_2f} :catch_46
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_2f} :catch_4b

    :try_start_2f
    new-instance v0, Lin/org/npci/commonlibrary/i;

    invoke-direct {v0}, Lin/org/npci/commonlibrary/i;-><init>()V

    iput-object v0, p0, Lin/org/npci/commonlibrary/e;->b:Lin/org/npci/commonlibrary/i;
    :try_end_36
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2f .. :try_end_36} :catch_57
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_2f .. :try_end_36} :catch_63

    return-void

    :cond_37
    :try_start_37
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "XML Not Validated"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v0, Lin/org/npci/commonlibrary/f;

    sget-object v1, Lin/org/npci/commonlibrary/g;->f:Lin/org/npci/commonlibrary/g;

    invoke-direct {v0, v1}, Lin/org/npci/commonlibrary/f;-><init>(Lin/org/npci/commonlibrary/g;)V

    throw v0
    :try_end_46
    .catch Lin/org/npci/commonlibrary/f; {:try_start_37 .. :try_end_46} :catch_46
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_46} :catch_4b

    :catch_46
    move-exception v0

    invoke-virtual {v0}, Lin/org/npci/commonlibrary/f;->printStackTrace()V

    throw v0

    :catch_4b
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v0, Lin/org/npci/commonlibrary/f;

    sget-object v1, Lin/org/npci/commonlibrary/g;->g:Lin/org/npci/commonlibrary/g;

    invoke-direct {v0, v1}, Lin/org/npci/commonlibrary/f;-><init>(Lin/org/npci/commonlibrary/g;)V

    throw v0

    :catch_57
    move-exception v0

    :goto_58
    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->printStackTrace()V

    new-instance v0, Lin/org/npci/commonlibrary/f;

    sget-object v1, Lin/org/npci/commonlibrary/g;->g:Lin/org/npci/commonlibrary/g;

    invoke-direct {v0, v1}, Lin/org/npci/commonlibrary/f;-><init>(Lin/org/npci/commonlibrary/g;)V

    throw v0

    :catch_63
    move-exception v0

    goto :goto_58
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 9

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x1f4

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    :try_start_7
    iget-object v1, p0, Lin/org/npci/commonlibrary/e;->b:Lin/org/npci/commonlibrary/i;

    iget-object v2, p0, Lin/org/npci/commonlibrary/e;->b:Lin/org/npci/commonlibrary/i;

    invoke-virtual {v2, p3}, Lin/org/npci/commonlibrary/i;->a(Ljava/lang/String;)[B

    move-result-object v2

    iget-object v3, p0, Lin/org/npci/commonlibrary/e;->b:Lin/org/npci/commonlibrary/i;

    invoke-virtual {v3, p4}, Lin/org/npci/commonlibrary/i;->b(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lin/org/npci/commonlibrary/i;->a([B[B)[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lin/org/npci/commonlibrary/a;->b([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_35} :catch_3a

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_3a
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v0, Lin/org/npci/commonlibrary/f;

    sget-object v1, Lin/org/npci/commonlibrary/g;->g:Lin/org/npci/commonlibrary/g;

    invoke-direct {v0, v1}, Lin/org/npci/commonlibrary/f;-><init>(Lin/org/npci/commonlibrary/g;)V

    throw v0
.end method

.method private a(Ljava/lang/String;)[B
    .registers 7

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v0, 0x0

    :try_start_5
    iget-object v2, p0, Lin/org/npci/commonlibrary/e;->e:Ljava/lang/String;

    invoke-direct {p0, v2}, Lin/org/npci/commonlibrary/e;->b(Ljava/lang/String;)Ljava/security/PublicKey;

    move-result-object v2

    const-string v3, "RSA/ECB/PKCS1Padding"

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    invoke-virtual {v3, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_18} :catch_1a

    move-result-object v0

    :goto_19
    return-object v0

    :catch_1a
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_19
.end method

.method private b(Ljava/lang/String;)Ljava/security/PublicKey;
    .registers 4

    const-string v0, "utf-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lin/org/npci/commonlibrary/a;->a([BI)[B

    move-result-object v0

    new-instance v1, Ljava/security/spec/X509EncodedKeySpec;

    invoke-direct {v1, v0}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    const-string v0, "RSA"

    invoke-static {v0}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lin/org/npci/commonlibrary/Message;
    .registers 13

    if-eqz p1, :cond_10

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_10

    new-instance v0, Lin/org/npci/commonlibrary/f;

    sget-object v1, Lin/org/npci/commonlibrary/g;->a:Lin/org/npci/commonlibrary/g;

    invoke-direct {v0, v1}, Lin/org/npci/commonlibrary/f;-><init>(Lin/org/npci/commonlibrary/g;)V

    throw v0

    :cond_10
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lin/org/npci/commonlibrary/e;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1b
    :goto_1b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lin/org/npci/commonlibrary/j;

    invoke-virtual {v0}, Lin/org/npci/commonlibrary/j;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1b

    :cond_35
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_43

    new-instance v0, Lin/org/npci/commonlibrary/f;

    sget-object v1, Lin/org/npci/commonlibrary/g;->b:Lin/org/npci/commonlibrary/g;

    invoke-direct {v0, v1}, Lin/org/npci/commonlibrary/f;-><init>(Lin/org/npci/commonlibrary/g;)V

    throw v0

    :cond_43
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lin/org/npci/commonlibrary/j;

    invoke-virtual {v0}, Lin/org/npci/commonlibrary/j;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lin/org/npci/commonlibrary/e;->e:Ljava/lang/String;

    invoke-direct {p0, p2, p3, p4, p5}, Lin/org/npci/commonlibrary/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lin/org/npci/commonlibrary/e;->a(Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lin/org/npci/commonlibrary/a;->b([BI)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lin/org/npci/commonlibrary/Message;

    const-string v3, ""

    const-string v4, ""

    new-instance v5, Lin/org/npci/commonlibrary/Data;

    invoke-virtual {v0}, Lin/org/npci/commonlibrary/j;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lin/org/npci/commonlibrary/j;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v6, v0, v1}, Lin/org/npci/commonlibrary/Data;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v3, v4, v5}, Lin/org/npci/commonlibrary/Message;-><init>(Ljava/lang/String;Ljava/lang/String;Lin/org/npci/commonlibrary/Data;)V

    return-object v2
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8

    :try_start_0
    new-instance v0, Lin/org/npci/commonlibrary/i;

    invoke-direct {v0}, Lin/org/npci/commonlibrary/i;-><init>()V

    invoke-virtual {v0, p2}, Lin/org/npci/commonlibrary/i;->a(Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lin/org/npci/commonlibrary/a;->b([BI)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {p1, v2}, Lin/org/npci/commonlibrary/a;->a(Ljava/lang/String;I)[B

    move-result-object v2

    invoke-virtual {v0, p3}, Lin/org/npci/commonlibrary/i;->b(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lin/org/npci/commonlibrary/i;->b([B[B)[B

    move-result-object v0

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lin/org/npci/commonlibrary/a;->b([BI)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_48

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_48

    new-instance v0, Lin/org/npci/commonlibrary/f;

    sget-object v1, Lin/org/npci/commonlibrary/g;->h:Lin/org/npci/commonlibrary/g;

    invoke-direct {v0, v1}, Lin/org/npci/commonlibrary/f;-><init>(Lin/org/npci/commonlibrary/g;)V

    throw v0
    :try_end_30
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_30} :catch_30
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_30} :catch_4d
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_30} :catch_4f
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_30} :catch_51
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_30} :catch_49
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_30} :catch_53
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_30} :catch_4b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_30} :catch_3c

    :catch_30
    move-exception v0

    :goto_31
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v0, Lin/org/npci/commonlibrary/f;

    sget-object v1, Lin/org/npci/commonlibrary/g;->g:Lin/org/npci/commonlibrary/g;

    invoke-direct {v0, v1}, Lin/org/npci/commonlibrary/f;-><init>(Lin/org/npci/commonlibrary/g;)V

    throw v0

    :catch_3c
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v0, Lin/org/npci/commonlibrary/f;

    sget-object v1, Lin/org/npci/commonlibrary/g;->g:Lin/org/npci/commonlibrary/g;

    invoke-direct {v0, v1}, Lin/org/npci/commonlibrary/f;-><init>(Lin/org/npci/commonlibrary/g;)V

    throw v0

    :cond_48
    return-void

    :catch_49
    move-exception v0

    goto :goto_31

    :catch_4b
    move-exception v0

    goto :goto_31

    :catch_4d
    move-exception v0

    goto :goto_31

    :catch_4f
    move-exception v0

    goto :goto_31

    :catch_51
    move-exception v0

    goto :goto_31

    :catch_53
    move-exception v0

    goto :goto_31
.end method
