.class Lin/org/npci/commonlibrary/k;
.super Lorg/xml/sax/helpers/DefaultHandler;


# static fields
.field private static a:Ljava/util/List;

.field private static b:Lin/org/npci/commonlibrary/j;

.field private static c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    const/4 v1, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lin/org/npci/commonlibrary/k;->a:Ljava/util/List;

    sput-object v1, Lin/org/npci/commonlibrary/k;->b:Lin/org/npci/commonlibrary/j;

    sput-object v1, Lin/org/npci/commonlibrary/k;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 5

    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v0

    :try_start_7
    invoke-virtual {v0}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v0

    new-instance v1, Lorg/xml/sax/InputSource;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0, v1, p0}, Ljavax/xml/parsers/SAXParser;->parse(Lorg/xml/sax/InputSource;Lorg/xml/sax/helpers/DefaultHandler;)V
    :try_end_18
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_7 .. :try_end_18} :catch_19
    .catch Lorg/xml/sax/SAXException; {:try_start_7 .. :try_end_18} :catch_24
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_18} :catch_22

    return-void

    :catch_19
    move-exception v0

    :goto_1a
    new-instance v0, Lin/org/npci/commonlibrary/f;

    sget-object v1, Lin/org/npci/commonlibrary/g;->d:Lin/org/npci/commonlibrary/g;

    invoke-direct {v0, v1}, Lin/org/npci/commonlibrary/f;-><init>(Lin/org/npci/commonlibrary/g;)V

    throw v0

    :catch_22
    move-exception v0

    goto :goto_1a

    :catch_24
    move-exception v0

    goto :goto_1a
.end method


# virtual methods
.method public a()Ljava/util/List;
    .registers 2

    sget-object v0, Lin/org/npci/commonlibrary/k;->a:Ljava/util/List;

    return-object v0
.end method

.method public characters([CII)V
    .registers 5

    invoke-static {p1, p2, p3}, Ljava/lang/String;->copyValueOf([CII)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lin/org/npci/commonlibrary/k;->c:Ljava/lang/String;

    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    const/4 v0, -0x1

    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_30

    :cond_8
    :goto_8
    packed-switch v0, :pswitch_data_3a

    :goto_b
    return-void

    :sswitch_c
    const-string v1, "key"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v0, 0x0

    goto :goto_8

    :sswitch_16
    const-string v1, "keyValue"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v0, 0x1

    goto :goto_8

    :pswitch_20
    sget-object v0, Lin/org/npci/commonlibrary/k;->a:Ljava/util/List;

    sget-object v1, Lin/org/npci/commonlibrary/k;->b:Lin/org/npci/commonlibrary/j;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :pswitch_28
    sget-object v0, Lin/org/npci/commonlibrary/k;->b:Lin/org/npci/commonlibrary/j;

    sget-object v1, Lin/org/npci/commonlibrary/k;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lin/org/npci/commonlibrary/j;->c(Ljava/lang/String;)V

    goto :goto_b

    :sswitch_data_30
    .sparse-switch
        0x19e5f -> :sswitch_c
        0x1d572652 -> :sswitch_16
    .end sparse-switch

    :pswitch_data_3a
    .packed-switch 0x0
        :pswitch_20
        :pswitch_28
    .end packed-switch
.end method

.method protected finalize()V
    .registers 3

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "KeyParser Destroyed"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .registers 7

    const/4 v0, -0x1

    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v1

    packed-switch v1, :pswitch_data_34

    :cond_8
    :goto_8
    packed-switch v0, :pswitch_data_3a

    :goto_b
    return-void

    :pswitch_c
    const-string v1, "key"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v0, 0x0

    goto :goto_8

    :pswitch_16
    new-instance v0, Lin/org/npci/commonlibrary/j;

    invoke-direct {v0}, Lin/org/npci/commonlibrary/j;-><init>()V

    sput-object v0, Lin/org/npci/commonlibrary/k;->b:Lin/org/npci/commonlibrary/j;

    sget-object v0, Lin/org/npci/commonlibrary/k;->b:Lin/org/npci/commonlibrary/j;

    const-string v1, "ki"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/org/npci/commonlibrary/j;->a(Ljava/lang/String;)V

    sget-object v0, Lin/org/npci/commonlibrary/k;->b:Lin/org/npci/commonlibrary/j;

    const-string v1, "owner"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/org/npci/commonlibrary/j;->b(Ljava/lang/String;)V

    goto :goto_b

    :pswitch_data_34
    .packed-switch 0x19e5f
        :pswitch_c
    .end packed-switch

    :pswitch_data_3a
    .packed-switch 0x0
        :pswitch_16
    .end packed-switch
.end method
