.class public Lin/org/npci/commonlibrary/a/b;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/security/cert/Certificate;


# direct methods
.method public constructor <init>()V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_3
    const-string v0, "signer.crt"

    invoke-direct {p0, v0}, Lin/org/npci/commonlibrary/a/b;->b(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v0

    iput-object v0, p0, Lin/org/npci/commonlibrary/a/b;->a:Ljava/security/cert/Certificate;
    :try_end_b
    .catch Ljava/security/cert/CertificateException; {:try_start_3 .. :try_end_b} :catch_c

    :goto_b
    return-void

    :catch_c
    move-exception v0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Error in loading exception"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto :goto_b
.end method

.method private b(Ljava/lang/String;)Ljava/security/cert/Certificate;
    .registers 5

    const-string v0, "X.509"

    invoke-static {v0}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    :try_start_17
    invoke-virtual {v0, v2}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
    :try_end_1a
    .catchall {:try_start_17 .. :try_end_1a} :catchall_22

    move-result-object v0

    :try_start_1b
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1e} :catch_2a

    :goto_1e
    :try_start_1e
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_21} :catch_2c

    :goto_21
    return-object v0

    :catchall_22
    move-exception v0

    :try_start_23
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_26} :catch_2e

    :goto_26
    :try_start_26
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_29
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_29} :catch_30

    :goto_29
    throw v0

    :catch_2a
    move-exception v2

    goto :goto_1e

    :catch_2c
    move-exception v1

    goto :goto_21

    :catch_2e
    move-exception v2

    goto :goto_26

    :catch_30
    move-exception v1

    goto :goto_29
.end method


# virtual methods
.method public a(Ljava/lang/String;)Z
    .registers 7

    const/4 v1, 0x0

    iget-object v0, p0, Lin/org/npci/commonlibrary/a/b;->a:Ljava/security/cert/Certificate;

    if-nez v0, :cond_d

    :try_start_5
    const-string v0, "signer.crt"

    invoke-direct {p0, v0}, Lin/org/npci/commonlibrary/a/b;->b(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v0

    iput-object v0, p0, Lin/org/npci/commonlibrary/a/b;->a:Ljava/security/cert/Certificate;
    :try_end_d
    .catch Ljava/security/cert/CertificateException; {:try_start_5 .. :try_end_d} :catch_1d

    :cond_d
    :try_start_d
    invoke-static {p1}, Lin/org/npci/commonlibrary/a/a;->a(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v0

    iget-object v2, p0, Lin/org/npci/commonlibrary/a/b;->a:Ljava/security/cert/Certificate;

    invoke-virtual {v2}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v2

    invoke-static {v0, v2}, Lin/org/npci/commonlibrary/a/a;->a(Lorg/w3c/dom/Document;Ljava/security/PublicKey;)Z
    :try_end_1a
    .catch Lorg/xml/sax/SAXException; {:try_start_d .. :try_end_1a} :catch_4d
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_d .. :try_end_1a} :catch_29
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_1a} :catch_47

    move-result v0

    :goto_1b
    move v1, v0

    :goto_1c
    return v1

    :catch_1d
    move-exception v0

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Error in loading certificate."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto :goto_1c

    :catch_29
    move-exception v0

    :goto_2a
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Parsing failed for message:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    goto :goto_1b

    :catch_47
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    goto :goto_1b

    :catch_4d
    move-exception v0

    goto :goto_2a
.end method
