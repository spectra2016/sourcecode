.class Lin/org/npci/upiapp/core/QRScannerInterface$2;
.super Ljava/lang/Object;
.source "QRScannerInterface.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lin/org/npci/upiapp/core/QRScannerInterface;->captureManager(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lin/org/npci/upiapp/core/QRScannerInterface;


# direct methods
.method constructor <init>(Lin/org/npci/upiapp/core/QRScannerInterface;Ljava/lang/String;)V
    .registers 3

    iput-object p1, p0, Lin/org/npci/upiapp/core/QRScannerInterface$2;->b:Lin/org/npci/upiapp/core/QRScannerInterface;

    iput-object p2, p0, Lin/org/npci/upiapp/core/QRScannerInterface$2;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface$2;->b:Lin/org/npci/upiapp/core/QRScannerInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/QRScannerInterface;->a(Lin/org/npci/upiapp/core/QRScannerInterface;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;

    move-result-object v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface$2;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1d

    iget-object v1, p0, Lin/org/npci/upiapp/core/QRScannerInterface$2;->a:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_5a

    :cond_1a
    :goto_1a
    packed-switch v0, :pswitch_data_68

    :cond_1d
    :goto_1d
    return-void

    :sswitch_1e
    const-string v2, "onResume"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    const/4 v0, 0x0

    goto :goto_1a

    :sswitch_28
    const-string v2, "onPause"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    const/4 v0, 0x1

    goto :goto_1a

    :sswitch_32
    const-string v2, "onDestroy"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    const/4 v0, 0x2

    goto :goto_1a

    :pswitch_3c
    iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface$2;->b:Lin/org/npci/upiapp/core/QRScannerInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/QRScannerInterface;->a(Lin/org/npci/upiapp/core/QRScannerInterface;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;

    move-result-object v0

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->b()V

    goto :goto_1d

    :pswitch_46
    iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface$2;->b:Lin/org/npci/upiapp/core/QRScannerInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/QRScannerInterface;->a(Lin/org/npci/upiapp/core/QRScannerInterface;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;

    move-result-object v0

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->c()V

    goto :goto_1d

    :pswitch_50
    iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface$2;->b:Lin/org/npci/upiapp/core/QRScannerInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/QRScannerInterface;->a(Lin/org/npci/upiapp/core/QRScannerInterface;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;

    move-result-object v0

    invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->d()V

    goto :goto_1d

    :sswitch_data_5a
    .sparse-switch
        -0x53865ee5 -> :sswitch_32
        -0x4fe204a9 -> :sswitch_28
        0x57429eec -> :sswitch_1e
    .end sparse-switch

    :pswitch_data_68
    .packed-switch 0x0
        :pswitch_3c
        :pswitch_46
        :pswitch_50
    .end packed-switch
.end method
