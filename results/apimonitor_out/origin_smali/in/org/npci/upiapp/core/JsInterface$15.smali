.class Lin/org/npci/upiapp/core/JsInterface$15;
.super Ljava/lang/Object;
.source "JsInterface.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lin/org/npci/upiapp/core/JsInterface;->viewToImage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:[Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lin/org/npci/upiapp/core/JsInterface;


# direct methods
.method constructor <init>(Lin/org/npci/upiapp/core/JsInterface;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    iput-object p1, p0, Lin/org/npci/upiapp/core/JsInterface$15;->e:Lin/org/npci/upiapp/core/JsInterface;

    iput-object p2, p0, Lin/org/npci/upiapp/core/JsInterface$15;->a:Ljava/lang/String;

    iput-object p3, p0, Lin/org/npci/upiapp/core/JsInterface$15;->b:[Ljava/lang/String;

    iput-object p4, p0, Lin/org/npci/upiapp/core/JsInterface$15;->c:Ljava/lang/String;

    iput-object p5, p0, Lin/org/npci/upiapp/core/JsInterface$15;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    :try_start_0
    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$15;->e:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->b(Lin/org/npci/upiapp/core/JsInterface;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$15;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setDrawingCacheEnabled(Z)V

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->buildDrawingCache()V

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$15;->b:[Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$15;->e:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v3}, Lin/org/npci/upiapp/core/JsInterface;->b(Lin/org/npci/upiapp/core/JsInterface;)Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lin/org/npci/upiapp/core/JsInterface$15;->c:Ljava/lang/String;

    const-string v5, ""

    invoke-static {v3, v0, v4, v5}, Landroid/provider/MediaStore$Images$Media;->insertImage(Landroid/content/ContentResolver;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    const-string v0, "window.callJSCallback(\'%s\',\'%s\');"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$15;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lin/org/npci/upiapp/core/JsInterface$15;->b:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$15;->e:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v1}, Lin/org/npci/upiapp/core/JsInterface;->c(Lin/org/npci/upiapp/core/JsInterface;)Lin/juspay/mystique/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
    :try_end_66
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_66} :catch_67

    :goto_66
    return-void

    :catch_67
    move-exception v0

    invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception"

    invoke-static {v1, v2, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_66
.end method
