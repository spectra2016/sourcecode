.class Lin/org/npci/upiapp/core/NPCIJSInterface$5;
.super Landroid/os/ResultReceiver;
.source "NPCIJSInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lin/org/npci/upiapp/core/NPCIJSInterface;->getCredentials(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lin/org/npci/upiapp/core/NPCIJSInterface;


# direct methods
.method constructor <init>(Lin/org/npci/upiapp/core/NPCIJSInterface;Landroid/os/Handler;Ljava/lang/String;)V
    .registers 4

    iput-object p1, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$5;->b:Lin/org/npci/upiapp/core/NPCIJSInterface;

    iput-object p3, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$5;->a:Ljava/lang/String;

    invoke-direct {p0, p2}, Landroid/os/ResultReceiver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method protected onReceiveResult(ILandroid/os/Bundle;)V
    .registers 8

    const-string v0, "NPCIJSInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ResultCode is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :try_start_1d
    invoke-super {p0, p1, p2}, Landroid/os/ResultReceiver;->onReceiveResult(ILandroid/os/Bundle;)V

    if-eqz p2, :cond_4d

    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_80

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_36
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_36} :catch_49

    :try_start_36
    iget-object v3, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$5;->b:Lin/org/npci/upiapp/core/NPCIJSInterface;

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v4}, Lin/org/npci/upiapp/core/NPCIJSInterface;->a(Lin/org/npci/upiapp/core/NPCIJSInterface;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_43
    .catch Lorg/json/JSONException; {:try_start_36 .. :try_end_43} :catch_44
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_43} :catch_49

    goto :goto_2a

    :catch_44
    move-exception v0

    :try_start_45
    invoke-static {v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V
    :try_end_48
    .catch Ljava/lang/Exception; {:try_start_45 .. :try_end_48} :catch_49

    goto :goto_2a

    :catch_49
    move-exception v0

    invoke-static {v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V

    :cond_4d
    :goto_4d
    iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$5;->b:Lin/org/npci/upiapp/core/NPCIJSInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/NPCIJSInterface;->a(Lin/org/npci/upiapp/core/NPCIJSInterface;)Lin/juspay/mystique/d;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "window.callUICallback(\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$5;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V

    return-void

    :cond_80
    :try_start_80
    const-string v0, "resultCode"

    invoke-virtual {v1, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_85
    .catch Ljava/lang/Exception; {:try_start_80 .. :try_end_85} :catch_86

    goto :goto_4d

    :catch_86
    move-exception v0

    :try_start_87
    invoke-static {v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V
    :try_end_8a
    .catch Ljava/lang/Exception; {:try_start_87 .. :try_end_8a} :catch_49

    goto :goto_4d
.end method
