.class Lin/org/npci/upiapp/core/JsInterface$8;
.super Landroid/os/AsyncTask;
.source "JsInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lin/org/npci/upiapp/core/JsInterface;->downloadFile(Ljava/lang/String;ZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Z

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lin/org/npci/upiapp/core/JsInterface;


# direct methods
.method constructor <init>(Lin/org/npci/upiapp/core/JsInterface;Ljava/lang/String;ZLjava/lang/String;)V
    .registers 5

    iput-object p1, p0, Lin/org/npci/upiapp/core/JsInterface$8;->d:Lin/org/npci/upiapp/core/JsInterface;

    iput-object p2, p0, Lin/org/npci/upiapp/core/JsInterface$8;->a:Ljava/lang/String;

    iput-boolean p3, p0, Lin/org/npci/upiapp/core/JsInterface$8;->b:Z

    iput-object p4, p0, Lin/org/npci/upiapp/core/JsInterface$8;->c:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    :try_start_4
    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$8;->d:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->b(Lin/org/npci/upiapp/core/JsInterface;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$8;->a:Ljava/lang/String;

    iget-boolean v2, p0, Lin/org/npci/upiapp/core/JsInterface$8;->b:Z

    invoke-static {v0, v1, v2}, Lin/org/npci/upiapp/utils/d;->a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/Boolean;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_15} :catch_3f

    move-result-object v0

    invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$8;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3e
    return-object v0

    :catch_3f
    move-exception v0

    invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "FAILURE"

    goto :goto_3e
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .registers 5

    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$8;->c:Ljava/lang/String;

    if-eqz v0, :cond_40

    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$8;->d:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->c(Lin/org/npci/upiapp/core/JsInterface;)Lin/juspay/mystique/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$8;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\", \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$8;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V

    :cond_40
    return-void
.end method
