.class Lin/org/npci/upiapp/core/JsInterface$4;
.super Ljava/lang/Object;
.source "JsInterface.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lin/org/npci/upiapp/core/JsInterface;->calender(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Calendar;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lin/org/npci/upiapp/core/JsInterface;


# direct methods
.method constructor <init>(Lin/org/npci/upiapp/core/JsInterface;Ljava/util/Calendar;Ljava/lang/String;)V
    .registers 4

    iput-object p1, p0, Lin/org/npci/upiapp/core/JsInterface$4;->c:Lin/org/npci/upiapp/core/JsInterface;

    iput-object p2, p0, Lin/org/npci/upiapp/core/JsInterface$4;->a:Ljava/util/Calendar;

    iput-object p3, p0, Lin/org/npci/upiapp/core/JsInterface$4;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateSet(Landroid/widget/DatePicker;III)V
    .registers 10

    const/4 v2, 0x2

    const/4 v4, 0x1

    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$4;->a:Ljava/util/Calendar;

    invoke-virtual {v0, v4, p2}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$4;->a:Ljava/util/Calendar;

    invoke-virtual {v0, v2, p3}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$4;->a:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p4}, Ljava/util/Calendar;->set(II)V

    const-string v0, "window.callJSCallback(\'%s\',\'%s\');"

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$4;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$4;->c:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v1}, Lin/org/npci/upiapp/core/JsInterface;->c(Lin/org/npci/upiapp/core/JsInterface;)Lin/juspay/mystique/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V

    return-void
.end method
