.class public Lin/org/npci/upiapp/core/b;
.super Ljava/lang/Object;
.source "Tracker.java"


# static fields
.field private static a:Lin/org/npci/upiapp/core/b;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Landroid/content/Context;

.field private d:Landroid/app/Activity;

.field private e:I

.field private f:Lin/juspay/tracker/i;

.field private g:Lin/juspay/tracker/j;

.field private h:Z

.field private final i:J


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .registers 4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lin/org/npci/upiapp/core/b;->e:I

    invoke-static {}, Lin/juspay/tracker/i;->a()Lin/juspay/tracker/i;

    move-result-object v0

    iput-object v0, p0, Lin/org/npci/upiapp/core/b;->f:Lin/juspay/tracker/i;

    invoke-static {}, Lin/juspay/tracker/j;->a()Lin/juspay/tracker/j;

    move-result-object v0

    iput-object v0, p0, Lin/org/npci/upiapp/core/b;->g:Lin/juspay/tracker/j;

    iput-boolean v1, p0, Lin/org/npci/upiapp/core/b;->h:Z

    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lin/org/npci/upiapp/core/b;->i:J

    iput-object p1, p0, Lin/org/npci/upiapp/core/b;->d:Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lin/org/npci/upiapp/core/b;->c:Landroid/content/Context;

    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070055

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lin/org/npci/upiapp/core/b;->b:Ljava/lang/String;

    sput-object p0, Lin/org/npci/upiapp/core/b;->a:Lin/org/npci/upiapp/core/b;

    invoke-direct {p0}, Lin/org/npci/upiapp/core/b;->a()V

    return-void
.end method

.method private a()V
    .registers 5

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lin/org/npci/upiapp/core/b$1;

    invoke-direct {v1, p0}, Lin/org/npci/upiapp/core/b$1;-><init>(Lin/org/npci/upiapp/core/b;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic a(Lin/org/npci/upiapp/core/b;)Z
    .registers 2

    iget-boolean v0, p0, Lin/org/npci/upiapp/core/b;->h:Z

    return v0
.end method

.method private b()V
    .registers 4

    sget-object v0, Lin/juspay/tracker/e$a;->b:Lin/juspay/tracker/e$a;

    invoke-static {v0}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/e$a;)V

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v0

    iget-object v1, p0, Lin/org/npci/upiapp/core/b;->d:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lin/juspay/tracker/e;->a(Landroid/content/Context;)V

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v0

    iget-object v1, p0, Lin/org/npci/upiapp/core/b;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/tracker/e;->a(Ljava/lang/String;)V

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v0

    const-string v1, "NPCI"

    invoke-virtual {v0, v1}, Lin/juspay/tracker/e;->b(Ljava/lang/String;)V

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v0

    invoke-virtual {v0}, Lin/juspay/tracker/e;->d()V

    return-void
.end method

.method private c()V
    .registers 4

    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->g:Lin/juspay/tracker/j;

    iget-object v1, p0, Lin/org/npci/upiapp/core/b;->d:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/tracker/j;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->g:Lin/juspay/tracker/j;

    iget-object v1, p0, Lin/org/npci/upiapp/core/b;->d:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/tracker/j;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->g:Lin/juspay/tracker/j;

    iget-object v1, p0, Lin/org/npci/upiapp/core/b;->d:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/tracker/j;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->g:Lin/juspay/tracker/j;

    iget-object v1, p0, Lin/org/npci/upiapp/core/b;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/tracker/j;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->f:Lin/juspay/tracker/i;

    iget v1, p0, Lin/org/npci/upiapp/core/b;->e:I

    invoke-virtual {v0, v1}, Lin/juspay/tracker/i;->a(I)V

    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->f:Lin/juspay/tracker/i;

    iget-object v1, p0, Lin/org/npci/upiapp/core/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lin/juspay/tracker/i;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public initLoggingService()V
    .registers 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-boolean v0, p0, Lin/org/npci/upiapp/core/b;->h:Z

    if-nez v0, :cond_d

    const/4 v0, 0x1

    iput-boolean v0, p0, Lin/org/npci/upiapp/core/b;->h:Z

    invoke-direct {p0}, Lin/org/npci/upiapp/core/b;->c()V

    invoke-direct {p0}, Lin/org/npci/upiapp/core/b;->b()V

    :cond_d
    return-void
.end method

.method public setLogLevel(Ljava/lang/String;)V
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lin/org/npci/upiapp/core/b;->e:I

    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->f:Lin/juspay/tracker/i;

    iget v1, p0, Lin/org/npci/upiapp/core/b;->e:I

    invoke-virtual {v0, v1}, Lin/juspay/tracker/i;->a(I)V
    :try_end_d
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_d} :catch_e

    :goto_d
    return-void

    :catch_e
    move-exception v0

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v1

    new-instance v2, Lin/juspay/tracker/c;

    invoke-direct {v2}, Lin/juspay/tracker/c;-><init>()V

    invoke-virtual {v2, v0}, Lin/juspay/tracker/c;->a(Ljava/lang/Throwable;)Lin/juspay/tracker/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/c;)V

    goto :goto_d
.end method

.method public setLogPushConfig(Ljava/lang/String;)V
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    :try_start_0
    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->f:Lin/juspay/tracker/i;

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lin/juspay/tracker/i;->a(Lorg/json/JSONObject;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_a} :catch_b

    :goto_a
    return-void

    :catch_b
    move-exception v0

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v1

    new-instance v2, Lin/juspay/tracker/c;

    invoke-direct {v2}, Lin/juspay/tracker/c;-><init>()V

    invoke-virtual {v2, v0}, Lin/juspay/tracker/c;->a(Ljava/lang/Throwable;)Lin/juspay/tracker/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/c;)V

    goto :goto_a
.end method

.method public setLoggingRules(Ljava/lang/String;)V
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    :try_start_0
    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->f:Lin/juspay/tracker/i;

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lin/juspay/tracker/i;->b(Lorg/json/JSONObject;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_a} :catch_b

    :goto_a
    return-void

    :catch_b
    move-exception v0

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v1

    new-instance v2, Lin/juspay/tracker/c;

    invoke-direct {v2}, Lin/juspay/tracker/c;-><init>()V

    invoke-virtual {v2, v0}, Lin/juspay/tracker/c;->a(Ljava/lang/Throwable;)Lin/juspay/tracker/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/c;)V

    goto :goto_a
.end method

.method public setRemoteVersion(Ljava/lang/String;)V
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->g:Lin/juspay/tracker/j;

    iget-object v1, p0, Lin/org/npci/upiapp/core/b;->d:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/tracker/j;->b(Ljava/lang/String;)V

    return-void
.end method

.method public setWhiteListedArray(Ljava/lang/String;)V
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    :try_start_0
    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->f:Lin/juspay/tracker/i;

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lin/juspay/tracker/i;->a(Lorg/json/JSONArray;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_a} :catch_b

    :goto_a
    return-void

    :catch_b
    move-exception v0

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v1

    new-instance v2, Lin/juspay/tracker/c;

    invoke-direct {v2}, Lin/juspay/tracker/c;-><init>()V

    invoke-virtual {v2, v0}, Lin/juspay/tracker/c;->a(Ljava/lang/Throwable;)Lin/juspay/tracker/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/c;)V

    goto :goto_a
.end method

.method public setblackListedArray(Ljava/lang/String;)V
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    :try_start_0
    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->f:Lin/juspay/tracker/i;

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lin/juspay/tracker/i;->b(Lorg/json/JSONArray;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_a} :catch_b

    :goto_a
    return-void

    :catch_b
    move-exception v0

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v1

    new-instance v2, Lin/juspay/tracker/c;

    invoke-direct {v2}, Lin/juspay/tracker/c;-><init>()V

    invoke-virtual {v2, v0}, Lin/juspay/tracker/c;->a(Ljava/lang/Throwable;)Lin/juspay/tracker/c;

    move-result-object v0

    invoke-virtual {v1, v0}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/c;)V

    goto :goto_a
.end method

.method public trackError(Ljava/lang/String;)V
    .registers 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1, p1}, Lin/juspay/tracker/e;->a(Ljava/util/Date;Ljava/lang/String;)V

    return-void
.end method

.method public trackEvent(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Lin/juspay/tracker/b;

    invoke-direct {v0}, Lin/juspay/tracker/b;-><init>()V

    invoke-virtual {v0, p1}, Lin/juspay/tracker/b;->b(Ljava/lang/String;)Lin/juspay/tracker/b;

    invoke-virtual {v0, p2}, Lin/juspay/tracker/b;->c(Ljava/lang/String;)Lin/juspay/tracker/b;

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/b;)V

    return-void
.end method

.method public trackInfo(Ljava/lang/String;)V
    .registers 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lin/juspay/tracker/e;->c(Ljava/lang/String;)V

    return-void
.end method

.method public trackSession()V
    .registers 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v0

    invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;

    move-result-object v1

    invoke-virtual {v1}, Lin/juspay/tracker/e;->f()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/tracker/e;->a(Ljava/util/Map;)V

    return-void
.end method

.method public updateLoggingEndPoint(Ljava/lang/String;)V
    .registers 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iput-object p1, p0, Lin/org/npci/upiapp/core/b;->b:Ljava/lang/String;

    iget-object v0, p0, Lin/org/npci/upiapp/core/b;->f:Lin/juspay/tracker/i;

    iget-object v1, p0, Lin/org/npci/upiapp/core/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lin/juspay/tracker/i;->a(Ljava/lang/String;)V

    return-void
.end method
