.class Lin/org/npci/upiapp/core/JsInterface$14;
.super Ljava/lang/Object;
.source "JsInterface.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lin/org/npci/upiapp/core/JsInterface;->shareQrCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Z

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Lin/org/npci/upiapp/core/JsInterface;


# direct methods
.method constructor <init>(Lin/org/npci/upiapp/core/JsInterface;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7

    iput-object p1, p0, Lin/org/npci/upiapp/core/JsInterface$14;->f:Lin/org/npci/upiapp/core/JsInterface;

    iput-object p2, p0, Lin/org/npci/upiapp/core/JsInterface$14;->a:Ljava/lang/String;

    iput-boolean p3, p0, Lin/org/npci/upiapp/core/JsInterface$14;->b:Z

    iput-object p4, p0, Lin/org/npci/upiapp/core/JsInterface$14;->c:Ljava/lang/String;

    iput-object p5, p0, Lin/org/npci/upiapp/core/JsInterface$14;->d:Ljava/lang/String;

    iput-object p6, p0, Lin/org/npci/upiapp/core/JsInterface$14;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$14;->f:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->b(Lin/org/npci/upiapp/core/JsInterface;)Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lin/org/npci/upiapp/HomeActivity;

    if-eqz v0, :cond_72

    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$14;->f:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->b(Lin/org/npci/upiapp/core/JsInterface;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lin/org/npci/upiapp/HomeActivity;

    iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$14;->f:Lin/org/npci/upiapp/core/JsInterface;

    iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$14;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lin/org/npci/upiapp/core/JsInterface;->getExternalFilePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iget-boolean v2, p0, Lin/org/npci/upiapp/core/JsInterface$14;->b:Z

    if-nez v2, :cond_24

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_73

    :cond_24
    iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$14;->a:Ljava/lang/String;

    iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$14;->c:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lin/org/npci/upiapp/HomeActivity;->a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$14;->f:Lin/org/npci/upiapp/core/JsInterface;

    iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$14;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lin/org/npci/upiapp/core/JsInterface;->getInternalFilePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$14;->f:Lin/org/npci/upiapp/core/JsInterface;

    iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$14;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lin/org/npci/upiapp/core/JsInterface;->saveToExternalStorage(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    :goto_40
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "image/png"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "android.intent.extra.SUBJECT"

    iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$14;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "android.intent.extra.TEXT"

    iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$14;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$14;->f:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->b(Lin/org/npci/upiapp/core/JsInterface;)Landroid/app/Activity;

    move-result-object v0

    const-string v2, "Share via"

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_72
    return-void

    :cond_73
    move-object v0, v1

    goto :goto_40
.end method
