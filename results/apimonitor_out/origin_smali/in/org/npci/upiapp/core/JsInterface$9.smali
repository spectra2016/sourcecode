.class Lin/org/npci/upiapp/core/JsInterface$9;
.super Landroid/content/BroadcastReceiver;
.source "JsInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lin/org/npci/upiapp/core/JsInterface;->sendSMS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lin/org/npci/upiapp/core/JsInterface;


# direct methods
.method constructor <init>(Lin/org/npci/upiapp/core/JsInterface;Ljava/lang/String;)V
    .registers 3

    iput-object p1, p0, Lin/org/npci/upiapp/core/JsInterface$9;->b:Lin/org/npci/upiapp/core/JsInterface;

    iput-object p2, p0, Lin/org/npci/upiapp/core/JsInterface$9;->a:Ljava/lang/String;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 6

    invoke-virtual {p0}, Lin/org/npci/upiapp/core/JsInterface$9;->getResultCode()I

    move-result v0

    packed-switch v0, :pswitch_data_e8

    :pswitch_7
    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$9;->b:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->c(Lin/org/npci/upiapp/core/JsInterface;)Lin/juspay/mystique/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window.callUICallback(\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$9;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\", \"Sms Sending Failed\", \"837\")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V

    :goto_2b
    return-void

    :pswitch_2c
    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$9;->b:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->c(Lin/org/npci/upiapp/core/JsInterface;)Lin/juspay/mystique/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window.callUICallback(\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$9;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\", \"SUCCESS\")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V

    goto :goto_2b

    :pswitch_51
    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$9;->b:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->c(Lin/org/npci/upiapp/core/JsInterface;)Lin/juspay/mystique/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window.callUICallback(\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$9;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\", \"Sms Sending Failed\", \"837\")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V

    goto :goto_2b

    :pswitch_76
    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$9;->b:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->c(Lin/org/npci/upiapp/core/JsInterface;)Lin/juspay/mystique/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window.callUICallback(\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$9;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\", \"Sms Sending Failed. No service\", \"838\")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V

    goto :goto_2b

    :pswitch_9b
    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$9;->b:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->c(Lin/org/npci/upiapp/core/JsInterface;)Lin/juspay/mystique/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window.callUICallback(\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$9;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\", \"Sms Sending Failed\", \"839\")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V

    goto/16 :goto_2b

    :pswitch_c1
    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$9;->b:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->c(Lin/org/npci/upiapp/core/JsInterface;)Lin/juspay/mystique/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window.callUICallback(\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$9;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\", \"Sms Sending Failed. Airplane Mode ON\", \"840\")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V

    goto/16 :goto_2b

    nop

    :pswitch_data_e8
    .packed-switch -0x1
        :pswitch_2c
        :pswitch_7
        :pswitch_51
        :pswitch_c1
        :pswitch_9b
        :pswitch_76
    .end packed-switch
.end method
