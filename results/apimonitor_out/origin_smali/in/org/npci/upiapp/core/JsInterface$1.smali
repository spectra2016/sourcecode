.class Lin/org/npci/upiapp/core/JsInterface$1;
.super Ljava/lang/Object;
.source "JsInterface.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lin/org/npci/upiapp/core/JsInterface;->getGCMToken()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lin/org/npci/upiapp/core/JsInterface;


# direct methods
.method constructor <init>(Lin/org/npci/upiapp/core/JsInterface;)V
    .registers 2

    iput-object p1, p0, Lin/org/npci/upiapp/core/JsInterface$1;->a:Lin/org/npci/upiapp/core/JsInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    :try_start_0
    iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$1;->a:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->a(Lin/org/npci/upiapp/core/JsInterface;)Z

    move-result v0

    if-eqz v0, :cond_1e

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$1;->a:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v1}, Lin/org/npci/upiapp/core/JsInterface;->b(Lin/org/npci/upiapp/core/JsInterface;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lin/org/npci/upiapp/gcm/RegistrationIntentService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$1;->a:Lin/org/npci/upiapp/core/JsInterface;

    invoke-static {v1}, Lin/org/npci/upiapp/core/JsInterface;->b(Lin/org/npci/upiapp/core/JsInterface;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1e} :catch_1f

    :cond_1e
    :goto_1e
    return-void

    :catch_1f
    move-exception v0

    invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception while calling GCM Token"

    invoke-static {v1, v2, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1e
.end method
