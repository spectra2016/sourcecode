.class public Lin/org/npci/upiapp/a/a;
.super Ljava/lang/Object;
.source "Logger.java"


# direct methods
.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 4

    sget-boolean v0, Lin/org/npci/upiapp/UpiApp;->a:Z

    if-eqz v0, :cond_8

    const/4 v0, 0x6

    invoke-static {v0, p0, p1}, Lcom/crashlytics/android/Crashlytics;->log(ILjava/lang/String;Ljava/lang/String;)V

    :cond_8
    invoke-static {p2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 4

    sget-boolean v0, Lin/org/npci/upiapp/UpiApp;->a:Z

    if-eqz v0, :cond_9

    const/4 v0, 0x3

    invoke-static {v0, p0, p1}, Lcom/crashlytics/android/Crashlytics;->log(ILjava/lang/String;Ljava/lang/String;)V

    :cond_8
    :goto_8
    return-void

    :cond_9
    if-eqz p2, :cond_8

    invoke-static {p1}, Lcom/crashlytics/android/Crashlytics;->log(Ljava/lang/String;)V

    goto :goto_8
.end method

.method public static a(Ljava/lang/Throwable;)V
    .registers 3

    sget-boolean v0, Lin/org/npci/upiapp/UpiApp;->a:Z

    if-eqz v0, :cond_f

    const-string v0, "Exception"

    const-string v1, "Exception"

    invoke-static {v0, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_e
    return-void

    :cond_f
    invoke-static {p0}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V

    goto :goto_e
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lin/org/npci/upiapp/a/a;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 4

    sget-boolean v0, Lin/org/npci/upiapp/UpiApp;->a:Z

    if-eqz v0, :cond_9

    const/4 v0, 0x6

    invoke-static {v0, p0, p1}, Lcom/crashlytics/android/Crashlytics;->log(ILjava/lang/String;Ljava/lang/String;)V

    :cond_8
    :goto_8
    return-void

    :cond_9
    if-eqz p2, :cond_8

    invoke-static {p1}, Lcom/crashlytics/android/Crashlytics;->log(Ljava/lang/String;)V

    goto :goto_8
.end method
