.class public Lin/org/npci/upiapp/utils/d;
.super Ljava/lang/Object;
.source "RemoteAssetService.java"


# direct methods
.method public static a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/Boolean;
    .registers 7

    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lin/org/npci/upiapp/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p2}, Lin/org/npci/upiapp/utils/d;->b(Landroid/content/Context;Ljava/lang/String;Z)[B

    move-result-object v1

    if-eqz v1, :cond_41

    if-eqz p2, :cond_20

    const-string v2, ".zip"

    const-string v3, ".jsa"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    :cond_20
    invoke-static {p0, v0, v1}, Lin/org/npci/upiapp/utils/b;->a(Landroid/content/Context;Ljava/lang/String;[B)V

    const-string v1, "RemoteAssetService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " downloaded successfully"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_40
    return-object v0

    :cond_41
    const-string v1, "RemoteAssetService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " null"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lin/org/npci/upiapp/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_40
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    const/4 v2, 0x0

    const-string v0, "\\?"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v2

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v2

    const-string v1, "[^a-zA-Z0-9.]"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Z)[B
    .registers 13

    const/4 v1, 0x0

    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lin/org/npci/upiapp/utils/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lin/org/npci/upiapp/utils/b;->b(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_104

    invoke-static {v0}, Lin/org/npci/upiapp/utils/b;->a([B)Ljava/lang/String;

    move-result-object v0

    :goto_1b
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v3, "ts"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "If-None-Match"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0, p1, v2}, Lin/org/npci/upiapp/utils/RestClient;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)[B

    move-result-object v4

    if-eqz v4, :cond_d2

    const-string v0, ".zip"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d2

    if-eqz p2, :cond_d2

    invoke-static {v4}, Lin/org/npci/upiapp/utils/b;->a([B)Ljava/lang/String;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v5, Ljava/util/zip/ZipInputStream;

    invoke-direct {v5, v0}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v2, v1

    move-object v3, v1

    :goto_51
    :try_start_51
    invoke-virtual {v5}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v6

    if-eqz v6, :cond_9e

    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v5}, Ljava/util/zip/ZipInputStream;->read()I

    move-result v0

    :goto_60
    const/4 v8, -0x1

    if-eq v0, v8, :cond_6b

    invoke-virtual {v7, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    invoke-virtual {v5}, Ljava/util/zip/ZipInputStream;->read()I

    move-result v0

    goto :goto_60

    :cond_6b
    invoke-virtual {v5}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v8, ".signature"

    invoke-virtual {v0, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8a

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v0

    move-object v2, v3

    :goto_87
    move-object v3, v2

    move-object v2, v0

    goto :goto_51

    :cond_8a
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v6, ".jsa"

    invoke-virtual {v0, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_101

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_99
    .catch Ljava/lang/Exception; {:try_start_51 .. :try_end_99} :catch_ec

    move-result-object v0

    move-object v9, v2

    move-object v2, v0

    move-object v0, v9

    goto :goto_87

    :cond_9e
    if-nez v3, :cond_a3

    if-nez v2, :cond_a3

    :goto_a2
    return-object v1

    :cond_a3
    :try_start_a3
    new-instance v5, Ljava/io/ObjectInputStream;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    const-string v6, "remoteAssetPublicKey"

    invoke-static {p0, v6}, Lin/org/npci/upiapp/utils/b;->c(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v5, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_b3
    .catchall {:try_start_a3 .. :try_end_b3} :catchall_e5
    .catch Ljava/lang/Exception; {:try_start_a3 .. :try_end_b3} :catch_d6

    :try_start_b3
    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/PublicKey;

    const-string v6, "DSA"

    invoke-static {v6}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    invoke-virtual {v6, v3}, Ljava/security/Signature;->update([B)V

    invoke-virtual {v6, v2}, Ljava/security/Signature;->verify([B)Z
    :try_end_c8
    .catchall {:try_start_b3 .. :try_end_c8} :catchall_f9
    .catch Ljava/lang/Exception; {:try_start_b3 .. :try_end_c8} :catch_fc

    move-result v0

    if-nez v0, :cond_d4

    move-object v0, v1

    :goto_cc
    if-eqz v5, :cond_d1

    :try_start_ce
    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->close()V
    :try_end_d1
    .catch Ljava/lang/Exception; {:try_start_ce .. :try_end_d1} :catch_f5

    :cond_d1
    :goto_d1
    move-object v4, v0

    :cond_d2
    :goto_d2
    move-object v1, v4

    goto :goto_a2

    :cond_d4
    move-object v0, v3

    goto :goto_cc

    :catch_d6
    move-exception v0

    :goto_d7
    :try_start_d7
    const-string v2, "RemoteAssetService"

    const-string v3, "Exception while checking digital signature of asset"

    invoke-static {v2, v3, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_de
    .catchall {:try_start_d7 .. :try_end_de} :catchall_e5

    if-eqz v1, :cond_ff

    :try_start_e0
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V

    move-object v0, v4

    goto :goto_d1

    :catchall_e5
    move-exception v0

    :goto_e6
    if-eqz v1, :cond_eb

    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V

    :cond_eb
    throw v0
    :try_end_ec
    .catch Ljava/lang/Exception; {:try_start_e0 .. :try_end_ec} :catch_ec

    :catch_ec
    move-exception v0

    :goto_ed
    const-string v1, "RemoteAssetService"

    const-string v2, "Exception while checking digital signature of asset"

    invoke-static {v1, v2, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_d2

    :catch_f5
    move-exception v1

    move-object v4, v0

    move-object v0, v1

    goto :goto_ed

    :catchall_f9
    move-exception v0

    move-object v1, v5

    goto :goto_e6

    :catch_fc
    move-exception v0

    move-object v1, v5

    goto :goto_d7

    :cond_ff
    move-object v0, v4

    goto :goto_d1

    :cond_101
    move-object v0, v2

    move-object v2, v3

    goto :goto_87

    :cond_104
    move-object v0, v1

    goto/16 :goto_1b
.end method
