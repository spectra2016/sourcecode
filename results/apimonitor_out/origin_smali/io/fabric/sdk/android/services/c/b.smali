.class public abstract Lio/fabric/sdk/android/services/c/b;
.super Ljava/lang/Object;
.source "EventsFilesManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/fabric/sdk/android/services/c/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final MAX_BYTE_SIZE_PER_FILE:I = 0x1f40

.field public static final MAX_FILES_IN_BATCH:I = 0x1

.field public static final MAX_FILES_TO_KEEP:I = 0x64

.field public static final ROLL_OVER_FILE_NAME_SEPARATOR:Ljava/lang/String; = "_"


# instance fields
.field protected final context:Landroid/content/Context;

.field protected final currentTimeProvider:Lio/fabric/sdk/android/services/b/k;

.field private final defaultMaxFilesToKeep:I

.field protected final eventStorage:Lio/fabric/sdk/android/services/c/c;

.field protected volatile lastRollOverTime:J

.field protected final rollOverListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/fabric/sdk/android/services/c/d;",
            ">;"
        }
    .end annotation
.end field

.field protected final transform:Lio/fabric/sdk/android/services/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/fabric/sdk/android/services/c/a",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lio/fabric/sdk/android/services/c/a;Lio/fabric/sdk/android/services/b/k;Lio/fabric/sdk/android/services/c/c;I)V
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lio/fabric/sdk/android/services/c/a",
            "<TT;>;",
            "Lio/fabric/sdk/android/services/b/k;",
            "Lio/fabric/sdk/android/services/c/c;",
            "I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lio/fabric/sdk/android/services/c/b;->rollOverListeners:Ljava/util/List;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lio/fabric/sdk/android/services/c/b;->context:Landroid/content/Context;

    iput-object p2, p0, Lio/fabric/sdk/android/services/c/b;->transform:Lio/fabric/sdk/android/services/c/a;

    iput-object p4, p0, Lio/fabric/sdk/android/services/c/b;->eventStorage:Lio/fabric/sdk/android/services/c/c;

    iput-object p3, p0, Lio/fabric/sdk/android/services/c/b;->currentTimeProvider:Lio/fabric/sdk/android/services/b/k;

    iget-object v0, p0, Lio/fabric/sdk/android/services/c/b;->currentTimeProvider:Lio/fabric/sdk/android/services/b/k;

    invoke-interface {v0}, Lio/fabric/sdk/android/services/b/k;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lio/fabric/sdk/android/services/c/b;->lastRollOverTime:J

    iput p5, p0, Lio/fabric/sdk/android/services/c/b;->defaultMaxFilesToKeep:I

    return-void
.end method

.method private rollFileOverIfNeeded(I)V
    .registers 7

    iget-object v0, p0, Lio/fabric/sdk/android/services/c/b;->eventStorage:Lio/fabric/sdk/android/services/c/c;

    invoke-virtual {p0}, Lio/fabric/sdk/android/services/c/b;->getMaxByteSizePerFile()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lio/fabric/sdk/android/services/c/c;->a(II)Z

    move-result v0

    if-nez v0, :cond_41

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "session analytics events file is %d bytes, new event is %d bytes, this is over flush limit of %d, rolling it over"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lio/fabric/sdk/android/services/c/b;->eventStorage:Lio/fabric/sdk/android/services/c/c;

    invoke-interface {v4}, Lio/fabric/sdk/android/services/c/c;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p0}, Lio/fabric/sdk/android/services/c/b;->getMaxByteSizePerFile()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lio/fabric/sdk/android/services/c/b;->context:Landroid/content/Context;

    const/4 v2, 0x4

    const-string v3, "Fabric"

    invoke-static {v1, v2, v3, v0}, Lio/fabric/sdk/android/services/b/i;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lio/fabric/sdk/android/services/c/b;->rollFileOver()Z

    :cond_41
    return-void
.end method

.method private triggerRollOverOnListeners(Ljava/lang/String;)V
    .registers 6

    iget-object v0, p0, Lio/fabric/sdk/android/services/c/b;->rollOverListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/fabric/sdk/android/services/c/d;

    :try_start_12
    invoke-interface {v0, p1}, Lio/fabric/sdk/android/services/c/d;->onRollOver(Ljava/lang/String;)V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_15} :catch_16

    goto :goto_6

    :catch_16
    move-exception v0

    iget-object v2, p0, Lio/fabric/sdk/android/services/c/b;->context:Landroid/content/Context;

    const-string v3, "One of the roll over listeners threw an exception"

    invoke-static {v2, v3, v0}, Lio/fabric/sdk/android/services/b/i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    :cond_1f
    return-void
.end method


# virtual methods
.method public deleteAllEventsFiles()V
    .registers 3

    iget-object v0, p0, Lio/fabric/sdk/android/services/c/b;->eventStorage:Lio/fabric/sdk/android/services/c/c;

    iget-object v1, p0, Lio/fabric/sdk/android/services/c/b;->eventStorage:Lio/fabric/sdk/android/services/c/c;

    invoke-interface {v1}, Lio/fabric/sdk/android/services/c/c;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/fabric/sdk/android/services/c/c;->a(Ljava/util/List;)V

    iget-object v0, p0, Lio/fabric/sdk/android/services/c/b;->eventStorage:Lio/fabric/sdk/android/services/c/c;

    invoke-interface {v0}, Lio/fabric/sdk/android/services/c/c;->d()V

    return-void
.end method

.method public deleteOldestInRollOverIfOverMax()V
    .registers 10

    iget-object v0, p0, Lio/fabric/sdk/android/services/c/b;->eventStorage:Lio/fabric/sdk/android/services/c/c;

    invoke-interface {v0}, Lio/fabric/sdk/android/services/c/c;->c()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lio/fabric/sdk/android/services/c/b;->getMaxFilesToKeep()I

    move-result v1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-gt v2, v1, :cond_11

    :goto_10
    return-void

    :cond_11
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v1

    iget-object v3, p0, Lio/fabric/sdk/android/services/c/b;->context:Landroid/content/Context;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Found %d files in  roll over directory, this is greater than %d, deleting %d oldest files"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v7

    const/4 v1, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lio/fabric/sdk/android/services/b/i;->a(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v1, Ljava/util/TreeSet;

    new-instance v3, Lio/fabric/sdk/android/services/c/b$1;

    invoke-direct {v3, p0}, Lio/fabric/sdk/android/services/c/b$1;-><init>(Lio/fabric/sdk/android/services/c/b;)V

    invoke-direct {v1, v3}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lio/fabric/sdk/android/services/c/b;->parseCreationTimestampFromFileName(Ljava/lang/String;)J

    move-result-wide v4

    new-instance v6, Lio/fabric/sdk/android/services/c/b$a;

    invoke-direct {v6, v0, v4, v5}, Lio/fabric/sdk/android/services/c/b$a;-><init>(Ljava/io/File;J)V

    invoke-virtual {v1, v6}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_4d

    :cond_6a
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_73
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/fabric/sdk/android/services/c/b$a;

    iget-object v0, v0, Lio/fabric/sdk/android/services/c/b$a;->a:Ljava/io/File;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v2, :cond_73

    :cond_8a
    iget-object v0, p0, Lio/fabric/sdk/android/services/c/b;->eventStorage:Lio/fabric/sdk/android/services/c/c;

    invoke-interface {v0, v3}, Lio/fabric/sdk/android/services/c/c;->a(Ljava/util/List;)V

    goto :goto_10
.end method

.method public deleteSentFiles(Ljava/util/List;)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lio/fabric/sdk/android/services/c/b;->eventStorage:Lio/fabric/sdk/android/services/c/c;

    invoke-interface {v0, p1}, Lio/fabric/sdk/android/services/c/c;->a(Ljava/util/List;)V

    return-void
.end method

.method protected abstract generateUniqueRollOverFileName()Ljava/lang/String;
.end method

.method public getBatchOfFilesToSend()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lio/fabric/sdk/android/services/c/b;->eventStorage:Lio/fabric/sdk/android/services/c/c;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lio/fabric/sdk/android/services/c/c;->a(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLastRollOverTime()J
    .registers 3

    iget-wide v0, p0, Lio/fabric/sdk/android/services/c/b;->lastRollOverTime:J

    return-wide v0
.end method

.method protected getMaxByteSizePerFile()I
    .registers 2

    const/16 v0, 0x1f40

    return v0
.end method

.method protected getMaxFilesToKeep()I
    .registers 2

    iget v0, p0, Lio/fabric/sdk/android/services/c/b;->defaultMaxFilesToKeep:I

    return v0
.end method

.method public parseCreationTimestampFromFileName(Ljava/lang/String;)J
    .registers 7

    const-wide/16 v0, 0x0

    const-string v2, "_"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_d

    :goto_c
    return-wide v0

    :cond_d
    const/4 v3, 0x2

    :try_start_e
    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_17
    .catch Ljava/lang/NumberFormatException; {:try_start_e .. :try_end_17} :catch_19

    move-result-wide v0

    goto :goto_c

    :catch_19
    move-exception v2

    goto :goto_c
.end method

.method public registerRollOverListener(Lio/fabric/sdk/android/services/c/d;)V
    .registers 3

    if-eqz p1, :cond_7

    iget-object v0, p0, Lio/fabric/sdk/android/services/c/b;->rollOverListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    return-void
.end method

.method public rollFileOver()Z
    .registers 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v3, p0, Lio/fabric/sdk/android/services/c/b;->eventStorage:Lio/fabric/sdk/android/services/c/c;

    invoke-interface {v3}, Lio/fabric/sdk/android/services/c/c;->b()Z

    move-result v3

    if-nez v3, :cond_34

    invoke-virtual {p0}, Lio/fabric/sdk/android/services/c/b;->generateUniqueRollOverFileName()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lio/fabric/sdk/android/services/c/b;->eventStorage:Lio/fabric/sdk/android/services/c/c;

    invoke-interface {v3, v0}, Lio/fabric/sdk/android/services/c/c;->a(Ljava/lang/String;)V

    iget-object v3, p0, Lio/fabric/sdk/android/services/c/b;->context:Landroid/content/Context;

    const/4 v4, 0x4

    const-string v5, "Fabric"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "generated new file %s"

    new-array v8, v1, [Ljava/lang/Object;

    aput-object v0, v8, v2

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v5, v2}, Lio/fabric/sdk/android/services/b/i;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lio/fabric/sdk/android/services/c/b;->currentTimeProvider:Lio/fabric/sdk/android/services/b/k;

    invoke-interface {v2}, Lio/fabric/sdk/android/services/b/k;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lio/fabric/sdk/android/services/c/b;->lastRollOverTime:J

    :goto_30
    invoke-direct {p0, v0}, Lio/fabric/sdk/android/services/c/b;->triggerRollOverOnListeners(Ljava/lang/String;)V

    return v1

    :cond_34
    move v1, v2

    goto :goto_30
.end method

.method public writeEvent(Ljava/lang/Object;)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lio/fabric/sdk/android/services/c/b;->transform:Lio/fabric/sdk/android/services/c/a;

    invoke-interface {v0, p1}, Lio/fabric/sdk/android/services/c/a;->toBytes(Ljava/lang/Object;)[B

    move-result-object v0

    array-length v1, v0

    invoke-direct {p0, v1}, Lio/fabric/sdk/android/services/c/b;->rollFileOverIfNeeded(I)V

    iget-object v1, p0, Lio/fabric/sdk/android/services/c/b;->eventStorage:Lio/fabric/sdk/android/services/c/c;

    invoke-interface {v1, v0}, Lio/fabric/sdk/android/services/c/c;->a([B)V

    return-void
.end method
