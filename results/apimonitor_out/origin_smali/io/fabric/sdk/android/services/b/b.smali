.class Lio/fabric/sdk/android/services/b/b;
.super Ljava/lang/Object;
.source "AdvertisingInfo.java"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Z)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lio/fabric/sdk/android/services/b/b;->a:Ljava/lang/String;

    iput-boolean p2, p0, Lio/fabric/sdk/android/services/b/b;->b:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_5

    :cond_4
    :goto_4
    return v0

    :cond_5
    if-eqz p1, :cond_11

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_13

    :cond_11
    move v0, v1

    goto :goto_4

    :cond_13
    check-cast p1, Lio/fabric/sdk/android/services/b/b;

    iget-boolean v2, p0, Lio/fabric/sdk/android/services/b/b;->b:Z

    iget-boolean v3, p1, Lio/fabric/sdk/android/services/b/b;->b:Z

    if-eq v2, v3, :cond_1d

    move v0, v1

    goto :goto_4

    :cond_1d
    iget-object v2, p0, Lio/fabric/sdk/android/services/b/b;->a:Ljava/lang/String;

    if-eqz v2, :cond_2d

    iget-object v2, p0, Lio/fabric/sdk/android/services/b/b;->a:Ljava/lang/String;

    iget-object v3, p1, Lio/fabric/sdk/android/services/b/b;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :goto_2b
    move v0, v1

    goto :goto_4

    :cond_2d
    iget-object v2, p1, Lio/fabric/sdk/android/services/b/b;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    goto :goto_2b
.end method

.method public hashCode()I
    .registers 4

    const/4 v1, 0x0

    iget-object v0, p0, Lio/fabric/sdk/android/services/b/b;->a:Ljava/lang/String;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lio/fabric/sdk/android/services/b/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_b
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lio/fabric/sdk/android/services/b/b;->b:Z

    if-eqz v2, :cond_12

    const/4 v1, 0x1

    :cond_12
    add-int/2addr v0, v1

    return v0

    :cond_14
    move v0, v1

    goto :goto_b
.end method
