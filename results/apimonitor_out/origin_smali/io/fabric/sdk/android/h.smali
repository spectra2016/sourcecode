.class public abstract Lio/fabric/sdk/android/h;
.super Ljava/lang/Object;
.source "Kit.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Result:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lio/fabric/sdk/android/h;",
        ">;"
    }
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field final dependsOnAnnotation:Lio/fabric/sdk/android/services/concurrency/d;

.field fabric:Lio/fabric/sdk/android/c;

.field idManager:Lio/fabric/sdk/android/services/b/o;

.field initializationCallback:Lio/fabric/sdk/android/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/fabric/sdk/android/f",
            "<TResult;>;"
        }
    .end annotation
.end field

.field initializationTask:Lio/fabric/sdk/android/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/fabric/sdk/android/g",
            "<TResult;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lio/fabric/sdk/android/g;

    invoke-direct {v0, p0}, Lio/fabric/sdk/android/g;-><init>(Lio/fabric/sdk/android/h;)V

    iput-object v0, p0, Lio/fabric/sdk/android/h;->initializationTask:Lio/fabric/sdk/android/g;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lio/fabric/sdk/android/services/concurrency/d;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lio/fabric/sdk/android/services/concurrency/d;

    iput-object v0, p0, Lio/fabric/sdk/android/h;->dependsOnAnnotation:Lio/fabric/sdk/android/services/concurrency/d;

    return-void
.end method


# virtual methods
.method public compareTo(Lio/fabric/sdk/android/h;)I
    .registers 5

    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-virtual {p0, p1}, Lio/fabric/sdk/android/h;->containsAnnotatedDependency(Lio/fabric/sdk/android/h;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_8
    :goto_8
    return v0

    :cond_9
    invoke-virtual {p1, p0}, Lio/fabric/sdk/android/h;->containsAnnotatedDependency(Lio/fabric/sdk/android/h;)Z

    move-result v2

    if-eqz v2, :cond_11

    move v0, v1

    goto :goto_8

    :cond_11
    invoke-virtual {p0}, Lio/fabric/sdk/android/h;->hasAnnotatedDependency()Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-virtual {p1}, Lio/fabric/sdk/android/h;->hasAnnotatedDependency()Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_1d
    invoke-virtual {p0}, Lio/fabric/sdk/android/h;->hasAnnotatedDependency()Z

    move-result v0

    if-nez v0, :cond_2b

    invoke-virtual {p1}, Lio/fabric/sdk/android/h;->hasAnnotatedDependency()Z

    move-result v0

    if-eqz v0, :cond_2b

    move v0, v1

    goto :goto_8

    :cond_2b
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3

    check-cast p1, Lio/fabric/sdk/android/h;

    invoke-virtual {p0, p1}, Lio/fabric/sdk/android/h;->compareTo(Lio/fabric/sdk/android/h;)I

    move-result v0

    return v0
.end method

.method containsAnnotatedDependency(Lio/fabric/sdk/android/h;)Z
    .registers 8

    const/4 v0, 0x0

    invoke-virtual {p0}, Lio/fabric/sdk/android/h;->hasAnnotatedDependency()Z

    move-result v1

    if-eqz v1, :cond_1e

    iget-object v1, p0, Lio/fabric/sdk/android/h;->dependsOnAnnotation:Lio/fabric/sdk/android/services/concurrency/d;

    invoke-interface {v1}, Lio/fabric/sdk/android/services/concurrency/d;->a()[Ljava/lang/Class;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_f
    if-ge v1, v3, :cond_1e

    aget-object v4, v2, v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_1f

    const/4 v0, 0x1

    :cond_1e
    return v0

    :cond_1f
    add-int/lit8 v1, v1, 0x1

    goto :goto_f
.end method

.method protected abstract doInBackground()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TResult;"
        }
    .end annotation
.end method

.method public getContext()Landroid/content/Context;
    .registers 2

    iget-object v0, p0, Lio/fabric/sdk/android/h;->context:Landroid/content/Context;

    return-object v0
.end method

.method protected getDependencies()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lio/fabric/sdk/android/services/concurrency/l;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lio/fabric/sdk/android/h;->initializationTask:Lio/fabric/sdk/android/g;

    invoke-virtual {v0}, Lio/fabric/sdk/android/g;->getDependencies()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getFabric()Lio/fabric/sdk/android/c;
    .registers 2

    iget-object v0, p0, Lio/fabric/sdk/android/h;->fabric:Lio/fabric/sdk/android/c;

    return-object v0
.end method

.method protected getIdManager()Lio/fabric/sdk/android/services/b/o;
    .registers 2

    iget-object v0, p0, Lio/fabric/sdk/android/h;->idManager:Lio/fabric/sdk/android/services/b/o;

    return-object v0
.end method

.method public abstract getIdentifier()Ljava/lang/String;
.end method

.method public getPath()Ljava/lang/String;
    .registers 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ".Fabric"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lio/fabric/sdk/android/h;->getIdentifier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getVersion()Ljava/lang/String;
.end method

.method hasAnnotatedDependency()Z
    .registers 2

    iget-object v0, p0, Lio/fabric/sdk/android/h;->dependsOnAnnotation:Lio/fabric/sdk/android/services/concurrency/d;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method final initialize()V
    .registers 6

    iget-object v1, p0, Lio/fabric/sdk/android/h;->initializationTask:Lio/fabric/sdk/android/g;

    iget-object v0, p0, Lio/fabric/sdk/android/h;->fabric:Lio/fabric/sdk/android/c;

    invoke-virtual {v0}, Lio/fabric/sdk/android/c;->f()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Void;

    const/4 v4, 0x0

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/Void;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lio/fabric/sdk/android/g;->a(Ljava/util/concurrent/ExecutorService;[Ljava/lang/Object;)V

    return-void
.end method

.method injectParameters(Landroid/content/Context;Lio/fabric/sdk/android/c;Lio/fabric/sdk/android/f;Lio/fabric/sdk/android/services/b/o;)V
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lio/fabric/sdk/android/c;",
            "Lio/fabric/sdk/android/f",
            "<TResult;>;",
            "Lio/fabric/sdk/android/services/b/o;",
            ")V"
        }
    .end annotation

    iput-object p2, p0, Lio/fabric/sdk/android/h;->fabric:Lio/fabric/sdk/android/c;

    new-instance v0, Lio/fabric/sdk/android/d;

    invoke-virtual {p0}, Lio/fabric/sdk/android/h;->getIdentifier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lio/fabric/sdk/android/h;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lio/fabric/sdk/android/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lio/fabric/sdk/android/h;->context:Landroid/content/Context;

    iput-object p3, p0, Lio/fabric/sdk/android/h;->initializationCallback:Lio/fabric/sdk/android/f;

    iput-object p4, p0, Lio/fabric/sdk/android/h;->idManager:Lio/fabric/sdk/android/services/b/o;

    return-void
.end method

.method protected onCancelled(Ljava/lang/Object;)V
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    return-void
.end method

.method protected onPreExecute()Z
    .registers 2

    const/4 v0, 0x1

    return v0
.end method
