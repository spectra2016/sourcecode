.class public final Lcom/google/android/gms/internal/m;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/m$1;,
        Lcom/google/android/gms/internal/m$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/gms/internal/m;


# instance fields
.field private final b:Z

.field private final c:Z

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/android/gms/common/api/c$d;

.field private final f:Z

.field private final g:Z

.field private final h:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Lcom/google/android/gms/internal/m$a;

    invoke-direct {v0}, Lcom/google/android/gms/internal/m$a;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/internal/m$a;->a()Lcom/google/android/gms/internal/m;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/m;->a:Lcom/google/android/gms/internal/m;

    return-void
.end method

.method private constructor <init>(ZZLjava/lang/String;Lcom/google/android/gms/common/api/c$d;ZZZ)V
    .registers 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/gms/internal/m;->b:Z

    iput-boolean p2, p0, Lcom/google/android/gms/internal/m;->c:Z

    iput-object p3, p0, Lcom/google/android/gms/internal/m;->d:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/m;->e:Lcom/google/android/gms/common/api/c$d;

    iput-boolean p5, p0, Lcom/google/android/gms/internal/m;->f:Z

    iput-boolean p6, p0, Lcom/google/android/gms/internal/m;->g:Z

    iput-boolean p7, p0, Lcom/google/android/gms/internal/m;->h:Z

    return-void
.end method

.method synthetic constructor <init>(ZZLjava/lang/String;Lcom/google/android/gms/common/api/c$d;ZZZLcom/google/android/gms/internal/m$1;)V
    .registers 9

    invoke-direct/range {p0 .. p7}, Lcom/google/android/gms/internal/m;-><init>(ZZLjava/lang/String;Lcom/google/android/gms/common/api/c$d;ZZZ)V

    return-void
.end method
