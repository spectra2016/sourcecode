.class public final Lcom/google/android/gms/common/internal/h$d;
.super Lcom/google/android/gms/common/internal/m$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/internal/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# instance fields
.field private a:Lcom/google/android/gms/common/internal/h;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/h;I)V
    .registers 3

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/m$a;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/internal/h$d;->a:Lcom/google/android/gms/common/internal/h;

    iput p2, p0, Lcom/google/android/gms/common/internal/h$d;->b:I

    return-void
.end method

.method private a()V
    .registers 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/h$d;->a:Lcom/google/android/gms/common/internal/h;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)V
    .registers 5

    iget-object v0, p0, Lcom/google/android/gms/common/internal/h$d;->a:Lcom/google/android/gms/common/internal/h;

    const-string v1, "onAccountValidationComplete can be called only once per call to validateAccount"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/h$d;->a:Lcom/google/android/gms/common/internal/h;

    iget v1, p0, Lcom/google/android/gms/common/internal/h$d;->b:I

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/common/internal/h;->a(ILandroid/os/Bundle;I)V

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/h$d;->a()V

    return-void
.end method

.method public a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .registers 6

    iget-object v0, p0, Lcom/google/android/gms/common/internal/h$d;->a:Lcom/google/android/gms/common/internal/h;

    const-string v1, "onPostInitComplete can be called only once per call to getRemoteService"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/h$d;->a:Lcom/google/android/gms/common/internal/h;

    iget v1, p0, Lcom/google/android/gms/common/internal/h$d;->b:I

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/google/android/gms/common/internal/h;->a(ILandroid/os/IBinder;Landroid/os/Bundle;I)V

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/h$d;->a()V

    return-void
.end method
