.class public final Lcom/google/android/gms/common/internal/h$h;
.super Lcom/google/android/gms/common/internal/h$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/internal/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x14
    name = "h"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/h",
        "<TT;>.a;"
    }
.end annotation


# instance fields
.field final synthetic e:Lcom/google/android/gms/common/internal/h;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/h;)V
    .registers 4

    iput-object p1, p0, Lcom/google/android/gms/common/internal/h$h;->e:Lcom/google/android/gms/common/internal/h;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/common/internal/h$a;-><init>(Lcom/google/android/gms/common/internal/h;ILandroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/common/ConnectionResult;)V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/common/internal/h$h;->e:Lcom/google/android/gms/common/internal/h;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/h;->a(Lcom/google/android/gms/common/internal/h;)Lcom/google/android/gms/common/api/c$e;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/c$e;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    iget-object v0, p0, Lcom/google/android/gms/common/internal/h$h;->e:Lcom/google/android/gms/common/internal/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/h;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method protected a()Z
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/common/internal/h$h;->e:Lcom/google/android/gms/common/internal/h;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/h;->a(Lcom/google/android/gms/common/internal/h;)Lcom/google/android/gms/common/api/c$e;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/ConnectionResult;->a:Lcom/google/android/gms/common/ConnectionResult;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/c$e;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    const/4 v0, 0x1

    return v0
.end method
