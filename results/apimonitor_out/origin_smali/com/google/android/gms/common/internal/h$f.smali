.class public Lcom/google/android/gms/common/internal/h$f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/c$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/internal/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/internal/h;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/h;)V
    .registers 2

    iput-object p1, p0, Lcom/google/android/gms/common/internal/h$f;->a:Lcom/google/android/gms/common/internal/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/common/ConnectionResult;)V
    .registers 5

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->a()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/gms/common/internal/h$f;->a:Lcom/google/android/gms/common/internal/h;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/common/internal/h$f;->a:Lcom/google/android/gms/common/internal/h;

    invoke-static {v2}, Lcom/google/android/gms/common/internal/h;->d(Lcom/google/android/gms/common/internal/h;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/h;->a(Lcom/google/android/gms/common/internal/l;Ljava/util/Set;)V

    :cond_12
    :goto_12
    return-void

    :cond_13
    iget-object v0, p0, Lcom/google/android/gms/common/internal/h$f;->a:Lcom/google/android/gms/common/internal/h;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/h;->e(Lcom/google/android/gms/common/internal/h;)Lcom/google/android/gms/common/api/c$c;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/gms/common/internal/h$f;->a:Lcom/google/android/gms/common/internal/h;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/h;->e(Lcom/google/android/gms/common/internal/h;)Lcom/google/android/gms/common/api/c$c;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/c$c;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_12
.end method

.method public b(Lcom/google/android/gms/common/ConnectionResult;)V
    .registers 4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Legacy GmsClient received onReportAccountValidation callback."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
