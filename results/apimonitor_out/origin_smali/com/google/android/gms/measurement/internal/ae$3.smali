.class Lcom/google/android/gms/measurement/internal/ae$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/measurement/internal/ae;->o()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/measurement/internal/ae;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ae;)V
    .registers 2

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ae$3;->a:Lcom/google/android/gms/measurement/internal/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$3;->a:Lcom/google/android/gms/measurement/internal/ae;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ae;->c(Lcom/google/android/gms/measurement/internal/ae;)Lcom/google/android/gms/measurement/internal/q;

    move-result-object v0

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$3;->a:Lcom/google/android/gms/measurement/internal/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Failed to send measurementEnabled to service"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    :goto_17
    return-void

    :cond_18
    :try_start_18
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$3;->a:Lcom/google/android/gms/measurement/internal/ae;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ae;->f()Lcom/google/android/gms/measurement/internal/r;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ae$3;->a:Lcom/google/android/gms/measurement/internal/ae;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/t;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/r;->a(Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/AppMetadata;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/measurement/internal/q;->b(Lcom/google/android/gms/measurement/internal/AppMetadata;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$3;->a:Lcom/google/android/gms/measurement/internal/ae;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ae;->d(Lcom/google/android/gms/measurement/internal/ae;)V
    :try_end_34
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_34} :catch_35

    goto :goto_17

    :catch_35
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$3;->a:Lcom/google/android/gms/measurement/internal/ae;

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v1

    const-string v2, "Failed to send measurementEnabled to AppMeasurementService"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_17
.end method
