.class public final Lcom/google/android/gms/measurement/internal/p;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/measurement/internal/p$a;
    }
.end annotation


# static fields
.field public static a:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static b:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static c:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static d:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static e:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static f:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static g:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static h:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static i:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static j:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static k:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static l:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static m:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static n:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static o:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static p:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static q:Lcom/google/android/gms/measurement/internal/p$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/measurement/internal/p$a",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 8

    const-wide/32 v6, 0x5265c00

    const-wide/32 v4, 0x36ee80

    const/4 v1, 0x1

    const-string v0, "measurement.service_enabled"

    invoke-static {v0, v1}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;Z)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->a:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.service_client_enabled"

    invoke-static {v0, v1}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;Z)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->b:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.log_tag"

    const-string v1, "GMPM"

    const-string v2, "GMPM-SVC"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->c:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.ad_id_cache_time"

    const-wide/16 v2, 0x2710

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->d:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.monitoring.sample_period_millis"

    invoke-static {v0, v6, v7}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->e:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.upload.max_bundles"

    const/16 v1, 0x64

    invoke-static {v0, v1}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;I)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->f:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.upload.max_batch_size"

    const/high16 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;I)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->g:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.upload.url"

    const-string v1, "https://app-measurement.com/a"

    invoke-static {v0, v1}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->h:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.upload.backoff_period"

    const-wide/32 v2, 0x2932e00

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->i:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.upload.window_interval"

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->j:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.upload.interval"

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->k:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.upload.stale_data_deletion_interval"

    invoke-static {v0, v6, v7}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->l:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.upload.initial_upload_delay_time"

    const-wide/16 v2, 0x3a98

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->m:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.upload.retry_time"

    const-wide/32 v2, 0x1b7740

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->n:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.upload.retry_count"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;I)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->o:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.upload.max_queue_time"

    const-wide v2, 0x90321000L

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->p:Lcom/google/android/gms/measurement/internal/p$a;

    const-string v0, "measurement.service_client.idle_disconnect_millis"

    const-wide/16 v2, 0x1388

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/p$a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/p;->q:Lcom/google/android/gms/measurement/internal/p$a;

    return-void
.end method
