.class public Lcom/google/android/gms/measurement/internal/r;
.super Lcom/google/android/gms/measurement/internal/ab;


# static fields
.field private static final a:Ljavax/security/auth/x500/X500Principal;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:J

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    new-instance v0, Ljavax/security/auth/x500/X500Principal;

    const-string v1, "CN=Android Debug,O=Android,C=US"

    invoke-direct {v0, v1}, Ljavax/security/auth/x500/X500Principal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/measurement/internal/r;->a:Ljavax/security/auth/x500/X500Principal;

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/measurement/internal/y;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/ab;-><init>(Lcom/google/android/gms/measurement/internal/y;)V

    return-void
.end method

.method static a([B)J
    .registers 9

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    array-length v0, p0

    if-lez v0, :cond_24

    const/4 v0, 0x1

    :goto_8
    invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->a(Z)V

    const-wide/16 v2, 0x0

    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    :goto_10
    if-ltz v0, :cond_26

    array-length v4, p0

    add-int/lit8 v4, v4, -0x8

    if-lt v0, v4, :cond_26

    aget-byte v4, p0, v0

    int-to-long v4, v4

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    shl-long/2addr v4, v1

    add-long/2addr v2, v4

    add-int/lit8 v1, v1, 0x8

    add-int/lit8 v0, v0, -0x1

    goto :goto_10

    :cond_24
    move v0, v1

    goto :goto_8

    :cond_26
    return-wide v2
.end method


# virtual methods
.method a(Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/AppMetadata;
    .registers 14

    new-instance v1, Lcom/google/android/gms/measurement/internal/AppMetadata;

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/r;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/measurement/internal/r;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/measurement/internal/r;->d:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->B()J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->o()J

    move-result-wide v8

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->m()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/w;->r()Z

    move-result v11

    move-object v10, p1

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/measurement/internal/AppMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Z)V

    return-object v1
.end method

.method protected a()V
    .registers 9

    const-string v1, "Unknown"

    const-string v0, "Unknown"

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->i()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->i()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_80

    const-string v2, "manual_install"

    :cond_1c
    :goto_1c
    :try_start_1c
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->i()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    if-eqz v5, :cond_3d

    iget-object v6, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3b

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_3b
    iget-object v1, v5, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_3d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1c .. :try_end_3d} :catch_8b

    :cond_3d
    :goto_3d
    iput-object v4, p0, Lcom/google/android/gms/measurement/internal/r;->b:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/gms/measurement/internal/r;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/r;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/r;->e:Ljava/lang/String;

    const-wide/16 v0, 0x0

    :try_start_47
    const-string v2, "MD5"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->p()Z

    move-result v4

    if-nez v4, :cond_7d

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->i()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x40

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    if-eqz v2, :cond_7d

    iget-object v4, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v4, :cond_7d

    iget-object v4, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v4, v4

    if-lez v4, :cond_7d

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/measurement/internal/r;->a([B)J
    :try_end_7c
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_47 .. :try_end_7c} :catch_9a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_47 .. :try_end_7c} :catch_a9

    move-result-wide v0

    :cond_7d
    :goto_7d
    iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/r;->f:J

    return-void

    :cond_80
    const-string v5, "com.android.vending"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1c

    const-string v2, ""

    goto :goto_1c

    :catch_8b
    move-exception v5

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v5

    const-string v6, "Error retrieving package info: appName"

    invoke-virtual {v5, v6, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_3d

    :catch_9a
    move-exception v2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v3

    const-string v4, "Could not get MD5 instance"

    invoke-virtual {v3, v4, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_7d

    :catch_a9
    move-exception v2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v3

    const-string v4, "Package name not found"

    invoke-virtual {v3, v4, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_7d
.end method

.method b()Ljava/lang/String;
    .registers 5

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->y()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->C()Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, ""

    :goto_f
    return-object v0

    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/r;->h:Ljava/lang/String;

    if-nez v0, :cond_38

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->i()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/measurement/b;->a(Landroid/content/Context;)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    if-eqz v1, :cond_53

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->d()Z

    move-result v0

    if-eqz v0, :cond_53

    :try_start_24
    invoke-static {}, Lcom/google/android/gms/measurement/b;->c()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-static {}, Lcom/google/android/gms/measurement/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_36

    const-string v0, ""

    :cond_36
    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/r;->h:Ljava/lang/String;
    :try_end_38
    .catch Ljava/lang/IllegalStateException; {:try_start_24 .. :try_end_38} :catch_40

    :cond_38
    :goto_38
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/r;->h:Ljava/lang/String;

    goto :goto_f

    :cond_3b
    :try_start_3b
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/r;->h:Ljava/lang/String;
    :try_end_3f
    .catch Ljava/lang/IllegalStateException; {:try_start_3b .. :try_end_3f} :catch_40

    goto :goto_38

    :catch_40
    move-exception v0

    const-string v1, ""

    iput-object v1, p0, Lcom/google/android/gms/measurement/internal/r;->h:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v1

    const-string v2, "getGoogleAppId or isMeasurementEnabled failed with exception"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_38

    :cond_53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/r;->h:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v2

    const-string v3, "getGoogleAppId failed with status"

    if-nez v1, :cond_83

    const/4 v0, 0x0

    :goto_64
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    if-eqz v1, :cond_38

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_38

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->s()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    goto :goto_38

    :cond_83
    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->e()I

    move-result v0

    goto :goto_64
.end method

.method public bridge synthetic c()V
    .registers 1

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->c()V

    return-void
.end method

.method public bridge synthetic d()V
    .registers 1

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->d()V

    return-void
.end method

.method public bridge synthetic e()V
    .registers 1

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->e()V

    return-void
.end method

.method public bridge synthetic f()Lcom/google/android/gms/measurement/internal/r;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->f()Lcom/google/android/gms/measurement/internal/r;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic g()Lcom/google/android/gms/measurement/internal/ae;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->g()Lcom/google/android/gms/measurement/internal/ae;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic h()Lcom/google/android/gms/internal/e;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->h()Lcom/google/android/gms/internal/e;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic i()Landroid/content/Context;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->i()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic j()Lcom/google/android/gms/measurement/internal/f;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->j()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic k()Lcom/google/android/gms/measurement/internal/x;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->k()Lcom/google/android/gms/measurement/internal/x;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic l()Lcom/google/android/gms/measurement/internal/t;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic m()Lcom/google/android/gms/measurement/internal/w;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->m()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic n()Lcom/google/android/gms/measurement/internal/h;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    return-object v0
.end method

.method o()J
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->y()V

    iget-wide v0, p0, Lcom/google/android/gms/measurement/internal/r;->f:J

    return-wide v0
.end method

.method p()Z
    .registers 4

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->i()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->i()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    if-eqz v0, :cond_54

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v1, :cond_54

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v1, v1

    if-lez v1, :cond_54

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const-string v1, "X.509"

    invoke-static {v1}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/measurement/internal/r;->a:Ljavax/security/auth/x500/X500Principal;

    invoke-virtual {v0, v1}, Ljavax/security/auth/x500/X500Principal;->equals(Ljava/lang/Object;)Z
    :try_end_44
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_44} :catch_46
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_44} :catch_56

    move-result v0

    :goto_45
    return v0

    :catch_46
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v1

    const-string v2, "Error obtaining certificate"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_54
    :goto_54
    const/4 v0, 0x1

    goto :goto_45

    :catch_56
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/r;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v1

    const-string v2, "Package name not found"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_54
.end method
