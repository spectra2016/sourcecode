.class public Lcom/google/android/gms/measurement/internal/ae;
.super Lcom/google/android/gms/measurement/internal/ab;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/measurement/internal/ae$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/measurement/internal/ae$a;

.field private b:Lcom/google/android/gms/measurement/internal/q;

.field private c:Ljava/lang/Boolean;

.field private final d:Lcom/google/android/gms/measurement/internal/j;

.field private final e:Lcom/google/android/gms/measurement/internal/b;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/google/android/gms/measurement/internal/j;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/measurement/internal/y;)V
    .registers 4

    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/ab;-><init>(Lcom/google/android/gms/measurement/internal/y;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->f:Ljava/util/List;

    new-instance v0, Lcom/google/android/gms/measurement/internal/b;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/y;->o()Lcom/google/android/gms/internal/e;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/measurement/internal/b;-><init>(Lcom/google/android/gms/internal/e;)V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->e:Lcom/google/android/gms/measurement/internal/b;

    new-instance v0, Lcom/google/android/gms/measurement/internal/ae$a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/measurement/internal/ae$a;-><init>(Lcom/google/android/gms/measurement/internal/ae;)V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->a:Lcom/google/android/gms/measurement/internal/ae$a;

    new-instance v0, Lcom/google/android/gms/measurement/internal/ae$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/measurement/internal/ae$1;-><init>(Lcom/google/android/gms/measurement/internal/ae;Lcom/google/android/gms/measurement/internal/y;)V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->d:Lcom/google/android/gms/measurement/internal/j;

    new-instance v0, Lcom/google/android/gms/measurement/internal/ae$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/measurement/internal/ae$2;-><init>(Lcom/google/android/gms/measurement/internal/ae;Lcom/google/android/gms/measurement/internal/y;)V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->h:Lcom/google/android/gms/measurement/internal/j;

    return-void
.end method

.method private A()V
    .registers 1

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ae;->s()V

    return-void
.end method

.method private B()V
    .registers 4

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Processing queued up service tasks"

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ae;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_20
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->k()Lcom/google/android/gms/measurement/internal/x;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V

    goto :goto_20

    :cond_34
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->h:Lcom/google/android/gms/measurement/internal/j;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/j;->c()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/measurement/internal/ae;)Lcom/google/android/gms/measurement/internal/ae$a;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->a:Lcom/google/android/gms/measurement/internal/ae$a;

    return-object v0
.end method

.method private a(Landroid/content/ComponentName;)V
    .registers 4

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->b:Lcom/google/android/gms/measurement/internal/q;

    if-eqz v0, :cond_1a

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->b:Lcom/google/android/gms/measurement/internal/q;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Disconnected from device MeasurementService"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ae;->A()V

    :cond_1a
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/measurement/internal/ae;Landroid/content/ComponentName;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/ae;->a(Landroid/content/ComponentName;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/measurement/internal/ae;Lcom/google/android/gms/measurement/internal/q;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/ae;->a(Lcom/google/android/gms/measurement/internal/q;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/measurement/internal/q;)V
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ae;->b:Lcom/google/android/gms/measurement/internal/q;

    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ae;->r()V

    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ae;->B()V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .registers 6

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :goto_c
    return-void

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/h;->G()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2e

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Discarding data. Max runnable queue size reached"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    goto :goto_c

    :cond_2e
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->g:Lcom/google/android/gms/measurement/internal/y;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->v()Z

    move-result v0

    if-nez v0, :cond_43

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->h:Lcom/google/android/gms/measurement/internal/j;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/j;->a(J)V

    :cond_43
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ae;->s()V

    goto :goto_c
.end method

.method static synthetic b(Lcom/google/android/gms/measurement/internal/ae;)V
    .registers 1

    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ae;->v()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/measurement/internal/ae;)Lcom/google/android/gms/measurement/internal/q;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->b:Lcom/google/android/gms/measurement/internal/q;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/measurement/internal/ae;)V
    .registers 1

    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ae;->r()V

    return-void
.end method

.method private r()V
    .registers 5

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->e:Lcom/google/android/gms/measurement/internal/b;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/b;->a()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->g:Lcom/google/android/gms/measurement/internal/y;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->v()Z

    move-result v0

    if-nez v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->d:Lcom/google/android/gms/measurement/internal/j;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/h;->y()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/j;->a(J)V

    :cond_1d
    return-void
.end method

.method private s()V
    .registers 5

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->y()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    :goto_c
    return-void

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_43

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->m()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/w;->q()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->c:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_43

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "State of service unknown"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ae;->u()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->c:Ljava/lang/Boolean;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->m()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/w;->a(Z)V

    :cond_43
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5e

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Using measurement service"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->a:Lcom/google/android/gms/measurement/internal/ae$a;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae$a;->a()V

    goto :goto_c

    :cond_5e
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/ae;->t()Z

    move-result v0

    if-eqz v0, :cond_95

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->g:Lcom/google/android/gms/measurement/internal/y;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->v()Z

    move-result v0

    if-nez v0, :cond_95

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Using local app measurement service"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.measurement.START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->i()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/google/android/gms/measurement/AppMeasurementService;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae;->a:Lcom/google/android/gms/measurement/internal/ae$a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/measurement/internal/ae$a;->a(Landroid/content/Intent;)V

    goto/16 :goto_c

    :cond_95
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->D()Z

    move-result v0

    if-eqz v0, :cond_b9

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Using direct local measurement implementation"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/measurement/internal/z;

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae;->g:Lcom/google/android/gms/measurement/internal/y;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/z;-><init>(Lcom/google/android/gms/measurement/internal/y;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/ae;->a(Lcom/google/android/gms/measurement/internal/q;)V

    goto/16 :goto_c

    :cond_b9
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Not in main process. Unable to use local measurement implementation. Please register the AppMeasurementService service in the app manifest"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    goto/16 :goto_c
.end method

.method private t()Z
    .registers 5

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->i()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->i()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/google/android/gms/measurement/AppMeasurementService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_23

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_23

    const/4 v0, 0x1

    :goto_22
    return v0

    :cond_23
    const/4 v0, 0x0

    goto :goto_22
.end method

.method private u()Z
    .registers 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->y()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/h;->C()Z

    move-result v2

    if-eqz v2, :cond_13

    :goto_12
    return v0

    :cond_13
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.measurement.START"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v3, Landroid/content/ComponentName;

    const-string v4, "com.google.android.gms"

    const-string v5, "com.google.android.gms.measurement.service.MeasurementBrokerService"

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v4

    const-string v5, "Checking service availability"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->i()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/measurement/internal/ae$7;

    invoke-direct {v5, p0}, Lcom/google/android/gms/measurement/internal/ae$7;-><init>(Lcom/google/android/gms/measurement/internal/ae;)V

    invoke-virtual {v3, v4, v2, v5, v1}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    if-eqz v2, :cond_54

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v1

    const-string v2, "Service available"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    goto :goto_12

    :cond_54
    move v0, v1

    goto :goto_12
.end method

.method private v()V
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->b()Z

    move-result v0

    if-nez v0, :cond_a

    :goto_9
    return-void

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Inactivity, disconnecting from AppMeasurementService"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->q()V

    goto :goto_9
.end method


# virtual methods
.method protected a()V
    .registers 1

    return-void
.end method

.method protected a(Lcom/google/android/gms/measurement/internal/EventParcel;Ljava/lang/String;)V
    .registers 4

    invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->y()V

    new-instance v0, Lcom/google/android/gms/measurement/internal/ae$4;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/gms/measurement/internal/ae$4;-><init>(Lcom/google/android/gms/measurement/internal/ae;Ljava/lang/String;Lcom/google/android/gms/measurement/internal/EventParcel;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/ae;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected a(Lcom/google/android/gms/measurement/internal/UserAttributeParcel;)V
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->y()V

    new-instance v0, Lcom/google/android/gms/measurement/internal/ae$5;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/measurement/internal/ae$5;-><init>(Lcom/google/android/gms/measurement/internal/ae;Lcom/google/android/gms/measurement/internal/UserAttributeParcel;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/ae;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b()Z
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->y()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->b:Lcom/google/android/gms/measurement/internal/q;

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public bridge synthetic c()V
    .registers 1

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->c()V

    return-void
.end method

.method public bridge synthetic d()V
    .registers 1

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->d()V

    return-void
.end method

.method public bridge synthetic e()V
    .registers 1

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->e()V

    return-void
.end method

.method public bridge synthetic f()Lcom/google/android/gms/measurement/internal/r;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->f()Lcom/google/android/gms/measurement/internal/r;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic g()Lcom/google/android/gms/measurement/internal/ae;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->g()Lcom/google/android/gms/measurement/internal/ae;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic h()Lcom/google/android/gms/internal/e;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->h()Lcom/google/android/gms/internal/e;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic i()Landroid/content/Context;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->i()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic j()Lcom/google/android/gms/measurement/internal/f;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->j()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic k()Lcom/google/android/gms/measurement/internal/x;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->k()Lcom/google/android/gms/measurement/internal/x;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic l()Lcom/google/android/gms/measurement/internal/t;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic m()Lcom/google/android/gms/measurement/internal/w;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->m()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic n()Lcom/google/android/gms/measurement/internal/h;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    return-object v0
.end method

.method protected o()V
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->y()V

    new-instance v0, Lcom/google/android/gms/measurement/internal/ae$3;

    invoke-direct {v0, p0}, Lcom/google/android/gms/measurement/internal/ae$3;-><init>(Lcom/google/android/gms/measurement/internal/ae;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/ae;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected p()V
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->y()V

    new-instance v0, Lcom/google/android/gms/measurement/internal/ae$6;

    invoke-direct {v0, p0}, Lcom/google/android/gms/measurement/internal/ae$6;-><init>(Lcom/google/android/gms/measurement/internal/ae;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/ae;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public q()V
    .registers 4

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->y()V

    :try_start_6
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ae;->i()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ae;->a:Lcom/google/android/gms/measurement/internal/ae$a;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_13
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_13} :catch_17
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_13} :catch_19

    :goto_13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ae;->b:Lcom/google/android/gms/measurement/internal/q;

    return-void

    :catch_17
    move-exception v0

    goto :goto_13

    :catch_19
    move-exception v0

    goto :goto_13
.end method
