.class abstract Lcom/google/android/gms/measurement/internal/ab;
.super Lcom/google/android/gms/measurement/internal/aa;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/y;)V
    .registers 3

    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/aa;-><init>(Lcom/google/android/gms/measurement/internal/y;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ab;->g:Lcom/google/android/gms/measurement/internal/y;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/measurement/internal/y;->a(Lcom/google/android/gms/measurement/internal/ab;)V

    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method w()Z
    .registers 2

    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/ab;->a:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/ab;->b:Z

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method x()Z
    .registers 2

    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/ab;->c:Z

    return v0
.end method

.method protected y()V
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ab;->w()Z

    move-result v0

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    return-void
.end method

.method public final z()V
    .registers 3

    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/ab;->a:Z

    if-eqz v0, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t initialize twice"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/ab;->a()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ab;->g:Lcom/google/android/gms/measurement/internal/y;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->y()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/ab;->a:Z

    return-void
.end method
