.class Lcom/google/android/gms/measurement/internal/a;
.super Ljava/lang/Object;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field final e:J

.field final f:J

.field final g:Ljava/lang/String;

.field final h:Ljava/lang/String;

.field final i:J

.field final j:J

.field final k:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JJZ)V
    .registers 21

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;

    const-wide/16 v2, 0x0

    cmp-long v2, p5, v2

    if-ltz v2, :cond_34

    const/4 v2, 0x1

    :goto_d
    invoke-static {v2}, Lcom/google/android/gms/common/internal/p;->b(Z)V

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/a;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/measurement/internal/a;->b:Ljava/lang/String;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1b

    const/4 p3, 0x0

    :cond_1b
    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/a;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/measurement/internal/a;->d:Ljava/lang/String;

    iput-wide p5, p0, Lcom/google/android/gms/measurement/internal/a;->e:J

    iput-wide p7, p0, Lcom/google/android/gms/measurement/internal/a;->f:J

    iput-object p9, p0, Lcom/google/android/gms/measurement/internal/a;->g:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/measurement/internal/a;->h:Ljava/lang/String;

    move-wide/from16 v0, p11

    iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/a;->i:J

    move-wide/from16 v0, p13

    iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/a;->j:J

    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/a;->k:Z

    return-void

    :cond_34
    const/4 v2, 0x0

    goto :goto_d
.end method


# virtual methods
.method public a(J)Lcom/google/android/gms/measurement/internal/a;
    .registers 22

    new-instance v3, Lcom/google/android/gms/measurement/internal/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/a;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/measurement/internal/a;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/measurement/internal/a;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/measurement/internal/a;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/measurement/internal/a;->e:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/measurement/internal/a;->f:J

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/measurement/internal/a;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/measurement/internal/a;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/gms/measurement/internal/a;->i:J

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gms/measurement/internal/a;->k:Z

    move/from16 v18, v0

    move-wide/from16 v16, p1

    invoke-direct/range {v3 .. v18}, Lcom/google/android/gms/measurement/internal/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JJZ)V

    return-object v3
.end method

.method public a(Lcom/google/android/gms/measurement/internal/t;J)Lcom/google/android/gms/measurement/internal/a;
    .registers 24

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/gms/measurement/internal/a;->e:J

    const-wide/16 v4, 0x1

    add-long v8, v2, v4

    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v8, v2

    if-lez v2, :cond_1d

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/measurement/internal/t;->o()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v2

    const-string v3, "Bundle index overflow"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    const-wide/16 v8, 0x0

    :cond_1d
    new-instance v3, Lcom/google/android/gms/measurement/internal/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/a;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/measurement/internal/a;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/measurement/internal/a;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/measurement/internal/a;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/measurement/internal/a;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/measurement/internal/a;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/gms/measurement/internal/a;->i:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/measurement/internal/a;->j:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gms/measurement/internal/a;->k:Z

    move/from16 v18, v0

    move-wide/from16 v10, p2

    invoke-direct/range {v3 .. v18}, Lcom/google/android/gms/measurement/internal/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JJZ)V

    return-object v3
.end method

.method public a(Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/a;
    .registers 24

    new-instance v3, Lcom/google/android/gms/measurement/internal/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/a;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/measurement/internal/a;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/measurement/internal/a;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/measurement/internal/a;->e:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/measurement/internal/a;->f:J

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/measurement/internal/a;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/measurement/internal/a;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/measurement/internal/a;->j:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gms/measurement/internal/a;->k:Z

    move/from16 v18, v0

    move-object/from16 v6, p1

    move-wide/from16 v14, p2

    invoke-direct/range {v3 .. v18}, Lcom/google/android/gms/measurement/internal/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JJZ)V

    return-object v3
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/a;
    .registers 22

    new-instance v3, Lcom/google/android/gms/measurement/internal/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/a;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/measurement/internal/a;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/measurement/internal/a;->e:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/measurement/internal/a;->f:J

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/measurement/internal/a;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/measurement/internal/a;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/gms/measurement/internal/a;->i:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/measurement/internal/a;->j:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gms/measurement/internal/a;->k:Z

    move/from16 v18, v0

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    invoke-direct/range {v3 .. v18}, Lcom/google/android/gms/measurement/internal/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JJZ)V

    return-object v3
.end method

.method public a(Z)Lcom/google/android/gms/measurement/internal/a;
    .registers 21

    new-instance v3, Lcom/google/android/gms/measurement/internal/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/a;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/measurement/internal/a;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/measurement/internal/a;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/measurement/internal/a;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/measurement/internal/a;->e:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/measurement/internal/a;->f:J

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/measurement/internal/a;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/measurement/internal/a;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/gms/measurement/internal/a;->i:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/measurement/internal/a;->j:J

    move-wide/from16 v16, v0

    move/from16 v18, p1

    invoke-direct/range {v3 .. v18}, Lcom/google/android/gms/measurement/internal/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JJZ)V

    return-object v3
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/a;
    .registers 22

    new-instance v3, Lcom/google/android/gms/measurement/internal/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/a;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/measurement/internal/a;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/measurement/internal/a;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/measurement/internal/a;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/measurement/internal/a;->e:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/measurement/internal/a;->f:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/gms/measurement/internal/a;->i:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/measurement/internal/a;->j:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gms/measurement/internal/a;->k:Z

    move/from16 v18, v0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    invoke-direct/range {v3 .. v18}, Lcom/google/android/gms/measurement/internal/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JJZ)V

    return-object v3
.end method
