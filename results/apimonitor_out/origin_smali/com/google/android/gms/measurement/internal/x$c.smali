.class final Lcom/google/android/gms/measurement/internal/x$c;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/measurement/internal/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/measurement/internal/x;

.field private final b:Ljava/lang/Object;

.field private final c:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/util/concurrent/FutureTask",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/gms/measurement/internal/x;Ljava/lang/String;Ljava/util/concurrent/BlockingQueue;)V
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/util/concurrent/FutureTask",
            "<*>;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/x$c;->a:Lcom/google/android/gms/measurement/internal/x;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    invoke-static {p2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->b:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/gms/measurement/internal/x$c;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-virtual {p0, p2}, Lcom/google/android/gms/measurement/internal/x$c;->setName(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/InterruptedException;)V
    .registers 5

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->a:Lcom/google/android/gms/measurement/internal/x;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/x;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->o()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/x$c;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was interrupted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/x$c;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public run()V
    .registers 5

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/FutureTask;

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->run()V

    goto :goto_0

    :cond_e
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/x$c;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_11
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_28

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->a:Lcom/google/android/gms/measurement/internal/x;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/x;->a(Lcom/google/android/gms/measurement/internal/x;)Z
    :try_end_1e
    .catchall {:try_start_11 .. :try_end_1e} :catchall_5f

    move-result v0

    if-nez v0, :cond_28

    :try_start_21
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->b:Ljava/lang/Object;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_28
    .catchall {:try_start_21 .. :try_end_28} :catchall_5f
    .catch Ljava/lang/InterruptedException; {:try_start_21 .. :try_end_28} :catch_5a

    :cond_28
    :goto_28
    :try_start_28
    monitor-exit v1
    :try_end_29
    .catchall {:try_start_28 .. :try_end_29} :catchall_5f

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->a:Lcom/google/android/gms/measurement/internal/x;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/x;->b(Lcom/google/android/gms/measurement/internal/x;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_30
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_84

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->a:Lcom/google/android/gms/measurement/internal/x;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/x;->c(Lcom/google/android/gms/measurement/internal/x;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->a:Lcom/google/android/gms/measurement/internal/x;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/x;->b(Lcom/google/android/gms/measurement/internal/x;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->a:Lcom/google/android/gms/measurement/internal/x;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/x;->d(Lcom/google/android/gms/measurement/internal/x;)Lcom/google/android/gms/measurement/internal/x$c;

    move-result-object v0

    if-ne p0, v0, :cond_62

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->a:Lcom/google/android/gms/measurement/internal/x;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/gms/measurement/internal/x;->a(Lcom/google/android/gms/measurement/internal/x;Lcom/google/android/gms/measurement/internal/x$c;)Lcom/google/android/gms/measurement/internal/x$c;

    :goto_58
    monitor-exit v1
    :try_end_59
    .catchall {:try_start_30 .. :try_end_59} :catchall_71

    return-void

    :catch_5a
    move-exception v0

    :try_start_5b
    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/x$c;->a(Ljava/lang/InterruptedException;)V

    goto :goto_28

    :catchall_5f
    move-exception v0

    monitor-exit v1
    :try_end_61
    .catchall {:try_start_5b .. :try_end_61} :catchall_5f

    throw v0

    :cond_62
    :try_start_62
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->a:Lcom/google/android/gms/measurement/internal/x;

    invoke-static {v0}, Lcom/google/android/gms/measurement/internal/x;->e(Lcom/google/android/gms/measurement/internal/x;)Lcom/google/android/gms/measurement/internal/x$c;

    move-result-object v0

    if-ne p0, v0, :cond_74

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->a:Lcom/google/android/gms/measurement/internal/x;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/gms/measurement/internal/x;->b(Lcom/google/android/gms/measurement/internal/x;Lcom/google/android/gms/measurement/internal/x$c;)Lcom/google/android/gms/measurement/internal/x$c;

    goto :goto_58

    :catchall_71
    move-exception v0

    monitor-exit v1
    :try_end_73
    .catchall {:try_start_62 .. :try_end_73} :catchall_71

    throw v0

    :cond_74
    :try_start_74
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$c;->a:Lcom/google/android/gms/measurement/internal/x;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/x;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v2, "Current scheduler thread is neither worker nor network"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    goto :goto_58

    :cond_84
    monitor-exit v1
    :try_end_85
    .catchall {:try_start_74 .. :try_end_85} :catchall_71

    goto/16 :goto_0
.end method
