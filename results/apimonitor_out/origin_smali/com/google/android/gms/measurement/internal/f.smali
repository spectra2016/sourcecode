.class public Lcom/google/android/gms/measurement/internal/f;
.super Lcom/google/android/gms/measurement/internal/aa;


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/y;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/aa;-><init>(Lcom/google/android/gms/measurement/internal/y;)V

    return-void
.end method

.method private a(ILjava/lang/Object;Z)Ljava/lang/Object;
    .registers 6

    const/4 v0, 0x0

    if-nez p2, :cond_5

    move-object p2, v0

    :cond_4
    :goto_4
    return-object p2

    :cond_5
    instance-of v1, p2, Ljava/lang/Long;

    if-nez v1, :cond_4

    instance-of v1, p2, Ljava/lang/Float;

    if-nez v1, :cond_4

    instance-of v1, p2, Ljava/lang/Integer;

    if-eqz v1, :cond_1d

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    goto :goto_4

    :cond_1d
    instance-of v1, p2, Ljava/lang/Byte;

    if-eqz v1, :cond_2d

    check-cast p2, Ljava/lang/Byte;

    invoke-virtual {p2}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    goto :goto_4

    :cond_2d
    instance-of v1, p2, Ljava/lang/Short;

    if-eqz v1, :cond_3d

    check-cast p2, Ljava/lang/Short;

    invoke-virtual {p2}, Ljava/lang/Short;->shortValue()S

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    goto :goto_4

    :cond_3d
    instance-of v1, p2, Ljava/lang/Boolean;

    if-eqz v1, :cond_53

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_50

    const-wide/16 v0, 0x1

    :goto_4b
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    goto :goto_4

    :cond_50
    const-wide/16 v0, 0x0

    goto :goto_4b

    :cond_53
    instance-of v1, p2, Ljava/lang/Double;

    if-eqz v1, :cond_63

    check-cast p2, Ljava/lang/Double;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    double-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    goto :goto_4

    :cond_63
    instance-of v1, p2, Ljava/lang/String;

    if-nez v1, :cond_6f

    instance-of v1, p2, Ljava/lang/Character;

    if-nez v1, :cond_6f

    instance-of v1, p2, Ljava/lang/CharSequence;

    if-eqz v1, :cond_83

    :cond_6f
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, p1, :cond_4

    if-eqz p3, :cond_81

    const/4 v0, 0x0

    invoke-virtual {p2, v0, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    goto :goto_4

    :cond_81
    move-object p2, v0

    goto :goto_4

    :cond_83
    move-object p2, v0

    goto :goto_4
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    .registers 9

    if-nez p4, :cond_25

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->q()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " value can\'t be null. Ignoring "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_24
    :goto_24
    return-void

    :cond_25
    instance-of v0, p4, Ljava/lang/Long;

    if-nez v0, :cond_24

    instance-of v0, p4, Ljava/lang/Float;

    if-nez v0, :cond_24

    instance-of v0, p4, Ljava/lang/Integer;

    if-nez v0, :cond_24

    instance-of v0, p4, Ljava/lang/Byte;

    if-nez v0, :cond_24

    instance-of v0, p4, Ljava/lang/Short;

    if-nez v0, :cond_24

    instance-of v0, p4, Ljava/lang/Boolean;

    if-nez v0, :cond_24

    instance-of v0, p4, Ljava/lang/Double;

    if-nez v0, :cond_24

    instance-of v0, p4, Ljava/lang/String;

    if-nez v0, :cond_4d

    instance-of v0, p4, Ljava/lang/Character;

    if-nez v0, :cond_4d

    instance-of v0, p4, Ljava/lang/CharSequence;

    if-eqz v0, :cond_24

    :cond_4d
    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, p3, :cond_24

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->q()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignoring "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Value is too long. name, value length"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, p2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_24
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Class;)Z
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Service;",
            ">;)Z"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, p0, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;

    move-result-object v0

    if-eqz v0, :cond_17

    iget-boolean v0, v0, Landroid/content/pm/ServiceInfo;->enabled:Z
    :try_end_12
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_12} :catch_16

    if-eqz v0, :cond_17

    const/4 v0, 0x1

    :goto_15
    return v0

    :catch_16
    move-exception v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Class;Z)Z
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/content/BroadcastReceiver;",
            ">;Z)Z"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, p0, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getReceiverInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    if-eqz v0, :cond_1d

    iget-boolean v1, v0, Landroid/content/pm/ActivityInfo;->enabled:Z

    if-eqz v1, :cond_1d

    if-eqz p2, :cond_1a

    iget-boolean v0, v0, Landroid/content/pm/ActivityInfo;->exported:Z
    :try_end_18
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_18} :catch_1c

    if-eqz v0, :cond_1d

    :cond_1a
    const/4 v0, 0x1

    :goto_1b
    return v0

    :catch_1c
    move-exception v0

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method private e(Ljava/lang/String;)I
    .registers 3

    const-string v0, "_ldl"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->s()I

    move-result v0

    :goto_10
    return v0

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->r()I

    move-result v0

    goto :goto_10
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1c

    const-string v0, "_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->q()I

    move-result v0

    :goto_16
    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/gms/measurement/internal/f;->a(ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_1c
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->p()I

    move-result v0

    goto :goto_16
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V
    .registers 7

    instance-of v0, p3, Ljava/lang/Long;

    if-eqz v0, :cond_e

    check-cast p3, Ljava/lang/Long;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_d
    :goto_d
    return-void

    :cond_e
    instance-of v0, p3, Ljava/lang/Float;

    if-eqz v0, :cond_1c

    check-cast p3, Ljava/lang/Float;

    invoke-virtual {p3}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    goto :goto_d

    :cond_1c
    instance-of v0, p3, Ljava/lang/String;

    if-eqz v0, :cond_28

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_d

    :cond_28
    if-eqz p2, :cond_d

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->q()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Not putting event parameter. Invalid value type. name, type"

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p2, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_d
.end method

.method public a(Lcom/google/android/gms/internal/j$b;Ljava/lang/Object;)V
    .registers 5

    const/4 v0, 0x0

    invoke-static {p2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v0, p1, Lcom/google/android/gms/internal/j$b;->b:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/android/gms/internal/j$b;->c:Ljava/lang/Long;

    iput-object v0, p1, Lcom/google/android/gms/internal/j$b;->d:Ljava/lang/Float;

    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_13

    check-cast p2, Ljava/lang/String;

    iput-object p2, p1, Lcom/google/android/gms/internal/j$b;->b:Ljava/lang/String;

    :goto_12
    return-void

    :cond_13
    instance-of v0, p2, Ljava/lang/Long;

    if-eqz v0, :cond_1c

    check-cast p2, Ljava/lang/Long;

    iput-object p2, p1, Lcom/google/android/gms/internal/j$b;->c:Ljava/lang/Long;

    goto :goto_12

    :cond_1c
    instance-of v0, p2, Ljava/lang/Float;

    if-eqz v0, :cond_25

    check-cast p2, Ljava/lang/Float;

    iput-object p2, p1, Lcom/google/android/gms/internal/j$b;->d:Ljava/lang/Float;

    goto :goto_12

    :cond_25
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Ignoring invalid (type) event param value"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_12
.end method

.method public a(Lcom/google/android/gms/internal/j$e;Ljava/lang/Object;)V
    .registers 5

    const/4 v0, 0x0

    invoke-static {p2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v0, p1, Lcom/google/android/gms/internal/j$e;->c:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/android/gms/internal/j$e;->d:Ljava/lang/Long;

    iput-object v0, p1, Lcom/google/android/gms/internal/j$e;->e:Ljava/lang/Float;

    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_13

    check-cast p2, Ljava/lang/String;

    iput-object p2, p1, Lcom/google/android/gms/internal/j$e;->c:Ljava/lang/String;

    :goto_12
    return-void

    :cond_13
    instance-of v0, p2, Ljava/lang/Long;

    if-eqz v0, :cond_1c

    check-cast p2, Ljava/lang/Long;

    iput-object p2, p1, Lcom/google/android/gms/internal/j$e;->d:Ljava/lang/Long;

    goto :goto_12

    :cond_1c
    instance-of v0, p2, Ljava/lang/Float;

    if-eqz v0, :cond_25

    check-cast p2, Ljava/lang/Float;

    iput-object p2, p1, Lcom/google/android/gms/internal/j$e;->e:Ljava/lang/Float;

    goto :goto_12

    :cond_25
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Ignoring invalid (type) user attribute value"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_12
.end method

.method public a(Ljava/lang/String;)V
    .registers 4

    const-string v0, "event"

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/h;->b()I

    move-result v1

    invoke-virtual {p0, v0, v1, p1}, Lcom/google/android/gms/measurement/internal/f;->a(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method a(Ljava/lang/String;ILjava/lang/String;)V
    .registers 7

    const/16 v2, 0x5f

    if-nez p3, :cond_1d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " name is required and can\'t be null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1d
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3c

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " name is required and can\'t be empty"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3c
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v1

    if-nez v1, :cond_62

    if-eq v0, v2, :cond_62

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " name must start with a letter or _"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_62
    const/4 v0, 0x1

    :goto_63
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_91

    invoke-virtual {p3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-eq v1, v2, :cond_8e

    invoke-static {v1}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v1

    if-nez v1, :cond_8e

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " name must consist of letters, digits or _ (underscores)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8e
    add-int/lit8 v0, v0, 0x1

    goto :goto_63

    :cond_91
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, p2, :cond_b4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " name is too long. The maximum supported length is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b4
    return-void
.end method

.method public a(JJ)Z
    .registers 10

    const-wide/16 v2, 0x0

    const/4 v0, 0x1

    cmp-long v1, p1, v2

    if-eqz v1, :cond_b

    cmp-long v1, p3, v2

    if-gtz v1, :cond_c

    :cond_b
    :goto_b
    return v0

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->h()Lcom/google/android/gms/internal/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/internal/e;->a()J

    move-result-wide v2

    sub-long/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    cmp-long v1, v2, p3

    if-gtz v1, :cond_b

    const/4 v0, 0x0

    goto :goto_b
.end method

.method public a(Lcom/google/android/gms/internal/j$c;)[B
    .registers 5

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/j$c;->e()I

    move-result v0

    new-array v0, v0, [B

    invoke-static {v0}, Lcom/google/android/gms/internal/zztd;->a([B)Lcom/google/android/gms/internal/zztd;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/gms/internal/j$c;->a(Lcom/google/android/gms/internal/zztd;)V

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zztd;->b()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_10} :catch_11

    :goto_10
    return-object v0

    :catch_11
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v1

    const-string v2, "Data loss. Failed to serialize batch"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v0, 0x0

    goto :goto_10
.end method

.method public a([B)[B
    .registers 5

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v1, v0}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v1, p1}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_16} :catch_18

    move-result-object v0

    return-object v0

    :catch_18
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v1

    const-string v2, "Failed to gzip content"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    throw v0
.end method

.method public b(Ljava/lang/String;)V
    .registers 4

    const-string v0, "user attribute"

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/h;->o()I

    move-result v1

    invoke-virtual {p0, v0, v1, p1}, Lcom/google/android/gms/measurement/internal/f;->a(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 5

    const-string v0, "_ldl"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "user attribute referrer"

    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/f;->e(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, v0, p1, v1, p2}, Lcom/google/android/gms/measurement/internal/f;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    :goto_11
    return-void

    :cond_12
    const-string v0, "user attribute"

    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/f;->e(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, v0, p1, v1, p2}, Lcom/google/android/gms/measurement/internal/f;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    goto :goto_11
.end method

.method public b([B)[B
    .registers 8

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v1, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v1, v0}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v3, 0x400

    new-array v3, v3, [B

    :goto_13
    invoke-virtual {v1, v3}, Ljava/util/zip/GZIPInputStream;->read([B)I

    move-result v4

    if-gtz v4, :cond_24

    invoke-virtual {v1}, Ljava/util/zip/GZIPInputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->close()V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    :cond_24
    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_28
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_28} :catch_29

    goto :goto_13

    :catch_29
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v1

    const-string v2, "Failed to ungzip content"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    throw v0
.end method

.method public c(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    const-string v0, "_ldl"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/f;->e(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/gms/measurement/internal/f;->a(ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    :goto_11
    return-object v0

    :cond_12
    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/f;->e(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/gms/measurement/internal/f;->a(ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    goto :goto_11
.end method

.method public bridge synthetic c()V
    .registers 1

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->c()V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .registers 4

    const-string v0, "event param"

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/h;->o()I

    move-result v1

    invoke-virtual {p0, v0, v1, p1}, Lcom/google/android/gms/measurement/internal/f;->a(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public bridge synthetic d()V
    .registers 1

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->d()V

    return-void
.end method

.method public d(Ljava/lang/String;)Z
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/f;->i()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_f

    const/4 v0, 0x1

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public bridge synthetic e()V
    .registers 1

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->e()V

    return-void
.end method

.method public bridge synthetic f()Lcom/google/android/gms/measurement/internal/r;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->f()Lcom/google/android/gms/measurement/internal/r;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic g()Lcom/google/android/gms/measurement/internal/ae;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->g()Lcom/google/android/gms/measurement/internal/ae;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic h()Lcom/google/android/gms/internal/e;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->h()Lcom/google/android/gms/internal/e;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic i()Landroid/content/Context;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->i()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic j()Lcom/google/android/gms/measurement/internal/f;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->j()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic k()Lcom/google/android/gms/measurement/internal/x;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->k()Lcom/google/android/gms/measurement/internal/x;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic l()Lcom/google/android/gms/measurement/internal/t;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->l()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic m()Lcom/google/android/gms/measurement/internal/w;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->m()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic n()Lcom/google/android/gms/measurement/internal/h;
    .registers 2

    invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->n()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    return-object v0
.end method
