.class public Lcom/google/android/gms/measurement/internal/y;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/google/android/gms/measurement/internal/ac;

.field private static volatile b:Lcom/google/android/gms/measurement/internal/y;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/measurement/internal/h;

.field private final e:Lcom/google/android/gms/measurement/internal/w;

.field private final f:Lcom/google/android/gms/measurement/internal/t;

.field private final g:Lcom/google/android/gms/measurement/internal/x;

.field private final h:Lcom/google/android/gms/measurement/a;

.field private final i:Lcom/google/android/gms/measurement/internal/f;

.field private final j:Lcom/google/android/gms/measurement/internal/i;

.field private final k:Lcom/google/android/gms/measurement/internal/u;

.field private final l:Lcom/google/android/gms/internal/e;

.field private final m:Lcom/google/android/gms/measurement/internal/ae;

.field private final n:Lcom/google/android/gms/measurement/internal/k;

.field private final o:Lcom/google/android/gms/measurement/internal/ad;

.field private final p:Lcom/google/android/gms/measurement/internal/r;

.field private final q:Lcom/google/android/gms/measurement/internal/v;

.field private final r:Lcom/google/android/gms/measurement/internal/c;

.field private final s:Z

.field private t:Ljava/lang/Boolean;

.field private u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private v:I

.field private w:I


# direct methods
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ac;)V
    .registers 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/android/gms/measurement/internal/ac;->a:Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->c:Landroid/content/Context;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->j(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/internal/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->l:Lcom/google/android/gms/internal/e;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->a(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->d:Lcom/google/android/gms/measurement/internal/h;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->b(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/internal/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/w;->z()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->e:Lcom/google/android/gms/measurement/internal/w;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->c(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->z()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->f:Lcom/google/android/gms/measurement/internal/t;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->g(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/internal/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->i:Lcom/google/android/gms/measurement/internal/f;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->l(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/internal/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/k;->z()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->n:Lcom/google/android/gms/measurement/internal/k;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->m(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/internal/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/r;->z()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->p:Lcom/google/android/gms/measurement/internal/r;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->h(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->z()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->j:Lcom/google/android/gms/measurement/internal/i;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->i(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/internal/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/u;->z()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->k:Lcom/google/android/gms/measurement/internal/u;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->k(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/internal/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->z()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->m:Lcom/google/android/gms/measurement/internal/ae;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->f(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/internal/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ad;->z()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->o:Lcom/google/android/gms/measurement/internal/ad;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->o(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/c;->z()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->r:Lcom/google/android/gms/measurement/internal/c;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->n(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/internal/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->q:Lcom/google/android/gms/measurement/internal/v;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->e(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->h:Lcom/google/android/gms/measurement/a;

    invoke-virtual {p1, p0}, Lcom/google/android/gms/measurement/internal/ac;->d(Lcom/google/android/gms/measurement/internal/y;)Lcom/google/android/gms/measurement/internal/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/x;->z()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->g:Lcom/google/android/gms/measurement/internal/x;

    iget v0, p0, Lcom/google/android/gms/measurement/internal/y;->v:I

    iget v1, p0, Lcom/google/android/gms/measurement/internal/y;->w:I

    if-eq v0, v1, :cond_a1

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Not all components initialized"

    iget v2, p0, Lcom/google/android/gms/measurement/internal/y;->v:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/measurement/internal/y;->w:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_a1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/y;->s:Z

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->d:Lcom/google/android/gms/measurement/internal/h;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->C()Z

    move-result v0

    if-nez v0, :cond_c9

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->v()Z

    move-result v0

    if-nez v0, :cond_c9

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Application;

    if-eqz v0, :cond_e2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_d4

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->i()Lcom/google/android/gms/measurement/internal/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ad;->b()V

    :cond_c9
    :goto_c9
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->g:Lcom/google/android/gms/measurement/internal/x;

    new-instance v1, Lcom/google/android/gms/measurement/internal/y$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/measurement/internal/y$1;-><init>(Lcom/google/android/gms/measurement/internal/y;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V

    return-void

    :cond_d4
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->s()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Not tracking deep linking pre-ICS"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    goto :goto_c9

    :cond_e2
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->o()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Application context is not an Application"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    goto :goto_c9
.end method

.method private A()Z
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private B()V
    .registers 11

    const-wide/16 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->u()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->a()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->b()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/y;->A()Z

    move-result v0

    if-nez v0, :cond_23

    :cond_14
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->s()Lcom/google/android/gms/measurement/internal/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/v;->b()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->t()Lcom/google/android/gms/measurement/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/c;->b()V

    :goto_22
    return-void

    :cond_23
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/y;->C()J

    move-result-wide v0

    cmp-long v2, v0, v8

    if-nez v2, :cond_3a

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->s()Lcom/google/android/gms/measurement/internal/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/v;->b()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->t()Lcom/google/android/gms/measurement/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/c;->b()V

    goto :goto_22

    :cond_3a
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->m()Lcom/google/android/gms/measurement/internal/u;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/u;->b()Z

    move-result v2

    if-nez v2, :cond_53

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->s()Lcom/google/android/gms/measurement/internal/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/v;->a()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->t()Lcom/google/android/gms/measurement/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/c;->b()V

    goto :goto_22

    :cond_53
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/w;->e:Lcom/google/android/gms/measurement/internal/w$a;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/w$a;->a()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/h;->L()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->k()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v6

    invoke-virtual {v6, v2, v3, v4, v5}, Lcom/google/android/gms/measurement/internal/f;->a(JJ)Z

    move-result v6

    if-nez v6, :cond_74

    add-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    :cond_74
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->s()Lcom/google/android/gms/measurement/internal/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/v;->b()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->o()Lcom/google/android/gms/internal/e;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/internal/e;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    cmp-long v2, v0, v8

    if-gtz v2, :cond_92

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->t()Lcom/google/android/gms/measurement/internal/c;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/c;->a(J)V

    goto :goto_22

    :cond_92
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v2

    const-string v3, "Upload scheduled in approximately ms"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->t()Lcom/google/android/gms/measurement/internal/c;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/measurement/internal/c;->a(J)V

    goto/16 :goto_22
.end method

.method private C()J
    .registers 16

    const-wide/16 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->o()Lcom/google/android/gms/internal/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/e;->a()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/h;->O()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/measurement/internal/h;->M()J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v8

    iget-object v8, v8, Lcom/google/android/gms/measurement/internal/w;->c:Lcom/google/android/gms/measurement/internal/w$a;

    invoke-virtual {v8}, Lcom/google/android/gms/measurement/internal/w$a;->a()J

    move-result-wide v8

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v10

    iget-object v10, v10, Lcom/google/android/gms/measurement/internal/w;->d:Lcom/google/android/gms/measurement/internal/w$a;

    invoke-virtual {v10}, Lcom/google/android/gms/measurement/internal/w$a;->a()J

    move-result-wide v10

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/gms/measurement/internal/i;->u()J

    move-result-wide v12

    cmp-long v14, v12, v4

    if-nez v14, :cond_3c

    move-wide v2, v4

    :cond_3b
    :goto_3b
    return-wide v2

    :cond_3c
    sub-long/2addr v12, v0

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(J)J

    move-result-wide v12

    sub-long/2addr v0, v12

    add-long/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->k()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v12

    invoke-virtual {v12, v8, v9, v6, v7}, Lcom/google/android/gms/measurement/internal/f;->a(JJ)Z

    move-result v12

    if-nez v12, :cond_4f

    add-long v2, v8, v6

    :cond_4f
    cmp-long v6, v10, v4

    if-eqz v6, :cond_3b

    cmp-long v0, v10, v0

    if-ltz v0, :cond_3b

    const/4 v0, 0x0

    :goto_58
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/h;->Q()I

    move-result v1

    if-ge v0, v1, :cond_76

    const/4 v1, 0x1

    shl-int/2addr v1, v0

    int-to-long v6, v1

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/h;->P()J

    move-result-wide v8

    mul-long/2addr v6, v8

    add-long/2addr v2, v6

    cmp-long v1, v2, v10

    if-gtz v1, :cond_3b

    add-int/lit8 v0, v0, 0x1

    goto :goto_58

    :cond_76
    move-wide v2, v4

    goto :goto_3b
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/measurement/internal/y;
    .registers 3

    invoke-static {p0}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/measurement/internal/y;->b:Lcom/google/android/gms/measurement/internal/y;

    if-nez v0, :cond_22

    const-class v1, Lcom/google/android/gms/measurement/internal/y;

    monitor-enter v1

    :try_start_11
    sget-object v0, Lcom/google/android/gms/measurement/internal/y;->b:Lcom/google/android/gms/measurement/internal/y;

    if-nez v0, :cond_21

    sget-object v0, Lcom/google/android/gms/measurement/internal/y;->a:Lcom/google/android/gms/measurement/internal/ac;

    if-eqz v0, :cond_25

    sget-object v0, Lcom/google/android/gms/measurement/internal/y;->a:Lcom/google/android/gms/measurement/internal/ac;

    :goto_1b
    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ac;->a()Lcom/google/android/gms/measurement/internal/y;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/measurement/internal/y;->b:Lcom/google/android/gms/measurement/internal/y;

    :cond_21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_11 .. :try_end_22} :catchall_2b

    :cond_22
    sget-object v0, Lcom/google/android/gms/measurement/internal/y;->b:Lcom/google/android/gms/measurement/internal/y;

    return-object v0

    :cond_25
    :try_start_25
    new-instance v0, Lcom/google/android/gms/measurement/internal/ac;

    invoke-direct {v0, p0}, Lcom/google/android/gms/measurement/internal/ac;-><init>(Landroid/content/Context;)V

    goto :goto_1b

    :catchall_2b
    move-exception v0

    monitor-exit v1
    :try_end_2d
    .catchall {:try_start_25 .. :try_end_2d} :catchall_2b

    throw v0
.end method

.method private a(ILjava/lang/Throwable;[B)V
    .registers 10

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->u()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->a()V

    if-nez p3, :cond_b

    new-array p3, v0, [B

    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/y;->u:Ljava/util/List;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/measurement/internal/y;->u:Ljava/util/List;

    const/16 v2, 0xc8

    if-eq p1, v2, :cond_18

    const/16 v2, 0xcc

    if-ne p1, v2, :cond_a1

    :cond_18
    if-nez p2, :cond_a1

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/w;->c:Lcom/google/android/gms/measurement/internal/w$a;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->o()Lcom/google/android/gms/internal/e;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/internal/e;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/w$a;->a(J)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/w;->d:Lcom/google/android/gms/measurement/internal/w$a;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/w$a;->a(J)V

    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/y;->B()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v2, "Successful upload. Got network response. code, size"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    array-length v4, p3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->b()V

    :try_start_56
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/measurement/internal/i;->a(J)V
    :try_end_71
    .catchall {:try_start_56 .. :try_end_71} :catchall_72

    goto :goto_5a

    :catchall_72
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/i;->p()V

    throw v0

    :cond_7b
    :try_start_7b
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->o()V
    :try_end_82
    .catchall {:try_start_7b .. :try_end_82} :catchall_72

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->p()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->m()Lcom/google/android/gms/measurement/internal/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/u;->b()Z

    move-result v0

    if-eqz v0, :cond_9d

    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/y;->A()Z

    move-result v0

    if-eqz v0, :cond_9d

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->x()V

    :goto_9c
    return-void

    :cond_9d
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/y;->B()V

    goto :goto_9c

    :cond_a1
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v1

    const-string v2, "Network upload failed. Will retry later. code, error"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/w;->d:Lcom/google/android/gms/measurement/internal/w$a;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->o()Lcom/google/android/gms/internal/e;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/internal/e;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/measurement/internal/w$a;->a(J)V

    const/16 v1, 0x1f7

    if-eq p1, v1, :cond_cb

    const/16 v1, 0x1ad

    if-ne p1, v1, :cond_cc

    :cond_cb
    const/4 v0, 0x1

    :cond_cc
    if-eqz v0, :cond_df

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/w;->e:Lcom/google/android/gms/measurement/internal/w$a;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->o()Lcom/google/android/gms/internal/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/internal/e;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/w$a;->a(J)V

    :cond_df
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/y;->B()V

    goto :goto_9c
.end method

.method private a(Lcom/google/android/gms/measurement/internal/aa;)V
    .registers 4

    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Component not created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/measurement/internal/y;ILjava/lang/Throwable;[B)V
    .registers 4

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/measurement/internal/y;->a(ILjava/lang/Throwable;[B)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1c

    const/4 v0, 0x1

    :goto_7
    invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->u:Ljava/util/List;

    if-eqz v0, :cond_1e

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Set uploading progress before finishing the previous upload"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    :goto_1b
    return-void

    :cond_1c
    const/4 v0, 0x0

    goto :goto_7

    :cond_1e
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->u:Ljava/util/List;

    goto :goto_1b
.end method

.method private b(Lcom/google/android/gms/measurement/internal/ab;)V
    .registers 4

    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Component not created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/ab;->w()Z

    move-result v0

    if-nez v0, :cond_18

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Component not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_18
    return-void
.end method

.method private c(Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    .registers 21

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->u()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->a()V

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/measurement/internal/i;->b(Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/a;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/w;->o()Ljava/lang/String;

    move-result-object v7

    const/4 v2, 0x0

    if-nez v3, :cond_e3

    new-instance v3, Lcom/google/android/gms/measurement/internal/a;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/w;->p()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->c:Ljava/lang/String;

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->e:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->f:J

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->g:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->i:Z

    move/from16 v18, v0

    invoke-direct/range {v3 .. v18}, Lcom/google/android/gms/measurement/internal/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JJZ)V

    const/4 v2, 0x1

    :cond_59
    :goto_59
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_86

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->c:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/android/gms/measurement/internal/a;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_79

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->f:J

    iget-wide v6, v3, Lcom/google/android/gms/measurement/internal/a;->i:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_86

    :cond_79
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->f:J

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/gms/measurement/internal/a;->a(Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/a;

    move-result-object v3

    const/4 v2, 0x1

    :cond_86
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b5

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->d:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/android/gms/measurement/internal/a;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a8

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->e:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/android/gms/measurement/internal/a;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b5

    :cond_a8
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->e:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/google/android/gms/measurement/internal/a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/a;

    move-result-object v3

    const/4 v2, 0x1

    :cond_b5
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->g:J

    iget-wide v6, v3, Lcom/google/android/gms/measurement/internal/a;->j:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_c8

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->g:J

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/measurement/internal/a;->a(J)Lcom/google/android/gms/measurement/internal/a;

    move-result-object v3

    const/4 v2, 0x1

    :cond_c8
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->i:Z

    iget-boolean v5, v3, Lcom/google/android/gms/measurement/internal/a;->k:Z

    if-eq v4, v5, :cond_d9

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->i:Z

    invoke-virtual {v3, v2}, Lcom/google/android/gms/measurement/internal/a;->a(Z)Lcom/google/android/gms/measurement/internal/a;

    move-result-object v3

    const/4 v2, 0x1

    :cond_d9
    if-eqz v2, :cond_e2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/gms/measurement/internal/i;->a(Lcom/google/android/gms/measurement/internal/a;)V

    :cond_e2
    return-void

    :cond_e3
    iget-object v4, v3, Lcom/google/android/gms/measurement/internal/a;->d:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_59

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/w;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2, v7}, Lcom/google/android/gms/measurement/internal/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/a;

    move-result-object v3

    const/4 v2, 0x1

    goto/16 :goto_59
.end method

.method private z()Z
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->u()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->u:Ljava/util/List;

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method


# virtual methods
.method a([Lcom/google/android/gms/measurement/internal/l;Lcom/google/android/gms/measurement/internal/AppMetadata;)Lcom/google/android/gms/internal/j$d;
    .registers 23

    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->u()V

    new-instance v19, Lcom/google/android/gms/internal/j$d;

    invoke-direct/range {v19 .. v19}, Lcom/google/android/gms/internal/j$d;-><init>()V

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->a:Ljava/lang/Integer;

    const-string v2, "android"

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->i:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->o:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->e:Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->n:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->d:Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->p:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->q:Ljava/lang/Long;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->c:Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->y:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->g:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_10c

    const/4 v2, 0x0

    :goto_54
    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->v:Ljava/lang/Long;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/w;->b()Landroid/util/Pair;

    move-result-object v3

    if-eqz v3, :cond_7a

    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v2, :cond_7a

    iget-object v2, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v2, :cond_7a

    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->s:Ljava/lang/String;

    iget-object v2, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->t:Ljava/lang/Boolean;

    :cond_7a
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->q()Lcom/google/android/gms/measurement/internal/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/k;->b()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->k:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->q()Lcom/google/android/gms/measurement/internal/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/k;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->j:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->q()Lcom/google/android/gms/measurement/internal/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/k;->p()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->m:Ljava/lang/Integer;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->q()Lcom/google/android/gms/measurement/internal/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/k;->q()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->l:Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->r:Ljava/lang/Long;

    const/4 v2, 0x0

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->d:Ljava/lang/Long;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    iget-wide v2, v2, Lcom/google/android/gms/measurement/internal/l;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->e:Ljava/lang/Long;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    iget-wide v2, v2, Lcom/google/android/gms/measurement/internal/l;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->f:Ljava/lang/Long;

    const/4 v2, 0x1

    :goto_d4
    move-object/from16 v0, p1

    array-length v3, v0

    if-ge v2, v3, :cond_116

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/android/gms/internal/j$d;->e:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aget-object v3, p1, v2

    iget-wide v6, v3, Lcom/google/android/gms/measurement/internal/l;->d:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/google/android/gms/internal/j$d;->e:Ljava/lang/Long;

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/android/gms/internal/j$d;->f:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aget-object v3, p1, v2

    iget-wide v6, v3, Lcom/google/android/gms/measurement/internal/l;->d:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/google/android/gms/internal/j$d;->f:Ljava/lang/Long;

    add-int/lit8 v2, v2, 0x1

    goto :goto_d4

    :cond_10c
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/16 :goto_54

    :cond_116
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/measurement/internal/i;->b(Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/a;

    move-result-object v3

    if-nez v3, :cond_15d

    new-instance v3, Lcom/google/android/gms/measurement/internal/a;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/w;->p()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->c:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/w;->o()Ljava/lang/String;

    move-result-object v7

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->d:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->e:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-wide v14, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->f:J

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->g:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->i:Z

    move/from16 v18, v0

    invoke-direct/range {v3 .. v18}, Lcom/google/android/gms/measurement/internal/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JJZ)V

    :cond_15d
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v2

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/gms/internal/j$d;->f:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/gms/measurement/internal/a;->a(Lcom/google/android/gms/measurement/internal/t;J)Lcom/google/android/gms/measurement/internal/a;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/gms/measurement/internal/i;->a(Lcom/google/android/gms/measurement/internal/a;)V

    iget-object v4, v2, Lcom/google/android/gms/measurement/internal/a;->b:Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v4, v0, Lcom/google/android/gms/internal/j$d;->u:Ljava/lang/String;

    iget-wide v4, v2, Lcom/google/android/gms/measurement/internal/a;->e:J

    long-to-int v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->w:Ljava/lang/Integer;

    iget-wide v4, v3, Lcom/google/android/gms/measurement/internal/a;->f:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-nez v2, :cond_1ee

    const/4 v2, 0x0

    :goto_18e
    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->h:Ljava/lang/Long;

    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/android/gms/internal/j$d;->h:Ljava/lang/Long;

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->g:Ljava/lang/Long;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/measurement/internal/i;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/gms/internal/j$e;

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->c:[Lcom/google/android/gms/internal/j$e;

    const/4 v2, 0x0

    move v3, v2

    :goto_1b2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_1f5

    new-instance v5, Lcom/google/android/gms/internal/j$e;

    invoke-direct {v5}, Lcom/google/android/gms/internal/j$e;-><init>()V

    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/android/gms/internal/j$d;->c:[Lcom/google/android/gms/internal/j$e;

    aput-object v5, v2, v3

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/measurement/internal/d;

    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/d;->b:Ljava/lang/String;

    iput-object v2, v5, Lcom/google/android/gms/internal/j$e;->b:Ljava/lang/String;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/measurement/internal/d;

    iget-wide v6, v2, Lcom/google/android/gms/measurement/internal/d;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v5, Lcom/google/android/gms/internal/j$e;->a:Ljava/lang/Long;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->k()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v6

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/measurement/internal/d;

    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/d;->d:Ljava/lang/Object;

    invoke-virtual {v6, v5, v2}, Lcom/google/android/gms/measurement/internal/f;->a(Lcom/google/android/gms/internal/j$e;Ljava/lang/Object;)V

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1b2

    :cond_1ee
    iget-wide v2, v3, Lcom/google/android/gms/measurement/internal/a;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_18e

    :cond_1f5
    move-object/from16 v0, p1

    array-length v2, v0

    new-array v2, v2, [Lcom/google/android/gms/internal/j$a;

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->b:[Lcom/google/android/gms/internal/j$a;

    const/4 v2, 0x0

    move v3, v2

    :goto_200
    move-object/from16 v0, p1

    array-length v2, v0

    if-ge v3, v2, :cond_264

    new-instance v6, Lcom/google/android/gms/internal/j$a;

    invoke-direct {v6}, Lcom/google/android/gms/internal/j$a;-><init>()V

    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/android/gms/internal/j$d;->b:[Lcom/google/android/gms/internal/j$a;

    aput-object v6, v2, v3

    aget-object v2, p1, v3

    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/l;->b:Ljava/lang/String;

    iput-object v2, v6, Lcom/google/android/gms/internal/j$a;->b:Ljava/lang/String;

    aget-object v2, p1, v3

    iget-wide v4, v2, Lcom/google/android/gms/measurement/internal/l;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v6, Lcom/google/android/gms/internal/j$a;->c:Ljava/lang/Long;

    aget-object v2, p1, v3

    iget-object v2, v2, Lcom/google/android/gms/measurement/internal/l;->f:Lcom/google/android/gms/measurement/internal/EventParams;

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/EventParams;->a()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/gms/internal/j$b;

    iput-object v2, v6, Lcom/google/android/gms/internal/j$a;->a:[Lcom/google/android/gms/internal/j$b;

    const/4 v2, 0x0

    aget-object v4, p1, v3

    iget-object v4, v4, Lcom/google/android/gms/measurement/internal/l;->f:Lcom/google/android/gms/measurement/internal/EventParams;

    invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/EventParams;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v4, v2

    :goto_236
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_260

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v8, Lcom/google/android/gms/internal/j$b;

    invoke-direct {v8}, Lcom/google/android/gms/internal/j$b;-><init>()V

    iget-object v9, v6, Lcom/google/android/gms/internal/j$a;->a:[Lcom/google/android/gms/internal/j$b;

    add-int/lit8 v5, v4, 0x1

    aput-object v8, v9, v4

    iput-object v2, v8, Lcom/google/android/gms/internal/j$b;->a:Ljava/lang/String;

    aget-object v4, p1, v3

    iget-object v4, v4, Lcom/google/android/gms/measurement/internal/l;->f:Lcom/google/android/gms/measurement/internal/EventParams;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/measurement/internal/EventParams;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->k()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v4

    invoke-virtual {v4, v8, v2}, Lcom/google/android/gms/measurement/internal/f;->a(Lcom/google/android/gms/internal/j$b;Ljava/lang/Object;)V

    move v4, v5

    goto :goto_236

    :cond_260
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_200

    :cond_264
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/t;->u()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    iput-object v2, v0, Lcom/google/android/gms/internal/j$d;->x:Ljava/lang/String;

    return-object v19
.end method

.method a()V
    .registers 3

    iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/y;->s:Z

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "AppMeasurement is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    return-void
.end method

.method a(Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->u()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->a()V

    iget-object v0, p1, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/y;->c(Lcom/google/android/gms/measurement/internal/AppMetadata;)V

    return-void
.end method

.method a(Lcom/google/android/gms/measurement/internal/EventParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    .registers 15

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->u()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->a()V

    iget-object v0, p2, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p2, Lcom/google/android/gms/measurement/internal/AppMetadata;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_14

    :goto_13
    return-void

    :cond_14
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Logging event"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/measurement/internal/l;

    iget-object v3, p1, Lcom/google/android/gms/measurement/internal/EventParcel;->d:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/gms/measurement/internal/EventParcel;->b:Ljava/lang/String;

    iget-wide v6, p1, Lcom/google/android/gms/measurement/internal/EventParcel;->e:J

    const-wide/16 v8, 0x0

    iget-object v0, p1, Lcom/google/android/gms/measurement/internal/EventParcel;->c:Lcom/google/android/gms/measurement/internal/EventParams;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/EventParams;->b()Landroid/os/Bundle;

    move-result-object v10

    move-object v2, p0

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/measurement/internal/l;-><init>(Lcom/google/android/gms/measurement/internal/y;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLandroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->b()V

    :try_start_3e
    invoke-direct {p0, p2}, Lcom/google/android/gms/measurement/internal/y;->c(Lcom/google/android/gms/measurement/internal/AppMetadata;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    iget-object v2, p2, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/android/gms/measurement/internal/l;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/i;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/m;

    move-result-object v0

    if-nez v0, :cond_96

    new-instance v3, Lcom/google/android/gms/measurement/internal/m;

    iget-object v4, p2, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    iget-object v5, v1, Lcom/google/android/gms/measurement/internal/l;->b:Ljava/lang/String;

    const-wide/16 v6, 0x1

    const-wide/16 v8, 0x1

    iget-wide v10, v1, Lcom/google/android/gms/measurement/internal/l;->d:J

    invoke-direct/range {v3 .. v11}, Lcom/google/android/gms/measurement/internal/m;-><init>(Ljava/lang/String;Ljava/lang/String;JJJ)V

    :goto_5e
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/measurement/internal/i;->a(Lcom/google/android/gms/measurement/internal/m;)V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/gms/measurement/internal/l;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/measurement/internal/y;->a([Lcom/google/android/gms/measurement/internal/l;Lcom/google/android/gms/measurement/internal/AppMetadata;)Lcom/google/android/gms/internal/j$d;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/measurement/internal/i;->a(Lcom/google/android/gms/internal/j$d;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->o()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->s()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v2, "Event logged"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_8a
    .catchall {:try_start_3e .. :try_end_8a} :catchall_a3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->p()V

    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/y;->B()V

    goto/16 :goto_13

    :cond_96
    :try_start_96
    iget-wide v2, v0, Lcom/google/android/gms/measurement/internal/m;->e:J

    invoke-virtual {v1, p0, v2, v3}, Lcom/google/android/gms/measurement/internal/l;->a(Lcom/google/android/gms/measurement/internal/y;J)Lcom/google/android/gms/measurement/internal/l;

    move-result-object v1

    iget-wide v2, v1, Lcom/google/android/gms/measurement/internal/l;->d:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/m;->a(J)Lcom/google/android/gms/measurement/internal/m;
    :try_end_a1
    .catchall {:try_start_96 .. :try_end_a1} :catchall_a3

    move-result-object v3

    goto :goto_5e

    :catchall_a3
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/i;->p()V

    throw v0
.end method

.method a(Lcom/google/android/gms/measurement/internal/EventParcel;Ljava/lang/String;)V
    .registers 15

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/measurement/internal/i;->b(Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/a;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v1, v0, Lcom/google/android/gms/measurement/internal/a;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_20

    :cond_12
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->s()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "No app data available; dropping event"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_1f
    return-void

    :cond_20
    new-instance v1, Lcom/google/android/gms/measurement/internal/AppMetadata;

    iget-object v3, v0, Lcom/google/android/gms/measurement/internal/a;->c:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/gms/measurement/internal/a;->g:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/gms/measurement/internal/a;->h:Ljava/lang/String;

    iget-wide v6, v0, Lcom/google/android/gms/measurement/internal/a;->i:J

    iget-wide v8, v0, Lcom/google/android/gms/measurement/internal/a;->j:J

    const/4 v10, 0x0

    iget-boolean v11, v0, Lcom/google/android/gms/measurement/internal/a;->k:Z

    move-object v2, p2

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/measurement/internal/AppMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Z)V

    invoke-virtual {p0, p1, v1}, Lcom/google/android/gms/measurement/internal/y;->a(Lcom/google/android/gms/measurement/internal/EventParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V

    goto :goto_1f
.end method

.method a(Lcom/google/android/gms/measurement/internal/UserAttributeParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    .registers 10

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->u()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->a()V

    iget-object v0, p2, Lcom/google/android/gms/measurement/internal/AppMetadata;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_e
    :goto_e
    return-void

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->k()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/measurement/internal/UserAttributeParcel;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/f;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->k()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/measurement/internal/UserAttributeParcel;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/UserAttributeParcel;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/f;->c(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_e

    new-instance v1, Lcom/google/android/gms/measurement/internal/d;

    iget-object v2, p2, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/measurement/internal/UserAttributeParcel;->b:Ljava/lang/String;

    iget-wide v4, p1, Lcom/google/android/gms/measurement/internal/UserAttributeParcel;->c:J

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/d;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->s()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v2, "Setting user attribute"

    iget-object v3, v1, Lcom/google/android/gms/measurement/internal/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v6}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->b()V

    :try_start_49
    invoke-direct {p0, p2}, Lcom/google/android/gms/measurement/internal/y;->c(Lcom/google/android/gms/measurement/internal/AppMetadata;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/i;->a(Lcom/google/android/gms/measurement/internal/d;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->o()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->s()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v2, "User attribute set"

    iget-object v3, v1, Lcom/google/android/gms/measurement/internal/d;->b:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/android/gms/measurement/internal/d;->d:Ljava/lang/Object;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_6b
    .catchall {:try_start_49 .. :try_end_6b} :catchall_73

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->p()V

    goto :goto_e

    :catchall_73
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/i;->p()V

    throw v0
.end method

.method a(Lcom/google/android/gms/measurement/internal/ab;)V
    .registers 3

    iget v0, p0, Lcom/google/android/gms/measurement/internal/y;->v:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/measurement/internal/y;->v:I

    return-void
.end method

.method public a(Z)V
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/y;->B()V

    return-void
.end method

.method public b(Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    .registers 12

    const-wide/32 v4, 0x36ee80

    const-wide/16 v6, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->u()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->a()V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/gms/measurement/internal/AppMetadata;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1c

    :cond_1b
    :goto_1b
    return-void

    :cond_1c
    invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/y;->c(Lcom/google/android/gms/measurement/internal/AppMetadata;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    const-string v2, "_f"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/i;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/m;

    move-result-object v0

    if-nez v0, :cond_1b

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->o()Lcom/google/android/gms/internal/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/e;->a()J

    move-result-wide v2

    div-long v0, v2, v4

    add-long/2addr v0, v6

    mul-long/2addr v4, v0

    new-instance v0, Lcom/google/android/gms/measurement/internal/UserAttributeParcel;

    const-string v1, "_fot"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const-string v5, "auto"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/measurement/internal/UserAttributeParcel;-><init>(Ljava/lang/String;JLjava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/measurement/internal/y;->a(Lcom/google/android/gms/measurement/internal/UserAttributeParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "_c"

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    new-instance v4, Lcom/google/android/gms/measurement/internal/EventParcel;

    const-string v5, "_f"

    new-instance v6, Lcom/google/android/gms/measurement/internal/EventParams;

    invoke-direct {v6, v0}, Lcom/google/android/gms/measurement/internal/EventParams;-><init>(Landroid/os/Bundle;)V

    const-string v7, "auto"

    move-wide v8, v2

    invoke-direct/range {v4 .. v9}, Lcom/google/android/gms/measurement/internal/EventParcel;-><init>(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/EventParams;Ljava/lang/String;J)V

    invoke-virtual {p0, v4, p1}, Lcom/google/android/gms/measurement/internal/y;->a(Lcom/google/android/gms/measurement/internal/EventParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V

    goto :goto_1b
.end method

.method b(Lcom/google/android/gms/measurement/internal/UserAttributeParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    .registers 6

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->u()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->a()V

    iget-object v0, p2, Lcom/google/android/gms/measurement/internal/AppMetadata;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    :goto_e
    return-void

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->s()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Removing user attribute"

    iget-object v2, p1, Lcom/google/android/gms/measurement/internal/UserAttributeParcel;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->b()V

    :try_start_25
    invoke-direct {p0, p2}, Lcom/google/android/gms/measurement/internal/y;->c(Lcom/google/android/gms/measurement/internal/AppMetadata;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/measurement/internal/UserAttributeParcel;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/i;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->o()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->s()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "User attribute removed"

    iget-object v2, p1, Lcom/google/android/gms/measurement/internal/UserAttributeParcel;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_49
    .catchall {:try_start_25 .. :try_end_49} :catchall_51

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->p()V

    goto :goto_e

    :catchall_51
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/i;->p()V

    throw v0
.end method

.method protected b()Z
    .registers 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->a()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->u()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->t:Ljava/lang/Boolean;

    if-nez v0, :cond_65

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->k()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v0

    const-string v3, "android.permission.INTERNET"

    invoke-virtual {v0, v3}, Lcom/google/android/gms/measurement/internal/f;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6c

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->k()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v0

    const-string v3, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v0, v3}, Lcom/google/android/gms/measurement/internal/f;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6c

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/measurement/AppMeasurementReceiver;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6c

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/measurement/AppMeasurementService;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6c

    move v0, v1

    :goto_39
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->t:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->t:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_65

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->C()Z

    move-result v0

    if-nez v0, :cond_65

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->r()Lcom/google/android/gms/measurement/internal/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/r;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6e

    :goto_5f
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->t:Ljava/lang/Boolean;

    :cond_65
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->t:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_6c
    move v0, v2

    goto :goto_39

    :cond_6e
    move v1, v2

    goto :goto_5f
.end method

.method protected c()V
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->u()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->r()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "App measurement is starting up"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->s()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Debug logging enabled"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->v()Z

    move-result v0

    if-eqz v0, :cond_41

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->g:Lcom/google/android/gms/measurement/internal/x;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/x;->w()Z

    move-result v0

    if-eqz v0, :cond_33

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->g:Lcom/google/android/gms/measurement/internal/x;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/x;->x()Z

    move-result v0

    if-eqz v0, :cond_41

    :cond_33
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Scheduler shutting down before Scion.start() called"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    :goto_40
    return-void

    :cond_41
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->s()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->b()Z

    move-result v0

    if-nez v0, :cond_bf

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->k()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v0

    const-string v1, "android.permission.INTERNET"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/f;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_67

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "App is missing INTERNET permission"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    :cond_67
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->k()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v0

    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/f;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_80

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "App is missing ACCESS_NETWORK_STATE permission"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    :cond_80
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/measurement/AppMeasurementReceiver;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_97

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "AppMeasurementReceiver not registered/enabled"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    :cond_97
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/measurement/AppMeasurementService;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_ae

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "AppMeasurementService not registered/enabled"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    :cond_ae
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Uploading is not possible. App measurement disabled"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    :cond_bb
    :goto_bb
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/y;->B()V

    goto :goto_40

    :cond_bf
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->C()Z

    move-result v0

    if-nez v0, :cond_bb

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->v()Z

    move-result v0

    if-nez v0, :cond_bb

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->r()Lcom/google/android/gms/measurement/internal/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/r;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_bb

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->i()Lcom/google/android/gms/measurement/internal/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ad;->o()V

    goto :goto_bb
.end method

.method public d()Lcom/google/android/gms/measurement/internal/h;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->d:Lcom/google/android/gms/measurement/internal/h;

    return-object v0
.end method

.method public e()Lcom/google/android/gms/measurement/internal/w;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->e:Lcom/google/android/gms/measurement/internal/w;

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/y;->a(Lcom/google/android/gms/measurement/internal/aa;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->e:Lcom/google/android/gms/measurement/internal/w;

    return-object v0
.end method

.method public f()Lcom/google/android/gms/measurement/internal/t;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->f:Lcom/google/android/gms/measurement/internal/t;

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/y;->b(Lcom/google/android/gms/measurement/internal/ab;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->f:Lcom/google/android/gms/measurement/internal/t;

    return-object v0
.end method

.method public g()Lcom/google/android/gms/measurement/internal/x;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->g:Lcom/google/android/gms/measurement/internal/x;

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/y;->b(Lcom/google/android/gms/measurement/internal/ab;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->g:Lcom/google/android/gms/measurement/internal/x;

    return-object v0
.end method

.method h()Lcom/google/android/gms/measurement/internal/x;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->g:Lcom/google/android/gms/measurement/internal/x;

    return-object v0
.end method

.method public i()Lcom/google/android/gms/measurement/internal/ad;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->o:Lcom/google/android/gms/measurement/internal/ad;

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/y;->b(Lcom/google/android/gms/measurement/internal/ab;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->o:Lcom/google/android/gms/measurement/internal/ad;

    return-object v0
.end method

.method public j()Lcom/google/android/gms/measurement/a;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->h:Lcom/google/android/gms/measurement/a;

    return-object v0
.end method

.method public k()Lcom/google/android/gms/measurement/internal/f;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->i:Lcom/google/android/gms/measurement/internal/f;

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/y;->a(Lcom/google/android/gms/measurement/internal/aa;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->i:Lcom/google/android/gms/measurement/internal/f;

    return-object v0
.end method

.method public l()Lcom/google/android/gms/measurement/internal/i;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->j:Lcom/google/android/gms/measurement/internal/i;

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/y;->b(Lcom/google/android/gms/measurement/internal/ab;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->j:Lcom/google/android/gms/measurement/internal/i;

    return-object v0
.end method

.method public m()Lcom/google/android/gms/measurement/internal/u;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->k:Lcom/google/android/gms/measurement/internal/u;

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/y;->b(Lcom/google/android/gms/measurement/internal/ab;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->k:Lcom/google/android/gms/measurement/internal/u;

    return-object v0
.end method

.method public n()Landroid/content/Context;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->c:Landroid/content/Context;

    return-object v0
.end method

.method public o()Lcom/google/android/gms/internal/e;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->l:Lcom/google/android/gms/internal/e;

    return-object v0
.end method

.method public p()Lcom/google/android/gms/measurement/internal/ae;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->m:Lcom/google/android/gms/measurement/internal/ae;

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/y;->b(Lcom/google/android/gms/measurement/internal/ab;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->m:Lcom/google/android/gms/measurement/internal/ae;

    return-object v0
.end method

.method public q()Lcom/google/android/gms/measurement/internal/k;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->n:Lcom/google/android/gms/measurement/internal/k;

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/y;->b(Lcom/google/android/gms/measurement/internal/ab;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->n:Lcom/google/android/gms/measurement/internal/k;

    return-object v0
.end method

.method public r()Lcom/google/android/gms/measurement/internal/r;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->p:Lcom/google/android/gms/measurement/internal/r;

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/y;->b(Lcom/google/android/gms/measurement/internal/ab;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->p:Lcom/google/android/gms/measurement/internal/r;

    return-object v0
.end method

.method public s()Lcom/google/android/gms/measurement/internal/v;
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->q:Lcom/google/android/gms/measurement/internal/v;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Network broadcast receiver not created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->q:Lcom/google/android/gms/measurement/internal/v;

    return-object v0
.end method

.method public t()Lcom/google/android/gms/measurement/internal/c;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->r:Lcom/google/android/gms/measurement/internal/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/y;->b(Lcom/google/android/gms/measurement/internal/ab;)V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/y;->r:Lcom/google/android/gms/measurement/internal/c;

    return-object v0
.end method

.method public u()V
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->g()Lcom/google/android/gms/measurement/internal/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/x;->e()V

    return-void
.end method

.method protected v()Z
    .registers 2

    const/4 v0, 0x0

    return v0
.end method

.method w()V
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->C()Z

    move-result v0

    if-eqz v0, :cond_12

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected call on package side"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    return-void
.end method

.method public x()V
    .registers 11

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->u()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->a()V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->C()Z

    move-result v0

    if-nez v0, :cond_3d

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/w;->q()Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_29

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->o()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Upload data called on the client side before use of service was decided"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    :cond_28
    :goto_28
    return-void

    :cond_29
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Upload called in the client side when service should be used"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    goto :goto_28

    :cond_3d
    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/y;->z()Z

    move-result v0

    if-eqz v0, :cond_51

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->o()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Uploading requested multiple times"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    goto :goto_28

    :cond_51
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->m()Lcom/google/android/gms/measurement/internal/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/u;->b()Z

    move-result v0

    if-nez v0, :cond_6c

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->o()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v1, "Network not connected, ignoring upload request"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/y;->B()V

    goto :goto_28

    :cond_6c
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/measurement/internal/w;->c:Lcom/google/android/gms/measurement/internal/w$a;

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/w$a;->a()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-eqz v3, :cond_9b

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/t;->s()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v3

    const-string v4, "Uploading events. Elapsed time since last upload attempt (ms)"

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->o()Lcom/google/android/gms/internal/e;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/internal/e;->a()J

    move-result-wide v6

    sub-long v0, v6, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_9b
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_28

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/h;->I()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/h;->J()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->l()Lcom/google/android/gms/measurement/internal/i;

    move-result-object v4

    invoke-virtual {v4, v0, v1, v3}, Lcom/google/android/gms/measurement/internal/i;->a(Ljava/lang/String;II)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_28

    const/4 v1, 0x0

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_cc
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1ca

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/internal/j$d;

    iget-object v5, v0, Lcom/google/android/gms/internal/j$d;->s:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_cc

    iget-object v0, v0, Lcom/google/android/gms/internal/j$d;->s:Ljava/lang/String;

    move-object v4, v0

    :goto_e7
    if-eqz v4, :cond_1c7

    move v1, v2

    :goto_ea
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1c7

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/internal/j$d;

    iget-object v5, v0, Lcom/google/android/gms/internal/j$d;->s:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_106

    :cond_102
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_ea

    :cond_106
    iget-object v0, v0, Lcom/google/android/gms/internal/j$d;->s:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_102

    invoke-interface {v3, v2, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    :goto_113
    new-instance v3, Lcom/google/android/gms/internal/j$c;

    invoke-direct {v3}, Lcom/google/android/gms/internal/j$c;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/internal/j$d;

    iput-object v0, v3, Lcom/google/android/gms/internal/j$c;->a:[Lcom/google/android/gms/internal/j$d;

    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->o()Lcom/google/android/gms/internal/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/e;->a()J

    move-result-wide v6

    :goto_131
    iget-object v0, v3, Lcom/google/android/gms/internal/j$c;->a:[Lcom/google/android/gms/internal/j$d;

    array-length v0, v0

    if-ge v2, v0, :cond_180

    iget-object v5, v3, Lcom/google/android/gms/internal/j$c;->a:[Lcom/google/android/gms/internal/j$d;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/internal/j$d;

    aput-object v0, v5, v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v3, Lcom/google/android/gms/internal/j$c;->a:[Lcom/google/android/gms/internal/j$d;

    aget-object v0, v0, v2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/measurement/internal/h;->B()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v0, Lcom/google/android/gms/internal/j$d;->r:Ljava/lang/Long;

    iget-object v0, v3, Lcom/google/android/gms/internal/j$c;->a:[Lcom/google/android/gms/internal/j$d;

    aget-object v0, v0, v2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v0, Lcom/google/android/gms/internal/j$d;->d:Ljava/lang/Long;

    iget-object v0, v3, Lcom/google/android/gms/internal/j$c;->a:[Lcom/google/android/gms/internal/j$d;

    aget-object v0, v0, v2

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/measurement/internal/h;->C()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v0, Lcom/google/android/gms/internal/j$d;->z:Ljava/lang/Boolean;

    add-int/lit8 v2, v2, 0x1

    goto :goto_131

    :cond_180
    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->k()Lcom/google/android/gms/measurement/internal/f;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/measurement/internal/f;->a(Lcom/google/android/gms/internal/j$c;)[B

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/h;->K()Ljava/lang/String;

    move-result-object v1

    :try_start_190
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/google/android/gms/measurement/internal/y;->a(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/measurement/internal/w;->d:Lcom/google/android/gms/measurement/internal/w$a;

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->o()Lcom/google/android/gms/internal/e;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/internal/e;->a()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/measurement/internal/w$a;->a(J)V

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->m()Lcom/google/android/gms/measurement/internal/u;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/measurement/internal/y$2;

    invoke-direct {v4, p0}, Lcom/google/android/gms/measurement/internal/y$2;-><init>(Lcom/google/android/gms/measurement/internal/y;)V

    invoke-virtual {v3, v2, v0, v4}, Lcom/google/android/gms/measurement/internal/u;->a(Ljava/net/URL;[BLcom/google/android/gms/measurement/internal/u$a;)V
    :try_end_1b5
    .catch Ljava/net/MalformedURLException; {:try_start_190 .. :try_end_1b5} :catch_1b7

    goto/16 :goto_28

    :catch_1b7
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;

    move-result-object v0

    const-string v2, "Failed to parse upload URL. Not uploading"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_28

    :cond_1c7
    move-object v1, v3

    goto/16 :goto_113

    :cond_1ca
    move-object v4, v1

    goto/16 :goto_e7
.end method

.method y()V
    .registers 2

    iget v0, p0, Lcom/google/android/gms/measurement/internal/y;->w:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/measurement/internal/y;->w:I

    return-void
.end method
