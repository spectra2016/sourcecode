.class public abstract Lcom/google/android/gms/gcm/a;
.super Landroid/app/Service;


# static fields
.field private static d:Z


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:I

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/gcm/a;->d:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/gcm/a;->a:Ljava/lang/Object;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/gcm/a;->c:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/gcm/a;Landroid/content/Intent;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/google/android/gms/gcm/a;->d(Landroid/content/Intent;)V

    return-void
.end method

.method static a(Landroid/content/Intent;)Z
    .registers 2

    sget-boolean v0, Lcom/google/android/gms/gcm/a;->d:Z

    if-eqz v0, :cond_12

    const-string v0, "gcm.a.campaign"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method static a(Landroid/os/Bundle;)Z
    .registers 2

    sget-boolean v0, Lcom/google/android/gms/gcm/a;->d:Z

    if-eqz v0, :cond_12

    const-string v0, "gcm.a.campaign"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private b()V
    .registers 3

    iget-object v1, p0, Lcom/google/android/gms/gcm/a;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/gcm/a;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/gcm/a;->c:I

    iget v0, p0, Lcom/google/android/gms/gcm/a;->c:I

    if-nez v0, :cond_12

    iget v0, p0, Lcom/google/android/gms/gcm/a;->b:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/a;->a(I)Z

    :cond_12
    monitor-exit v1

    return-void

    :catchall_14
    move-exception v0

    monitor-exit v1
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_14

    throw v0
.end method

.method private b(Landroid/content/Intent;)V
    .registers 4

    const-string v0, "com.google.android.gms.gcm.PENDING_INTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    if-eqz v0, :cond_d

    :try_start_a
    invoke-virtual {v0}, Landroid/app/PendingIntent;->send()V
    :try_end_d
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_a .. :try_end_d} :catch_11

    :cond_d
    :goto_d
    invoke-static {p0, p1}, Lcom/google/android/gms/gcm/d;->b(Landroid/content/Context;Landroid/content/Intent;)V

    return-void

    :catch_11
    move-exception v0

    const-string v0, "GcmListenerService"

    const-string v1, "Notification pending intent canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d
.end method

.method private c(Landroid/content/Intent;)V
    .registers 4

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_11

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/gms/gcm/a$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/gcm/a$1;-><init>(Lcom/google/android/gms/gcm/a;Landroid/content/Intent;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_10
    return-void

    :cond_11
    new-instance v0, Lcom/google/android/gms/gcm/a$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/gcm/a$2;-><init>(Lcom/google/android/gms/gcm/a;Landroid/content/Intent;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/a$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_10
.end method

.method private d(Landroid/content/Intent;)V
    .registers 5

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_54

    :cond_c
    :goto_c
    packed-switch v0, :pswitch_data_5e

    const-string v0, "GcmListenerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown intent action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2b
    invoke-direct {p0}, Lcom/google/android/gms/gcm/a;->b()V
    :try_end_2e
    .catchall {:try_start_0 .. :try_end_2e} :catchall_4a

    invoke-static {p1}, Lcom/google/android/gms/gcm/GcmReceiver;->a(Landroid/content/Intent;)Z

    return-void

    :sswitch_32
    :try_start_32
    const-string v2, "com.google.android.c2dm.intent.RECEIVE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v0, 0x0

    goto :goto_c

    :sswitch_3c
    const-string v2, "com.google.android.gms.gcm.NOTIFICATION_DISMISS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v0, 0x1

    goto :goto_c

    :pswitch_46
    invoke-direct {p0, p1}, Lcom/google/android/gms/gcm/a;->e(Landroid/content/Intent;)V
    :try_end_49
    .catchall {:try_start_32 .. :try_end_49} :catchall_4a

    goto :goto_2b

    :catchall_4a
    move-exception v0

    invoke-static {p1}, Lcom/google/android/gms/gcm/GcmReceiver;->a(Landroid/content/Intent;)Z

    throw v0

    :pswitch_4f
    :try_start_4f
    invoke-static {p0, p1}, Lcom/google/android/gms/gcm/d;->c(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_52
    .catchall {:try_start_4f .. :try_end_52} :catchall_4a

    goto :goto_2b

    nop

    :sswitch_data_54
    .sparse-switch
        0xcc40d1b -> :sswitch_3c
        0x15d8a480 -> :sswitch_32
    .end sparse-switch

    :pswitch_data_5e
    .packed-switch 0x0
        :pswitch_46
        :pswitch_4f
    .end packed-switch
.end method

.method private e(Landroid/content/Intent;)V
    .registers 6

    const-string v0, "message_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_a

    const-string v0, "gcm"

    :cond_a
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_82

    :cond_12
    :goto_12
    packed-switch v1, :pswitch_data_94

    const-string v1, "GcmListenerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received message with unknown type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2d
    return-void

    :sswitch_2e
    const-string v2, "gcm"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    const/4 v1, 0x0

    goto :goto_12

    :sswitch_38
    const-string v2, "deleted_messages"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    const/4 v1, 0x1

    goto :goto_12

    :sswitch_42
    const-string v2, "send_event"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    const/4 v1, 0x2

    goto :goto_12

    :sswitch_4c
    const-string v2, "send_error"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    const/4 v1, 0x3

    goto :goto_12

    :pswitch_56
    invoke-static {p1}, Lcom/google/android/gms/gcm/a;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_5f

    invoke-static {p0, p1}, Lcom/google/android/gms/gcm/d;->a(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_5f
    invoke-direct {p0, p1}, Lcom/google/android/gms/gcm/a;->f(Landroid/content/Intent;)V

    goto :goto_2d

    :pswitch_63
    invoke-virtual {p0}, Lcom/google/android/gms/gcm/a;->a()V

    goto :goto_2d

    :pswitch_67
    const-string v0, "google.message_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/a;->a(Ljava/lang/String;)V

    goto :goto_2d

    :pswitch_71
    const-string v0, "google.message_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "error"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/gcm/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2d

    nop

    :sswitch_data_82
    .sparse-switch
        -0x7aedf14e -> :sswitch_38
        0x18f11 -> :sswitch_2e
        0x308f3e91 -> :sswitch_4c
        0x3090df23 -> :sswitch_42
    .end sparse-switch

    :pswitch_data_94
    .packed-switch 0x0
        :pswitch_56
        :pswitch_63
        :pswitch_67
        :pswitch_71
    .end packed-switch
.end method

.method private f(Landroid/content/Intent;)V
    .registers 5

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "message_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string v1, "android.support.content.wakelockid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/gcm/e;->a(Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_32

    invoke-static {p0}, Lcom/google/android/gms/gcm/e;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_26

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/gms/gcm/e;->a(Landroid/content/Context;Ljava/lang/Class;)Lcom/google/android/gms/gcm/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/gcm/e;->c(Landroid/os/Bundle;)Z

    :goto_25
    return-void

    :cond_26
    invoke-static {v0}, Lcom/google/android/gms/gcm/e;->b(Landroid/os/Bundle;)V

    invoke-static {p1}, Lcom/google/android/gms/gcm/a;->a(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_32

    invoke-static {p0, p1}, Lcom/google/android/gms/gcm/d;->d(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_32
    const-string v1, "from"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "from"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/gcm/a;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_25
.end method


# virtual methods
.method public a()V
    .registers 1

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2

    return-void
.end method

.method public a(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 3

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    return-void
.end method

.method a(I)Z
    .registers 3

    invoke-virtual {p0, p1}, Lcom/google/android/gms/gcm/a;->stopSelfResult(I)Z

    move-result v0

    return v0
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3

    const/4 v0, 0x0

    return-object v0
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .registers 6

    iget-object v1, p0, Lcom/google/android/gms/gcm/a;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iput p3, p0, Lcom/google/android/gms/gcm/a;->b:I

    iget v0, p0, Lcom/google/android/gms/gcm/a;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/gcm/a;->c:I

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_23

    const-string v0, "com.google.android.gms.gcm.NOTIFICATION_OPEN"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-direct {p0, p1}, Lcom/google/android/gms/gcm/a;->b(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/google/android/gms/gcm/a;->b()V

    invoke-static {p1}, Lcom/google/android/gms/gcm/GcmReceiver;->a(Landroid/content/Intent;)Z

    :goto_21
    const/4 v0, 0x3

    return v0

    :catchall_23
    move-exception v0

    :try_start_24
    monitor-exit v1
    :try_end_25
    .catchall {:try_start_24 .. :try_end_25} :catchall_23

    throw v0

    :cond_26
    invoke-direct {p0, p1}, Lcom/google/android/gms/gcm/a;->c(Landroid/content/Intent;)V

    goto :goto_21
.end method
