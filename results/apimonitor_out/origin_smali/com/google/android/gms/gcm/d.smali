.class Lcom/google/android/gms/gcm/d;
.super Ljava/lang/Object;


# static fields
.field static a:Lcom/google/android/gms/measurement/a;


# direct methods
.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 3

    const-string v0, "_nr"

    invoke-static {p0, v0, p1}, Lcom/google/android/gms/gcm/d;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)V
    .registers 8

    const-string v0, "gcm.a.campaign"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "GcmAnalytics"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_31

    const-string v1, "GcmAnalytics"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending event="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " campaign="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_31
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "nc"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_54

    const-string v2, "/topics/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_62

    const-string v2, "nt"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_54
    :goto_54
    :try_start_54
    sget-object v0, Lcom/google/android/gms/gcm/d;->a:Lcom/google/android/gms/measurement/a;

    if-nez v0, :cond_85

    invoke-static {p0}, Lcom/google/android/gms/measurement/a;->a(Landroid/content/Context;)Lcom/google/android/gms/measurement/a;

    move-result-object v0

    :goto_5c
    const-string v2, "gcm"

    invoke-virtual {v0, v2, p1, v1}, Lcom/google/android/gms/measurement/a;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_61
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_54 .. :try_end_61} :catch_88

    :goto_61
    return-void

    :cond_62
    :try_start_62
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    const-string v2, "nsid"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6a
    .catch Ljava/lang/NumberFormatException; {:try_start_62 .. :try_end_6a} :catch_6b

    goto :goto_54

    :catch_6b
    move-exception v2

    const-string v2, "GcmAnalytics"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unrecognised from address: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_54

    :cond_85
    :try_start_85
    sget-object v0, Lcom/google/android/gms/gcm/d;->a:Lcom/google/android/gms/measurement/a;
    :try_end_87
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_85 .. :try_end_87} :catch_88

    goto :goto_5c

    :catch_88
    move-exception v0

    const-string v0, "GcmAnalytics"

    const-string v1, "Unable to log event, missing GMS measurement library"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_61
.end method

.method public static b(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 3

    const-string v0, "_no"

    invoke-static {p0, v0, p1}, Lcom/google/android/gms/gcm/d;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)V

    return-void
.end method

.method public static c(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 3

    const-string v0, "_nd"

    invoke-static {p0, v0, p1}, Lcom/google/android/gms/gcm/d;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)V

    return-void
.end method

.method public static d(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 3

    const-string v0, "_nf"

    invoke-static {p0, v0, p1}, Lcom/google/android/gms/gcm/d;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)V

    return-void
.end method
