.class public Lio/fabric/sdk/android/c$a;
.super Ljava/lang/Object;
.source "Fabric.java"
.field private final a:Landroid/content/Context;
.field private b:[Lio/fabric/sdk/android/h;
.field private c:Lio/fabric/sdk/android/services/concurrency/k;
.field private d:Landroid/os/Handler;
.field private e:Lio/fabric/sdk/android/k;
.field private f:Z
.field private g:Ljava/lang/String;
.field private h:Ljava/lang/String;
.field private i:Lio/fabric/sdk/android/f;
.method public constructor <init>(Landroid/content/Context;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
if-nez p1, :cond_d
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "Context must not be null."
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_d
iput-object p1, p0, Lio/fabric/sdk/android/c$a;->a:Landroid/content/Context;
return-void
.end method
.method public varargs a([Lio/fabric/sdk/android/h;)Lio/fabric/sdk/android/c$a;
.registers 4
iget-object v0, p0, Lio/fabric/sdk/android/c$a;->b:[Lio/fabric/sdk/android/h;
if-eqz v0, :cond_c
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "Kits already set."
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_c
iput-object p1, p0, Lio/fabric/sdk/android/c$a;->b:[Lio/fabric/sdk/android/h;
return-object p0
.end method
.method public a()Lio/fabric/sdk/android/c;
.registers 10
iget-object v0, p0, Lio/fabric/sdk/android/c$a;->c:Lio/fabric/sdk/android/services/concurrency/k;
if-nez v0, :cond_a
invoke-static {}, Lio/fabric/sdk/android/services/concurrency/k;->a()Lio/fabric/sdk/android/services/concurrency/k;
move-result-object v0
iput-object v0, p0, Lio/fabric/sdk/android/c$a;->c:Lio/fabric/sdk/android/services/concurrency/k;
:cond_a
iget-object v0, p0, Lio/fabric/sdk/android/c$a;->d:Landroid/os/Handler;
if-nez v0, :cond_19
new-instance v0, Landroid/os/Handler;
invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;
move-result-object v1
invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V
iput-object v0, p0, Lio/fabric/sdk/android/c$a;->d:Landroid/os/Handler;
:cond_19
iget-object v0, p0, Lio/fabric/sdk/android/c$a;->e:Lio/fabric/sdk/android/k;
if-nez v0, :cond_29
iget-boolean v0, p0, Lio/fabric/sdk/android/c$a;->f:Z
if-eqz v0, :cond_67
new-instance v0, Lio/fabric/sdk/android/b;
const/4 v1, 0x3
invoke-direct {v0, v1}, Lio/fabric/sdk/android/b;-><init>(I)V
iput-object v0, p0, Lio/fabric/sdk/android/c$a;->e:Lio/fabric/sdk/android/k;
:cond_29
:goto_29
iget-object v0, p0, Lio/fabric/sdk/android/c$a;->h:Ljava/lang/String;
if-nez v0, :cond_35
iget-object v0, p0, Lio/fabric/sdk/android/c$a;->a:Landroid/content/Context;
invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lio/fabric/sdk/android/c$a;->h:Ljava/lang/String;
:cond_35
iget-object v0, p0, Lio/fabric/sdk/android/c$a;->i:Lio/fabric/sdk/android/f;
if-nez v0, :cond_3d
sget-object v0, Lio/fabric/sdk/android/f;->d:Lio/fabric/sdk/android/f;
iput-object v0, p0, Lio/fabric/sdk/android/c$a;->i:Lio/fabric/sdk/android/f;
:cond_3d
iget-object v0, p0, Lio/fabric/sdk/android/c$a;->b:[Lio/fabric/sdk/android/h;
if-nez v0, :cond_6f
new-instance v2, Ljava/util/HashMap;
invoke-direct {v2}, Ljava/util/HashMap;-><init>()V
:goto_46
new-instance v8, Lio/fabric/sdk/android/services/b/o;
iget-object v0, p0, Lio/fabric/sdk/android/c$a;->a:Landroid/content/Context;
iget-object v1, p0, Lio/fabric/sdk/android/c$a;->h:Ljava/lang/String;
iget-object v3, p0, Lio/fabric/sdk/android/c$a;->g:Ljava/lang/String;
invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;
move-result-object v4
invoke-direct {v8, v0, v1, v3, v4}, Lio/fabric/sdk/android/services/b/o;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V
new-instance v0, Lio/fabric/sdk/android/c;
iget-object v1, p0, Lio/fabric/sdk/android/c$a;->a:Landroid/content/Context;
iget-object v3, p0, Lio/fabric/sdk/android/c$a;->c:Lio/fabric/sdk/android/services/concurrency/k;
iget-object v4, p0, Lio/fabric/sdk/android/c$a;->d:Landroid/os/Handler;
iget-object v5, p0, Lio/fabric/sdk/android/c$a;->e:Lio/fabric/sdk/android/k;
iget-boolean v6, p0, Lio/fabric/sdk/android/c$a;->f:Z
iget-object v7, p0, Lio/fabric/sdk/android/c$a;->i:Lio/fabric/sdk/android/f;
invoke-direct/range {v0 .. v8}, Lio/fabric/sdk/android/c;-><init>(Landroid/content/Context;Ljava/util/Map;Lio/fabric/sdk/android/services/concurrency/k;Landroid/os/Handler;Lio/fabric/sdk/android/k;ZLio/fabric/sdk/android/f;Lio/fabric/sdk/android/services/b/o;)V
return-object v0
:cond_67
new-instance v0, Lio/fabric/sdk/android/b;
invoke-direct {v0}, Lio/fabric/sdk/android/b;-><init>()V
iput-object v0, p0, Lio/fabric/sdk/android/c$a;->e:Lio/fabric/sdk/android/k;
goto :goto_29
:cond_6f
iget-object v0, p0, Lio/fabric/sdk/android/c$a;->b:[Lio/fabric/sdk/android/h;
invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;
move-result-object v0
invoke-static {v0}, Lio/fabric/sdk/android/c;->a(Ljava/util/Collection;)Ljava/util/Map;
move-result-object v2
goto :goto_46
.end method