.class  Lio/fabric/sdk/android/g;
.super Lio/fabric/sdk/android/services/concurrency/f;
.source "InitializationTask.java"
.field final a:Lio/fabric/sdk/android/h;
.method public constructor <init>(Lio/fabric/sdk/android/h;)V
.registers 2
invoke-direct {p0}, Lio/fabric/sdk/android/services/concurrency/f;-><init>()V
iput-object p1, p0, Lio/fabric/sdk/android/g;->a:Lio/fabric/sdk/android/h;
return-void
.end method
.method private a(Ljava/lang/String;)Lio/fabric/sdk/android/services/b/t;
.registers 5
new-instance v0, Lio/fabric/sdk/android/services/b/t;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
iget-object v2, p0, Lio/fabric/sdk/android/g;->a:Lio/fabric/sdk/android/h;
invoke-virtual {v2}, Lio/fabric/sdk/android/h;->getIdentifier()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "."
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
const-string v2, "KitInitialization"
invoke-direct {v0, v1, v2}, Lio/fabric/sdk/android/services/b/t;-><init>(Ljava/lang/String;Ljava/lang/String;)V
invoke-virtual {v0}, Lio/fabric/sdk/android/services/b/t;->a()V
return-object v0
.end method
.method protected bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
.registers 3
check-cast p1, [Ljava/lang/Void;
invoke-virtual {p0, p1}, Lio/fabric/sdk/android/g;->a([Ljava/lang/Void;)Ljava/lang/Object;
move-result-object v0
return-object v0
.end method
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Object;
.registers 5
const-string v0, "doInBackground"
invoke-direct {p0, v0}, Lio/fabric/sdk/android/g;->a(Ljava/lang/String;)Lio/fabric/sdk/android/services/b/t;
move-result-object v1
const/4 v0, 0x0
invoke-virtual {p0}, Lio/fabric/sdk/android/g;->d()Z
move-result v2
if-nez v2, :cond_13
iget-object v0, p0, Lio/fabric/sdk/android/g;->a:Lio/fabric/sdk/android/h;
invoke-virtual {v0}, Lio/fabric/sdk/android/h;->doInBackground()Ljava/lang/Object;
move-result-object v0
:cond_13
invoke-virtual {v1}, Lio/fabric/sdk/android/services/b/t;->b()V
return-object v0
.end method
.method protected a()V
.registers 7
const/4 v5, 0x1
invoke-super {p0}, Lio/fabric/sdk/android/services/concurrency/f;->a()V
const-string v0, "onPreExecute"
invoke-direct {p0, v0}, Lio/fabric/sdk/android/g;->a(Ljava/lang/String;)Lio/fabric/sdk/android/services/b/t;
move-result-object v1
:try_start_a
iget-object v0, p0, Lio/fabric/sdk/android/g;->a:Lio/fabric/sdk/android/h;
invoke-virtual {v0}, Lio/fabric/sdk/android/h;->onPreExecute()Z
:try_end_f
.catchall {:try_start_a .. :try_end_f} :catchall_1b
.catch Lio/fabric/sdk/android/services/concurrency/UnmetDependencyException; {:try_start_a .. :try_end_f} :catch_19
.catch Ljava/lang/Exception; {:try_start_a .. :try_end_f} :catch_23
move-result v0
invoke-virtual {v1}, Lio/fabric/sdk/android/services/b/t;->b()V
if-nez v0, :cond_18
invoke-virtual {p0, v5}, Lio/fabric/sdk/android/g;->a(Z)Z
:cond_18
:goto_18
return-void
:catch_19
move-exception v0
:try_start_1a
throw v0
:catchall_1b
:try_end_1b
.catchall {:try_start_1a .. :try_end_1b} :catchall_1b
move-exception v0
invoke-virtual {v1}, Lio/fabric/sdk/android/services/b/t;->b()V
invoke-virtual {p0, v5}, Lio/fabric/sdk/android/g;->a(Z)Z
throw v0
:catch_23
move-exception v0
:try_start_24
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v2
const-string v3, "Fabric"
const-string v4, "Failure onPreExecute()"
invoke-interface {v2, v3, v4, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
:try_end_2f
.catchall {:try_start_24 .. :try_end_2f} :catchall_1b
invoke-virtual {v1}, Lio/fabric/sdk/android/services/b/t;->b()V
invoke-virtual {p0, v5}, Lio/fabric/sdk/android/g;->a(Z)Z
goto :goto_18
.end method
.method protected a(Ljava/lang/Object;)V
.registers 3
iget-object v0, p0, Lio/fabric/sdk/android/g;->a:Lio/fabric/sdk/android/h;
invoke-virtual {v0, p1}, Lio/fabric/sdk/android/h;->onPostExecute(Ljava/lang/Object;)V
iget-object v0, p0, Lio/fabric/sdk/android/g;->a:Lio/fabric/sdk/android/h;
iget-object v0, v0, Lio/fabric/sdk/android/h;->initializationCallback:Lio/fabric/sdk/android/f;
invoke-interface {v0, p1}, Lio/fabric/sdk/android/f;->a(Ljava/lang/Object;)V
return-void
.end method
.method protected b(Ljava/lang/Object;)V
.registers 4
iget-object v0, p0, Lio/fabric/sdk/android/g;->a:Lio/fabric/sdk/android/h;
invoke-virtual {v0, p1}, Lio/fabric/sdk/android/h;->onCancelled(Ljava/lang/Object;)V
new-instance v0, Ljava/lang/StringBuilder;
invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
iget-object v1, p0, Lio/fabric/sdk/android/g;->a:Lio/fabric/sdk/android/h;
invoke-virtual {v1}, Lio/fabric/sdk/android/h;->getIdentifier()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, " Initialization was cancelled"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
new-instance v1, Lio/fabric/sdk/android/InitializationException;
invoke-direct {v1, v0}, Lio/fabric/sdk/android/InitializationException;-><init>(Ljava/lang/String;)V
iget-object v0, p0, Lio/fabric/sdk/android/g;->a:Lio/fabric/sdk/android/h;
iget-object v0, v0, Lio/fabric/sdk/android/h;->initializationCallback:Lio/fabric/sdk/android/f;
invoke-interface {v0, v1}, Lio/fabric/sdk/android/f;->a(Ljava/lang/Exception;)V
return-void
.end method
.method public getPriority()Lio/fabric/sdk/android/services/concurrency/e;
.registers 2
sget-object v0, Lio/fabric/sdk/android/services/concurrency/e;->c:Lio/fabric/sdk/android/services/concurrency/e;
return-object v0
.end method