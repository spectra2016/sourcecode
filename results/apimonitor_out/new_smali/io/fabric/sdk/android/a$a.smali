.class  Lio/fabric/sdk/android/a$a;
.super Ljava/lang/Object;
.source "ActivityLifecycleManager.java"
.field private final a:Ljava/util/Set;
.field private final b:Landroid/app/Application;
.method constructor <init>(Landroid/app/Application;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
new-instance v0, Ljava/util/HashSet;
invoke-direct {v0}, Ljava/util/HashSet;-><init>()V
iput-object v0, p0, Lio/fabric/sdk/android/a$a;->a:Ljava/util/Set;
iput-object p1, p0, Lio/fabric/sdk/android/a$a;->b:Landroid/app/Application;
return-void
.end method
.method private a()V
.registers 4
iget-object v0, p0, Lio/fabric/sdk/android/a$a;->a:Ljava/util/Set;
invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
move-result-object v1
:goto_6
invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_18
invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/app/Application$ActivityLifecycleCallbacks;
iget-object v2, p0, Lio/fabric/sdk/android/a$a;->b:Landroid/app/Application;
invoke-virtual {v2, v0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V
goto :goto_6
:cond_18
return-void
.end method
.method static synthetic a(Lio/fabric/sdk/android/a$a;)V
.registers 1
invoke-direct {p0}, Lio/fabric/sdk/android/a$a;->a()V
return-void
.end method
.method static synthetic a(Lio/fabric/sdk/android/a$a;Lio/fabric/sdk/android/a$b;)Z
.registers 3
invoke-direct {p0, p1}, Lio/fabric/sdk/android/a$a;->a(Lio/fabric/sdk/android/a$b;)Z
move-result v0
return v0
.end method
.method private a(Lio/fabric/sdk/android/a$b;)Z
.registers 4
iget-object v0, p0, Lio/fabric/sdk/android/a$a;->b:Landroid/app/Application;
if-eqz v0, :cond_15
new-instance v0, Lio/fabric/sdk/android/a$a$1;
invoke-direct {v0, p0, p1}, Lio/fabric/sdk/android/a$a$1;-><init>(Lio/fabric/sdk/android/a$a;Lio/fabric/sdk/android/a$b;)V
iget-object v1, p0, Lio/fabric/sdk/android/a$a;->b:Landroid/app/Application;
invoke-virtual {v1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V
iget-object v1, p0, Lio/fabric/sdk/android/a$a;->a:Ljava/util/Set;
invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
const/4 v0, 0x1
:goto_14
return v0
:cond_15
const/4 v0, 0x0
goto :goto_14
.end method