.class public final enum Lio/fabric/sdk/android/services/concurrency/a$d;
.super Ljava/lang/Enum;
.source "AsyncTask.java"
.field public static final enum a:Lio/fabric/sdk/android/services/concurrency/a$d;
.field public static final enum b:Lio/fabric/sdk/android/services/concurrency/a$d;
.field public static final enum c:Lio/fabric/sdk/android/services/concurrency/a$d;
.field private static final synthetic d:[Lio/fabric/sdk/android/services/concurrency/a$d;
.method static constructor <clinit>()V
.registers 5
const/4 v4, 0x2
const/4 v3, 0x1
const/4 v2, 0x0
new-instance v0, Lio/fabric/sdk/android/services/concurrency/a$d;
const-string v1, "PENDING"
invoke-direct {v0, v1, v2}, Lio/fabric/sdk/android/services/concurrency/a$d;-><init>(Ljava/lang/String;I)V
sput-object v0, Lio/fabric/sdk/android/services/concurrency/a$d;->a:Lio/fabric/sdk/android/services/concurrency/a$d;
new-instance v0, Lio/fabric/sdk/android/services/concurrency/a$d;
const-string v1, "RUNNING"
invoke-direct {v0, v1, v3}, Lio/fabric/sdk/android/services/concurrency/a$d;-><init>(Ljava/lang/String;I)V
sput-object v0, Lio/fabric/sdk/android/services/concurrency/a$d;->b:Lio/fabric/sdk/android/services/concurrency/a$d;
new-instance v0, Lio/fabric/sdk/android/services/concurrency/a$d;
const-string v1, "FINISHED"
invoke-direct {v0, v1, v4}, Lio/fabric/sdk/android/services/concurrency/a$d;-><init>(Ljava/lang/String;I)V
sput-object v0, Lio/fabric/sdk/android/services/concurrency/a$d;->c:Lio/fabric/sdk/android/services/concurrency/a$d;
const/4 v0, 0x3
new-array v0, v0, [Lio/fabric/sdk/android/services/concurrency/a$d;
sget-object v1, Lio/fabric/sdk/android/services/concurrency/a$d;->a:Lio/fabric/sdk/android/services/concurrency/a$d;
aput-object v1, v0, v2
sget-object v1, Lio/fabric/sdk/android/services/concurrency/a$d;->b:Lio/fabric/sdk/android/services/concurrency/a$d;
aput-object v1, v0, v3
sget-object v1, Lio/fabric/sdk/android/services/concurrency/a$d;->c:Lio/fabric/sdk/android/services/concurrency/a$d;
aput-object v1, v0, v4
sput-object v0, Lio/fabric/sdk/android/services/concurrency/a$d;->d:[Lio/fabric/sdk/android/services/concurrency/a$d;
return-void
.end method
.method private constructor <init>(Ljava/lang/String;I)V
.registers 3
invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V
return-void
.end method
.method public static valueOf(Ljava/lang/String;)Lio/fabric/sdk/android/services/concurrency/a$d;
.registers 2
const-class v0, Lio/fabric/sdk/android/services/concurrency/a$d;
invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/services/concurrency/a$d;
return-object v0
.end method
.method public static values()[Lio/fabric/sdk/android/services/concurrency/a$d;
.registers 1
sget-object v0, Lio/fabric/sdk/android/services/concurrency/a$d;->d:[Lio/fabric/sdk/android/services/concurrency/a$d;
invoke-virtual {v0}, [Lio/fabric/sdk/android/services/concurrency/a$d;->clone()Ljava/lang/Object;
move-result-object v0
check-cast v0, [Lio/fabric/sdk/android/services/concurrency/a$d;
return-object v0
.end method