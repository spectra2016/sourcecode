.class public Lio/fabric/sdk/android/services/b/t;
.super Ljava/lang/Object;
.source "TimingMetric.java"
.field private final a:Ljava/lang/String;
.field private final b:Ljava/lang/String;
.field private final c:Z
.field private d:J
.field private e:J
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lio/fabric/sdk/android/services/b/t;->a:Ljava/lang/String;
iput-object p2, p0, Lio/fabric/sdk/android/services/b/t;->b:Ljava/lang/String;
const/4 v0, 0x2
invoke-static {p2, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z
move-result v0
if-nez v0, :cond_12
const/4 v0, 0x1
:goto_f
iput-boolean v0, p0, Lio/fabric/sdk/android/services/b/t;->c:Z
return-void
:cond_12
const/4 v0, 0x0
goto :goto_f
.end method
.method private c()V
.registers 5
iget-object v0, p0, Lio/fabric/sdk/android/services/b/t;->b:Ljava/lang/String;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
iget-object v2, p0, Lio/fabric/sdk/android/services/b/t;->a:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, ": "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-wide v2, p0, Lio/fabric/sdk/android/services/b/t;->e:J
invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "ms"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
return-void
.end method
.method public declared-synchronized a()V
.registers 3
monitor-enter p0
:try_start_1
iget-boolean v0, p0, Lio/fabric/sdk/android/services/b/t;->c:Z
:try_end_3
.catchall {:try_start_1 .. :try_end_3} :catchall_12
if-eqz v0, :cond_7
:goto_5
monitor-exit p0
return-void
:try_start_7
:cond_7
invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
move-result-wide v0
iput-wide v0, p0, Lio/fabric/sdk/android/services/b/t;->d:J
const-wide/16 v0, 0x0
iput-wide v0, p0, Lio/fabric/sdk/android/services/b/t;->e:J
:try_end_11
.catchall {:try_start_7 .. :try_end_11} :catchall_12
goto :goto_5
:catchall_12
move-exception v0
monitor-exit p0
throw v0
.end method
.method public declared-synchronized b()V
.registers 5
monitor-enter p0
:try_start_1
iget-boolean v0, p0, Lio/fabric/sdk/android/services/b/t;->c:Z
:try_end_3
.catchall {:try_start_1 .. :try_end_3} :catchall_1c
if-eqz v0, :cond_7
:goto_5
:cond_5
monitor-exit p0
return-void
:try_start_7
:cond_7
iget-wide v0, p0, Lio/fabric/sdk/android/services/b/t;->e:J
const-wide/16 v2, 0x0
cmp-long v0, v0, v2
if-nez v0, :cond_5
invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
move-result-wide v0
iget-wide v2, p0, Lio/fabric/sdk/android/services/b/t;->d:J
sub-long/2addr v0, v2
iput-wide v0, p0, Lio/fabric/sdk/android/services/b/t;->e:J
invoke-direct {p0}, Lio/fabric/sdk/android/services/b/t;->c()V
:try_end_1b
.catchall {:try_start_7 .. :try_end_1b} :catchall_1c
goto :goto_5
:catchall_1c
move-exception v0
monitor-exit p0
throw v0
.end method