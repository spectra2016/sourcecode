.class public Lio/fabric/sdk/android/services/c/i;
.super Ljava/lang/Object;
.source "TimeBasedFileRollOverRunnable.java"
.implements Ljava/lang/Runnable;
.field private final a:Landroid/content/Context;
.field private final b:Lio/fabric/sdk/android/services/c/e;
.method public constructor <init>(Landroid/content/Context;Lio/fabric/sdk/android/services/c/e;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lio/fabric/sdk/android/services/c/i;->a:Landroid/content/Context;
iput-object p2, p0, Lio/fabric/sdk/android/services/c/i;->b:Lio/fabric/sdk/android/services/c/e;
return-void
.end method
.method public run()V
.registers 4
:try_start_0
iget-object v0, p0, Lio/fabric/sdk/android/services/c/i;->a:Landroid/content/Context;
const-string v1, "Performing time based file roll over."
invoke-static {v0, v1}, Lio/fabric/sdk/android/services/b/i;->a(Landroid/content/Context;Ljava/lang/String;)V
iget-object v0, p0, Lio/fabric/sdk/android/services/c/i;->b:Lio/fabric/sdk/android/services/c/e;
invoke-interface {v0}, Lio/fabric/sdk/android/services/c/e;->rollFileOver()Z
move-result v0
if-nez v0, :cond_14
iget-object v0, p0, Lio/fabric/sdk/android/services/c/i;->b:Lio/fabric/sdk/android/services/c/e;
invoke-interface {v0}, Lio/fabric/sdk/android/services/c/e;->cancelTimeBasedFileRollOver()V
:goto_14
:try_end_14
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_14} :catch_15
:cond_14
return-void
:catch_15
move-exception v0
iget-object v1, p0, Lio/fabric/sdk/android/services/c/i;->a:Landroid/content/Context;
const-string v2, "Failed to roll over file"
invoke-static {v1, v2, v0}, Lio/fabric/sdk/android/services/b/i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V
goto :goto_14
.end method