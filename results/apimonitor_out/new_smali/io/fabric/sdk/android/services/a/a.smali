.class public abstract Lio/fabric/sdk/android/services/a/a;
.super Ljava/lang/Object;
.source "AbstractValueCache.java"
.implements Lio/fabric/sdk/android/services/a/c;
.field private final a:Lio/fabric/sdk/android/services/a/c;
.method public constructor <init>(Lio/fabric/sdk/android/services/a/c;)V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lio/fabric/sdk/android/services/a/a;->a:Lio/fabric/sdk/android/services/a/c;
return-void
.end method
.method private b(Landroid/content/Context;Ljava/lang/Object;)V
.registers 4
if-nez p2, :cond_8
new-instance v0, Ljava/lang/NullPointerException;
invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V
throw v0
:cond_8
invoke-virtual {p0, p1, p2}, Lio/fabric/sdk/android/services/a/a;->a(Landroid/content/Context;Ljava/lang/Object;)V
return-void
.end method
.method protected abstract a(Landroid/content/Context;)Ljava/lang/Object;
.end method
.method public final declared-synchronized a(Landroid/content/Context;Lio/fabric/sdk/android/services/a/d;)Ljava/lang/Object;
.registers 4
monitor-enter p0
:try_start_1
invoke-virtual {p0, p1}, Lio/fabric/sdk/android/services/a/a;->a(Landroid/content/Context;)Ljava/lang/Object;
move-result-object v0
if-nez v0, :cond_14
iget-object v0, p0, Lio/fabric/sdk/android/services/a/a;->a:Lio/fabric/sdk/android/services/a/c;
if-eqz v0, :cond_16
iget-object v0, p0, Lio/fabric/sdk/android/services/a/a;->a:Lio/fabric/sdk/android/services/a/c;
invoke-interface {v0, p1, p2}, Lio/fabric/sdk/android/services/a/c;->a(Landroid/content/Context;Lio/fabric/sdk/android/services/a/d;)Ljava/lang/Object;
move-result-object v0
:goto_11
invoke-direct {p0, p1, v0}, Lio/fabric/sdk/android/services/a/a;->b(Landroid/content/Context;Ljava/lang/Object;)V
:cond_14
:try_end_14
.catchall {:try_start_1 .. :try_end_14} :catchall_1b
monitor-exit p0
return-object v0
:try_start_16
:cond_16
invoke-interface {p2, p1}, Lio/fabric/sdk/android/services/a/d;->load(Landroid/content/Context;)Ljava/lang/Object;
:try_end_19
.catchall {:try_start_16 .. :try_end_19} :catchall_1b
move-result-object v0
goto :goto_11
:catchall_1b
move-exception v0
monitor-exit p0
throw v0
.end method
.method protected abstract a(Landroid/content/Context;Ljava/lang/Object;)V
.end method