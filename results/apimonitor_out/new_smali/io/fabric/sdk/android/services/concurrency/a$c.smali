.class  Lio/fabric/sdk/android/services/concurrency/a$c;
.super Ljava/lang/Object;
.source "AsyncTask.java"
.implements Ljava/util/concurrent/Executor;
.field final a:Ljava/util/LinkedList;
.field  b:Ljava/lang/Runnable;
.method private constructor <init>()V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
new-instance v0, Ljava/util/LinkedList;
invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V
iput-object v0, p0, Lio/fabric/sdk/android/services/concurrency/a$c;->a:Ljava/util/LinkedList;
return-void
.end method
.method synthetic constructor <init>(Lio/fabric/sdk/android/services/concurrency/a$1;)V
.registers 2
invoke-direct {p0}, Lio/fabric/sdk/android/services/concurrency/a$c;-><init>()V
return-void
.end method
.method protected declared-synchronized a()V
.registers 3
monitor-enter p0
:try_start_1
iget-object v0, p0, Lio/fabric/sdk/android/services/concurrency/a$c;->a:Ljava/util/LinkedList;
invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Runnable;
iput-object v0, p0, Lio/fabric/sdk/android/services/concurrency/a$c;->b:Ljava/lang/Runnable;
if-eqz v0, :cond_14
sget-object v0, Lio/fabric/sdk/android/services/concurrency/a;->b:Ljava/util/concurrent/Executor;
iget-object v1, p0, Lio/fabric/sdk/android/services/concurrency/a$c;->b:Ljava/lang/Runnable;
invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
:try_end_14
.catchall {:try_start_1 .. :try_end_14} :catchall_16
:cond_14
monitor-exit p0
return-void
:catchall_16
move-exception v0
monitor-exit p0
throw v0
.end method
.method public declared-synchronized execute(Ljava/lang/Runnable;)V
.registers 4
monitor-enter p0
:try_start_1
iget-object v0, p0, Lio/fabric/sdk/android/services/concurrency/a$c;->a:Ljava/util/LinkedList;
new-instance v1, Lio/fabric/sdk/android/services/concurrency/a$c$1;
invoke-direct {v1, p0, p1}, Lio/fabric/sdk/android/services/concurrency/a$c$1;-><init>(Lio/fabric/sdk/android/services/concurrency/a$c;Ljava/lang/Runnable;)V
invoke-virtual {v0, v1}, Ljava/util/LinkedList;->offer(Ljava/lang/Object;)Z
iget-object v0, p0, Lio/fabric/sdk/android/services/concurrency/a$c;->b:Ljava/lang/Runnable;
if-nez v0, :cond_12
invoke-virtual {p0}, Lio/fabric/sdk/android/services/concurrency/a$c;->a()V
:cond_12
:try_end_12
.catchall {:try_start_1 .. :try_end_12} :catchall_14
monitor-exit p0
return-void
:catchall_14
move-exception v0
monitor-exit p0
throw v0
.end method