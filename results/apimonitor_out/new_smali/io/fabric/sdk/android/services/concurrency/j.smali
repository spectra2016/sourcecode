.class public Lio/fabric/sdk/android/services/concurrency/j;
.super Ljava/lang/Object;
.source "PriorityTask.java"
.implements Lio/fabric/sdk/android/services/concurrency/b;
.implements Lio/fabric/sdk/android/services/concurrency/i;
.implements Lio/fabric/sdk/android/services/concurrency/l;
.field private final dependencies:Ljava/util/List;
.field private final hasRun:Ljava/util/concurrent/atomic/AtomicBoolean;
.field private final throwable:Ljava/util/concurrent/atomic/AtomicReference;
.method public constructor <init>()V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Lio/fabric/sdk/android/services/concurrency/j;->dependencies:Ljava/util/List;
new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;
const/4 v1, 0x0
invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V
iput-object v0, p0, Lio/fabric/sdk/android/services/concurrency/j;->hasRun:Ljava/util/concurrent/atomic/AtomicBoolean;
new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;
const/4 v1, 0x0
invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V
iput-object v0, p0, Lio/fabric/sdk/android/services/concurrency/j;->throwable:Ljava/util/concurrent/atomic/AtomicReference;
return-void
.end method
.method public static isProperDelegate(Ljava/lang/Object;)Z
.registers 5
const/4 v3, 0x0
:try_start_1
move-object v0, p0
check-cast v0, Lio/fabric/sdk/android/services/concurrency/b;
move-object v1, v0
move-object v0, p0
check-cast v0, Lio/fabric/sdk/android/services/concurrency/l;
move-object v2, v0
check-cast p0, Lio/fabric/sdk/android/services/concurrency/i;
:try_end_b
.catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_b} :catch_15
if-eqz v1, :cond_13
if-eqz v2, :cond_13
if-eqz p0, :cond_13
const/4 v1, 0x1
:goto_12
return v1
:cond_13
move v1, v3
goto :goto_12
:catch_15
move-exception v1
move v1, v3
goto :goto_12
.end method
.method public declared-synchronized addDependency(Lio/fabric/sdk/android/services/concurrency/l;)V
.registers 3
monitor-enter p0
:try_start_1
iget-object v0, p0, Lio/fabric/sdk/android/services/concurrency/j;->dependencies:Ljava/util/List;
invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
:try_end_6
.catchall {:try_start_1 .. :try_end_6} :catchall_8
monitor-exit p0
return-void
:catchall_8
move-exception v0
monitor-exit p0
throw v0
.end method
.method public bridge synthetic addDependency(Ljava/lang/Object;)V
.registers 2
check-cast p1, Lio/fabric/sdk/android/services/concurrency/l;
invoke-virtual {p0, p1}, Lio/fabric/sdk/android/services/concurrency/j;->addDependency(Lio/fabric/sdk/android/services/concurrency/l;)V
return-void
.end method
.method public areDependenciesMet()Z
.registers 3
invoke-virtual {p0}, Lio/fabric/sdk/android/services/concurrency/j;->getDependencies()Ljava/util/Collection;
move-result-object v0
invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;
move-result-object v1
:cond_8
invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_1c
invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/services/concurrency/l;
invoke-interface {v0}, Lio/fabric/sdk/android/services/concurrency/l;->isFinished()Z
move-result v0
if-nez v0, :cond_8
const/4 v0, 0x0
:goto_1b
return v0
:cond_1c
const/4 v0, 0x1
goto :goto_1b
.end method
.method public compareTo(Ljava/lang/Object;)I
.registers 3
invoke-static {p0, p1}, Lio/fabric/sdk/android/services/concurrency/e;->a(Lio/fabric/sdk/android/services/concurrency/i;Ljava/lang/Object;)I
move-result v0
return v0
.end method
.method public declared-synchronized getDependencies()Ljava/util/Collection;
.registers 2
monitor-enter p0
:try_start_1
iget-object v0, p0, Lio/fabric/sdk/android/services/concurrency/j;->dependencies:Ljava/util/List;
invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;
:try_end_6
.catchall {:try_start_1 .. :try_end_6} :catchall_9
move-result-object v0
monitor-exit p0
return-object v0
:catchall_9
move-exception v0
monitor-exit p0
throw v0
.end method
.method public getError()Ljava/lang/Throwable;
.registers 2
iget-object v0, p0, Lio/fabric/sdk/android/services/concurrency/j;->throwable:Ljava/util/concurrent/atomic/AtomicReference;
invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Throwable;
return-object v0
.end method
.method public getPriority()Lio/fabric/sdk/android/services/concurrency/e;
.registers 2
sget-object v0, Lio/fabric/sdk/android/services/concurrency/e;->b:Lio/fabric/sdk/android/services/concurrency/e;
return-object v0
.end method
.method public isFinished()Z
.registers 2
iget-object v0, p0, Lio/fabric/sdk/android/services/concurrency/j;->hasRun:Ljava/util/concurrent/atomic/AtomicBoolean;
invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
move-result v0
return v0
.end method
.method public setError(Ljava/lang/Throwable;)V
.registers 3
iget-object v0, p0, Lio/fabric/sdk/android/services/concurrency/j;->throwable:Ljava/util/concurrent/atomic/AtomicReference;
invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
return-void
.end method
.method public declared-synchronized setFinished(Z)V
.registers 3
monitor-enter p0
:try_start_1
iget-object v0, p0, Lio/fabric/sdk/android/services/concurrency/j;->hasRun:Ljava/util/concurrent/atomic/AtomicBoolean;
invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
:try_end_6
.catchall {:try_start_1 .. :try_end_6} :catchall_8
monitor-exit p0
return-void
:catchall_8
move-exception v0
monitor-exit p0
throw v0
.end method