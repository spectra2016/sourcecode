.class public Lio/fabric/sdk/android/services/d/b;
.super Ljava/lang/Object;
.source "FileStoreImpl.java"
.implements Lio/fabric/sdk/android/services/d/a;
.field private final a:Landroid/content/Context;
.field private final b:Ljava/lang/String;
.field private final c:Ljava/lang/String;
.method public constructor <init>(Lio/fabric/sdk/android/h;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
invoke-virtual {p1}, Lio/fabric/sdk/android/h;->getContext()Landroid/content/Context;
move-result-object v0
if-nez v0, :cond_11
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "Cannot get directory before context has been set. Call Fabric.with() first"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_11
invoke-virtual {p1}, Lio/fabric/sdk/android/h;->getContext()Landroid/content/Context;
move-result-object v0
iput-object v0, p0, Lio/fabric/sdk/android/services/d/b;->a:Landroid/content/Context;
invoke-virtual {p1}, Lio/fabric/sdk/android/h;->getPath()Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lio/fabric/sdk/android/services/d/b;->b:Ljava/lang/String;
new-instance v0, Ljava/lang/StringBuilder;
invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
const-string v1, "Android/"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
iget-object v1, p0, Lio/fabric/sdk/android/services/d/b;->a:Landroid/content/Context;
invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lio/fabric/sdk/android/services/d/b;->c:Ljava/lang/String;
return-void
.end method
.method public a()Ljava/io/File;
.registers 2
iget-object v0, p0, Lio/fabric/sdk/android/services/d/b;->a:Landroid/content/Context;
invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;
move-result-object v0
invoke-virtual {p0, v0}, Lio/fabric/sdk/android/services/d/b;->a(Ljava/io/File;)Ljava/io/File;
move-result-object v0
return-object v0
.end method
.method  a(Ljava/io/File;)Ljava/io/File;
.registers 5
if-eqz p1, :cond_1c
invoke-virtual {p1}, Ljava/io/File;->exists()Z
move-result v0
if-nez v0, :cond_e
invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z
move-result v0
if-eqz v0, :cond_f
:cond_e
:goto_e
return-object p1
:cond_f
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "Fabric"
const-string v2, "Couldn\'t create file"
invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->d(Ljava/lang/String;Ljava/lang/String;)V
:goto_1a
const/4 p1, 0x0
goto :goto_e
:cond_1c
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "Fabric"
const-string v2, "Null File"
invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
goto :goto_1a
.end method