.class public final Lio/fabric/sdk/android/services/concurrency/k$a;
.super Ljava/lang/Object;
.source "PriorityThreadPoolExecutor.java"
.implements Ljava/util/concurrent/ThreadFactory;
.field private final a:I
.method public constructor <init>(I)V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput p1, p0, Lio/fabric/sdk/android/services/concurrency/k$a;->a:I
return-void
.end method
.method public newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
.registers 4
new-instance v0, Ljava/lang/Thread;
invoke-direct {v0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V
iget v1, p0, Lio/fabric/sdk/android/services/concurrency/k$a;->a:I
invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V
const-string v1, "Queue"
invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V
return-object v0
.end method