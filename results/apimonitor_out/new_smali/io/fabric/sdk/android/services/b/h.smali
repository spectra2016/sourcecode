.class public abstract Lio/fabric/sdk/android/services/b/h;
.super Ljava/lang/Object;
.source "BackgroundPriorityRunnable.java"
.implements Ljava/lang/Runnable;
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method protected abstract onRun()V
.end method
.method public final run()V
.registers 2
const/16 v0, 0xa
invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
invoke-virtual {p0}, Lio/fabric/sdk/android/services/b/h;->onRun()V
return-void
.end method