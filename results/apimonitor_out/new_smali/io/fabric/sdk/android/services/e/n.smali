.class public Lio/fabric/sdk/android/services/e/n;
.super Ljava/lang/Object;
.source "IconRequest.java"
.field public final a:Ljava/lang/String;
.field public final b:I
.field public final c:I
.field public final d:I
.method public constructor <init>(Ljava/lang/String;III)V
.registers 5
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lio/fabric/sdk/android/services/e/n;->a:Ljava/lang/String;
iput p2, p0, Lio/fabric/sdk/android/services/e/n;->b:I
iput p3, p0, Lio/fabric/sdk/android/services/e/n;->c:I
iput p4, p0, Lio/fabric/sdk/android/services/e/n;->d:I
return-void
.end method
.method public static a(Landroid/content/Context;Ljava/lang/String;)Lio/fabric/sdk/android/services/e/n;
.registers 8
const/4 v1, 0x0
if-eqz p1, :cond_48
:try_start_3
invoke-static {p0}, Lio/fabric/sdk/android/services/b/i;->l(Landroid/content/Context;)I
move-result v2
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v3, "Fabric"
new-instance v4, Ljava/lang/StringBuilder;
invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
const-string v5, "App icon resource ID is "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v4
invoke-interface {v0, v3, v4}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
new-instance v3, Landroid/graphics/BitmapFactory$Options;
invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V
const/4 v0, 0x1
iput-boolean v0, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z
invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-static {v0, v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
new-instance v0, Lio/fabric/sdk/android/services/e/n;
iget v4, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I
iget v3, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I
invoke-direct {v0, p1, v2, v4, v3}, Lio/fabric/sdk/android/services/e/n;-><init>(Ljava/lang/String;III)V
:goto_3b
:try_end_3b
.catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3b} :catch_3c
return-object v0
:catch_3c
move-exception v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v2
const-string v3, "Fabric"
const-string v4, "Failed to load icon"
invoke-interface {v2, v3, v4, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
:cond_48
move-object v0, v1
goto :goto_3b
.end method