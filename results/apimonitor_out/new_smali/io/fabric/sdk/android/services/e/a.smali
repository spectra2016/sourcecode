.class abstract Lio/fabric/sdk/android/services/e/a;
.super Lio/fabric/sdk/android/services/b/a;
.source "AbstractAppSpiCall.java"
.method public constructor <init>(Lio/fabric/sdk/android/h;Ljava/lang/String;Ljava/lang/String;Lio/fabric/sdk/android/services/network/d;Lio/fabric/sdk/android/services/network/c;)V
.registers 6
invoke-direct/range {p0 .. p5}, Lio/fabric/sdk/android/services/b/a;-><init>(Lio/fabric/sdk/android/h;Ljava/lang/String;Ljava/lang/String;Lio/fabric/sdk/android/services/network/d;Lio/fabric/sdk/android/services/network/c;)V
return-void
.end method
.method private a(Lio/fabric/sdk/android/services/network/HttpRequest;Lio/fabric/sdk/android/services/e/d;)Lio/fabric/sdk/android/services/network/HttpRequest;
.registers 6
const-string v0, "X-CRASHLYTICS-API-KEY"
iget-object v1, p2, Lio/fabric/sdk/android/services/e/d;->a:Ljava/lang/String;
invoke-virtual {p1, v0, v1}, Lio/fabric/sdk/android/services/network/HttpRequest;->a(Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
const-string v1, "X-CRASHLYTICS-API-CLIENT-TYPE"
const-string v2, "android"
invoke-virtual {v0, v1, v2}, Lio/fabric/sdk/android/services/network/HttpRequest;->a(Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
const-string v1, "X-CRASHLYTICS-API-CLIENT-VERSION"
iget-object v2, p0, Lio/fabric/sdk/android/services/e/a;->kit:Lio/fabric/sdk/android/h;
invoke-virtual {v2}, Lio/fabric/sdk/android/h;->getVersion()Ljava/lang/String;
move-result-object v2
invoke-virtual {v0, v1, v2}, Lio/fabric/sdk/android/services/network/HttpRequest;->a(Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
return-object v0
.end method
.method private b(Lio/fabric/sdk/android/services/network/HttpRequest;Lio/fabric/sdk/android/services/e/d;)Lio/fabric/sdk/android/services/network/HttpRequest;
.registers 10
const-string v0, "app[identifier]"
iget-object v1, p2, Lio/fabric/sdk/android/services/e/d;->b:Ljava/lang/String;
invoke-virtual {p1, v0, v1}, Lio/fabric/sdk/android/services/network/HttpRequest;->e(Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
const-string v1, "app[name]"
iget-object v2, p2, Lio/fabric/sdk/android/services/e/d;->f:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Lio/fabric/sdk/android/services/network/HttpRequest;->e(Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
const-string v1, "app[display_version]"
iget-object v2, p2, Lio/fabric/sdk/android/services/e/d;->c:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Lio/fabric/sdk/android/services/network/HttpRequest;->e(Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
const-string v1, "app[build_version]"
iget-object v2, p2, Lio/fabric/sdk/android/services/e/d;->d:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Lio/fabric/sdk/android/services/network/HttpRequest;->e(Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
const-string v1, "app[source]"
iget v2, p2, Lio/fabric/sdk/android/services/e/d;->g:I
invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v2
invoke-virtual {v0, v1, v2}, Lio/fabric/sdk/android/services/network/HttpRequest;->a(Ljava/lang/String;Ljava/lang/Number;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
const-string v1, "app[minimum_sdk_version]"
iget-object v2, p2, Lio/fabric/sdk/android/services/e/d;->h:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Lio/fabric/sdk/android/services/network/HttpRequest;->e(Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
const-string v1, "app[built_sdk_version]"
iget-object v2, p2, Lio/fabric/sdk/android/services/e/d;->i:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Lio/fabric/sdk/android/services/network/HttpRequest;->e(Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v2
iget-object v0, p2, Lio/fabric/sdk/android/services/e/d;->e:Ljava/lang/String;
invoke-static {v0}, Lio/fabric/sdk/android/services/b/i;->c(Ljava/lang/String;)Z
move-result v0
if-nez v0, :cond_4b
const-string v0, "app[instance_identifier]"
iget-object v1, p2, Lio/fabric/sdk/android/services/e/d;->e:Ljava/lang/String;
invoke-virtual {v2, v0, v1}, Lio/fabric/sdk/android/services/network/HttpRequest;->e(Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
:cond_4b
iget-object v0, p2, Lio/fabric/sdk/android/services/e/d;->j:Lio/fabric/sdk/android/services/e/n;
if-eqz v0, :cond_96
const/4 v1, 0x0
:try_start_50
iget-object v0, p0, Lio/fabric/sdk/android/services/e/a;->kit:Lio/fabric/sdk/android/h;
invoke-virtual {v0}, Lio/fabric/sdk/android/h;->getContext()Landroid/content/Context;
move-result-object v0
invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
iget-object v3, p2, Lio/fabric/sdk/android/services/e/d;->j:Lio/fabric/sdk/android/services/e/n;
iget v3, v3, Lio/fabric/sdk/android/services/e/n;->b:I
invoke-virtual {v0, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
move-result-object v1
const-string v0, "app[icon][hash]"
iget-object v3, p2, Lio/fabric/sdk/android/services/e/d;->j:Lio/fabric/sdk/android/services/e/n;
iget-object v3, v3, Lio/fabric/sdk/android/services/e/n;->a:Ljava/lang/String;
invoke-virtual {v2, v0, v3}, Lio/fabric/sdk/android/services/network/HttpRequest;->e(Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
const-string v3, "app[icon][data]"
const-string v4, "icon.png"
const-string v5, "application/octet-stream"
invoke-virtual {v0, v3, v4, v5, v1}, Lio/fabric/sdk/android/services/network/HttpRequest;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
const-string v3, "app[icon][width]"
iget-object v4, p2, Lio/fabric/sdk/android/services/e/d;->j:Lio/fabric/sdk/android/services/e/n;
iget v4, v4, Lio/fabric/sdk/android/services/e/n;->c:I
invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v4
invoke-virtual {v0, v3, v4}, Lio/fabric/sdk/android/services/network/HttpRequest;->a(Ljava/lang/String;Ljava/lang/Number;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
const-string v3, "app[icon][height]"
iget-object v4, p2, Lio/fabric/sdk/android/services/e/d;->j:Lio/fabric/sdk/android/services/e/n;
iget v4, v4, Lio/fabric/sdk/android/services/e/n;->d:I
invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v4
invoke-virtual {v0, v3, v4}, Lio/fabric/sdk/android/services/network/HttpRequest;->a(Ljava/lang/String;Ljava/lang/Number;)Lio/fabric/sdk/android/services/network/HttpRequest;
:try_end_91
.catchall {:try_start_50 .. :try_end_91} :catchall_ea
.catch Landroid/content/res/Resources$NotFoundException; {:try_start_50 .. :try_end_91} :catch_c3
const-string v0, "Failed to close app icon InputStream."
invoke-static {v1, v0}, Lio/fabric/sdk/android/services/b/i;->a(Ljava/io/Closeable;Ljava/lang/String;)V
:cond_96
:goto_96
iget-object v0, p2, Lio/fabric/sdk/android/services/e/d;->k:Ljava/util/Collection;
if-eqz v0, :cond_f1
iget-object v0, p2, Lio/fabric/sdk/android/services/e/d;->k:Ljava/util/Collection;
invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;
move-result-object v1
:goto_a0
invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_f1
invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/j;
invoke-virtual {p0, v0}, Lio/fabric/sdk/android/services/e/a;->a(Lio/fabric/sdk/android/j;)Ljava/lang/String;
move-result-object v3
invoke-virtual {v0}, Lio/fabric/sdk/android/j;->b()Ljava/lang/String;
move-result-object v4
invoke-virtual {v2, v3, v4}, Lio/fabric/sdk/android/services/network/HttpRequest;->e(Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
invoke-virtual {p0, v0}, Lio/fabric/sdk/android/services/e/a;->b(Lio/fabric/sdk/android/j;)Ljava/lang/String;
move-result-object v3
invoke-virtual {v0}, Lio/fabric/sdk/android/j;->c()Ljava/lang/String;
move-result-object v0
invoke-virtual {v2, v3, v0}, Lio/fabric/sdk/android/services/network/HttpRequest;->e(Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
goto :goto_a0
:catch_c3
move-exception v0
:try_start_c4
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v3
const-string v4, "Fabric"
new-instance v5, Ljava/lang/StringBuilder;
invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
const-string v6, "Failed to find app icon with resource ID: "
invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v5
iget-object v6, p2, Lio/fabric/sdk/android/services/e/d;->j:Lio/fabric/sdk/android/services/e/n;
iget v6, v6, Lio/fabric/sdk/android/services/e/n;->b:I
invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v5
invoke-interface {v3, v4, v5, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
:try_end_e4
.catchall {:try_start_c4 .. :try_end_e4} :catchall_ea
const-string v0, "Failed to close app icon InputStream."
invoke-static {v1, v0}, Lio/fabric/sdk/android/services/b/i;->a(Ljava/io/Closeable;Ljava/lang/String;)V
goto :goto_96
:catchall_ea
move-exception v0
const-string v2, "Failed to close app icon InputStream."
invoke-static {v1, v2}, Lio/fabric/sdk/android/services/b/i;->a(Ljava/io/Closeable;Ljava/lang/String;)V
throw v0
:cond_f1
return-object v2
.end method
.method  a(Lio/fabric/sdk/android/j;)Ljava/lang/String;
.registers 7
sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;
const-string v1, "app[build][libraries][%s][version]"
const/4 v2, 0x1
new-array v2, v2, [Ljava/lang/Object;
const/4 v3, 0x0
invoke-virtual {p1}, Lio/fabric/sdk/android/j;->a()Ljava/lang/String;
move-result-object v4
aput-object v4, v2, v3
invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public a(Lio/fabric/sdk/android/services/e/d;)Z
.registers 8
invoke-virtual {p0}, Lio/fabric/sdk/android/services/e/a;->getHttpRequest()Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
invoke-direct {p0, v0, p1}, Lio/fabric/sdk/android/services/e/a;->a(Lio/fabric/sdk/android/services/network/HttpRequest;Lio/fabric/sdk/android/services/e/d;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
invoke-direct {p0, v0, p1}, Lio/fabric/sdk/android/services/e/a;->b(Lio/fabric/sdk/android/services/network/HttpRequest;Lio/fabric/sdk/android/services/e/d;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v1
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v2, "Fabric"
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "Sending app info to "
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {p0}, Lio/fabric/sdk/android/services/e/a;->getUrl()Ljava/lang/String;
move-result-object v4
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v3
invoke-interface {v0, v2, v3}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
iget-object v0, p1, Lio/fabric/sdk/android/services/e/d;->j:Lio/fabric/sdk/android/services/e/n;
if-eqz v0, :cond_7e
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v2, "Fabric"
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "App icon hash is "
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
iget-object v4, p1, Lio/fabric/sdk/android/services/e/d;->j:Lio/fabric/sdk/android/services/e/n;
iget-object v4, v4, Lio/fabric/sdk/android/services/e/n;->a:Ljava/lang/String;
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v3
invoke-interface {v0, v2, v3}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v2, "Fabric"
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "App icon size is "
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
iget-object v4, p1, Lio/fabric/sdk/android/services/e/d;->j:Lio/fabric/sdk/android/services/e/n;
iget v4, v4, Lio/fabric/sdk/android/services/e/n;->c:I
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v3
const-string v4, "x"
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
iget-object v4, p1, Lio/fabric/sdk/android/services/e/d;->j:Lio/fabric/sdk/android/services/e/n;
iget v4, v4, Lio/fabric/sdk/android/services/e/n;->d:I
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v3
invoke-interface {v0, v2, v3}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
:cond_7e
invoke-virtual {v1}, Lio/fabric/sdk/android/services/network/HttpRequest;->b()I
move-result v2
const-string v0, "POST"
invoke-virtual {v1}, Lio/fabric/sdk/android/services/network/HttpRequest;->p()Ljava/lang/String;
move-result-object v3
invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_da
const-string v0, "Create"
:goto_90
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v3
const-string v4, "Fabric"
new-instance v5, Ljava/lang/StringBuilder;
invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v5, " app request ID: "
invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v5, "X-REQUEST-ID"
invoke-virtual {v1, v5}, Lio/fabric/sdk/android/services/network/HttpRequest;->b(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-interface {v3, v4, v0}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "Fabric"
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "Result was "
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v3
invoke-interface {v0, v1, v3}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-static {v2}, Lio/fabric/sdk/android/services/b/r;->a(I)I
move-result v0
if-nez v0, :cond_dd
const/4 v0, 0x1
:goto_d9
return v0
:cond_da
const-string v0, "Update"
goto :goto_90
:cond_dd
const/4 v0, 0x0
goto :goto_d9
.end method
.method  b(Lio/fabric/sdk/android/j;)Ljava/lang/String;
.registers 7
sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;
const-string v1, "app[build][libraries][%s][type]"
const/4 v2, 0x1
new-array v2, v2, [Ljava/lang/Object;
const/4 v3, 0x0
invoke-virtual {p1}, Lio/fabric/sdk/android/j;->a()Ljava/lang/String;
move-result-object v4
aput-object v4, v2, v3
invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method