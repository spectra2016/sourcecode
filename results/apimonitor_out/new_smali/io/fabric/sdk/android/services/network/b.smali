.class public Lio/fabric/sdk/android/services/network/b;
.super Ljava/lang/Object;
.source "DefaultHttpRequestFactory.java"
.implements Lio/fabric/sdk/android/services/network/d;
.field private final a:Lio/fabric/sdk/android/k;
.field private b:Lio/fabric/sdk/android/services/network/f;
.field private c:Ljavax/net/ssl/SSLSocketFactory;
.field private d:Z
.method public constructor <init>()V
.registers 2
new-instance v0, Lio/fabric/sdk/android/b;
invoke-direct {v0}, Lio/fabric/sdk/android/b;-><init>()V
invoke-direct {p0, v0}, Lio/fabric/sdk/android/services/network/b;-><init>(Lio/fabric/sdk/android/k;)V
return-void
.end method
.method public constructor <init>(Lio/fabric/sdk/android/k;)V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lio/fabric/sdk/android/services/network/b;->a:Lio/fabric/sdk/android/k;
return-void
.end method
.method private declared-synchronized a()V
.registers 2
monitor-enter p0
const/4 v0, 0x0
:try_start_2
iput-boolean v0, p0, Lio/fabric/sdk/android/services/network/b;->d:Z
const/4 v0, 0x0
iput-object v0, p0, Lio/fabric/sdk/android/services/network/b;->c:Ljavax/net/ssl/SSLSocketFactory;
:try_end_7
.catchall {:try_start_2 .. :try_end_7} :catchall_9
monitor-exit p0
return-void
:catchall_9
move-exception v0
monitor-exit p0
throw v0
.end method
.method private a(Ljava/lang/String;)Z
.registers 4
if-eqz p1, :cond_12
sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;
invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
move-result-object v0
const-string v1, "https"
invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_12
const/4 v0, 0x1
:goto_11
return v0
:cond_12
const/4 v0, 0x0
goto :goto_11
.end method
.method private declared-synchronized b()Ljavax/net/ssl/SSLSocketFactory;
.registers 2
monitor-enter p0
:try_start_1
iget-object v0, p0, Lio/fabric/sdk/android/services/network/b;->c:Ljavax/net/ssl/SSLSocketFactory;
if-nez v0, :cond_f
iget-boolean v0, p0, Lio/fabric/sdk/android/services/network/b;->d:Z
if-nez v0, :cond_f
invoke-direct {p0}, Lio/fabric/sdk/android/services/network/b;->c()Ljavax/net/ssl/SSLSocketFactory;
move-result-object v0
iput-object v0, p0, Lio/fabric/sdk/android/services/network/b;->c:Ljavax/net/ssl/SSLSocketFactory;
:cond_f
iget-object v0, p0, Lio/fabric/sdk/android/services/network/b;->c:Ljavax/net/ssl/SSLSocketFactory;
:try_end_11
.catchall {:try_start_1 .. :try_end_11} :catchall_13
monitor-exit p0
return-object v0
:catchall_13
move-exception v0
monitor-exit p0
throw v0
.end method
.method private declared-synchronized c()Ljavax/net/ssl/SSLSocketFactory;
.registers 5
monitor-enter p0
const/4 v0, 0x1
:try_start_2
iput-boolean v0, p0, Lio/fabric/sdk/android/services/network/b;->d:Z
:try_start_4
:try_end_4
.catchall {:try_start_2 .. :try_end_4} :catchall_21
iget-object v0, p0, Lio/fabric/sdk/android/services/network/b;->b:Lio/fabric/sdk/android/services/network/f;
invoke-static {v0}, Lio/fabric/sdk/android/services/network/e;->a(Lio/fabric/sdk/android/services/network/f;)Ljavax/net/ssl/SSLSocketFactory;
move-result-object v0
iget-object v1, p0, Lio/fabric/sdk/android/services/network/b;->a:Lio/fabric/sdk/android/k;
const-string v2, "Fabric"
const-string v3, "Custom SSL pinning enabled"
invoke-interface {v1, v2, v3}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
:try_end_13
.catchall {:try_start_4 .. :try_end_13} :catchall_21
.catch Ljava/lang/Exception; {:try_start_4 .. :try_end_13} :catch_15
:goto_13
monitor-exit p0
return-object v0
:catch_15
move-exception v0
:try_start_16
iget-object v1, p0, Lio/fabric/sdk/android/services/network/b;->a:Lio/fabric/sdk/android/k;
const-string v2, "Fabric"
const-string v3, "Exception while validating pinned certs"
invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
:try_end_1f
.catchall {:try_start_16 .. :try_end_1f} :catchall_21
const/4 v0, 0x0
goto :goto_13
:catchall_21
move-exception v0
monitor-exit p0
throw v0
.end method
.method public a(Lio/fabric/sdk/android/services/network/c;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
.registers 4
invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;
move-result-object v0
invoke-virtual {p0, p1, p2, v0}, Lio/fabric/sdk/android/services/network/b;->a(Lio/fabric/sdk/android/services/network/c;Ljava/lang/String;Ljava/util/Map;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
return-object v0
.end method
.method public a(Lio/fabric/sdk/android/services/network/c;Ljava/lang/String;Ljava/util/Map;)Lio/fabric/sdk/android/services/network/HttpRequest;
.registers 7
const/4 v2, 0x1
sget-object v0, Lio/fabric/sdk/android/services/network/b$1;->a:[I
invoke-virtual {p1}, Lio/fabric/sdk/android/services/network/c;->ordinal()I
move-result v1
aget v0, v0, v1
packed-switch v0, :pswitch_data_46
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "Unsupported HTTP method!"
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:pswitch_14
invoke-static {p2, p3, v2}, Lio/fabric/sdk/android/services/network/HttpRequest;->a(Ljava/lang/CharSequence;Ljava/util/Map;Z)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
move-object v1, v0
:goto_19
invoke-direct {p0, p2}, Lio/fabric/sdk/android/services/network/b;->a(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_32
iget-object v0, p0, Lio/fabric/sdk/android/services/network/b;->b:Lio/fabric/sdk/android/services/network/f;
if-eqz v0, :cond_32
invoke-direct {p0}, Lio/fabric/sdk/android/services/network/b;->b()Ljavax/net/ssl/SSLSocketFactory;
move-result-object v2
if-eqz v2, :cond_32
invoke-virtual {v1}, Lio/fabric/sdk/android/services/network/HttpRequest;->a()Ljava/net/HttpURLConnection;
move-result-object v0
check-cast v0, Ljavax/net/ssl/HttpsURLConnection;
invoke-virtual {v0, v2}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V
:cond_32
return-object v1
:pswitch_33
invoke-static {p2, p3, v2}, Lio/fabric/sdk/android/services/network/HttpRequest;->b(Ljava/lang/CharSequence;Ljava/util/Map;Z)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
move-object v1, v0
goto :goto_19
:pswitch_39
invoke-static {p2}, Lio/fabric/sdk/android/services/network/HttpRequest;->d(Ljava/lang/CharSequence;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
move-object v1, v0
goto :goto_19
:pswitch_3f
invoke-static {p2}, Lio/fabric/sdk/android/services/network/HttpRequest;->e(Ljava/lang/CharSequence;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v0
move-object v1, v0
goto :goto_19
nop
:pswitch_data_46
.packed-switch 0x1
:pswitch_14
:pswitch_33
:pswitch_39
:pswitch_3f
.end packed-switch
.end method
.method public a(Lio/fabric/sdk/android/services/network/f;)V
.registers 3
iget-object v0, p0, Lio/fabric/sdk/android/services/network/b;->b:Lio/fabric/sdk/android/services/network/f;
if-eq v0, p1, :cond_9
iput-object p1, p0, Lio/fabric/sdk/android/services/network/b;->b:Lio/fabric/sdk/android/services/network/f;
invoke-direct {p0}, Lio/fabric/sdk/android/services/network/b;->a()V
:cond_9
return-void
.end method