.class public abstract Lio/fabric/sdk/android/services/concurrency/f;
.super Lio/fabric/sdk/android/services/concurrency/a;
.source "PriorityAsyncTask.java"
.implements Lio/fabric/sdk/android/services/concurrency/b;
.implements Lio/fabric/sdk/android/services/concurrency/i;
.implements Lio/fabric/sdk/android/services/concurrency/l;
.field private final a:Lio/fabric/sdk/android/services/concurrency/j;
.method public constructor <init>()V
.registers 2
invoke-direct {p0}, Lio/fabric/sdk/android/services/concurrency/a;-><init>()V
new-instance v0, Lio/fabric/sdk/android/services/concurrency/j;
invoke-direct {v0}, Lio/fabric/sdk/android/services/concurrency/j;-><init>()V
iput-object v0, p0, Lio/fabric/sdk/android/services/concurrency/f;->a:Lio/fabric/sdk/android/services/concurrency/j;
return-void
.end method
.method public a(Lio/fabric/sdk/android/services/concurrency/l;)V
.registers 4
invoke-virtual {p0}, Lio/fabric/sdk/android/services/concurrency/f;->b()Lio/fabric/sdk/android/services/concurrency/a$d;
move-result-object v0
sget-object v1, Lio/fabric/sdk/android/services/concurrency/a$d;->a:Lio/fabric/sdk/android/services/concurrency/a$d;
if-eq v0, v1, :cond_10
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "Must not add Dependency after task is running"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_10
invoke-virtual {p0}, Lio/fabric/sdk/android/services/concurrency/f;->e()Lio/fabric/sdk/android/services/concurrency/b;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/services/concurrency/i;
check-cast v0, Lio/fabric/sdk/android/services/concurrency/b;
invoke-interface {v0, p1}, Lio/fabric/sdk/android/services/concurrency/b;->addDependency(Ljava/lang/Object;)V
return-void
.end method
.method public final varargs a(Ljava/util/concurrent/ExecutorService;[Ljava/lang/Object;)V
.registers 4
new-instance v0, Lio/fabric/sdk/android/services/concurrency/f$a;
invoke-direct {v0, p1, p0}, Lio/fabric/sdk/android/services/concurrency/f$a;-><init>(Ljava/util/concurrent/Executor;Lio/fabric/sdk/android/services/concurrency/f;)V
invoke-super {p0, v0, p2}, Lio/fabric/sdk/android/services/concurrency/a;->a(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Lio/fabric/sdk/android/services/concurrency/a;
return-void
.end method
.method public synthetic addDependency(Ljava/lang/Object;)V
.registers 2
check-cast p1, Lio/fabric/sdk/android/services/concurrency/l;
invoke-virtual {p0, p1}, Lio/fabric/sdk/android/services/concurrency/f;->a(Lio/fabric/sdk/android/services/concurrency/l;)V
return-void
.end method
.method public areDependenciesMet()Z
.registers 2
invoke-virtual {p0}, Lio/fabric/sdk/android/services/concurrency/f;->e()Lio/fabric/sdk/android/services/concurrency/b;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/services/concurrency/i;
check-cast v0, Lio/fabric/sdk/android/services/concurrency/b;
invoke-interface {v0}, Lio/fabric/sdk/android/services/concurrency/b;->areDependenciesMet()Z
move-result v0
return v0
.end method
.method public compareTo(Ljava/lang/Object;)I
.registers 3
invoke-static {p0, p1}, Lio/fabric/sdk/android/services/concurrency/e;->a(Lio/fabric/sdk/android/services/concurrency/i;Ljava/lang/Object;)I
move-result v0
return v0
.end method
.method public e()Lio/fabric/sdk/android/services/concurrency/b;
.registers 2
iget-object v0, p0, Lio/fabric/sdk/android/services/concurrency/f;->a:Lio/fabric/sdk/android/services/concurrency/j;
return-object v0
.end method
.method public getDependencies()Ljava/util/Collection;
.registers 2
invoke-virtual {p0}, Lio/fabric/sdk/android/services/concurrency/f;->e()Lio/fabric/sdk/android/services/concurrency/b;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/services/concurrency/i;
check-cast v0, Lio/fabric/sdk/android/services/concurrency/b;
invoke-interface {v0}, Lio/fabric/sdk/android/services/concurrency/b;->getDependencies()Ljava/util/Collection;
move-result-object v0
return-object v0
.end method
.method public getPriority()Lio/fabric/sdk/android/services/concurrency/e;
.registers 2
invoke-virtual {p0}, Lio/fabric/sdk/android/services/concurrency/f;->e()Lio/fabric/sdk/android/services/concurrency/b;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/services/concurrency/i;
invoke-interface {v0}, Lio/fabric/sdk/android/services/concurrency/i;->getPriority()Lio/fabric/sdk/android/services/concurrency/e;
move-result-object v0
return-object v0
.end method
.method public isFinished()Z
.registers 2
invoke-virtual {p0}, Lio/fabric/sdk/android/services/concurrency/f;->e()Lio/fabric/sdk/android/services/concurrency/b;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/services/concurrency/i;
check-cast v0, Lio/fabric/sdk/android/services/concurrency/l;
invoke-interface {v0}, Lio/fabric/sdk/android/services/concurrency/l;->isFinished()Z
move-result v0
return v0
.end method
.method public setError(Ljava/lang/Throwable;)V
.registers 3
invoke-virtual {p0}, Lio/fabric/sdk/android/services/concurrency/f;->e()Lio/fabric/sdk/android/services/concurrency/b;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/services/concurrency/i;
check-cast v0, Lio/fabric/sdk/android/services/concurrency/l;
invoke-interface {v0, p1}, Lio/fabric/sdk/android/services/concurrency/l;->setError(Ljava/lang/Throwable;)V
return-void
.end method
.method public setFinished(Z)V
.registers 3
invoke-virtual {p0}, Lio/fabric/sdk/android/services/concurrency/f;->e()Lio/fabric/sdk/android/services/concurrency/b;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/services/concurrency/i;
check-cast v0, Lio/fabric/sdk/android/services/concurrency/l;
invoke-interface {v0, p1}, Lio/fabric/sdk/android/services/concurrency/l;->setFinished(Z)V
return-void
.end method