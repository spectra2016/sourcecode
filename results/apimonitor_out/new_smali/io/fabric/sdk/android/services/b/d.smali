.class  Lio/fabric/sdk/android/services/b/d;
.super Ljava/lang/Object;
.source "AdvertisingInfoReflectionStrategy.java"
.implements Lio/fabric/sdk/android/services/b/f;
.field private final a:Landroid/content/Context;
.method public constructor <init>(Landroid/content/Context;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
move-result-object v0
iput-object v0, p0, Lio/fabric/sdk/android/services/b/d;->a:Landroid/content/Context;
return-void
.end method
.method private b()Ljava/lang/String;
.registers 4
:try_start_0
const-string v0, "com.google.android.gms.ads.identifier.AdvertisingIdClient$Info"
invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
const-string v1, "getId"
const/4 v2, 0x0
new-array v2, v2, [Ljava/lang/Class;
invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
move-result-object v0
invoke-direct {p0}, Lio/fabric/sdk/android/services/b/d;->d()Ljava/lang/Object;
move-result-object v1
const/4 v2, 0x0
new-array v2, v2, [Ljava/lang/Object;
invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
:try_end_1c
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1c} :catch_1d
:goto_1c
return-object v0
:catch_1d
move-exception v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "Fabric"
const-string v2, "Could not call getId on com.google.android.gms.ads.identifier.AdvertisingIdClient$Info"
invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->d(Ljava/lang/String;Ljava/lang/String;)V
const/4 v0, 0x0
goto :goto_1c
.end method
.method private c()Z
.registers 5
const/4 v1, 0x0
:try_start_1
const-string v0, "com.google.android.gms.ads.identifier.AdvertisingIdClient$Info"
invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
const-string v2, "isLimitAdTrackingEnabled"
const/4 v3, 0x0
new-array v3, v3, [Ljava/lang/Class;
invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
move-result-object v0
invoke-direct {p0}, Lio/fabric/sdk/android/services/b/d;->d()Ljava/lang/Object;
move-result-object v2
const/4 v3, 0x0
new-array v3, v3, [Ljava/lang/Object;
invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Boolean;
invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
:try_end_20
.catch Ljava/lang/Exception; {:try_start_1 .. :try_end_20} :catch_22
move-result v0
:goto_21
return v0
:catch_22
move-exception v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v2, "Fabric"
const-string v3, "Could not call isLimitAdTrackingEnabled on com.google.android.gms.ads.identifier.AdvertisingIdClient$Info"
invoke-interface {v0, v2, v3}, Lio/fabric/sdk/android/k;->d(Ljava/lang/String;Ljava/lang/String;)V
move v0, v1
goto :goto_21
.end method
.method private d()Ljava/lang/Object;
.registers 7
const/4 v0, 0x0
:try_start_1
const-string v1, "com.google.android.gms.ads.identifier.AdvertisingIdClient"
invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v1
const-string v2, "getAdvertisingIdInfo"
const/4 v3, 0x1
new-array v3, v3, [Ljava/lang/Class;
const/4 v4, 0x0
const-class v5, Landroid/content/Context;
aput-object v5, v3, v4
invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
move-result-object v1
const/4 v2, 0x0
const/4 v3, 0x1
new-array v3, v3, [Ljava/lang/Object;
const/4 v4, 0x0
iget-object v5, p0, Lio/fabric/sdk/android/services/b/d;->a:Landroid/content/Context;
aput-object v5, v3, v4
invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
:try_end_21
.catch Ljava/lang/Exception; {:try_start_1 .. :try_end_21} :catch_23
move-result-object v0
:goto_22
return-object v0
:catch_23
move-exception v1
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "Fabric"
const-string v3, "Could not call getAdvertisingIdInfo on com.google.android.gms.ads.identifier.AdvertisingIdClient"
invoke-interface {v1, v2, v3}, Lio/fabric/sdk/android/k;->d(Ljava/lang/String;Ljava/lang/String;)V
goto :goto_22
.end method
.method public a()Lio/fabric/sdk/android/services/b/b;
.registers 4
iget-object v0, p0, Lio/fabric/sdk/android/services/b/d;->a:Landroid/content/Context;
invoke-virtual {p0, v0}, Lio/fabric/sdk/android/services/b/d;->a(Landroid/content/Context;)Z
move-result v0
if-eqz v0, :cond_16
new-instance v0, Lio/fabric/sdk/android/services/b/b;
invoke-direct {p0}, Lio/fabric/sdk/android/services/b/d;->b()Ljava/lang/String;
move-result-object v1
invoke-direct {p0}, Lio/fabric/sdk/android/services/b/d;->c()Z
move-result v2
invoke-direct {v0, v1, v2}, Lio/fabric/sdk/android/services/b/b;-><init>(Ljava/lang/String;Z)V
:goto_15
return-object v0
:cond_16
const/4 v0, 0x0
goto :goto_15
.end method
.method  a(Landroid/content/Context;)Z
.registers 9
const/4 v1, 0x1
const/4 v2, 0x0
:try_start_2
const-string v0, "com.google.android.gms.common.GooglePlayServicesUtil"
invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
const-string v3, "isGooglePlayServicesAvailable"
const/4 v4, 0x1
new-array v4, v4, [Ljava/lang/Class;
const/4 v5, 0x0
const-class v6, Landroid/content/Context;
aput-object v6, v4, v5
invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
move-result-object v0
const/4 v3, 0x0
const/4 v4, 0x1
new-array v4, v4, [Ljava/lang/Object;
const/4 v5, 0x0
aput-object p1, v4, v5
invoke-virtual {v0, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Integer;
invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
:try_end_26
.catch Ljava/lang/Exception; {:try_start_2 .. :try_end_26} :catch_2d
move-result v0
if-nez v0, :cond_2b
move v0, v1
:goto_2a
return v0
:cond_2b
move v0, v2
goto :goto_2a
:catch_2d
move-exception v0
move v0, v2
goto :goto_2a
.end method