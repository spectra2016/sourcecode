.class public Lio/fabric/sdk/android/services/d/d;
.super Ljava/lang/Object;
.source "PreferenceStoreImpl.java"
.implements Lio/fabric/sdk/android/services/d/c;
.field private final a:Landroid/content/SharedPreferences;
.field private final b:Ljava/lang/String;
.field private final c:Landroid/content/Context;
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
.registers 6
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
if-nez p1, :cond_d
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "Cannot get directory before context has been set. Call Fabric.with() first"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_d
iput-object p1, p0, Lio/fabric/sdk/android/services/d/d;->c:Landroid/content/Context;
iput-object p2, p0, Lio/fabric/sdk/android/services/d/d;->b:Ljava/lang/String;
iget-object v0, p0, Lio/fabric/sdk/android/services/d/d;->c:Landroid/content/Context;
iget-object v1, p0, Lio/fabric/sdk/android/services/d/d;->b:Ljava/lang/String;
const/4 v2, 0x0
invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
move-result-object v0
iput-object v0, p0, Lio/fabric/sdk/android/services/d/d;->a:Landroid/content/SharedPreferences;
return-void
.end method
.method public constructor <init>(Lio/fabric/sdk/android/h;)V
.registers 4
invoke-virtual {p1}, Lio/fabric/sdk/android/h;->getContext()Landroid/content/Context;
move-result-object v0
invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v1
invoke-direct {p0, v0, v1}, Lio/fabric/sdk/android/services/d/d;-><init>(Landroid/content/Context;Ljava/lang/String;)V
return-void
.end method
.method public a()Landroid/content/SharedPreferences;
.registers 2
iget-object v0, p0, Lio/fabric/sdk/android/services/d/d;->a:Landroid/content/SharedPreferences;
return-object v0
.end method
.method public a(Landroid/content/SharedPreferences$Editor;)Z
.registers 4
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x9
if-lt v0, v1, :cond_b
invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V
const/4 v0, 0x1
:goto_a
return v0
:cond_b
invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z
move-result v0
goto :goto_a
.end method
.method public b()Landroid/content/SharedPreferences$Editor;
.registers 2
iget-object v0, p0, Lio/fabric/sdk/android/services/d/d;->a:Landroid/content/SharedPreferences;
invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;
move-result-object v0
return-object v0
.end method