.class public Lio/fabric/sdk/android/c;
.super Ljava/lang/Object;
.source "Fabric.java"
.field static volatile a:Lio/fabric/sdk/android/c;
.field static final b:Lio/fabric/sdk/android/k;
.field final c:Lio/fabric/sdk/android/k;
.field final d:Z
.field private final e:Landroid/content/Context;
.field private final f:Ljava/util/Map;
.field private final g:Ljava/util/concurrent/ExecutorService;
.field private final h:Landroid/os/Handler;
.field private final i:Lio/fabric/sdk/android/f;
.field private final j:Lio/fabric/sdk/android/f;
.field private final k:Lio/fabric/sdk/android/services/b/o;
.field private l:Lio/fabric/sdk/android/a;
.field private m:Ljava/lang/ref/WeakReference;
.field private n:Ljava/util/concurrent/atomic/AtomicBoolean;
.method static constructor <clinit>()V
.registers 1
new-instance v0, Lio/fabric/sdk/android/b;
invoke-direct {v0}, Lio/fabric/sdk/android/b;-><init>()V
sput-object v0, Lio/fabric/sdk/android/c;->b:Lio/fabric/sdk/android/k;
return-void
.end method
.method constructor <init>(Landroid/content/Context;Ljava/util/Map;Lio/fabric/sdk/android/services/concurrency/k;Landroid/os/Handler;Lio/fabric/sdk/android/k;ZLio/fabric/sdk/android/f;Lio/fabric/sdk/android/services/b/o;)V
.registers 11
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
move-result-object v0
iput-object v0, p0, Lio/fabric/sdk/android/c;->e:Landroid/content/Context;
iput-object p2, p0, Lio/fabric/sdk/android/c;->f:Ljava/util/Map;
iput-object p3, p0, Lio/fabric/sdk/android/c;->g:Ljava/util/concurrent/ExecutorService;
iput-object p4, p0, Lio/fabric/sdk/android/c;->h:Landroid/os/Handler;
iput-object p5, p0, Lio/fabric/sdk/android/c;->c:Lio/fabric/sdk/android/k;
iput-boolean p6, p0, Lio/fabric/sdk/android/c;->d:Z
iput-object p7, p0, Lio/fabric/sdk/android/c;->i:Lio/fabric/sdk/android/f;
new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;
const/4 v1, 0x0
invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V
iput-object v0, p0, Lio/fabric/sdk/android/c;->n:Ljava/util/concurrent/atomic/AtomicBoolean;
invoke-interface {p2}, Ljava/util/Map;->size()I
move-result v0
invoke-virtual {p0, v0}, Lio/fabric/sdk/android/c;->a(I)Lio/fabric/sdk/android/f;
move-result-object v0
iput-object v0, p0, Lio/fabric/sdk/android/c;->j:Lio/fabric/sdk/android/f;
iput-object p8, p0, Lio/fabric/sdk/android/c;->k:Lio/fabric/sdk/android/services/b/o;
invoke-direct {p0, p1}, Lio/fabric/sdk/android/c;->c(Landroid/content/Context;)Landroid/app/Activity;
move-result-object v0
invoke-virtual {p0, v0}, Lio/fabric/sdk/android/c;->a(Landroid/app/Activity;)Lio/fabric/sdk/android/c;
return-void
.end method
.method static a()Lio/fabric/sdk/android/c;
.registers 2
sget-object v0, Lio/fabric/sdk/android/c;->a:Lio/fabric/sdk/android/c;
if-nez v0, :cond_c
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "Must Initialize Fabric before using singleton()"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_c
sget-object v0, Lio/fabric/sdk/android/c;->a:Lio/fabric/sdk/android/c;
return-object v0
.end method
.method public static varargs a(Landroid/content/Context;[Lio/fabric/sdk/android/h;)Lio/fabric/sdk/android/c;
.registers 4
sget-object v0, Lio/fabric/sdk/android/c;->a:Lio/fabric/sdk/android/c;
if-nez v0, :cond_1c
const-class v1, Lio/fabric/sdk/android/c;
monitor-enter v1
:try_start_7
sget-object v0, Lio/fabric/sdk/android/c;->a:Lio/fabric/sdk/android/c;
if-nez v0, :cond_1b
new-instance v0, Lio/fabric/sdk/android/c$a;
invoke-direct {v0, p0}, Lio/fabric/sdk/android/c$a;-><init>(Landroid/content/Context;)V
invoke-virtual {v0, p1}, Lio/fabric/sdk/android/c$a;->a([Lio/fabric/sdk/android/h;)Lio/fabric/sdk/android/c$a;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/c$a;->a()Lio/fabric/sdk/android/c;
move-result-object v0
invoke-static {v0}, Lio/fabric/sdk/android/c;->c(Lio/fabric/sdk/android/c;)V
:cond_1b
monitor-exit v1
:try_end_1c
.catchall {:try_start_7 .. :try_end_1c} :catchall_1f
:cond_1c
sget-object v0, Lio/fabric/sdk/android/c;->a:Lio/fabric/sdk/android/c;
return-object v0
:catchall_1f
move-exception v0
:try_start_20
monitor-exit v1
:try_end_21
.catchall {:try_start_20 .. :try_end_21} :catchall_1f
throw v0
.end method
.method public static a(Ljava/lang/Class;)Lio/fabric/sdk/android/h;
.registers 2
invoke-static {}, Lio/fabric/sdk/android/c;->a()Lio/fabric/sdk/android/c;
move-result-object v0
iget-object v0, v0, Lio/fabric/sdk/android/c;->f:Ljava/util/Map;
invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/h;
return-object v0
.end method
.method static synthetic a(Ljava/util/Collection;)Ljava/util/Map;
.registers 2
invoke-static {p0}, Lio/fabric/sdk/android/c;->b(Ljava/util/Collection;)Ljava/util/Map;
move-result-object v0
return-object v0
.end method
.method static synthetic a(Lio/fabric/sdk/android/c;)Ljava/util/concurrent/atomic/AtomicBoolean;
.registers 2
iget-object v0, p0, Lio/fabric/sdk/android/c;->n:Ljava/util/concurrent/atomic/AtomicBoolean;
return-object v0
.end method
.method private static a(Ljava/util/Map;Ljava/util/Collection;)V
.registers 5
invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;
move-result-object v1
:cond_4
:goto_4
invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_25
invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/h;
invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v2
invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
instance-of v2, v0, Lio/fabric/sdk/android/i;
if-eqz v2, :cond_4
check-cast v0, Lio/fabric/sdk/android/i;
invoke-interface {v0}, Lio/fabric/sdk/android/i;->getKits()Ljava/util/Collection;
move-result-object v0
invoke-static {p0, v0}, Lio/fabric/sdk/android/c;->a(Ljava/util/Map;Ljava/util/Collection;)V
goto :goto_4
:cond_25
return-void
.end method
.method static synthetic b(Lio/fabric/sdk/android/c;)Lio/fabric/sdk/android/f;
.registers 2
iget-object v0, p0, Lio/fabric/sdk/android/c;->i:Lio/fabric/sdk/android/f;
return-object v0
.end method
.method private static b(Ljava/util/Collection;)Ljava/util/Map;
.registers 3
new-instance v0, Ljava/util/HashMap;
invoke-interface {p0}, Ljava/util/Collection;->size()I
move-result v1
invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V
invoke-static {v0, p0}, Lio/fabric/sdk/android/c;->a(Ljava/util/Map;Ljava/util/Collection;)V
return-object v0
.end method
.method private c(Landroid/content/Context;)Landroid/app/Activity;
.registers 3
instance-of v0, p1, Landroid/app/Activity;
if-eqz v0, :cond_7
check-cast p1, Landroid/app/Activity;
:goto_6
return-object p1
:cond_7
const/4 p1, 0x0
goto :goto_6
.end method
.method private static c(Lio/fabric/sdk/android/c;)V
.registers 1
sput-object p0, Lio/fabric/sdk/android/c;->a:Lio/fabric/sdk/android/c;
invoke-direct {p0}, Lio/fabric/sdk/android/c;->j()V
return-void
.end method
.method public static h()Lio/fabric/sdk/android/k;
.registers 1
sget-object v0, Lio/fabric/sdk/android/c;->a:Lio/fabric/sdk/android/c;
if-nez v0, :cond_7
sget-object v0, Lio/fabric/sdk/android/c;->b:Lio/fabric/sdk/android/k;
:goto_6
return-object v0
:cond_7
sget-object v0, Lio/fabric/sdk/android/c;->a:Lio/fabric/sdk/android/c;
iget-object v0, v0, Lio/fabric/sdk/android/c;->c:Lio/fabric/sdk/android/k;
goto :goto_6
.end method
.method public static i()Z
.registers 1
sget-object v0, Lio/fabric/sdk/android/c;->a:Lio/fabric/sdk/android/c;
if-nez v0, :cond_6
const/4 v0, 0x0
:goto_5
return v0
:cond_6
sget-object v0, Lio/fabric/sdk/android/c;->a:Lio/fabric/sdk/android/c;
iget-boolean v0, v0, Lio/fabric/sdk/android/c;->d:Z
goto :goto_5
.end method
.method private j()V
.registers 3
new-instance v0, Lio/fabric/sdk/android/a;
iget-object v1, p0, Lio/fabric/sdk/android/c;->e:Landroid/content/Context;
invoke-direct {v0, v1}, Lio/fabric/sdk/android/a;-><init>(Landroid/content/Context;)V
iput-object v0, p0, Lio/fabric/sdk/android/c;->l:Lio/fabric/sdk/android/a;
iget-object v0, p0, Lio/fabric/sdk/android/c;->l:Lio/fabric/sdk/android/a;
new-instance v1, Lio/fabric/sdk/android/c$1;
invoke-direct {v1, p0}, Lio/fabric/sdk/android/c$1;-><init>(Lio/fabric/sdk/android/c;)V
invoke-virtual {v0, v1}, Lio/fabric/sdk/android/a;->a(Lio/fabric/sdk/android/a$b;)Z
iget-object v0, p0, Lio/fabric/sdk/android/c;->e:Landroid/content/Context;
invoke-virtual {p0, v0}, Lio/fabric/sdk/android/c;->a(Landroid/content/Context;)V
return-void
.end method
.method public a(Landroid/app/Activity;)Lio/fabric/sdk/android/c;
.registers 3
new-instance v0, Ljava/lang/ref/WeakReference;
invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V
iput-object v0, p0, Lio/fabric/sdk/android/c;->m:Ljava/lang/ref/WeakReference;
return-object p0
.end method
.method  a(I)Lio/fabric/sdk/android/f;
.registers 3
new-instance v0, Lio/fabric/sdk/android/c$2;
invoke-direct {v0, p0, p1}, Lio/fabric/sdk/android/c$2;-><init>(Lio/fabric/sdk/android/c;I)V
return-object v0
.end method
.method  a(Landroid/content/Context;)V
.registers 8
invoke-virtual {p0, p1}, Lio/fabric/sdk/android/c;->b(Landroid/content/Context;)Ljava/util/concurrent/Future;
move-result-object v0
invoke-virtual {p0}, Lio/fabric/sdk/android/c;->g()Ljava/util/Collection;
move-result-object v1
new-instance v2, Lio/fabric/sdk/android/l;
invoke-direct {v2, v0, v1}, Lio/fabric/sdk/android/l;-><init>(Ljava/util/concurrent/Future;Ljava/util/Collection;)V
new-instance v3, Ljava/util/ArrayList;
invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V
sget-object v0, Lio/fabric/sdk/android/f;->d:Lio/fabric/sdk/android/f;
iget-object v1, p0, Lio/fabric/sdk/android/c;->k:Lio/fabric/sdk/android/services/b/o;
invoke-virtual {v2, p1, p0, v0, v1}, Lio/fabric/sdk/android/l;->injectParameters(Landroid/content/Context;Lio/fabric/sdk/android/c;Lio/fabric/sdk/android/f;Lio/fabric/sdk/android/services/b/o;)V
invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v1
:goto_20
invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_34
invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/h;
iget-object v4, p0, Lio/fabric/sdk/android/c;->j:Lio/fabric/sdk/android/f;
iget-object v5, p0, Lio/fabric/sdk/android/c;->k:Lio/fabric/sdk/android/services/b/o;
invoke-virtual {v0, p1, p0, v4, v5}, Lio/fabric/sdk/android/h;->injectParameters(Landroid/content/Context;Lio/fabric/sdk/android/c;Lio/fabric/sdk/android/f;Lio/fabric/sdk/android/services/b/o;)V
goto :goto_20
:cond_34
invoke-virtual {v2}, Lio/fabric/sdk/android/l;->initialize()V
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "Fabric"
const/4 v4, 0x3
invoke-interface {v0, v1, v4}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;I)Z
move-result v0
if-eqz v0, :cond_a5
new-instance v0, Ljava/lang/StringBuilder;
const-string v1, "Initializing "
invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V
invoke-virtual {p0}, Lio/fabric/sdk/android/c;->d()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, " [Version: "
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {p0}, Lio/fabric/sdk/android/c;->c()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, "], with the following kits:\n"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
move-object v1, v0
:goto_68
invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v3
:goto_6c
:cond_6c
invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_a8
invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/h;
iget-object v4, v0, Lio/fabric/sdk/android/h;->initializationTask:Lio/fabric/sdk/android/g;
iget-object v5, v2, Lio/fabric/sdk/android/l;->initializationTask:Lio/fabric/sdk/android/g;
invoke-virtual {v4, v5}, Lio/fabric/sdk/android/g;->a(Lio/fabric/sdk/android/services/concurrency/l;)V
iget-object v4, p0, Lio/fabric/sdk/android/c;->f:Ljava/util/Map;
invoke-virtual {p0, v4, v0}, Lio/fabric/sdk/android/c;->a(Ljava/util/Map;Lio/fabric/sdk/android/h;)V
invoke-virtual {v0}, Lio/fabric/sdk/android/h;->initialize()V
if-eqz v1, :cond_6c
invoke-virtual {v0}, Lio/fabric/sdk/android/h;->getIdentifier()Ljava/lang/String;
move-result-object v4
invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
const-string v5, " [Version: "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v0}, Lio/fabric/sdk/android/h;->getVersion()Ljava/lang/String;
move-result-object v0
invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v4, "]\n"
invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
goto :goto_6c
:cond_a5
const/4 v0, 0x0
move-object v1, v0
goto :goto_68
:cond_a8
if-eqz v1, :cond_b7
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v2, "Fabric"
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-interface {v0, v2, v1}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
:cond_b7
return-void
.end method
.method  a(Ljava/util/Map;Lio/fabric/sdk/android/h;)V
.registers 10
iget-object v0, p2, Lio/fabric/sdk/android/h;->dependsOnAnnotation:Lio/fabric/sdk/android/services/concurrency/d;
if-eqz v0, :cond_5c
invoke-interface {v0}, Lio/fabric/sdk/android/services/concurrency/d;->a()[Ljava/lang/Class;
move-result-object v2
array-length v3, v2
const/4 v0, 0x0
move v1, v0
:goto_b
if-ge v1, v3, :cond_5c
aget-object v4, v2, v1
invoke-virtual {v4}, Ljava/lang/Class;->isInterface()Z
move-result v0
if-eqz v0, :cond_3b
invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;
move-result-object v0
invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;
move-result-object v5
:cond_1d
:goto_1d
invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_58
invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/h;
invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v6
invoke-virtual {v4, v6}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
move-result v6
if-eqz v6, :cond_1d
iget-object v6, p2, Lio/fabric/sdk/android/h;->initializationTask:Lio/fabric/sdk/android/g;
iget-object v0, v0, Lio/fabric/sdk/android/h;->initializationTask:Lio/fabric/sdk/android/g;
invoke-virtual {v6, v0}, Lio/fabric/sdk/android/g;->a(Lio/fabric/sdk/android/services/concurrency/l;)V
goto :goto_1d
:cond_3b
invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/h;
if-nez v0, :cond_4b
new-instance v0, Lio/fabric/sdk/android/services/concurrency/UnmetDependencyException;
const-string v1, "Referenced Kit was null, does the kit exist?"
invoke-direct {v0, v1}, Lio/fabric/sdk/android/services/concurrency/UnmetDependencyException;-><init>(Ljava/lang/String;)V
throw v0
:cond_4b
iget-object v5, p2, Lio/fabric/sdk/android/h;->initializationTask:Lio/fabric/sdk/android/g;
invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/h;
iget-object v0, v0, Lio/fabric/sdk/android/h;->initializationTask:Lio/fabric/sdk/android/g;
invoke-virtual {v5, v0}, Lio/fabric/sdk/android/g;->a(Lio/fabric/sdk/android/services/concurrency/l;)V
:cond_58
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_b
:cond_5c
return-void
.end method
.method public b()Landroid/app/Activity;
.registers 2
iget-object v0, p0, Lio/fabric/sdk/android/c;->m:Ljava/lang/ref/WeakReference;
if-eqz v0, :cond_d
iget-object v0, p0, Lio/fabric/sdk/android/c;->m:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/app/Activity;
:goto_c
return-object v0
:cond_d
const/4 v0, 0x0
goto :goto_c
.end method
.method  b(Landroid/content/Context;)Ljava/util/concurrent/Future;
.registers 4
new-instance v0, Lio/fabric/sdk/android/e;
invoke-virtual {p1}, Landroid/content/Context;->getPackageCodePath()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Lio/fabric/sdk/android/e;-><init>(Ljava/lang/String;)V
invoke-virtual {p0}, Lio/fabric/sdk/android/c;->f()Ljava/util/concurrent/ExecutorService;
move-result-object v1
invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
move-result-object v0
return-object v0
.end method
.method public c()Ljava/lang/String;
.registers 2
const-string v0, "1.3.14.143"
return-object v0
.end method
.method public d()Ljava/lang/String;
.registers 2
const-string v0, "io.fabric.sdk.android:fabric"
return-object v0
.end method
.method public e()Lio/fabric/sdk/android/a;
.registers 2
iget-object v0, p0, Lio/fabric/sdk/android/c;->l:Lio/fabric/sdk/android/a;
return-object v0
.end method
.method public f()Ljava/util/concurrent/ExecutorService;
.registers 2
iget-object v0, p0, Lio/fabric/sdk/android/c;->g:Ljava/util/concurrent/ExecutorService;
return-object v0
.end method
.method public g()Ljava/util/Collection;
.registers 2
iget-object v0, p0, Lio/fabric/sdk/android/c;->f:Ljava/util/Map;
invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;
move-result-object v0
return-object v0
.end method