.class public Lio/fabric/sdk/android/b;
.super Ljava/lang/Object;
.source "DefaultLogger.java"
.implements Lio/fabric/sdk/android/k;
.field private a:I
.method public constructor <init>()V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, 0x4
iput v0, p0, Lio/fabric/sdk/android/b;->a:I
return-void
.end method
.method public constructor <init>(I)V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput p1, p0, Lio/fabric/sdk/android/b;->a:I
return-void
.end method
.method public a(ILjava/lang/String;Ljava/lang/String;)V
.registers 5
const/4 v0, 0x0
invoke-virtual {p0, p1, p2, p3, v0}, Lio/fabric/sdk/android/b;->a(ILjava/lang/String;Ljava/lang/String;Z)V
return-void
.end method
.method public a(ILjava/lang/String;Ljava/lang/String;Z)V
.registers 6
if-nez p4, :cond_8
invoke-virtual {p0, p2, p1}, Lio/fabric/sdk/android/b;->a(Ljava/lang/String;I)Z
move-result v0
if-eqz v0, :cond_b
:cond_8
invoke-static {p1, p2, p3}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I
:cond_b
return-void
.end method
.method public a(Ljava/lang/String;Ljava/lang/String;)V
.registers 4
const/4 v0, 0x0
invoke-virtual {p0, p1, p2, v0}, Lio/fabric/sdk/android/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return-void
.end method
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.registers 5
const/4 v0, 0x3
invoke-virtual {p0, p1, v0}, Lio/fabric/sdk/android/b;->a(Ljava/lang/String;I)Z
move-result v0
if-eqz v0, :cond_a
invoke-static {p1, p2, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
:cond_a
return-void
.end method
.method public a(Ljava/lang/String;I)Z
.registers 4
iget v0, p0, Lio/fabric/sdk/android/b;->a:I
if-gt v0, p2, :cond_6
const/4 v0, 0x1
:goto_5
return v0
:cond_6
const/4 v0, 0x0
goto :goto_5
.end method
.method public b(Ljava/lang/String;Ljava/lang/String;)V
.registers 4
const/4 v0, 0x0
invoke-virtual {p0, p1, p2, v0}, Lio/fabric/sdk/android/b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return-void
.end method
.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.registers 5
const/4 v0, 0x2
invoke-virtual {p0, p1, v0}, Lio/fabric/sdk/android/b;->a(Ljava/lang/String;I)Z
move-result v0
if-eqz v0, :cond_a
invoke-static {p1, p2, p3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
:cond_a
return-void
.end method
.method public c(Ljava/lang/String;Ljava/lang/String;)V
.registers 4
const/4 v0, 0x0
invoke-virtual {p0, p1, p2, v0}, Lio/fabric/sdk/android/b;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return-void
.end method
.method public c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.registers 5
const/4 v0, 0x4
invoke-virtual {p0, p1, v0}, Lio/fabric/sdk/android/b;->a(Ljava/lang/String;I)Z
move-result v0
if-eqz v0, :cond_a
invoke-static {p1, p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
:cond_a
return-void
.end method
.method public d(Ljava/lang/String;Ljava/lang/String;)V
.registers 4
const/4 v0, 0x0
invoke-virtual {p0, p1, p2, v0}, Lio/fabric/sdk/android/b;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return-void
.end method
.method public d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.registers 5
const/4 v0, 0x5
invoke-virtual {p0, p1, v0}, Lio/fabric/sdk/android/b;->a(Ljava/lang/String;I)Z
move-result v0
if-eqz v0, :cond_a
invoke-static {p1, p2, p3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
:cond_a
return-void
.end method
.method public e(Ljava/lang/String;Ljava/lang/String;)V
.registers 4
const/4 v0, 0x0
invoke-virtual {p0, p1, p2, v0}, Lio/fabric/sdk/android/b;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return-void
.end method
.method public e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.registers 5
const/4 v0, 0x6
invoke-virtual {p0, p1, v0}, Lio/fabric/sdk/android/b;->a(Ljava/lang/String;I)Z
move-result v0
if-eqz v0, :cond_a
invoke-static {p1, p2, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
:cond_a
return-void
.end method