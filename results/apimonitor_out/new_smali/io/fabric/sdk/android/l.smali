.class  Lio/fabric/sdk/android/l;
.super Lio/fabric/sdk/android/h;
.source "Onboarding.java"
.field private final a:Lio/fabric/sdk/android/services/network/d;
.field private b:Landroid/content/pm/PackageManager;
.field private c:Ljava/lang/String;
.field private d:Landroid/content/pm/PackageInfo;
.field private e:Ljava/lang/String;
.field private f:Ljava/lang/String;
.field private g:Ljava/lang/String;
.field private h:Ljava/lang/String;
.field private i:Ljava/lang/String;
.field private final j:Ljava/util/concurrent/Future;
.field private final k:Ljava/util/Collection;
.method public constructor <init>(Ljava/util/concurrent/Future;Ljava/util/Collection;)V
.registers 4
invoke-direct {p0}, Lio/fabric/sdk/android/h;-><init>()V
new-instance v0, Lio/fabric/sdk/android/services/network/b;
invoke-direct {v0}, Lio/fabric/sdk/android/services/network/b;-><init>()V
iput-object v0, p0, Lio/fabric/sdk/android/l;->a:Lio/fabric/sdk/android/services/network/d;
iput-object p1, p0, Lio/fabric/sdk/android/l;->j:Ljava/util/concurrent/Future;
iput-object p2, p0, Lio/fabric/sdk/android/l;->k:Ljava/util/Collection;
return-void
.end method
.method private a(Lio/fabric/sdk/android/services/e/n;Ljava/util/Collection;)Lio/fabric/sdk/android/services/e/d;
.registers 15
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->getContext()Landroid/content/Context;
move-result-object v0
new-instance v1, Lio/fabric/sdk/android/services/b/g;
invoke-direct {v1}, Lio/fabric/sdk/android/services/b/g;-><init>()V
invoke-virtual {v1, v0}, Lio/fabric/sdk/android/services/b/g;->a(Landroid/content/Context;)Ljava/lang/String;
move-result-object v1
invoke-static {v0}, Lio/fabric/sdk/android/services/b/i;->m(Landroid/content/Context;)Ljava/lang/String;
move-result-object v0
const/4 v2, 0x1
new-array v2, v2, [Ljava/lang/String;
const/4 v3, 0x0
aput-object v0, v2, v3
invoke-static {v2}, Lio/fabric/sdk/android/services/b/i;->a([Ljava/lang/String;)Ljava/lang/String;
move-result-object v5
iget-object v0, p0, Lio/fabric/sdk/android/l;->g:Ljava/lang/String;
invoke-static {v0}, Lio/fabric/sdk/android/services/b/l;->a(Ljava/lang/String;)Lio/fabric/sdk/android/services/b/l;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/services/b/l;->a()I
move-result v7
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->getIdManager()Lio/fabric/sdk/android/services/b/o;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/services/b/o;->c()Ljava/lang/String;
move-result-object v2
new-instance v0, Lio/fabric/sdk/android/services/e/d;
iget-object v3, p0, Lio/fabric/sdk/android/l;->f:Ljava/lang/String;
iget-object v4, p0, Lio/fabric/sdk/android/l;->e:Ljava/lang/String;
iget-object v6, p0, Lio/fabric/sdk/android/l;->h:Ljava/lang/String;
iget-object v8, p0, Lio/fabric/sdk/android/l;->i:Ljava/lang/String;
const-string v9, "0"
move-object v10, p1
move-object v11, p2
invoke-direct/range {v0 .. v11}, Lio/fabric/sdk/android/services/e/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lio/fabric/sdk/android/services/e/n;Ljava/util/Collection;)V
return-object v0
.end method
.method private a(Lio/fabric/sdk/android/services/e/e;Lio/fabric/sdk/android/services/e/n;Ljava/util/Collection;)Z
.registers 9
invoke-direct {p0, p2, p3}, Lio/fabric/sdk/android/l;->a(Lio/fabric/sdk/android/services/e/n;Ljava/util/Collection;)Lio/fabric/sdk/android/services/e/d;
move-result-object v0
new-instance v1, Lio/fabric/sdk/android/services/e/y;
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->b()Ljava/lang/String;
move-result-object v2
iget-object v3, p1, Lio/fabric/sdk/android/services/e/e;->c:Ljava/lang/String;
iget-object v4, p0, Lio/fabric/sdk/android/l;->a:Lio/fabric/sdk/android/services/network/d;
invoke-direct {v1, p0, v2, v3, v4}, Lio/fabric/sdk/android/services/e/y;-><init>(Lio/fabric/sdk/android/h;Ljava/lang/String;Ljava/lang/String;Lio/fabric/sdk/android/services/network/d;)V
invoke-virtual {v1, v0}, Lio/fabric/sdk/android/services/e/y;->a(Lio/fabric/sdk/android/services/e/d;)Z
move-result v0
return v0
.end method
.method private a(Ljava/lang/String;Lio/fabric/sdk/android/services/e/e;Ljava/util/Collection;)Z
.registers 8
const/4 v0, 0x1
const-string v1, "new"
iget-object v2, p2, Lio/fabric/sdk/android/services/e/e;->b:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_28
invoke-direct {p0, p1, p2, p3}, Lio/fabric/sdk/android/l;->b(Ljava/lang/String;Lio/fabric/sdk/android/services/e/e;Ljava/util/Collection;)Z
move-result v0
if-eqz v0, :cond_1a
invoke-static {}, Lio/fabric/sdk/android/services/e/q;->a()Lio/fabric/sdk/android/services/e/q;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/services/e/q;->d()Z
move-result v0
:goto_19
:cond_19
return v0
:cond_1a
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "Fabric"
const-string v2, "Failed to create app with Crashlytics service."
const/4 v3, 0x0
invoke-interface {v0, v1, v2, v3}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
const/4 v0, 0x0
goto :goto_19
:cond_28
const-string v1, "configured"
iget-object v2, p2, Lio/fabric/sdk/android/services/e/e;->b:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_3b
invoke-static {}, Lio/fabric/sdk/android/services/e/q;->a()Lio/fabric/sdk/android/services/e/q;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/services/e/q;->d()Z
move-result v0
goto :goto_19
:cond_3b
iget-boolean v1, p2, Lio/fabric/sdk/android/services/e/e;->e:Z
if-eqz v1, :cond_19
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "Fabric"
const-string v3, "Server says an update is required - forcing a full App update."
invoke-interface {v1, v2, v3}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-direct {p0, p1, p2, p3}, Lio/fabric/sdk/android/l;->c(Ljava/lang/String;Lio/fabric/sdk/android/services/e/e;Ljava/util/Collection;)Z
goto :goto_19
.end method
.method private b(Ljava/lang/String;Lio/fabric/sdk/android/services/e/e;Ljava/util/Collection;)Z
.registers 9
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->getContext()Landroid/content/Context;
move-result-object v0
invoke-static {v0, p1}, Lio/fabric/sdk/android/services/e/n;->a(Landroid/content/Context;Ljava/lang/String;)Lio/fabric/sdk/android/services/e/n;
move-result-object v0
invoke-direct {p0, v0, p3}, Lio/fabric/sdk/android/l;->a(Lio/fabric/sdk/android/services/e/n;Ljava/util/Collection;)Lio/fabric/sdk/android/services/e/d;
move-result-object v0
new-instance v1, Lio/fabric/sdk/android/services/e/h;
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->b()Ljava/lang/String;
move-result-object v2
iget-object v3, p2, Lio/fabric/sdk/android/services/e/e;->c:Ljava/lang/String;
iget-object v4, p0, Lio/fabric/sdk/android/l;->a:Lio/fabric/sdk/android/services/network/d;
invoke-direct {v1, p0, v2, v3, v4}, Lio/fabric/sdk/android/services/e/h;-><init>(Lio/fabric/sdk/android/h;Ljava/lang/String;Ljava/lang/String;Lio/fabric/sdk/android/services/network/d;)V
invoke-virtual {v1, v0}, Lio/fabric/sdk/android/services/e/h;->a(Lio/fabric/sdk/android/services/e/d;)Z
move-result v0
return v0
.end method
.method private c()Lio/fabric/sdk/android/services/e/t;
.registers 8
:try_start_0
invoke-static {}, Lio/fabric/sdk/android/services/e/q;->a()Lio/fabric/sdk/android/services/e/q;
move-result-object v0
iget-object v2, p0, Lio/fabric/sdk/android/l;->idManager:Lio/fabric/sdk/android/services/b/o;
iget-object v3, p0, Lio/fabric/sdk/android/l;->a:Lio/fabric/sdk/android/services/network/d;
iget-object v4, p0, Lio/fabric/sdk/android/l;->e:Ljava/lang/String;
iget-object v5, p0, Lio/fabric/sdk/android/l;->f:Ljava/lang/String;
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->b()Ljava/lang/String;
move-result-object v6
move-object v1, p0
invoke-virtual/range {v0 .. v6}, Lio/fabric/sdk/android/services/e/q;->a(Lio/fabric/sdk/android/h;Lio/fabric/sdk/android/services/b/o;Lio/fabric/sdk/android/services/network/d;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/e/q;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/services/e/q;->c()Z
invoke-static {}, Lio/fabric/sdk/android/services/e/q;->a()Lio/fabric/sdk/android/services/e/q;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/services/e/q;->b()Lio/fabric/sdk/android/services/e/t;
:try_end_1f
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1f} :catch_21
move-result-object v0
:goto_20
return-object v0
:catch_21
move-exception v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "Fabric"
const-string v3, "Error dealing with settings"
invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
const/4 v0, 0x0
goto :goto_20
.end method
.method private c(Ljava/lang/String;Lio/fabric/sdk/android/services/e/e;Ljava/util/Collection;)Z
.registers 5
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->getContext()Landroid/content/Context;
move-result-object v0
invoke-static {v0, p1}, Lio/fabric/sdk/android/services/e/n;->a(Landroid/content/Context;Ljava/lang/String;)Lio/fabric/sdk/android/services/e/n;
move-result-object v0
invoke-direct {p0, p2, v0, p3}, Lio/fabric/sdk/android/l;->a(Lio/fabric/sdk/android/services/e/e;Lio/fabric/sdk/android/services/e/n;Ljava/util/Collection;)Z
move-result v0
return v0
.end method
.method protected a()Ljava/lang/Boolean;
.registers 6
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->getContext()Landroid/content/Context;
move-result-object v0
invoke-static {v0}, Lio/fabric/sdk/android/services/b/i;->k(Landroid/content/Context;)Ljava/lang/String;
move-result-object v2
const/4 v1, 0x0
invoke-direct {p0}, Lio/fabric/sdk/android/l;->c()Lio/fabric/sdk/android/services/e/t;
move-result-object v3
if-eqz v3, :cond_42
:try_start_f
iget-object v0, p0, Lio/fabric/sdk/android/l;->j:Ljava/util/concurrent/Future;
if-eqz v0, :cond_30
iget-object v0, p0, Lio/fabric/sdk/android/l;->j:Ljava/util/concurrent/Future;
invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/util/Map;
:goto_1b
iget-object v4, p0, Lio/fabric/sdk/android/l;->k:Ljava/util/Collection;
invoke-virtual {p0, v0, v4}, Lio/fabric/sdk/android/l;->a(Ljava/util/Map;Ljava/util/Collection;)Ljava/util/Map;
move-result-object v0
iget-object v3, v3, Lio/fabric/sdk/android/services/e/t;->a:Lio/fabric/sdk/android/services/e/e;
invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;
move-result-object v0
invoke-direct {p0, v2, v3, v0}, Lio/fabric/sdk/android/l;->a(Ljava/lang/String;Lio/fabric/sdk/android/services/e/e;Ljava/util/Collection;)Z
:try_end_2a
.catch Ljava/lang/Exception; {:try_start_f .. :try_end_2a} :catch_36
move-result v0
:goto_2b
invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v0
return-object v0
:cond_30
:try_start_30
new-instance v0, Ljava/util/HashMap;
invoke-direct {v0}, Ljava/util/HashMap;-><init>()V
:try_end_35
.catch Ljava/lang/Exception; {:try_start_30 .. :try_end_35} :catch_36
goto :goto_1b
:catch_36
move-exception v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v2
const-string v3, "Fabric"
const-string v4, "Error performing auto configuration."
invoke-interface {v2, v3, v4, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
:cond_42
move v0, v1
goto :goto_2b
.end method
.method  a(Ljava/util/Map;Ljava/util/Collection;)Ljava/util/Map;
.registers 9
invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;
move-result-object v1
:goto_4
:cond_4
invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_31
invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/h;
invoke-virtual {v0}, Lio/fabric/sdk/android/h;->getIdentifier()Ljava/lang/String;
move-result-object v2
invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
move-result v2
if-nez v2, :cond_4
invoke-virtual {v0}, Lio/fabric/sdk/android/h;->getIdentifier()Ljava/lang/String;
move-result-object v2
new-instance v3, Lio/fabric/sdk/android/j;
invoke-virtual {v0}, Lio/fabric/sdk/android/h;->getIdentifier()Ljava/lang/String;
move-result-object v4
invoke-virtual {v0}, Lio/fabric/sdk/android/h;->getVersion()Ljava/lang/String;
move-result-object v0
const-string v5, "binary"
invoke-direct {v3, v4, v0, v5}, Lio/fabric/sdk/android/j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
invoke-interface {p1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
goto :goto_4
:cond_31
return-object p1
.end method
.method  b()Ljava/lang/String;
.registers 3
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->getContext()Landroid/content/Context;
move-result-object v0
const-string v1, "com.crashlytics.ApiEndpoint"
invoke-static {v0, v1}, Lio/fabric/sdk/android/services/b/i;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method protected synthetic doInBackground()Ljava/lang/Object;
.registers 2
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->a()Ljava/lang/Boolean;
move-result-object v0
return-object v0
.end method
.method public getIdentifier()Ljava/lang/String;
.registers 2
const-string v0, "io.fabric.sdk.android:fabric"
return-object v0
.end method
.method public getVersion()Ljava/lang/String;
.registers 2
const-string v0, "1.3.14.143"
return-object v0
.end method
.method protected onPreExecute()Z
.registers 6
const/4 v0, 0x0
:try_start_1
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->getIdManager()Lio/fabric/sdk/android/services/b/o;
move-result-object v1
invoke-virtual {v1}, Lio/fabric/sdk/android/services/b/o;->j()Ljava/lang/String;
move-result-object v1
iput-object v1, p0, Lio/fabric/sdk/android/l;->g:Ljava/lang/String;
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->getContext()Landroid/content/Context;
move-result-object v1
invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v1
iput-object v1, p0, Lio/fabric/sdk/android/l;->b:Landroid/content/pm/PackageManager;
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->getContext()Landroid/content/Context;
move-result-object v1
invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v1
iput-object v1, p0, Lio/fabric/sdk/android/l;->c:Ljava/lang/String;
iget-object v1, p0, Lio/fabric/sdk/android/l;->b:Landroid/content/pm/PackageManager;
iget-object v2, p0, Lio/fabric/sdk/android/l;->c:Ljava/lang/String;
const/4 v3, 0x0
invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
move-result-object v1
iput-object v1, p0, Lio/fabric/sdk/android/l;->d:Landroid/content/pm/PackageInfo;
iget-object v1, p0, Lio/fabric/sdk/android/l;->d:Landroid/content/pm/PackageInfo;
iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I
invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
move-result-object v1
iput-object v1, p0, Lio/fabric/sdk/android/l;->e:Ljava/lang/String;
iget-object v1, p0, Lio/fabric/sdk/android/l;->d:Landroid/content/pm/PackageInfo;
iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
if-nez v1, :cond_64
const-string v1, "0.0"
:goto_3c
iput-object v1, p0, Lio/fabric/sdk/android/l;->f:Ljava/lang/String;
iget-object v1, p0, Lio/fabric/sdk/android/l;->b:Landroid/content/pm/PackageManager;
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->getContext()Landroid/content/Context;
move-result-object v2
invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;
move-result-object v2
invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
move-result-object v1
invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
move-result-object v1
iput-object v1, p0, Lio/fabric/sdk/android/l;->h:Ljava/lang/String;
invoke-virtual {p0}, Lio/fabric/sdk/android/l;->getContext()Landroid/content/Context;
move-result-object v1
invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;
move-result-object v1
iget v1, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I
invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
move-result-object v1
iput-object v1, p0, Lio/fabric/sdk/android/l;->i:Ljava/lang/String;
const/4 v0, 0x1
:goto_63
return v0
:cond_64
iget-object v1, p0, Lio/fabric/sdk/android/l;->d:Landroid/content/pm/PackageInfo;
iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
:try_end_68
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_68} :catch_69
goto :goto_3c
:catch_69
move-exception v1
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v2
const-string v3, "Fabric"
const-string v4, "Failed init"
invoke-interface {v2, v3, v4, v1}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
goto :goto_63
.end method