.class public Landroid/support/v7/a/r$a;
.super Landroid/support/v7/view/b;
.source "WindowDecorActionBar.java"
.implements Landroid/support/v7/view/menu/f$a;
.field final synthetic a:Landroid/support/v7/a/r;
.field private final b:Landroid/content/Context;
.field private final c:Landroid/support/v7/view/menu/f;
.field private d:Landroid/support/v7/view/b$a;
.field private e:Ljava/lang/ref/WeakReference;
.method public constructor <init>(Landroid/support/v7/a/r;Landroid/content/Context;Landroid/support/v7/view/b$a;)V
.registers 6
iput-object p1, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-direct {p0}, Landroid/support/v7/view/b;-><init>()V
iput-object p2, p0, Landroid/support/v7/a/r$a;->b:Landroid/content/Context;
iput-object p3, p0, Landroid/support/v7/a/r$a;->d:Landroid/support/v7/view/b$a;
new-instance v0, Landroid/support/v7/view/menu/f;
invoke-direct {v0, p2}, Landroid/support/v7/view/menu/f;-><init>(Landroid/content/Context;)V
const/4 v1, 0x1
invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/f;->a(I)Landroid/support/v7/view/menu/f;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/a/r$a;->c:Landroid/support/v7/view/menu/f;
iget-object v0, p0, Landroid/support/v7/a/r$a;->c:Landroid/support/v7/view/menu/f;
invoke-virtual {v0, p0}, Landroid/support/v7/view/menu/f;->a(Landroid/support/v7/view/menu/f$a;)V
return-void
.end method
.method public a()Landroid/view/MenuInflater;
.registers 3
new-instance v0, Landroid/support/v7/view/g;
iget-object v1, p0, Landroid/support/v7/a/r$a;->b:Landroid/content/Context;
invoke-direct {v0, v1}, Landroid/support/v7/view/g;-><init>(Landroid/content/Context;)V
return-object v0
.end method
.method public a(I)V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->i(Landroid/support/v7/a/r;)Landroid/content/Context;
move-result-object v0
invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/support/v7/a/r$a;->b(Ljava/lang/CharSequence;)V
return-void
.end method
.method public a(Landroid/support/v7/view/menu/f;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/r$a;->d:Landroid/support/v7/view/b$a;
if-nez v0, :cond_5
:goto_4
return-void
:cond_5
invoke-virtual {p0}, Landroid/support/v7/a/r$a;->d()V
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->g(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ActionBarContextView;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->a()Z
goto :goto_4
.end method
.method public a(Landroid/view/View;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->g(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ActionBarContextView;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/support/v7/widget/ActionBarContextView;->setCustomView(Landroid/view/View;)V
new-instance v0, Ljava/lang/ref/WeakReference;
invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V
iput-object v0, p0, Landroid/support/v7/a/r$a;->e:Ljava/lang/ref/WeakReference;
return-void
.end method
.method public a(Ljava/lang/CharSequence;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->g(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ActionBarContextView;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/support/v7/widget/ActionBarContextView;->setSubtitle(Ljava/lang/CharSequence;)V
return-void
.end method
.method public a(Z)V
.registers 3
invoke-super {p0, p1}, Landroid/support/v7/view/b;->a(Z)V
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->g(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ActionBarContextView;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/support/v7/widget/ActionBarContextView;->setTitleOptional(Z)V
return-void
.end method
.method public a(Landroid/support/v7/view/menu/f;Landroid/view/MenuItem;)Z
.registers 4
iget-object v0, p0, Landroid/support/v7/a/r$a;->d:Landroid/support/v7/view/b$a;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/a/r$a;->d:Landroid/support/v7/view/b$a;
invoke-interface {v0, p0, p2}, Landroid/support/v7/view/b$a;->a(Landroid/support/v7/view/b;Landroid/view/MenuItem;)Z
move-result v0
:goto_a
return v0
:cond_b
const/4 v0, 0x0
goto :goto_a
.end method
.method public b()Landroid/view/Menu;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r$a;->c:Landroid/support/v7/view/menu/f;
return-object v0
.end method
.method public b(I)V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->i(Landroid/support/v7/a/r;)Landroid/content/Context;
move-result-object v0
invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/support/v7/a/r$a;->a(Ljava/lang/CharSequence;)V
return-void
.end method
.method public b(Ljava/lang/CharSequence;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->g(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ActionBarContextView;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/support/v7/widget/ActionBarContextView;->setTitle(Ljava/lang/CharSequence;)V
return-void
.end method
.method public c()V
.registers 5
const/4 v3, 0x0
const/4 v2, 0x0
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
iget-object v0, v0, Landroid/support/v7/a/r;->a:Landroid/support/v7/a/r$a;
if-eq v0, p0, :cond_9
:goto_8
return-void
:cond_9
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->e(Landroid/support/v7/a/r;)Z
move-result v0
iget-object v1, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v1}, Landroid/support/v7/a/r;->f(Landroid/support/v7/a/r;)Z
move-result v1
invoke-static {v0, v1, v2}, Landroid/support/v7/a/r;->a(ZZZ)Z
move-result v0
if-nez v0, :cond_56
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
iput-object p0, v0, Landroid/support/v7/a/r;->b:Landroid/support/v7/view/b;
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
iget-object v1, p0, Landroid/support/v7/a/r$a;->d:Landroid/support/v7/view/b$a;
iput-object v1, v0, Landroid/support/v7/a/r;->c:Landroid/support/v7/view/b$a;
:goto_25
iput-object v3, p0, Landroid/support/v7/a/r$a;->d:Landroid/support/v7/view/b$a;
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-virtual {v0, v2}, Landroid/support/v7/a/r;->j(Z)V
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->g(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ActionBarContextView;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->b()V
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->h(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ac;
move-result-object v0
invoke-interface {v0}, Landroid/support/v7/widget/ac;->a()Landroid/view/ViewGroup;
move-result-object v0
const/16 v1, 0x20
invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->d(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ActionBarOverlayLayout;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
iget-boolean v1, v1, Landroid/support/v7/a/r;->d:Z
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
iput-object v3, v0, Landroid/support/v7/a/r;->a:Landroid/support/v7/a/r$a;
goto :goto_8
:cond_56
iget-object v0, p0, Landroid/support/v7/a/r$a;->d:Landroid/support/v7/view/b$a;
invoke-interface {v0, p0}, Landroid/support/v7/view/b$a;->a(Landroid/support/v7/view/b;)V
goto :goto_25
.end method
.method public d()V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
iget-object v0, v0, Landroid/support/v7/a/r;->a:Landroid/support/v7/a/r$a;
if-eq v0, p0, :cond_7
:goto_6
return-void
:cond_7
iget-object v0, p0, Landroid/support/v7/a/r$a;->c:Landroid/support/v7/view/menu/f;
invoke-virtual {v0}, Landroid/support/v7/view/menu/f;->g()V
:try_start_c
iget-object v0, p0, Landroid/support/v7/a/r$a;->d:Landroid/support/v7/view/b$a;
iget-object v1, p0, Landroid/support/v7/a/r$a;->c:Landroid/support/v7/view/menu/f;
invoke-interface {v0, p0, v1}, Landroid/support/v7/view/b$a;->b(Landroid/support/v7/view/b;Landroid/view/Menu;)Z
:try_end_13
.catchall {:try_start_c .. :try_end_13} :catchall_19
iget-object v0, p0, Landroid/support/v7/a/r$a;->c:Landroid/support/v7/view/menu/f;
invoke-virtual {v0}, Landroid/support/v7/view/menu/f;->h()V
goto :goto_6
:catchall_19
move-exception v0
iget-object v1, p0, Landroid/support/v7/a/r$a;->c:Landroid/support/v7/view/menu/f;
invoke-virtual {v1}, Landroid/support/v7/view/menu/f;->h()V
throw v0
.end method
.method public e()Z
.registers 3
iget-object v0, p0, Landroid/support/v7/a/r$a;->c:Landroid/support/v7/view/menu/f;
invoke-virtual {v0}, Landroid/support/v7/view/menu/f;->g()V
:try_start_5
iget-object v0, p0, Landroid/support/v7/a/r$a;->d:Landroid/support/v7/view/b$a;
iget-object v1, p0, Landroid/support/v7/a/r$a;->c:Landroid/support/v7/view/menu/f;
invoke-interface {v0, p0, v1}, Landroid/support/v7/view/b$a;->a(Landroid/support/v7/view/b;Landroid/view/Menu;)Z
:try_end_c
.catchall {:try_start_5 .. :try_end_c} :catchall_13
move-result v0
iget-object v1, p0, Landroid/support/v7/a/r$a;->c:Landroid/support/v7/view/menu/f;
invoke-virtual {v1}, Landroid/support/v7/view/menu/f;->h()V
return v0
:catchall_13
move-exception v0
iget-object v1, p0, Landroid/support/v7/a/r$a;->c:Landroid/support/v7/view/menu/f;
invoke-virtual {v1}, Landroid/support/v7/view/menu/f;->h()V
throw v0
.end method
.method public f()Ljava/lang/CharSequence;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->g(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ActionBarContextView;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->getTitle()Ljava/lang/CharSequence;
move-result-object v0
return-object v0
.end method
.method public g()Ljava/lang/CharSequence;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->g(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ActionBarContextView;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->getSubtitle()Ljava/lang/CharSequence;
move-result-object v0
return-object v0
.end method
.method public h()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r$a;->a:Landroid/support/v7/a/r;
invoke-static {v0}, Landroid/support/v7/a/r;->g(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ActionBarContextView;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->d()Z
move-result v0
return v0
.end method
.method public i()Landroid/view/View;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r$a;->e:Ljava/lang/ref/WeakReference;
if-eqz v0, :cond_d
iget-object v0, p0, Landroid/support/v7/a/r$a;->e:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
:goto_c
return-object v0
:cond_d
const/4 v0, 0x0
goto :goto_c
.end method