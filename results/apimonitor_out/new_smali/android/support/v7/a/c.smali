.class  Landroid/support/v7/a/c;
.super Ljava/lang/Object;
.source "AlertController.java"
.field private A:Landroid/widget/TextView;
.field private B:Landroid/widget/TextView;
.field private C:Landroid/view/View;
.field private D:Landroid/widget/ListAdapter;
.field private E:I
.field private F:I
.field private G:I
.field private H:I
.field private I:I
.field private J:I
.field private K:I
.field private L:I
.field private M:Landroid/os/Handler;
.field private final N:Landroid/view/View$OnClickListener;
.field private final a:Landroid/content/Context;
.field private final b:Landroid/support/v7/a/m;
.field private final c:Landroid/view/Window;
.field private d:Ljava/lang/CharSequence;
.field private e:Ljava/lang/CharSequence;
.field private f:Landroid/widget/ListView;
.field private g:Landroid/view/View;
.field private h:I
.field private i:I
.field private j:I
.field private k:I
.field private l:I
.field private m:Z
.field private n:Landroid/widget/Button;
.field private o:Ljava/lang/CharSequence;
.field private p:Landroid/os/Message;
.field private q:Landroid/widget/Button;
.field private r:Ljava/lang/CharSequence;
.field private s:Landroid/os/Message;
.field private t:Landroid/widget/Button;
.field private u:Ljava/lang/CharSequence;
.field private v:Landroid/os/Message;
.field private w:Landroid/support/v4/widget/NestedScrollView;
.field private x:I
.field private y:Landroid/graphics/drawable/Drawable;
.field private z:Landroid/widget/ImageView;
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/a/m;Landroid/view/Window;)V
.registers 8
const/4 v3, 0x0
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-boolean v3, p0, Landroid/support/v7/a/c;->m:Z
iput v3, p0, Landroid/support/v7/a/c;->x:I
const/4 v0, -0x1
iput v0, p0, Landroid/support/v7/a/c;->E:I
iput v3, p0, Landroid/support/v7/a/c;->L:I
new-instance v0, Landroid/support/v7/a/c$1;
invoke-direct {v0, p0}, Landroid/support/v7/a/c$1;-><init>(Landroid/support/v7/a/c;)V
iput-object v0, p0, Landroid/support/v7/a/c;->N:Landroid/view/View$OnClickListener;
iput-object p1, p0, Landroid/support/v7/a/c;->a:Landroid/content/Context;
iput-object p2, p0, Landroid/support/v7/a/c;->b:Landroid/support/v7/a/m;
iput-object p3, p0, Landroid/support/v7/a/c;->c:Landroid/view/Window;
new-instance v0, Landroid/support/v7/a/c$b;
invoke-direct {v0, p2}, Landroid/support/v7/a/c$b;-><init>(Landroid/content/DialogInterface;)V
iput-object v0, p0, Landroid/support/v7/a/c;->M:Landroid/os/Handler;
const/4 v0, 0x0
sget-object v1, Landroid/support/v7/b/a$k;->AlertDialog:[I
sget v2, Landroid/support/v7/b/a$a;->alertDialogStyle:I
invoke-virtual {p1, v0, v1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
move-result-object v0
sget v1, Landroid/support/v7/b/a$k;->AlertDialog_android_layout:I
invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v1
iput v1, p0, Landroid/support/v7/a/c;->F:I
sget v1, Landroid/support/v7/b/a$k;->AlertDialog_buttonPanelSideLayout:I
invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v1
iput v1, p0, Landroid/support/v7/a/c;->G:I
sget v1, Landroid/support/v7/b/a$k;->AlertDialog_listLayout:I
invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v1
iput v1, p0, Landroid/support/v7/a/c;->H:I
sget v1, Landroid/support/v7/b/a$k;->AlertDialog_multiChoiceItemLayout:I
invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v1
iput v1, p0, Landroid/support/v7/a/c;->I:I
sget v1, Landroid/support/v7/b/a$k;->AlertDialog_singleChoiceItemLayout:I
invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v1
iput v1, p0, Landroid/support/v7/a/c;->J:I
sget v1, Landroid/support/v7/b/a$k;->AlertDialog_listItemLayout:I
invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v1
iput v1, p0, Landroid/support/v7/a/c;->K:I
invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
const/4 v0, 0x1
invoke-virtual {p2, v0}, Landroid/support/v7/a/m;->a(I)Z
return-void
.end method
.method static synthetic a(Landroid/support/v7/a/c;I)I
.registers 2
iput p1, p0, Landroid/support/v7/a/c;->E:I
return p1
.end method
.method private a(Landroid/view/View;Landroid/view/View;)Landroid/view/ViewGroup;
.registers 5
if-nez p1, :cond_f
instance-of v0, p2, Landroid/view/ViewStub;
if-eqz v0, :cond_2d
check-cast p2, Landroid/view/ViewStub;
invoke-virtual {p2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;
move-result-object v0
:goto_c
check-cast v0, Landroid/view/ViewGroup;
:goto_e
return-object v0
:cond_f
if-eqz p2, :cond_1e
invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;
move-result-object v0
instance-of v1, v0, Landroid/view/ViewGroup;
if-eqz v1, :cond_1e
check-cast v0, Landroid/view/ViewGroup;
invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
:cond_1e
instance-of v0, p1, Landroid/view/ViewStub;
if-eqz v0, :cond_2b
check-cast p1, Landroid/view/ViewStub;
invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;
move-result-object v0
:goto_28
check-cast v0, Landroid/view/ViewGroup;
goto :goto_e
:cond_2b
move-object v0, p1
goto :goto_28
:cond_2d
move-object v0, p2
goto :goto_c
.end method
.method static synthetic a(Landroid/support/v7/a/c;)Landroid/widget/Button;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/c;->n:Landroid/widget/Button;
return-object v0
.end method
.method static synthetic a(Landroid/support/v7/a/c;Landroid/widget/ListAdapter;)Landroid/widget/ListAdapter;
.registers 2
iput-object p1, p0, Landroid/support/v7/a/c;->D:Landroid/widget/ListAdapter;
return-object p1
.end method
.method static synthetic a(Landroid/support/v7/a/c;Landroid/widget/ListView;)Landroid/widget/ListView;
.registers 2
iput-object p1, p0, Landroid/support/v7/a/c;->f:Landroid/widget/ListView;
return-object p1
.end method
.method static synthetic a(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
.registers 3
invoke-static {p0, p1, p2}, Landroid/support/v7/a/c;->b(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
return-void
.end method
.method private a(Landroid/view/ViewGroup;)V
.registers 7
const/high16 v4, 0x2
const/4 v0, 0x0
const/4 v3, -0x1
iget-object v1, p0, Landroid/support/v7/a/c;->g:Landroid/view/View;
if-eqz v1, :cond_4b
iget-object v1, p0, Landroid/support/v7/a/c;->g:Landroid/view/View;
:goto_a
if-eqz v1, :cond_d
const/4 v0, 0x1
:cond_d
if-eqz v0, :cond_15
invoke-static {v1}, Landroid/support/v7/a/c;->a(Landroid/view/View;)Z
move-result v2
if-nez v2, :cond_1a
:cond_15
iget-object v2, p0, Landroid/support/v7/a/c;->c:Landroid/view/Window;
invoke-virtual {v2, v4, v4}, Landroid/view/Window;->setFlags(II)V
:cond_1a
if-eqz v0, :cond_5e
iget-object v0, p0, Landroid/support/v7/a/c;->c:Landroid/view/Window;
sget v2, Landroid/support/v7/b/a$f;->custom:I
invoke-virtual {v0, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/FrameLayout;
new-instance v2, Landroid/view/ViewGroup$LayoutParams;
invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V
invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
iget-boolean v1, p0, Landroid/support/v7/a/c;->m:Z
if-eqz v1, :cond_3d
iget v1, p0, Landroid/support/v7/a/c;->i:I
iget v2, p0, Landroid/support/v7/a/c;->j:I
iget v3, p0, Landroid/support/v7/a/c;->k:I
iget v4, p0, Landroid/support/v7/a/c;->l:I
invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/FrameLayout;->setPadding(IIII)V
:cond_3d
iget-object v0, p0, Landroid/support/v7/a/c;->f:Landroid/widget/ListView;
if-eqz v0, :cond_4a
invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/widget/LinearLayout$LayoutParams;
const/4 v1, 0x0
iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F
:goto_4a
:cond_4a
return-void
:cond_4b
iget v1, p0, Landroid/support/v7/a/c;->h:I
if-eqz v1, :cond_5c
iget-object v1, p0, Landroid/support/v7/a/c;->a:Landroid/content/Context;
invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v1
iget v2, p0, Landroid/support/v7/a/c;->h:I
invoke-virtual {v1, v2, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
move-result-object v1
goto :goto_a
:cond_5c
const/4 v1, 0x0
goto :goto_a
:cond_5e
const/16 v0, 0x8
invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V
goto :goto_4a
.end method
.method private a(Landroid/view/ViewGroup;Landroid/view/View;II)V
.registers 10
const/4 v0, 0x0
iget-object v1, p0, Landroid/support/v7/a/c;->c:Landroid/view/Window;
sget v2, Landroid/support/v7/b/a$f;->scrollIndicatorUp:I
invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;
move-result-object v2
iget-object v1, p0, Landroid/support/v7/a/c;->c:Landroid/view/Window;
sget v3, Landroid/support/v7/b/a$f;->scrollIndicatorDown:I
invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;
move-result-object v1
sget v3, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v4, 0x17
if-lt v3, v4, :cond_25
invoke-static {p2, p3, p4}, Landroid/support/v4/f/af;->a(Landroid/view/View;II)V
if-eqz v2, :cond_1f
invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
:cond_1f
if-eqz v1, :cond_24
invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
:goto_24
:cond_24
return-void
:cond_25
if-eqz v2, :cond_2f
and-int/lit8 v3, p3, 0x1
if-nez v3, :cond_2f
invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
move-object v2, v0
:cond_2f
if-eqz v1, :cond_79
and-int/lit8 v3, p3, 0x2
if-nez v3, :cond_79
invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
:goto_38
if-nez v2, :cond_3c
if-eqz v0, :cond_24
:cond_3c
iget-object v1, p0, Landroid/support/v7/a/c;->e:Ljava/lang/CharSequence;
if-eqz v1, :cond_55
iget-object v1, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
new-instance v3, Landroid/support/v7/a/c$2;
invoke-direct {v3, p0, v2, v0}, Landroid/support/v7/a/c$2;-><init>(Landroid/support/v7/a/c;Landroid/view/View;Landroid/view/View;)V
invoke-virtual {v1, v3}, Landroid/support/v4/widget/NestedScrollView;->setOnScrollChangeListener(Landroid/support/v4/widget/NestedScrollView$b;)V
iget-object v1, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
new-instance v3, Landroid/support/v7/a/c$3;
invoke-direct {v3, p0, v2, v0}, Landroid/support/v7/a/c$3;-><init>(Landroid/support/v7/a/c;Landroid/view/View;Landroid/view/View;)V
invoke-virtual {v1, v3}, Landroid/support/v4/widget/NestedScrollView;->post(Ljava/lang/Runnable;)Z
goto :goto_24
:cond_55
iget-object v1, p0, Landroid/support/v7/a/c;->f:Landroid/widget/ListView;
if-eqz v1, :cond_6e
iget-object v1, p0, Landroid/support/v7/a/c;->f:Landroid/widget/ListView;
new-instance v3, Landroid/support/v7/a/c$4;
invoke-direct {v3, p0, v2, v0}, Landroid/support/v7/a/c$4;-><init>(Landroid/support/v7/a/c;Landroid/view/View;Landroid/view/View;)V
invoke-virtual {v1, v3}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
iget-object v1, p0, Landroid/support/v7/a/c;->f:Landroid/widget/ListView;
new-instance v3, Landroid/support/v7/a/c$5;
invoke-direct {v3, p0, v2, v0}, Landroid/support/v7/a/c$5;-><init>(Landroid/support/v7/a/c;Landroid/view/View;Landroid/view/View;)V
invoke-virtual {v1, v3}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z
goto :goto_24
:cond_6e
if-eqz v2, :cond_73
invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
:cond_73
if-eqz v0, :cond_24
invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
goto :goto_24
:cond_79
move-object v0, v1
goto :goto_38
.end method
.method static a(Landroid/view/View;)Z
.registers 5
const/4 v0, 0x1
const/4 v1, 0x0
invoke-virtual {p0}, Landroid/view/View;->onCheckIsTextEditor()Z
move-result v2
if-eqz v2, :cond_9
:goto_8
return v0
:cond_9
instance-of v2, p0, Landroid/view/ViewGroup;
if-nez v2, :cond_f
move v0, v1
goto :goto_8
:cond_f
check-cast p0, Landroid/view/ViewGroup;
invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I
move-result v2
:cond_15
if-lez v2, :cond_24
add-int/lit8 v2, v2, -0x1
invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;
move-result-object v3
invoke-static {v3}, Landroid/support/v7/a/c;->a(Landroid/view/View;)Z
move-result v3
if-eqz v3, :cond_15
goto :goto_8
:cond_24
move v0, v1
goto :goto_8
.end method
.method private b()I
.registers 3
iget v0, p0, Landroid/support/v7/a/c;->G:I
if-nez v0, :cond_7
iget v0, p0, Landroid/support/v7/a/c;->F:I
:goto_6
return v0
:cond_7
iget v0, p0, Landroid/support/v7/a/c;->L:I
const/4 v1, 0x1
if-ne v0, v1, :cond_f
iget v0, p0, Landroid/support/v7/a/c;->G:I
goto :goto_6
:cond_f
iget v0, p0, Landroid/support/v7/a/c;->F:I
goto :goto_6
.end method
.method static synthetic b(Landroid/support/v7/a/c;)Landroid/os/Message;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/c;->p:Landroid/os/Message;
return-object v0
.end method
.method private static b(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
.registers 6
const/4 v2, 0x4
const/4 v1, 0x0
if-eqz p1, :cond_f
const/4 v0, -0x1
invoke-static {p0, v0}, Landroid/support/v4/f/af;->a(Landroid/view/View;I)Z
move-result v0
if-eqz v0, :cond_1c
move v0, v1
:goto_c
invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V
:cond_f
if-eqz p2, :cond_1b
const/4 v0, 0x1
invoke-static {p0, v0}, Landroid/support/v4/f/af;->a(Landroid/view/View;I)Z
move-result v0
if-eqz v0, :cond_1e
:goto_18
invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V
:cond_1b
return-void
:cond_1c
move v0, v2
goto :goto_c
:cond_1e
move v1, v2
goto :goto_18
.end method
.method private b(Landroid/view/ViewGroup;)V
.registers 8
const/4 v1, 0x0
const/16 v5, 0x8
iget-object v0, p0, Landroid/support/v7/a/c;->C:Landroid/view/View;
if-eqz v0, :cond_1f
new-instance v0, Landroid/view/ViewGroup$LayoutParams;
const/4 v2, -0x1
const/4 v3, -0x2
invoke-direct {v0, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V
iget-object v2, p0, Landroid/support/v7/a/c;->C:Landroid/view/View;
invoke-virtual {p1, v2, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
iget-object v0, p0, Landroid/support/v7/a/c;->c:Landroid/view/Window;
sget v1, Landroid/support/v7/b/a$f;->title_template:I
invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;
move-result-object v0
invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V
:goto_1e
return-void
:cond_1f
iget-object v0, p0, Landroid/support/v7/a/c;->c:Landroid/view/Window;
const v2, 0x1020006
invoke-virtual {v0, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/ImageView;
iput-object v0, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
iget-object v0, p0, Landroid/support/v7/a/c;->d:Ljava/lang/CharSequence;
invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-nez v0, :cond_56
const/4 v0, 0x1
:goto_35
if-eqz v0, :cond_87
iget-object v0, p0, Landroid/support/v7/a/c;->c:Landroid/view/Window;
sget v1, Landroid/support/v7/b/a$f;->alertTitle:I
invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/TextView;
iput-object v0, p0, Landroid/support/v7/a/c;->A:Landroid/widget/TextView;
iget-object v0, p0, Landroid/support/v7/a/c;->A:Landroid/widget/TextView;
iget-object v1, p0, Landroid/support/v7/a/c;->d:Ljava/lang/CharSequence;
invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
iget v0, p0, Landroid/support/v7/a/c;->x:I
if-eqz v0, :cond_58
iget-object v0, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
iget v1, p0, Landroid/support/v7/a/c;->x:I
invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V
goto :goto_1e
:cond_56
move v0, v1
goto :goto_35
:cond_58
iget-object v0, p0, Landroid/support/v7/a/c;->y:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_64
iget-object v0, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
iget-object v1, p0, Landroid/support/v7/a/c;->y:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
goto :goto_1e
:cond_64
iget-object v0, p0, Landroid/support/v7/a/c;->A:Landroid/widget/TextView;
iget-object v1, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
invoke-virtual {v1}, Landroid/widget/ImageView;->getPaddingLeft()I
move-result v1
iget-object v2, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
invoke-virtual {v2}, Landroid/widget/ImageView;->getPaddingTop()I
move-result v2
iget-object v3, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
invoke-virtual {v3}, Landroid/widget/ImageView;->getPaddingRight()I
move-result v3
iget-object v4, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
invoke-virtual {v4}, Landroid/widget/ImageView;->getPaddingBottom()I
move-result v4
invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V
iget-object v0, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V
goto :goto_1e
:cond_87
iget-object v0, p0, Landroid/support/v7/a/c;->c:Landroid/view/Window;
sget v1, Landroid/support/v7/b/a$f;->title_template:I
invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;
move-result-object v0
invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V
iget-object v0, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V
invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V
goto :goto_1e
.end method
.method static synthetic c(Landroid/support/v7/a/c;)Landroid/widget/Button;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/c;->q:Landroid/widget/Button;
return-object v0
.end method
.method private c()V
.registers 11
const/16 v9, 0x8
const/4 v3, 0x1
const/4 v1, 0x0
iget-object v0, p0, Landroid/support/v7/a/c;->c:Landroid/view/Window;
sget v2, Landroid/support/v7/b/a$f;->parentPanel:I
invoke-virtual {v0, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;
move-result-object v0
sget v2, Landroid/support/v7/b/a$f;->topPanel:I
invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v2
sget v4, Landroid/support/v7/b/a$f;->contentPanel:I
invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v4
sget v5, Landroid/support/v7/b/a$f;->buttonPanel:I
invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v5
sget v6, Landroid/support/v7/b/a$f;->customPanel:I
invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup;
invoke-direct {p0, v0}, Landroid/support/v7/a/c;->a(Landroid/view/ViewGroup;)V
sget v6, Landroid/support/v7/b/a$f;->topPanel:I
invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v6
sget v7, Landroid/support/v7/b/a$f;->contentPanel:I
invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v7
sget v8, Landroid/support/v7/b/a$f;->buttonPanel:I
invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v8
invoke-direct {p0, v6, v2}, Landroid/support/v7/a/c;->a(Landroid/view/View;Landroid/view/View;)Landroid/view/ViewGroup;
move-result-object v2
invoke-direct {p0, v7, v4}, Landroid/support/v7/a/c;->a(Landroid/view/View;Landroid/view/View;)Landroid/view/ViewGroup;
move-result-object v6
invoke-direct {p0, v8, v5}, Landroid/support/v7/a/c;->a(Landroid/view/View;Landroid/view/View;)Landroid/view/ViewGroup;
move-result-object v4
invoke-direct {p0, v6}, Landroid/support/v7/a/c;->c(Landroid/view/ViewGroup;)V
invoke-direct {p0, v4}, Landroid/support/v7/a/c;->d(Landroid/view/ViewGroup;)V
invoke-direct {p0, v2}, Landroid/support/v7/a/c;->b(Landroid/view/ViewGroup;)V
if-eqz v0, :cond_b4
invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I
move-result v0
if-eq v0, v9, :cond_b4
move v0, v3
:goto_59
if-eqz v2, :cond_b6
invoke-virtual {v2}, Landroid/view/ViewGroup;->getVisibility()I
move-result v2
if-eq v2, v9, :cond_b6
move v2, v3
:goto_62
if-eqz v4, :cond_b8
invoke-virtual {v4}, Landroid/view/ViewGroup;->getVisibility()I
move-result v4
if-eq v4, v9, :cond_b8
move v5, v3
:goto_6b
if-nez v5, :cond_7a
if-eqz v6, :cond_7a
sget v4, Landroid/support/v7/b/a$f;->textSpacerNoButtons:I
invoke-virtual {v6, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v4
if-eqz v4, :cond_7a
invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V
:cond_7a
if-eqz v2, :cond_85
iget-object v4, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
if-eqz v4, :cond_85
iget-object v4, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
invoke-virtual {v4, v3}, Landroid/support/v4/widget/NestedScrollView;->setClipToPadding(Z)V
:cond_85
if-nez v0, :cond_9b
iget-object v0, p0, Landroid/support/v7/a/c;->f:Landroid/widget/ListView;
if-eqz v0, :cond_ba
iget-object v0, p0, Landroid/support/v7/a/c;->f:Landroid/widget/ListView;
move-object v4, v0
:goto_8e
if-eqz v4, :cond_9b
if-eqz v2, :cond_be
move v2, v3
:goto_93
if-eqz v5, :cond_c0
const/4 v0, 0x2
:goto_96
or-int/2addr v0, v2
const/4 v1, 0x3
invoke-direct {p0, v6, v4, v0, v1}, Landroid/support/v7/a/c;->a(Landroid/view/ViewGroup;Landroid/view/View;II)V
:cond_9b
iget-object v0, p0, Landroid/support/v7/a/c;->f:Landroid/widget/ListView;
if-eqz v0, :cond_b3
iget-object v1, p0, Landroid/support/v7/a/c;->D:Landroid/widget/ListAdapter;
if-eqz v1, :cond_b3
iget-object v1, p0, Landroid/support/v7/a/c;->D:Landroid/widget/ListAdapter;
invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V
iget v1, p0, Landroid/support/v7/a/c;->E:I
const/4 v2, -0x1
if-le v1, v2, :cond_b3
invoke-virtual {v0, v1, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V
invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V
:cond_b3
return-void
:cond_b4
move v0, v1
goto :goto_59
:cond_b6
move v2, v1
goto :goto_62
:cond_b8
move v5, v1
goto :goto_6b
:cond_ba
iget-object v0, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
move-object v4, v0
goto :goto_8e
:cond_be
move v2, v1
goto :goto_93
:cond_c0
move v0, v1
goto :goto_96
.end method
.method private c(Landroid/view/ViewGroup;)V
.registers 7
const/16 v3, 0x8
const/4 v2, 0x0
const/4 v4, -0x1
iget-object v0, p0, Landroid/support/v7/a/c;->c:Landroid/view/Window;
sget v1, Landroid/support/v7/b/a$f;->scrollView:I
invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/support/v4/widget/NestedScrollView;
iput-object v0, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
iget-object v0, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
invoke-virtual {v0, v2}, Landroid/support/v4/widget/NestedScrollView;->setFocusable(Z)V
iget-object v0, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
invoke-virtual {v0, v2}, Landroid/support/v4/widget/NestedScrollView;->setNestedScrollingEnabled(Z)V
const v0, 0x102000b
invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/TextView;
iput-object v0, p0, Landroid/support/v7/a/c;->B:Landroid/widget/TextView;
iget-object v0, p0, Landroid/support/v7/a/c;->B:Landroid/widget/TextView;
if-nez v0, :cond_2a
:goto_29
return-void
:cond_2a
iget-object v0, p0, Landroid/support/v7/a/c;->e:Ljava/lang/CharSequence;
if-eqz v0, :cond_36
iget-object v0, p0, Landroid/support/v7/a/c;->B:Landroid/widget/TextView;
iget-object v1, p0, Landroid/support/v7/a/c;->e:Ljava/lang/CharSequence;
invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
goto :goto_29
:cond_36
iget-object v0, p0, Landroid/support/v7/a/c;->B:Landroid/widget/TextView;
invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V
iget-object v0, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
iget-object v1, p0, Landroid/support/v7/a/c;->B:Landroid/widget/TextView;
invoke-virtual {v0, v1}, Landroid/support/v4/widget/NestedScrollView;->removeView(Landroid/view/View;)V
iget-object v0, p0, Landroid/support/v7/a/c;->f:Landroid/widget/ListView;
if-eqz v0, :cond_62
iget-object v0, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
invoke-virtual {v0}, Landroid/support/v4/widget/NestedScrollView;->getParent()Landroid/view/ViewParent;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup;
iget-object v1, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I
move-result v1
invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewAt(I)V
iget-object v2, p0, Landroid/support/v7/a/c;->f:Landroid/widget/ListView;
new-instance v3, Landroid/view/ViewGroup$LayoutParams;
invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V
invoke-virtual {v0, v2, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
goto :goto_29
:cond_62
invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V
goto :goto_29
.end method
.method static synthetic d(Landroid/support/v7/a/c;)Landroid/os/Message;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/c;->s:Landroid/os/Message;
return-object v0
.end method
.method private d(Landroid/view/ViewGroup;)V
.registers 10
const/4 v3, 0x1
const/16 v7, 0x8
const/4 v2, 0x0
const/4 v4, 0x2
const/4 v5, 0x4
const v0, 0x1020019
invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/Button;
iput-object v0, p0, Landroid/support/v7/a/c;->n:Landroid/widget/Button;
iget-object v0, p0, Landroid/support/v7/a/c;->n:Landroid/widget/Button;
iget-object v1, p0, Landroid/support/v7/a/c;->N:Landroid/view/View$OnClickListener;
invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
iget-object v0, p0, Landroid/support/v7/a/c;->o:Ljava/lang/CharSequence;
invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-eqz v0, :cond_6c
iget-object v0, p0, Landroid/support/v7/a/c;->n:Landroid/widget/Button;
invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V
move v1, v2
:goto_26
const v0, 0x102001a
invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/Button;
iput-object v0, p0, Landroid/support/v7/a/c;->q:Landroid/widget/Button;
iget-object v0, p0, Landroid/support/v7/a/c;->q:Landroid/widget/Button;
iget-object v6, p0, Landroid/support/v7/a/c;->N:Landroid/view/View$OnClickListener;
invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
iget-object v0, p0, Landroid/support/v7/a/c;->r:Ljava/lang/CharSequence;
invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-eqz v0, :cond_7a
iget-object v0, p0, Landroid/support/v7/a/c;->q:Landroid/widget/Button;
invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V
:goto_45
const v0, 0x102001b
invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/Button;
iput-object v0, p0, Landroid/support/v7/a/c;->t:Landroid/widget/Button;
iget-object v0, p0, Landroid/support/v7/a/c;->t:Landroid/widget/Button;
iget-object v4, p0, Landroid/support/v7/a/c;->N:Landroid/view/View$OnClickListener;
invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
iget-object v0, p0, Landroid/support/v7/a/c;->u:Ljava/lang/CharSequence;
invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-eqz v0, :cond_88
iget-object v0, p0, Landroid/support/v7/a/c;->t:Landroid/widget/Button;
invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V
:goto_64
if-eqz v1, :cond_96
:goto_66
if-nez v3, :cond_6b
invoke-virtual {p1, v7}, Landroid/view/ViewGroup;->setVisibility(I)V
:cond_6b
return-void
:cond_6c
iget-object v0, p0, Landroid/support/v7/a/c;->n:Landroid/widget/Button;
iget-object v1, p0, Landroid/support/v7/a/c;->o:Ljava/lang/CharSequence;
invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
iget-object v0, p0, Landroid/support/v7/a/c;->n:Landroid/widget/Button;
invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V
move v1, v3
goto :goto_26
:cond_7a
iget-object v0, p0, Landroid/support/v7/a/c;->q:Landroid/widget/Button;
iget-object v6, p0, Landroid/support/v7/a/c;->r:Ljava/lang/CharSequence;
invoke-virtual {v0, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
iget-object v0, p0, Landroid/support/v7/a/c;->q:Landroid/widget/Button;
invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V
or-int/2addr v1, v4
goto :goto_45
:cond_88
iget-object v0, p0, Landroid/support/v7/a/c;->t:Landroid/widget/Button;
iget-object v4, p0, Landroid/support/v7/a/c;->u:Ljava/lang/CharSequence;
invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
iget-object v0, p0, Landroid/support/v7/a/c;->t:Landroid/widget/Button;
invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V
or-int/2addr v1, v5
goto :goto_64
:cond_96
move v3, v2
goto :goto_66
.end method
.method static synthetic e(Landroid/support/v7/a/c;)Landroid/widget/Button;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/c;->t:Landroid/widget/Button;
return-object v0
.end method
.method static synthetic f(Landroid/support/v7/a/c;)Landroid/os/Message;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/c;->v:Landroid/os/Message;
return-object v0
.end method
.method static synthetic g(Landroid/support/v7/a/c;)Landroid/support/v7/a/m;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/c;->b:Landroid/support/v7/a/m;
return-object v0
.end method
.method static synthetic h(Landroid/support/v7/a/c;)Landroid/os/Handler;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/c;->M:Landroid/os/Handler;
return-object v0
.end method
.method static synthetic i(Landroid/support/v7/a/c;)Landroid/support/v4/widget/NestedScrollView;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
return-object v0
.end method
.method static synthetic j(Landroid/support/v7/a/c;)Landroid/widget/ListView;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/c;->f:Landroid/widget/ListView;
return-object v0
.end method
.method static synthetic k(Landroid/support/v7/a/c;)I
.registers 2
iget v0, p0, Landroid/support/v7/a/c;->H:I
return v0
.end method
.method static synthetic l(Landroid/support/v7/a/c;)I
.registers 2
iget v0, p0, Landroid/support/v7/a/c;->I:I
return v0
.end method
.method static synthetic m(Landroid/support/v7/a/c;)I
.registers 2
iget v0, p0, Landroid/support/v7/a/c;->J:I
return v0
.end method
.method static synthetic n(Landroid/support/v7/a/c;)I
.registers 2
iget v0, p0, Landroid/support/v7/a/c;->K:I
return v0
.end method
.method public a()V
.registers 3
invoke-direct {p0}, Landroid/support/v7/a/c;->b()I
move-result v0
iget-object v1, p0, Landroid/support/v7/a/c;->b:Landroid/support/v7/a/m;
invoke-virtual {v1, v0}, Landroid/support/v7/a/m;->setContentView(I)V
invoke-direct {p0}, Landroid/support/v7/a/c;->c()V
return-void
.end method
.method public a(I)V
.registers 3
const/4 v0, 0x0
iput-object v0, p0, Landroid/support/v7/a/c;->g:Landroid/view/View;
iput p1, p0, Landroid/support/v7/a/c;->h:I
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v7/a/c;->m:Z
return-void
.end method
.method public a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V
.registers 7
if-nez p4, :cond_a
if-eqz p3, :cond_a
iget-object v0, p0, Landroid/support/v7/a/c;->M:Landroid/os/Handler;
invoke-virtual {v0, p1, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
move-result-object p4
:cond_a
packed-switch p1, :pswitch_data_24
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "Button does not exist"
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:pswitch_15
iput-object p2, p0, Landroid/support/v7/a/c;->o:Ljava/lang/CharSequence;
iput-object p4, p0, Landroid/support/v7/a/c;->p:Landroid/os/Message;
:goto_19
return-void
:pswitch_1a
iput-object p2, p0, Landroid/support/v7/a/c;->r:Ljava/lang/CharSequence;
iput-object p4, p0, Landroid/support/v7/a/c;->s:Landroid/os/Message;
goto :goto_19
:pswitch_1f
iput-object p2, p0, Landroid/support/v7/a/c;->u:Ljava/lang/CharSequence;
iput-object p4, p0, Landroid/support/v7/a/c;->v:Landroid/os/Message;
goto :goto_19
:pswitch_data_24
.packed-switch -0x3
:pswitch_1f
:pswitch_1a
:pswitch_15
.end packed-switch
.end method
.method public a(Landroid/graphics/drawable/Drawable;)V
.registers 4
const/4 v1, 0x0
iput-object p1, p0, Landroid/support/v7/a/c;->y:Landroid/graphics/drawable/Drawable;
iput v1, p0, Landroid/support/v7/a/c;->x:I
iget-object v0, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
if-eqz v0, :cond_15
if-eqz p1, :cond_16
iget-object v0, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
iget-object v0, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
:goto_15
:cond_15
return-void
:cond_16
iget-object v0, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
const/16 v1, 0x8
invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
goto :goto_15
.end method
.method public a(Landroid/view/View;IIII)V
.registers 7
iput-object p1, p0, Landroid/support/v7/a/c;->g:Landroid/view/View;
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/a/c;->h:I
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v7/a/c;->m:Z
iput p2, p0, Landroid/support/v7/a/c;->i:I
iput p3, p0, Landroid/support/v7/a/c;->j:I
iput p4, p0, Landroid/support/v7/a/c;->k:I
iput p5, p0, Landroid/support/v7/a/c;->l:I
return-void
.end method
.method public a(Ljava/lang/CharSequence;)V
.registers 3
iput-object p1, p0, Landroid/support/v7/a/c;->d:Ljava/lang/CharSequence;
iget-object v0, p0, Landroid/support/v7/a/c;->A:Landroid/widget/TextView;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/a/c;->A:Landroid/widget/TextView;
invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
:cond_b
return-void
.end method
.method public a(ILandroid/view/KeyEvent;)Z
.registers 4
iget-object v0, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
if-eqz v0, :cond_e
iget-object v0, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
invoke-virtual {v0, p2}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/view/KeyEvent;)Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method public b(I)V
.registers 4
const/4 v0, 0x0
iput-object v0, p0, Landroid/support/v7/a/c;->y:Landroid/graphics/drawable/Drawable;
iput p1, p0, Landroid/support/v7/a/c;->x:I
iget-object v0, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
if-eqz v0, :cond_18
if-eqz p1, :cond_19
iget-object v0, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
const/4 v1, 0x0
invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
iget-object v0, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
iget v1, p0, Landroid/support/v7/a/c;->x:I
invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V
:goto_18
:cond_18
return-void
:cond_19
iget-object v0, p0, Landroid/support/v7/a/c;->z:Landroid/widget/ImageView;
const/16 v1, 0x8
invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
goto :goto_18
.end method
.method public b(Landroid/view/View;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/a/c;->C:Landroid/view/View;
return-void
.end method
.method public b(Ljava/lang/CharSequence;)V
.registers 3
iput-object p1, p0, Landroid/support/v7/a/c;->e:Ljava/lang/CharSequence;
iget-object v0, p0, Landroid/support/v7/a/c;->B:Landroid/widget/TextView;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/a/c;->B:Landroid/widget/TextView;
invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
:cond_b
return-void
.end method
.method public b(ILandroid/view/KeyEvent;)Z
.registers 4
iget-object v0, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
if-eqz v0, :cond_e
iget-object v0, p0, Landroid/support/v7/a/c;->w:Landroid/support/v4/widget/NestedScrollView;
invoke-virtual {v0, p2}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/view/KeyEvent;)Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method public c(I)I
.registers 5
new-instance v0, Landroid/util/TypedValue;
invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V
iget-object v1, p0, Landroid/support/v7/a/c;->a:Landroid/content/Context;
invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v1
const/4 v2, 0x1
invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z
iget v0, v0, Landroid/util/TypedValue;->resourceId:I
return v0
.end method
.method public c(Landroid/view/View;)V
.registers 3
const/4 v0, 0x0
iput-object p1, p0, Landroid/support/v7/a/c;->g:Landroid/view/View;
iput v0, p0, Landroid/support/v7/a/c;->h:I
iput-boolean v0, p0, Landroid/support/v7/a/c;->m:Z
return-void
.end method