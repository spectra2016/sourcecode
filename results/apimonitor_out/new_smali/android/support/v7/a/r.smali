.class public Landroid/support/v7/a/r;
.super Landroid/support/v7/a/a;
.source "WindowDecorActionBar.java"
.implements Landroid/support/v7/widget/ActionBarOverlayLayout$a;
.field static final synthetic h:Z
.field private static final i:Landroid/view/animation/Interpolator;
.field private static final j:Landroid/view/animation/Interpolator;
.field private static final k:Z
.field private A:Z
.field private B:I
.field private C:Z
.field private D:Z
.field private E:Z
.field private F:Z
.field private G:Z
.field private H:Landroid/support/v7/view/h;
.field private I:Z
.field  a:Landroid/support/v7/a/r$a;
.field  b:Landroid/support/v7/view/b;
.field  c:Landroid/support/v7/view/b$a;
.field  d:Z
.field final e:Landroid/support/v4/f/ay;
.field final f:Landroid/support/v4/f/ay;
.field final g:Landroid/support/v4/f/ba;
.field private l:Landroid/content/Context;
.field private m:Landroid/content/Context;
.field private n:Landroid/app/Activity;
.field private o:Landroid/app/Dialog;
.field private p:Landroid/support/v7/widget/ActionBarOverlayLayout;
.field private q:Landroid/support/v7/widget/ActionBarContainer;
.field private r:Landroid/support/v7/widget/ac;
.field private s:Landroid/support/v7/widget/ActionBarContextView;
.field private t:Landroid/view/View;
.field private u:Landroid/support/v7/widget/ak;
.field private v:Ljava/util/ArrayList;
.field private w:I
.field private x:Z
.field private y:Z
.field private z:Ljava/util/ArrayList;
.method static constructor <clinit>()V
.registers 4
const/4 v1, 0x1
const/4 v2, 0x0
const-class v0, Landroid/support/v7/a/r;
invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z
move-result v0
if-nez v0, :cond_24
move v0, v1
:goto_b
sput-boolean v0, Landroid/support/v7/a/r;->h:Z
new-instance v0, Landroid/view/animation/AccelerateInterpolator;
invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V
sput-object v0, Landroid/support/v7/a/r;->i:Landroid/view/animation/Interpolator;
new-instance v0, Landroid/view/animation/DecelerateInterpolator;
invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V
sput-object v0, Landroid/support/v7/a/r;->j:Landroid/view/animation/Interpolator;
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v3, 0xe
if-lt v0, v3, :cond_26
:goto_21
sput-boolean v1, Landroid/support/v7/a/r;->k:Z
return-void
:cond_24
move v0, v2
goto :goto_b
:cond_26
move v1, v2
goto :goto_21
.end method
.method public constructor <init>(Landroid/app/Activity;Z)V
.registers 5
const/4 v1, 0x1
invoke-direct {p0}, Landroid/support/v7/a/a;-><init>()V
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Landroid/support/v7/a/r;->v:Ljava/util/ArrayList;
const/4 v0, -0x1
iput v0, p0, Landroid/support/v7/a/r;->w:I
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Landroid/support/v7/a/r;->z:Ljava/util/ArrayList;
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/a/r;->B:I
iput-boolean v1, p0, Landroid/support/v7/a/r;->C:Z
iput-boolean v1, p0, Landroid/support/v7/a/r;->G:Z
new-instance v0, Landroid/support/v7/a/r$1;
invoke-direct {v0, p0}, Landroid/support/v7/a/r$1;-><init>(Landroid/support/v7/a/r;)V
iput-object v0, p0, Landroid/support/v7/a/r;->e:Landroid/support/v4/f/ay;
new-instance v0, Landroid/support/v7/a/r$2;
invoke-direct {v0, p0}, Landroid/support/v7/a/r$2;-><init>(Landroid/support/v7/a/r;)V
iput-object v0, p0, Landroid/support/v7/a/r;->f:Landroid/support/v4/f/ay;
new-instance v0, Landroid/support/v7/a/r$3;
invoke-direct {v0, p0}, Landroid/support/v7/a/r$3;-><init>(Landroid/support/v7/a/r;)V
iput-object v0, p0, Landroid/support/v7/a/r;->g:Landroid/support/v4/f/ba;
iput-object p1, p0, Landroid/support/v7/a/r;->n:Landroid/app/Activity;
invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;
move-result-object v0
invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
move-result-object v0
invoke-direct {p0, v0}, Landroid/support/v7/a/r;->a(Landroid/view/View;)V
if-nez p2, :cond_49
const v1, 0x1020002
invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/a/r;->t:Landroid/view/View;
:cond_49
return-void
.end method
.method public constructor <init>(Landroid/app/Dialog;)V
.registers 4
const/4 v1, 0x1
invoke-direct {p0}, Landroid/support/v7/a/a;-><init>()V
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Landroid/support/v7/a/r;->v:Ljava/util/ArrayList;
const/4 v0, -0x1
iput v0, p0, Landroid/support/v7/a/r;->w:I
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Landroid/support/v7/a/r;->z:Ljava/util/ArrayList;
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/a/r;->B:I
iput-boolean v1, p0, Landroid/support/v7/a/r;->C:Z
iput-boolean v1, p0, Landroid/support/v7/a/r;->G:Z
new-instance v0, Landroid/support/v7/a/r$1;
invoke-direct {v0, p0}, Landroid/support/v7/a/r$1;-><init>(Landroid/support/v7/a/r;)V
iput-object v0, p0, Landroid/support/v7/a/r;->e:Landroid/support/v4/f/ay;
new-instance v0, Landroid/support/v7/a/r$2;
invoke-direct {v0, p0}, Landroid/support/v7/a/r$2;-><init>(Landroid/support/v7/a/r;)V
iput-object v0, p0, Landroid/support/v7/a/r;->f:Landroid/support/v4/f/ay;
new-instance v0, Landroid/support/v7/a/r$3;
invoke-direct {v0, p0}, Landroid/support/v7/a/r$3;-><init>(Landroid/support/v7/a/r;)V
iput-object v0, p0, Landroid/support/v7/a/r;->g:Landroid/support/v4/f/ba;
iput-object p1, p0, Landroid/support/v7/a/r;->o:Landroid/app/Dialog;
invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;
move-result-object v0
invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
move-result-object v0
invoke-direct {p0, v0}, Landroid/support/v7/a/r;->a(Landroid/view/View;)V
return-void
.end method
.method static synthetic a(Landroid/support/v7/a/r;Landroid/support/v7/view/h;)Landroid/support/v7/view/h;
.registers 2
iput-object p1, p0, Landroid/support/v7/a/r;->H:Landroid/support/v7/view/h;
return-object p1
.end method
.method private a(Landroid/view/View;)V
.registers 8
const/4 v2, 0x1
const/4 v1, 0x0
sget v0, Landroid/support/v7/b/a$f;->decor_content_parent:I
invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionBarOverlayLayout;
iput-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
iget-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
if-eqz v0, :cond_15
iget-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
invoke-virtual {v0, p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->setActionBarVisibilityCallback(Landroid/support/v7/widget/ActionBarOverlayLayout$a;)V
:cond_15
sget v0, Landroid/support/v7/b/a$f;->action_bar:I
invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v0
invoke-direct {p0, v0}, Landroid/support/v7/a/r;->b(Landroid/view/View;)Landroid/support/v7/widget/ac;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
sget v0, Landroid/support/v7/b/a$f;->action_context_bar:I
invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionBarContextView;
iput-object v0, p0, Landroid/support/v7/a/r;->s:Landroid/support/v7/widget/ActionBarContextView;
sget v0, Landroid/support/v7/b/a$f;->action_bar_container:I
invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionBarContainer;
iput-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
if-eqz v0, :cond_41
iget-object v0, p0, Landroid/support/v7/a/r;->s:Landroid/support/v7/widget/ActionBarContextView;
if-eqz v0, :cond_41
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
if-nez v0, :cond_68
:cond_41
new-instance v0, Ljava/lang/IllegalStateException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, " can only be used "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "with a compatible window decor layout"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_68
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->b()Landroid/content/Context;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/a/r;->l:Landroid/content/Context;
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->o()I
move-result v0
and-int/lit8 v0, v0, 0x4
if-eqz v0, :cond_be
move v0, v2
:goto_7b
if-eqz v0, :cond_7f
iput-boolean v2, p0, Landroid/support/v7/a/r;->x:Z
:cond_7f
iget-object v3, p0, Landroid/support/v7/a/r;->l:Landroid/content/Context;
invoke-static {v3}, Landroid/support/v7/view/a;->a(Landroid/content/Context;)Landroid/support/v7/view/a;
move-result-object v3
invoke-virtual {v3}, Landroid/support/v7/view/a;->f()Z
move-result v4
if-nez v4, :cond_8d
if-eqz v0, :cond_c0
:cond_8d
move v0, v2
:goto_8e
invoke-virtual {p0, v0}, Landroid/support/v7/a/r;->a(Z)V
invoke-virtual {v3}, Landroid/support/v7/view/a;->d()Z
move-result v0
invoke-direct {p0, v0}, Landroid/support/v7/a/r;->k(Z)V
iget-object v0, p0, Landroid/support/v7/a/r;->l:Landroid/content/Context;
const/4 v3, 0x0
sget-object v4, Landroid/support/v7/b/a$k;->ActionBar:[I
sget v5, Landroid/support/v7/b/a$a;->actionBarStyle:I
invoke-virtual {v0, v3, v4, v5, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
move-result-object v0
sget v3, Landroid/support/v7/b/a$k;->ActionBar_hideOnContentScroll:I
invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z
move-result v3
if-eqz v3, :cond_ae
invoke-virtual {p0, v2}, Landroid/support/v7/a/r;->b(Z)V
:cond_ae
sget v2, Landroid/support/v7/b/a$k;->ActionBar_elevation:I
invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I
move-result v1
if-eqz v1, :cond_ba
int-to-float v1, v1
invoke-virtual {p0, v1}, Landroid/support/v7/a/r;->a(F)V
:cond_ba
invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
return-void
:cond_be
move v0, v1
goto :goto_7b
:cond_c0
move v0, v1
goto :goto_8e
.end method
.method static synthetic a(Landroid/support/v7/a/r;)Z
.registers 2
iget-boolean v0, p0, Landroid/support/v7/a/r;->C:Z
return v0
.end method
.method static synthetic a(ZZZ)Z
.registers 4
invoke-static {p0, p1, p2}, Landroid/support/v7/a/r;->b(ZZZ)Z
move-result v0
return v0
.end method
.method private b(Landroid/view/View;)Landroid/support/v7/widget/ac;
.registers 5
instance-of v0, p1, Landroid/support/v7/widget/ac;
if-eqz v0, :cond_7
check-cast p1, Landroid/support/v7/widget/ac;
:goto_6
return-object p1
:cond_7
instance-of v0, p1, Landroid/support/v7/widget/Toolbar;
if-eqz v0, :cond_12
check-cast p1, Landroid/support/v7/widget/Toolbar;
invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getWrapper()Landroid/support/v7/widget/ac;
move-result-object p1
goto :goto_6
:cond_12
new-instance v1, Ljava/lang/IllegalStateException;
new-instance v0, Ljava/lang/StringBuilder;
invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Can\'t make a decor toolbar out of "
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_35
invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v0
:goto_31
invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v1
:cond_35
const-string v0, "null"
goto :goto_31
.end method
.method static synthetic b(Landroid/support/v7/a/r;)Landroid/view/View;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r;->t:Landroid/view/View;
return-object v0
.end method
.method private static b(ZZZ)Z
.registers 4
const/4 v0, 0x1
if-eqz p2, :cond_4
:cond_3
:goto_3
return v0
:cond_4
if-nez p0, :cond_8
if-eqz p1, :cond_3
:cond_8
const/4 v0, 0x0
goto :goto_3
.end method
.method static synthetic c(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ActionBarContainer;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
return-object v0
.end method
.method static synthetic d(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ActionBarOverlayLayout;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
return-object v0
.end method
.method static synthetic e(Landroid/support/v7/a/r;)Z
.registers 2
iget-boolean v0, p0, Landroid/support/v7/a/r;->D:Z
return v0
.end method
.method static synthetic f(Landroid/support/v7/a/r;)Z
.registers 2
iget-boolean v0, p0, Landroid/support/v7/a/r;->E:Z
return v0
.end method
.method static synthetic g(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ActionBarContextView;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r;->s:Landroid/support/v7/widget/ActionBarContextView;
return-object v0
.end method
.method static synthetic h(Landroid/support/v7/a/r;)Landroid/support/v7/widget/ac;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
return-object v0
.end method
.method static synthetic i(Landroid/support/v7/a/r;)Landroid/content/Context;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r;->l:Landroid/content/Context;
return-object v0
.end method
.method private k(Z)V
.registers 7
const/4 v3, 0x0
const/4 v1, 0x1
const/4 v2, 0x0
iput-boolean p1, p0, Landroid/support/v7/a/r;->A:Z
iget-boolean v0, p0, Landroid/support/v7/a/r;->A:Z
if-nez v0, :cond_49
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
invoke-interface {v0, v3}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/ak;)V
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
iget-object v3, p0, Landroid/support/v7/a/r;->u:Landroid/support/v7/widget/ak;
invoke-virtual {v0, v3}, Landroid/support/v7/widget/ActionBarContainer;->setTabContainer(Landroid/support/v7/widget/ak;)V
:goto_15
invoke-virtual {p0}, Landroid/support/v7/a/r;->j()I
move-result v0
const/4 v3, 0x2
if-ne v0, v3, :cond_56
move v0, v1
:goto_1d
iget-object v3, p0, Landroid/support/v7/a/r;->u:Landroid/support/v7/widget/ak;
if-eqz v3, :cond_31
if-eqz v0, :cond_58
iget-object v3, p0, Landroid/support/v7/a/r;->u:Landroid/support/v7/widget/ak;
invoke-virtual {v3, v2}, Landroid/support/v7/widget/ak;->setVisibility(I)V
iget-object v3, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
if-eqz v3, :cond_31
iget-object v3, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
invoke-static {v3}, Landroid/support/v4/f/af;->n(Landroid/view/View;)V
:goto_31
:cond_31
iget-object v4, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
iget-boolean v3, p0, Landroid/support/v7/a/r;->A:Z
if-nez v3, :cond_60
if-eqz v0, :cond_60
move v3, v1
:goto_3a
invoke-interface {v4, v3}, Landroid/support/v7/widget/ac;->a(Z)V
iget-object v3, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
iget-boolean v4, p0, Landroid/support/v7/a/r;->A:Z
if-nez v4, :cond_62
if-eqz v0, :cond_62
:goto_45
invoke-virtual {v3, v1}, Landroid/support/v7/widget/ActionBarOverlayLayout;->setHasNonEmbeddedTabs(Z)V
return-void
:cond_49
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v0, v3}, Landroid/support/v7/widget/ActionBarContainer;->setTabContainer(Landroid/support/v7/widget/ak;)V
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
iget-object v3, p0, Landroid/support/v7/a/r;->u:Landroid/support/v7/widget/ak;
invoke-interface {v0, v3}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/ak;)V
goto :goto_15
:cond_56
move v0, v2
goto :goto_1d
:cond_58
iget-object v3, p0, Landroid/support/v7/a/r;->u:Landroid/support/v7/widget/ak;
const/16 v4, 0x8
invoke-virtual {v3, v4}, Landroid/support/v7/widget/ak;->setVisibility(I)V
goto :goto_31
:cond_60
move v3, v2
goto :goto_3a
:cond_62
move v1, v2
goto :goto_45
.end method
.method private l(Z)V
.registers 5
iget-boolean v0, p0, Landroid/support/v7/a/r;->D:Z
iget-boolean v1, p0, Landroid/support/v7/a/r;->E:Z
iget-boolean v2, p0, Landroid/support/v7/a/r;->F:Z
invoke-static {v0, v1, v2}, Landroid/support/v7/a/r;->b(ZZZ)Z
move-result v0
if-eqz v0, :cond_17
iget-boolean v0, p0, Landroid/support/v7/a/r;->G:Z
if-nez v0, :cond_16
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v7/a/r;->G:Z
invoke-virtual {p0, p1}, Landroid/support/v7/a/r;->h(Z)V
:cond_16
:goto_16
return-void
:cond_17
iget-boolean v0, p0, Landroid/support/v7/a/r;->G:Z
if-eqz v0, :cond_16
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v7/a/r;->G:Z
invoke-virtual {p0, p1}, Landroid/support/v7/a/r;->i(Z)V
goto :goto_16
.end method
.method private p()V
.registers 3
const/4 v1, 0x1
iget-boolean v0, p0, Landroid/support/v7/a/r;->F:Z
if-nez v0, :cond_14
iput-boolean v1, p0, Landroid/support/v7/a/r;->F:Z
iget-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
if-eqz v0, :cond_10
iget-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionBarOverlayLayout;->setShowingForActionMode(Z)V
:cond_10
const/4 v0, 0x0
invoke-direct {p0, v0}, Landroid/support/v7/a/r;->l(Z)V
:cond_14
return-void
.end method
.method private q()V
.registers 3
const/4 v1, 0x0
iget-boolean v0, p0, Landroid/support/v7/a/r;->F:Z
if-eqz v0, :cond_13
iput-boolean v1, p0, Landroid/support/v7/a/r;->F:Z
iget-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
if-eqz v0, :cond_10
iget-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionBarOverlayLayout;->setShowingForActionMode(Z)V
:cond_10
invoke-direct {p0, v1}, Landroid/support/v7/a/r;->l(Z)V
:cond_13
return-void
.end method
.method public a()I
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->o()I
move-result v0
return v0
.end method
.method public a(Landroid/support/v7/view/b$a;)Landroid/support/v7/view/b;
.registers 5
iget-object v0, p0, Landroid/support/v7/a/r;->a:Landroid/support/v7/a/r$a;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/a/r;->a:Landroid/support/v7/a/r$a;
invoke-virtual {v0}, Landroid/support/v7/a/r$a;->c()V
:cond_9
iget-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
const/4 v1, 0x0
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V
iget-object v0, p0, Landroid/support/v7/a/r;->s:Landroid/support/v7/widget/ActionBarContextView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->c()V
new-instance v0, Landroid/support/v7/a/r$a;
iget-object v1, p0, Landroid/support/v7/a/r;->s:Landroid/support/v7/widget/ActionBarContextView;
invoke-virtual {v1}, Landroid/support/v7/widget/ActionBarContextView;->getContext()Landroid/content/Context;
move-result-object v1
invoke-direct {v0, p0, v1, p1}, Landroid/support/v7/a/r$a;-><init>(Landroid/support/v7/a/r;Landroid/content/Context;Landroid/support/v7/view/b$a;)V
invoke-virtual {v0}, Landroid/support/v7/a/r$a;->e()Z
move-result v1
if-eqz v1, :cond_3b
invoke-virtual {v0}, Landroid/support/v7/a/r$a;->d()V
iget-object v1, p0, Landroid/support/v7/a/r;->s:Landroid/support/v7/widget/ActionBarContextView;
invoke-virtual {v1, v0}, Landroid/support/v7/widget/ActionBarContextView;->a(Landroid/support/v7/view/b;)V
const/4 v1, 0x1
invoke-virtual {p0, v1}, Landroid/support/v7/a/r;->j(Z)V
iget-object v1, p0, Landroid/support/v7/a/r;->s:Landroid/support/v7/widget/ActionBarContextView;
const/16 v2, 0x20
invoke-virtual {v1, v2}, Landroid/support/v7/widget/ActionBarContextView;->sendAccessibilityEvent(I)V
iput-object v0, p0, Landroid/support/v7/a/r;->a:Landroid/support/v7/a/r$a;
:goto_3a
return-object v0
:cond_3b
const/4 v0, 0x0
goto :goto_3a
.end method
.method public a(F)V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
invoke-static {v0, p1}, Landroid/support/v4/f/af;->d(Landroid/view/View;F)V
return-void
.end method
.method public a(I)V
.registers 2
iput p1, p0, Landroid/support/v7/a/r;->B:I
return-void
.end method
.method public a(II)V
.registers 7
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->o()I
move-result v0
and-int/lit8 v1, p2, 0x4
if-eqz v1, :cond_d
const/4 v1, 0x1
iput-boolean v1, p0, Landroid/support/v7/a/r;->x:Z
:cond_d
iget-object v1, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
and-int v2, p1, p2
xor-int/lit8 v3, p2, -0x1
and-int/2addr v0, v3
or-int/2addr v0, v2
invoke-interface {v1, v0}, Landroid/support/v7/widget/ac;->c(I)V
return-void
.end method
.method public a(Landroid/content/res/Configuration;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/r;->l:Landroid/content/Context;
invoke-static {v0}, Landroid/support/v7/view/a;->a(Landroid/content/Context;)Landroid/support/v7/view/a;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/view/a;->d()Z
move-result v0
invoke-direct {p0, v0}, Landroid/support/v7/a/r;->k(Z)V
return-void
.end method
.method public a(Ljava/lang/CharSequence;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
invoke-interface {v0, p1}, Landroid/support/v7/widget/ac;->a(Ljava/lang/CharSequence;)V
return-void
.end method
.method public a(Z)V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
invoke-interface {v0, p1}, Landroid/support/v7/widget/ac;->b(Z)V
return-void
.end method
.method public b(Z)V
.registers 4
if-eqz p1, :cond_12
iget-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->a()Z
move-result v0
if-nez v0, :cond_12
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_12
iput-boolean p1, p0, Landroid/support/v7/a/r;->d:Z
iget-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
invoke-virtual {v0, p1}, Landroid/support/v7/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V
return-void
.end method
.method public b()Z
.registers 3
invoke-virtual {p0}, Landroid/support/v7/a/r;->k()I
move-result v0
iget-boolean v1, p0, Landroid/support/v7/a/r;->G:Z
if-eqz v1, :cond_12
if-eqz v0, :cond_10
invoke-virtual {p0}, Landroid/support/v7/a/r;->d()I
move-result v1
if-ge v1, v0, :cond_12
:cond_10
const/4 v0, 0x1
:goto_11
return v0
:cond_12
const/4 v0, 0x0
goto :goto_11
.end method
.method public c()Landroid/content/Context;
.registers 5
iget-object v0, p0, Landroid/support/v7/a/r;->m:Landroid/content/Context;
if-nez v0, :cond_22
new-instance v0, Landroid/util/TypedValue;
invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V
iget-object v1, p0, Landroid/support/v7/a/r;->l:Landroid/content/Context;
invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v1
sget v2, Landroid/support/v7/b/a$a;->actionBarWidgetTheme:I
const/4 v3, 0x1
invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z
iget v0, v0, Landroid/util/TypedValue;->resourceId:I
if-eqz v0, :cond_25
new-instance v1, Landroid/view/ContextThemeWrapper;
iget-object v2, p0, Landroid/support/v7/a/r;->l:Landroid/content/Context;
invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V
iput-object v1, p0, Landroid/support/v7/a/r;->m:Landroid/content/Context;
:cond_22
:goto_22
iget-object v0, p0, Landroid/support/v7/a/r;->m:Landroid/content/Context;
return-object v0
:cond_25
iget-object v0, p0, Landroid/support/v7/a/r;->l:Landroid/content/Context;
iput-object v0, p0, Landroid/support/v7/a/r;->m:Landroid/content/Context;
goto :goto_22
.end method
.method public c(Z)V
.registers 3
iget-boolean v0, p0, Landroid/support/v7/a/r;->x:Z
if-nez v0, :cond_7
invoke-virtual {p0, p1}, Landroid/support/v7/a/r;->f(Z)V
:cond_7
return-void
.end method
.method public d()I
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getActionBarHideOffset()I
move-result v0
return v0
.end method
.method public d(Z)V
.registers 3
iput-boolean p1, p0, Landroid/support/v7/a/r;->I:Z
if-nez p1, :cond_d
iget-object v0, p0, Landroid/support/v7/a/r;->H:Landroid/support/v7/view/h;
if-eqz v0, :cond_d
iget-object v0, p0, Landroid/support/v7/a/r;->H:Landroid/support/v7/view/h;
invoke-virtual {v0}, Landroid/support/v7/view/h;->b()V
:cond_d
return-void
.end method
.method public e(Z)V
.registers 5
iget-boolean v0, p0, Landroid/support/v7/a/r;->y:Z
if-ne p1, v0, :cond_5
:cond_4
return-void
:cond_5
iput-boolean p1, p0, Landroid/support/v7/a/r;->y:Z
iget-object v0, p0, Landroid/support/v7/a/r;->z:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v2
const/4 v0, 0x0
move v1, v0
:goto_f
if-ge v1, v2, :cond_4
iget-object v0, p0, Landroid/support/v7/a/r;->z:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v7/a/a$b;
invoke-interface {v0, p1}, Landroid/support/v7/a/a$b;->a(Z)V
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_f
.end method
.method public f(Z)V
.registers 4
const/4 v1, 0x4
if-eqz p1, :cond_8
move v0, v1
:goto_4
invoke-virtual {p0, v0, v1}, Landroid/support/v7/a/r;->a(II)V
return-void
:cond_8
const/4 v0, 0x0
goto :goto_4
.end method
.method public f()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
if-eqz v0, :cond_13
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->c()Z
move-result v0
if-eqz v0, :cond_13
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->d()V
const/4 v0, 0x1
:goto_12
return v0
:cond_13
const/4 v0, 0x0
goto :goto_12
.end method
.method public g(Z)V
.registers 2
iput-boolean p1, p0, Landroid/support/v7/a/r;->C:Z
return-void
.end method
.method public g()Z
.registers 3
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->a()Landroid/view/ViewGroup;
move-result-object v0
if-eqz v0, :cond_13
invoke-virtual {v0}, Landroid/view/ViewGroup;->hasFocus()Z
move-result v1
if-nez v1, :cond_13
invoke-virtual {v0}, Landroid/view/ViewGroup;->requestFocus()Z
const/4 v0, 0x1
:goto_12
return v0
:cond_13
const/4 v0, 0x0
goto :goto_12
.end method
.method public h(Z)V
.registers 7
const/4 v4, 0x0
iget-object v0, p0, Landroid/support/v7/a/r;->H:Landroid/support/v7/view/h;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/v7/a/r;->H:Landroid/support/v7/view/h;
invoke-virtual {v0}, Landroid/support/v7/view/h;->b()V
:cond_a
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
const/4 v1, 0x0
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionBarContainer;->setVisibility(I)V
iget v0, p0, Landroid/support/v7/a/r;->B:I
if-nez v0, :cond_91
sget-boolean v0, Landroid/support/v7/a/r;->k:Z
if-eqz v0, :cond_91
iget-boolean v0, p0, Landroid/support/v7/a/r;->I:Z
if-nez v0, :cond_1e
if-eqz p1, :cond_91
:cond_1e
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
invoke-static {v0, v4}, Landroid/support/v4/f/af;->a(Landroid/view/View;F)V
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContainer;->getHeight()I
move-result v0
neg-int v0, v0
int-to-float v0, v0
if-eqz p1, :cond_3d
const/4 v1, 0x2
new-array v1, v1, [I
fill-array-data v1, :array_b2
iget-object v2, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v2, v1}, Landroid/support/v7/widget/ActionBarContainer;->getLocationInWindow([I)V
const/4 v2, 0x1
aget v1, v1, v2
int-to-float v1, v1
sub-float/2addr v0, v1
:cond_3d
iget-object v1, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
invoke-static {v1, v0}, Landroid/support/v4/f/af;->a(Landroid/view/View;F)V
new-instance v1, Landroid/support/v7/view/h;
invoke-direct {v1}, Landroid/support/v7/view/h;-><init>()V
iget-object v2, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
invoke-static {v2}, Landroid/support/v4/f/af;->k(Landroid/view/View;)Landroid/support/v4/f/au;
move-result-object v2
invoke-virtual {v2, v4}, Landroid/support/v4/f/au;->b(F)Landroid/support/v4/f/au;
move-result-object v2
iget-object v3, p0, Landroid/support/v7/a/r;->g:Landroid/support/v4/f/ba;
invoke-virtual {v2, v3}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/ba;)Landroid/support/v4/f/au;
invoke-virtual {v1, v2}, Landroid/support/v7/view/h;->a(Landroid/support/v4/f/au;)Landroid/support/v7/view/h;
iget-boolean v2, p0, Landroid/support/v7/a/r;->C:Z
if-eqz v2, :cond_73
iget-object v2, p0, Landroid/support/v7/a/r;->t:Landroid/view/View;
if-eqz v2, :cond_73
iget-object v2, p0, Landroid/support/v7/a/r;->t:Landroid/view/View;
invoke-static {v2, v0}, Landroid/support/v4/f/af;->a(Landroid/view/View;F)V
iget-object v0, p0, Landroid/support/v7/a/r;->t:Landroid/view/View;
invoke-static {v0}, Landroid/support/v4/f/af;->k(Landroid/view/View;)Landroid/support/v4/f/au;
move-result-object v0
invoke-virtual {v0, v4}, Landroid/support/v4/f/au;->b(F)Landroid/support/v4/f/au;
move-result-object v0
invoke-virtual {v1, v0}, Landroid/support/v7/view/h;->a(Landroid/support/v4/f/au;)Landroid/support/v7/view/h;
:cond_73
sget-object v0, Landroid/support/v7/a/r;->j:Landroid/view/animation/Interpolator;
invoke-virtual {v1, v0}, Landroid/support/v7/view/h;->a(Landroid/view/animation/Interpolator;)Landroid/support/v7/view/h;
const-wide/16 v2, 0xfa
invoke-virtual {v1, v2, v3}, Landroid/support/v7/view/h;->a(J)Landroid/support/v7/view/h;
iget-object v0, p0, Landroid/support/v7/a/r;->f:Landroid/support/v4/f/ay;
invoke-virtual {v1, v0}, Landroid/support/v7/view/h;->a(Landroid/support/v4/f/ay;)Landroid/support/v7/view/h;
iput-object v1, p0, Landroid/support/v7/a/r;->H:Landroid/support/v7/view/h;
invoke-virtual {v1}, Landroid/support/v7/view/h;->a()V
:goto_87
iget-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
if-eqz v0, :cond_90
iget-object v0, p0, Landroid/support/v7/a/r;->p:Landroid/support/v7/widget/ActionBarOverlayLayout;
invoke-static {v0}, Landroid/support/v4/f/af;->n(Landroid/view/View;)V
:cond_90
return-void
:cond_91
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
const/high16 v1, 0x3f80
invoke-static {v0, v1}, Landroid/support/v4/f/af;->b(Landroid/view/View;F)V
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
invoke-static {v0, v4}, Landroid/support/v4/f/af;->a(Landroid/view/View;F)V
iget-boolean v0, p0, Landroid/support/v7/a/r;->C:Z
if-eqz v0, :cond_aa
iget-object v0, p0, Landroid/support/v7/a/r;->t:Landroid/view/View;
if-eqz v0, :cond_aa
iget-object v0, p0, Landroid/support/v7/a/r;->t:Landroid/view/View;
invoke-static {v0, v4}, Landroid/support/v4/f/af;->a(Landroid/view/View;F)V
:cond_aa
iget-object v0, p0, Landroid/support/v7/a/r;->f:Landroid/support/v4/f/ay;
const/4 v1, 0x0
invoke-interface {v0, v1}, Landroid/support/v4/f/ay;->b(Landroid/view/View;)V
goto :goto_87
nop
:array_b2
.array-data 0x4
0x0t 0x0t 0x0t 0x0t
0x0t 0x0t 0x0t 0x0t
.end array-data
.end method
.method  i()V
.registers 4
const/4 v2, 0x0
iget-object v0, p0, Landroid/support/v7/a/r;->c:Landroid/support/v7/view/b$a;
if-eqz v0, :cond_10
iget-object v0, p0, Landroid/support/v7/a/r;->c:Landroid/support/v7/view/b$a;
iget-object v1, p0, Landroid/support/v7/a/r;->b:Landroid/support/v7/view/b;
invoke-interface {v0, v1}, Landroid/support/v7/view/b$a;->a(Landroid/support/v7/view/b;)V
iput-object v2, p0, Landroid/support/v7/a/r;->b:Landroid/support/v7/view/b;
iput-object v2, p0, Landroid/support/v7/a/r;->c:Landroid/support/v7/view/b$a;
:cond_10
return-void
.end method
.method public i(Z)V
.registers 7
const/4 v4, 0x1
iget-object v0, p0, Landroid/support/v7/a/r;->H:Landroid/support/v7/view/h;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/v7/a/r;->H:Landroid/support/v7/view/h;
invoke-virtual {v0}, Landroid/support/v7/view/h;->b()V
:cond_a
iget v0, p0, Landroid/support/v7/a/r;->B:I
if-nez v0, :cond_7e
sget-boolean v0, Landroid/support/v7/a/r;->k:Z
if-eqz v0, :cond_7e
iget-boolean v0, p0, Landroid/support/v7/a/r;->I:Z
if-nez v0, :cond_18
if-eqz p1, :cond_7e
:cond_18
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
const/high16 v1, 0x3f80
invoke-static {v0, v1}, Landroid/support/v4/f/af;->b(Landroid/view/View;F)V
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v0, v4}, Landroid/support/v7/widget/ActionBarContainer;->setTransitioning(Z)V
new-instance v1, Landroid/support/v7/view/h;
invoke-direct {v1}, Landroid/support/v7/view/h;-><init>()V
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContainer;->getHeight()I
move-result v0
neg-int v0, v0
int-to-float v0, v0
if-eqz p1, :cond_42
const/4 v2, 0x2
new-array v2, v2, [I
fill-array-data v2, :array_86
iget-object v3, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v3, v2}, Landroid/support/v7/widget/ActionBarContainer;->getLocationInWindow([I)V
aget v2, v2, v4
int-to-float v2, v2
sub-float/2addr v0, v2
:cond_42
iget-object v2, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
invoke-static {v2}, Landroid/support/v4/f/af;->k(Landroid/view/View;)Landroid/support/v4/f/au;
move-result-object v2
invoke-virtual {v2, v0}, Landroid/support/v4/f/au;->b(F)Landroid/support/v4/f/au;
move-result-object v2
iget-object v3, p0, Landroid/support/v7/a/r;->g:Landroid/support/v4/f/ba;
invoke-virtual {v2, v3}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/ba;)Landroid/support/v4/f/au;
invoke-virtual {v1, v2}, Landroid/support/v7/view/h;->a(Landroid/support/v4/f/au;)Landroid/support/v7/view/h;
iget-boolean v2, p0, Landroid/support/v7/a/r;->C:Z
if-eqz v2, :cond_69
iget-object v2, p0, Landroid/support/v7/a/r;->t:Landroid/view/View;
if-eqz v2, :cond_69
iget-object v2, p0, Landroid/support/v7/a/r;->t:Landroid/view/View;
invoke-static {v2}, Landroid/support/v4/f/af;->k(Landroid/view/View;)Landroid/support/v4/f/au;
move-result-object v2
invoke-virtual {v2, v0}, Landroid/support/v4/f/au;->b(F)Landroid/support/v4/f/au;
move-result-object v0
invoke-virtual {v1, v0}, Landroid/support/v7/view/h;->a(Landroid/support/v4/f/au;)Landroid/support/v7/view/h;
:cond_69
sget-object v0, Landroid/support/v7/a/r;->i:Landroid/view/animation/Interpolator;
invoke-virtual {v1, v0}, Landroid/support/v7/view/h;->a(Landroid/view/animation/Interpolator;)Landroid/support/v7/view/h;
const-wide/16 v2, 0xfa
invoke-virtual {v1, v2, v3}, Landroid/support/v7/view/h;->a(J)Landroid/support/v7/view/h;
iget-object v0, p0, Landroid/support/v7/a/r;->e:Landroid/support/v4/f/ay;
invoke-virtual {v1, v0}, Landroid/support/v7/view/h;->a(Landroid/support/v4/f/ay;)Landroid/support/v7/view/h;
iput-object v1, p0, Landroid/support/v7/a/r;->H:Landroid/support/v7/view/h;
invoke-virtual {v1}, Landroid/support/v7/view/h;->a()V
:goto_7d
return-void
:cond_7e
iget-object v0, p0, Landroid/support/v7/a/r;->e:Landroid/support/v4/f/ay;
const/4 v1, 0x0
invoke-interface {v0, v1}, Landroid/support/v4/f/ay;->b(Landroid/view/View;)V
goto :goto_7d
nop
:array_86
.array-data 0x4
0x0t 0x0t 0x0t 0x0t
0x0t 0x0t 0x0t 0x0t
.end array-data
.end method
.method public j()I
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->p()I
move-result v0
return v0
.end method
.method public j(Z)V
.registers 10
const-wide/16 v6, 0xc8
const-wide/16 v4, 0x64
const/4 v2, 0x0
if-eqz p1, :cond_25
invoke-direct {p0}, Landroid/support/v7/a/r;->p()V
:goto_a
if-eqz p1, :cond_29
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
const/4 v1, 0x4
invoke-interface {v0, v1, v4, v5}, Landroid/support/v7/widget/ac;->a(IJ)Landroid/support/v4/f/au;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/a/r;->s:Landroid/support/v7/widget/ActionBarContextView;
invoke-virtual {v1, v2, v6, v7}, Landroid/support/v7/widget/ActionBarContextView;->a(IJ)Landroid/support/v4/f/au;
move-result-object v1
:goto_19
new-instance v2, Landroid/support/v7/view/h;
invoke-direct {v2}, Landroid/support/v7/view/h;-><init>()V
invoke-virtual {v2, v0, v1}, Landroid/support/v7/view/h;->a(Landroid/support/v4/f/au;Landroid/support/v4/f/au;)Landroid/support/v7/view/h;
invoke-virtual {v2}, Landroid/support/v7/view/h;->a()V
return-void
:cond_25
invoke-direct {p0}, Landroid/support/v7/a/r;->q()V
goto :goto_a
:cond_29
iget-object v0, p0, Landroid/support/v7/a/r;->r:Landroid/support/v7/widget/ac;
invoke-interface {v0, v2, v6, v7}, Landroid/support/v7/widget/ac;->a(IJ)Landroid/support/v4/f/au;
move-result-object v1
iget-object v0, p0, Landroid/support/v7/a/r;->s:Landroid/support/v7/widget/ActionBarContextView;
const/16 v2, 0x8
invoke-virtual {v0, v2, v4, v5}, Landroid/support/v7/widget/ActionBarContextView;->a(IJ)Landroid/support/v4/f/au;
move-result-object v0
goto :goto_19
.end method
.method public k()I
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r;->q:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContainer;->getHeight()I
move-result v0
return v0
.end method
.method public l()V
.registers 2
iget-boolean v0, p0, Landroid/support/v7/a/r;->E:Z
if-eqz v0, :cond_b
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v7/a/r;->E:Z
const/4 v0, 0x1
invoke-direct {p0, v0}, Landroid/support/v7/a/r;->l(Z)V
:cond_b
return-void
.end method
.method public m()V
.registers 3
const/4 v1, 0x1
iget-boolean v0, p0, Landroid/support/v7/a/r;->E:Z
if-nez v0, :cond_a
iput-boolean v1, p0, Landroid/support/v7/a/r;->E:Z
invoke-direct {p0, v1}, Landroid/support/v7/a/r;->l(Z)V
:cond_a
return-void
.end method
.method public n()V
.registers 2
iget-object v0, p0, Landroid/support/v7/a/r;->H:Landroid/support/v7/view/h;
if-eqz v0, :cond_c
iget-object v0, p0, Landroid/support/v7/a/r;->H:Landroid/support/v7/view/h;
invoke-virtual {v0}, Landroid/support/v7/view/h;->b()V
const/4 v0, 0x0
iput-object v0, p0, Landroid/support/v7/a/r;->H:Landroid/support/v7/view/h;
:cond_c
return-void
.end method
.method public o()V
.registers 1
return-void
.end method