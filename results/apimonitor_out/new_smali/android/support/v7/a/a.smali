.class public abstract Landroid/support/v7/a/a;
.super Ljava/lang/Object;
.source "ActionBar.java"
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public abstract a()I
.end method
.method public a(Landroid/support/v7/view/b$a;)Landroid/support/v7/view/b;
.registers 3
const/4 v0, 0x0
return-object v0
.end method
.method public a(F)V
.registers 4
const/4 v0, 0x0
cmpl-float v0, p1, v0
if-eqz v0, :cond_d
new-instance v0, Ljava/lang/UnsupportedOperationException;
const-string v1, "Setting a non-zero elevation is not supported in this action bar configuration."
invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V
throw v0
:cond_d
return-void
.end method
.method public a(Landroid/content/res/Configuration;)V
.registers 2
return-void
.end method
.method public a(Ljava/lang/CharSequence;)V
.registers 2
return-void
.end method
.method public a(Z)V
.registers 2
return-void
.end method
.method public a(ILandroid/view/KeyEvent;)Z
.registers 4
const/4 v0, 0x0
return v0
.end method
.method public b(Z)V
.registers 4
if-eqz p1, :cond_a
new-instance v0, Ljava/lang/UnsupportedOperationException;
const-string v1, "Hide on content scroll is not supported in this action bar configuration."
invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V
throw v0
:cond_a
return-void
.end method
.method public abstract b()Z
.end method
.method public c()Landroid/content/Context;
.registers 2
const/4 v0, 0x0
return-object v0
.end method
.method public c(Z)V
.registers 2
return-void
.end method
.method public d()I
.registers 2
const/4 v0, 0x0
return v0
.end method
.method public d(Z)V
.registers 2
return-void
.end method
.method public e(Z)V
.registers 2
return-void
.end method
.method public e()Z
.registers 2
const/4 v0, 0x0
return v0
.end method
.method public f()Z
.registers 2
const/4 v0, 0x0
return v0
.end method
.method  g()Z
.registers 2
const/4 v0, 0x0
return v0
.end method
.method  h()V
.registers 1
return-void
.end method