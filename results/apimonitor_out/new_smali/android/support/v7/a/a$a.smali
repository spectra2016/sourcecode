.class public Landroid/support/v7/a/a$a;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "ActionBar.java"
.field public a:I
.method public constructor <init>(II)V
.registers 4
invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/a/a$a;->a:I
const v0, 0x800013
iput v0, p0, Landroid/support/v7/a/a$a;->a:I
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 6
const/4 v2, 0x0
invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
iput v2, p0, Landroid/support/v7/a/a$a;->a:I
sget-object v0, Landroid/support/v7/b/a$k;->ActionBarLayout:[I
invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
move-result-object v0
sget v1, Landroid/support/v7/b/a$k;->ActionBarLayout_android_layout_gravity:I
invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I
move-result v1
iput v1, p0, Landroid/support/v7/a/a$a;->a:I
invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
return-void
.end method
.method public constructor <init>(Landroid/support/v7/a/a$a;)V
.registers 3
invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/a/a$a;->a:I
iget v0, p1, Landroid/support/v7/a/a$a;->a:I
iput v0, p0, Landroid/support/v7/a/a$a;->a:I
return-void
.end method
.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
.registers 3
invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/a/a$a;->a:I
return-void
.end method