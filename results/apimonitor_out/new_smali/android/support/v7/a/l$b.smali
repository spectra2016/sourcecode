.class  Landroid/support/v7/a/l$b;
.super Ljava/lang/Object;
.source "AppCompatDelegateImplV7.java"
.implements Landroid/support/v7/view/b$a;
.field final synthetic a:Landroid/support/v7/a/l;
.field private b:Landroid/support/v7/view/b$a;
.method public constructor <init>(Landroid/support/v7/a/l;Landroid/support/v7/view/b$a;)V
.registers 3
iput-object p1, p0, Landroid/support/v7/a/l$b;->a:Landroid/support/v7/a/l;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p2, p0, Landroid/support/v7/a/l$b;->b:Landroid/support/v7/view/b$a;
return-void
.end method
.method public a(Landroid/support/v7/view/b;)V
.registers 5
iget-object v0, p0, Landroid/support/v7/a/l$b;->b:Landroid/support/v7/view/b$a;
invoke-interface {v0, p1}, Landroid/support/v7/view/b$a;->a(Landroid/support/v7/view/b;)V
iget-object v0, p0, Landroid/support/v7/a/l$b;->a:Landroid/support/v7/a/l;
iget-object v0, v0, Landroid/support/v7/a/l;->o:Landroid/widget/PopupWindow;
if-eqz v0, :cond_1a
iget-object v0, p0, Landroid/support/v7/a/l$b;->a:Landroid/support/v7/a/l;
iget-object v0, v0, Landroid/support/v7/a/l;->b:Landroid/view/Window;
invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/a/l$b;->a:Landroid/support/v7/a/l;
iget-object v1, v1, Landroid/support/v7/a/l;->p:Ljava/lang/Runnable;
invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z
:cond_1a
iget-object v0, p0, Landroid/support/v7/a/l$b;->a:Landroid/support/v7/a/l;
iget-object v0, v0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
if-eqz v0, :cond_42
iget-object v0, p0, Landroid/support/v7/a/l$b;->a:Landroid/support/v7/a/l;
invoke-static {v0}, Landroid/support/v7/a/l;->c(Landroid/support/v7/a/l;)V
iget-object v0, p0, Landroid/support/v7/a/l$b;->a:Landroid/support/v7/a/l;
iget-object v1, p0, Landroid/support/v7/a/l$b;->a:Landroid/support/v7/a/l;
iget-object v1, v1, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
invoke-static {v1}, Landroid/support/v4/f/af;->k(Landroid/view/View;)Landroid/support/v4/f/au;
move-result-object v1
const/4 v2, 0x0
invoke-virtual {v1, v2}, Landroid/support/v4/f/au;->a(F)Landroid/support/v4/f/au;
move-result-object v1
iput-object v1, v0, Landroid/support/v7/a/l;->q:Landroid/support/v4/f/au;
iget-object v0, p0, Landroid/support/v7/a/l$b;->a:Landroid/support/v7/a/l;
iget-object v0, v0, Landroid/support/v7/a/l;->q:Landroid/support/v4/f/au;
new-instance v1, Landroid/support/v7/a/l$b$1;
invoke-direct {v1, p0}, Landroid/support/v7/a/l$b$1;-><init>(Landroid/support/v7/a/l$b;)V
invoke-virtual {v0, v1}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/ay;)Landroid/support/v4/f/au;
:cond_42
iget-object v0, p0, Landroid/support/v7/a/l$b;->a:Landroid/support/v7/a/l;
iget-object v0, v0, Landroid/support/v7/a/l;->e:Landroid/support/v7/a/f;
if-eqz v0, :cond_53
iget-object v0, p0, Landroid/support/v7/a/l$b;->a:Landroid/support/v7/a/l;
iget-object v0, v0, Landroid/support/v7/a/l;->e:Landroid/support/v7/a/f;
iget-object v1, p0, Landroid/support/v7/a/l$b;->a:Landroid/support/v7/a/l;
iget-object v1, v1, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
invoke-interface {v0, v1}, Landroid/support/v7/a/f;->b(Landroid/support/v7/view/b;)V
:cond_53
iget-object v0, p0, Landroid/support/v7/a/l$b;->a:Landroid/support/v7/a/l;
const/4 v1, 0x0
iput-object v1, v0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
return-void
.end method
.method public a(Landroid/support/v7/view/b;Landroid/view/Menu;)Z
.registers 4
iget-object v0, p0, Landroid/support/v7/a/l$b;->b:Landroid/support/v7/view/b$a;
invoke-interface {v0, p1, p2}, Landroid/support/v7/view/b$a;->a(Landroid/support/v7/view/b;Landroid/view/Menu;)Z
move-result v0
return v0
.end method
.method public a(Landroid/support/v7/view/b;Landroid/view/MenuItem;)Z
.registers 4
iget-object v0, p0, Landroid/support/v7/a/l$b;->b:Landroid/support/v7/view/b$a;
invoke-interface {v0, p1, p2}, Landroid/support/v7/view/b$a;->a(Landroid/support/v7/view/b;Landroid/view/MenuItem;)Z
move-result v0
return v0
.end method
.method public b(Landroid/support/v7/view/b;Landroid/view/Menu;)Z
.registers 4
iget-object v0, p0, Landroid/support/v7/a/l$b;->b:Landroid/support/v7/view/b$a;
invoke-interface {v0, p1, p2}, Landroid/support/v7/view/b$a;->b(Landroid/support/v7/view/b;Landroid/view/Menu;)Z
move-result v0
return v0
.end method