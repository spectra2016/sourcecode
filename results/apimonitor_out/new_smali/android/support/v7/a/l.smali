.class  Landroid/support/v7/a/l;
.super Landroid/support/v7/a/h;
.source "AppCompatDelegateImplV7.java"
.implements Landroid/support/v4/f/m;
.implements Landroid/support/v7/view/menu/f$a;
.field private A:Z
.field private B:[Landroid/support/v7/a/l$d;
.field private C:Landroid/support/v7/a/l$d;
.field private D:Z
.field private E:Z
.field private F:I
.field private final G:Ljava/lang/Runnable;
.field private H:Z
.field private I:Landroid/graphics/Rect;
.field private J:Landroid/graphics/Rect;
.field private K:Landroid/support/v7/a/n;
.field  m:Landroid/support/v7/view/b;
.field  n:Landroid/support/v7/widget/ActionBarContextView;
.field  o:Landroid/widget/PopupWindow;
.field  p:Ljava/lang/Runnable;
.field  q:Landroid/support/v4/f/au;
.field private r:Landroid/support/v7/widget/ab;
.field private s:Landroid/support/v7/a/l$a;
.field private t:Landroid/support/v7/a/l$e;
.field private u:Z
.field private v:Landroid/view/ViewGroup;
.field private w:Landroid/widget/TextView;
.field private x:Landroid/view/View;
.field private y:Z
.field private z:Z
.method constructor <init>(Landroid/content/Context;Landroid/view/Window;Landroid/support/v7/a/f;)V
.registers 5
invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/a/h;-><init>(Landroid/content/Context;Landroid/view/Window;Landroid/support/v7/a/f;)V
const/4 v0, 0x0
iput-object v0, p0, Landroid/support/v7/a/l;->q:Landroid/support/v4/f/au;
new-instance v0, Landroid/support/v7/a/l$1;
invoke-direct {v0, p0}, Landroid/support/v7/a/l$1;-><init>(Landroid/support/v7/a/l;)V
iput-object v0, p0, Landroid/support/v7/a/l;->G:Ljava/lang/Runnable;
return-void
.end method
.method static synthetic a(Landroid/support/v7/a/l;)I
.registers 2
iget v0, p0, Landroid/support/v7/a/l;->F:I
return v0
.end method
.method private a(IZ)Landroid/support/v7/a/l$d;
.registers 7
const/4 v3, 0x0
iget-object v0, p0, Landroid/support/v7/a/l;->B:[Landroid/support/v7/a/l$d;
if-eqz v0, :cond_8
array-length v1, v0
if-gt v1, p1, :cond_15
:cond_8
add-int/lit8 v1, p1, 0x1
new-array v1, v1, [Landroid/support/v7/a/l$d;
if-eqz v0, :cond_12
array-length v2, v0
invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
:cond_12
iput-object v1, p0, Landroid/support/v7/a/l;->B:[Landroid/support/v7/a/l$d;
move-object v0, v1
:cond_15
aget-object v1, v0, p1
if-nez v1, :cond_22
new-instance v1, Landroid/support/v7/a/l$d;
invoke-direct {v1, p1}, Landroid/support/v7/a/l$d;-><init>(I)V
aput-object v1, v0, p1
move-object v0, v1
:goto_21
return-object v0
:cond_22
move-object v0, v1
goto :goto_21
.end method
.method static synthetic a(Landroid/support/v7/a/l;Landroid/view/Menu;)Landroid/support/v7/a/l$d;
.registers 3
invoke-direct {p0, p1}, Landroid/support/v7/a/l;->a(Landroid/view/Menu;)Landroid/support/v7/a/l$d;
move-result-object v0
return-object v0
.end method
.method private a(Landroid/view/Menu;)Landroid/support/v7/a/l$d;
.registers 7
const/4 v1, 0x0
iget-object v3, p0, Landroid/support/v7/a/l;->B:[Landroid/support/v7/a/l$d;
if-eqz v3, :cond_13
array-length v0, v3
:goto_6
move v2, v1
:goto_7
if-ge v2, v0, :cond_19
aget-object v1, v3, v2
if-eqz v1, :cond_15
iget-object v4, v1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
if-ne v4, p1, :cond_15
move-object v0, v1
:goto_12
return-object v0
:cond_13
move v0, v1
goto :goto_6
:cond_15
add-int/lit8 v1, v2, 0x1
move v2, v1
goto :goto_7
:cond_19
const/4 v0, 0x0
goto :goto_12
.end method
.method private a(ILandroid/support/v7/a/l$d;Landroid/view/Menu;)V
.registers 5
if-nez p3, :cond_13
if-nez p2, :cond_f
if-ltz p1, :cond_f
iget-object v0, p0, Landroid/support/v7/a/l;->B:[Landroid/support/v7/a/l$d;
array-length v0, v0
if-ge p1, v0, :cond_f
iget-object v0, p0, Landroid/support/v7/a/l;->B:[Landroid/support/v7/a/l$d;
aget-object p2, v0, p1
:cond_f
if-eqz p2, :cond_13
iget-object p3, p2, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
:cond_13
if-eqz p2, :cond_1a
iget-boolean v0, p2, Landroid/support/v7/a/l$d;->o:Z
if-nez v0, :cond_1a
:goto_19
:cond_19
return-void
:cond_1a
invoke-virtual {p0}, Landroid/support/v7/a/l;->o()Z
move-result v0
if-nez v0, :cond_19
iget-object v0, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
invoke-interface {v0, p1, p3}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V
goto :goto_19
.end method
.method private a(Landroid/support/v7/a/l$d;Landroid/view/KeyEvent;)V
.registers 13
const/4 v1, -0x1
const/4 v3, 0x0
const/4 v9, 0x1
const/4 v2, -0x2
iget-boolean v0, p1, Landroid/support/v7/a/l$d;->o:Z
if-nez v0, :cond_e
invoke-virtual {p0}, Landroid/support/v7/a/l;->o()Z
move-result v0
if-eqz v0, :cond_f
:goto_e
:cond_e
return-void
:cond_f
iget v0, p1, Landroid/support/v7/a/l$d;->a:I
if-nez v0, :cond_34
iget-object v4, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
move-result-object v0
iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I
and-int/lit8 v0, v0, 0xf
const/4 v5, 0x4
if-ne v0, v5, :cond_48
move v0, v9
:goto_25
invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;
move-result-object v4
iget v4, v4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I
const/16 v5, 0xb
if-lt v4, v5, :cond_4a
move v4, v9
:goto_30
if-eqz v0, :cond_34
if-nez v4, :cond_e
:cond_34
invoke-virtual {p0}, Landroid/support/v7/a/l;->p()Landroid/view/Window$Callback;
move-result-object v0
if-eqz v0, :cond_4c
iget v4, p1, Landroid/support/v7/a/l$d;->a:I
iget-object v5, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-interface {v0, v4, v5}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z
move-result v0
if-nez v0, :cond_4c
invoke-direct {p0, p1, v9}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;Z)V
goto :goto_e
:cond_48
move v0, v3
goto :goto_25
:cond_4a
move v4, v3
goto :goto_30
:cond_4c
iget-object v0, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
const-string v4, "window"
invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
move-object v8, v0
check-cast v8, Landroid/view/WindowManager;
if-eqz v8, :cond_e
invoke-direct {p0, p1, p2}, Landroid/support/v7/a/l;->b(Landroid/support/v7/a/l$d;Landroid/view/KeyEvent;)Z
move-result v0
if-eqz v0, :cond_e
iget-object v0, p1, Landroid/support/v7/a/l$d;->g:Landroid/view/ViewGroup;
if-eqz v0, :cond_67
iget-boolean v0, p1, Landroid/support/v7/a/l$d;->q:Z
if-eqz v0, :cond_f1
:cond_67
iget-object v0, p1, Landroid/support/v7/a/l$d;->g:Landroid/view/ViewGroup;
if-nez v0, :cond_df
invoke-direct {p0, p1}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;)Z
move-result v0
if-eqz v0, :cond_e
iget-object v0, p1, Landroid/support/v7/a/l$d;->g:Landroid/view/ViewGroup;
if-eqz v0, :cond_e
:cond_75
:goto_75
invoke-direct {p0, p1}, Landroid/support/v7/a/l;->c(Landroid/support/v7/a/l$d;)Z
move-result v0
if-eqz v0, :cond_e
invoke-virtual {p1}, Landroid/support/v7/a/l$d;->a()Z
move-result v0
if-eqz v0, :cond_e
iget-object v0, p1, Landroid/support/v7/a/l$d;->h:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
if-nez v0, :cond_103
new-instance v0, Landroid/view/ViewGroup$LayoutParams;
invoke-direct {v0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V
move-object v1, v0
:goto_8f
iget v0, p1, Landroid/support/v7/a/l$d;->b:I
iget-object v4, p1, Landroid/support/v7/a/l$d;->g:Landroid/view/ViewGroup;
invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setBackgroundResource(I)V
iget-object v0, p1, Landroid/support/v7/a/l$d;->h:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;
move-result-object v0
if-eqz v0, :cond_a9
instance-of v4, v0, Landroid/view/ViewGroup;
if-eqz v4, :cond_a9
check-cast v0, Landroid/view/ViewGroup;
iget-object v4, p1, Landroid/support/v7/a/l$d;->h:Landroid/view/View;
invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
:cond_a9
iget-object v0, p1, Landroid/support/v7/a/l$d;->g:Landroid/view/ViewGroup;
iget-object v4, p1, Landroid/support/v7/a/l$d;->h:Landroid/view/View;
invoke-virtual {v0, v4, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
iget-object v0, p1, Landroid/support/v7/a/l$d;->h:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z
move-result v0
if-nez v0, :cond_bd
iget-object v0, p1, Landroid/support/v7/a/l$d;->h:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z
:cond_bd
move v1, v2
:goto_be
:cond_be
iput-boolean v3, p1, Landroid/support/v7/a/l$d;->n:Z
new-instance v0, Landroid/view/WindowManager$LayoutParams;
iget v3, p1, Landroid/support/v7/a/l$d;->d:I
iget v4, p1, Landroid/support/v7/a/l$d;->e:I
const/16 v5, 0x3ea
const/high16 v6, 0x82
const/4 v7, -0x3
invoke-direct/range {v0 .. v7}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIIIII)V
iget v1, p1, Landroid/support/v7/a/l$d;->c:I
iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I
iget v1, p1, Landroid/support/v7/a/l$d;->f:I
iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I
iget-object v1, p1, Landroid/support/v7/a/l$d;->g:Landroid/view/ViewGroup;
invoke-interface {v8, v1, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
iput-boolean v9, p1, Landroid/support/v7/a/l$d;->o:Z
goto/16 :goto_e
:cond_df
iget-boolean v0, p1, Landroid/support/v7/a/l$d;->q:Z
if-eqz v0, :cond_75
iget-object v0, p1, Landroid/support/v7/a/l$d;->g:Landroid/view/ViewGroup;
invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I
move-result v0
if-lez v0, :cond_75
iget-object v0, p1, Landroid/support/v7/a/l$d;->g:Landroid/view/ViewGroup;
invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V
goto :goto_75
:cond_f1
iget-object v0, p1, Landroid/support/v7/a/l$d;->i:Landroid/view/View;
if-eqz v0, :cond_101
iget-object v0, p1, Landroid/support/v7/a/l$d;->i:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
if-eqz v0, :cond_101
iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I
if-eq v0, v1, :cond_be
:cond_101
move v1, v2
goto :goto_be
:cond_103
move-object v1, v0
goto :goto_8f
.end method
.method private a(Landroid/support/v7/a/l$d;Z)V
.registers 7
const/4 v3, 0x0
const/4 v2, 0x0
if-eqz p2, :cond_1a
iget v0, p1, Landroid/support/v7/a/l$d;->a:I
if-nez v0, :cond_1a
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
if-eqz v0, :cond_1a
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v0}, Landroid/support/v7/widget/ab;->e()Z
move-result v0
if-eqz v0, :cond_1a
iget-object v0, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-direct {p0, v0}, Landroid/support/v7/a/l;->b(Landroid/support/v7/view/menu/f;)V
:goto_19
:cond_19
return-void
:cond_1a
iget-object v0, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
const-string v1, "window"
invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/WindowManager;
if-eqz v0, :cond_3a
iget-boolean v1, p1, Landroid/support/v7/a/l$d;->o:Z
if-eqz v1, :cond_3a
iget-object v1, p1, Landroid/support/v7/a/l$d;->g:Landroid/view/ViewGroup;
if-eqz v1, :cond_3a
iget-object v1, p1, Landroid/support/v7/a/l$d;->g:Landroid/view/ViewGroup;
invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
if-eqz p2, :cond_3a
iget v0, p1, Landroid/support/v7/a/l$d;->a:I
invoke-direct {p0, v0, p1, v3}, Landroid/support/v7/a/l;->a(ILandroid/support/v7/a/l$d;Landroid/view/Menu;)V
:cond_3a
iput-boolean v2, p1, Landroid/support/v7/a/l$d;->m:Z
iput-boolean v2, p1, Landroid/support/v7/a/l$d;->n:Z
iput-boolean v2, p1, Landroid/support/v7/a/l$d;->o:Z
iput-object v3, p1, Landroid/support/v7/a/l$d;->h:Landroid/view/View;
const/4 v0, 0x1
iput-boolean v0, p1, Landroid/support/v7/a/l$d;->q:Z
iget-object v0, p0, Landroid/support/v7/a/l;->C:Landroid/support/v7/a/l$d;
if-ne v0, p1, :cond_19
iput-object v3, p0, Landroid/support/v7/a/l;->C:Landroid/support/v7/a/l$d;
goto :goto_19
.end method
.method static synthetic a(Landroid/support/v7/a/l;I)V
.registers 2
invoke-direct {p0, p1}, Landroid/support/v7/a/l;->f(I)V
return-void
.end method
.method static synthetic a(Landroid/support/v7/a/l;ILandroid/support/v7/a/l$d;Landroid/view/Menu;)V
.registers 4
invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/a/l;->a(ILandroid/support/v7/a/l$d;Landroid/view/Menu;)V
return-void
.end method
.method static synthetic a(Landroid/support/v7/a/l;Landroid/support/v7/a/l$d;Z)V
.registers 3
invoke-direct {p0, p1, p2}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;Z)V
return-void
.end method
.method static synthetic a(Landroid/support/v7/a/l;Landroid/support/v7/view/menu/f;)V
.registers 2
invoke-direct {p0, p1}, Landroid/support/v7/a/l;->b(Landroid/support/v7/view/menu/f;)V
return-void
.end method
.method private a(Landroid/support/v7/view/menu/f;Z)V
.registers 9
const/16 v5, 0x6c
const/4 v3, 0x1
const/4 v4, 0x0
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
if-eqz v0, :cond_8a
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v0}, Landroid/support/v7/widget/ab;->d()Z
move-result v0
if-eqz v0, :cond_8a
iget-object v0, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
move-result-object v0
invoke-static {v0}, Landroid/support/v4/f/aq;->a(Landroid/view/ViewConfiguration;)Z
move-result v0
if-eqz v0, :cond_24
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v0}, Landroid/support/v7/widget/ab;->f()Z
move-result v0
if-eqz v0, :cond_8a
:cond_24
invoke-virtual {p0}, Landroid/support/v7/a/l;->p()Landroid/view/Window$Callback;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v1}, Landroid/support/v7/widget/ab;->e()Z
move-result v1
if-eqz v1, :cond_32
if-nez p2, :cond_75
:cond_32
if-eqz v0, :cond_74
invoke-virtual {p0}, Landroid/support/v7/a/l;->o()Z
move-result v1
if-nez v1, :cond_74
iget-boolean v1, p0, Landroid/support/v7/a/l;->E:Z
if-eqz v1, :cond_54
iget v1, p0, Landroid/support/v7/a/l;->F:I
and-int/lit8 v1, v1, 0x1
if-eqz v1, :cond_54
iget-object v1, p0, Landroid/support/v7/a/l;->b:Landroid/view/Window;
invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;
move-result-object v1
iget-object v2, p0, Landroid/support/v7/a/l;->G:Ljava/lang/Runnable;
invoke-virtual {v1, v2}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z
iget-object v1, p0, Landroid/support/v7/a/l;->G:Ljava/lang/Runnable;
invoke-interface {v1}, Ljava/lang/Runnable;->run()V
:cond_54
invoke-direct {p0, v4, v3}, Landroid/support/v7/a/l;->a(IZ)Landroid/support/v7/a/l$d;
move-result-object v1
iget-object v2, v1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
if-eqz v2, :cond_74
iget-boolean v2, v1, Landroid/support/v7/a/l$d;->r:Z
if-nez v2, :cond_74
iget-object v2, v1, Landroid/support/v7/a/l$d;->i:Landroid/view/View;
iget-object v3, v1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-interface {v0, v4, v2, v3}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
move-result v2
if-eqz v2, :cond_74
iget-object v1, v1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-interface {v0, v5, v1}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v0}, Landroid/support/v7/widget/ab;->g()Z
:goto_74
:cond_74
return-void
:cond_75
iget-object v1, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v1}, Landroid/support/v7/widget/ab;->h()Z
invoke-virtual {p0}, Landroid/support/v7/a/l;->o()Z
move-result v1
if-nez v1, :cond_74
invoke-direct {p0, v4, v3}, Landroid/support/v7/a/l;->a(IZ)Landroid/support/v7/a/l$d;
move-result-object v1
iget-object v1, v1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-interface {v0, v5, v1}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V
goto :goto_74
:cond_8a
invoke-direct {p0, v4, v3}, Landroid/support/v7/a/l;->a(IZ)Landroid/support/v7/a/l$d;
move-result-object v0
iput-boolean v3, v0, Landroid/support/v7/a/l$d;->q:Z
invoke-direct {p0, v0, v4}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;Z)V
const/4 v1, 0x0
invoke-direct {p0, v0, v1}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;Landroid/view/KeyEvent;)V
goto :goto_74
.end method
.method private a(Landroid/support/v7/a/l$d;)Z
.registers 4
invoke-virtual {p0}, Landroid/support/v7/a/l;->m()Landroid/content/Context;
move-result-object v0
invoke-virtual {p1, v0}, Landroid/support/v7/a/l$d;->a(Landroid/content/Context;)V
new-instance v0, Landroid/support/v7/a/l$c;
iget-object v1, p1, Landroid/support/v7/a/l$d;->l:Landroid/content/Context;
invoke-direct {v0, p0, v1}, Landroid/support/v7/a/l$c;-><init>(Landroid/support/v7/a/l;Landroid/content/Context;)V
iput-object v0, p1, Landroid/support/v7/a/l$d;->g:Landroid/view/ViewGroup;
const/16 v0, 0x51
iput v0, p1, Landroid/support/v7/a/l$d;->c:I
const/4 v0, 0x1
return v0
.end method
.method private a(Landroid/support/v7/a/l$d;ILandroid/view/KeyEvent;I)Z
.registers 7
const/4 v0, 0x0
invoke-virtual {p3}, Landroid/view/KeyEvent;->isSystem()Z
move-result v1
if-eqz v1, :cond_8
:cond_7
:goto_7
return v0
:cond_8
iget-boolean v1, p1, Landroid/support/v7/a/l$d;->m:Z
if-nez v1, :cond_12
invoke-direct {p0, p1, p3}, Landroid/support/v7/a/l;->b(Landroid/support/v7/a/l$d;Landroid/view/KeyEvent;)Z
move-result v1
if-eqz v1, :cond_1c
:cond_12
iget-object v1, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
if-eqz v1, :cond_1c
iget-object v0, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-virtual {v0, p2, p3, p4}, Landroid/support/v7/view/menu/f;->performShortcut(ILandroid/view/KeyEvent;I)Z
move-result v0
:cond_1c
if-eqz v0, :cond_7
and-int/lit8 v1, p4, 0x1
if-nez v1, :cond_7
iget-object v1, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
if-nez v1, :cond_7
const/4 v1, 0x1
invoke-direct {p0, p1, v1}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;Z)V
goto :goto_7
.end method
.method static synthetic a(Landroid/support/v7/a/l;Z)Z
.registers 2
iput-boolean p1, p0, Landroid/support/v7/a/l;->E:Z
return p1
.end method
.method private a(Landroid/view/ViewParent;)Z
.registers 6
const/4 v2, 0x0
if-nez p1, :cond_5
move v0, v2
:goto_4
return v0
:cond_5
iget-object v0, p0, Landroid/support/v7/a/l;->b:Landroid/view/Window;
invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
move-result-object v3
move-object v1, p1
:goto_c
if-nez v1, :cond_10
const/4 v0, 0x1
goto :goto_4
:cond_10
if-eq v1, v3, :cond_1f
instance-of v0, v1, Landroid/view/View;
if-eqz v0, :cond_1f
move-object v0, v1
check-cast v0, Landroid/view/View;
invoke-static {v0}, Landroid/support/v4/f/af;->u(Landroid/view/View;)Z
move-result v0
if-eqz v0, :cond_21
:cond_1f
move v0, v2
goto :goto_4
:cond_21
invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;
move-result-object v1
goto :goto_c
.end method
.method static synthetic b(Landroid/support/v7/a/l;I)I
.registers 2
iput p1, p0, Landroid/support/v7/a/l;->F:I
return p1
.end method
.method static synthetic b(Landroid/support/v7/a/l;)V
.registers 1
invoke-direct {p0}, Landroid/support/v7/a/l;->x()V
return-void
.end method
.method private b(Landroid/support/v7/view/menu/f;)V
.registers 4
iget-boolean v0, p0, Landroid/support/v7/a/l;->A:Z
if-eqz v0, :cond_5
:goto_4
return-void
:cond_5
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v7/a/l;->A:Z
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v0}, Landroid/support/v7/widget/ab;->j()V
invoke-virtual {p0}, Landroid/support/v7/a/l;->p()Landroid/view/Window$Callback;
move-result-object v0
if-eqz v0, :cond_1e
invoke-virtual {p0}, Landroid/support/v7/a/l;->o()Z
move-result v1
if-nez v1, :cond_1e
const/16 v1, 0x6c
invoke-interface {v0, v1, p1}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V
:cond_1e
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v7/a/l;->A:Z
goto :goto_4
.end method
.method private b(Landroid/support/v7/a/l$d;)Z
.registers 8
const/4 v5, 0x1
iget-object v1, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
iget v0, p1, Landroid/support/v7/a/l$d;->a:I
if-eqz v0, :cond_d
iget v0, p1, Landroid/support/v7/a/l$d;->a:I
const/16 v2, 0x6c
if-ne v0, v2, :cond_71
:cond_d
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
if-eqz v0, :cond_71
new-instance v2, Landroid/util/TypedValue;
invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V
invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v3
sget v0, Landroid/support/v7/b/a$a;->actionBarTheme:I
invoke-virtual {v3, v0, v2, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z
const/4 v0, 0x0
iget v4, v2, Landroid/util/TypedValue;->resourceId:I
if-eqz v4, :cond_6b
invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;
move-result-object v0
invoke-virtual {v0, v3}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V
iget v4, v2, Landroid/util/TypedValue;->resourceId:I
invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V
sget v4, Landroid/support/v7/b/a$a;->actionBarWidgetTheme:I
invoke-virtual {v0, v4, v2, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z
:goto_39
iget v4, v2, Landroid/util/TypedValue;->resourceId:I
if-eqz v4, :cond_4f
if-nez v0, :cond_4a
invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;
move-result-object v0
invoke-virtual {v0, v3}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V
:cond_4a
iget v2, v2, Landroid/util/TypedValue;->resourceId:I
invoke-virtual {v0, v2, v5}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V
:cond_4f
move-object v2, v0
if-eqz v2, :cond_71
new-instance v0, Landroid/support/v7/view/d;
const/4 v3, 0x0
invoke-direct {v0, v1, v3}, Landroid/support/v7/view/d;-><init>(Landroid/content/Context;I)V
invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v1
invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V
:goto_5f
new-instance v1, Landroid/support/v7/view/menu/f;
invoke-direct {v1, v0}, Landroid/support/v7/view/menu/f;-><init>(Landroid/content/Context;)V
invoke-virtual {v1, p0}, Landroid/support/v7/view/menu/f;->a(Landroid/support/v7/view/menu/f$a;)V
invoke-virtual {p1, v1}, Landroid/support/v7/a/l$d;->a(Landroid/support/v7/view/menu/f;)V
return v5
:cond_6b
sget v4, Landroid/support/v7/b/a$a;->actionBarWidgetTheme:I
invoke-virtual {v3, v4, v2, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z
goto :goto_39
:cond_71
move-object v0, v1
goto :goto_5f
.end method
.method private b(Landroid/support/v7/a/l$d;Landroid/view/KeyEvent;)Z
.registers 11
const/4 v7, 0x0
const/4 v1, 0x1
const/4 v2, 0x0
invoke-virtual {p0}, Landroid/support/v7/a/l;->o()Z
move-result v0
if-eqz v0, :cond_a
:goto_9
:cond_9
return v2
:cond_a
iget-boolean v0, p1, Landroid/support/v7/a/l$d;->m:Z
if-eqz v0, :cond_10
move v2, v1
goto :goto_9
:cond_10
iget-object v0, p0, Landroid/support/v7/a/l;->C:Landroid/support/v7/a/l$d;
if-eqz v0, :cond_1d
iget-object v0, p0, Landroid/support/v7/a/l;->C:Landroid/support/v7/a/l$d;
if-eq v0, p1, :cond_1d
iget-object v0, p0, Landroid/support/v7/a/l;->C:Landroid/support/v7/a/l$d;
invoke-direct {p0, v0, v2}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;Z)V
:cond_1d
invoke-virtual {p0}, Landroid/support/v7/a/l;->p()Landroid/view/Window$Callback;
move-result-object v3
if-eqz v3, :cond_2b
iget v0, p1, Landroid/support/v7/a/l$d;->a:I
invoke-interface {v3, v0}, Landroid/view/Window$Callback;->onCreatePanelView(I)Landroid/view/View;
move-result-object v0
iput-object v0, p1, Landroid/support/v7/a/l$d;->i:Landroid/view/View;
:cond_2b
iget v0, p1, Landroid/support/v7/a/l$d;->a:I
if-eqz v0, :cond_35
iget v0, p1, Landroid/support/v7/a/l$d;->a:I
const/16 v4, 0x6c
if-ne v0, v4, :cond_a0
:cond_35
move v0, v1
:goto_36
if-eqz v0, :cond_41
iget-object v4, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
if-eqz v4, :cond_41
iget-object v4, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v4}, Landroid/support/v7/widget/ab;->i()V
:cond_41
iget-object v4, p1, Landroid/support/v7/a/l$d;->i:Landroid/view/View;
if-nez v4, :cond_f3
if-eqz v0, :cond_4f
invoke-virtual {p0}, Landroid/support/v7/a/l;->l()Landroid/support/v7/a/a;
move-result-object v4
instance-of v4, v4, Landroid/support/v7/a/o;
if-nez v4, :cond_f3
:cond_4f
iget-object v4, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
if-eqz v4, :cond_57
iget-boolean v4, p1, Landroid/support/v7/a/l$d;->r:Z
if-eqz v4, :cond_a4
:cond_57
iget-object v4, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
if-nez v4, :cond_65
invoke-direct {p0, p1}, Landroid/support/v7/a/l;->b(Landroid/support/v7/a/l$d;)Z
move-result v4
if-eqz v4, :cond_9
iget-object v4, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
if-eqz v4, :cond_9
:cond_65
if-eqz v0, :cond_7f
iget-object v4, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
if-eqz v4, :cond_7f
iget-object v4, p0, Landroid/support/v7/a/l;->s:Landroid/support/v7/a/l$a;
if-nez v4, :cond_76
new-instance v4, Landroid/support/v7/a/l$a;
invoke-direct {v4, p0, v7}, Landroid/support/v7/a/l$a;-><init>(Landroid/support/v7/a/l;Landroid/support/v7/a/l$1;)V
iput-object v4, p0, Landroid/support/v7/a/l;->s:Landroid/support/v7/a/l$a;
:cond_76
iget-object v4, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
iget-object v5, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
iget-object v6, p0, Landroid/support/v7/a/l;->s:Landroid/support/v7/a/l$a;
invoke-interface {v4, v5, v6}, Landroid/support/v7/widget/ab;->a(Landroid/view/Menu;Landroid/support/v7/view/menu/l$a;)V
:cond_7f
iget-object v4, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-virtual {v4}, Landroid/support/v7/view/menu/f;->g()V
iget v4, p1, Landroid/support/v7/a/l$d;->a:I
iget-object v5, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-interface {v3, v4, v5}, Landroid/view/Window$Callback;->onCreatePanelMenu(ILandroid/view/Menu;)Z
move-result v4
if-nez v4, :cond_a2
invoke-virtual {p1, v7}, Landroid/support/v7/a/l$d;->a(Landroid/support/v7/view/menu/f;)V
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
iget-object v1, p0, Landroid/support/v7/a/l;->s:Landroid/support/v7/a/l$a;
invoke-interface {v0, v7, v1}, Landroid/support/v7/widget/ab;->a(Landroid/view/Menu;Landroid/support/v7/view/menu/l$a;)V
goto/16 :goto_9
:cond_a0
move v0, v2
goto :goto_36
:cond_a2
iput-boolean v2, p1, Landroid/support/v7/a/l$d;->r:Z
:cond_a4
iget-object v4, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-virtual {v4}, Landroid/support/v7/view/menu/f;->g()V
iget-object v4, p1, Landroid/support/v7/a/l$d;->s:Landroid/os/Bundle;
if-eqz v4, :cond_b6
iget-object v4, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
iget-object v5, p1, Landroid/support/v7/a/l$d;->s:Landroid/os/Bundle;
invoke-virtual {v4, v5}, Landroid/support/v7/view/menu/f;->b(Landroid/os/Bundle;)V
iput-object v7, p1, Landroid/support/v7/a/l$d;->s:Landroid/os/Bundle;
:cond_b6
iget-object v4, p1, Landroid/support/v7/a/l$d;->i:Landroid/view/View;
iget-object v5, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-interface {v3, v2, v4, v5}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
move-result v3
if-nez v3, :cond_d4
if-eqz v0, :cond_cd
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
if-eqz v0, :cond_cd
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
iget-object v1, p0, Landroid/support/v7/a/l;->s:Landroid/support/v7/a/l$a;
invoke-interface {v0, v7, v1}, Landroid/support/v7/widget/ab;->a(Landroid/view/Menu;Landroid/support/v7/view/menu/l$a;)V
:cond_cd
iget-object v0, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-virtual {v0}, Landroid/support/v7/view/menu/f;->h()V
goto/16 :goto_9
:cond_d4
if-eqz p2, :cond_fc
invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I
move-result v0
:goto_da
invoke-static {v0}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;
move-result-object v0
invoke-virtual {v0}, Landroid/view/KeyCharacterMap;->getKeyboardType()I
move-result v0
if-eq v0, v1, :cond_fe
move v0, v1
:goto_e5
iput-boolean v0, p1, Landroid/support/v7/a/l$d;->p:Z
iget-object v0, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
iget-boolean v3, p1, Landroid/support/v7/a/l$d;->p:Z
invoke-virtual {v0, v3}, Landroid/support/v7/view/menu/f;->setQwertyMode(Z)V
iget-object v0, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-virtual {v0}, Landroid/support/v7/view/menu/f;->h()V
:cond_f3
iput-boolean v1, p1, Landroid/support/v7/a/l$d;->m:Z
iput-boolean v2, p1, Landroid/support/v7/a/l$d;->n:Z
iput-object p1, p0, Landroid/support/v7/a/l;->C:Landroid/support/v7/a/l$d;
move v2, v1
goto/16 :goto_9
:cond_fc
const/4 v0, -0x1
goto :goto_da
:cond_fe
move v0, v2
goto :goto_e5
.end method
.method static synthetic c(Landroid/support/v7/a/l;I)I
.registers 3
invoke-direct {p0, p1}, Landroid/support/v7/a/l;->g(I)I
move-result v0
return v0
.end method
.method static synthetic c(Landroid/support/v7/a/l;)V
.registers 1
invoke-direct {p0}, Landroid/support/v7/a/l;->v()V
return-void
.end method
.method private c(Landroid/support/v7/a/l$d;)Z
.registers 6
const/4 v1, 0x1
const/4 v2, 0x0
iget-object v0, p1, Landroid/support/v7/a/l$d;->i:Landroid/view/View;
if-eqz v0, :cond_b
iget-object v0, p1, Landroid/support/v7/a/l$d;->i:Landroid/view/View;
iput-object v0, p1, Landroid/support/v7/a/l$d;->h:Landroid/view/View;
:goto_a
return v1
:cond_b
iget-object v0, p1, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
if-nez v0, :cond_11
move v1, v2
goto :goto_a
:cond_11
iget-object v0, p0, Landroid/support/v7/a/l;->t:Landroid/support/v7/a/l$e;
if-nez v0, :cond_1d
new-instance v0, Landroid/support/v7/a/l$e;
const/4 v3, 0x0
invoke-direct {v0, p0, v3}, Landroid/support/v7/a/l$e;-><init>(Landroid/support/v7/a/l;Landroid/support/v7/a/l$1;)V
iput-object v0, p0, Landroid/support/v7/a/l;->t:Landroid/support/v7/a/l$e;
:cond_1d
iget-object v0, p0, Landroid/support/v7/a/l;->t:Landroid/support/v7/a/l$e;
invoke-virtual {p1, v0}, Landroid/support/v7/a/l$d;->a(Landroid/support/v7/view/menu/l$a;)Landroid/support/v7/view/menu/m;
move-result-object v0
check-cast v0, Landroid/view/View;
iput-object v0, p1, Landroid/support/v7/a/l$d;->h:Landroid/view/View;
iget-object v0, p1, Landroid/support/v7/a/l$d;->h:Landroid/view/View;
if-eqz v0, :cond_2e
move v0, v1
:goto_2c
move v1, v0
goto :goto_a
:cond_2e
move v0, v2
goto :goto_2c
.end method
.method private d(I)V
.registers 4
const/4 v1, 0x1
invoke-direct {p0, p1, v1}, Landroid/support/v7/a/l;->a(IZ)Landroid/support/v7/a/l$d;
move-result-object v0
invoke-direct {p0, v0, v1}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;Z)V
return-void
.end method
.method static synthetic d(Landroid/support/v7/a/l;I)V
.registers 2
invoke-direct {p0, p1}, Landroid/support/v7/a/l;->d(I)V
return-void
.end method
.method private d(ILandroid/view/KeyEvent;)Z
.registers 5
invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I
move-result v0
if-nez v0, :cond_14
const/4 v0, 0x1
invoke-direct {p0, p1, v0}, Landroid/support/v7/a/l;->a(IZ)Landroid/support/v7/a/l$d;
move-result-object v0
iget-boolean v1, v0, Landroid/support/v7/a/l$d;->o:Z
if-nez v1, :cond_14
invoke-direct {p0, v0, p2}, Landroid/support/v7/a/l;->b(Landroid/support/v7/a/l$d;Landroid/view/KeyEvent;)Z
move-result v0
:goto_13
return v0
:cond_14
const/4 v0, 0x0
goto :goto_13
.end method
.method private e(I)V
.registers 5
const/4 v2, 0x1
iget v0, p0, Landroid/support/v7/a/l;->F:I
shl-int v1, v2, p1
or-int/2addr v0, v1
iput v0, p0, Landroid/support/v7/a/l;->F:I
iget-boolean v0, p0, Landroid/support/v7/a/l;->E:Z
if-nez v0, :cond_19
iget-object v0, p0, Landroid/support/v7/a/l;->b:Landroid/view/Window;
invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/a/l;->G:Ljava/lang/Runnable;
invoke-static {v0, v1}, Landroid/support/v4/f/af;->a(Landroid/view/View;Ljava/lang/Runnable;)V
iput-boolean v2, p0, Landroid/support/v7/a/l;->E:Z
:cond_19
return-void
.end method
.method private e(ILandroid/view/KeyEvent;)Z
.registers 7
const/4 v2, 0x1
const/4 v1, 0x0
iget-object v0, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
if-eqz v0, :cond_8
move v0, v1
:goto_7
return v0
:cond_8
invoke-direct {p0, p1, v2}, Landroid/support/v7/a/l;->a(IZ)Landroid/support/v7/a/l$d;
move-result-object v3
if-nez p1, :cond_5a
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
if-eqz v0, :cond_5a
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v0}, Landroid/support/v7/widget/ab;->d()Z
move-result v0
if-eqz v0, :cond_5a
iget-object v0, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
move-result-object v0
invoke-static {v0}, Landroid/support/v4/f/aq;->a(Landroid/view/ViewConfiguration;)Z
move-result v0
if-nez v0, :cond_5a
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v0}, Landroid/support/v7/widget/ab;->e()Z
move-result v0
if-nez v0, :cond_53
invoke-virtual {p0}, Landroid/support/v7/a/l;->o()Z
move-result v0
if-nez v0, :cond_85
invoke-direct {p0, v3, p2}, Landroid/support/v7/a/l;->b(Landroid/support/v7/a/l$d;Landroid/view/KeyEvent;)Z
move-result v0
if-eqz v0, :cond_85
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v0}, Landroid/support/v7/widget/ab;->g()Z
move-result v2
:goto_40
if-eqz v2, :cond_51
iget-object v0, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
const-string v3, "audio"
invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/media/AudioManager;
if-eqz v0, :cond_7d
invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V
:goto_51
:cond_51
move v0, v2
goto :goto_7
:cond_53
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v0}, Landroid/support/v7/widget/ab;->h()Z
move-result v2
goto :goto_40
:cond_5a
iget-boolean v0, v3, Landroid/support/v7/a/l$d;->o:Z
if-nez v0, :cond_62
iget-boolean v0, v3, Landroid/support/v7/a/l$d;->n:Z
if-eqz v0, :cond_69
:cond_62
iget-boolean v0, v3, Landroid/support/v7/a/l$d;->o:Z
invoke-direct {p0, v3, v2}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;Z)V
move v2, v0
goto :goto_40
:cond_69
iget-boolean v0, v3, Landroid/support/v7/a/l$d;->m:Z
if-eqz v0, :cond_85
iget-boolean v0, v3, Landroid/support/v7/a/l$d;->r:Z
if-eqz v0, :cond_87
iput-boolean v1, v3, Landroid/support/v7/a/l$d;->m:Z
invoke-direct {p0, v3, p2}, Landroid/support/v7/a/l;->b(Landroid/support/v7/a/l$d;Landroid/view/KeyEvent;)Z
move-result v0
:goto_77
if-eqz v0, :cond_85
invoke-direct {p0, v3, p2}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;Landroid/view/KeyEvent;)V
goto :goto_40
:cond_7d
const-string v0, "AppCompatDelegate"
const-string v1, "Couldn\'t get audio manager"
invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_51
:cond_85
move v2, v1
goto :goto_40
:cond_87
move v0, v2
goto :goto_77
.end method
.method private f(I)V
.registers 7
const/4 v4, 0x1
const/4 v3, 0x0
invoke-direct {p0, p1, v4}, Landroid/support/v7/a/l;->a(IZ)Landroid/support/v7/a/l$d;
move-result-object v0
iget-object v1, v0, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
if-eqz v1, :cond_26
new-instance v1, Landroid/os/Bundle;
invoke-direct {v1}, Landroid/os/Bundle;-><init>()V
iget-object v2, v0, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-virtual {v2, v1}, Landroid/support/v7/view/menu/f;->a(Landroid/os/Bundle;)V
invoke-virtual {v1}, Landroid/os/Bundle;->size()I
move-result v2
if-lez v2, :cond_1c
iput-object v1, v0, Landroid/support/v7/a/l$d;->s:Landroid/os/Bundle;
:cond_1c
iget-object v1, v0, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-virtual {v1}, Landroid/support/v7/view/menu/f;->g()V
iget-object v1, v0, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-virtual {v1}, Landroid/support/v7/view/menu/f;->clear()V
:cond_26
iput-boolean v4, v0, Landroid/support/v7/a/l$d;->r:Z
iput-boolean v4, v0, Landroid/support/v7/a/l$d;->q:Z
const/16 v0, 0x6c
if-eq p1, v0, :cond_30
if-nez p1, :cond_40
:cond_30
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
if-eqz v0, :cond_40
invoke-direct {p0, v3, v3}, Landroid/support/v7/a/l;->a(IZ)Landroid/support/v7/a/l$d;
move-result-object v0
if-eqz v0, :cond_40
iput-boolean v3, v0, Landroid/support/v7/a/l$d;->m:Z
const/4 v1, 0x0
invoke-direct {p0, v0, v1}, Landroid/support/v7/a/l;->b(Landroid/support/v7/a/l$d;Landroid/view/KeyEvent;)Z
:cond_40
return-void
.end method
.method private g(I)I
.registers 10
const/4 v6, -0x1
const/4 v3, 0x1
const/4 v2, 0x0
iget-object v0, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
if-eqz v0, :cond_be
iget-object v0, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;
if-eqz v0, :cond_be
iget-object v0, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;
iget-object v1, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
invoke-virtual {v1}, Landroid/support/v7/widget/ActionBarContextView;->isShown()Z
move-result v1
if-eqz v1, :cond_ae
iget-object v1, p0, Landroid/support/v7/a/l;->I:Landroid/graphics/Rect;
if-nez v1, :cond_33
new-instance v1, Landroid/graphics/Rect;
invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V
iput-object v1, p0, Landroid/support/v7/a/l;->I:Landroid/graphics/Rect;
new-instance v1, Landroid/graphics/Rect;
invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V
iput-object v1, p0, Landroid/support/v7/a/l;->J:Landroid/graphics/Rect;
:cond_33
iget-object v1, p0, Landroid/support/v7/a/l;->I:Landroid/graphics/Rect;
iget-object v4, p0, Landroid/support/v7/a/l;->J:Landroid/graphics/Rect;
invoke-virtual {v1, v2, p1, v2, v2}, Landroid/graphics/Rect;->set(IIII)V
iget-object v5, p0, Landroid/support/v7/a/l;->v:Landroid/view/ViewGroup;
invoke-static {v5, v1, v4}, Landroid/support/v7/widget/au;->a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
iget v1, v4, Landroid/graphics/Rect;->top:I
if-nez v1, :cond_97
move v1, p1
:goto_44
iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I
if-eq v4, v1, :cond_bc
iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I
iget-object v1, p0, Landroid/support/v7/a/l;->x:Landroid/view/View;
if-nez v1, :cond_99
new-instance v1, Landroid/view/View;
iget-object v4, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
invoke-direct {v1, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V
iput-object v1, p0, Landroid/support/v7/a/l;->x:Landroid/view/View;
iget-object v1, p0, Landroid/support/v7/a/l;->x:Landroid/view/View;
iget-object v4, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v4
sget v5, Landroid/support/v7/b/a$c;->abc_input_method_navigation_guard:I
invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I
move-result v4
invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V
iget-object v1, p0, Landroid/support/v7/a/l;->v:Landroid/view/ViewGroup;
iget-object v4, p0, Landroid/support/v7/a/l;->x:Landroid/view/View;
new-instance v5, Landroid/view/ViewGroup$LayoutParams;
invoke-direct {v5, v6, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V
invoke-virtual {v1, v4, v6, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
move v1, v3
:goto_75
iget-object v4, p0, Landroid/support/v7/a/l;->x:Landroid/view/View;
if-eqz v4, :cond_ac
:goto_79
iget-boolean v4, p0, Landroid/support/v7/a/l;->j:Z
if-nez v4, :cond_80
if-eqz v3, :cond_80
move p1, v2
:cond_80
move v7, v1
move v1, v3
move v3, v7
:goto_83
if-eqz v3, :cond_8a
iget-object v3, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
invoke-virtual {v3, v0}, Landroid/support/v7/widget/ActionBarContextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
:cond_8a
move v0, v1
:goto_8b
iget-object v1, p0, Landroid/support/v7/a/l;->x:Landroid/view/View;
if-eqz v1, :cond_96
iget-object v1, p0, Landroid/support/v7/a/l;->x:Landroid/view/View;
if-eqz v0, :cond_b6
:goto_93
invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V
:cond_96
return p1
:cond_97
move v1, v2
goto :goto_44
:cond_99
iget-object v1, p0, Landroid/support/v7/a/l;->x:Landroid/view/View;
invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v1
iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I
if-eq v4, p1, :cond_aa
iput p1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I
iget-object v4, p0, Landroid/support/v7/a/l;->x:Landroid/view/View;
invoke-virtual {v4, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
:cond_aa
move v1, v3
goto :goto_75
:cond_ac
move v3, v2
goto :goto_79
:cond_ae
iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I
if-eqz v1, :cond_b9
iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I
move v1, v2
goto :goto_83
:cond_b6
const/16 v2, 0x8
goto :goto_93
:cond_b9
move v3, v2
move v1, v2
goto :goto_83
:cond_bc
move v1, v2
goto :goto_75
:cond_be
move v0, v2
goto :goto_8b
.end method
.method private h(I)I
.registers 4
const/16 v0, 0x8
if-ne p1, v0, :cond_e
const-string v0, "AppCompatDelegate"
const-string v1, "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature."
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
const/16 p1, 0x6c
:goto_d
:cond_d
return p1
:cond_e
const/16 v0, 0x9
if-ne p1, v0, :cond_d
const-string v0, "AppCompatDelegate"
const-string v1, "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature."
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
const/16 p1, 0x6d
goto :goto_d
.end method
.method private s()V
.registers 4
const/4 v2, 0x0
iget-boolean v0, p0, Landroid/support/v7/a/l;->u:Z
if-nez v0, :cond_38
invoke-direct {p0}, Landroid/support/v7/a/l;->t()Landroid/view/ViewGroup;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/a/l;->v:Landroid/view/ViewGroup;
invoke-virtual {p0}, Landroid/support/v7/a/l;->q()Ljava/lang/CharSequence;
move-result-object v0
invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v1
if-nez v1, :cond_18
invoke-virtual {p0, v0}, Landroid/support/v7/a/l;->b(Ljava/lang/CharSequence;)V
:cond_18
invoke-direct {p0}, Landroid/support/v7/a/l;->u()V
iget-object v0, p0, Landroid/support/v7/a/l;->v:Landroid/view/ViewGroup;
invoke-virtual {p0, v0}, Landroid/support/v7/a/l;->a(Landroid/view/ViewGroup;)V
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v7/a/l;->u:Z
invoke-direct {p0, v2, v2}, Landroid/support/v7/a/l;->a(IZ)Landroid/support/v7/a/l$d;
move-result-object v0
invoke-virtual {p0}, Landroid/support/v7/a/l;->o()Z
move-result v1
if-nez v1, :cond_38
if-eqz v0, :cond_33
iget-object v0, v0, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
if-nez v0, :cond_38
:cond_33
const/16 v0, 0x6c
invoke-direct {p0, v0}, Landroid/support/v7/a/l;->e(I)V
:cond_38
return-void
.end method
.method private t()Landroid/view/ViewGroup;
.registers 9
const v7, 0x1020002
const/16 v6, 0x6d
const/4 v4, 0x1
const/4 v3, 0x0
const/4 v5, 0x0
iget-object v0, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
sget-object v1, Landroid/support/v7/b/a$k;->AppCompatTheme:[I
invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;
move-result-object v0
sget v1, Landroid/support/v7/b/a$k;->AppCompatTheme_windowActionBar:I
invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v1
if-nez v1, :cond_23
invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "You need to use a Theme.AppCompat theme (or descendant) with this activity."
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_23
sget v1, Landroid/support/v7/b/a$k;->AppCompatTheme_windowNoTitle:I
invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z
move-result v1
if-eqz v1, :cond_c4
invoke-virtual {p0, v4}, Landroid/support/v7/a/l;->c(I)Z
:goto_2e
:cond_2e
sget v1, Landroid/support/v7/b/a$k;->AppCompatTheme_windowActionBarOverlay:I
invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z
move-result v1
if-eqz v1, :cond_39
invoke-virtual {p0, v6}, Landroid/support/v7/a/l;->c(I)Z
:cond_39
sget v1, Landroid/support/v7/b/a$k;->AppCompatTheme_windowActionModeOverlay:I
invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z
move-result v1
if-eqz v1, :cond_46
const/16 v1, 0xa
invoke-virtual {p0, v1}, Landroid/support/v7/a/l;->c(I)Z
:cond_46
sget v1, Landroid/support/v7/b/a$k;->AppCompatTheme_android_windowIsFloating:I
invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z
move-result v1
iput-boolean v1, p0, Landroid/support/v7/a/l;->k:Z
invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
iget-object v0, p0, Landroid/support/v7/a/l;->b:Landroid/view/Window;
invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
iget-object v0, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v0
iget-boolean v1, p0, Landroid/support/v7/a/l;->l:Z
if-nez v1, :cond_136
iget-boolean v1, p0, Landroid/support/v7/a/l;->k:Z
if-eqz v1, :cond_d3
sget v1, Landroid/support/v7/b/a$h;->abc_dialog_title_material:I
invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup;
iput-boolean v5, p0, Landroid/support/v7/a/l;->i:Z
iput-boolean v5, p0, Landroid/support/v7/a/l;->h:Z
move-object v2, v0
:goto_71
if-nez v2, :cond_16c
new-instance v0, Ljava/lang/IllegalArgumentException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "AppCompat does not support the current theme features: { windowActionBar: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-boolean v2, p0, Landroid/support/v7/a/l;->h:Z
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, ", windowActionBarOverlay: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-boolean v2, p0, Landroid/support/v7/a/l;->i:Z
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, ", android:windowIsFloating: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-boolean v2, p0, Landroid/support/v7/a/l;->k:Z
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, ", windowActionModeOverlay: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-boolean v2, p0, Landroid/support/v7/a/l;->j:Z
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, ", windowNoTitle: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-boolean v2, p0, Landroid/support/v7/a/l;->l:Z
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, " }"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_c4
sget v1, Landroid/support/v7/b/a$k;->AppCompatTheme_windowActionBar:I
invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z
move-result v1
if-eqz v1, :cond_2e
const/16 v1, 0x6c
invoke-virtual {p0, v1}, Landroid/support/v7/a/l;->c(I)Z
goto/16 :goto_2e
:cond_d3
iget-boolean v0, p0, Landroid/support/v7/a/l;->h:Z
if-eqz v0, :cond_1be
new-instance v1, Landroid/util/TypedValue;
invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V
iget-object v0, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v0
sget v2, Landroid/support/v7/b/a$a;->actionBarTheme:I
invoke-virtual {v0, v2, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z
iget v0, v1, Landroid/util/TypedValue;->resourceId:I
if-eqz v0, :cond_133
new-instance v0, Landroid/support/v7/view/d;
iget-object v2, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
iget v1, v1, Landroid/util/TypedValue;->resourceId:I
invoke-direct {v0, v2, v1}, Landroid/support/v7/view/d;-><init>(Landroid/content/Context;I)V
:goto_f4
invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v0
sget v1, Landroid/support/v7/b/a$h;->abc_screen_toolbar:I
invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup;
sget v1, Landroid/support/v7/b/a$f;->decor_content_parent:I
invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v1
check-cast v1, Landroid/support/v7/widget/ab;
iput-object v1, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
iget-object v1, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-virtual {p0}, Landroid/support/v7/a/l;->p()Landroid/view/Window$Callback;
move-result-object v2
invoke-interface {v1, v2}, Landroid/support/v7/widget/ab;->setWindowCallback(Landroid/view/Window$Callback;)V
iget-boolean v1, p0, Landroid/support/v7/a/l;->i:Z
if-eqz v1, :cond_11c
iget-object v1, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v1, v6}, Landroid/support/v7/widget/ab;->a(I)V
:cond_11c
iget-boolean v1, p0, Landroid/support/v7/a/l;->y:Z
if-eqz v1, :cond_126
iget-object v1, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
const/4 v2, 0x2
invoke-interface {v1, v2}, Landroid/support/v7/widget/ab;->a(I)V
:cond_126
iget-boolean v1, p0, Landroid/support/v7/a/l;->z:Z
if-eqz v1, :cond_130
iget-object v1, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
const/4 v2, 0x5
invoke-interface {v1, v2}, Landroid/support/v7/widget/ab;->a(I)V
:cond_130
move-object v2, v0
goto/16 :goto_71
:cond_133
iget-object v0, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
goto :goto_f4
:cond_136
iget-boolean v1, p0, Landroid/support/v7/a/l;->j:Z
if-eqz v1, :cond_154
sget v1, Landroid/support/v7/b/a$h;->abc_screen_simple_overlay_action_mode:I
invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup;
move-object v1, v0
:goto_143
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v2, 0x15
if-lt v0, v2, :cond_15e
new-instance v0, Landroid/support/v7/a/l$2;
invoke-direct {v0, p0}, Landroid/support/v7/a/l$2;-><init>(Landroid/support/v7/a/l;)V
invoke-static {v1, v0}, Landroid/support/v4/f/af;->a(Landroid/view/View;Landroid/support/v4/f/aa;)V
move-object v2, v1
goto/16 :goto_71
:cond_154
sget v1, Landroid/support/v7/b/a$h;->abc_screen_simple:I
invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup;
move-object v1, v0
goto :goto_143
:cond_15e
move-object v0, v1
check-cast v0, Landroid/support/v7/widget/ae;
new-instance v2, Landroid/support/v7/a/l$3;
invoke-direct {v2, p0}, Landroid/support/v7/a/l$3;-><init>(Landroid/support/v7/a/l;)V
invoke-interface {v0, v2}, Landroid/support/v7/widget/ae;->setOnFitSystemWindowsListener(Landroid/support/v7/widget/ae$a;)V
move-object v2, v1
goto/16 :goto_71
:cond_16c
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
if-nez v0, :cond_17a
sget v0, Landroid/support/v7/b/a$f;->title:I
invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/TextView;
iput-object v0, p0, Landroid/support/v7/a/l;->w:Landroid/widget/TextView;
:cond_17a
invoke-static {v2}, Landroid/support/v7/widget/au;->b(Landroid/view/View;)V
sget v0, Landroid/support/v7/b/a$f;->action_bar_activity_content:I
invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ContentFrameLayout;
iget-object v1, p0, Landroid/support/v7/a/l;->b:Landroid/view/Window;
invoke-virtual {v1, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;
move-result-object v1
check-cast v1, Landroid/view/ViewGroup;
if-eqz v1, :cond_1b0
:goto_18f
invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I
move-result v4
if-lez v4, :cond_1a0
invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;
move-result-object v4
invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->removeViewAt(I)V
invoke-virtual {v0, v4}, Landroid/support/v7/widget/ContentFrameLayout;->addView(Landroid/view/View;)V
goto :goto_18f
:cond_1a0
const/4 v4, -0x1
invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setId(I)V
invoke-virtual {v0, v7}, Landroid/support/v7/widget/ContentFrameLayout;->setId(I)V
instance-of v4, v1, Landroid/widget/FrameLayout;
if-eqz v4, :cond_1b0
check-cast v1, Landroid/widget/FrameLayout;
invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V
:cond_1b0
iget-object v1, p0, Landroid/support/v7/a/l;->b:Landroid/view/Window;
invoke-virtual {v1, v2}, Landroid/view/Window;->setContentView(Landroid/view/View;)V
new-instance v1, Landroid/support/v7/a/l$4;
invoke-direct {v1, p0}, Landroid/support/v7/a/l$4;-><init>(Landroid/support/v7/a/l;)V
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ContentFrameLayout;->setAttachListener(Landroid/support/v7/widget/ContentFrameLayout$a;)V
return-object v2
:cond_1be
move-object v2, v3
goto/16 :goto_71
.end method
.method private u()V
.registers 6
iget-object v0, p0, Landroid/support/v7/a/l;->v:Landroid/view/ViewGroup;
const v1, 0x1020002
invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ContentFrameLayout;
iget-object v1, p0, Landroid/support/v7/a/l;->b:Landroid/view/Window;
invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;
move-result-object v1
invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I
move-result v2
invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I
move-result v3
invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I
move-result v4
invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I
move-result v1
invoke-virtual {v0, v2, v3, v4, v1}, Landroid/support/v7/widget/ContentFrameLayout;->a(IIII)V
iget-object v1, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
sget-object v2, Landroid/support/v7/b/a$k;->AppCompatTheme:[I
invoke-virtual {v1, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;
move-result-object v1
sget v2, Landroid/support/v7/b/a$k;->AppCompatTheme_windowMinWidthMajor:I
invoke-virtual {v0}, Landroid/support/v7/widget/ContentFrameLayout;->getMinWidthMajor()Landroid/util/TypedValue;
move-result-object v3
invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z
sget v2, Landroid/support/v7/b/a$k;->AppCompatTheme_windowMinWidthMinor:I
invoke-virtual {v0}, Landroid/support/v7/widget/ContentFrameLayout;->getMinWidthMinor()Landroid/util/TypedValue;
move-result-object v3
invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z
sget v2, Landroid/support/v7/b/a$k;->AppCompatTheme_windowFixedWidthMajor:I
invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v2
if-eqz v2, :cond_4f
sget v2, Landroid/support/v7/b/a$k;->AppCompatTheme_windowFixedWidthMajor:I
invoke-virtual {v0}, Landroid/support/v7/widget/ContentFrameLayout;->getFixedWidthMajor()Landroid/util/TypedValue;
move-result-object v3
invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z
:cond_4f
sget v2, Landroid/support/v7/b/a$k;->AppCompatTheme_windowFixedWidthMinor:I
invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v2
if-eqz v2, :cond_60
sget v2, Landroid/support/v7/b/a$k;->AppCompatTheme_windowFixedWidthMinor:I
invoke-virtual {v0}, Landroid/support/v7/widget/ContentFrameLayout;->getFixedWidthMinor()Landroid/util/TypedValue;
move-result-object v3
invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z
:cond_60
sget v2, Landroid/support/v7/b/a$k;->AppCompatTheme_windowFixedHeightMajor:I
invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v2
if-eqz v2, :cond_71
sget v2, Landroid/support/v7/b/a$k;->AppCompatTheme_windowFixedHeightMajor:I
invoke-virtual {v0}, Landroid/support/v7/widget/ContentFrameLayout;->getFixedHeightMajor()Landroid/util/TypedValue;
move-result-object v3
invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z
:cond_71
sget v2, Landroid/support/v7/b/a$k;->AppCompatTheme_windowFixedHeightMinor:I
invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v2
if-eqz v2, :cond_82
sget v2, Landroid/support/v7/b/a$k;->AppCompatTheme_windowFixedHeightMinor:I
invoke-virtual {v0}, Landroid/support/v7/widget/ContentFrameLayout;->getFixedHeightMinor()Landroid/util/TypedValue;
move-result-object v3
invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z
:cond_82
invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V
invoke-virtual {v0}, Landroid/support/v7/widget/ContentFrameLayout;->requestLayout()V
return-void
.end method
.method private v()V
.registers 2
iget-object v0, p0, Landroid/support/v7/a/l;->q:Landroid/support/v4/f/au;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/a/l;->q:Landroid/support/v4/f/au;
invoke-virtual {v0}, Landroid/support/v4/f/au;->b()V
:cond_9
return-void
.end method
.method private w()V
.registers 3
iget-boolean v0, p0, Landroid/support/v7/a/l;->u:Z
if-eqz v0, :cond_c
new-instance v0, Landroid/util/AndroidRuntimeException;
const-string v1, "Window feature must be requested before adding content"
invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V
throw v0
:cond_c
return-void
.end method
.method private x()V
.registers 4
const/4 v2, 0x0
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v0}, Landroid/support/v7/widget/ab;->j()V
:cond_a
iget-object v0, p0, Landroid/support/v7/a/l;->o:Landroid/widget/PopupWindow;
if-eqz v0, :cond_29
iget-object v0, p0, Landroid/support/v7/a/l;->b:Landroid/view/Window;
invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/a/l;->p:Ljava/lang/Runnable;
invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z
iget-object v0, p0, Landroid/support/v7/a/l;->o:Landroid/widget/PopupWindow;
invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z
move-result v0
if-eqz v0, :cond_26
:try_start_21
iget-object v0, p0, Landroid/support/v7/a/l;->o:Landroid/widget/PopupWindow;
invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V
:goto_26
:cond_26
:try_end_26
.catch Ljava/lang/IllegalArgumentException; {:try_start_21 .. :try_end_26} :catch_3c
const/4 v0, 0x0
iput-object v0, p0, Landroid/support/v7/a/l;->o:Landroid/widget/PopupWindow;
:cond_29
invoke-direct {p0}, Landroid/support/v7/a/l;->v()V
invoke-direct {p0, v2, v2}, Landroid/support/v7/a/l;->a(IZ)Landroid/support/v7/a/l$d;
move-result-object v0
if-eqz v0, :cond_3b
iget-object v1, v0, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
if-eqz v1, :cond_3b
iget-object v0, v0, Landroid/support/v7/a/l$d;->j:Landroid/support/v7/view/menu/f;
invoke-virtual {v0}, Landroid/support/v7/view/menu/f;->close()V
:cond_3b
return-void
:catch_3c
move-exception v0
goto :goto_26
.end method
.method  a(Landroid/support/v7/view/b$a;)Landroid/support/v7/view/b;
.registers 10
const/4 v2, 0x0
const/4 v3, 0x0
const/4 v1, 0x1
invoke-direct {p0}, Landroid/support/v7/a/l;->v()V
iget-object v0, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
if-eqz v0, :cond_f
iget-object v0, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
invoke-virtual {v0}, Landroid/support/v7/view/b;->c()V
:cond_f
new-instance v4, Landroid/support/v7/a/l$b;
invoke-direct {v4, p0, p1}, Landroid/support/v7/a/l$b;-><init>(Landroid/support/v7/a/l;Landroid/support/v7/view/b$a;)V
iget-object v0, p0, Landroid/support/v7/a/l;->e:Landroid/support/v7/a/f;
if-eqz v0, :cond_151
invoke-virtual {p0}, Landroid/support/v7/a/l;->o()Z
move-result v0
if-nez v0, :cond_151
:try_start_1e
iget-object v0, p0, Landroid/support/v7/a/l;->e:Landroid/support/v7/a/f;
invoke-interface {v0, v4}, Landroid/support/v7/a/f;->a(Landroid/support/v7/view/b$a;)Landroid/support/v7/view/b;
:try_end_23
.catch Ljava/lang/AbstractMethodError; {:try_start_1e .. :try_end_23} :catch_3a
move-result-object v0
:goto_24
if-eqz v0, :cond_3d
iput-object v0, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
:cond_28
:goto_28
iget-object v0, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
if-eqz v0, :cond_37
iget-object v0, p0, Landroid/support/v7/a/l;->e:Landroid/support/v7/a/f;
if-eqz v0, :cond_37
iget-object v0, p0, Landroid/support/v7/a/l;->e:Landroid/support/v7/a/f;
iget-object v1, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
invoke-interface {v0, v1}, Landroid/support/v7/a/f;->a(Landroid/support/v7/view/b;)V
:cond_37
iget-object v0, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
return-object v0
:catch_3a
move-exception v0
move-object v0, v3
goto :goto_24
:cond_3d
iget-object v0, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
if-nez v0, :cond_c5
iget-boolean v0, p0, Landroid/support/v7/a/l;->k:Z
if-eqz v0, :cond_12a
new-instance v5, Landroid/util/TypedValue;
invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V
iget-object v0, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v0
sget v6, Landroid/support/v7/b/a$a;->actionBarTheme:I
invoke-virtual {v0, v6, v5, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z
iget v6, v5, Landroid/util/TypedValue;->resourceId:I
if-eqz v6, :cond_126
iget-object v6, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v6
invoke-virtual {v6}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;
move-result-object v6
invoke-virtual {v6, v0}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V
iget v0, v5, Landroid/util/TypedValue;->resourceId:I
invoke-virtual {v6, v0, v1}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V
new-instance v0, Landroid/support/v7/view/d;
iget-object v7, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
invoke-direct {v0, v7, v2}, Landroid/support/v7/view/d;-><init>(Landroid/content/Context;I)V
invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v7
invoke-virtual {v7, v6}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V
:goto_79
new-instance v6, Landroid/support/v7/widget/ActionBarContextView;
invoke-direct {v6, v0}, Landroid/support/v7/widget/ActionBarContextView;-><init>(Landroid/content/Context;)V
iput-object v6, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
new-instance v6, Landroid/widget/PopupWindow;
sget v7, Landroid/support/v7/b/a$a;->actionModePopupWindowStyle:I
invoke-direct {v6, v0, v3, v7}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
iput-object v6, p0, Landroid/support/v7/a/l;->o:Landroid/widget/PopupWindow;
iget-object v6, p0, Landroid/support/v7/a/l;->o:Landroid/widget/PopupWindow;
const/4 v7, 0x2
invoke-static {v6, v7}, Landroid/support/v4/widget/o;->a(Landroid/widget/PopupWindow;I)V
iget-object v6, p0, Landroid/support/v7/a/l;->o:Landroid/widget/PopupWindow;
iget-object v7, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
invoke-virtual {v6, v7}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V
iget-object v6, p0, Landroid/support/v7/a/l;->o:Landroid/widget/PopupWindow;
const/4 v7, -0x1
invoke-virtual {v6, v7}, Landroid/widget/PopupWindow;->setWidth(I)V
invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v6
sget v7, Landroid/support/v7/b/a$a;->actionBarSize:I
invoke-virtual {v6, v7, v5, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z
iget v5, v5, Landroid/util/TypedValue;->data:I
invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
move-result-object v0
invoke-static {v5, v0}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I
move-result v0
iget-object v5, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
invoke-virtual {v5, v0}, Landroid/support/v7/widget/ActionBarContextView;->setContentHeight(I)V
iget-object v0, p0, Landroid/support/v7/a/l;->o:Landroid/widget/PopupWindow;
const/4 v5, -0x2
invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setHeight(I)V
new-instance v0, Landroid/support/v7/a/l$5;
invoke-direct {v0, p0}, Landroid/support/v7/a/l$5;-><init>(Landroid/support/v7/a/l;)V
iput-object v0, p0, Landroid/support/v7/a/l;->p:Ljava/lang/Runnable;
:goto_c5
:cond_c5
iget-object v0, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
if-eqz v0, :cond_28
invoke-direct {p0}, Landroid/support/v7/a/l;->v()V
iget-object v0, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->c()V
new-instance v5, Landroid/support/v7/view/e;
iget-object v0, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContextView;->getContext()Landroid/content/Context;
move-result-object v6
iget-object v7, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
iget-object v0, p0, Landroid/support/v7/a/l;->o:Landroid/widget/PopupWindow;
if-nez v0, :cond_14b
move v0, v1
:goto_e0
invoke-direct {v5, v6, v7, v4, v0}, Landroid/support/v7/view/e;-><init>(Landroid/content/Context;Landroid/support/v7/widget/ActionBarContextView;Landroid/support/v7/view/b$a;Z)V
invoke-virtual {v5}, Landroid/support/v7/view/b;->b()Landroid/view/Menu;
move-result-object v0
invoke-interface {p1, v5, v0}, Landroid/support/v7/view/b$a;->a(Landroid/support/v7/view/b;Landroid/view/Menu;)Z
move-result v0
if-eqz v0, :cond_14d
invoke-virtual {v5}, Landroid/support/v7/view/b;->d()V
iget-object v0, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
invoke-virtual {v0, v5}, Landroid/support/v7/widget/ActionBarContextView;->a(Landroid/support/v7/view/b;)V
iput-object v5, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
iget-object v0, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
const/4 v1, 0x0
invoke-static {v0, v1}, Landroid/support/v4/f/af;->b(Landroid/view/View;F)V
iget-object v0, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
invoke-static {v0}, Landroid/support/v4/f/af;->k(Landroid/view/View;)Landroid/support/v4/f/au;
move-result-object v0
const/high16 v1, 0x3f80
invoke-virtual {v0, v1}, Landroid/support/v4/f/au;->a(F)Landroid/support/v4/f/au;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/a/l;->q:Landroid/support/v4/f/au;
iget-object v0, p0, Landroid/support/v7/a/l;->q:Landroid/support/v4/f/au;
new-instance v1, Landroid/support/v7/a/l$6;
invoke-direct {v1, p0}, Landroid/support/v7/a/l$6;-><init>(Landroid/support/v7/a/l;)V
invoke-virtual {v0, v1}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/ay;)Landroid/support/v4/f/au;
iget-object v0, p0, Landroid/support/v7/a/l;->o:Landroid/widget/PopupWindow;
if-eqz v0, :cond_28
iget-object v0, p0, Landroid/support/v7/a/l;->b:Landroid/view/Window;
invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/a/l;->p:Ljava/lang/Runnable;
invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z
goto/16 :goto_28
:cond_126
iget-object v0, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
goto/16 :goto_79
:cond_12a
iget-object v0, p0, Landroid/support/v7/a/l;->v:Landroid/view/ViewGroup;
sget v5, Landroid/support/v7/b/a$f;->action_mode_bar_stub:I
invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ViewStubCompat;
if-eqz v0, :cond_c5
invoke-virtual {p0}, Landroid/support/v7/a/l;->m()Landroid/content/Context;
move-result-object v5
invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v5
invoke-virtual {v0, v5}, Landroid/support/v7/widget/ViewStubCompat;->setLayoutInflater(Landroid/view/LayoutInflater;)V
invoke-virtual {v0}, Landroid/support/v7/widget/ViewStubCompat;->a()Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionBarContextView;
iput-object v0, p0, Landroid/support/v7/a/l;->n:Landroid/support/v7/widget/ActionBarContextView;
goto/16 :goto_c5
:cond_14b
move v0, v2
goto :goto_e0
:cond_14d
iput-object v3, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
goto/16 :goto_28
:cond_151
move-object v0, v3
goto/16 :goto_24
.end method
.method public a(I)Landroid/view/View;
.registers 3
invoke-direct {p0}, Landroid/support/v7/a/l;->s()V
iget-object v0, p0, Landroid/support/v7/a/l;->b:Landroid/view/Window;
invoke-virtual {v0, p1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;
move-result-object v0
return-object v0
.end method
.method public final a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
.registers 6
invoke-virtual {p0, p1, p2, p3, p4}, Landroid/support/v7/a/l;->b(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
move-result-object v0
if-eqz v0, :cond_7
:goto_6
return-object v0
:cond_7
invoke-virtual {p0, p1, p2, p3, p4}, Landroid/support/v7/a/l;->c(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
move-result-object v0
goto :goto_6
.end method
.method  a(ILandroid/view/Menu;)V
.registers 6
const/4 v2, 0x0
const/16 v0, 0x6c
if-ne p1, v0, :cond_f
invoke-virtual {p0}, Landroid/support/v7/a/l;->a()Landroid/support/v7/a/a;
move-result-object v0
if-eqz v0, :cond_e
invoke-virtual {v0, v2}, Landroid/support/v7/a/a;->e(Z)V
:cond_e
:goto_e
return-void
:cond_f
if-nez p1, :cond_e
const/4 v0, 0x1
invoke-direct {p0, p1, v0}, Landroid/support/v7/a/l;->a(IZ)Landroid/support/v7/a/l$d;
move-result-object v0
iget-boolean v1, v0, Landroid/support/v7/a/l$d;->o:Z
if-eqz v1, :cond_e
invoke-direct {p0, v0, v2}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;Z)V
goto :goto_e
.end method
.method public a(Landroid/content/res/Configuration;)V
.registers 3
iget-boolean v0, p0, Landroid/support/v7/a/l;->h:Z
if-eqz v0, :cond_11
iget-boolean v0, p0, Landroid/support/v7/a/l;->u:Z
if-eqz v0, :cond_11
invoke-virtual {p0}, Landroid/support/v7/a/l;->a()Landroid/support/v7/a/a;
move-result-object v0
if-eqz v0, :cond_11
invoke-virtual {v0, p1}, Landroid/support/v7/a/a;->a(Landroid/content/res/Configuration;)V
:cond_11
invoke-virtual {p0}, Landroid/support/v7/a/l;->h()Z
return-void
.end method
.method public a(Landroid/os/Bundle;)V
.registers 4
const/4 v1, 0x1
iget-object v0, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
instance-of v0, v0, Landroid/app/Activity;
if-eqz v0, :cond_19
iget-object v0, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
check-cast v0, Landroid/app/Activity;
invoke-static {v0}, Landroid/support/v4/app/v;->b(Landroid/app/Activity;)Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_19
invoke-virtual {p0}, Landroid/support/v7/a/l;->l()Landroid/support/v7/a/a;
move-result-object v0
if-nez v0, :cond_1a
iput-boolean v1, p0, Landroid/support/v7/a/l;->H:Z
:goto_19
:cond_19
return-void
:cond_1a
invoke-virtual {v0, v1}, Landroid/support/v7/a/a;->c(Z)V
goto :goto_19
.end method
.method public a(Landroid/support/v7/view/menu/f;)V
.registers 3
const/4 v0, 0x1
invoke-direct {p0, p1, v0}, Landroid/support/v7/a/l;->a(Landroid/support/v7/view/menu/f;Z)V
return-void
.end method
.method public a(Landroid/view/View;)V
.registers 4
invoke-direct {p0}, Landroid/support/v7/a/l;->s()V
iget-object v0, p0, Landroid/support/v7/a/l;->v:Landroid/view/ViewGroup;
const v1, 0x1020002
invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup;
invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V
invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V
iget-object v0, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V
return-void
.end method
.method public a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.registers 5
invoke-direct {p0}, Landroid/support/v7/a/l;->s()V
iget-object v0, p0, Landroid/support/v7/a/l;->v:Landroid/view/ViewGroup;
const v1, 0x1020002
invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup;
invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V
invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
iget-object v0, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V
return-void
.end method
.method  a(Landroid/view/ViewGroup;)V
.registers 2
return-void
.end method
.method  a(ILandroid/view/KeyEvent;)Z
.registers 7
const/4 v1, 0x0
const/4 v0, 0x1
invoke-virtual {p0}, Landroid/support/v7/a/l;->a()Landroid/support/v7/a/a;
move-result-object v2
if-eqz v2, :cond_f
invoke-virtual {v2, p1, p2}, Landroid/support/v7/a/a;->a(ILandroid/view/KeyEvent;)Z
move-result v2
if-eqz v2, :cond_f
:cond_e
:goto_e
return v0
:cond_f
iget-object v2, p0, Landroid/support/v7/a/l;->C:Landroid/support/v7/a/l$d;
if-eqz v2, :cond_28
iget-object v2, p0, Landroid/support/v7/a/l;->C:Landroid/support/v7/a/l$d;
invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I
move-result v3
invoke-direct {p0, v2, v3, p2, v0}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;ILandroid/view/KeyEvent;I)Z
move-result v2
if-eqz v2, :cond_28
iget-object v1, p0, Landroid/support/v7/a/l;->C:Landroid/support/v7/a/l$d;
if-eqz v1, :cond_e
iget-object v1, p0, Landroid/support/v7/a/l;->C:Landroid/support/v7/a/l$d;
iput-boolean v0, v1, Landroid/support/v7/a/l$d;->n:Z
goto :goto_e
:cond_28
iget-object v2, p0, Landroid/support/v7/a/l;->C:Landroid/support/v7/a/l$d;
if-nez v2, :cond_3f
invoke-direct {p0, v1, v0}, Landroid/support/v7/a/l;->a(IZ)Landroid/support/v7/a/l$d;
move-result-object v2
invoke-direct {p0, v2, p2}, Landroid/support/v7/a/l;->b(Landroid/support/v7/a/l$d;Landroid/view/KeyEvent;)Z
invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I
move-result v3
invoke-direct {p0, v2, v3, p2, v0}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;ILandroid/view/KeyEvent;I)Z
move-result v3
iput-boolean v1, v2, Landroid/support/v7/a/l$d;->m:Z
if-nez v3, :cond_e
:cond_3f
move v0, v1
goto :goto_e
.end method
.method public a(Landroid/support/v7/view/menu/f;Landroid/view/MenuItem;)Z
.registers 5
invoke-virtual {p0}, Landroid/support/v7/a/l;->p()Landroid/view/Window$Callback;
move-result-object v0
if-eqz v0, :cond_1d
invoke-virtual {p0}, Landroid/support/v7/a/l;->o()Z
move-result v1
if-nez v1, :cond_1d
invoke-virtual {p1}, Landroid/support/v7/view/menu/f;->p()Landroid/support/v7/view/menu/f;
move-result-object v1
invoke-direct {p0, v1}, Landroid/support/v7/a/l;->a(Landroid/view/Menu;)Landroid/support/v7/a/l$d;
move-result-object v1
if-eqz v1, :cond_1d
iget v1, v1, Landroid/support/v7/a/l$d;->a:I
invoke-interface {v0, v1, p2}, Landroid/view/Window$Callback;->onMenuItemSelected(ILandroid/view/MenuItem;)Z
move-result v0
:goto_1c
return v0
:cond_1d
const/4 v0, 0x0
goto :goto_1c
.end method
.method  a(Landroid/view/KeyEvent;)Z
.registers 5
const/4 v0, 0x1
invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
move-result v1
const/16 v2, 0x52
if-ne v1, v2, :cond_12
iget-object v1, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
invoke-interface {v1, p1}, Landroid/view/Window$Callback;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z
move-result v1
if-eqz v1, :cond_12
:goto_11
return v0
:cond_12
invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
move-result v1
invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
move-result v2
if-nez v2, :cond_23
:goto_1c
if-eqz v0, :cond_25
invoke-virtual {p0, v1, p1}, Landroid/support/v7/a/l;->c(ILandroid/view/KeyEvent;)Z
move-result v0
goto :goto_11
:cond_23
const/4 v0, 0x0
goto :goto_1c
:cond_25
invoke-virtual {p0, v1, p1}, Landroid/support/v7/a/l;->b(ILandroid/view/KeyEvent;)Z
move-result v0
goto :goto_11
.end method
.method public b(Landroid/support/v7/view/b$a;)Landroid/support/v7/view/b;
.registers 5
if-nez p1, :cond_a
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "ActionMode callback can not be null."
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_a
iget-object v0, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
if-eqz v0, :cond_13
iget-object v0, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
invoke-virtual {v0}, Landroid/support/v7/view/b;->c()V
:cond_13
new-instance v0, Landroid/support/v7/a/l$b;
invoke-direct {v0, p0, p1}, Landroid/support/v7/a/l$b;-><init>(Landroid/support/v7/a/l;Landroid/support/v7/view/b$a;)V
invoke-virtual {p0}, Landroid/support/v7/a/l;->a()Landroid/support/v7/a/a;
move-result-object v1
if-eqz v1, :cond_33
invoke-virtual {v1, v0}, Landroid/support/v7/a/a;->a(Landroid/support/v7/view/b$a;)Landroid/support/v7/view/b;
move-result-object v1
iput-object v1, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
iget-object v1, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
if-eqz v1, :cond_33
iget-object v1, p0, Landroid/support/v7/a/l;->e:Landroid/support/v7/a/f;
if-eqz v1, :cond_33
iget-object v1, p0, Landroid/support/v7/a/l;->e:Landroid/support/v7/a/f;
iget-object v2, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
invoke-interface {v1, v2}, Landroid/support/v7/a/f;->a(Landroid/support/v7/view/b;)V
:cond_33
iget-object v1, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
if-nez v1, :cond_3d
invoke-virtual {p0, v0}, Landroid/support/v7/a/l;->a(Landroid/support/v7/view/b$a;)Landroid/support/v7/view/b;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
:cond_3d
iget-object v0, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
return-object v0
.end method
.method  b(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
.registers 6
iget-object v0, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
instance-of v0, v0, Landroid/view/LayoutInflater$Factory;
if-eqz v0, :cond_11
iget-object v0, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
check-cast v0, Landroid/view/LayoutInflater$Factory;
invoke-interface {v0, p2, p3, p4}, Landroid/view/LayoutInflater$Factory;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
move-result-object v0
if-eqz v0, :cond_11
:goto_10
return-object v0
:cond_11
const/4 v0, 0x0
goto :goto_10
.end method
.method public b(I)V
.registers 4
invoke-direct {p0}, Landroid/support/v7/a/l;->s()V
iget-object v0, p0, Landroid/support/v7/a/l;->v:Landroid/view/ViewGroup;
const v1, 0x1020002
invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup;
invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V
iget-object v1, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v1
invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
iget-object v0, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V
return-void
.end method
.method public b(Landroid/os/Bundle;)V
.registers 2
invoke-direct {p0}, Landroid/support/v7/a/l;->s()V
return-void
.end method
.method public b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.registers 5
invoke-direct {p0}, Landroid/support/v7/a/l;->s()V
iget-object v0, p0, Landroid/support/v7/a/l;->v:Landroid/view/ViewGroup;
const v1, 0x1020002
invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup;
invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
iget-object v0, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V
return-void
.end method
.method  b(Ljava/lang/CharSequence;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/v7/a/l;->r:Landroid/support/v7/widget/ab;
invoke-interface {v0, p1}, Landroid/support/v7/widget/ab;->setWindowTitle(Ljava/lang/CharSequence;)V
:cond_9
:goto_9
return-void
:cond_a
invoke-virtual {p0}, Landroid/support/v7/a/l;->l()Landroid/support/v7/a/a;
move-result-object v0
if-eqz v0, :cond_18
invoke-virtual {p0}, Landroid/support/v7/a/l;->l()Landroid/support/v7/a/a;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/support/v7/a/a;->a(Ljava/lang/CharSequence;)V
goto :goto_9
:cond_18
iget-object v0, p0, Landroid/support/v7/a/l;->w:Landroid/widget/TextView;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/a/l;->w:Landroid/widget/TextView;
invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
goto :goto_9
.end method
.method  b(ILandroid/view/KeyEvent;)Z
.registers 8
const/4 v0, 0x1
const/4 v1, 0x0
sparse-switch p1, :sswitch_data_26
:cond_5
move v0, v1
:goto_6
:cond_6
return v0
:sswitch_7
invoke-direct {p0, v1, p2}, Landroid/support/v7/a/l;->e(ILandroid/view/KeyEvent;)Z
goto :goto_6
:sswitch_b
iget-boolean v2, p0, Landroid/support/v7/a/l;->D:Z
iput-boolean v1, p0, Landroid/support/v7/a/l;->D:Z
invoke-direct {p0, v1, v1}, Landroid/support/v7/a/l;->a(IZ)Landroid/support/v7/a/l$d;
move-result-object v3
if-eqz v3, :cond_1f
iget-boolean v4, v3, Landroid/support/v7/a/l$d;->o:Z
if-eqz v4, :cond_1f
if-nez v2, :cond_6
invoke-direct {p0, v3, v0}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l$d;Z)V
goto :goto_6
:cond_1f
invoke-virtual {p0}, Landroid/support/v7/a/l;->r()Z
move-result v2
if-eqz v2, :cond_5
goto :goto_6
:sswitch_data_26
.sparse-switch
0x4 -> :sswitch_b
0x52 -> :sswitch_7
.end sparse-switch
.end method
.method  b(ILandroid/view/Menu;)Z
.registers 5
const/4 v0, 0x1
const/16 v1, 0x6c
if-ne p1, v1, :cond_f
invoke-virtual {p0}, Landroid/support/v7/a/l;->a()Landroid/support/v7/a/a;
move-result-object v1
if-eqz v1, :cond_e
invoke-virtual {v1, v0}, Landroid/support/v7/a/a;->e(Z)V
:cond_e
:goto_e
return v0
:cond_f
const/4 v0, 0x0
goto :goto_e
.end method
.method public c(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
.registers 14
const/4 v1, 0x0
const/4 v7, 0x1
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v2, 0x15
if-ge v0, v2, :cond_2f
move v6, v7
:goto_9
iget-object v0, p0, Landroid/support/v7/a/l;->K:Landroid/support/v7/a/n;
if-nez v0, :cond_14
new-instance v0, Landroid/support/v7/a/n;
invoke-direct {v0}, Landroid/support/v7/a/n;-><init>()V
iput-object v0, p0, Landroid/support/v7/a/l;->K:Landroid/support/v7/a/n;
:cond_14
if-eqz v6, :cond_31
move-object v0, p1
check-cast v0, Landroid/view/ViewParent;
invoke-direct {p0, v0}, Landroid/support/v7/a/l;->a(Landroid/view/ViewParent;)Z
move-result v0
if-eqz v0, :cond_31
move v5, v7
:goto_20
iget-object v0, p0, Landroid/support/v7/a/l;->K:Landroid/support/v7/a/n;
invoke-static {}, Landroid/support/v7/widget/at;->a()Z
move-result v8
move-object v1, p1
move-object v2, p2
move-object v3, p3
move-object v4, p4
invoke-virtual/range {v0 .. v8}, Landroid/support/v7/a/n;->a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;ZZZZ)Landroid/view/View;
move-result-object v0
return-object v0
:cond_2f
move v6, v1
goto :goto_9
:cond_31
move v5, v1
goto :goto_20
.end method
.method public c()V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/a/l;->a()Landroid/support/v7/a/a;
move-result-object v0
if-eqz v0, :cond_a
const/4 v1, 0x0
invoke-virtual {v0, v1}, Landroid/support/v7/a/a;->d(Z)V
:cond_a
return-void
.end method
.method public c(I)Z
.registers 6
const/4 v0, 0x0
const/4 v1, 0x1
invoke-direct {p0, p1}, Landroid/support/v7/a/l;->h(I)I
move-result v2
iget-boolean v3, p0, Landroid/support/v7/a/l;->l:Z
if-eqz v3, :cond_f
const/16 v3, 0x6c
if-ne v2, v3, :cond_f
:goto_e
return v0
:cond_f
iget-boolean v3, p0, Landroid/support/v7/a/l;->h:Z
if-eqz v3, :cond_17
if-ne v2, v1, :cond_17
iput-boolean v0, p0, Landroid/support/v7/a/l;->h:Z
:cond_17
sparse-switch v2, :sswitch_data_4c
iget-object v0, p0, Landroid/support/v7/a/l;->b:Landroid/view/Window;
invoke-virtual {v0, v2}, Landroid/view/Window;->requestFeature(I)Z
move-result v0
goto :goto_e
:sswitch_21
invoke-direct {p0}, Landroid/support/v7/a/l;->w()V
iput-boolean v1, p0, Landroid/support/v7/a/l;->h:Z
move v0, v1
goto :goto_e
:sswitch_28
invoke-direct {p0}, Landroid/support/v7/a/l;->w()V
iput-boolean v1, p0, Landroid/support/v7/a/l;->i:Z
move v0, v1
goto :goto_e
:sswitch_2f
invoke-direct {p0}, Landroid/support/v7/a/l;->w()V
iput-boolean v1, p0, Landroid/support/v7/a/l;->j:Z
move v0, v1
goto :goto_e
:sswitch_36
invoke-direct {p0}, Landroid/support/v7/a/l;->w()V
iput-boolean v1, p0, Landroid/support/v7/a/l;->y:Z
move v0, v1
goto :goto_e
:sswitch_3d
invoke-direct {p0}, Landroid/support/v7/a/l;->w()V
iput-boolean v1, p0, Landroid/support/v7/a/l;->z:Z
move v0, v1
goto :goto_e
:sswitch_44
invoke-direct {p0}, Landroid/support/v7/a/l;->w()V
iput-boolean v1, p0, Landroid/support/v7/a/l;->l:Z
move v0, v1
goto :goto_e
nop
:sswitch_data_4c
.sparse-switch
0x1 -> :sswitch_44
0x2 -> :sswitch_36
0x5 -> :sswitch_3d
0xa -> :sswitch_2f
0x6c -> :sswitch_21
0x6d -> :sswitch_28
.end sparse-switch
.end method
.method  c(ILandroid/view/KeyEvent;)Z
.registers 6
const/4 v0, 0x1
const/4 v1, 0x0
sparse-switch p1, :sswitch_data_22
:goto_5
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v2, 0xb
if-ge v0, v2, :cond_e
invoke-virtual {p0, p1, p2}, Landroid/support/v7/a/l;->a(ILandroid/view/KeyEvent;)Z
:cond_e
move v0, v1
:goto_f
return v0
:sswitch_10
invoke-direct {p0, v1, p2}, Landroid/support/v7/a/l;->d(ILandroid/view/KeyEvent;)Z
goto :goto_f
:sswitch_14
invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I
move-result v2
and-int/lit16 v2, v2, 0x80
if-eqz v2, :cond_1f
:goto_1c
iput-boolean v0, p0, Landroid/support/v7/a/l;->D:Z
goto :goto_5
:cond_1f
move v0, v1
goto :goto_1c
nop
:sswitch_data_22
.sparse-switch
0x4 -> :sswitch_14
0x52 -> :sswitch_10
.end sparse-switch
.end method
.method public d()V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/a/l;->a()Landroid/support/v7/a/a;
move-result-object v0
if-eqz v0, :cond_a
const/4 v1, 0x1
invoke-virtual {v0, v1}, Landroid/support/v7/a/a;->d(Z)V
:cond_a
return-void
.end method
.method public e()V
.registers 2
invoke-virtual {p0}, Landroid/support/v7/a/l;->a()Landroid/support/v7/a/a;
move-result-object v0
if-eqz v0, :cond_d
invoke-virtual {v0}, Landroid/support/v7/a/a;->e()Z
move-result v0
if-eqz v0, :cond_d
:goto_c
return-void
:cond_d
const/4 v0, 0x0
invoke-direct {p0, v0}, Landroid/support/v7/a/l;->e(I)V
goto :goto_c
.end method
.method public f()V
.registers 2
invoke-super {p0}, Landroid/support/v7/a/h;->f()V
iget-object v0, p0, Landroid/support/v7/a/l;->f:Landroid/support/v7/a/a;
if-eqz v0, :cond_c
iget-object v0, p0, Landroid/support/v7/a/l;->f:Landroid/support/v7/a/a;
invoke-virtual {v0}, Landroid/support/v7/a/a;->h()V
:cond_c
return-void
.end method
.method public g()V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/l;->a:Landroid/content/Context;
invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v0
invoke-virtual {v0}, Landroid/view/LayoutInflater;->getFactory()Landroid/view/LayoutInflater$Factory;
move-result-object v1
if-nez v1, :cond_10
invoke-static {v0, p0}, Landroid/support/v4/f/i;->a(Landroid/view/LayoutInflater;Landroid/support/v4/f/m;)V
:goto_f
:cond_f
return-void
:cond_10
invoke-static {v0}, Landroid/support/v4/f/i;->a(Landroid/view/LayoutInflater;)Landroid/support/v4/f/m;
move-result-object v0
instance-of v0, v0, Landroid/support/v7/a/l;
if-nez v0, :cond_f
const-string v0, "AppCompatDelegate"
const-string v1, "The Activity\'s LayoutInflater already has a Factory installed so we can not install AppCompat\'s"
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_f
.end method
.method public k()V
.registers 4
invoke-direct {p0}, Landroid/support/v7/a/l;->s()V
iget-boolean v0, p0, Landroid/support/v7/a/l;->h:Z
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/a/l;->f:Landroid/support/v7/a/a;
if-eqz v0, :cond_c
:cond_b
:goto_b
return-void
:cond_c
iget-object v0, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
instance-of v0, v0, Landroid/app/Activity;
if-eqz v0, :cond_2b
new-instance v1, Landroid/support/v7/a/r;
iget-object v0, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
check-cast v0, Landroid/app/Activity;
iget-boolean v2, p0, Landroid/support/v7/a/l;->i:Z
invoke-direct {v1, v0, v2}, Landroid/support/v7/a/r;-><init>(Landroid/app/Activity;Z)V
iput-object v1, p0, Landroid/support/v7/a/l;->f:Landroid/support/v7/a/a;
:goto_1f
:cond_1f
iget-object v0, p0, Landroid/support/v7/a/l;->f:Landroid/support/v7/a/a;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/a/l;->f:Landroid/support/v7/a/a;
iget-boolean v1, p0, Landroid/support/v7/a/l;->H:Z
invoke-virtual {v0, v1}, Landroid/support/v7/a/a;->c(Z)V
goto :goto_b
:cond_2b
iget-object v0, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
instance-of v0, v0, Landroid/app/Dialog;
if-eqz v0, :cond_1f
new-instance v1, Landroid/support/v7/a/r;
iget-object v0, p0, Landroid/support/v7/a/l;->c:Landroid/view/Window$Callback;
check-cast v0, Landroid/app/Dialog;
invoke-direct {v1, v0}, Landroid/support/v7/a/r;-><init>(Landroid/app/Dialog;)V
iput-object v1, p0, Landroid/support/v7/a/l;->f:Landroid/support/v7/a/a;
goto :goto_1f
.end method
.method  r()Z
.registers 3
const/4 v0, 0x1
iget-object v1, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
if-eqz v1, :cond_b
iget-object v1, p0, Landroid/support/v7/a/l;->m:Landroid/support/v7/view/b;
invoke-virtual {v1}, Landroid/support/v7/view/b;->c()V
:cond_a
:goto_a
return v0
:cond_b
invoke-virtual {p0}, Landroid/support/v7/a/l;->a()Landroid/support/v7/a/a;
move-result-object v1
if-eqz v1, :cond_17
invoke-virtual {v1}, Landroid/support/v7/a/a;->f()Z
move-result v1
if-nez v1, :cond_a
:cond_17
const/4 v0, 0x0
goto :goto_a
.end method