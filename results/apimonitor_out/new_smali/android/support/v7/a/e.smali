.class public Landroid/support/v7/a/e;
.super Landroid/support/v4/app/l;
.source "AppCompatActivity.java"
.implements Landroid/support/v4/app/ap$a;
.implements Landroid/support/v7/a/f;
.field private m:Landroid/support/v7/a/g;
.field private n:I
.field private o:Z
.field private p:Landroid/content/res/Resources;
.method public constructor <init>()V
.registers 2
invoke-direct {p0}, Landroid/support/v4/app/l;-><init>()V
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/a/e;->n:I
return-void
.end method
.method public a()Landroid/content/Intent;
.registers 2
invoke-static {p0}, Landroid/support/v4/app/v;->a(Landroid/app/Activity;)Landroid/content/Intent;
move-result-object v0
return-object v0
.end method
.method public a(Landroid/support/v7/view/b$a;)Landroid/support/v7/view/b;
.registers 3
const/4 v0, 0x0
return-object v0
.end method
.method public a(Landroid/support/v4/app/ap;)V
.registers 2
invoke-virtual {p1, p0}, Landroid/support/v4/app/ap;->a(Landroid/app/Activity;)Landroid/support/v4/app/ap;
return-void
.end method
.method public a(Landroid/support/v7/view/b;)V
.registers 2
return-void
.end method
.method public a(Landroid/content/Intent;)Z
.registers 3
invoke-static {p0, p1}, Landroid/support/v4/app/v;->a(Landroid/app/Activity;Landroid/content/Intent;)Z
move-result v0
return v0
.end method
.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.registers 4
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0, p1, p2}, Landroid/support/v7/a/g;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
return-void
.end method
.method public b(Landroid/content/Intent;)V
.registers 2
invoke-static {p0, p1}, Landroid/support/v4/app/v;->b(Landroid/app/Activity;Landroid/content/Intent;)V
return-void
.end method
.method public b(Landroid/support/v4/app/ap;)V
.registers 2
return-void
.end method
.method public b(Landroid/support/v7/view/b;)V
.registers 2
return-void
.end method
.method public d()V
.registers 2
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/a/g;->e()V
return-void
.end method
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
.registers 5
const/4 v0, 0x1
const/16 v1, 0x1000
invoke-static {p1, v1}, Landroid/support/v4/f/g;->a(Landroid/view/KeyEvent;I)Z
move-result v1
if-eqz v1, :cond_3c
invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I
move-result v1
and-int/lit16 v1, v1, -0x7001
invoke-virtual {p1, v1}, Landroid/view/KeyEvent;->getUnicodeChar(I)I
move-result v1
const/16 v2, 0x3c
if-ne v1, v2, :cond_3c
invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
move-result v1
if-nez v1, :cond_32
invoke-virtual {p0}, Landroid/support/v7/a/e;->g()Landroid/support/v7/a/a;
move-result-object v1
if-eqz v1, :cond_3c
invoke-virtual {v1}, Landroid/support/v7/a/a;->b()Z
move-result v2
if-eqz v2, :cond_3c
invoke-virtual {v1}, Landroid/support/v7/a/a;->g()Z
move-result v1
if-eqz v1, :cond_3c
iput-boolean v0, p0, Landroid/support/v7/a/e;->o:Z
:goto_31
return v0
:cond_32
if-ne v1, v0, :cond_3c
iget-boolean v1, p0, Landroid/support/v7/a/e;->o:Z
if-eqz v1, :cond_3c
const/4 v1, 0x0
iput-boolean v1, p0, Landroid/support/v7/a/e;->o:Z
goto :goto_31
:cond_3c
invoke-super {p0, p1}, Landroid/support/v4/app/l;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z
move-result v0
goto :goto_31
.end method
.method public findViewById(I)Landroid/view/View;
.registers 3
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/support/v7/a/g;->a(I)Landroid/view/View;
move-result-object v0
return-object v0
.end method
.method public g()Landroid/support/v7/a/a;
.registers 2
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/a/g;->a()Landroid/support/v7/a/a;
move-result-object v0
return-object v0
.end method
.method public getMenuInflater()Landroid/view/MenuInflater;
.registers 2
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/a/g;->b()Landroid/view/MenuInflater;
move-result-object v0
return-object v0
.end method
.method public getResources()Landroid/content/res/Resources;
.registers 3
iget-object v0, p0, Landroid/support/v7/a/e;->p:Landroid/content/res/Resources;
if-nez v0, :cond_15
invoke-static {}, Landroid/support/v7/widget/at;->a()Z
move-result v0
if-eqz v0, :cond_15
new-instance v0, Landroid/support/v7/widget/at;
invoke-super {p0}, Landroid/support/v4/app/l;->getResources()Landroid/content/res/Resources;
move-result-object v1
invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/at;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V
iput-object v0, p0, Landroid/support/v7/a/e;->p:Landroid/content/res/Resources;
:cond_15
iget-object v0, p0, Landroid/support/v7/a/e;->p:Landroid/content/res/Resources;
if-nez v0, :cond_1e
invoke-super {p0}, Landroid/support/v4/app/l;->getResources()Landroid/content/res/Resources;
move-result-object v0
:goto_1d
return-object v0
:cond_1e
iget-object v0, p0, Landroid/support/v7/a/e;->p:Landroid/content/res/Resources;
goto :goto_1d
.end method
.method public h()Z
.registers 3
invoke-virtual {p0}, Landroid/support/v7/a/e;->a()Landroid/content/Intent;
move-result-object v0
if-eqz v0, :cond_27
invoke-virtual {p0, v0}, Landroid/support/v7/a/e;->a(Landroid/content/Intent;)Z
move-result v1
if-eqz v1, :cond_23
invoke-static {p0}, Landroid/support/v4/app/ap;->a(Landroid/content/Context;)Landroid/support/v4/app/ap;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/support/v7/a/e;->a(Landroid/support/v4/app/ap;)V
invoke-virtual {p0, v0}, Landroid/support/v7/a/e;->b(Landroid/support/v4/app/ap;)V
invoke-virtual {v0}, Landroid/support/v4/app/ap;->a()V
:try_start_19
invoke-static {p0}, Landroid/support/v4/app/a;->a(Landroid/app/Activity;)V
:try_end_1c
.catch Ljava/lang/IllegalStateException; {:try_start_19 .. :try_end_1c} :catch_1e
:goto_1c
const/4 v0, 0x1
:goto_1d
return v0
:catch_1e
move-exception v0
invoke-virtual {p0}, Landroid/support/v7/a/e;->finish()V
goto :goto_1c
:cond_23
invoke-virtual {p0, v0}, Landroid/support/v7/a/e;->b(Landroid/content/Intent;)V
goto :goto_1c
:cond_27
const/4 v0, 0x0
goto :goto_1d
.end method
.method public i()V
.registers 1
return-void
.end method
.method public invalidateOptionsMenu()V
.registers 2
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/a/g;->e()V
return-void
.end method
.method public j()Landroid/support/v7/a/g;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/e;->m:Landroid/support/v7/a/g;
if-nez v0, :cond_a
invoke-static {p0, p0}, Landroid/support/v7/a/g;->a(Landroid/app/Activity;Landroid/support/v7/a/f;)Landroid/support/v7/a/g;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/a/e;->m:Landroid/support/v7/a/g;
:cond_a
iget-object v0, p0, Landroid/support/v7/a/e;->m:Landroid/support/v7/a/g;
return-object v0
.end method
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
.registers 4
invoke-super {p0, p1}, Landroid/support/v4/app/l;->onConfigurationChanged(Landroid/content/res/Configuration;)V
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/support/v7/a/g;->a(Landroid/content/res/Configuration;)V
iget-object v0, p0, Landroid/support/v7/a/e;->p:Landroid/content/res/Resources;
if-eqz v0, :cond_1b
invoke-super {p0}, Landroid/support/v4/app/l;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/a/e;->p:Landroid/content/res/Resources;
invoke-virtual {v1, p1, v0}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V
:cond_1b
return-void
.end method
.method public onContentChanged()V
.registers 1
invoke-virtual {p0}, Landroid/support/v7/a/e;->i()V
return-void
.end method
.method protected onCreate(Landroid/os/Bundle;)V
.registers 5
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/a/g;->g()V
invoke-virtual {v0, p1}, Landroid/support/v7/a/g;->a(Landroid/os/Bundle;)V
invoke-virtual {v0}, Landroid/support/v7/a/g;->h()Z
move-result v0
if-eqz v0, :cond_24
iget v0, p0, Landroid/support/v7/a/e;->n:I
if-eqz v0, :cond_24
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x17
if-lt v0, v1, :cond_28
invoke-virtual {p0}, Landroid/support/v7/a/e;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v0
iget v1, p0, Landroid/support/v7/a/e;->n:I
const/4 v2, 0x0
invoke-virtual {p0, v0, v1, v2}, Landroid/support/v7/a/e;->onApplyThemeResource(Landroid/content/res/Resources$Theme;IZ)V
:goto_24
:cond_24
invoke-super {p0, p1}, Landroid/support/v4/app/l;->onCreate(Landroid/os/Bundle;)V
return-void
:cond_28
iget v0, p0, Landroid/support/v7/a/e;->n:I
invoke-virtual {p0, v0}, Landroid/support/v7/a/e;->setTheme(I)V
goto :goto_24
.end method
.method protected onDestroy()V
.registers 2
invoke-super {p0}, Landroid/support/v4/app/l;->onDestroy()V
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/a/g;->f()V
return-void
.end method
.method public final onMenuItemSelected(ILandroid/view/MenuItem;)Z
.registers 6
invoke-super {p0, p1, p2}, Landroid/support/v4/app/l;->onMenuItemSelected(ILandroid/view/MenuItem;)Z
move-result v0
if-eqz v0, :cond_8
const/4 v0, 0x1
:goto_7
return v0
:cond_8
invoke-virtual {p0}, Landroid/support/v7/a/e;->g()Landroid/support/v7/a/a;
move-result-object v0
invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I
move-result v1
const v2, 0x102002c
if-ne v1, v2, :cond_24
if-eqz v0, :cond_24
invoke-virtual {v0}, Landroid/support/v7/a/a;->a()I
move-result v0
and-int/lit8 v0, v0, 0x4
if-eqz v0, :cond_24
invoke-virtual {p0}, Landroid/support/v7/a/e;->h()Z
move-result v0
goto :goto_7
:cond_24
const/4 v0, 0x0
goto :goto_7
.end method
.method public onMenuOpened(ILandroid/view/Menu;)Z
.registers 4
invoke-super {p0, p1, p2}, Landroid/support/v4/app/l;->onMenuOpened(ILandroid/view/Menu;)Z
move-result v0
return v0
.end method
.method public onPanelClosed(ILandroid/view/Menu;)V
.registers 3
invoke-super {p0, p1, p2}, Landroid/support/v4/app/l;->onPanelClosed(ILandroid/view/Menu;)V
return-void
.end method
.method protected onPostCreate(Landroid/os/Bundle;)V
.registers 3
invoke-super {p0, p1}, Landroid/support/v4/app/l;->onPostCreate(Landroid/os/Bundle;)V
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/support/v7/a/g;->b(Landroid/os/Bundle;)V
return-void
.end method
.method protected onPostResume()V
.registers 2
invoke-super {p0}, Landroid/support/v4/app/l;->onPostResume()V
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/a/g;->d()V
return-void
.end method
.method protected onSaveInstanceState(Landroid/os/Bundle;)V
.registers 3
invoke-super {p0, p1}, Landroid/support/v4/app/l;->onSaveInstanceState(Landroid/os/Bundle;)V
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/support/v7/a/g;->c(Landroid/os/Bundle;)V
return-void
.end method
.method protected onStop()V
.registers 2
invoke-super {p0}, Landroid/support/v4/app/l;->onStop()V
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/a/g;->c()V
return-void
.end method
.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
.registers 4
invoke-super {p0, p1, p2}, Landroid/support/v4/app/l;->onTitleChanged(Ljava/lang/CharSequence;I)V
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/support/v7/a/g;->a(Ljava/lang/CharSequence;)V
return-void
.end method
.method public setContentView(I)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/support/v7/a/g;->b(I)V
return-void
.end method
.method public setContentView(Landroid/view/View;)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/support/v7/a/g;->a(Landroid/view/View;)V
return-void
.end method
.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.registers 4
invoke-virtual {p0}, Landroid/support/v7/a/e;->j()Landroid/support/v7/a/g;
move-result-object v0
invoke-virtual {v0, p1, p2}, Landroid/support/v7/a/g;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
return-void
.end method
.method public setTheme(I)V
.registers 2
invoke-super {p0, p1}, Landroid/support/v4/app/l;->setTheme(I)V
iput p1, p0, Landroid/support/v7/a/e;->n:I
return-void
.end method