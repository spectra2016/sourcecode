.class public Landroid/support/v7/a/c$a;
.super Ljava/lang/Object;
.source "AlertController.java"
.field public A:I
.field public B:Z
.field public C:[Z
.field public D:Z
.field public E:Z
.field public F:I
.field public G:Landroid/content/DialogInterface$OnMultiChoiceClickListener;
.field public H:Landroid/database/Cursor;
.field public I:Ljava/lang/String;
.field public J:Ljava/lang/String;
.field public K:Landroid/widget/AdapterView$OnItemSelectedListener;
.field public L:Landroid/support/v7/a/c$a$a;
.field public M:Z
.field public final a:Landroid/content/Context;
.field public final b:Landroid/view/LayoutInflater;
.field public c:I
.field public d:Landroid/graphics/drawable/Drawable;
.field public e:I
.field public f:Ljava/lang/CharSequence;
.field public g:Landroid/view/View;
.field public h:Ljava/lang/CharSequence;
.field public i:Ljava/lang/CharSequence;
.field public j:Landroid/content/DialogInterface$OnClickListener;
.field public k:Ljava/lang/CharSequence;
.field public l:Landroid/content/DialogInterface$OnClickListener;
.field public m:Ljava/lang/CharSequence;
.field public n:Landroid/content/DialogInterface$OnClickListener;
.field public o:Z
.field public p:Landroid/content/DialogInterface$OnCancelListener;
.field public q:Landroid/content/DialogInterface$OnDismissListener;
.field public r:Landroid/content/DialogInterface$OnKeyListener;
.field public s:[Ljava/lang/CharSequence;
.field public t:Landroid/widget/ListAdapter;
.field public u:Landroid/content/DialogInterface$OnClickListener;
.field public v:I
.field public w:Landroid/view/View;
.field public x:I
.field public y:I
.field public z:I
.method public constructor <init>(Landroid/content/Context;)V
.registers 4
const/4 v1, 0x1
const/4 v0, 0x0
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput v0, p0, Landroid/support/v7/a/c$a;->c:I
iput v0, p0, Landroid/support/v7/a/c$a;->e:I
iput-boolean v0, p0, Landroid/support/v7/a/c$a;->B:Z
const/4 v0, -0x1
iput v0, p0, Landroid/support/v7/a/c$a;->F:I
iput-boolean v1, p0, Landroid/support/v7/a/c$a;->M:Z
iput-object p1, p0, Landroid/support/v7/a/c$a;->a:Landroid/content/Context;
iput-boolean v1, p0, Landroid/support/v7/a/c$a;->o:Z
const-string v0, "layout_inflater"
invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/LayoutInflater;
iput-object v0, p0, Landroid/support/v7/a/c$a;->b:Landroid/view/LayoutInflater;
return-void
.end method
.method private b(Landroid/support/v7/a/c;)V
.registers 12
const v4, 0x1020014
const/4 v9, 0x1
const/4 v5, 0x0
iget-object v0, p0, Landroid/support/v7/a/c$a;->b:Landroid/view/LayoutInflater;
invoke-static {p1}, Landroid/support/v7/a/c;->k(Landroid/support/v7/a/c;)I
move-result v1
const/4 v2, 0x0
invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
move-result-object v6
check-cast v6, Landroid/widget/ListView;
iget-boolean v0, p0, Landroid/support/v7/a/c$a;->D:Z
if-eqz v0, :cond_66
iget-object v0, p0, Landroid/support/v7/a/c$a;->H:Landroid/database/Cursor;
if-nez v0, :cond_59
new-instance v0, Landroid/support/v7/a/c$a$1;
iget-object v2, p0, Landroid/support/v7/a/c$a;->a:Landroid/content/Context;
invoke-static {p1}, Landroid/support/v7/a/c;->l(Landroid/support/v7/a/c;)I
move-result v3
iget-object v5, p0, Landroid/support/v7/a/c$a;->s:[Ljava/lang/CharSequence;
move-object v1, p0
invoke-direct/range {v0 .. v6}, Landroid/support/v7/a/c$a$1;-><init>(Landroid/support/v7/a/c$a;Landroid/content/Context;II[Ljava/lang/CharSequence;Landroid/widget/ListView;)V
:goto_28
iget-object v1, p0, Landroid/support/v7/a/c$a;->L:Landroid/support/v7/a/c$a$a;
if-eqz v1, :cond_31
iget-object v1, p0, Landroid/support/v7/a/c$a;->L:Landroid/support/v7/a/c$a$a;
invoke-interface {v1, v6}, Landroid/support/v7/a/c$a$a;->a(Landroid/widget/ListView;)V
:cond_31
invoke-static {p1, v0}, Landroid/support/v7/a/c;->a(Landroid/support/v7/a/c;Landroid/widget/ListAdapter;)Landroid/widget/ListAdapter;
iget v0, p0, Landroid/support/v7/a/c$a;->F:I
invoke-static {p1, v0}, Landroid/support/v7/a/c;->a(Landroid/support/v7/a/c;I)I
iget-object v0, p0, Landroid/support/v7/a/c$a;->u:Landroid/content/DialogInterface$OnClickListener;
if-eqz v0, :cond_9e
new-instance v0, Landroid/support/v7/a/c$a$3;
invoke-direct {v0, p0, p1}, Landroid/support/v7/a/c$a$3;-><init>(Landroid/support/v7/a/c$a;Landroid/support/v7/a/c;)V
invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
:goto_45
:cond_45
iget-object v0, p0, Landroid/support/v7/a/c$a;->K:Landroid/widget/AdapterView$OnItemSelectedListener;
if-eqz v0, :cond_4e
iget-object v0, p0, Landroid/support/v7/a/c$a;->K:Landroid/widget/AdapterView$OnItemSelectedListener;
invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
:cond_4e
iget-boolean v0, p0, Landroid/support/v7/a/c$a;->E:Z
if-eqz v0, :cond_ab
invoke-virtual {v6, v9}, Landroid/widget/ListView;->setChoiceMode(I)V
:goto_55
:cond_55
invoke-static {p1, v6}, Landroid/support/v7/a/c;->a(Landroid/support/v7/a/c;Landroid/widget/ListView;)Landroid/widget/ListView;
return-void
:cond_59
new-instance v1, Landroid/support/v7/a/c$a$2;
iget-object v3, p0, Landroid/support/v7/a/c$a;->a:Landroid/content/Context;
iget-object v4, p0, Landroid/support/v7/a/c$a;->H:Landroid/database/Cursor;
move-object v2, p0
move-object v7, p1
invoke-direct/range {v1 .. v7}, Landroid/support/v7/a/c$a$2;-><init>(Landroid/support/v7/a/c$a;Landroid/content/Context;Landroid/database/Cursor;ZLandroid/widget/ListView;Landroid/support/v7/a/c;)V
move-object v0, v1
goto :goto_28
:cond_66
iget-boolean v0, p0, Landroid/support/v7/a/c$a;->E:Z
if-eqz v0, :cond_88
invoke-static {p1}, Landroid/support/v7/a/c;->m(Landroid/support/v7/a/c;)I
move-result v2
:goto_6e
iget-object v0, p0, Landroid/support/v7/a/c$a;->H:Landroid/database/Cursor;
if-eqz v0, :cond_8d
new-instance v0, Landroid/widget/SimpleCursorAdapter;
iget-object v1, p0, Landroid/support/v7/a/c$a;->a:Landroid/content/Context;
iget-object v3, p0, Landroid/support/v7/a/c$a;->H:Landroid/database/Cursor;
new-array v7, v9, [Ljava/lang/String;
iget-object v8, p0, Landroid/support/v7/a/c$a;->I:Ljava/lang/String;
aput-object v8, v7, v5
new-array v8, v9, [I
aput v4, v8, v5
move-object v4, v7
move-object v5, v8
invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V
goto :goto_28
:cond_88
invoke-static {p1}, Landroid/support/v7/a/c;->n(Landroid/support/v7/a/c;)I
move-result v2
goto :goto_6e
:cond_8d
iget-object v0, p0, Landroid/support/v7/a/c$a;->t:Landroid/widget/ListAdapter;
if-eqz v0, :cond_94
iget-object v0, p0, Landroid/support/v7/a/c$a;->t:Landroid/widget/ListAdapter;
goto :goto_28
:cond_94
new-instance v0, Landroid/support/v7/a/c$c;
iget-object v1, p0, Landroid/support/v7/a/c$a;->a:Landroid/content/Context;
iget-object v3, p0, Landroid/support/v7/a/c$a;->s:[Ljava/lang/CharSequence;
invoke-direct {v0, v1, v2, v4, v3}, Landroid/support/v7/a/c$c;-><init>(Landroid/content/Context;II[Ljava/lang/CharSequence;)V
goto :goto_28
:cond_9e
iget-object v0, p0, Landroid/support/v7/a/c$a;->G:Landroid/content/DialogInterface$OnMultiChoiceClickListener;
if-eqz v0, :cond_45
new-instance v0, Landroid/support/v7/a/c$a$4;
invoke-direct {v0, p0, v6, p1}, Landroid/support/v7/a/c$a$4;-><init>(Landroid/support/v7/a/c$a;Landroid/widget/ListView;Landroid/support/v7/a/c;)V
invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
goto :goto_45
:cond_ab
iget-boolean v0, p0, Landroid/support/v7/a/c$a;->D:Z
if-eqz v0, :cond_55
const/4 v0, 0x2
invoke-virtual {v6, v0}, Landroid/widget/ListView;->setChoiceMode(I)V
goto :goto_55
.end method
.method public a(Landroid/support/v7/a/c;)V
.registers 8
const/4 v3, 0x0
iget-object v0, p0, Landroid/support/v7/a/c$a;->g:Landroid/view/View;
if-eqz v0, :cond_5d
iget-object v0, p0, Landroid/support/v7/a/c$a;->g:Landroid/view/View;
invoke-virtual {p1, v0}, Landroid/support/v7/a/c;->b(Landroid/view/View;)V
:cond_a
:goto_a
iget-object v0, p0, Landroid/support/v7/a/c$a;->h:Ljava/lang/CharSequence;
if-eqz v0, :cond_13
iget-object v0, p0, Landroid/support/v7/a/c$a;->h:Ljava/lang/CharSequence;
invoke-virtual {p1, v0}, Landroid/support/v7/a/c;->b(Ljava/lang/CharSequence;)V
:cond_13
iget-object v0, p0, Landroid/support/v7/a/c$a;->i:Ljava/lang/CharSequence;
if-eqz v0, :cond_1f
const/4 v0, -0x1
iget-object v1, p0, Landroid/support/v7/a/c$a;->i:Ljava/lang/CharSequence;
iget-object v2, p0, Landroid/support/v7/a/c$a;->j:Landroid/content/DialogInterface$OnClickListener;
invoke-virtual {p1, v0, v1, v2, v3}, Landroid/support/v7/a/c;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V
:cond_1f
iget-object v0, p0, Landroid/support/v7/a/c$a;->k:Ljava/lang/CharSequence;
if-eqz v0, :cond_2b
const/4 v0, -0x2
iget-object v1, p0, Landroid/support/v7/a/c$a;->k:Ljava/lang/CharSequence;
iget-object v2, p0, Landroid/support/v7/a/c$a;->l:Landroid/content/DialogInterface$OnClickListener;
invoke-virtual {p1, v0, v1, v2, v3}, Landroid/support/v7/a/c;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V
:cond_2b
iget-object v0, p0, Landroid/support/v7/a/c$a;->m:Ljava/lang/CharSequence;
if-eqz v0, :cond_37
const/4 v0, -0x3
iget-object v1, p0, Landroid/support/v7/a/c$a;->m:Ljava/lang/CharSequence;
iget-object v2, p0, Landroid/support/v7/a/c$a;->n:Landroid/content/DialogInterface$OnClickListener;
invoke-virtual {p1, v0, v1, v2, v3}, Landroid/support/v7/a/c;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V
:cond_37
iget-object v0, p0, Landroid/support/v7/a/c$a;->s:[Ljava/lang/CharSequence;
if-nez v0, :cond_43
iget-object v0, p0, Landroid/support/v7/a/c$a;->H:Landroid/database/Cursor;
if-nez v0, :cond_43
iget-object v0, p0, Landroid/support/v7/a/c$a;->t:Landroid/widget/ListAdapter;
if-eqz v0, :cond_46
:cond_43
invoke-direct {p0, p1}, Landroid/support/v7/a/c$a;->b(Landroid/support/v7/a/c;)V
:cond_46
iget-object v0, p0, Landroid/support/v7/a/c$a;->w:Landroid/view/View;
if-eqz v0, :cond_8c
iget-boolean v0, p0, Landroid/support/v7/a/c$a;->B:Z
if-eqz v0, :cond_86
iget-object v1, p0, Landroid/support/v7/a/c$a;->w:Landroid/view/View;
iget v2, p0, Landroid/support/v7/a/c$a;->x:I
iget v3, p0, Landroid/support/v7/a/c$a;->y:I
iget v4, p0, Landroid/support/v7/a/c$a;->z:I
iget v5, p0, Landroid/support/v7/a/c$a;->A:I
move-object v0, p1
invoke-virtual/range {v0 .. v5}, Landroid/support/v7/a/c;->a(Landroid/view/View;IIII)V
:goto_5c
:cond_5c
return-void
:cond_5d
iget-object v0, p0, Landroid/support/v7/a/c$a;->f:Ljava/lang/CharSequence;
if-eqz v0, :cond_66
iget-object v0, p0, Landroid/support/v7/a/c$a;->f:Ljava/lang/CharSequence;
invoke-virtual {p1, v0}, Landroid/support/v7/a/c;->a(Ljava/lang/CharSequence;)V
:cond_66
iget-object v0, p0, Landroid/support/v7/a/c$a;->d:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_6f
iget-object v0, p0, Landroid/support/v7/a/c$a;->d:Landroid/graphics/drawable/Drawable;
invoke-virtual {p1, v0}, Landroid/support/v7/a/c;->a(Landroid/graphics/drawable/Drawable;)V
:cond_6f
iget v0, p0, Landroid/support/v7/a/c$a;->c:I
if-eqz v0, :cond_78
iget v0, p0, Landroid/support/v7/a/c$a;->c:I
invoke-virtual {p1, v0}, Landroid/support/v7/a/c;->b(I)V
:cond_78
iget v0, p0, Landroid/support/v7/a/c$a;->e:I
if-eqz v0, :cond_a
iget v0, p0, Landroid/support/v7/a/c$a;->e:I
invoke-virtual {p1, v0}, Landroid/support/v7/a/c;->c(I)I
move-result v0
invoke-virtual {p1, v0}, Landroid/support/v7/a/c;->b(I)V
goto :goto_a
:cond_86
iget-object v0, p0, Landroid/support/v7/a/c$a;->w:Landroid/view/View;
invoke-virtual {p1, v0}, Landroid/support/v7/a/c;->c(Landroid/view/View;)V
goto :goto_5c
:cond_8c
iget v0, p0, Landroid/support/v7/a/c$a;->v:I
if-eqz v0, :cond_5c
iget v0, p0, Landroid/support/v7/a/c$a;->v:I
invoke-virtual {p1, v0}, Landroid/support/v7/a/c;->a(I)V
goto :goto_5c
.end method