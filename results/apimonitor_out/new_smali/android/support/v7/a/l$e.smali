.class final Landroid/support/v7/a/l$e;
.super Ljava/lang/Object;
.source "AppCompatDelegateImplV7.java"
.implements Landroid/support/v7/view/menu/l$a;
.field final synthetic a:Landroid/support/v7/a/l;
.method private constructor <init>(Landroid/support/v7/a/l;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/a/l$e;->a:Landroid/support/v7/a/l;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method synthetic constructor <init>(Landroid/support/v7/a/l;Landroid/support/v7/a/l$1;)V
.registers 3
invoke-direct {p0, p1}, Landroid/support/v7/a/l$e;-><init>(Landroid/support/v7/a/l;)V
return-void
.end method
.method public a(Landroid/support/v7/view/menu/f;Z)V
.registers 8
const/4 v1, 0x1
invoke-virtual {p1}, Landroid/support/v7/view/menu/f;->p()Landroid/support/v7/view/menu/f;
move-result-object v2
if-eq v2, p1, :cond_22
move v0, v1
:goto_8
iget-object v3, p0, Landroid/support/v7/a/l$e;->a:Landroid/support/v7/a/l;
if-eqz v0, :cond_d
move-object p1, v2
:cond_d
invoke-static {v3, p1}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l;Landroid/view/Menu;)Landroid/support/v7/a/l$d;
move-result-object v3
if-eqz v3, :cond_21
if-eqz v0, :cond_24
iget-object v0, p0, Landroid/support/v7/a/l$e;->a:Landroid/support/v7/a/l;
iget v4, v3, Landroid/support/v7/a/l$d;->a:I
invoke-static {v0, v4, v3, v2}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l;ILandroid/support/v7/a/l$d;Landroid/view/Menu;)V
iget-object v0, p0, Landroid/support/v7/a/l$e;->a:Landroid/support/v7/a/l;
invoke-static {v0, v3, v1}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l;Landroid/support/v7/a/l$d;Z)V
:goto_21
:cond_21
return-void
:cond_22
const/4 v0, 0x0
goto :goto_8
:cond_24
iget-object v0, p0, Landroid/support/v7/a/l$e;->a:Landroid/support/v7/a/l;
invoke-static {v0, v3, p2}, Landroid/support/v7/a/l;->a(Landroid/support/v7/a/l;Landroid/support/v7/a/l$d;Z)V
goto :goto_21
.end method
.method public a(Landroid/support/v7/view/menu/f;)Z
.registers 4
if-nez p1, :cond_1d
iget-object v0, p0, Landroid/support/v7/a/l$e;->a:Landroid/support/v7/a/l;
iget-boolean v0, v0, Landroid/support/v7/a/l;->h:Z
if-eqz v0, :cond_1d
iget-object v0, p0, Landroid/support/v7/a/l$e;->a:Landroid/support/v7/a/l;
invoke-virtual {v0}, Landroid/support/v7/a/l;->p()Landroid/view/Window$Callback;
move-result-object v0
if-eqz v0, :cond_1d
iget-object v1, p0, Landroid/support/v7/a/l$e;->a:Landroid/support/v7/a/l;
invoke-virtual {v1}, Landroid/support/v7/a/l;->o()Z
move-result v1
if-nez v1, :cond_1d
const/16 v1, 0x6c
invoke-interface {v0, v1, p1}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z
:cond_1d
const/4 v0, 0x1
return v0
.end method