.class  Landroid/support/v7/a/o;
.super Landroid/support/v7/a/a;
.source "ToolbarActionBar.java"
.field private a:Landroid/support/v7/widget/ac;
.field private b:Landroid/view/Window$Callback;
.field private c:Z
.field private d:Z
.field private e:Ljava/util/ArrayList;
.field private final f:Ljava/lang/Runnable;
.method static synthetic a(Landroid/support/v7/a/o;)Landroid/view/Window$Callback;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/o;->b:Landroid/view/Window$Callback;
return-object v0
.end method
.method static synthetic b(Landroid/support/v7/a/o;)Landroid/support/v7/widget/ac;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
return-object v0
.end method
.method private j()Landroid/view/Menu;
.registers 5
const/4 v3, 0x0
iget-boolean v0, p0, Landroid/support/v7/a/o;->c:Z
if-nez v0, :cond_17
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
new-instance v1, Landroid/support/v7/a/o$a;
invoke-direct {v1, p0, v3}, Landroid/support/v7/a/o$a;-><init>(Landroid/support/v7/a/o;Landroid/support/v7/a/o$1;)V
new-instance v2, Landroid/support/v7/a/o$b;
invoke-direct {v2, p0, v3}, Landroid/support/v7/a/o$b;-><init>(Landroid/support/v7/a/o;Landroid/support/v7/a/o$1;)V
invoke-interface {v0, v1, v2}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/view/menu/l$a;Landroid/support/v7/view/menu/f$a;)V
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v7/a/o;->c:Z
:cond_17
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->r()Landroid/view/Menu;
move-result-object v0
return-object v0
.end method
.method public a()I
.registers 2
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->o()I
move-result v0
return v0
.end method
.method public a(F)V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->a()Landroid/view/ViewGroup;
move-result-object v0
invoke-static {v0, p1}, Landroid/support/v4/f/af;->d(Landroid/view/View;F)V
return-void
.end method
.method public a(Landroid/content/res/Configuration;)V
.registers 2
invoke-super {p0, p1}, Landroid/support/v7/a/a;->a(Landroid/content/res/Configuration;)V
return-void
.end method
.method public a(Ljava/lang/CharSequence;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
invoke-interface {v0, p1}, Landroid/support/v7/widget/ac;->a(Ljava/lang/CharSequence;)V
return-void
.end method
.method public a(Z)V
.registers 2
return-void
.end method
.method public a(ILandroid/view/KeyEvent;)Z
.registers 7
const/4 v2, 0x0
const/4 v1, 0x1
invoke-direct {p0}, Landroid/support/v7/a/o;->j()Landroid/view/Menu;
move-result-object v3
if-eqz v3, :cond_1f
if-eqz p2, :cond_20
invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I
move-result v0
:goto_e
invoke-static {v0}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;
move-result-object v0
invoke-virtual {v0}, Landroid/view/KeyCharacterMap;->getKeyboardType()I
move-result v0
if-eq v0, v1, :cond_22
move v0, v1
:goto_19
invoke-interface {v3, v0}, Landroid/view/Menu;->setQwertyMode(Z)V
invoke-interface {v3, p1, p2, v2}, Landroid/view/Menu;->performShortcut(ILandroid/view/KeyEvent;I)Z
:cond_1f
return v1
:cond_20
const/4 v0, -0x1
goto :goto_e
:cond_22
move v0, v2
goto :goto_19
.end method
.method public b()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->q()I
move-result v0
if-nez v0, :cond_a
const/4 v0, 0x1
:goto_9
return v0
:cond_a
const/4 v0, 0x0
goto :goto_9
.end method
.method public c()Landroid/content/Context;
.registers 2
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->b()Landroid/content/Context;
move-result-object v0
return-object v0
.end method
.method public c(Z)V
.registers 2
return-void
.end method
.method public d(Z)V
.registers 2
return-void
.end method
.method public e(Z)V
.registers 5
iget-boolean v0, p0, Landroid/support/v7/a/o;->d:Z
if-ne p1, v0, :cond_5
:cond_4
return-void
:cond_5
iput-boolean p1, p0, Landroid/support/v7/a/o;->d:Z
iget-object v0, p0, Landroid/support/v7/a/o;->e:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v2
const/4 v0, 0x0
move v1, v0
:goto_f
if-ge v1, v2, :cond_4
iget-object v0, p0, Landroid/support/v7/a/o;->e:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v7/a/a$b;
invoke-interface {v0, p1}, Landroid/support/v7/a/a$b;->a(Z)V
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_f
.end method
.method public e()Z
.registers 3
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->a()Landroid/view/ViewGroup;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/a/o;->f:Ljava/lang/Runnable;
invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->a()Landroid/view/ViewGroup;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/a/o;->f:Ljava/lang/Runnable;
invoke-static {v0, v1}, Landroid/support/v4/f/af;->a(Landroid/view/View;Ljava/lang/Runnable;)V
const/4 v0, 0x1
return v0
.end method
.method public f()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->c()Z
move-result v0
if-eqz v0, :cond_f
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->d()V
const/4 v0, 0x1
:goto_e
return v0
:cond_f
const/4 v0, 0x0
goto :goto_e
.end method
.method public g()Z
.registers 3
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->a()Landroid/view/ViewGroup;
move-result-object v0
if-eqz v0, :cond_13
invoke-virtual {v0}, Landroid/view/ViewGroup;->hasFocus()Z
move-result v1
if-nez v1, :cond_13
invoke-virtual {v0}, Landroid/view/ViewGroup;->requestFocus()Z
const/4 v0, 0x1
:goto_12
return v0
:cond_13
const/4 v0, 0x0
goto :goto_12
.end method
.method  h()V
.registers 3
iget-object v0, p0, Landroid/support/v7/a/o;->a:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->a()Landroid/view/ViewGroup;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/a/o;->f:Ljava/lang/Runnable;
invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z
return-void
.end method
.method  i()V
.registers 6
const/4 v0, 0x0
invoke-direct {p0}, Landroid/support/v7/a/o;->j()Landroid/view/Menu;
move-result-object v1
instance-of v2, v1, Landroid/support/v7/view/menu/f;
if-eqz v2, :cond_31
move-object v0, v1
check-cast v0, Landroid/support/v7/view/menu/f;
move-object v2, v0
:goto_d
if-eqz v2, :cond_12
invoke-virtual {v2}, Landroid/support/v7/view/menu/f;->g()V
:cond_12
:try_start_12
invoke-interface {v1}, Landroid/view/Menu;->clear()V
iget-object v0, p0, Landroid/support/v7/a/o;->b:Landroid/view/Window$Callback;
const/4 v3, 0x0
invoke-interface {v0, v3, v1}, Landroid/view/Window$Callback;->onCreatePanelMenu(ILandroid/view/Menu;)Z
move-result v0
if-eqz v0, :cond_28
iget-object v0, p0, Landroid/support/v7/a/o;->b:Landroid/view/Window$Callback;
const/4 v3, 0x0
const/4 v4, 0x0
invoke-interface {v0, v3, v4, v1}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
move-result v0
if-nez v0, :cond_2b
:cond_28
invoke-interface {v1}, Landroid/view/Menu;->clear()V
:try_end_2b
.catchall {:try_start_12 .. :try_end_2b} :catchall_33
:cond_2b
if-eqz v2, :cond_30
invoke-virtual {v2}, Landroid/support/v7/view/menu/f;->h()V
:cond_30
return-void
:cond_31
move-object v2, v0
goto :goto_d
:catchall_33
move-exception v0
if-eqz v2, :cond_39
invoke-virtual {v2}, Landroid/support/v7/view/menu/f;->h()V
:cond_39
throw v0
.end method