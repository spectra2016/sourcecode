.class  Landroid/support/v7/a/j$a;
.super Landroid/support/v7/a/h$a;
.source "AppCompatDelegateImplV14.java"
.field final synthetic b:Landroid/support/v7/a/j;
.method constructor <init>(Landroid/support/v7/a/j;Landroid/view/Window$Callback;)V
.registers 3
iput-object p1, p0, Landroid/support/v7/a/j$a;->b:Landroid/support/v7/a/j;
invoke-direct {p0, p1, p2}, Landroid/support/v7/a/h$a;-><init>(Landroid/support/v7/a/h;Landroid/view/Window$Callback;)V
return-void
.end method
.method final a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
.registers 4
new-instance v0, Landroid/support/v7/view/f$a;
iget-object v1, p0, Landroid/support/v7/a/j$a;->b:Landroid/support/v7/a/j;
iget-object v1, v1, Landroid/support/v7/a/j;->a:Landroid/content/Context;
invoke-direct {v0, v1, p1}, Landroid/support/v7/view/f$a;-><init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V
iget-object v1, p0, Landroid/support/v7/a/j$a;->b:Landroid/support/v7/a/j;
invoke-virtual {v1, v0}, Landroid/support/v7/a/j;->b(Landroid/support/v7/view/b$a;)Landroid/support/v7/view/b;
move-result-object v1
if-eqz v1, :cond_16
invoke-virtual {v0, v1}, Landroid/support/v7/view/f$a;->b(Landroid/support/v7/view/b;)Landroid/view/ActionMode;
move-result-object v0
:goto_15
return-object v0
:cond_16
const/4 v0, 0x0
goto :goto_15
.end method
.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
.registers 3
iget-object v0, p0, Landroid/support/v7/a/j$a;->b:Landroid/support/v7/a/j;
invoke-virtual {v0}, Landroid/support/v7/a/j;->n()Z
move-result v0
if-eqz v0, :cond_d
invoke-virtual {p0, p1}, Landroid/support/v7/a/j$a;->a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
move-result-object v0
:goto_c
return-object v0
:cond_d
invoke-super {p0, p1}, Landroid/support/v7/a/h$a;->onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
move-result-object v0
goto :goto_c
.end method