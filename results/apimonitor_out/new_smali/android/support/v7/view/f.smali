.class public Landroid/support/v7/view/f;
.super Landroid/view/ActionMode;
.source "SupportActionModeWrapper.java"
.field final a:Landroid/content/Context;
.field final b:Landroid/support/v7/view/b;
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/view/b;)V
.registers 3
invoke-direct {p0}, Landroid/view/ActionMode;-><init>()V
iput-object p1, p0, Landroid/support/v7/view/f;->a:Landroid/content/Context;
iput-object p2, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
return-void
.end method
.method public finish()V
.registers 2
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0}, Landroid/support/v7/view/b;->c()V
return-void
.end method
.method public getCustomView()Landroid/view/View;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0}, Landroid/support/v7/view/b;->i()Landroid/view/View;
move-result-object v0
return-object v0
.end method
.method public getMenu()Landroid/view/Menu;
.registers 3
iget-object v1, p0, Landroid/support/v7/view/f;->a:Landroid/content/Context;
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0}, Landroid/support/v7/view/b;->b()Landroid/view/Menu;
move-result-object v0
check-cast v0, Landroid/support/v4/c/a/a;
invoke-static {v1, v0}, Landroid/support/v7/view/menu/n;->a(Landroid/content/Context;Landroid/support/v4/c/a/a;)Landroid/view/Menu;
move-result-object v0
return-object v0
.end method
.method public getMenuInflater()Landroid/view/MenuInflater;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0}, Landroid/support/v7/view/b;->a()Landroid/view/MenuInflater;
move-result-object v0
return-object v0
.end method
.method public getSubtitle()Ljava/lang/CharSequence;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0}, Landroid/support/v7/view/b;->g()Ljava/lang/CharSequence;
move-result-object v0
return-object v0
.end method
.method public getTag()Ljava/lang/Object;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0}, Landroid/support/v7/view/b;->j()Ljava/lang/Object;
move-result-object v0
return-object v0
.end method
.method public getTitle()Ljava/lang/CharSequence;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0}, Landroid/support/v7/view/b;->f()Ljava/lang/CharSequence;
move-result-object v0
return-object v0
.end method
.method public getTitleOptionalHint()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0}, Landroid/support/v7/view/b;->k()Z
move-result v0
return v0
.end method
.method public invalidate()V
.registers 2
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0}, Landroid/support/v7/view/b;->d()V
return-void
.end method
.method public isTitleOptional()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0}, Landroid/support/v7/view/b;->h()Z
move-result v0
return v0
.end method
.method public setCustomView(Landroid/view/View;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0, p1}, Landroid/support/v7/view/b;->a(Landroid/view/View;)V
return-void
.end method
.method public setSubtitle(I)V
.registers 3
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0, p1}, Landroid/support/v7/view/b;->b(I)V
return-void
.end method
.method public setSubtitle(Ljava/lang/CharSequence;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0, p1}, Landroid/support/v7/view/b;->a(Ljava/lang/CharSequence;)V
return-void
.end method
.method public setTag(Ljava/lang/Object;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0, p1}, Landroid/support/v7/view/b;->a(Ljava/lang/Object;)V
return-void
.end method
.method public setTitle(I)V
.registers 3
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0, p1}, Landroid/support/v7/view/b;->a(I)V
return-void
.end method
.method public setTitle(Ljava/lang/CharSequence;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0, p1}, Landroid/support/v7/view/b;->b(Ljava/lang/CharSequence;)V
return-void
.end method
.method public setTitleOptionalHint(Z)V
.registers 3
iget-object v0, p0, Landroid/support/v7/view/f;->b:Landroid/support/v7/view/b;
invoke-virtual {v0, p1}, Landroid/support/v7/view/b;->a(Z)V
return-void
.end method