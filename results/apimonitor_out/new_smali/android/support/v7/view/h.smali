.class public Landroid/support/v7/view/h;
.super Ljava/lang/Object;
.source "ViewPropertyAnimatorCompatSet.java"
.field private final a:Ljava/util/ArrayList;
.field private b:J
.field private c:Landroid/view/animation/Interpolator;
.field private d:Landroid/support/v4/f/ay;
.field private e:Z
.field private final f:Landroid/support/v4/f/az;
.method public constructor <init>()V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const-wide/16 v0, -0x1
iput-wide v0, p0, Landroid/support/v7/view/h;->b:J
new-instance v0, Landroid/support/v7/view/h$1;
invoke-direct {v0, p0}, Landroid/support/v7/view/h$1;-><init>(Landroid/support/v7/view/h;)V
iput-object v0, p0, Landroid/support/v7/view/h;->f:Landroid/support/v4/f/az;
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Landroid/support/v7/view/h;->a:Ljava/util/ArrayList;
return-void
.end method
.method static synthetic a(Landroid/support/v7/view/h;)Landroid/support/v4/f/ay;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/h;->d:Landroid/support/v4/f/ay;
return-object v0
.end method
.method static synthetic b(Landroid/support/v7/view/h;)V
.registers 1
invoke-direct {p0}, Landroid/support/v7/view/h;->c()V
return-void
.end method
.method static synthetic c(Landroid/support/v7/view/h;)Ljava/util/ArrayList;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/h;->a:Ljava/util/ArrayList;
return-object v0
.end method
.method private c()V
.registers 2
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v7/view/h;->e:Z
return-void
.end method
.method public a(J)Landroid/support/v7/view/h;
.registers 4
iget-boolean v0, p0, Landroid/support/v7/view/h;->e:Z
if-nez v0, :cond_6
iput-wide p1, p0, Landroid/support/v7/view/h;->b:J
:cond_6
return-object p0
.end method
.method public a(Landroid/support/v4/f/au;)Landroid/support/v7/view/h;
.registers 3
iget-boolean v0, p0, Landroid/support/v7/view/h;->e:Z
if-nez v0, :cond_9
iget-object v0, p0, Landroid/support/v7/view/h;->a:Ljava/util/ArrayList;
invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
:cond_9
return-object p0
.end method
.method public a(Landroid/support/v4/f/au;Landroid/support/v4/f/au;)Landroid/support/v7/view/h;
.registers 5
iget-object v0, p0, Landroid/support/v7/view/h;->a:Ljava/util/ArrayList;
invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
invoke-virtual {p1}, Landroid/support/v4/f/au;->a()J
move-result-wide v0
invoke-virtual {p2, v0, v1}, Landroid/support/v4/f/au;->b(J)Landroid/support/v4/f/au;
iget-object v0, p0, Landroid/support/v7/view/h;->a:Ljava/util/ArrayList;
invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
return-object p0
.end method
.method public a(Landroid/support/v4/f/ay;)Landroid/support/v7/view/h;
.registers 3
iget-boolean v0, p0, Landroid/support/v7/view/h;->e:Z
if-nez v0, :cond_6
iput-object p1, p0, Landroid/support/v7/view/h;->d:Landroid/support/v4/f/ay;
:cond_6
return-object p0
.end method
.method public a(Landroid/view/animation/Interpolator;)Landroid/support/v7/view/h;
.registers 3
iget-boolean v0, p0, Landroid/support/v7/view/h;->e:Z
if-nez v0, :cond_6
iput-object p1, p0, Landroid/support/v7/view/h;->c:Landroid/view/animation/Interpolator;
:cond_6
return-object p0
.end method
.method public a()V
.registers 7
iget-boolean v0, p0, Landroid/support/v7/view/h;->e:Z
if-eqz v0, :cond_5
:goto_4
return-void
:cond_5
iget-object v0, p0, Landroid/support/v7/view/h;->a:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
move-result-object v1
:goto_b
invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_3a
invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/f/au;
iget-wide v2, p0, Landroid/support/v7/view/h;->b:J
const-wide/16 v4, 0x0
cmp-long v2, v2, v4
if-ltz v2, :cond_24
iget-wide v2, p0, Landroid/support/v7/view/h;->b:J
invoke-virtual {v0, v2, v3}, Landroid/support/v4/f/au;->a(J)Landroid/support/v4/f/au;
:cond_24
iget-object v2, p0, Landroid/support/v7/view/h;->c:Landroid/view/animation/Interpolator;
if-eqz v2, :cond_2d
iget-object v2, p0, Landroid/support/v7/view/h;->c:Landroid/view/animation/Interpolator;
invoke-virtual {v0, v2}, Landroid/support/v4/f/au;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/f/au;
:cond_2d
iget-object v2, p0, Landroid/support/v7/view/h;->d:Landroid/support/v4/f/ay;
if-eqz v2, :cond_36
iget-object v2, p0, Landroid/support/v7/view/h;->f:Landroid/support/v4/f/az;
invoke-virtual {v0, v2}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/ay;)Landroid/support/v4/f/au;
:cond_36
invoke-virtual {v0}, Landroid/support/v4/f/au;->c()V
goto :goto_b
:cond_3a
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v7/view/h;->e:Z
goto :goto_4
.end method
.method public b()V
.registers 3
iget-boolean v0, p0, Landroid/support/v7/view/h;->e:Z
if-nez v0, :cond_5
:goto_4
return-void
:cond_5
iget-object v0, p0, Landroid/support/v7/view/h;->a:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
move-result-object v1
:goto_b
invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_1b
invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/f/au;
invoke-virtual {v0}, Landroid/support/v4/f/au;->b()V
goto :goto_b
:cond_1b
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v7/view/h;->e:Z
goto :goto_4
.end method