.class  Landroid/support/v7/view/menu/i$c;
.super Landroid/support/v7/view/menu/d;
.source "MenuItemWrapperICS.java"
.implements Landroid/support/v4/f/p$e;
.field final synthetic a:Landroid/support/v7/view/menu/i;
.method constructor <init>(Landroid/support/v7/view/menu/i;Landroid/view/MenuItem$OnActionExpandListener;)V
.registers 3
iput-object p1, p0, Landroid/support/v7/view/menu/i$c;->a:Landroid/support/v7/view/menu/i;
invoke-direct {p0, p2}, Landroid/support/v7/view/menu/d;-><init>(Ljava/lang/Object;)V
return-void
.end method
.method public a(Landroid/view/MenuItem;)Z
.registers 4
iget-object v0, p0, Landroid/support/v7/view/menu/i$c;->b:Ljava/lang/Object;
check-cast v0, Landroid/view/MenuItem$OnActionExpandListener;
iget-object v1, p0, Landroid/support/v7/view/menu/i$c;->a:Landroid/support/v7/view/menu/i;
invoke-virtual {v1, p1}, Landroid/support/v7/view/menu/i;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
move-result-object v1
invoke-interface {v0, v1}, Landroid/view/MenuItem$OnActionExpandListener;->onMenuItemActionExpand(Landroid/view/MenuItem;)Z
move-result v0
return v0
.end method
.method public b(Landroid/view/MenuItem;)Z
.registers 4
iget-object v0, p0, Landroid/support/v7/view/menu/i$c;->b:Ljava/lang/Object;
check-cast v0, Landroid/view/MenuItem$OnActionExpandListener;
iget-object v1, p0, Landroid/support/v7/view/menu/i$c;->a:Landroid/support/v7/view/menu/i;
invoke-virtual {v1, p1}, Landroid/support/v7/view/menu/i;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
move-result-object v1
invoke-interface {v0, v1}, Landroid/view/MenuItem$OnActionExpandListener;->onMenuItemActionCollapse(Landroid/view/MenuItem;)Z
move-result v0
return v0
.end method