.class  Landroid/support/v7/view/menu/i$b;
.super Landroid/widget/FrameLayout;
.source "MenuItemWrapperICS.java"
.implements Landroid/support/v7/view/c;
.field final a:Landroid/view/CollapsibleActionView;
.method constructor <init>(Landroid/view/View;)V
.registers 3
invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;
move-result-object v0
invoke-direct {p0, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V
move-object v0, p1
check-cast v0, Landroid/view/CollapsibleActionView;
iput-object v0, p0, Landroid/support/v7/view/menu/i$b;->a:Landroid/view/CollapsibleActionView;
invoke-virtual {p0, p1}, Landroid/support/v7/view/menu/i$b;->addView(Landroid/view/View;)V
return-void
.end method
.method public a()V
.registers 2
iget-object v0, p0, Landroid/support/v7/view/menu/i$b;->a:Landroid/view/CollapsibleActionView;
invoke-interface {v0}, Landroid/view/CollapsibleActionView;->onActionViewExpanded()V
return-void
.end method
.method public b()V
.registers 2
iget-object v0, p0, Landroid/support/v7/view/menu/i$b;->a:Landroid/view/CollapsibleActionView;
invoke-interface {v0}, Landroid/view/CollapsibleActionView;->onActionViewCollapsed()V
return-void
.end method
.method  c()Landroid/view/View;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/menu/i$b;->a:Landroid/view/CollapsibleActionView;
check-cast v0, Landroid/view/View;
return-object v0
.end method