.class public Landroid/support/v7/view/menu/e;
.super Ljava/lang/Object;
.source "ListMenuPresenter.java"
.implements Landroid/support/v7/view/menu/l;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.field  a:Landroid/content/Context;
.field  b:Landroid/view/LayoutInflater;
.field  c:Landroid/support/v7/view/menu/f;
.field  d:Landroid/support/v7/view/menu/ExpandedMenuView;
.field  e:I
.field  f:I
.field  g:Landroid/support/v7/view/menu/e$a;
.field private h:I
.field private i:Landroid/support/v7/view/menu/l$a;
.method public constructor <init>(II)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput p1, p0, Landroid/support/v7/view/menu/e;->f:I
iput p2, p0, Landroid/support/v7/view/menu/e;->e:I
return-void
.end method
.method public constructor <init>(Landroid/content/Context;I)V
.registers 4
const/4 v0, 0x0
invoke-direct {p0, p2, v0}, Landroid/support/v7/view/menu/e;-><init>(II)V
iput-object p1, p0, Landroid/support/v7/view/menu/e;->a:Landroid/content/Context;
iget-object v0, p0, Landroid/support/v7/view/menu/e;->a:Landroid/content/Context;
invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/view/menu/e;->b:Landroid/view/LayoutInflater;
return-void
.end method
.method static synthetic a(Landroid/support/v7/view/menu/e;)I
.registers 2
iget v0, p0, Landroid/support/v7/view/menu/e;->h:I
return v0
.end method
.method public a(Landroid/view/ViewGroup;)Landroid/support/v7/view/menu/m;
.registers 5
iget-object v0, p0, Landroid/support/v7/view/menu/e;->d:Landroid/support/v7/view/menu/ExpandedMenuView;
if-nez v0, :cond_28
iget-object v0, p0, Landroid/support/v7/view/menu/e;->b:Landroid/view/LayoutInflater;
sget v1, Landroid/support/v7/b/a$h;->abc_expanded_menu_layout:I
const/4 v2, 0x0
invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/support/v7/view/menu/ExpandedMenuView;
iput-object v0, p0, Landroid/support/v7/view/menu/e;->d:Landroid/support/v7/view/menu/ExpandedMenuView;
iget-object v0, p0, Landroid/support/v7/view/menu/e;->g:Landroid/support/v7/view/menu/e$a;
if-nez v0, :cond_1c
new-instance v0, Landroid/support/v7/view/menu/e$a;
invoke-direct {v0, p0}, Landroid/support/v7/view/menu/e$a;-><init>(Landroid/support/v7/view/menu/e;)V
iput-object v0, p0, Landroid/support/v7/view/menu/e;->g:Landroid/support/v7/view/menu/e$a;
:cond_1c
iget-object v0, p0, Landroid/support/v7/view/menu/e;->d:Landroid/support/v7/view/menu/ExpandedMenuView;
iget-object v1, p0, Landroid/support/v7/view/menu/e;->g:Landroid/support/v7/view/menu/e$a;
invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/ExpandedMenuView;->setAdapter(Landroid/widget/ListAdapter;)V
iget-object v0, p0, Landroid/support/v7/view/menu/e;->d:Landroid/support/v7/view/menu/ExpandedMenuView;
invoke-virtual {v0, p0}, Landroid/support/v7/view/menu/ExpandedMenuView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
:cond_28
iget-object v0, p0, Landroid/support/v7/view/menu/e;->d:Landroid/support/v7/view/menu/ExpandedMenuView;
return-object v0
.end method
.method public a()Landroid/widget/ListAdapter;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/menu/e;->g:Landroid/support/v7/view/menu/e$a;
if-nez v0, :cond_b
new-instance v0, Landroid/support/v7/view/menu/e$a;
invoke-direct {v0, p0}, Landroid/support/v7/view/menu/e$a;-><init>(Landroid/support/v7/view/menu/e;)V
iput-object v0, p0, Landroid/support/v7/view/menu/e;->g:Landroid/support/v7/view/menu/e$a;
:cond_b
iget-object v0, p0, Landroid/support/v7/view/menu/e;->g:Landroid/support/v7/view/menu/e$a;
return-object v0
.end method
.method public a(Landroid/content/Context;Landroid/support/v7/view/menu/f;)V
.registers 5
iget v0, p0, Landroid/support/v7/view/menu/e;->e:I
if-eqz v0, :cond_21
new-instance v0, Landroid/view/ContextThemeWrapper;
iget v1, p0, Landroid/support/v7/view/menu/e;->e:I
invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V
iput-object v0, p0, Landroid/support/v7/view/menu/e;->a:Landroid/content/Context;
iget-object v0, p0, Landroid/support/v7/view/menu/e;->a:Landroid/content/Context;
invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/view/menu/e;->b:Landroid/view/LayoutInflater;
:goto_15
:cond_15
iput-object p2, p0, Landroid/support/v7/view/menu/e;->c:Landroid/support/v7/view/menu/f;
iget-object v0, p0, Landroid/support/v7/view/menu/e;->g:Landroid/support/v7/view/menu/e$a;
if-eqz v0, :cond_20
iget-object v0, p0, Landroid/support/v7/view/menu/e;->g:Landroid/support/v7/view/menu/e$a;
invoke-virtual {v0}, Landroid/support/v7/view/menu/e$a;->notifyDataSetChanged()V
:cond_20
return-void
:cond_21
iget-object v0, p0, Landroid/support/v7/view/menu/e;->a:Landroid/content/Context;
if-eqz v0, :cond_15
iput-object p1, p0, Landroid/support/v7/view/menu/e;->a:Landroid/content/Context;
iget-object v0, p0, Landroid/support/v7/view/menu/e;->b:Landroid/view/LayoutInflater;
if-nez v0, :cond_15
iget-object v0, p0, Landroid/support/v7/view/menu/e;->a:Landroid/content/Context;
invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/view/menu/e;->b:Landroid/view/LayoutInflater;
goto :goto_15
.end method
.method public a(Landroid/support/v7/view/menu/f;Z)V
.registers 4
iget-object v0, p0, Landroid/support/v7/view/menu/e;->i:Landroid/support/v7/view/menu/l$a;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/view/menu/e;->i:Landroid/support/v7/view/menu/l$a;
invoke-interface {v0, p1, p2}, Landroid/support/v7/view/menu/l$a;->a(Landroid/support/v7/view/menu/f;Z)V
:cond_9
return-void
.end method
.method public a(Landroid/support/v7/view/menu/l$a;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/view/menu/e;->i:Landroid/support/v7/view/menu/l$a;
return-void
.end method
.method public a(Landroid/support/v7/view/menu/f;Landroid/support/v7/view/menu/h;)Z
.registers 4
const/4 v0, 0x0
return v0
.end method
.method public a(Landroid/support/v7/view/menu/p;)Z
.registers 4
invoke-virtual {p1}, Landroid/support/v7/view/menu/p;->hasVisibleItems()Z
move-result v0
if-nez v0, :cond_8
const/4 v0, 0x0
:goto_7
return v0
:cond_8
new-instance v0, Landroid/support/v7/view/menu/g;
invoke-direct {v0, p1}, Landroid/support/v7/view/menu/g;-><init>(Landroid/support/v7/view/menu/f;)V
const/4 v1, 0x0
invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/g;->a(Landroid/os/IBinder;)V
iget-object v0, p0, Landroid/support/v7/view/menu/e;->i:Landroid/support/v7/view/menu/l$a;
if-eqz v0, :cond_1a
iget-object v0, p0, Landroid/support/v7/view/menu/e;->i:Landroid/support/v7/view/menu/l$a;
invoke-interface {v0, p1}, Landroid/support/v7/view/menu/l$a;->a(Landroid/support/v7/view/menu/f;)Z
:cond_1a
const/4 v0, 0x1
goto :goto_7
.end method
.method public b(Z)V
.registers 3
iget-object v0, p0, Landroid/support/v7/view/menu/e;->g:Landroid/support/v7/view/menu/e$a;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/view/menu/e;->g:Landroid/support/v7/view/menu/e$a;
invoke-virtual {v0}, Landroid/support/v7/view/menu/e$a;->notifyDataSetChanged()V
:cond_9
return-void
.end method
.method public b()Z
.registers 2
const/4 v0, 0x0
return v0
.end method
.method public b(Landroid/support/v7/view/menu/f;Landroid/support/v7/view/menu/h;)Z
.registers 4
const/4 v0, 0x0
return v0
.end method
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.registers 9
iget-object v0, p0, Landroid/support/v7/view/menu/e;->c:Landroid/support/v7/view/menu/f;
iget-object v1, p0, Landroid/support/v7/view/menu/e;->g:Landroid/support/v7/view/menu/e$a;
invoke-virtual {v1, p3}, Landroid/support/v7/view/menu/e$a;->a(I)Landroid/support/v7/view/menu/h;
move-result-object v1
const/4 v2, 0x0
invoke-virtual {v0, v1, p0, v2}, Landroid/support/v7/view/menu/f;->a(Landroid/view/MenuItem;Landroid/support/v7/view/menu/l;I)Z
return-void
.end method