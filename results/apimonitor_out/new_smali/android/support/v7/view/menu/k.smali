.class public Landroid/support/v7/view/menu/k;
.super Ljava/lang/Object;
.source "MenuPopupHelper.java"
.implements Landroid/support/v7/view/menu/l;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;
.field static final a:I
.field  b:Z
.field private final c:Landroid/content/Context;
.field private final d:Landroid/view/LayoutInflater;
.field private final e:Landroid/support/v7/view/menu/f;
.field private final f:Landroid/support/v7/view/menu/k$a;
.field private final g:Z
.field private final h:I
.field private final i:I
.field private final j:I
.field private k:Landroid/view/View;
.field private l:Landroid/support/v7/widget/ag;
.field private m:Landroid/view/ViewTreeObserver;
.field private n:Landroid/support/v7/view/menu/l$a;
.field private o:Landroid/view/ViewGroup;
.field private p:Z
.field private q:I
.field private r:I
.method static constructor <clinit>()V
.registers 1
sget v0, Landroid/support/v7/b/a$h;->abc_popup_menu_item_layout:I
sput v0, Landroid/support/v7/view/menu/k;->a:I
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/view/menu/f;Landroid/view/View;)V
.registers 10
const/4 v4, 0x0
sget v5, Landroid/support/v7/b/a$a;->popupMenuStyle:I
move-object v0, p0
move-object v1, p1
move-object v2, p2
move-object v3, p3
invoke-direct/range {v0 .. v5}, Landroid/support/v7/view/menu/k;-><init>(Landroid/content/Context;Landroid/support/v7/view/menu/f;Landroid/view/View;ZI)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/view/menu/f;Landroid/view/View;ZI)V
.registers 13
const/4 v6, 0x0
move-object v0, p0
move-object v1, p1
move-object v2, p2
move-object v3, p3
move v4, p4
move v5, p5
invoke-direct/range {v0 .. v6}, Landroid/support/v7/view/menu/k;-><init>(Landroid/content/Context;Landroid/support/v7/view/menu/f;Landroid/view/View;ZII)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/view/menu/f;Landroid/view/View;ZII)V
.registers 10
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/view/menu/k;->r:I
iput-object p1, p0, Landroid/support/v7/view/menu/k;->c:Landroid/content/Context;
invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/view/menu/k;->d:Landroid/view/LayoutInflater;
iput-object p2, p0, Landroid/support/v7/view/menu/k;->e:Landroid/support/v7/view/menu/f;
new-instance v0, Landroid/support/v7/view/menu/k$a;
iget-object v1, p0, Landroid/support/v7/view/menu/k;->e:Landroid/support/v7/view/menu/f;
invoke-direct {v0, p0, v1}, Landroid/support/v7/view/menu/k$a;-><init>(Landroid/support/v7/view/menu/k;Landroid/support/v7/view/menu/f;)V
iput-object v0, p0, Landroid/support/v7/view/menu/k;->f:Landroid/support/v7/view/menu/k$a;
iput-boolean p4, p0, Landroid/support/v7/view/menu/k;->g:Z
iput p5, p0, Landroid/support/v7/view/menu/k;->i:I
iput p6, p0, Landroid/support/v7/view/menu/k;->j:I
invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
move-result-object v1
iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I
div-int/lit8 v1, v1, 0x2
sget v2, Landroid/support/v7/b/a$d;->abc_config_prefDialogWidth:I
invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I
move-result v0
invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I
move-result v0
iput v0, p0, Landroid/support/v7/view/menu/k;->h:I
iput-object p3, p0, Landroid/support/v7/view/menu/k;->k:Landroid/view/View;
invoke-virtual {p2, p0, p1}, Landroid/support/v7/view/menu/f;->a(Landroid/support/v7/view/menu/l;Landroid/content/Context;)V
return-void
.end method
.method static synthetic a(Landroid/support/v7/view/menu/k;)Z
.registers 2
iget-boolean v0, p0, Landroid/support/v7/view/menu/k;->g:Z
return v0
.end method
.method static synthetic b(Landroid/support/v7/view/menu/k;)Landroid/view/LayoutInflater;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/menu/k;->d:Landroid/view/LayoutInflater;
return-object v0
.end method
.method static synthetic c(Landroid/support/v7/view/menu/k;)Landroid/support/v7/view/menu/f;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/menu/k;->e:Landroid/support/v7/view/menu/f;
return-object v0
.end method
.method private g()I
.registers 12
const/4 v3, 0x0
const/4 v0, 0x0
iget-object v6, p0, Landroid/support/v7/view/menu/k;->f:Landroid/support/v7/view/menu/k$a;
invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v7
invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v8
invoke-interface {v6}, Landroid/widget/ListAdapter;->getCount()I
move-result v9
move v5, v0
move v2, v0
move-object v4, v3
move v1, v0
:goto_14
if-ge v5, v9, :cond_3e
invoke-interface {v6, v5}, Landroid/widget/ListAdapter;->getItemViewType(I)I
move-result v0
if-eq v0, v2, :cond_46
move v2, v0
move-object v0, v3
:goto_1e
iget-object v4, p0, Landroid/support/v7/view/menu/k;->o:Landroid/view/ViewGroup;
if-nez v4, :cond_2b
new-instance v4, Landroid/widget/FrameLayout;
iget-object v10, p0, Landroid/support/v7/view/menu/k;->c:Landroid/content/Context;
invoke-direct {v4, v10}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V
iput-object v4, p0, Landroid/support/v7/view/menu/k;->o:Landroid/view/ViewGroup;
:cond_2b
iget-object v4, p0, Landroid/support/v7/view/menu/k;->o:Landroid/view/ViewGroup;
invoke-interface {v6, v5, v0, v4}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
move-result-object v4
invoke-virtual {v4, v7, v8}, Landroid/view/View;->measure(II)V
invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I
move-result v0
iget v10, p0, Landroid/support/v7/view/menu/k;->h:I
if-lt v0, v10, :cond_3f
iget v1, p0, Landroid/support/v7/view/menu/k;->h:I
:cond_3e
return v1
:cond_3f
if-le v0, v1, :cond_48
:goto_41
add-int/lit8 v1, v5, 0x1
move v5, v1
move v1, v0
goto :goto_14
:cond_46
move-object v0, v4
goto :goto_1e
:cond_48
move v0, v1
goto :goto_41
.end method
.method public a()V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/view/menu/k;->d()Z
move-result v0
if-nez v0, :cond_e
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "MenuPopupHelper cannot be used without an anchor"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_e
return-void
.end method
.method public a(I)V
.registers 2
iput p1, p0, Landroid/support/v7/view/menu/k;->r:I
return-void
.end method
.method public a(Landroid/content/Context;Landroid/support/v7/view/menu/f;)V
.registers 3
return-void
.end method
.method public a(Landroid/support/v7/view/menu/f;Z)V
.registers 4
iget-object v0, p0, Landroid/support/v7/view/menu/k;->e:Landroid/support/v7/view/menu/f;
if-eq p1, v0, :cond_5
:goto_4
:cond_4
return-void
:cond_5
invoke-virtual {p0}, Landroid/support/v7/view/menu/k;->e()V
iget-object v0, p0, Landroid/support/v7/view/menu/k;->n:Landroid/support/v7/view/menu/l$a;
if-eqz v0, :cond_4
iget-object v0, p0, Landroid/support/v7/view/menu/k;->n:Landroid/support/v7/view/menu/l$a;
invoke-interface {v0, p1, p2}, Landroid/support/v7/view/menu/l$a;->a(Landroid/support/v7/view/menu/f;Z)V
goto :goto_4
.end method
.method public a(Landroid/support/v7/view/menu/l$a;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/view/menu/k;->n:Landroid/support/v7/view/menu/l$a;
return-void
.end method
.method public a(Landroid/view/View;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/view/menu/k;->k:Landroid/view/View;
return-void
.end method
.method public a(Z)V
.registers 2
iput-boolean p1, p0, Landroid/support/v7/view/menu/k;->b:Z
return-void
.end method
.method public a(Landroid/support/v7/view/menu/f;Landroid/support/v7/view/menu/h;)Z
.registers 4
const/4 v0, 0x0
return v0
.end method
.method public a(Landroid/support/v7/view/menu/p;)Z
.registers 9
const/4 v1, 0x1
const/4 v2, 0x0
invoke-virtual {p1}, Landroid/support/v7/view/menu/p;->hasVisibleItems()Z
move-result v0
if-eqz v0, :cond_44
new-instance v3, Landroid/support/v7/view/menu/k;
iget-object v0, p0, Landroid/support/v7/view/menu/k;->c:Landroid/content/Context;
iget-object v4, p0, Landroid/support/v7/view/menu/k;->k:Landroid/view/View;
invoke-direct {v3, v0, p1, v4}, Landroid/support/v7/view/menu/k;-><init>(Landroid/content/Context;Landroid/support/v7/view/menu/f;Landroid/view/View;)V
iget-object v0, p0, Landroid/support/v7/view/menu/k;->n:Landroid/support/v7/view/menu/l$a;
invoke-virtual {v3, v0}, Landroid/support/v7/view/menu/k;->a(Landroid/support/v7/view/menu/l$a;)V
invoke-virtual {p1}, Landroid/support/v7/view/menu/p;->size()I
move-result v4
move v0, v2
:goto_1b
if-ge v0, v4, :cond_46
invoke-virtual {p1, v0}, Landroid/support/v7/view/menu/p;->getItem(I)Landroid/view/MenuItem;
move-result-object v5
invoke-interface {v5}, Landroid/view/MenuItem;->isVisible()Z
move-result v6
if-eqz v6, :cond_41
invoke-interface {v5}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;
move-result-object v5
if-eqz v5, :cond_41
move v0, v1
:goto_2e
invoke-virtual {v3, v0}, Landroid/support/v7/view/menu/k;->a(Z)V
invoke-virtual {v3}, Landroid/support/v7/view/menu/k;->d()Z
move-result v0
if-eqz v0, :cond_44
iget-object v0, p0, Landroid/support/v7/view/menu/k;->n:Landroid/support/v7/view/menu/l$a;
if-eqz v0, :cond_40
iget-object v0, p0, Landroid/support/v7/view/menu/k;->n:Landroid/support/v7/view/menu/l$a;
invoke-interface {v0, p1}, Landroid/support/v7/view/menu/l$a;->a(Landroid/support/v7/view/menu/f;)Z
:goto_40
:cond_40
return v1
:cond_41
add-int/lit8 v0, v0, 0x1
goto :goto_1b
:cond_44
move v1, v2
goto :goto_40
:cond_46
move v0, v2
goto :goto_2e
.end method
.method public b(Z)V
.registers 3
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v7/view/menu/k;->p:Z
iget-object v0, p0, Landroid/support/v7/view/menu/k;->f:Landroid/support/v7/view/menu/k$a;
if-eqz v0, :cond_c
iget-object v0, p0, Landroid/support/v7/view/menu/k;->f:Landroid/support/v7/view/menu/k$a;
invoke-virtual {v0}, Landroid/support/v7/view/menu/k$a;->notifyDataSetChanged()V
:cond_c
return-void
.end method
.method public b()Z
.registers 2
const/4 v0, 0x0
return v0
.end method
.method public b(Landroid/support/v7/view/menu/f;Landroid/support/v7/view/menu/h;)Z
.registers 4
const/4 v0, 0x0
return v0
.end method
.method public c()Landroid/support/v7/widget/ag;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
return-object v0
.end method
.method public d()Z
.registers 8
const/4 v0, 0x0
const/4 v1, 0x1
new-instance v2, Landroid/support/v7/widget/ag;
iget-object v3, p0, Landroid/support/v7/view/menu/k;->c:Landroid/content/Context;
const/4 v4, 0x0
iget v5, p0, Landroid/support/v7/view/menu/k;->i:I
iget v6, p0, Landroid/support/v7/view/menu/k;->j:I
invoke-direct {v2, v3, v4, v5, v6}, Landroid/support/v7/widget/ag;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
iput-object v2, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
iget-object v2, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
invoke-virtual {v2, p0}, Landroid/support/v7/widget/ag;->a(Landroid/widget/PopupWindow$OnDismissListener;)V
iget-object v2, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
invoke-virtual {v2, p0}, Landroid/support/v7/widget/ag;->a(Landroid/widget/AdapterView$OnItemClickListener;)V
iget-object v2, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
iget-object v3, p0, Landroid/support/v7/view/menu/k;->f:Landroid/support/v7/view/menu/k$a;
invoke-virtual {v2, v3}, Landroid/support/v7/widget/ag;->a(Landroid/widget/ListAdapter;)V
iget-object v2, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
invoke-virtual {v2, v1}, Landroid/support/v7/widget/ag;->a(Z)V
iget-object v2, p0, Landroid/support/v7/view/menu/k;->k:Landroid/view/View;
if-eqz v2, :cond_70
iget-object v3, p0, Landroid/support/v7/view/menu/k;->m:Landroid/view/ViewTreeObserver;
if-nez v3, :cond_2f
move v0, v1
:cond_2f
invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;
move-result-object v3
iput-object v3, p0, Landroid/support/v7/view/menu/k;->m:Landroid/view/ViewTreeObserver;
if-eqz v0, :cond_3c
iget-object v0, p0, Landroid/support/v7/view/menu/k;->m:Landroid/view/ViewTreeObserver;
invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
:cond_3c
iget-object v0, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
invoke-virtual {v0, v2}, Landroid/support/v7/widget/ag;->a(Landroid/view/View;)V
iget-object v0, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
iget v2, p0, Landroid/support/v7/view/menu/k;->r:I
invoke-virtual {v0, v2}, Landroid/support/v7/widget/ag;->d(I)V
iget-boolean v0, p0, Landroid/support/v7/view/menu/k;->p:Z
if-nez v0, :cond_54
invoke-direct {p0}, Landroid/support/v7/view/menu/k;->g()I
move-result v0
iput v0, p0, Landroid/support/v7/view/menu/k;->q:I
iput-boolean v1, p0, Landroid/support/v7/view/menu/k;->p:Z
:cond_54
iget-object v0, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
iget v2, p0, Landroid/support/v7/view/menu/k;->q:I
invoke-virtual {v0, v2}, Landroid/support/v7/widget/ag;->f(I)V
iget-object v0, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
const/4 v2, 0x2
invoke-virtual {v0, v2}, Landroid/support/v7/widget/ag;->g(I)V
iget-object v0, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
invoke-virtual {v0}, Landroid/support/v7/widget/ag;->c()V
iget-object v0, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
invoke-virtual {v0}, Landroid/support/v7/widget/ag;->m()Landroid/widget/ListView;
move-result-object v0
invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V
:goto_6f
return v1
:cond_70
move v1, v0
goto :goto_6f
.end method
.method public e()V
.registers 2
invoke-virtual {p0}, Landroid/support/v7/view/menu/k;->f()Z
move-result v0
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
invoke-virtual {v0}, Landroid/support/v7/widget/ag;->i()V
:cond_b
return-void
.end method
.method public f()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
if-eqz v0, :cond_e
iget-object v0, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
invoke-virtual {v0}, Landroid/support/v7/widget/ag;->k()Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method public onDismiss()V
.registers 3
const/4 v1, 0x0
iput-object v1, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
iget-object v0, p0, Landroid/support/v7/view/menu/k;->e:Landroid/support/v7/view/menu/f;
invoke-virtual {v0}, Landroid/support/v7/view/menu/f;->close()V
iget-object v0, p0, Landroid/support/v7/view/menu/k;->m:Landroid/view/ViewTreeObserver;
if-eqz v0, :cond_23
iget-object v0, p0, Landroid/support/v7/view/menu/k;->m:Landroid/view/ViewTreeObserver;
invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z
move-result v0
if-nez v0, :cond_1c
iget-object v0, p0, Landroid/support/v7/view/menu/k;->k:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/view/menu/k;->m:Landroid/view/ViewTreeObserver;
:cond_1c
iget-object v0, p0, Landroid/support/v7/view/menu/k;->m:Landroid/view/ViewTreeObserver;
invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
iput-object v1, p0, Landroid/support/v7/view/menu/k;->m:Landroid/view/ViewTreeObserver;
:cond_23
return-void
.end method
.method public onGlobalLayout()V
.registers 2
invoke-virtual {p0}, Landroid/support/v7/view/menu/k;->f()Z
move-result v0
if-eqz v0, :cond_13
iget-object v0, p0, Landroid/support/v7/view/menu/k;->k:Landroid/view/View;
if-eqz v0, :cond_10
invoke-virtual {v0}, Landroid/view/View;->isShown()Z
move-result v0
if-nez v0, :cond_14
:cond_10
invoke-virtual {p0}, Landroid/support/v7/view/menu/k;->e()V
:cond_13
:goto_13
return-void
:cond_14
invoke-virtual {p0}, Landroid/support/v7/view/menu/k;->f()Z
move-result v0
if-eqz v0, :cond_13
iget-object v0, p0, Landroid/support/v7/view/menu/k;->l:Landroid/support/v7/widget/ag;
invoke-virtual {v0}, Landroid/support/v7/widget/ag;->c()V
goto :goto_13
.end method
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.registers 9
iget-object v0, p0, Landroid/support/v7/view/menu/k;->f:Landroid/support/v7/view/menu/k$a;
invoke-static {v0}, Landroid/support/v7/view/menu/k$a;->a(Landroid/support/v7/view/menu/k$a;)Landroid/support/v7/view/menu/f;
move-result-object v1
invoke-virtual {v0, p3}, Landroid/support/v7/view/menu/k$a;->a(I)Landroid/support/v7/view/menu/h;
move-result-object v0
const/4 v2, 0x0
invoke-virtual {v1, v0, v2}, Landroid/support/v7/view/menu/f;->a(Landroid/view/MenuItem;I)Z
return-void
.end method
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
.registers 6
const/4 v0, 0x1
invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I
move-result v1
if-ne v1, v0, :cond_f
const/16 v1, 0x52
if-ne p2, v1, :cond_f
invoke-virtual {p0}, Landroid/support/v7/view/menu/k;->e()V
:goto_e
return v0
:cond_f
const/4 v0, 0x0
goto :goto_e
.end method