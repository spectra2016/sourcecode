.class public final Landroid/support/v7/view/menu/ExpandedMenuView;
.super Landroid/widget/ListView;
.source "ExpandedMenuView.java"
.implements Landroid/support/v7/view/menu/f$b;
.implements Landroid/support/v7/view/menu/m;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.field private static final a:[I
.field private b:Landroid/support/v7/view/menu/f;
.field private c:I
.method static constructor <clinit>()V
.registers 1
const/4 v0, 0x2
new-array v0, v0, [I
fill-array-data v0, :array_a
sput-object v0, Landroid/support/v7/view/menu/ExpandedMenuView;->a:[I
return-void
nop
:array_a
.array-data 0x4
0xd4t 0x0t 0x1t 0x1t
0x29t 0x1t 0x1t 0x1t
.end array-data
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 4
const v0, 0x1010074
invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/view/menu/ExpandedMenuView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.registers 8
const/4 v3, 0x1
const/4 v2, 0x0
invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
invoke-virtual {p0, p0}, Landroid/support/v7/view/menu/ExpandedMenuView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
sget-object v0, Landroid/support/v7/view/menu/ExpandedMenuView;->a:[I
invoke-static {p1, p2, v0, p3, v2}, Landroid/support/v7/widget/ar;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/widget/ar;
move-result-object v0
invoke-virtual {v0, v2}, Landroid/support/v7/widget/ar;->f(I)Z
move-result v1
if-eqz v1, :cond_1b
invoke-virtual {v0, v2}, Landroid/support/v7/widget/ar;->a(I)Landroid/graphics/drawable/Drawable;
move-result-object v1
invoke-virtual {p0, v1}, Landroid/support/v7/view/menu/ExpandedMenuView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
:cond_1b
invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->f(I)Z
move-result v1
if-eqz v1, :cond_28
invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->a(I)Landroid/graphics/drawable/Drawable;
move-result-object v1
invoke-virtual {p0, v1}, Landroid/support/v7/view/menu/ExpandedMenuView;->setDivider(Landroid/graphics/drawable/Drawable;)V
:cond_28
invoke-virtual {v0}, Landroid/support/v7/widget/ar;->a()V
return-void
.end method
.method public a(Landroid/support/v7/view/menu/f;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/view/menu/ExpandedMenuView;->b:Landroid/support/v7/view/menu/f;
return-void
.end method
.method public a(Landroid/support/v7/view/menu/h;)Z
.registers 4
iget-object v0, p0, Landroid/support/v7/view/menu/ExpandedMenuView;->b:Landroid/support/v7/view/menu/f;
const/4 v1, 0x0
invoke-virtual {v0, p1, v1}, Landroid/support/v7/view/menu/f;->a(Landroid/view/MenuItem;I)Z
move-result v0
return v0
.end method
.method public getWindowAnimations()I
.registers 2
iget v0, p0, Landroid/support/v7/view/menu/ExpandedMenuView;->c:I
return v0
.end method
.method protected onDetachedFromWindow()V
.registers 2
invoke-super {p0}, Landroid/widget/ListView;->onDetachedFromWindow()V
const/4 v0, 0x0
invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/ExpandedMenuView;->setChildrenDrawingCacheEnabled(Z)V
return-void
.end method
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.registers 7
invoke-virtual {p0}, Landroid/support/v7/view/menu/ExpandedMenuView;->getAdapter()Landroid/widget/ListAdapter;
move-result-object v0
invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v7/view/menu/h;
invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/ExpandedMenuView;->a(Landroid/support/v7/view/menu/h;)Z
return-void
.end method