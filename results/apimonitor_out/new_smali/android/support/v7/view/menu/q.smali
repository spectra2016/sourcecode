.class  Landroid/support/v7/view/menu/q;
.super Landroid/support/v7/view/menu/o;
.source "SubMenuWrapperICS.java"
.implements Landroid/view/SubMenu;
.method constructor <init>(Landroid/content/Context;Landroid/support/v4/c/a/c;)V
.registers 3
invoke-direct {p0, p1, p2}, Landroid/support/v7/view/menu/o;-><init>(Landroid/content/Context;Landroid/support/v4/c/a/a;)V
return-void
.end method
.method public b()Landroid/support/v4/c/a/c;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/menu/q;->b:Ljava/lang/Object;
check-cast v0, Landroid/support/v4/c/a/c;
return-object v0
.end method
.method public clearHeader()V
.registers 2
invoke-virtual {p0}, Landroid/support/v7/view/menu/q;->b()Landroid/support/v4/c/a/c;
move-result-object v0
invoke-interface {v0}, Landroid/support/v4/c/a/c;->clearHeader()V
return-void
.end method
.method public getItem()Landroid/view/MenuItem;
.registers 2
invoke-virtual {p0}, Landroid/support/v7/view/menu/q;->b()Landroid/support/v4/c/a/c;
move-result-object v0
invoke-interface {v0}, Landroid/support/v4/c/a/c;->getItem()Landroid/view/MenuItem;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/q;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
move-result-object v0
return-object v0
.end method
.method public setHeaderIcon(I)Landroid/view/SubMenu;
.registers 3
invoke-virtual {p0}, Landroid/support/v7/view/menu/q;->b()Landroid/support/v4/c/a/c;
move-result-object v0
invoke-interface {v0, p1}, Landroid/support/v4/c/a/c;->setHeaderIcon(I)Landroid/view/SubMenu;
return-object p0
.end method
.method public setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
.registers 3
invoke-virtual {p0}, Landroid/support/v7/view/menu/q;->b()Landroid/support/v4/c/a/c;
move-result-object v0
invoke-interface {v0, p1}, Landroid/support/v4/c/a/c;->setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
return-object p0
.end method
.method public setHeaderTitle(I)Landroid/view/SubMenu;
.registers 3
invoke-virtual {p0}, Landroid/support/v7/view/menu/q;->b()Landroid/support/v4/c/a/c;
move-result-object v0
invoke-interface {v0, p1}, Landroid/support/v4/c/a/c;->setHeaderTitle(I)Landroid/view/SubMenu;
return-object p0
.end method
.method public setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
.registers 3
invoke-virtual {p0}, Landroid/support/v7/view/menu/q;->b()Landroid/support/v4/c/a/c;
move-result-object v0
invoke-interface {v0, p1}, Landroid/support/v4/c/a/c;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
return-object p0
.end method
.method public setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
.registers 3
invoke-virtual {p0}, Landroid/support/v7/view/menu/q;->b()Landroid/support/v4/c/a/c;
move-result-object v0
invoke-interface {v0, p1}, Landroid/support/v4/c/a/c;->setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
return-object p0
.end method
.method public setIcon(I)Landroid/view/SubMenu;
.registers 3
invoke-virtual {p0}, Landroid/support/v7/view/menu/q;->b()Landroid/support/v4/c/a/c;
move-result-object v0
invoke-interface {v0, p1}, Landroid/support/v4/c/a/c;->setIcon(I)Landroid/view/SubMenu;
return-object p0
.end method
.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
.registers 3
invoke-virtual {p0}, Landroid/support/v7/view/menu/q;->b()Landroid/support/v4/c/a/c;
move-result-object v0
invoke-interface {v0, p1}, Landroid/support/v4/c/a/c;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
return-object p0
.end method