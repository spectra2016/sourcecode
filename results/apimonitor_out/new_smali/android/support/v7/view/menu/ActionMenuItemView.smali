.class public Landroid/support/v7/view/menu/ActionMenuItemView;
.super Landroid/support/v7/widget/aa;
.source "ActionMenuItemView.java"
.implements Landroid/support/v7/view/menu/m$a;
.implements Landroid/support/v7/widget/ActionMenuView$a;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.field private a:Landroid/support/v7/view/menu/h;
.field private b:Ljava/lang/CharSequence;
.field private c:Landroid/graphics/drawable/Drawable;
.field private d:Landroid/support/v7/view/menu/f$b;
.field private e:Landroid/support/v7/widget/ag$b;
.field private f:Landroid/support/v7/view/menu/ActionMenuItemView$b;
.field private g:Z
.field private h:Z
.field private i:I
.field private j:I
.field private k:I
.method public constructor <init>(Landroid/content/Context;)V
.registers 3
const/4 v0, 0x0
invoke-direct {p0, p1, v0}, Landroid/support/v7/view/menu/ActionMenuItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 4
const/4 v0, 0x0
invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/view/menu/ActionMenuItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.registers 8
const/4 v3, 0x0
invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
sget v1, Landroid/support/v7/b/a$b;->abc_config_allowActionMenuItemTextWithIcon:I
invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
move-result v1
iput-boolean v1, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->g:Z
sget-object v1, Landroid/support/v7/b/a$k;->ActionMenuItemView:[I
invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
move-result-object v1
sget v2, Landroid/support/v7/b/a$k;->ActionMenuItemView_android_minWidth:I
invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I
move-result v2
iput v2, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->i:I
invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V
invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
move-result-object v0
iget v0, v0, Landroid/util/DisplayMetrics;->density:F
const/high16 v1, 0x4200
mul-float/2addr v0, v1
const/high16 v1, 0x3f00
add-float/2addr v0, v1
float-to-int v0, v0
iput v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->k:I
invoke-virtual {p0, p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V
invoke-virtual {p0, p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
const/4 v0, -0x1
iput v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->j:I
return-void
.end method
.method static synthetic a(Landroid/support/v7/view/menu/ActionMenuItemView;)Landroid/support/v7/view/menu/ActionMenuItemView$b;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->f:Landroid/support/v7/view/menu/ActionMenuItemView$b;
return-object v0
.end method
.method static synthetic b(Landroid/support/v7/view/menu/ActionMenuItemView;)Landroid/support/v7/view/menu/f$b;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->d:Landroid/support/v7/view/menu/f$b;
return-object v0
.end method
.method static synthetic c(Landroid/support/v7/view/menu/ActionMenuItemView;)Landroid/support/v7/view/menu/h;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->a:Landroid/support/v7/view/menu/h;
return-object v0
.end method
.method private e()V
.registers 5
const/4 v1, 0x1
const/4 v2, 0x0
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->b:Ljava/lang/CharSequence;
invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-nez v0, :cond_29
move v0, v1
:goto_b
iget-object v3, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->c:Landroid/graphics/drawable/Drawable;
if-eqz v3, :cond_1f
iget-object v3, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->a:Landroid/support/v7/view/menu/h;
invoke-virtual {v3}, Landroid/support/v7/view/menu/h;->m()Z
move-result v3
if-eqz v3, :cond_20
iget-boolean v3, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->g:Z
if-nez v3, :cond_1f
iget-boolean v3, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->h:Z
if-eqz v3, :cond_20
:cond_1f
move v2, v1
:cond_20
and-int/2addr v0, v2
if-eqz v0, :cond_2b
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->b:Ljava/lang/CharSequence;
:goto_25
invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/ActionMenuItemView;->setText(Ljava/lang/CharSequence;)V
return-void
:cond_29
move v0, v2
goto :goto_b
:cond_2b
const/4 v0, 0x0
goto :goto_25
.end method
.method public a(Landroid/support/v7/view/menu/h;I)V
.registers 4
iput-object p1, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->a:Landroid/support/v7/view/menu/h;
invoke-virtual {p1}, Landroid/support/v7/view/menu/h;->getIcon()Landroid/graphics/drawable/Drawable;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/ActionMenuItemView;->setIcon(Landroid/graphics/drawable/Drawable;)V
invoke-virtual {p1, p0}, Landroid/support/v7/view/menu/h;->a(Landroid/support/v7/view/menu/m$a;)Ljava/lang/CharSequence;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/ActionMenuItemView;->setTitle(Ljava/lang/CharSequence;)V
invoke-virtual {p1}, Landroid/support/v7/view/menu/h;->getItemId()I
move-result v0
invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/ActionMenuItemView;->setId(I)V
invoke-virtual {p1}, Landroid/support/v7/view/menu/h;->isVisible()Z
move-result v0
if-eqz v0, :cond_3a
const/4 v0, 0x0
:goto_1e
invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/ActionMenuItemView;->setVisibility(I)V
invoke-virtual {p1}, Landroid/support/v7/view/menu/h;->isEnabled()Z
move-result v0
invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/ActionMenuItemView;->setEnabled(Z)V
invoke-virtual {p1}, Landroid/support/v7/view/menu/h;->hasSubMenu()Z
move-result v0
if-eqz v0, :cond_39
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->e:Landroid/support/v7/widget/ag$b;
if-nez v0, :cond_39
new-instance v0, Landroid/support/v7/view/menu/ActionMenuItemView$a;
invoke-direct {v0, p0}, Landroid/support/v7/view/menu/ActionMenuItemView$a;-><init>(Landroid/support/v7/view/menu/ActionMenuItemView;)V
iput-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->e:Landroid/support/v7/widget/ag$b;
:cond_39
return-void
:cond_3a
const/16 v0, 0x8
goto :goto_1e
.end method
.method public a()Z
.registers 2
const/4 v0, 0x1
return v0
.end method
.method public b()Z
.registers 2
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->getText()Ljava/lang/CharSequence;
move-result-object v0
invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-nez v0, :cond_c
const/4 v0, 0x1
:goto_b
return v0
:cond_c
const/4 v0, 0x0
goto :goto_b
.end method
.method public c()Z
.registers 2
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->b()Z
move-result v0
if-eqz v0, :cond_10
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->a:Landroid/support/v7/view/menu/h;
invoke-virtual {v0}, Landroid/support/v7/view/menu/h;->getIcon()Landroid/graphics/drawable/Drawable;
move-result-object v0
if-nez v0, :cond_10
const/4 v0, 0x1
:goto_f
return v0
:cond_10
const/4 v0, 0x0
goto :goto_f
.end method
.method public d()Z
.registers 2
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->b()Z
move-result v0
return v0
.end method
.method public getItemData()Landroid/support/v7/view/menu/h;
.registers 2
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->a:Landroid/support/v7/view/menu/h;
return-object v0
.end method
.method public onClick(Landroid/view/View;)V
.registers 4
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->d:Landroid/support/v7/view/menu/f$b;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->d:Landroid/support/v7/view/menu/f$b;
iget-object v1, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->a:Landroid/support/v7/view/menu/h;
invoke-interface {v0, v1}, Landroid/support/v7/view/menu/f$b;->a(Landroid/support/v7/view/menu/h;)Z
:cond_b
return-void
.end method
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
.registers 4
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x8
if-lt v0, v1, :cond_9
invoke-super {p0, p1}, Landroid/support/v7/widget/aa;->onConfigurationChanged(Landroid/content/res/Configuration;)V
:cond_9
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->getContext()Landroid/content/Context;
move-result-object v0
invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
sget v1, Landroid/support/v7/b/a$b;->abc_config_allowActionMenuItemTextWithIcon:I
invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
move-result v0
iput-boolean v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->g:Z
invoke-direct {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->e()V
return-void
.end method
.method public onLongClick(Landroid/view/View;)Z
.registers 11
const/4 v2, 0x1
const/4 v1, 0x0
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->b()Z
move-result v0
if-eqz v0, :cond_a
move v0, v1
:goto_9
return v0
:cond_a
const/4 v0, 0x2
new-array v3, v0, [I
new-instance v4, Landroid/graphics/Rect;
invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V
invoke-virtual {p0, v3}, Landroid/support/v7/view/menu/ActionMenuItemView;->getLocationOnScreen([I)V
invoke-virtual {p0, v4}, Landroid/support/v7/view/menu/ActionMenuItemView;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->getContext()Landroid/content/Context;
move-result-object v5
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->getWidth()I
move-result v0
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->getHeight()I
move-result v6
aget v7, v3, v2
div-int/lit8 v8, v6, 0x2
add-int/2addr v7, v8
aget v8, v3, v1
div-int/lit8 v0, v0, 0x2
add-int/2addr v0, v8
invoke-static {p1}, Landroid/support/v4/f/af;->d(Landroid/view/View;)I
move-result v8
if-nez v8, :cond_40
invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v8
invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
move-result-object v8
iget v8, v8, Landroid/util/DisplayMetrics;->widthPixels:I
sub-int v0, v8, v0
:cond_40
iget-object v8, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->a:Landroid/support/v7/view/menu/h;
invoke-virtual {v8}, Landroid/support/v7/view/menu/h;->getTitle()Ljava/lang/CharSequence;
move-result-object v8
invoke-static {v5, v8, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
move-result-object v5
invoke-virtual {v4}, Landroid/graphics/Rect;->height()I
move-result v8
if-ge v7, v8, :cond_61
const v1, 0x800035
aget v3, v3, v2
add-int/2addr v3, v6
iget v4, v4, Landroid/graphics/Rect;->top:I
sub-int/2addr v3, v4
invoke-virtual {v5, v1, v0, v3}, Landroid/widget/Toast;->setGravity(III)V
:goto_5c
invoke-virtual {v5}, Landroid/widget/Toast;->show()V
move v0, v2
goto :goto_9
:cond_61
const/16 v0, 0x51
invoke-virtual {v5, v0, v1, v6}, Landroid/widget/Toast;->setGravity(III)V
goto :goto_5c
.end method
.method protected onMeasure(II)V
.registers 9
const/high16 v5, 0x4000
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->b()Z
move-result v1
if-eqz v1, :cond_1d
iget v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->j:I
if-ltz v0, :cond_1d
iget v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->j:I
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->getPaddingTop()I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->getPaddingRight()I
move-result v3
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->getPaddingBottom()I
move-result v4
invoke-super {p0, v0, v2, v3, v4}, Landroid/support/v7/widget/aa;->setPadding(IIII)V
:cond_1d
invoke-super {p0, p1, p2}, Landroid/support/v7/widget/aa;->onMeasure(II)V
invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I
move-result v2
invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I
move-result v0
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->getMeasuredWidth()I
move-result v3
const/high16 v4, -0x8000
if-ne v2, v4, :cond_6c
iget v4, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->i:I
invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I
move-result v0
:goto_36
if-eq v2, v5, :cond_45
iget v2, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->i:I
if-lez v2, :cond_45
if-ge v3, v0, :cond_45
invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v0
invoke-super {p0, v0, p2}, Landroid/support/v7/widget/aa;->onMeasure(II)V
:cond_45
if-nez v1, :cond_6b
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->c:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_6b
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->getMeasuredWidth()I
move-result v0
iget-object v1, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->c:Landroid/graphics/drawable/Drawable;
invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;
move-result-object v1
invoke-virtual {v1}, Landroid/graphics/Rect;->width()I
move-result v1
sub-int/2addr v0, v1
div-int/lit8 v0, v0, 0x2
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->getPaddingTop()I
move-result v1
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->getPaddingRight()I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->getPaddingBottom()I
move-result v3
invoke-super {p0, v0, v1, v2, v3}, Landroid/support/v7/widget/aa;->setPadding(IIII)V
:cond_6b
return-void
:cond_6c
iget v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->i:I
goto :goto_36
.end method
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
.registers 3
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->a:Landroid/support/v7/view/menu/h;
invoke-virtual {v0}, Landroid/support/v7/view/menu/h;->hasSubMenu()Z
move-result v0
if-eqz v0, :cond_16
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->e:Landroid/support/v7/widget/ag$b;
if-eqz v0, :cond_16
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->e:Landroid/support/v7/widget/ag$b;
invoke-virtual {v0, p0, p1}, Landroid/support/v7/widget/ag$b;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
move-result v0
if-eqz v0, :cond_16
const/4 v0, 0x1
:goto_15
return v0
:cond_16
invoke-super {p0, p1}, Landroid/support/v7/widget/aa;->onTouchEvent(Landroid/view/MotionEvent;)Z
move-result v0
goto :goto_15
.end method
.method public setCheckable(Z)V
.registers 2
return-void
.end method
.method public setChecked(Z)V
.registers 2
return-void
.end method
.method public setExpandedFormat(Z)V
.registers 3
iget-boolean v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->h:Z
if-eq v0, p1, :cond_f
iput-boolean p1, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->h:Z
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->a:Landroid/support/v7/view/menu/h;
if-eqz v0, :cond_f
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->a:Landroid/support/v7/view/menu/h;
invoke-virtual {v0}, Landroid/support/v7/view/menu/h;->h()V
:cond_f
return-void
.end method
.method public setIcon(Landroid/graphics/drawable/Drawable;)V
.registers 7
const/4 v4, 0x0
const/4 v3, 0x0
iput-object p1, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->c:Landroid/graphics/drawable/Drawable;
if-eqz p1, :cond_2d
invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I
move-result v1
invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I
move-result v0
iget v2, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->k:I
if-le v1, v2, :cond_1c
iget v2, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->k:I
int-to-float v2, v2
int-to-float v1, v1
div-float/2addr v2, v1
iget v1, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->k:I
int-to-float v0, v0
mul-float/2addr v0, v2
float-to-int v0, v0
:cond_1c
iget v2, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->k:I
if-le v0, v2, :cond_2a
iget v2, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->k:I
int-to-float v2, v2
int-to-float v0, v0
div-float/2addr v2, v0
iget v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->k:I
int-to-float v1, v1
mul-float/2addr v1, v2
float-to-int v1, v1
:cond_2a
invoke-virtual {p1, v4, v4, v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V
:cond_2d
invoke-virtual {p0, p1, v3, v3, v3}, Landroid/support/v7/view/menu/ActionMenuItemView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
invoke-direct {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->e()V
return-void
.end method
.method public setItemInvoker(Landroid/support/v7/view/menu/f$b;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->d:Landroid/support/v7/view/menu/f$b;
return-void
.end method
.method public setPadding(IIII)V
.registers 5
iput p1, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->j:I
invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/aa;->setPadding(IIII)V
return-void
.end method
.method public setPopupCallback(Landroid/support/v7/view/menu/ActionMenuItemView$b;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->f:Landroid/support/v7/view/menu/ActionMenuItemView$b;
return-void
.end method
.method public setTitle(Ljava/lang/CharSequence;)V
.registers 3
iput-object p1, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->b:Ljava/lang/CharSequence;
iget-object v0, p0, Landroid/support/v7/view/menu/ActionMenuItemView;->b:Ljava/lang/CharSequence;
invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/ActionMenuItemView;->setContentDescription(Ljava/lang/CharSequence;)V
invoke-direct {p0}, Landroid/support/v7/view/menu/ActionMenuItemView;->e()V
return-void
.end method