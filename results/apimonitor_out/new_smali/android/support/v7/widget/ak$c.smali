.class  Landroid/support/v7/widget/ak$c;
.super Landroid/support/v7/widget/af;
.source "ScrollingTabContainerView.java"
.implements Landroid/view/View$OnLongClickListener;
.field final synthetic a:Landroid/support/v7/widget/ak;
.field private final b:[I
.field private c:Landroid/support/v7/a/a$c;
.field private d:Landroid/widget/TextView;
.field private e:Landroid/widget/ImageView;
.field private f:Landroid/view/View;
.method public constructor <init>(Landroid/support/v7/widget/ak;Landroid/content/Context;Landroid/support/v7/a/a$c;Z)V
.registers 9
const/4 v3, 0x0
const/4 v2, 0x0
iput-object p1, p0, Landroid/support/v7/widget/ak$c;->a:Landroid/support/v7/widget/ak;
sget v0, Landroid/support/v7/b/a$a;->actionBarTabStyle:I
invoke-direct {p0, p2, v3, v0}, Landroid/support/v7/widget/af;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
const/4 v0, 0x1
new-array v0, v0, [I
const v1, 0x10100d4
aput v1, v0, v2
iput-object v0, p0, Landroid/support/v7/widget/ak$c;->b:[I
iput-object p3, p0, Landroid/support/v7/widget/ak$c;->c:Landroid/support/v7/a/a$c;
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->b:[I
sget v1, Landroid/support/v7/b/a$a;->actionBarTabStyle:I
invoke-static {p2, v3, v0, v1, v2}, Landroid/support/v7/widget/ar;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/widget/ar;
move-result-object v0
invoke-virtual {v0, v2}, Landroid/support/v7/widget/ar;->f(I)Z
move-result v1
if-eqz v1, :cond_2a
invoke-virtual {v0, v2}, Landroid/support/v7/widget/ar;->a(I)Landroid/graphics/drawable/Drawable;
move-result-object v1
invoke-virtual {p0, v1}, Landroid/support/v7/widget/ak$c;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
:cond_2a
invoke-virtual {v0}, Landroid/support/v7/widget/ar;->a()V
if-eqz p4, :cond_35
const v0, 0x800013
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ak$c;->setGravity(I)V
:cond_35
invoke-virtual {p0}, Landroid/support/v7/widget/ak$c;->a()V
return-void
.end method
.method public a()V
.registers 11
const/16 v9, 0x10
const/16 v6, 0x8
const/4 v8, -0x2
const/4 v1, 0x0
const/4 v7, 0x0
iget-object v2, p0, Landroid/support/v7/widget/ak$c;->c:Landroid/support/v7/a/a$c;
invoke-virtual {v2}, Landroid/support/v7/a/a$c;->c()Landroid/view/View;
move-result-object v3
if-eqz v3, :cond_39
invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;
move-result-object v0
if-eq v0, p0, :cond_1f
if-eqz v0, :cond_1c
check-cast v0, Landroid/view/ViewGroup;
invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
:cond_1c
invoke-virtual {p0, v3}, Landroid/support/v7/widget/ak$c;->addView(Landroid/view/View;)V
:cond_1f
iput-object v3, p0, Landroid/support/v7/widget/ak$c;->f:Landroid/view/View;
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->d:Landroid/widget/TextView;
if-eqz v0, :cond_2a
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->d:Landroid/widget/TextView;
invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V
:cond_2a
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->e:Landroid/widget/ImageView;
if-eqz v0, :cond_38
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->e:Landroid/widget/ImageView;
invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->e:Landroid/widget/ImageView;
invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
:cond_38
:goto_38
return-void
:cond_39
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->f:Landroid/view/View;
if-eqz v0, :cond_44
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->f:Landroid/view/View;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ak$c;->removeView(Landroid/view/View;)V
iput-object v7, p0, Landroid/support/v7/widget/ak$c;->f:Landroid/view/View;
:cond_44
invoke-virtual {v2}, Landroid/support/v7/a/a$c;->a()Landroid/graphics/drawable/Drawable;
move-result-object v0
invoke-virtual {v2}, Landroid/support/v7/a/a$c;->b()Ljava/lang/CharSequence;
move-result-object v3
if-eqz v0, :cond_c8
iget-object v4, p0, Landroid/support/v7/widget/ak$c;->e:Landroid/widget/ImageView;
if-nez v4, :cond_6a
new-instance v4, Landroid/widget/ImageView;
invoke-virtual {p0}, Landroid/support/v7/widget/ak$c;->getContext()Landroid/content/Context;
move-result-object v5
invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V
new-instance v5, Landroid/support/v7/widget/af$a;
invoke-direct {v5, v8, v8}, Landroid/support/v7/widget/af$a;-><init>(II)V
iput v9, v5, Landroid/support/v7/widget/af$a;->h:I
invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
invoke-virtual {p0, v4, v1}, Landroid/support/v7/widget/ak$c;->addView(Landroid/view/View;I)V
iput-object v4, p0, Landroid/support/v7/widget/ak$c;->e:Landroid/widget/ImageView;
:cond_6a
iget-object v4, p0, Landroid/support/v7/widget/ak$c;->e:Landroid/widget/ImageView;
invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->e:Landroid/widget/ImageView;
invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V
:cond_74
:goto_74
invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-nez v0, :cond_d7
const/4 v0, 0x1
:goto_7b
if-eqz v0, :cond_d9
iget-object v4, p0, Landroid/support/v7/widget/ak$c;->d:Landroid/widget/TextView;
if-nez v4, :cond_a0
new-instance v4, Landroid/support/v7/widget/aa;
invoke-virtual {p0}, Landroid/support/v7/widget/ak$c;->getContext()Landroid/content/Context;
move-result-object v5
sget v6, Landroid/support/v7/b/a$a;->actionBarTabTextStyle:I
invoke-direct {v4, v5, v7, v6}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;
invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
new-instance v5, Landroid/support/v7/widget/af$a;
invoke-direct {v5, v8, v8}, Landroid/support/v7/widget/af$a;-><init>(II)V
iput v9, v5, Landroid/support/v7/widget/af$a;->h:I
invoke-virtual {v4, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
invoke-virtual {p0, v4}, Landroid/support/v7/widget/ak$c;->addView(Landroid/view/View;)V
iput-object v4, p0, Landroid/support/v7/widget/ak$c;->d:Landroid/widget/TextView;
:cond_a0
iget-object v4, p0, Landroid/support/v7/widget/ak$c;->d:Landroid/widget/TextView;
invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
iget-object v3, p0, Landroid/support/v7/widget/ak$c;->d:Landroid/widget/TextView;
invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V
:cond_aa
:goto_aa
iget-object v3, p0, Landroid/support/v7/widget/ak$c;->e:Landroid/widget/ImageView;
if-eqz v3, :cond_b7
iget-object v3, p0, Landroid/support/v7/widget/ak$c;->e:Landroid/widget/ImageView;
invoke-virtual {v2}, Landroid/support/v7/a/a$c;->e()Ljava/lang/CharSequence;
move-result-object v4
invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V
:cond_b7
if-nez v0, :cond_e8
invoke-virtual {v2}, Landroid/support/v7/a/a$c;->e()Ljava/lang/CharSequence;
move-result-object v0
invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-nez v0, :cond_e8
invoke-virtual {p0, p0}, Landroid/support/v7/widget/ak$c;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
goto/16 :goto_38
:cond_c8
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->e:Landroid/widget/ImageView;
if-eqz v0, :cond_74
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->e:Landroid/widget/ImageView;
invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->e:Landroid/widget/ImageView;
invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
goto :goto_74
:cond_d7
move v0, v1
goto :goto_7b
:cond_d9
iget-object v3, p0, Landroid/support/v7/widget/ak$c;->d:Landroid/widget/TextView;
if-eqz v3, :cond_aa
iget-object v3, p0, Landroid/support/v7/widget/ak$c;->d:Landroid/widget/TextView;
invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V
iget-object v3, p0, Landroid/support/v7/widget/ak$c;->d:Landroid/widget/TextView;
invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
goto :goto_aa
:cond_e8
invoke-virtual {p0, v7}, Landroid/support/v7/widget/ak$c;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
invoke-virtual {p0, v1}, Landroid/support/v7/widget/ak$c;->setLongClickable(Z)V
goto/16 :goto_38
.end method
.method public a(Landroid/support/v7/a/a$c;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/widget/ak$c;->c:Landroid/support/v7/a/a$c;
invoke-virtual {p0}, Landroid/support/v7/widget/ak$c;->a()V
return-void
.end method
.method public b()Landroid/support/v7/a/a$c;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->c:Landroid/support/v7/a/a$c;
return-object v0
.end method
.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
.registers 3
invoke-super {p0, p1}, Landroid/support/v7/widget/af;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
const-class v0, Landroid/support/v7/a/a$c;
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V
return-void
.end method
.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
.registers 4
invoke-super {p0, p1}, Landroid/support/v7/widget/af;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0xe
if-lt v0, v1, :cond_12
const-class v0, Landroid/support/v7/a/a$c;
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V
:cond_12
return-void
.end method
.method public onLongClick(Landroid/view/View;)Z
.registers 9
const/4 v6, 0x0
const/4 v0, 0x2
new-array v0, v0, [I
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ak$c;->getLocationOnScreen([I)V
invoke-virtual {p0}, Landroid/support/v7/widget/ak$c;->getContext()Landroid/content/Context;
move-result-object v1
invoke-virtual {p0}, Landroid/support/v7/widget/ak$c;->getWidth()I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/widget/ak$c;->getHeight()I
move-result v3
invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v4
invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
move-result-object v4
iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I
iget-object v5, p0, Landroid/support/v7/widget/ak$c;->c:Landroid/support/v7/a/a$c;
invoke-virtual {v5}, Landroid/support/v7/a/a$c;->e()Ljava/lang/CharSequence;
move-result-object v5
invoke-static {v1, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
move-result-object v1
const/16 v5, 0x31
aget v0, v0, v6
div-int/lit8 v2, v2, 0x2
add-int/2addr v0, v2
div-int/lit8 v2, v4, 0x2
sub-int/2addr v0, v2
invoke-virtual {v1, v5, v0, v3}, Landroid/widget/Toast;->setGravity(III)V
invoke-virtual {v1}, Landroid/widget/Toast;->show()V
const/4 v0, 0x1
return v0
.end method
.method public onMeasure(II)V
.registers 5
invoke-super {p0, p1, p2}, Landroid/support/v7/widget/af;->onMeasure(II)V
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->a:Landroid/support/v7/widget/ak;
iget v0, v0, Landroid/support/v7/widget/ak;->b:I
if-lez v0, :cond_20
invoke-virtual {p0}, Landroid/support/v7/widget/ak$c;->getMeasuredWidth()I
move-result v0
iget-object v1, p0, Landroid/support/v7/widget/ak$c;->a:Landroid/support/v7/widget/ak;
iget v1, v1, Landroid/support/v7/widget/ak;->b:I
if-le v0, v1, :cond_20
iget-object v0, p0, Landroid/support/v7/widget/ak$c;->a:Landroid/support/v7/widget/ak;
iget v0, v0, Landroid/support/v7/widget/ak;->b:I
const/high16 v1, 0x4000
invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v0
invoke-super {p0, v0, p2}, Landroid/support/v7/widget/af;->onMeasure(II)V
:cond_20
return-void
.end method
.method public setSelected(Z)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/ak$c;->isSelected()Z
move-result v0
if-eq v0, p1, :cond_13
const/4 v0, 0x1
:goto_7
invoke-super {p0, p1}, Landroid/support/v7/widget/af;->setSelected(Z)V
if-eqz v0, :cond_12
if-eqz p1, :cond_12
const/4 v0, 0x4
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ak$c;->sendAccessibilityEvent(I)V
:cond_12
return-void
:cond_13
const/4 v0, 0x0
goto :goto_7
.end method