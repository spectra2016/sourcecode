.class  Landroid/support/v7/widget/z;
.super Landroid/support/v7/widget/y;
.source "AppCompatTextHelperV17.java"
.field private static final b:[I
.field private c:Landroid/support/v7/widget/ap;
.field private d:Landroid/support/v7/widget/ap;
.method static constructor <clinit>()V
.registers 1
const/4 v0, 0x2
new-array v0, v0, [I
fill-array-data v0, :array_a
sput-object v0, Landroid/support/v7/widget/z;->b:[I
return-void
nop
:array_a
.array-data 0x4
0x92t 0x3t 0x1t 0x1t
0x93t 0x3t 0x1t 0x1t
.end array-data
.end method
.method constructor <init>(Landroid/widget/TextView;)V
.registers 2
invoke-direct {p0, p1}, Landroid/support/v7/widget/y;-><init>(Landroid/widget/TextView;)V
return-void
.end method
.method  a()V
.registers 4
invoke-super {p0}, Landroid/support/v7/widget/y;->a()V
iget-object v0, p0, Landroid/support/v7/widget/z;->c:Landroid/support/v7/widget/ap;
if-nez v0, :cond_b
iget-object v0, p0, Landroid/support/v7/widget/z;->d:Landroid/support/v7/widget/ap;
if-eqz v0, :cond_21
:cond_b
iget-object v0, p0, Landroid/support/v7/widget/z;->a:Landroid/widget/TextView;
invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;
move-result-object v0
const/4 v1, 0x0
aget-object v1, v0, v1
iget-object v2, p0, Landroid/support/v7/widget/z;->c:Landroid/support/v7/widget/ap;
invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/z;->a(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/ap;)V
const/4 v1, 0x2
aget-object v0, v0, v1
iget-object v1, p0, Landroid/support/v7/widget/z;->d:Landroid/support/v7/widget/ap;
invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/z;->a(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/ap;)V
:cond_21
return-void
.end method
.method  a(Landroid/util/AttributeSet;I)V
.registers 9
const/4 v5, 0x1
const/4 v4, 0x0
invoke-super {p0, p1, p2}, Landroid/support/v7/widget/y;->a(Landroid/util/AttributeSet;I)V
iget-object v0, p0, Landroid/support/v7/widget/z;->a:Landroid/widget/TextView;
invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;
move-result-object v0
invoke-static {}, Landroid/support/v7/widget/l;->a()Landroid/support/v7/widget/l;
move-result-object v1
sget-object v2, Landroid/support/v7/widget/z;->b:[I
invoke-virtual {v0, p1, v2, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
move-result-object v2
invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v3
if-eqz v3, :cond_25
invoke-virtual {v2, v4, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v3
invoke-static {v0, v1, v3}, Landroid/support/v7/widget/z;->a(Landroid/content/Context;Landroid/support/v7/widget/l;I)Landroid/support/v7/widget/ap;
move-result-object v3
iput-object v3, p0, Landroid/support/v7/widget/z;->c:Landroid/support/v7/widget/ap;
:cond_25
invoke-virtual {v2, v5}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v3
if-eqz v3, :cond_35
invoke-virtual {v2, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v3
invoke-static {v0, v1, v3}, Landroid/support/v7/widget/z;->a(Landroid/content/Context;Landroid/support/v7/widget/l;I)Landroid/support/v7/widget/ap;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/widget/z;->d:Landroid/support/v7/widget/ap;
:cond_35
invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V
return-void
.end method