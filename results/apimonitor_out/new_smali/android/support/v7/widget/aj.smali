.class  Landroid/support/v7/widget/aj;
.super Ljava/lang/Object;
.source "RtlSpacingHelper.java"
.field private a:I
.field private b:I
.field private c:I
.field private d:I
.field private e:I
.field private f:I
.field private g:Z
.field private h:Z
.method constructor <init>()V
.registers 3
const/high16 v1, -0x8000
const/4 v0, 0x0
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput v0, p0, Landroid/support/v7/widget/aj;->a:I
iput v0, p0, Landroid/support/v7/widget/aj;->b:I
iput v1, p0, Landroid/support/v7/widget/aj;->c:I
iput v1, p0, Landroid/support/v7/widget/aj;->d:I
iput v0, p0, Landroid/support/v7/widget/aj;->e:I
iput v0, p0, Landroid/support/v7/widget/aj;->f:I
iput-boolean v0, p0, Landroid/support/v7/widget/aj;->g:Z
iput-boolean v0, p0, Landroid/support/v7/widget/aj;->h:Z
return-void
.end method
.method public a()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/aj;->a:I
return v0
.end method
.method public a(II)V
.registers 5
const/high16 v1, -0x8000
iput p1, p0, Landroid/support/v7/widget/aj;->c:I
iput p2, p0, Landroid/support/v7/widget/aj;->d:I
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v7/widget/aj;->h:Z
iget-boolean v0, p0, Landroid/support/v7/widget/aj;->g:Z
if-eqz v0, :cond_16
if-eq p2, v1, :cond_11
iput p2, p0, Landroid/support/v7/widget/aj;->a:I
:cond_11
if-eq p1, v1, :cond_15
iput p1, p0, Landroid/support/v7/widget/aj;->b:I
:cond_15
:goto_15
return-void
:cond_16
if-eq p1, v1, :cond_1a
iput p1, p0, Landroid/support/v7/widget/aj;->a:I
:cond_1a
if-eq p2, v1, :cond_15
iput p2, p0, Landroid/support/v7/widget/aj;->b:I
goto :goto_15
.end method
.method public a(Z)V
.registers 4
const/high16 v1, -0x8000
iget-boolean v0, p0, Landroid/support/v7/widget/aj;->g:Z
if-ne p1, v0, :cond_7
:goto_6
return-void
:cond_7
iput-boolean p1, p0, Landroid/support/v7/widget/aj;->g:Z
iget-boolean v0, p0, Landroid/support/v7/widget/aj;->h:Z
if-eqz v0, :cond_3d
if-eqz p1, :cond_26
iget v0, p0, Landroid/support/v7/widget/aj;->d:I
if-eq v0, v1, :cond_20
iget v0, p0, Landroid/support/v7/widget/aj;->d:I
:goto_15
iput v0, p0, Landroid/support/v7/widget/aj;->a:I
iget v0, p0, Landroid/support/v7/widget/aj;->c:I
if-eq v0, v1, :cond_23
iget v0, p0, Landroid/support/v7/widget/aj;->c:I
:goto_1d
iput v0, p0, Landroid/support/v7/widget/aj;->b:I
goto :goto_6
:cond_20
iget v0, p0, Landroid/support/v7/widget/aj;->e:I
goto :goto_15
:cond_23
iget v0, p0, Landroid/support/v7/widget/aj;->f:I
goto :goto_1d
:cond_26
iget v0, p0, Landroid/support/v7/widget/aj;->c:I
if-eq v0, v1, :cond_37
iget v0, p0, Landroid/support/v7/widget/aj;->c:I
:goto_2c
iput v0, p0, Landroid/support/v7/widget/aj;->a:I
iget v0, p0, Landroid/support/v7/widget/aj;->d:I
if-eq v0, v1, :cond_3a
iget v0, p0, Landroid/support/v7/widget/aj;->d:I
:goto_34
iput v0, p0, Landroid/support/v7/widget/aj;->b:I
goto :goto_6
:cond_37
iget v0, p0, Landroid/support/v7/widget/aj;->e:I
goto :goto_2c
:cond_3a
iget v0, p0, Landroid/support/v7/widget/aj;->f:I
goto :goto_34
:cond_3d
iget v0, p0, Landroid/support/v7/widget/aj;->e:I
iput v0, p0, Landroid/support/v7/widget/aj;->a:I
iget v0, p0, Landroid/support/v7/widget/aj;->f:I
iput v0, p0, Landroid/support/v7/widget/aj;->b:I
goto :goto_6
.end method
.method public b()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/aj;->b:I
return v0
.end method
.method public b(II)V
.registers 5
const/high16 v1, -0x8000
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v7/widget/aj;->h:Z
if-eq p1, v1, :cond_b
iput p1, p0, Landroid/support/v7/widget/aj;->e:I
iput p1, p0, Landroid/support/v7/widget/aj;->a:I
:cond_b
if-eq p2, v1, :cond_11
iput p2, p0, Landroid/support/v7/widget/aj;->f:I
iput p2, p0, Landroid/support/v7/widget/aj;->b:I
:cond_11
return-void
.end method
.method public c()I
.registers 2
iget-boolean v0, p0, Landroid/support/v7/widget/aj;->g:Z
if-eqz v0, :cond_7
iget v0, p0, Landroid/support/v7/widget/aj;->b:I
:goto_6
return v0
:cond_7
iget v0, p0, Landroid/support/v7/widget/aj;->a:I
goto :goto_6
.end method
.method public d()I
.registers 2
iget-boolean v0, p0, Landroid/support/v7/widget/aj;->g:Z
if-eqz v0, :cond_7
iget v0, p0, Landroid/support/v7/widget/aj;->a:I
:goto_6
return v0
:cond_7
iget v0, p0, Landroid/support/v7/widget/aj;->b:I
goto :goto_6
.end method