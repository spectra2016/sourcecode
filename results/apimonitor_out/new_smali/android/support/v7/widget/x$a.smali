.class  Landroid/support/v7/widget/x$a;
.super Ljava/lang/Object;
.source "AppCompatSpinner.java"
.implements Landroid/widget/ListAdapter;
.implements Landroid/widget/SpinnerAdapter;
.field private a:Landroid/widget/SpinnerAdapter;
.field private b:Landroid/widget/ListAdapter;
.method public constructor <init>(Landroid/widget/SpinnerAdapter;Landroid/content/res/Resources$Theme;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
instance-of v0, p1, Landroid/widget/ListAdapter;
if-eqz v0, :cond_e
move-object v0, p1
check-cast v0, Landroid/widget/ListAdapter;
iput-object v0, p0, Landroid/support/v7/widget/x$a;->b:Landroid/widget/ListAdapter;
:cond_e
if-eqz p2, :cond_25
invoke-static {}, Landroid/support/v7/widget/x;->a()Z
move-result v0
if-eqz v0, :cond_26
instance-of v0, p1, Landroid/widget/ThemedSpinnerAdapter;
if-eqz v0, :cond_26
check-cast p1, Landroid/widget/ThemedSpinnerAdapter;
invoke-interface {p1}, Landroid/widget/ThemedSpinnerAdapter;->getDropDownViewTheme()Landroid/content/res/Resources$Theme;
move-result-object v0
if-eq v0, p2, :cond_25
invoke-interface {p1, p2}, Landroid/widget/ThemedSpinnerAdapter;->setDropDownViewTheme(Landroid/content/res/Resources$Theme;)V
:goto_25
:cond_25
return-void
:cond_26
instance-of v0, p1, Landroid/support/v7/widget/an;
if-eqz v0, :cond_25
check-cast p1, Landroid/support/v7/widget/an;
invoke-interface {p1}, Landroid/support/v7/widget/an;->a()Landroid/content/res/Resources$Theme;
move-result-object v0
if-nez v0, :cond_25
invoke-interface {p1, p2}, Landroid/support/v7/widget/an;->a(Landroid/content/res/Resources$Theme;)V
goto :goto_25
.end method
.method public areAllItemsEnabled()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/x$a;->b:Landroid/widget/ListAdapter;
if-eqz v0, :cond_9
invoke-interface {v0}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z
move-result v0
:goto_8
return v0
:cond_9
const/4 v0, 0x1
goto :goto_8
.end method
.method public getCount()I
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
if-nez v0, :cond_6
const/4 v0, 0x0
:goto_5
return v0
:cond_6
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I
move-result v0
goto :goto_5
.end method
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.registers 5
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
if-nez v0, :cond_6
const/4 v0, 0x0
:goto_5
return-object v0
:cond_6
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
invoke-interface {v0, p1, p2, p3}, Landroid/widget/SpinnerAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
move-result-object v0
goto :goto_5
.end method
.method public getItem(I)Ljava/lang/Object;
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
if-nez v0, :cond_6
const/4 v0, 0x0
:goto_5
return-object v0
:cond_6
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
invoke-interface {v0, p1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;
move-result-object v0
goto :goto_5
.end method
.method public getItemId(I)J
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
if-nez v0, :cond_7
const-wide/16 v0, -0x1
:goto_6
return-wide v0
:cond_7
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
invoke-interface {v0, p1}, Landroid/widget/SpinnerAdapter;->getItemId(I)J
move-result-wide v0
goto :goto_6
.end method
.method public getItemViewType(I)I
.registers 3
const/4 v0, 0x0
return v0
.end method
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.registers 5
invoke-virtual {p0, p1, p2, p3}, Landroid/support/v7/widget/x$a;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
move-result-object v0
return-object v0
.end method
.method public getViewTypeCount()I
.registers 2
const/4 v0, 0x1
return v0
.end method
.method public hasStableIds()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
if-eqz v0, :cond_e
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->hasStableIds()Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method public isEmpty()Z
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/x$a;->getCount()I
move-result v0
if-nez v0, :cond_8
const/4 v0, 0x1
:goto_7
return v0
:cond_8
const/4 v0, 0x0
goto :goto_7
.end method
.method public isEnabled(I)Z
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/x$a;->b:Landroid/widget/ListAdapter;
if-eqz v0, :cond_9
invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z
move-result v0
:goto_8
return v0
:cond_9
const/4 v0, 0x1
goto :goto_8
.end method
.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
invoke-interface {v0, p1}, Landroid/widget/SpinnerAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V
:cond_9
return-void
.end method
.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/widget/x$a;->a:Landroid/widget/SpinnerAdapter;
invoke-interface {v0, p1}, Landroid/widget/SpinnerAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
:cond_9
return-void
.end method