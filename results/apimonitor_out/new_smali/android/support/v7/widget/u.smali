.class public Landroid/support/v7/widget/u;
.super Landroid/widget/RatingBar;
.source "AppCompatRatingBar.java"
.field private a:Landroid/support/v7/widget/s;
.field private b:Landroid/support/v7/widget/l;
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 4
sget v0, Landroid/support/v7/b/a$a;->ratingBarStyle:I
invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/u;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.registers 6
invoke-direct {p0, p1, p2, p3}, Landroid/widget/RatingBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
invoke-static {}, Landroid/support/v7/widget/l;->a()Landroid/support/v7/widget/l;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/widget/u;->b:Landroid/support/v7/widget/l;
new-instance v0, Landroid/support/v7/widget/s;
iget-object v1, p0, Landroid/support/v7/widget/u;->b:Landroid/support/v7/widget/l;
invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/s;-><init>(Landroid/widget/ProgressBar;Landroid/support/v7/widget/l;)V
iput-object v0, p0, Landroid/support/v7/widget/u;->a:Landroid/support/v7/widget/s;
iget-object v0, p0, Landroid/support/v7/widget/u;->a:Landroid/support/v7/widget/s;
invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/s;->a(Landroid/util/AttributeSet;I)V
return-void
.end method
.method protected declared-synchronized onMeasure(II)V
.registers 5
monitor-enter p0
:try_start_1
invoke-super {p0, p1, p2}, Landroid/widget/RatingBar;->onMeasure(II)V
iget-object v0, p0, Landroid/support/v7/widget/u;->a:Landroid/support/v7/widget/s;
invoke-virtual {v0}, Landroid/support/v7/widget/s;->a()Landroid/graphics/Bitmap;
move-result-object v0
if-eqz v0, :cond_21
invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I
move-result v0
invoke-virtual {p0}, Landroid/support/v7/widget/u;->getNumStars()I
move-result v1
mul-int/2addr v0, v1
const/4 v1, 0x0
invoke-static {v0, p1, v1}, Landroid/support/v4/f/af;->a(III)I
move-result v0
invoke-virtual {p0}, Landroid/support/v7/widget/u;->getMeasuredHeight()I
move-result v1
invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/u;->setMeasuredDimension(II)V
:cond_21
:try_end_21
.catchall {:try_start_1 .. :try_end_21} :catchall_23
monitor-exit p0
return-void
:catchall_23
move-exception v0
monitor-exit p0
throw v0
.end method