.class  Landroid/support/v7/widget/k;
.super Ljava/lang/Object;
.source "AppCompatCompoundButtonHelper.java"
.field private final a:Landroid/widget/CompoundButton;
.field private final b:Landroid/support/v7/widget/l;
.field private c:Landroid/content/res/ColorStateList;
.field private d:Landroid/graphics/PorterDuff$Mode;
.field private e:Z
.field private f:Z
.field private g:Z
.method constructor <init>(Landroid/widget/CompoundButton;Landroid/support/v7/widget/l;)V
.registers 5
const/4 v1, 0x0
const/4 v0, 0x0
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object v1, p0, Landroid/support/v7/widget/k;->c:Landroid/content/res/ColorStateList;
iput-object v1, p0, Landroid/support/v7/widget/k;->d:Landroid/graphics/PorterDuff$Mode;
iput-boolean v0, p0, Landroid/support/v7/widget/k;->e:Z
iput-boolean v0, p0, Landroid/support/v7/widget/k;->f:Z
iput-object p1, p0, Landroid/support/v7/widget/k;->a:Landroid/widget/CompoundButton;
iput-object p2, p0, Landroid/support/v7/widget/k;->b:Landroid/support/v7/widget/l;
return-void
.end method
.method  a(I)I
.registers 4
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x11
if-ge v0, v1, :cond_13
iget-object v0, p0, Landroid/support/v7/widget/k;->a:Landroid/widget/CompoundButton;
invoke-static {v0}, Landroid/support/v4/widget/b;->a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;
move-result-object v0
if-eqz v0, :cond_13
invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I
move-result v0
add-int/2addr p1, v0
:cond_13
return p1
.end method
.method  a()Landroid/content/res/ColorStateList;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/k;->c:Landroid/content/res/ColorStateList;
return-object v0
.end method
.method  a(Landroid/content/res/ColorStateList;)V
.registers 3
iput-object p1, p0, Landroid/support/v7/widget/k;->c:Landroid/content/res/ColorStateList;
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v7/widget/k;->e:Z
invoke-virtual {p0}, Landroid/support/v7/widget/k;->d()V
return-void
.end method
.method  a(Landroid/graphics/PorterDuff$Mode;)V
.registers 3
iput-object p1, p0, Landroid/support/v7/widget/k;->d:Landroid/graphics/PorterDuff$Mode;
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v7/widget/k;->f:Z
invoke-virtual {p0}, Landroid/support/v7/widget/k;->d()V
return-void
.end method
.method  a(Landroid/util/AttributeSet;I)V
.registers 8
const/4 v2, 0x0
iget-object v0, p0, Landroid/support/v7/widget/k;->a:Landroid/widget/CompoundButton;
invoke-virtual {v0}, Landroid/widget/CompoundButton;->getContext()Landroid/content/Context;
move-result-object v0
sget-object v1, Landroid/support/v7/b/a$k;->CompoundButton:[I
invoke-virtual {v0, p1, v1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
move-result-object v1
:try_start_d
sget v0, Landroid/support/v7/b/a$k;->CompoundButton_android_button:I
invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v0
if-eqz v0, :cond_2f
sget v0, Landroid/support/v7/b/a$k;->CompoundButton_android_button:I
const/4 v2, 0x0
invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v0
if-eqz v0, :cond_2f
iget-object v2, p0, Landroid/support/v7/widget/k;->a:Landroid/widget/CompoundButton;
iget-object v3, p0, Landroid/support/v7/widget/k;->b:Landroid/support/v7/widget/l;
iget-object v4, p0, Landroid/support/v7/widget/k;->a:Landroid/widget/CompoundButton;
invoke-virtual {v4}, Landroid/widget/CompoundButton;->getContext()Landroid/content/Context;
move-result-object v4
invoke-virtual {v3, v4, v0}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v0
invoke-virtual {v2, v0}, Landroid/widget/CompoundButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
:cond_2f
sget v0, Landroid/support/v7/b/a$k;->CompoundButton_buttonTint:I
invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v0
if-eqz v0, :cond_42
iget-object v0, p0, Landroid/support/v7/widget/k;->a:Landroid/widget/CompoundButton;
sget v2, Landroid/support/v7/b/a$k;->CompoundButton_buttonTint:I
invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;
move-result-object v2
invoke-static {v0, v2}, Landroid/support/v4/widget/b;->a(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V
:cond_42
sget v0, Landroid/support/v7/b/a$k;->CompoundButton_buttonTintMode:I
invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v0
if-eqz v0, :cond_5b
iget-object v0, p0, Landroid/support/v7/widget/k;->a:Landroid/widget/CompoundButton;
sget v2, Landroid/support/v7/b/a$k;->CompoundButton_buttonTintMode:I
const/4 v3, -0x1
invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I
move-result v2
const/4 v3, 0x0
invoke-static {v2, v3}, Landroid/support/v7/widget/ad;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;
move-result-object v2
invoke-static {v0, v2}, Landroid/support/v4/widget/b;->a(Landroid/widget/CompoundButton;Landroid/graphics/PorterDuff$Mode;)V
:try_end_5b
.catchall {:try_start_d .. :try_end_5b} :catchall_5f
:cond_5b
invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V
return-void
:catchall_5f
move-exception v0
invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V
throw v0
.end method
.method  b()Landroid/graphics/PorterDuff$Mode;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/k;->d:Landroid/graphics/PorterDuff$Mode;
return-object v0
.end method
.method  c()V
.registers 2
iget-boolean v0, p0, Landroid/support/v7/widget/k;->g:Z
if-eqz v0, :cond_8
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v7/widget/k;->g:Z
:goto_7
return-void
:cond_8
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v7/widget/k;->g:Z
invoke-virtual {p0}, Landroid/support/v7/widget/k;->d()V
goto :goto_7
.end method
.method  d()V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/k;->a:Landroid/widget/CompoundButton;
invoke-static {v0}, Landroid/support/v4/widget/b;->a(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;
move-result-object v0
if-eqz v0, :cond_3e
iget-boolean v1, p0, Landroid/support/v7/widget/k;->e:Z
if-nez v1, :cond_10
iget-boolean v1, p0, Landroid/support/v7/widget/k;->f:Z
if-eqz v1, :cond_3e
:cond_10
invoke-static {v0}, Landroid/support/v4/b/a/a;->f(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
move-result-object v0
invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;
move-result-object v0
iget-boolean v1, p0, Landroid/support/v7/widget/k;->e:Z
if-eqz v1, :cond_21
iget-object v1, p0, Landroid/support/v7/widget/k;->c:Landroid/content/res/ColorStateList;
invoke-static {v0, v1}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
:cond_21
iget-boolean v1, p0, Landroid/support/v7/widget/k;->f:Z
if-eqz v1, :cond_2a
iget-object v1, p0, Landroid/support/v7/widget/k;->d:Landroid/graphics/PorterDuff$Mode;
invoke-static {v0, v1}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
:cond_2a
invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z
move-result v1
if-eqz v1, :cond_39
iget-object v1, p0, Landroid/support/v7/widget/k;->a:Landroid/widget/CompoundButton;
invoke-virtual {v1}, Landroid/widget/CompoundButton;->getDrawableState()[I
move-result-object v1
invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z
:cond_39
iget-object v1, p0, Landroid/support/v7/widget/k;->a:Landroid/widget/CompoundButton;
invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
:cond_3e
return-void
.end method