.class public Landroid/support/v7/widget/Toolbar;
.super Landroid/view/ViewGroup;
.source "Toolbar.java"
.field private A:Z
.field private final B:Ljava/util/ArrayList;
.field private final C:Ljava/util/ArrayList;
.field private final D:[I
.field private E:Landroid/support/v7/widget/Toolbar$c;
.field private final F:Landroid/support/v7/widget/ActionMenuView$e;
.field private G:Landroid/support/v7/widget/as;
.field private H:Landroid/support/v7/widget/d;
.field private I:Landroid/support/v7/widget/Toolbar$a;
.field private J:Landroid/support/v7/view/menu/l$a;
.field private K:Landroid/support/v7/view/menu/f$a;
.field private L:Z
.field private final M:Ljava/lang/Runnable;
.field private final N:Landroid/support/v7/widget/l;
.field  a:Landroid/view/View;
.field private b:Landroid/support/v7/widget/ActionMenuView;
.field private c:Landroid/widget/TextView;
.field private d:Landroid/widget/TextView;
.field private e:Landroid/widget/ImageButton;
.field private f:Landroid/widget/ImageView;
.field private g:Landroid/graphics/drawable/Drawable;
.field private h:Ljava/lang/CharSequence;
.field private i:Landroid/widget/ImageButton;
.field private j:Landroid/content/Context;
.field private k:I
.field private l:I
.field private m:I
.field private n:I
.field private o:I
.field private p:I
.field private q:I
.field private r:I
.field private s:I
.field private final t:Landroid/support/v7/widget/aj;
.field private u:I
.field private v:Ljava/lang/CharSequence;
.field private w:Ljava/lang/CharSequence;
.field private x:I
.field private y:I
.field private z:Z
.method public constructor <init>(Landroid/content/Context;)V
.registers 3
const/4 v0, 0x0
invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/Toolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 4
sget v0, Landroid/support/v7/b/a$a;->toolbarStyle:I
invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/Toolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.registers 13
const/high16 v8, -0x8000
const/4 v7, 0x0
const/4 v6, -0x1
invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
new-instance v0, Landroid/support/v7/widget/aj;
invoke-direct {v0}, Landroid/support/v7/widget/aj;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->t:Landroid/support/v7/widget/aj;
const v0, 0x800013
iput v0, p0, Landroid/support/v7/widget/Toolbar;->u:I
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->B:Ljava/util/ArrayList;
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->C:Ljava/util/ArrayList;
const/4 v0, 0x2
new-array v0, v0, [I
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->D:[I
new-instance v0, Landroid/support/v7/widget/Toolbar$1;
invoke-direct {v0, p0}, Landroid/support/v7/widget/Toolbar$1;-><init>(Landroid/support/v7/widget/Toolbar;)V
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->F:Landroid/support/v7/widget/ActionMenuView$e;
new-instance v0, Landroid/support/v7/widget/Toolbar$2;
invoke-direct {v0, p0}, Landroid/support/v7/widget/Toolbar$2;-><init>(Landroid/support/v7/widget/Toolbar;)V
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->M:Ljava/lang/Runnable;
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v0
sget-object v1, Landroid/support/v7/b/a$k;->Toolbar:[I
invoke-static {v0, p2, v1, p3, v7}, Landroid/support/v7/widget/ar;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/widget/ar;
move-result-object v0
sget v1, Landroid/support/v7/b/a$k;->Toolbar_titleTextAppearance:I
invoke-virtual {v0, v1, v7}, Landroid/support/v7/widget/ar;->g(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/Toolbar;->l:I
sget v1, Landroid/support/v7/b/a$k;->Toolbar_subtitleTextAppearance:I
invoke-virtual {v0, v1, v7}, Landroid/support/v7/widget/ar;->g(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/Toolbar;->m:I
sget v1, Landroid/support/v7/b/a$k;->Toolbar_android_gravity:I
iget v2, p0, Landroid/support/v7/widget/Toolbar;->u:I
invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ar;->c(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/Toolbar;->u:I
const/16 v1, 0x30
iput v1, p0, Landroid/support/v7/widget/Toolbar;->n:I
sget v1, Landroid/support/v7/b/a$k;->Toolbar_titleMargins:I
invoke-virtual {v0, v1, v7}, Landroid/support/v7/widget/ar;->d(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/Toolbar;->s:I
iput v1, p0, Landroid/support/v7/widget/Toolbar;->r:I
iput v1, p0, Landroid/support/v7/widget/Toolbar;->q:I
iput v1, p0, Landroid/support/v7/widget/Toolbar;->p:I
sget v1, Landroid/support/v7/b/a$k;->Toolbar_titleMarginStart:I
invoke-virtual {v0, v1, v6}, Landroid/support/v7/widget/ar;->d(II)I
move-result v1
if-ltz v1, :cond_74
iput v1, p0, Landroid/support/v7/widget/Toolbar;->p:I
:cond_74
sget v1, Landroid/support/v7/b/a$k;->Toolbar_titleMarginEnd:I
invoke-virtual {v0, v1, v6}, Landroid/support/v7/widget/ar;->d(II)I
move-result v1
if-ltz v1, :cond_7e
iput v1, p0, Landroid/support/v7/widget/Toolbar;->q:I
:cond_7e
sget v1, Landroid/support/v7/b/a$k;->Toolbar_titleMarginTop:I
invoke-virtual {v0, v1, v6}, Landroid/support/v7/widget/ar;->d(II)I
move-result v1
if-ltz v1, :cond_88
iput v1, p0, Landroid/support/v7/widget/Toolbar;->r:I
:cond_88
sget v1, Landroid/support/v7/b/a$k;->Toolbar_titleMarginBottom:I
invoke-virtual {v0, v1, v6}, Landroid/support/v7/widget/ar;->d(II)I
move-result v1
if-ltz v1, :cond_92
iput v1, p0, Landroid/support/v7/widget/Toolbar;->s:I
:cond_92
sget v1, Landroid/support/v7/b/a$k;->Toolbar_maxButtonHeight:I
invoke-virtual {v0, v1, v6}, Landroid/support/v7/widget/ar;->e(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/Toolbar;->o:I
sget v1, Landroid/support/v7/b/a$k;->Toolbar_contentInsetStart:I
invoke-virtual {v0, v1, v8}, Landroid/support/v7/widget/ar;->d(II)I
move-result v1
sget v2, Landroid/support/v7/b/a$k;->Toolbar_contentInsetEnd:I
invoke-virtual {v0, v2, v8}, Landroid/support/v7/widget/ar;->d(II)I
move-result v2
sget v3, Landroid/support/v7/b/a$k;->Toolbar_contentInsetLeft:I
invoke-virtual {v0, v3, v7}, Landroid/support/v7/widget/ar;->e(II)I
move-result v3
sget v4, Landroid/support/v7/b/a$k;->Toolbar_contentInsetRight:I
invoke-virtual {v0, v4, v7}, Landroid/support/v7/widget/ar;->e(II)I
move-result v4
iget-object v5, p0, Landroid/support/v7/widget/Toolbar;->t:Landroid/support/v7/widget/aj;
invoke-virtual {v5, v3, v4}, Landroid/support/v7/widget/aj;->b(II)V
if-ne v1, v8, :cond_bb
if-eq v2, v8, :cond_c0
:cond_bb
iget-object v3, p0, Landroid/support/v7/widget/Toolbar;->t:Landroid/support/v7/widget/aj;
invoke-virtual {v3, v1, v2}, Landroid/support/v7/widget/aj;->a(II)V
:cond_c0
sget v1, Landroid/support/v7/b/a$k;->Toolbar_collapseIcon:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->a(I)Landroid/graphics/drawable/Drawable;
move-result-object v1
iput-object v1, p0, Landroid/support/v7/widget/Toolbar;->g:Landroid/graphics/drawable/Drawable;
sget v1, Landroid/support/v7/b/a$k;->Toolbar_collapseContentDescription:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->c(I)Ljava/lang/CharSequence;
move-result-object v1
iput-object v1, p0, Landroid/support/v7/widget/Toolbar;->h:Ljava/lang/CharSequence;
sget v1, Landroid/support/v7/b/a$k;->Toolbar_title:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->c(I)Ljava/lang/CharSequence;
move-result-object v1
invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v2
if-nez v2, :cond_df
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V
:cond_df
sget v1, Landroid/support/v7/b/a$k;->Toolbar_subtitle:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->c(I)Ljava/lang/CharSequence;
move-result-object v1
invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v2
if-nez v2, :cond_ee
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V
:cond_ee
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v1
iput-object v1, p0, Landroid/support/v7/widget/Toolbar;->j:Landroid/content/Context;
sget v1, Landroid/support/v7/b/a$k;->Toolbar_popupTheme:I
invoke-virtual {v0, v1, v7}, Landroid/support/v7/widget/ar;->g(II)I
move-result v1
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->setPopupTheme(I)V
sget v1, Landroid/support/v7/b/a$k;->Toolbar_navigationIcon:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->a(I)Landroid/graphics/drawable/Drawable;
move-result-object v1
if-eqz v1, :cond_108
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V
:cond_108
sget v1, Landroid/support/v7/b/a$k;->Toolbar_navigationContentDescription:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->c(I)Ljava/lang/CharSequence;
move-result-object v1
invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v2
if-nez v2, :cond_117
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(Ljava/lang/CharSequence;)V
:cond_117
sget v1, Landroid/support/v7/b/a$k;->Toolbar_logo:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->a(I)Landroid/graphics/drawable/Drawable;
move-result-object v1
if-eqz v1, :cond_122
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->setLogo(Landroid/graphics/drawable/Drawable;)V
:cond_122
sget v1, Landroid/support/v7/b/a$k;->Toolbar_logoDescription:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->c(I)Ljava/lang/CharSequence;
move-result-object v1
invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v2
if-nez v2, :cond_131
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->setLogoDescription(Ljava/lang/CharSequence;)V
:cond_131
sget v1, Landroid/support/v7/b/a$k;->Toolbar_titleTextColor:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->f(I)Z
move-result v1
if-eqz v1, :cond_142
sget v1, Landroid/support/v7/b/a$k;->Toolbar_titleTextColor:I
invoke-virtual {v0, v1, v6}, Landroid/support/v7/widget/ar;->b(II)I
move-result v1
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->setTitleTextColor(I)V
:cond_142
sget v1, Landroid/support/v7/b/a$k;->Toolbar_subtitleTextColor:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->f(I)Z
move-result v1
if-eqz v1, :cond_153
sget v1, Landroid/support/v7/b/a$k;->Toolbar_subtitleTextColor:I
invoke-virtual {v0, v1, v6}, Landroid/support/v7/widget/ar;->b(II)I
move-result v1
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->setSubtitleTextColor(I)V
:cond_153
invoke-virtual {v0}, Landroid/support/v7/widget/ar;->a()V
invoke-static {}, Landroid/support/v7/widget/l;->a()Landroid/support/v7/widget/l;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->N:Landroid/support/v7/widget/l;
return-void
.end method
.method private a(I)I
.registers 3
and-int/lit8 v0, p1, 0x70
sparse-switch v0, :sswitch_data_a
iget v0, p0, Landroid/support/v7/widget/Toolbar;->u:I
and-int/lit8 v0, v0, 0x70
:sswitch_9
return v0
:sswitch_data_a
.sparse-switch
0x10 -> :sswitch_9
0x30 -> :sswitch_9
0x50 -> :sswitch_9
.end sparse-switch
.end method
.method private a(Landroid/view/View;I)I
.registers 11
const/4 v2, 0x0
invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/Toolbar$b;
invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I
move-result v3
if-lez p2, :cond_34
sub-int v1, v3, p2
div-int/lit8 v1, v1, 0x2
:goto_11
iget v4, v0, Landroid/support/v7/widget/Toolbar$b;->a:I
invoke-direct {p0, v4}, Landroid/support/v7/widget/Toolbar;->a(I)I
move-result v4
sparse-switch v4, :sswitch_data_62
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingTop()I
move-result v4
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingBottom()I
move-result v5
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getHeight()I
move-result v6
sub-int v1, v6, v4
sub-int/2addr v1, v5
sub-int/2addr v1, v3
div-int/lit8 v1, v1, 0x2
iget v7, v0, Landroid/support/v7/widget/Toolbar$b;->topMargin:I
if-ge v1, v7, :cond_4c
iget v0, v0, Landroid/support/v7/widget/Toolbar$b;->topMargin:I
:goto_32
add-int/2addr v0, v4
:goto_33
return v0
:cond_34
move v1, v2
goto :goto_11
:sswitch_36
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingTop()I
move-result v0
sub-int/2addr v0, v1
goto :goto_33
:sswitch_3c
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getHeight()I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingBottom()I
move-result v4
sub-int/2addr v2, v4
sub-int/2addr v2, v3
iget v0, v0, Landroid/support/v7/widget/Toolbar$b;->bottomMargin:I
sub-int v0, v2, v0
sub-int/2addr v0, v1
goto :goto_33
:cond_4c
sub-int v5, v6, v5
sub-int v3, v5, v3
sub-int/2addr v3, v1
sub-int/2addr v3, v4
iget v5, v0, Landroid/support/v7/widget/Toolbar$b;->bottomMargin:I
if-ge v3, v5, :cond_60
iget v0, v0, Landroid/support/v7/widget/Toolbar$b;->bottomMargin:I
sub-int/2addr v0, v3
sub-int v0, v1, v0
invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I
move-result v0
goto :goto_32
:cond_60
move v0, v1
goto :goto_32
:sswitch_data_62
.sparse-switch
0x30 -> :sswitch_36
0x50 -> :sswitch_3c
.end sparse-switch
.end method
.method private a(Landroid/view/View;IIII[I)I
.registers 14
const/4 v6, 0x1
const/4 v5, 0x0
invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;
iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I
aget v2, p6, v5
sub-int/2addr v1, v2
iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I
aget v3, p6, v6
sub-int/2addr v2, v3
invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I
move-result v3
invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I
move-result v4
add-int/2addr v3, v4
neg-int v1, v1
invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I
move-result v1
aput v1, p6, v5
neg-int v1, v2
invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I
move-result v1
aput v1, p6, v6
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingLeft()I
move-result v1
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingRight()I
move-result v2
add-int/2addr v1, v2
add-int/2addr v1, v3
add-int/2addr v1, p3
iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I
invoke-static {p2, v1, v2}, Landroid/support/v7/widget/Toolbar;->getChildMeasureSpec(III)I
move-result v1
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingTop()I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingBottom()I
move-result v4
add-int/2addr v2, v4
iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I
add-int/2addr v2, v4
iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I
add-int/2addr v2, v4
add-int/2addr v2, p5
iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I
invoke-static {p4, v2, v0}, Landroid/support/v7/widget/Toolbar;->getChildMeasureSpec(III)I
move-result v0
invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V
invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I
move-result v0
add-int/2addr v0, v3
return v0
.end method
.method private a(Landroid/view/View;I[II)I
.registers 11
const/4 v3, 0x0
invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/Toolbar$b;
iget v1, v0, Landroid/support/v7/widget/Toolbar$b;->leftMargin:I
aget v2, p3, v3
sub-int/2addr v1, v2
invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I
move-result v2
add-int/2addr v2, p2
neg-int v1, v1
invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I
move-result v1
aput v1, p3, v3
invoke-direct {p0, p1, p4}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;I)I
move-result v1
invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I
move-result v3
add-int v4, v2, v3
invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I
move-result v5
add-int/2addr v5, v1
invoke-virtual {p1, v2, v1, v4, v5}, Landroid/view/View;->layout(IIII)V
iget v0, v0, Landroid/support/v7/widget/Toolbar$b;->rightMargin:I
add-int/2addr v0, v3
add-int/2addr v0, v2
return v0
.end method
.method private a(Ljava/util/List;[I)I
.registers 13
const/4 v3, 0x0
aget v1, p2, v3
const/4 v0, 0x1
aget v0, p2, v0
invoke-interface {p1}, Ljava/util/List;->size()I
move-result v7
move v2, v3
move v4, v3
move v5, v0
move v6, v1
:goto_e
if-ge v2, v7, :cond_42
invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v1
check-cast v1, Landroid/support/v7/widget/Toolbar$b;
iget v8, v1, Landroid/support/v7/widget/Toolbar$b;->leftMargin:I
sub-int v6, v8, v6
iget v1, v1, Landroid/support/v7/widget/Toolbar$b;->rightMargin:I
sub-int/2addr v1, v5
invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I
move-result v8
invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I
move-result v9
neg-int v5, v6
invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I
move-result v6
neg-int v1, v1
invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I
move-result v5
invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I
move-result v0
add-int/2addr v0, v8
add-int/2addr v0, v9
add-int v1, v4, v0
add-int/lit8 v0, v2, 0x1
move v2, v0
move v4, v1
goto :goto_e
:cond_42
return v4
.end method
.method static synthetic a(Landroid/support/v7/widget/Toolbar;)Landroid/support/v7/widget/Toolbar$c;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->E:Landroid/support/v7/widget/Toolbar$c;
return-object v0
.end method
.method private a(Landroid/view/View;IIIII)V
.registers 12
const/high16 v4, 0x4000
invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingLeft()I
move-result v1
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingRight()I
move-result v2
add-int/2addr v1, v2
iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I
add-int/2addr v1, v2
iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I
add-int/2addr v1, v2
add-int/2addr v1, p3
iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I
invoke-static {p2, v1, v2}, Landroid/support/v7/widget/Toolbar;->getChildMeasureSpec(III)I
move-result v1
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingTop()I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingBottom()I
move-result v3
add-int/2addr v2, v3
iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I
add-int/2addr v2, v3
iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I
add-int/2addr v2, v3
add-int/2addr v2, p5
iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I
invoke-static {p4, v2, v0}, Landroid/support/v7/widget/Toolbar;->getChildMeasureSpec(III)I
move-result v0
invoke-static {v0}, Landroid/view/View$MeasureSpec;->getMode(I)I
move-result v2
if-eq v2, v4, :cond_4a
if-ltz p6, :cond_4a
if-eqz v2, :cond_46
invoke-static {v0}, Landroid/view/View$MeasureSpec;->getSize(I)I
move-result v0
invoke-static {v0, p6}, Ljava/lang/Math;->min(II)I
move-result p6
:cond_46
invoke-static {p6, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v0
:cond_4a
invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V
return-void
.end method
.method private a(Landroid/view/View;Z)V
.registers 5
invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
if-nez v0, :cond_1c
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->i()Landroid/support/v7/widget/Toolbar$b;
move-result-object v0
:goto_a
const/4 v1, 0x1
iput v1, v0, Landroid/support/v7/widget/Toolbar$b;->b:I
if-eqz p2, :cond_2a
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
if-eqz v1, :cond_2a
invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->C:Ljava/util/ArrayList;
invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
:goto_1b
return-void
:cond_1c
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
move-result v1
if-nez v1, :cond_27
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/Toolbar$b;
move-result-object v0
goto :goto_a
:cond_27
check-cast v0, Landroid/support/v7/widget/Toolbar$b;
goto :goto_a
:cond_2a
invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
goto :goto_1b
.end method
.method private a(Ljava/util/List;I)V
.registers 9
const/4 v0, 0x1
const/4 v1, 0x0
invoke-static {p0}, Landroid/support/v4/f/af;->d(Landroid/view/View;)I
move-result v2
if-ne v2, v0, :cond_41
:goto_8
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getChildCount()I
move-result v2
invoke-static {p0}, Landroid/support/v4/f/af;->d(Landroid/view/View;)I
move-result v3
invoke-static {p2, v3}, Landroid/support/v4/f/e;->a(II)I
move-result v3
invoke-interface {p1}, Ljava/util/List;->clear()V
if-eqz v0, :cond_43
add-int/lit8 v0, v2, -0x1
move v1, v0
:goto_1c
if-ltz v1, :cond_67
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->getChildAt(I)Landroid/view/View;
move-result-object v2
invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/Toolbar$b;
iget v4, v0, Landroid/support/v7/widget/Toolbar$b;->b:I
if-nez v4, :cond_3d
invoke-direct {p0, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v4
if-eqz v4, :cond_3d
iget v0, v0, Landroid/support/v7/widget/Toolbar$b;->a:I
invoke-direct {p0, v0}, Landroid/support/v7/widget/Toolbar;->b(I)I
move-result v0
if-ne v0, v3, :cond_3d
invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
:cond_3d
add-int/lit8 v0, v1, -0x1
move v1, v0
goto :goto_1c
:cond_41
move v0, v1
goto :goto_8
:goto_43
:cond_43
if-ge v1, v2, :cond_67
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->getChildAt(I)Landroid/view/View;
move-result-object v4
invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/Toolbar$b;
iget v5, v0, Landroid/support/v7/widget/Toolbar$b;->b:I
if-nez v5, :cond_64
invoke-direct {p0, v4}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v5
if-eqz v5, :cond_64
iget v0, v0, Landroid/support/v7/widget/Toolbar$b;->a:I
invoke-direct {p0, v0}, Landroid/support/v7/widget/Toolbar;->b(I)I
move-result v0
if-ne v0, v3, :cond_64
invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
:cond_64
add-int/lit8 v1, v1, 0x1
goto :goto_43
:cond_67
return-void
.end method
.method private a(Landroid/view/View;)Z
.registers 4
if-eqz p1, :cond_12
invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;
move-result-object v0
if-ne v0, p0, :cond_12
invoke-virtual {p1}, Landroid/view/View;->getVisibility()I
move-result v0
const/16 v1, 0x8
if-eq v0, v1, :cond_12
const/4 v0, 0x1
:goto_11
return v0
:cond_12
const/4 v0, 0x0
goto :goto_11
.end method
.method private b(I)I
.registers 4
invoke-static {p0}, Landroid/support/v4/f/af;->d(Landroid/view/View;)I
move-result v1
invoke-static {p1, v1}, Landroid/support/v4/f/e;->a(II)I
move-result v0
and-int/lit8 v0, v0, 0x7
packed-switch v0, :pswitch_data_14
:pswitch_d
const/4 v0, 0x1
if-ne v1, v0, :cond_12
const/4 v0, 0x5
:goto_11
:pswitch_11
return v0
:cond_12
const/4 v0, 0x3
goto :goto_11
:pswitch_data_14
.packed-switch 0x1
:pswitch_11
:pswitch_d
:pswitch_11
:pswitch_d
:pswitch_11
.end packed-switch
.end method
.method private b(Landroid/view/View;)I
.registers 4
invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;
invoke-static {v0}, Landroid/support/v4/f/n;->a(Landroid/view/ViewGroup$MarginLayoutParams;)I
move-result v1
invoke-static {v0}, Landroid/support/v4/f/n;->b(Landroid/view/ViewGroup$MarginLayoutParams;)I
move-result v0
add-int/2addr v0, v1
return v0
.end method
.method private b(Landroid/view/View;I[II)I
.registers 11
const/4 v4, 0x1
const/4 v3, 0x0
invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/Toolbar$b;
iget v1, v0, Landroid/support/v7/widget/Toolbar$b;->rightMargin:I
aget v2, p3, v4
sub-int/2addr v1, v2
invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I
move-result v2
sub-int v2, p2, v2
neg-int v1, v1
invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I
move-result v1
aput v1, p3, v4
invoke-direct {p0, p1, p4}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;I)I
move-result v1
invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I
move-result v3
sub-int v4, v2, v3
invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I
move-result v5
add-int/2addr v5, v1
invoke-virtual {p1, v4, v1, v2, v5}, Landroid/view/View;->layout(IIII)V
iget v0, v0, Landroid/support/v7/widget/Toolbar$b;->leftMargin:I
add-int/2addr v0, v3
sub-int v0, v2, v0
return v0
.end method
.method static synthetic b(Landroid/support/v7/widget/Toolbar;)V
.registers 1
invoke-direct {p0}, Landroid/support/v7/widget/Toolbar;->p()V
return-void
.end method
.method private c(Landroid/view/View;)I
.registers 4
invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;
iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I
iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I
add-int/2addr v0, v1
return v0
.end method
.method static synthetic c(Landroid/support/v7/widget/Toolbar;)Landroid/widget/ImageButton;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
return-object v0
.end method
.method static synthetic d(Landroid/support/v7/widget/Toolbar;)I
.registers 2
iget v0, p0, Landroid/support/v7/widget/Toolbar;->n:I
return v0
.end method
.method private d(Landroid/view/View;)Z
.registers 3
invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;
move-result-object v0
if-eq v0, p0, :cond_e
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->C:Ljava/util/ArrayList;
invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_10
:cond_e
const/4 v0, 0x1
:goto_f
return v0
:cond_10
const/4 v0, 0x0
goto :goto_f
.end method
.method private getMenuInflater()Landroid/view/MenuInflater;
.registers 3
new-instance v0, Landroid/support/v7/view/g;
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v1
invoke-direct {v0, v1}, Landroid/support/v7/view/g;-><init>(Landroid/content/Context;)V
return-object v0
.end method
.method private l()V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
if-nez v0, :cond_f
new-instance v0, Landroid/widget/ImageView;
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v1
invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
:cond_f
return-void
.end method
.method private m()V
.registers 4
invoke-direct {p0}, Landroid/support/v7/widget/Toolbar;->n()V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->d()Landroid/support/v7/view/menu/f;
move-result-object v0
if-nez v0, :cond_2c
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->getMenu()Landroid/view/Menu;
move-result-object v0
check-cast v0, Landroid/support/v7/view/menu/f;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
if-nez v1, :cond_1f
new-instance v1, Landroid/support/v7/widget/Toolbar$a;
const/4 v2, 0x0
invoke-direct {v1, p0, v2}, Landroid/support/v7/widget/Toolbar$a;-><init>(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/widget/Toolbar$1;)V
iput-object v1, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
:cond_1f
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
const/4 v2, 0x1
invoke-virtual {v1, v2}, Landroid/support/v7/widget/ActionMenuView;->setExpandedActionViewsExclusive(Z)V
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
iget-object v2, p0, Landroid/support/v7/widget/Toolbar;->j:Landroid/content/Context;
invoke-virtual {v0, v1, v2}, Landroid/support/v7/view/menu/f;->a(Landroid/support/v7/view/menu/l;Landroid/content/Context;)V
:cond_2c
return-void
.end method
.method private n()V
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
if-nez v0, :cond_3f
new-instance v0, Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v1
invoke-direct {v0, v1}, Landroid/support/v7/widget/ActionMenuView;-><init>(Landroid/content/Context;)V
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
iget v1, p0, Landroid/support/v7/widget/Toolbar;->k:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->setPopupTheme(I)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->F:Landroid/support/v7/widget/ActionMenuView$e;
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->setOnMenuItemClickListener(Landroid/support/v7/widget/ActionMenuView$e;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->J:Landroid/support/v7/view/menu/l$a;
iget-object v2, p0, Landroid/support/v7/widget/Toolbar;->K:Landroid/support/v7/view/menu/f$a;
invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/support/v7/view/menu/l$a;Landroid/support/v7/view/menu/f$a;)V
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->i()Landroid/support/v7/widget/Toolbar$b;
move-result-object v0
const v1, 0x800005
iget v2, p0, Landroid/support/v7/widget/Toolbar;->n:I
and-int/lit8 v2, v2, 0x70
or-int/2addr v1, v2
iput v1, v0, Landroid/support/v7/widget/Toolbar$b;->a:I
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v1, v0}, Landroid/support/v7/widget/ActionMenuView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
const/4 v1, 0x0
invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;Z)V
:cond_3f
return-void
.end method
.method private o()V
.registers 5
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
if-nez v0, :cond_25
new-instance v0, Landroid/widget/ImageButton;
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v1
const/4 v2, 0x0
sget v3, Landroid/support/v7/b/a$a;->toolbarNavigationButtonStyle:I
invoke-direct {v0, v1, v2, v3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->i()Landroid/support/v7/widget/Toolbar$b;
move-result-object v0
const v1, 0x800003
iget v2, p0, Landroid/support/v7/widget/Toolbar;->n:I
and-int/lit8 v2, v2, 0x70
or-int/2addr v1, v2
iput v1, v0, Landroid/support/v7/widget/Toolbar$b;->a:I
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
:cond_25
return-void
.end method
.method private p()V
.registers 5
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
if-nez v0, :cond_40
new-instance v0, Landroid/widget/ImageButton;
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v1
const/4 v2, 0x0
sget v3, Landroid/support/v7/b/a$a;->toolbarNavigationButtonStyle:I
invoke-direct {v0, v1, v2, v3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->g:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->h:Ljava/lang/CharSequence;
invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->i()Landroid/support/v7/widget/Toolbar$b;
move-result-object v0
const v1, 0x800003
iget v2, p0, Landroid/support/v7/widget/Toolbar;->n:I
and-int/lit8 v2, v2, 0x70
or-int/2addr v1, v2
iput v1, v0, Landroid/support/v7/widget/Toolbar$b;->a:I
const/4 v1, 0x2
iput v1, v0, Landroid/support/v7/widget/Toolbar$b;->b:I
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
new-instance v1, Landroid/support/v7/widget/Toolbar$3;
invoke-direct {v1, p0}, Landroid/support/v7/widget/Toolbar$3;-><init>(Landroid/support/v7/widget/Toolbar;)V
invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V
:cond_40
return-void
.end method
.method private q()V
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->M:Ljava/lang/Runnable;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->removeCallbacks(Ljava/lang/Runnable;)Z
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->M:Ljava/lang/Runnable;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->post(Ljava/lang/Runnable;)Z
return-void
.end method
.method private r()Z
.registers 6
const/4 v0, 0x0
iget-boolean v1, p0, Landroid/support/v7/widget/Toolbar;->L:Z
if-nez v1, :cond_6
:goto_5
:cond_5
return v0
:cond_6
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getChildCount()I
move-result v2
move v1, v0
:goto_b
if-ge v1, v2, :cond_26
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->getChildAt(I)Landroid/view/View;
move-result-object v3
invoke-direct {p0, v3}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v4
if-eqz v4, :cond_23
invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I
move-result v4
if-lez v4, :cond_23
invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I
move-result v3
if-gtz v3, :cond_5
:cond_23
add-int/lit8 v1, v1, 0x1
goto :goto_b
:cond_26
const/4 v0, 0x1
goto :goto_5
.end method
.method public a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/Toolbar$b;
.registers 4
new-instance v0, Landroid/support/v7/widget/Toolbar$b;
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v1
invoke-direct {v0, v1, p1}, Landroid/support/v7/widget/Toolbar$b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-object v0
.end method
.method protected a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/Toolbar$b;
.registers 3
instance-of v0, p1, Landroid/support/v7/widget/Toolbar$b;
if-eqz v0, :cond_c
new-instance v0, Landroid/support/v7/widget/Toolbar$b;
check-cast p1, Landroid/support/v7/widget/Toolbar$b;
invoke-direct {v0, p1}, Landroid/support/v7/widget/Toolbar$b;-><init>(Landroid/support/v7/widget/Toolbar$b;)V
:goto_b
return-object v0
:cond_c
instance-of v0, p1, Landroid/support/v7/a/a$a;
if-eqz v0, :cond_18
new-instance v0, Landroid/support/v7/widget/Toolbar$b;
check-cast p1, Landroid/support/v7/a/a$a;
invoke-direct {v0, p1}, Landroid/support/v7/widget/Toolbar$b;-><init>(Landroid/support/v7/a/a$a;)V
goto :goto_b
:cond_18
instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;
if-eqz v0, :cond_24
new-instance v0, Landroid/support/v7/widget/Toolbar$b;
check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;
invoke-direct {v0, p1}, Landroid/support/v7/widget/Toolbar$b;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
goto :goto_b
:cond_24
new-instance v0, Landroid/support/v7/widget/Toolbar$b;
invoke-direct {v0, p1}, Landroid/support/v7/widget/Toolbar$b;-><init>(Landroid/view/ViewGroup$LayoutParams;)V
goto :goto_b
.end method
.method public a(II)V
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->t:Landroid/support/v7/widget/aj;
invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/aj;->a(II)V
return-void
.end method
.method public a(Landroid/content/Context;I)V
.registers 4
iput p2, p0, Landroid/support/v7/widget/Toolbar;->l:I
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V
:cond_b
return-void
.end method
.method public a(Landroid/support/v7/view/menu/f;Landroid/support/v7/widget/d;)V
.registers 7
const/4 v3, 0x0
const/4 v2, 0x1
if-nez p1, :cond_9
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
if-nez v0, :cond_9
:cond_8
:goto_8
return-void
:cond_9
invoke-direct {p0}, Landroid/support/v7/widget/Toolbar;->n()V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->d()Landroid/support/v7/view/menu/f;
move-result-object v0
if-eq v0, p1, :cond_8
if-eqz v0, :cond_20
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->H:Landroid/support/v7/widget/d;
invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/f;->b(Landroid/support/v7/view/menu/l;)V
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/f;->b(Landroid/support/v7/view/menu/l;)V
:cond_20
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
if-nez v0, :cond_2b
new-instance v0, Landroid/support/v7/widget/Toolbar$a;
invoke-direct {v0, p0, v3}, Landroid/support/v7/widget/Toolbar$a;-><init>(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/widget/Toolbar$1;)V
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
:cond_2b
invoke-virtual {p2, v2}, Landroid/support/v7/widget/d;->d(Z)V
if-eqz p1, :cond_4b
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->j:Landroid/content/Context;
invoke-virtual {p1, p2, v0}, Landroid/support/v7/view/menu/f;->a(Landroid/support/v7/view/menu/l;Landroid/content/Context;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->j:Landroid/content/Context;
invoke-virtual {p1, v0, v1}, Landroid/support/v7/view/menu/f;->a(Landroid/support/v7/view/menu/l;Landroid/content/Context;)V
:goto_3c
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
iget v1, p0, Landroid/support/v7/widget/Toolbar;->k:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->setPopupTheme(I)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0, p2}, Landroid/support/v7/widget/ActionMenuView;->setPresenter(Landroid/support/v7/widget/d;)V
iput-object p2, p0, Landroid/support/v7/widget/Toolbar;->H:Landroid/support/v7/widget/d;
goto :goto_8
:cond_4b
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->j:Landroid/content/Context;
invoke-virtual {p2, v0, v3}, Landroid/support/v7/widget/d;->a(Landroid/content/Context;Landroid/support/v7/view/menu/f;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->j:Landroid/content/Context;
invoke-virtual {v0, v1, v3}, Landroid/support/v7/widget/Toolbar$a;->a(Landroid/content/Context;Landroid/support/v7/view/menu/f;)V
invoke-virtual {p2, v2}, Landroid/support/v7/widget/d;->b(Z)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
invoke-virtual {v0, v2}, Landroid/support/v7/widget/Toolbar$a;->b(Z)V
goto :goto_3c
.end method
.method public a(Landroid/support/v7/view/menu/l$a;Landroid/support/v7/view/menu/f$a;)V
.registers 4
iput-object p1, p0, Landroid/support/v7/widget/Toolbar;->J:Landroid/support/v7/view/menu/l$a;
iput-object p2, p0, Landroid/support/v7/widget/Toolbar;->K:Landroid/support/v7/view/menu/f$a;
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
if-eqz v0, :cond_d
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/support/v7/view/menu/l$a;Landroid/support/v7/view/menu/f$a;)V
:cond_d
return-void
.end method
.method public a()Z
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getVisibility()I
move-result v0
if-nez v0, :cond_14
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
if-eqz v0, :cond_14
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->a()Z
move-result v0
if-eqz v0, :cond_14
const/4 v0, 0x1
:goto_13
return v0
:cond_14
const/4 v0, 0x0
goto :goto_13
.end method
.method public b(Landroid/content/Context;I)V
.registers 4
iput p2, p0, Landroid/support/v7/widget/Toolbar;->m:I
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V
:cond_b
return-void
.end method
.method public b()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
if-eqz v0, :cond_e
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->g()Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method public c()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
if-eqz v0, :cond_e
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->h()Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
.registers 3
invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
move-result v0
if-eqz v0, :cond_c
instance-of v0, p1, Landroid/support/v7/widget/Toolbar$b;
if-eqz v0, :cond_c
const/4 v0, 0x1
:goto_b
return v0
:cond_c
const/4 v0, 0x0
goto :goto_b
.end method
.method public d()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
if-eqz v0, :cond_e
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->e()Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method public e()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
if-eqz v0, :cond_e
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->f()Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method public f()V
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->i()V
:cond_9
return-void
.end method
.method public g()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
if-eqz v0, :cond_c
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
iget-object v0, v0, Landroid/support/v7/widget/Toolbar$a;->b:Landroid/support/v7/view/menu/h;
if-eqz v0, :cond_c
const/4 v0, 0x1
:goto_b
return v0
:cond_c
const/4 v0, 0x0
goto :goto_b
.end method
.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->i()Landroid/support/v7/widget/Toolbar$b;
move-result-object v0
return-object v0
.end method
.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
.registers 3
invoke-virtual {p0, p1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/Toolbar$b;
move-result-object v0
return-object v0
.end method
.method protected synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
.registers 3
invoke-virtual {p0, p1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/Toolbar$b;
move-result-object v0
return-object v0
.end method
.method public getContentInsetEnd()I
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->t:Landroid/support/v7/widget/aj;
invoke-virtual {v0}, Landroid/support/v7/widget/aj;->d()I
move-result v0
return v0
.end method
.method public getContentInsetLeft()I
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->t:Landroid/support/v7/widget/aj;
invoke-virtual {v0}, Landroid/support/v7/widget/aj;->a()I
move-result v0
return v0
.end method
.method public getContentInsetRight()I
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->t:Landroid/support/v7/widget/aj;
invoke-virtual {v0}, Landroid/support/v7/widget/aj;->b()I
move-result v0
return v0
.end method
.method public getContentInsetStart()I
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->t:Landroid/support/v7/widget/aj;
invoke-virtual {v0}, Landroid/support/v7/widget/aj;->c()I
move-result v0
return v0
.end method
.method public getLogo()Landroid/graphics/drawable/Drawable;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;
move-result-object v0
:goto_a
return-object v0
:cond_b
const/4 v0, 0x0
goto :goto_a
.end method
.method public getLogoDescription()Ljava/lang/CharSequence;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
invoke-virtual {v0}, Landroid/widget/ImageView;->getContentDescription()Ljava/lang/CharSequence;
move-result-object v0
:goto_a
return-object v0
:cond_b
const/4 v0, 0x0
goto :goto_a
.end method
.method public getMenu()Landroid/view/Menu;
.registers 2
invoke-direct {p0}, Landroid/support/v7/widget/Toolbar;->m()V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->getMenu()Landroid/view/Menu;
move-result-object v0
return-object v0
.end method
.method public getNavigationContentDescription()Ljava/lang/CharSequence;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-virtual {v0}, Landroid/widget/ImageButton;->getContentDescription()Ljava/lang/CharSequence;
move-result-object v0
:goto_a
return-object v0
:cond_b
const/4 v0, 0x0
goto :goto_a
.end method
.method public getNavigationIcon()Landroid/graphics/drawable/Drawable;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;
move-result-object v0
:goto_a
return-object v0
:cond_b
const/4 v0, 0x0
goto :goto_a
.end method
.method public getOverflowIcon()Landroid/graphics/drawable/Drawable;
.registers 2
invoke-direct {p0}, Landroid/support/v7/widget/Toolbar;->m()V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->getOverflowIcon()Landroid/graphics/drawable/Drawable;
move-result-object v0
return-object v0
.end method
.method public getPopupTheme()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/Toolbar;->k:I
return v0
.end method
.method public getSubtitle()Ljava/lang/CharSequence;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->w:Ljava/lang/CharSequence;
return-object v0
.end method
.method public getTitle()Ljava/lang/CharSequence;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->v:Ljava/lang/CharSequence;
return-object v0
.end method
.method public getWrapper()Landroid/support/v7/widget/ac;
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->G:Landroid/support/v7/widget/as;
if-nez v0, :cond_c
new-instance v0, Landroid/support/v7/widget/as;
const/4 v1, 0x1
invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/as;-><init>(Landroid/support/v7/widget/Toolbar;Z)V
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->G:Landroid/support/v7/widget/as;
:cond_c
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->G:Landroid/support/v7/widget/as;
return-object v0
.end method
.method public h()V
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
if-nez v0, :cond_b
const/4 v0, 0x0
:goto_5
if-eqz v0, :cond_a
invoke-virtual {v0}, Landroid/support/v7/view/menu/h;->collapseActionView()Z
:cond_a
return-void
:cond_b
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
iget-object v0, v0, Landroid/support/v7/widget/Toolbar$a;->b:Landroid/support/v7/view/menu/h;
goto :goto_5
.end method
.method protected i()Landroid/support/v7/widget/Toolbar$b;
.registers 3
const/4 v1, -0x2
new-instance v0, Landroid/support/v7/widget/Toolbar$b;
invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/Toolbar$b;-><init>(II)V
return-object v0
.end method
.method  j()V
.registers 5
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getChildCount()I
move-result v0
add-int/lit8 v0, v0, -0x1
move v1, v0
:goto_7
if-ltz v1, :cond_28
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->getChildAt(I)Landroid/view/View;
move-result-object v2
invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/Toolbar$b;
iget v0, v0, Landroid/support/v7/widget/Toolbar$b;->b:I
const/4 v3, 0x2
if-eq v0, v3, :cond_24
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
if-eq v2, v0, :cond_24
invoke-virtual {p0, v1}, Landroid/support/v7/widget/Toolbar;->removeViewAt(I)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->C:Ljava/util/ArrayList;
invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
:cond_24
add-int/lit8 v0, v1, -0x1
move v1, v0
goto :goto_7
:cond_28
return-void
.end method
.method  k()V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->C:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
add-int/lit8 v0, v0, -0x1
move v1, v0
:goto_9
if-ltz v1, :cond_1a
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->C:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V
add-int/lit8 v0, v1, -0x1
move v1, v0
goto :goto_9
:cond_1a
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->C:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
return-void
.end method
.method protected onDetachedFromWindow()V
.registers 2
invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->M:Ljava/lang/Runnable;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->removeCallbacks(Ljava/lang/Runnable;)Z
return-void
.end method
.method public onHoverEvent(Landroid/view/MotionEvent;)Z
.registers 7
const/16 v4, 0x9
const/4 v3, 0x1
const/4 v2, 0x0
invoke-static {p1}, Landroid/support/v4/f/s;->a(Landroid/view/MotionEvent;)I
move-result v0
if-ne v0, v4, :cond_c
iput-boolean v2, p0, Landroid/support/v7/widget/Toolbar;->A:Z
:cond_c
iget-boolean v1, p0, Landroid/support/v7/widget/Toolbar;->A:Z
if-nez v1, :cond_1a
invoke-super {p0, p1}, Landroid/view/ViewGroup;->onHoverEvent(Landroid/view/MotionEvent;)Z
move-result v1
if-ne v0, v4, :cond_1a
if-nez v1, :cond_1a
iput-boolean v3, p0, Landroid/support/v7/widget/Toolbar;->A:Z
:cond_1a
const/16 v1, 0xa
if-eq v0, v1, :cond_21
const/4 v1, 0x3
if-ne v0, v1, :cond_23
:cond_21
iput-boolean v2, p0, Landroid/support/v7/widget/Toolbar;->A:Z
:cond_23
return v3
.end method
.method protected onLayout(ZIIII)V
.registers 29
invoke-static/range {p0 .. p0}, Landroid/support/v4/f/af;->d(Landroid/view/View;)I
move-result v3
const/4 v4, 0x1
if-ne v3, v4, :cond_254
const/4 v3, 0x1
move v5, v3
:goto_9
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/Toolbar;->getWidth()I
move-result v12
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/Toolbar;->getHeight()I
move-result v13
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/Toolbar;->getPaddingLeft()I
move-result v6
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/Toolbar;->getPaddingRight()I
move-result v14
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/Toolbar;->getPaddingTop()I
move-result v15
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/Toolbar;->getPaddingBottom()I
move-result v16
sub-int v3, v12, v14
move-object/from16 v0, p0
iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->D:[I
move-object/from16 v17, v0
const/4 v4, 0x0
const/4 v7, 0x1
const/4 v8, 0x0
aput v8, v17, v7
aput v8, v17, v4
invoke-static/range {p0 .. p0}, Landroid/support/v4/f/af;->j(Landroid/view/View;)I
move-result v18
move-object/from16 v0, p0
iget-object v4, v0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
move-object/from16 v0, p0
invoke-direct {v0, v4}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v4
if-eqz v4, :cond_41b
if-eqz v5, :cond_258
move-object/from16 v0, p0
iget-object v4, v0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
move-object/from16 v0, p0
move-object/from16 v1, v17
move/from16 v2, v18
invoke-direct {v0, v4, v3, v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Landroid/view/View;I[II)I
move-result v3
move v4, v6
:goto_51
move-object/from16 v0, p0
iget-object v7, v0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
move-object/from16 v0, p0
invoke-direct {v0, v7}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v7
if-eqz v7, :cond_6d
if-eqz v5, :cond_268
move-object/from16 v0, p0
iget-object v7, v0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
move-object/from16 v0, p0
move-object/from16 v1, v17
move/from16 v2, v18
invoke-direct {v0, v7, v3, v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Landroid/view/View;I[II)I
move-result v3
:goto_6d
:cond_6d
move-object/from16 v0, p0
iget-object v7, v0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
move-object/from16 v0, p0
invoke-direct {v0, v7}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v7
if-eqz v7, :cond_89
if-eqz v5, :cond_278
move-object/from16 v0, p0
iget-object v7, v0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
move-object/from16 v0, p0
move-object/from16 v1, v17
move/from16 v2, v18
invoke-direct {v0, v7, v4, v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;I[II)I
move-result v4
:goto_89
:cond_89
const/4 v7, 0x0
const/4 v8, 0x0
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/Toolbar;->getContentInsetLeft()I
move-result v9
sub-int/2addr v9, v4
invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I
move-result v8
aput v8, v17, v7
const/4 v7, 0x1
const/4 v8, 0x0
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/Toolbar;->getContentInsetRight()I
move-result v9
sub-int v10, v12, v14
sub-int/2addr v10, v3
sub-int/2addr v9, v10
invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I
move-result v8
aput v8, v17, v7
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/Toolbar;->getContentInsetLeft()I
move-result v7
invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I
move-result v4
sub-int v7, v12, v14
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/Toolbar;->getContentInsetRight()I
move-result v8
sub-int/2addr v7, v8
invoke-static {v3, v7}, Ljava/lang/Math;->min(II)I
move-result v3
move-object/from16 v0, p0
iget-object v7, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
move-object/from16 v0, p0
invoke-direct {v0, v7}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v7
if-eqz v7, :cond_d5
if-eqz v5, :cond_288
move-object/from16 v0, p0
iget-object v7, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
move-object/from16 v0, p0
move-object/from16 v1, v17
move/from16 v2, v18
invoke-direct {v0, v7, v3, v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Landroid/view/View;I[II)I
move-result v3
:cond_d5
:goto_d5
move-object/from16 v0, p0
iget-object v7, v0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
move-object/from16 v0, p0
invoke-direct {v0, v7}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v7
if-eqz v7, :cond_417
if-eqz v5, :cond_298
move-object/from16 v0, p0
iget-object v7, v0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
move-object/from16 v0, p0
move-object/from16 v1, v17
move/from16 v2, v18
invoke-direct {v0, v7, v3, v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Landroid/view/View;I[II)I
move-result v3
move v7, v3
move v8, v4
:goto_f3
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
move-object/from16 v0, p0
invoke-direct {v0, v3}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v19
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
move-object/from16 v0, p0
invoke-direct {v0, v3}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v20
const/4 v4, 0x0
if-eqz v19, :cond_123
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
check-cast v3, Landroid/support/v7/widget/Toolbar$b;
iget v9, v3, Landroid/support/v7/widget/Toolbar$b;->topMargin:I
move-object/from16 v0, p0
iget-object v10, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v10}, Landroid/widget/TextView;->getMeasuredHeight()I
move-result v10
add-int/2addr v9, v10
iget v3, v3, Landroid/support/v7/widget/Toolbar$b;->bottomMargin:I
add-int/2addr v3, v9
add-int/2addr v4, v3
:cond_123
if-eqz v20, :cond_414
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
check-cast v3, Landroid/support/v7/widget/Toolbar$b;
iget v9, v3, Landroid/support/v7/widget/Toolbar$b;->topMargin:I
move-object/from16 v0, p0
iget-object v10, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v10}, Landroid/widget/TextView;->getMeasuredHeight()I
move-result v10
add-int/2addr v9, v10
iget v3, v3, Landroid/support/v7/widget/Toolbar$b;->bottomMargin:I
add-int/2addr v3, v9
add-int/2addr v3, v4
move v11, v3
:goto_13f
if-nez v19, :cond_143
if-eqz v20, :cond_225
:cond_143
if-eqz v19, :cond_2aa
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
move-object v9, v3
:goto_14a
if-eqz v20, :cond_2b1
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
move-object v4, v3
:goto_151
invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
check-cast v3, Landroid/support/v7/widget/Toolbar$b;
invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v4
check-cast v4, Landroid/support/v7/widget/Toolbar$b;
if-eqz v19, :cond_169
move-object/from16 v0, p0
iget-object v9, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v9}, Landroid/widget/TextView;->getMeasuredWidth()I
move-result v9
if-gtz v9, :cond_175
:cond_169
if-eqz v20, :cond_2b8
move-object/from16 v0, p0
iget-object v9, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v9}, Landroid/widget/TextView;->getMeasuredWidth()I
move-result v9
if-lez v9, :cond_2b8
:cond_175
const/4 v9, 0x1
:goto_176
move-object/from16 v0, p0
iget v10, v0, Landroid/support/v7/widget/Toolbar;->u:I
and-int/lit8 v10, v10, 0x70
sparse-switch v10, :sswitch_data_41e
sub-int v10, v13, v15
sub-int v10, v10, v16
sub-int/2addr v10, v11
div-int/lit8 v10, v10, 0x2
iget v0, v3, Landroid/support/v7/widget/Toolbar$b;->topMargin:I
move/from16 v21, v0
move-object/from16 v0, p0
iget v0, v0, Landroid/support/v7/widget/Toolbar;->r:I
move/from16 v22, v0
add-int v21, v21, v22
move/from16 v0, v21
if-ge v10, v0, :cond_2ca
iget v3, v3, Landroid/support/v7/widget/Toolbar$b;->topMargin:I
move-object/from16 v0, p0
iget v4, v0, Landroid/support/v7/widget/Toolbar;->r:I
add-int/2addr v3, v4
:goto_19d
add-int v10, v15, v3
:goto_19f
if-eqz v5, :cond_2fb
if-eqz v9, :cond_2f8
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/Toolbar;->p:I
:goto_1a7
const/4 v4, 0x1
aget v4, v17, v4
sub-int/2addr v3, v4
const/4 v4, 0x0
invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I
move-result v4
sub-int v4, v7, v4
const/4 v5, 0x1
const/4 v7, 0x0
neg-int v3, v3
invoke-static {v7, v3}, Ljava/lang/Math;->max(II)I
move-result v3
aput v3, v17, v5
if-eqz v19, :cond_40e
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
check-cast v3, Landroid/support/v7/widget/Toolbar$b;
move-object/from16 v0, p0
iget-object v5, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I
move-result v5
sub-int v5, v4, v5
move-object/from16 v0, p0
iget-object v7, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I
move-result v7
add-int/2addr v7, v10
move-object/from16 v0, p0
iget-object v11, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v11, v5, v10, v4, v7}, Landroid/widget/TextView;->layout(IIII)V
move-object/from16 v0, p0
iget v10, v0, Landroid/support/v7/widget/Toolbar;->q:I
sub-int/2addr v5, v10
iget v3, v3, Landroid/support/v7/widget/Toolbar$b;->bottomMargin:I
add-int v10, v7, v3
move v7, v5
:goto_1eb
if-eqz v20, :cond_40b
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
check-cast v3, Landroid/support/v7/widget/Toolbar$b;
iget v5, v3, Landroid/support/v7/widget/Toolbar$b;->topMargin:I
add-int/2addr v5, v10
move-object/from16 v0, p0
iget-object v10, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v10}, Landroid/widget/TextView;->getMeasuredWidth()I
move-result v10
sub-int v10, v4, v10
move-object/from16 v0, p0
iget-object v11, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v11}, Landroid/widget/TextView;->getMeasuredHeight()I
move-result v11
add-int/2addr v11, v5
move-object/from16 v0, p0
iget-object v13, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v13, v10, v5, v4, v11}, Landroid/widget/TextView;->layout(IIII)V
move-object/from16 v0, p0
iget v5, v0, Landroid/support/v7/widget/Toolbar;->q:I
sub-int v5, v4, v5
iget v3, v3, Landroid/support/v7/widget/Toolbar$b;->bottomMargin:I
add-int/2addr v3, v11
move v3, v5
:goto_21e
if-eqz v9, :cond_408
invoke-static {v7, v3}, Ljava/lang/Math;->min(II)I
move-result v3
:goto_224
move v7, v3
:cond_225
:goto_225
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->B:Ljava/util/ArrayList;
const/4 v4, 0x3
move-object/from16 v0, p0
invoke-direct {v0, v3, v4}, Landroid/support/v7/widget/Toolbar;->a(Ljava/util/List;I)V
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->B:Ljava/util/ArrayList;
invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
move-result v9
const/4 v3, 0x0
move v5, v3
move v4, v8
:goto_23a
if-ge v5, v9, :cond_37e
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->B:Ljava/util/ArrayList;
invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v3
check-cast v3, Landroid/view/View;
move-object/from16 v0, p0
move-object/from16 v1, v17
move/from16 v2, v18
invoke-direct {v0, v3, v4, v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;I[II)I
move-result v4
add-int/lit8 v3, v5, 0x1
move v5, v3
goto :goto_23a
:cond_254
const/4 v3, 0x0
move v5, v3
goto/16 :goto_9
:cond_258
move-object/from16 v0, p0
iget-object v4, v0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
move-object/from16 v0, p0
move-object/from16 v1, v17
move/from16 v2, v18
invoke-direct {v0, v4, v6, v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;I[II)I
move-result v4
goto/16 :goto_51
:cond_268
move-object/from16 v0, p0
iget-object v7, v0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
move-object/from16 v0, p0
move-object/from16 v1, v17
move/from16 v2, v18
invoke-direct {v0, v7, v4, v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;I[II)I
move-result v4
goto/16 :goto_6d
:cond_278
move-object/from16 v0, p0
iget-object v7, v0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
move-object/from16 v0, p0
move-object/from16 v1, v17
move/from16 v2, v18
invoke-direct {v0, v7, v3, v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Landroid/view/View;I[II)I
move-result v3
goto/16 :goto_89
:cond_288
move-object/from16 v0, p0
iget-object v7, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
move-object/from16 v0, p0
move-object/from16 v1, v17
move/from16 v2, v18
invoke-direct {v0, v7, v4, v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;I[II)I
move-result v4
goto/16 :goto_d5
:cond_298
move-object/from16 v0, p0
iget-object v7, v0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
move-object/from16 v0, p0
move-object/from16 v1, v17
move/from16 v2, v18
invoke-direct {v0, v7, v4, v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;I[II)I
move-result v4
move v7, v3
move v8, v4
goto/16 :goto_f3
:cond_2aa
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
move-object v9, v3
goto/16 :goto_14a
:cond_2b1
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
move-object v4, v3
goto/16 :goto_151
:cond_2b8
const/4 v9, 0x0
goto/16 :goto_176
:sswitch_2bb
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/Toolbar;->getPaddingTop()I
move-result v4
iget v3, v3, Landroid/support/v7/widget/Toolbar$b;->topMargin:I
add-int/2addr v3, v4
move-object/from16 v0, p0
iget v4, v0, Landroid/support/v7/widget/Toolbar;->r:I
add-int v10, v3, v4
goto/16 :goto_19f
:cond_2ca
sub-int v13, v13, v16
sub-int v11, v13, v11
sub-int/2addr v11, v10
sub-int/2addr v11, v15
iget v3, v3, Landroid/support/v7/widget/Toolbar$b;->bottomMargin:I
move-object/from16 v0, p0
iget v13, v0, Landroid/support/v7/widget/Toolbar;->s:I
add-int/2addr v3, v13
if-ge v11, v3, :cond_411
const/4 v3, 0x0
iget v4, v4, Landroid/support/v7/widget/Toolbar$b;->bottomMargin:I
move-object/from16 v0, p0
iget v13, v0, Landroid/support/v7/widget/Toolbar;->s:I
add-int/2addr v4, v13
sub-int/2addr v4, v11
sub-int v4, v10, v4
invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I
move-result v3
goto/16 :goto_19d
:sswitch_2ea
sub-int v3, v13, v16
iget v4, v4, Landroid/support/v7/widget/Toolbar$b;->bottomMargin:I
sub-int/2addr v3, v4
move-object/from16 v0, p0
iget v4, v0, Landroid/support/v7/widget/Toolbar;->s:I
sub-int/2addr v3, v4
sub-int v10, v3, v11
goto/16 :goto_19f
:cond_2f8
const/4 v3, 0x0
goto/16 :goto_1a7
:cond_2fb
if-eqz v9, :cond_37c
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/Toolbar;->p:I
:goto_301
const/4 v4, 0x0
aget v4, v17, v4
sub-int/2addr v3, v4
const/4 v4, 0x0
invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I
move-result v4
add-int/2addr v8, v4
const/4 v4, 0x0
const/4 v5, 0x0
neg-int v3, v3
invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I
move-result v3
aput v3, v17, v4
if-eqz v19, :cond_404
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
check-cast v3, Landroid/support/v7/widget/Toolbar$b;
move-object/from16 v0, p0
iget-object v4, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I
move-result v4
add-int/2addr v4, v8
move-object/from16 v0, p0
iget-object v5, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I
move-result v5
add-int/2addr v5, v10
move-object/from16 v0, p0
iget-object v11, v0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v11, v8, v10, v4, v5}, Landroid/widget/TextView;->layout(IIII)V
move-object/from16 v0, p0
iget v10, v0, Landroid/support/v7/widget/Toolbar;->q:I
add-int/2addr v4, v10
iget v3, v3, Landroid/support/v7/widget/Toolbar$b;->bottomMargin:I
add-int/2addr v3, v5
move v5, v4
move v4, v3
:goto_343
if-eqz v20, :cond_401
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
check-cast v3, Landroid/support/v7/widget/Toolbar$b;
iget v10, v3, Landroid/support/v7/widget/Toolbar$b;->topMargin:I
add-int/2addr v4, v10
move-object/from16 v0, p0
iget-object v10, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v10}, Landroid/widget/TextView;->getMeasuredWidth()I
move-result v10
add-int/2addr v10, v8
move-object/from16 v0, p0
iget-object v11, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v11}, Landroid/widget/TextView;->getMeasuredHeight()I
move-result v11
add-int/2addr v11, v4
move-object/from16 v0, p0
iget-object v13, v0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v13, v8, v4, v10, v11}, Landroid/widget/TextView;->layout(IIII)V
move-object/from16 v0, p0
iget v4, v0, Landroid/support/v7/widget/Toolbar;->q:I
add-int/2addr v4, v10
iget v3, v3, Landroid/support/v7/widget/Toolbar$b;->bottomMargin:I
add-int/2addr v3, v11
move v3, v4
:goto_374
if-eqz v9, :cond_225
invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I
move-result v8
goto/16 :goto_225
:cond_37c
const/4 v3, 0x0
goto :goto_301
:cond_37e
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->B:Ljava/util/ArrayList;
const/4 v5, 0x5
move-object/from16 v0, p0
invoke-direct {v0, v3, v5}, Landroid/support/v7/widget/Toolbar;->a(Ljava/util/List;I)V
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->B:Ljava/util/ArrayList;
invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
move-result v8
const/4 v3, 0x0
move v5, v3
:goto_392
if-ge v5, v8, :cond_3ac
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->B:Ljava/util/ArrayList;
invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v3
check-cast v3, Landroid/view/View;
move-object/from16 v0, p0
move-object/from16 v1, v17
move/from16 v2, v18
invoke-direct {v0, v3, v7, v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Landroid/view/View;I[II)I
move-result v7
add-int/lit8 v3, v5, 0x1
move v5, v3
goto :goto_392
:cond_3ac
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->B:Ljava/util/ArrayList;
const/4 v5, 0x1
move-object/from16 v0, p0
invoke-direct {v0, v3, v5}, Landroid/support/v7/widget/Toolbar;->a(Ljava/util/List;I)V
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->B:Ljava/util/ArrayList;
move-object/from16 v0, p0
move-object/from16 v1, v17
invoke-direct {v0, v3, v1}, Landroid/support/v7/widget/Toolbar;->a(Ljava/util/List;[I)I
move-result v5
sub-int v3, v12, v6
sub-int/2addr v3, v14
div-int/lit8 v3, v3, 0x2
add-int/2addr v3, v6
div-int/lit8 v6, v5, 0x2
sub-int/2addr v3, v6
add-int/2addr v5, v3
if-ge v3, v4, :cond_3f3
move v3, v4
:cond_3cf
:goto_3cf
move-object/from16 v0, p0
iget-object v4, v0, Landroid/support/v7/widget/Toolbar;->B:Ljava/util/ArrayList;
invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
move-result v6
const/4 v4, 0x0
move v5, v3
:goto_3d9
if-ge v4, v6, :cond_3f9
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->B:Ljava/util/ArrayList;
invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v3
check-cast v3, Landroid/view/View;
move-object/from16 v0, p0
move-object/from16 v1, v17
move/from16 v2, v18
invoke-direct {v0, v3, v5, v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;I[II)I
move-result v5
add-int/lit8 v3, v4, 0x1
move v4, v3
goto :goto_3d9
:cond_3f3
if-le v5, v7, :cond_3cf
sub-int v4, v5, v7
sub-int/2addr v3, v4
goto :goto_3cf
:cond_3f9
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->B:Ljava/util/ArrayList;
invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V
return-void
:cond_401
move v3, v8
goto/16 :goto_374
:cond_404
move v5, v8
move v4, v10
goto/16 :goto_343
:cond_408
move v3, v4
goto/16 :goto_224
:cond_40b
move v3, v4
goto/16 :goto_21e
:cond_40e
move v7, v4
goto/16 :goto_1eb
:cond_411
move v3, v10
goto/16 :goto_19d
:cond_414
move v11, v4
goto/16 :goto_13f
:cond_417
move v7, v3
move v8, v4
goto/16 :goto_f3
:cond_41b
move v4, v6
goto/16 :goto_51
:sswitch_data_41e
.sparse-switch
0x30 -> :sswitch_2bb
0x50 -> :sswitch_2ea
.end sparse-switch
.end method
.method protected onMeasure(II)V
.registers 16
const/4 v3, 0x0
const/4 v11, 0x0
const/4 v9, 0x0
iget-object v10, p0, Landroid/support/v7/widget/Toolbar;->D:[I
invoke-static {p0}, Landroid/support/v7/widget/au;->a(Landroid/view/View;)Z
move-result v0
if-eqz v0, :cond_16c
const/4 v1, 0x1
const/4 v0, 0x0
move v7, v0
move v8, v1
:goto_f
const/4 v0, 0x0
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-direct {p0, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v1
if-eqz v1, :cond_4d
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
const/4 v5, 0x0
iget v6, p0, Landroid/support/v7/widget/Toolbar;->o:I
move-object v0, p0
move v2, p1
move v4, p2
invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;IIIII)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-virtual {v0}, Landroid/widget/ImageButton;->getMeasuredWidth()I
move-result v0
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-direct {p0, v1}, Landroid/support/v7/widget/Toolbar;->b(Landroid/view/View;)I
move-result v1
add-int/2addr v0, v1
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-virtual {v1}, Landroid/widget/ImageButton;->getMeasuredHeight()I
move-result v1
iget-object v2, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-direct {p0, v2}, Landroid/support/v7/widget/Toolbar;->c(Landroid/view/View;)I
move-result v2
add-int/2addr v1, v2
invoke-static {v11, v1}, Ljava/lang/Math;->max(II)I
move-result v2
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-static {v1}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v1
invoke-static {v9, v1}, Landroid/support/v7/widget/au;->a(II)I
move-result v1
move v9, v1
move v11, v2
:cond_4d
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
invoke-direct {p0, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v1
if-eqz v1, :cond_88
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
const/4 v5, 0x0
iget v6, p0, Landroid/support/v7/widget/Toolbar;->o:I
move-object v0, p0
move v2, p1
move v4, p2
invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;IIIII)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
invoke-virtual {v0}, Landroid/widget/ImageButton;->getMeasuredWidth()I
move-result v0
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
invoke-direct {p0, v1}, Landroid/support/v7/widget/Toolbar;->b(Landroid/view/View;)I
move-result v1
add-int/2addr v0, v1
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
invoke-virtual {v1}, Landroid/widget/ImageButton;->getMeasuredHeight()I
move-result v1
iget-object v2, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
invoke-direct {p0, v2}, Landroid/support/v7/widget/Toolbar;->c(Landroid/view/View;)I
move-result v2
add-int/2addr v1, v2
invoke-static {v11, v1}, Ljava/lang/Math;->max(II)I
move-result v11
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->i:Landroid/widget/ImageButton;
invoke-static {v1}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v1
invoke-static {v9, v1}, Landroid/support/v7/widget/au;->a(II)I
move-result v9
:cond_88
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContentInsetStart()I
move-result v1
invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I
move-result v2
add-int/2addr v3, v2
const/4 v2, 0x0
sub-int v0, v1, v0
invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I
move-result v0
aput v0, v10, v8
const/4 v0, 0x0
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-direct {p0, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v1
if-eqz v1, :cond_d6
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
const/4 v5, 0x0
iget v6, p0, Landroid/support/v7/widget/Toolbar;->o:I
move-object v0, p0
move v2, p1
move v4, p2
invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;IIIII)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->getMeasuredWidth()I
move-result v0
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-direct {p0, v1}, Landroid/support/v7/widget/Toolbar;->b(Landroid/view/View;)I
move-result v1
add-int/2addr v0, v1
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v1}, Landroid/support/v7/widget/ActionMenuView;->getMeasuredHeight()I
move-result v1
iget-object v2, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-direct {p0, v2}, Landroid/support/v7/widget/Toolbar;->c(Landroid/view/View;)I
move-result v2
add-int/2addr v1, v2
invoke-static {v11, v1}, Ljava/lang/Math;->max(II)I
move-result v11
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-static {v1}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v1
invoke-static {v9, v1}, Landroid/support/v7/widget/au;->a(II)I
move-result v9
:cond_d6
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContentInsetEnd()I
move-result v1
invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I
move-result v2
add-int/2addr v3, v2
const/4 v2, 0x0
sub-int v0, v1, v0
invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I
move-result v0
aput v0, v10, v7
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
invoke-direct {p0, v0}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v0
if-eqz v0, :cond_117
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
const/4 v5, 0x0
move-object v0, p0
move v2, p1
move v4, p2
move-object v6, v10
invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;IIII[I)I
move-result v0
add-int/2addr v3, v0
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I
move-result v0
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
invoke-direct {p0, v1}, Landroid/support/v7/widget/Toolbar;->c(Landroid/view/View;)I
move-result v1
add-int/2addr v0, v1
invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I
move-result v11
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
invoke-static {v0}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v0
invoke-static {v9, v0}, Landroid/support/v7/widget/au;->a(II)I
move-result v9
:cond_117
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
invoke-direct {p0, v0}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v0
if-eqz v0, :cond_146
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
const/4 v5, 0x0
move-object v0, p0
move v2, p1
move v4, p2
move-object v6, v10
invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;IIII[I)I
move-result v0
add-int/2addr v3, v0
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I
move-result v0
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
invoke-direct {p0, v1}, Landroid/support/v7/widget/Toolbar;->c(Landroid/view/View;)I
move-result v1
add-int/2addr v0, v1
invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I
move-result v11
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
invoke-static {v0}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v0
invoke-static {v9, v0}, Landroid/support/v7/widget/au;->a(II)I
move-result v9
:cond_146
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getChildCount()I
move-result v8
const/4 v0, 0x0
move v7, v0
move v12, v11
move v11, v9
:goto_14e
if-ge v7, v8, :cond_193
invoke-virtual {p0, v7}, Landroid/support/v7/widget/Toolbar;->getChildAt(I)Landroid/view/View;
move-result-object v1
invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/Toolbar$b;
iget v0, v0, Landroid/support/v7/widget/Toolbar$b;->b:I
if-nez v0, :cond_247
invoke-direct {p0, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v0
if-nez v0, :cond_172
move v0, v11
move v1, v12
:goto_166
add-int/lit8 v2, v7, 0x1
move v7, v2
move v11, v0
move v12, v1
goto :goto_14e
:cond_16c
const/4 v1, 0x0
const/4 v0, 0x1
move v7, v0
move v8, v1
goto/16 :goto_f
:cond_172
const/4 v5, 0x0
move-object v0, p0
move v2, p1
move v4, p2
move-object v6, v10
invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;IIII[I)I
move-result v0
add-int/2addr v3, v0
invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I
move-result v0
invoke-direct {p0, v1}, Landroid/support/v7/widget/Toolbar;->c(Landroid/view/View;)I
move-result v2
add-int/2addr v0, v2
invoke-static {v12, v0}, Ljava/lang/Math;->max(II)I
move-result v2
invoke-static {v1}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v0
invoke-static {v11, v0}, Landroid/support/v7/widget/au;->a(II)I
move-result v0
move v1, v2
goto :goto_166
:cond_193
const/4 v1, 0x0
const/4 v0, 0x0
iget v2, p0, Landroid/support/v7/widget/Toolbar;->r:I
iget v4, p0, Landroid/support/v7/widget/Toolbar;->s:I
add-int v9, v2, v4
iget v2, p0, Landroid/support/v7/widget/Toolbar;->p:I
iget v4, p0, Landroid/support/v7/widget/Toolbar;->q:I
add-int/2addr v2, v4
iget-object v4, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-direct {p0, v4}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v4
if-eqz v4, :cond_1d6
iget-object v5, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
add-int v7, v3, v2
move-object v4, p0
move v6, p1
move v8, p2
invoke-direct/range {v4 .. v10}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;IIII[I)I
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I
move-result v0
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-direct {p0, v1}, Landroid/support/v7/widget/Toolbar;->b(Landroid/view/View;)I
move-result v1
add-int/2addr v1, v0
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I
move-result v0
iget-object v4, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-direct {p0, v4}, Landroid/support/v7/widget/Toolbar;->c(Landroid/view/View;)I
move-result v4
add-int/2addr v0, v4
iget-object v4, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-static {v4}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v4
invoke-static {v11, v4}, Landroid/support/v7/widget/au;->a(II)I
move-result v11
:cond_1d6
iget-object v4, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-direct {p0, v4}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)Z
move-result v4
if-eqz v4, :cond_206
iget-object v5, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
add-int v7, v3, v2
add-int/2addr v9, v0
move-object v4, p0
move v6, p1
move v8, p2
invoke-direct/range {v4 .. v10}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;IIII[I)I
move-result v2
invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I
move-result v1
iget-object v2, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I
move-result v2
iget-object v4, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-direct {p0, v4}, Landroid/support/v7/widget/Toolbar;->c(Landroid/view/View;)I
move-result v4
add-int/2addr v2, v4
add-int/2addr v0, v2
iget-object v2, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-static {v2}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v2
invoke-static {v11, v2}, Landroid/support/v7/widget/au;->a(II)I
move-result v11
:cond_206
add-int/2addr v1, v3
invoke-static {v12, v0}, Ljava/lang/Math;->max(II)I
move-result v0
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingLeft()I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingRight()I
move-result v3
add-int/2addr v2, v3
add-int/2addr v1, v2
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingTop()I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getPaddingBottom()I
move-result v3
add-int/2addr v2, v3
add-int/2addr v0, v2
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getSuggestedMinimumWidth()I
move-result v2
invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I
move-result v1
const/high16 v2, -0x100
and-int/2addr v2, v11
invoke-static {v1, p1, v2}, Landroid/support/v4/f/af;->a(III)I
move-result v1
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getSuggestedMinimumHeight()I
move-result v2
invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I
move-result v0
shl-int/lit8 v2, v11, 0x10
invoke-static {v0, p2, v2}, Landroid/support/v4/f/af;->a(III)I
move-result v0
invoke-direct {p0}, Landroid/support/v7/widget/Toolbar;->r()Z
move-result v2
if-eqz v2, :cond_243
const/4 v0, 0x0
:cond_243
invoke-virtual {p0, v1, v0}, Landroid/support/v7/widget/Toolbar;->setMeasuredDimension(II)V
return-void
:cond_247
move v0, v11
move v1, v12
goto/16 :goto_166
.end method
.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
.registers 4
instance-of v0, p1, Landroid/support/v7/widget/Toolbar$SavedState;
if-nez v0, :cond_8
invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V
:goto_7
:cond_7
return-void
:cond_8
check-cast p1, Landroid/support/v7/widget/Toolbar$SavedState;
invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar$SavedState;->getSuperState()Landroid/os/Parcelable;
move-result-object v0
invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
if-eqz v0, :cond_38
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->d()Landroid/support/v7/view/menu/f;
move-result-object v0
:goto_1b
iget v1, p1, Landroid/support/v7/widget/Toolbar$SavedState;->a:I
if-eqz v1, :cond_30
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
if-eqz v1, :cond_30
if-eqz v0, :cond_30
iget v1, p1, Landroid/support/v7/widget/Toolbar$SavedState;->a:I
invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;
move-result-object v0
if-eqz v0, :cond_30
invoke-static {v0}, Landroid/support/v4/f/p;->b(Landroid/view/MenuItem;)Z
:cond_30
iget-boolean v0, p1, Landroid/support/v7/widget/Toolbar$SavedState;->b:Z
if-eqz v0, :cond_7
invoke-direct {p0}, Landroid/support/v7/widget/Toolbar;->q()V
goto :goto_7
:cond_38
const/4 v0, 0x0
goto :goto_1b
.end method
.method public onRtlPropertiesChanged(I)V
.registers 5
const/4 v0, 0x1
sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v2, 0x11
if-lt v1, v2, :cond_a
invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRtlPropertiesChanged(I)V
:cond_a
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->t:Landroid/support/v7/widget/aj;
if-ne p1, v0, :cond_12
:goto_e
invoke-virtual {v1, v0}, Landroid/support/v7/widget/aj;->a(Z)V
return-void
:cond_12
const/4 v0, 0x0
goto :goto_e
.end method
.method protected onSaveInstanceState()Landroid/os/Parcelable;
.registers 3
new-instance v0, Landroid/support/v7/widget/Toolbar$SavedState;
invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;
move-result-object v1
invoke-direct {v0, v1}, Landroid/support/v7/widget/Toolbar$SavedState;-><init>(Landroid/os/Parcelable;)V
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
if-eqz v1, :cond_1d
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
iget-object v1, v1, Landroid/support/v7/widget/Toolbar$a;->b:Landroid/support/v7/view/menu/h;
if-eqz v1, :cond_1d
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->I:Landroid/support/v7/widget/Toolbar$a;
iget-object v1, v1, Landroid/support/v7/widget/Toolbar$a;->b:Landroid/support/v7/view/menu/h;
invoke-virtual {v1}, Landroid/support/v7/view/menu/h;->getItemId()I
move-result v1
iput v1, v0, Landroid/support/v7/widget/Toolbar$SavedState;->a:I
:cond_1d
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->b()Z
move-result v1
iput-boolean v1, v0, Landroid/support/v7/widget/Toolbar$SavedState;->b:Z
return-object v0
.end method
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
.registers 6
const/4 v3, 0x0
const/4 v2, 0x1
invoke-static {p1}, Landroid/support/v4/f/s;->a(Landroid/view/MotionEvent;)I
move-result v0
if-nez v0, :cond_a
iput-boolean v3, p0, Landroid/support/v7/widget/Toolbar;->z:Z
:cond_a
iget-boolean v1, p0, Landroid/support/v7/widget/Toolbar;->z:Z
if-nez v1, :cond_18
invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z
move-result v1
if-nez v0, :cond_18
if-nez v1, :cond_18
iput-boolean v2, p0, Landroid/support/v7/widget/Toolbar;->z:Z
:cond_18
if-eq v0, v2, :cond_1d
const/4 v1, 0x3
if-ne v0, v1, :cond_1f
:cond_1d
iput-boolean v3, p0, Landroid/support/v7/widget/Toolbar;->z:Z
:cond_1f
return v2
.end method
.method public setCollapsible(Z)V
.registers 2
iput-boolean p1, p0, Landroid/support/v7/widget/Toolbar;->L:Z
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->requestLayout()V
return-void
.end method
.method public setLogo(I)V
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->N:Landroid/support/v7/widget/l;
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v1
invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->setLogo(Landroid/graphics/drawable/Drawable;)V
return-void
.end method
.method public setLogo(Landroid/graphics/drawable/Drawable;)V
.registers 4
if-eqz p1, :cond_1d
invoke-direct {p0}, Landroid/support/v7/widget/Toolbar;->l()V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
invoke-direct {p0, v0}, Landroid/support/v7/widget/Toolbar;->d(Landroid/view/View;)Z
move-result v0
if-nez v0, :cond_13
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
const/4 v1, 0x1
invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;Z)V
:cond_13
:goto_13
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
if-eqz v0, :cond_1c
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
:cond_1c
return-void
:cond_1d
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
if-eqz v0, :cond_13
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
invoke-direct {p0, v0}, Landroid/support/v7/widget/Toolbar;->d(Landroid/view/View;)Z
move-result v0
if-eqz v0, :cond_13
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->C:Ljava/util/ArrayList;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
goto :goto_13
.end method
.method public setLogoDescription(I)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->setLogoDescription(Ljava/lang/CharSequence;)V
return-void
.end method
.method public setLogoDescription(Ljava/lang/CharSequence;)V
.registers 3
invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-nez v0, :cond_9
invoke-direct {p0}, Landroid/support/v7/widget/Toolbar;->l()V
:cond_9
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
if-eqz v0, :cond_12
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->f:Landroid/widget/ImageView;
invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V
:cond_12
return-void
.end method
.method public setNavigationContentDescription(I)V
.registers 3
if-eqz p1, :cond_e
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;
move-result-object v0
:goto_a
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(Ljava/lang/CharSequence;)V
return-void
:cond_e
const/4 v0, 0x0
goto :goto_a
.end method
.method public setNavigationContentDescription(Ljava/lang/CharSequence;)V
.registers 3
invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-nez v0, :cond_9
invoke-direct {p0}, Landroid/support/v7/widget/Toolbar;->o()V
:cond_9
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
if-eqz v0, :cond_12
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V
:cond_12
return-void
.end method
.method public setNavigationIcon(I)V
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->N:Landroid/support/v7/widget/l;
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v1
invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V
return-void
.end method
.method public setNavigationIcon(Landroid/graphics/drawable/Drawable;)V
.registers 4
if-eqz p1, :cond_1d
invoke-direct {p0}, Landroid/support/v7/widget/Toolbar;->o()V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-direct {p0, v0}, Landroid/support/v7/widget/Toolbar;->d(Landroid/view/View;)Z
move-result v0
if-nez v0, :cond_13
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
const/4 v1, 0x1
invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;Z)V
:cond_13
:goto_13
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
if-eqz v0, :cond_1c
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
:cond_1c
return-void
:cond_1d
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
if-eqz v0, :cond_13
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-direct {p0, v0}, Landroid/support/v7/widget/Toolbar;->d(Landroid/view/View;)Z
move-result v0
if-eqz v0, :cond_13
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->C:Ljava/util/ArrayList;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
goto :goto_13
.end method
.method public setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V
.registers 3
invoke-direct {p0}, Landroid/support/v7/widget/Toolbar;->o()V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->e:Landroid/widget/ImageButton;
invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V
return-void
.end method
.method public setOnMenuItemClickListener(Landroid/support/v7/widget/Toolbar$c;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/widget/Toolbar;->E:Landroid/support/v7/widget/Toolbar$c;
return-void
.end method
.method public setOverflowIcon(Landroid/graphics/drawable/Drawable;)V
.registers 3
invoke-direct {p0}, Landroid/support/v7/widget/Toolbar;->m()V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->b:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v0, p1}, Landroid/support/v7/widget/ActionMenuView;->setOverflowIcon(Landroid/graphics/drawable/Drawable;)V
return-void
.end method
.method public setPopupTheme(I)V
.registers 4
iget v0, p0, Landroid/support/v7/widget/Toolbar;->k:I
if-eq v0, p1, :cond_e
iput p1, p0, Landroid/support/v7/widget/Toolbar;->k:I
if-nez p1, :cond_f
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->j:Landroid/content/Context;
:cond_e
:goto_e
return-void
:cond_f
new-instance v0, Landroid/view/ContextThemeWrapper;
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v1
invoke-direct {v0, v1, p1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V
iput-object v0, p0, Landroid/support/v7/widget/Toolbar;->j:Landroid/content/Context;
goto :goto_e
.end method
.method public setSubtitle(I)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->setSubtitle(Ljava/lang/CharSequence;)V
return-void
.end method
.method public setSubtitle(Ljava/lang/CharSequence;)V
.registers 5
invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-nez v0, :cond_51
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
if-nez v0, :cond_37
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v0
new-instance v1, Landroid/widget/TextView;
invoke-direct {v1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V
iput-object v1, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;
invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
iget v1, p0, Landroid/support/v7/widget/Toolbar;->m:I
if-eqz v1, :cond_2c
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
iget v2, p0, Landroid/support/v7/widget/Toolbar;->m:I
invoke-virtual {v1, v0, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V
:cond_2c
iget v0, p0, Landroid/support/v7/widget/Toolbar;->y:I
if-eqz v0, :cond_37
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
iget v1, p0, Landroid/support/v7/widget/Toolbar;->y:I
invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V
:cond_37
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-direct {p0, v0}, Landroid/support/v7/widget/Toolbar;->d(Landroid/view/View;)Z
move-result v0
if-nez v0, :cond_45
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
const/4 v1, 0x1
invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;Z)V
:goto_45
:cond_45
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
if-eqz v0, :cond_4e
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
:cond_4e
iput-object p1, p0, Landroid/support/v7/widget/Toolbar;->w:Ljava/lang/CharSequence;
return-void
:cond_51
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
if-eqz v0, :cond_45
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-direct {p0, v0}, Landroid/support/v7/widget/Toolbar;->d(Landroid/view/View;)Z
move-result v0
if-eqz v0, :cond_45
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->C:Ljava/util/ArrayList;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
goto :goto_45
.end method
.method public setSubtitleTextColor(I)V
.registers 3
iput p1, p0, Landroid/support/v7/widget/Toolbar;->y:I
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->d:Landroid/widget/TextView;
invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V
:cond_b
return-void
.end method
.method public setTitle(I)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V
return-void
.end method
.method public setTitle(Ljava/lang/CharSequence;)V
.registers 5
invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-nez v0, :cond_51
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
if-nez v0, :cond_37
invoke-virtual {p0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;
move-result-object v0
new-instance v1, Landroid/widget/TextView;
invoke-direct {v1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V
iput-object v1, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;
invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
iget v1, p0, Landroid/support/v7/widget/Toolbar;->l:I
if-eqz v1, :cond_2c
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
iget v2, p0, Landroid/support/v7/widget/Toolbar;->l:I
invoke-virtual {v1, v0, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V
:cond_2c
iget v0, p0, Landroid/support/v7/widget/Toolbar;->x:I
if-eqz v0, :cond_37
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
iget v1, p0, Landroid/support/v7/widget/Toolbar;->x:I
invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V
:cond_37
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-direct {p0, v0}, Landroid/support/v7/widget/Toolbar;->d(Landroid/view/View;)Z
move-result v0
if-nez v0, :cond_45
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
const/4 v1, 0x1
invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;Z)V
:goto_45
:cond_45
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
if-eqz v0, :cond_4e
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
:cond_4e
iput-object p1, p0, Landroid/support/v7/widget/Toolbar;->v:Ljava/lang/CharSequence;
return-void
:cond_51
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
if-eqz v0, :cond_45
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-direct {p0, v0}, Landroid/support/v7/widget/Toolbar;->d(Landroid/view/View;)Z
move-result v0
if-eqz v0, :cond_45
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->C:Ljava/util/ArrayList;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
goto :goto_45
.end method
.method public setTitleTextColor(I)V
.registers 3
iput p1, p0, Landroid/support/v7/widget/Toolbar;->x:I
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/TextView;
invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V
:cond_b
return-void
.end method