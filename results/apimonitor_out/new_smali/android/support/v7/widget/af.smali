.class public Landroid/support/v7/widget/af;
.super Landroid/view/ViewGroup;
.source "LinearLayoutCompat.java"
.field private a:Z
.field private b:I
.field private c:I
.field private d:I
.field private e:I
.field private f:I
.field private g:F
.field private h:Z
.field private i:[I
.field private j:[I
.field private k:Landroid/graphics/drawable/Drawable;
.field private l:I
.field private m:I
.field private n:I
.field private o:I
.method public constructor <init>(Landroid/content/Context;)V
.registers 3
const/4 v0, 0x0
invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/af;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 4
const/4 v0, 0x0
invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/af;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.registers 9
const/4 v2, 0x1
const/4 v4, -0x1
const/4 v3, 0x0
invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
iput-boolean v2, p0, Landroid/support/v7/widget/af;->a:Z
iput v4, p0, Landroid/support/v7/widget/af;->b:I
iput v3, p0, Landroid/support/v7/widget/af;->c:I
const v0, 0x800033
iput v0, p0, Landroid/support/v7/widget/af;->e:I
sget-object v0, Landroid/support/v7/b/a$k;->LinearLayoutCompat:[I
invoke-static {p1, p2, v0, p3, v3}, Landroid/support/v7/widget/ar;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/widget/ar;
move-result-object v0
sget v1, Landroid/support/v7/b/a$k;->LinearLayoutCompat_android_orientation:I
invoke-virtual {v0, v1, v4}, Landroid/support/v7/widget/ar;->a(II)I
move-result v1
if-ltz v1, :cond_22
invoke-virtual {p0, v1}, Landroid/support/v7/widget/af;->setOrientation(I)V
:cond_22
sget v1, Landroid/support/v7/b/a$k;->LinearLayoutCompat_android_gravity:I
invoke-virtual {v0, v1, v4}, Landroid/support/v7/widget/ar;->a(II)I
move-result v1
if-ltz v1, :cond_2d
invoke-virtual {p0, v1}, Landroid/support/v7/widget/af;->setGravity(I)V
:cond_2d
sget v1, Landroid/support/v7/b/a$k;->LinearLayoutCompat_android_baselineAligned:I
invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ar;->a(IZ)Z
move-result v1
if-nez v1, :cond_38
invoke-virtual {p0, v1}, Landroid/support/v7/widget/af;->setBaselineAligned(Z)V
:cond_38
sget v1, Landroid/support/v7/b/a$k;->LinearLayoutCompat_android_weightSum:I
const/high16 v2, -0x4080
invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ar;->a(IF)F
move-result v1
iput v1, p0, Landroid/support/v7/widget/af;->g:F
sget v1, Landroid/support/v7/b/a$k;->LinearLayoutCompat_android_baselineAlignedChildIndex:I
invoke-virtual {v0, v1, v4}, Landroid/support/v7/widget/ar;->a(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/af;->b:I
sget v1, Landroid/support/v7/b/a$k;->LinearLayoutCompat_measureWithLargestChild:I
invoke-virtual {v0, v1, v3}, Landroid/support/v7/widget/ar;->a(IZ)Z
move-result v1
iput-boolean v1, p0, Landroid/support/v7/widget/af;->h:Z
sget v1, Landroid/support/v7/b/a$k;->LinearLayoutCompat_divider:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->a(I)Landroid/graphics/drawable/Drawable;
move-result-object v1
invoke-virtual {p0, v1}, Landroid/support/v7/widget/af;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V
sget v1, Landroid/support/v7/b/a$k;->LinearLayoutCompat_showDividers:I
invoke-virtual {v0, v1, v3}, Landroid/support/v7/widget/ar;->a(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/af;->n:I
sget v1, Landroid/support/v7/b/a$k;->LinearLayoutCompat_dividerPadding:I
invoke-virtual {v0, v1, v3}, Landroid/support/v7/widget/ar;->e(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/af;->o:I
invoke-virtual {v0}, Landroid/support/v7/widget/ar;->a()V
return-void
.end method
.method private a(Landroid/view/View;IIII)V
.registers 8
add-int v0, p2, p4
add-int v1, p3, p5
invoke-virtual {p1, p2, p3, v0, v1}, Landroid/view/View;->layout(IIII)V
return-void
.end method
.method private c(II)V
.registers 12
const/4 v3, 0x0
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getMeasuredWidth()I
move-result v0
const/high16 v1, 0x4000
invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v2
move v7, v3
:goto_c
if-ge v7, p1, :cond_3a
invoke-virtual {p0, v7}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v1
invoke-virtual {v1}, Landroid/view/View;->getVisibility()I
move-result v0
const/16 v4, 0x8
if-eq v0, v4, :cond_36
invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
move-object v6, v0
check-cast v6, Landroid/support/v7/widget/af$a;
iget v0, v6, Landroid/support/v7/widget/af$a;->width:I
const/4 v4, -0x1
if-ne v0, v4, :cond_36
iget v8, v6, Landroid/support/v7/widget/af$a;->height:I
invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I
move-result v0
iput v0, v6, Landroid/support/v7/widget/af$a;->height:I
move-object v0, p0
move v4, p2
move v5, v3
invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/af;->measureChildWithMargins(Landroid/view/View;IIII)V
iput v8, v6, Landroid/support/v7/widget/af$a;->height:I
:cond_36
add-int/lit8 v0, v7, 0x1
move v7, v0
goto :goto_c
:cond_3a
return-void
.end method
.method private d(II)V
.registers 12
const/4 v3, 0x0
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getMeasuredHeight()I
move-result v0
const/high16 v1, 0x4000
invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v4
move v7, v3
:goto_c
if-ge v7, p1, :cond_3a
invoke-virtual {p0, v7}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v1
invoke-virtual {v1}, Landroid/view/View;->getVisibility()I
move-result v0
const/16 v2, 0x8
if-eq v0, v2, :cond_36
invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
move-object v6, v0
check-cast v6, Landroid/support/v7/widget/af$a;
iget v0, v6, Landroid/support/v7/widget/af$a;->height:I
const/4 v2, -0x1
if-ne v0, v2, :cond_36
iget v8, v6, Landroid/support/v7/widget/af$a;->width:I
invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I
move-result v0
iput v0, v6, Landroid/support/v7/widget/af$a;->width:I
move-object v0, p0
move v2, p2
move v5, v3
invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/af;->measureChildWithMargins(Landroid/view/View;IIII)V
iput v8, v6, Landroid/support/v7/widget/af$a;->width:I
:cond_36
add-int/lit8 v0, v7, 0x1
move v7, v0
goto :goto_c
:cond_3a
return-void
.end method
.method  a(Landroid/view/View;)I
.registers 3
const/4 v0, 0x0
return v0
.end method
.method  a(Landroid/view/View;I)I
.registers 4
const/4 v0, 0x0
return v0
.end method
.method  a(II)V
.registers 30
const/4 v3, 0x0
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
const/16 v19, 0x0
const/16 v18, 0x0
const/4 v13, 0x0
const/4 v12, 0x0
const/16 v17, 0x1
const/4 v6, 0x0
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getVirtualChildCount()I
move-result v21
invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I
move-result v22
invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I
move-result v23
const/4 v11, 0x0
const/4 v15, 0x0
move-object/from16 v0, p0
iget v0, v0, Landroid/support/v7/widget/af;->b:I
move/from16 v24, v0
move-object/from16 v0, p0
iget-boolean v0, v0, Landroid/support/v7/widget/af;->h:Z
move/from16 v25, v0
const/high16 v14, -0x8000
const/4 v5, 0x0
:goto_2b
move/from16 v0, v21
if-ge v5, v0, :cond_196
move-object/from16 v0, p0
invoke-virtual {v0, v5}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v4
if-nez v4, :cond_59
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
move-object/from16 v0, p0
invoke-virtual {v0, v5}, Landroid/support/v7/widget/af;->d(I)I
move-result v4
add-int/2addr v3, v4
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
move v3, v14
move v4, v15
move/from16 v7, v17
move/from16 v8, v18
move/from16 v9, v19
:goto_4e
add-int/lit8 v5, v5, 0x1
move v14, v3
move v15, v4
move/from16 v17, v7
move/from16 v18, v8
move/from16 v19, v9
goto :goto_2b
:cond_59
invoke-virtual {v4}, Landroid/view/View;->getVisibility()I
move-result v3
const/16 v7, 0x8
if-ne v3, v7, :cond_71
move-object/from16 v0, p0
invoke-virtual {v0, v4, v5}, Landroid/support/v7/widget/af;->a(Landroid/view/View;I)I
move-result v3
add-int/2addr v5, v3
move v3, v14
move v4, v15
move/from16 v7, v17
move/from16 v8, v18
move/from16 v9, v19
goto :goto_4e
:cond_71
move-object/from16 v0, p0
invoke-virtual {v0, v5}, Landroid/support/v7/widget/af;->c(I)Z
move-result v3
if-eqz v3, :cond_86
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
move-object/from16 v0, p0
iget v7, v0, Landroid/support/v7/widget/af;->m:I
add-int/2addr v3, v7
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
:cond_86
invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
move-object v10, v3
check-cast v10, Landroid/support/v7/widget/af$a;
iget v3, v10, Landroid/support/v7/widget/af$a;->g:F
add-float v16, v6, v3
const/high16 v3, 0x4000
move/from16 v0, v23
if-ne v0, v3, :cond_d8
iget v3, v10, Landroid/support/v7/widget/af$a;->height:I
if-nez v3, :cond_d8
iget v3, v10, Landroid/support/v7/widget/af$a;->g:F
const/4 v6, 0x0
cmpl-float v3, v3, v6
if-lez v3, :cond_d8
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
iget v6, v10, Landroid/support/v7/widget/af$a;->topMargin:I
add-int/2addr v6, v3
iget v7, v10, Landroid/support/v7/widget/af$a;->bottomMargin:I
add-int/2addr v6, v7
invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I
move-result v3
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
const/4 v15, 0x1
:cond_b5
:goto_b5
if-ltz v24, :cond_c5
add-int/lit8 v3, v5, 0x1
move/from16 v0, v24
if-ne v0, v3, :cond_c5
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->c:I
:cond_c5
move/from16 v0, v24
if-ge v5, v0, :cond_130
iget v3, v10, Landroid/support/v7/widget/af$a;->g:F
const/4 v6, 0x0
cmpl-float v3, v3, v6
if-lez v3, :cond_130
new-instance v3, Ljava/lang/RuntimeException;
const-string v4, "A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won\'t work.  Either remove the weight, or don\'t set mBaselineAlignedChildIndex."
invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V
throw v3
:cond_d8
const/high16 v3, -0x8000
iget v6, v10, Landroid/support/v7/widget/af$a;->height:I
if-nez v6, :cond_e9
iget v6, v10, Landroid/support/v7/widget/af$a;->g:F
const/4 v7, 0x0
cmpl-float v6, v6, v7
if-lez v6, :cond_e9
const/4 v3, 0x0
const/4 v6, -0x2
iput v6, v10, Landroid/support/v7/widget/af$a;->height:I
:cond_e9
move/from16 v20, v3
const/4 v7, 0x0
const/4 v3, 0x0
cmpl-float v3, v16, v3
if-nez v3, :cond_12e
move-object/from16 v0, p0
iget v9, v0, Landroid/support/v7/widget/af;->f:I
:goto_f5
move-object/from16 v3, p0
move/from16 v6, p1
move/from16 v8, p2
invoke-virtual/range {v3 .. v9}, Landroid/support/v7/widget/af;->a(Landroid/view/View;IIIII)V
const/high16 v3, -0x8000
move/from16 v0, v20
if-eq v0, v3, :cond_108
move/from16 v0, v20
iput v0, v10, Landroid/support/v7/widget/af$a;->height:I
:cond_108
invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I
move-result v3
move-object/from16 v0, p0
iget v6, v0, Landroid/support/v7/widget/af;->f:I
add-int v7, v6, v3
iget v8, v10, Landroid/support/v7/widget/af$a;->topMargin:I
add-int/2addr v7, v8
iget v8, v10, Landroid/support/v7/widget/af$a;->bottomMargin:I
add-int/2addr v7, v8
move-object/from16 v0, p0
invoke-virtual {v0, v4}, Landroid/support/v7/widget/af;->b(Landroid/view/View;)I
move-result v8
add-int/2addr v7, v8
invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I
move-result v6
move-object/from16 v0, p0
iput v6, v0, Landroid/support/v7/widget/af;->f:I
if-eqz v25, :cond_b5
invoke-static {v3, v14}, Ljava/lang/Math;->max(II)I
move-result v14
goto :goto_b5
:cond_12e
const/4 v9, 0x0
goto :goto_f5
:cond_130
const/4 v3, 0x0
const/high16 v6, 0x4000
move/from16 v0, v22
if-eq v0, v6, :cond_3e5
iget v6, v10, Landroid/support/v7/widget/af$a;->width:I
const/4 v7, -0x1
if-ne v6, v7, :cond_3e5
const/4 v6, 0x1
const/4 v3, 0x1
:goto_13e
iget v7, v10, Landroid/support/v7/widget/af$a;->leftMargin:I
iget v8, v10, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v8, v7
invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I
move-result v7
add-int v9, v7, v8
move/from16 v0, v19
invoke-static {v0, v9}, Ljava/lang/Math;->max(II)I
move-result v19
invoke-static {v4}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v7
move/from16 v0, v18
invoke-static {v0, v7}, Landroid/support/v7/widget/au;->a(II)I
move-result v11
if-eqz v17, :cond_187
iget v7, v10, Landroid/support/v7/widget/af$a;->width:I
const/16 v17, -0x1
move/from16 v0, v17
if-ne v7, v0, :cond_187
const/4 v7, 0x1
:goto_164
iget v10, v10, Landroid/support/v7/widget/af$a;->g:F
const/16 v17, 0x0
cmpl-float v10, v10, v17
if-lez v10, :cond_18b
if-eqz v3, :cond_189
move v3, v8
:goto_16f
invoke-static {v12, v3}, Ljava/lang/Math;->max(II)I
move-result v3
move v8, v13
:goto_174
move-object/from16 v0, p0
invoke-virtual {v0, v4, v5}, Landroid/support/v7/widget/af;->a(Landroid/view/View;I)I
move-result v4
add-int/2addr v5, v4
move v4, v15
move v12, v3
move v13, v8
move/from16 v9, v19
move v3, v14
move v8, v11
move v11, v6
move/from16 v6, v16
goto/16 :goto_4e
:cond_187
const/4 v7, 0x0
goto :goto_164
:cond_189
move v3, v9
goto :goto_16f
:cond_18b
if-eqz v3, :cond_194
:goto_18d
invoke-static {v13, v8}, Ljava/lang/Math;->max(II)I
move-result v3
move v8, v3
move v3, v12
goto :goto_174
:cond_194
move v8, v9
goto :goto_18d
:cond_196
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
if-lez v3, :cond_1b3
move-object/from16 v0, p0
move/from16 v1, v21
invoke-virtual {v0, v1}, Landroid/support/v7/widget/af;->c(I)Z
move-result v3
if-eqz v3, :cond_1b3
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
move-object/from16 v0, p0
iget v4, v0, Landroid/support/v7/widget/af;->m:I
add-int/2addr v3, v4
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
:cond_1b3
if-eqz v25, :cond_215
const/high16 v3, -0x8000
move/from16 v0, v23
if-eq v0, v3, :cond_1bd
if-nez v23, :cond_215
:cond_1bd
const/4 v3, 0x0
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
const/4 v4, 0x0
:goto_1c3
move/from16 v0, v21
if-ge v4, v0, :cond_215
move-object/from16 v0, p0
invoke-virtual {v0, v4}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v5
if-nez v5, :cond_1e2
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
move-object/from16 v0, p0
invoke-virtual {v0, v4}, Landroid/support/v7/widget/af;->d(I)I
move-result v5
add-int/2addr v3, v5
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
move v3, v4
:goto_1df
add-int/lit8 v4, v3, 0x1
goto :goto_1c3
:cond_1e2
invoke-virtual {v5}, Landroid/view/View;->getVisibility()I
move-result v3
const/16 v7, 0x8
if-ne v3, v7, :cond_1f2
move-object/from16 v0, p0
invoke-virtual {v0, v5, v4}, Landroid/support/v7/widget/af;->a(Landroid/view/View;I)I
move-result v3
add-int/2addr v3, v4
goto :goto_1df
:cond_1f2
invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
check-cast v3, Landroid/support/v7/widget/af$a;
move-object/from16 v0, p0
iget v7, v0, Landroid/support/v7/widget/af;->f:I
add-int v8, v7, v14
iget v9, v3, Landroid/support/v7/widget/af$a;->topMargin:I
add-int/2addr v8, v9
iget v3, v3, Landroid/support/v7/widget/af$a;->bottomMargin:I
add-int/2addr v3, v8
move-object/from16 v0, p0
invoke-virtual {v0, v5}, Landroid/support/v7/widget/af;->b(Landroid/view/View;)I
move-result v5
add-int/2addr v3, v5
invoke-static {v7, v3}, Ljava/lang/Math;->max(II)I
move-result v3
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
move v3, v4
goto :goto_1df
:cond_215
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingTop()I
move-result v4
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingBottom()I
move-result v5
add-int/2addr v4, v5
add-int/2addr v3, v4
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getSuggestedMinimumHeight()I
move-result v4
invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I
move-result v3
const/4 v4, 0x0
move/from16 v0, p2
invoke-static {v3, v0, v4}, Landroid/support/v4/f/af;->a(III)I
move-result v16
const v3, 0xffffff
and-int v3, v3, v16
move-object/from16 v0, p0
iget v4, v0, Landroid/support/v7/widget/af;->f:I
sub-int v4, v3, v4
if-nez v15, :cond_24e
if-eqz v4, :cond_392
const/4 v3, 0x0
cmpl-float v3, v6, v3
if-lez v3, :cond_392
:cond_24e
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->g:F
const/4 v5, 0x0
cmpl-float v3, v3, v5
if-lez v3, :cond_25b
move-object/from16 v0, p0
iget v6, v0, Landroid/support/v7/widget/af;->g:F
:cond_25b
const/4 v3, 0x0
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
const/4 v3, 0x0
move v14, v3
move v5, v6
move/from16 v9, v17
move v10, v13
move/from16 v7, v18
move/from16 v12, v19
move v6, v4
:goto_26b
move/from16 v0, v21
if-ge v14, v0, :cond_345
move-object/from16 v0, p0
invoke-virtual {v0, v14}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v15
invoke-virtual {v15}, Landroid/view/View;->getVisibility()I
move-result v3
const/16 v4, 0x8
if-ne v3, v4, :cond_289
move v3, v10
move v4, v7
move v8, v12
move v7, v9
:goto_281
add-int/lit8 v9, v14, 0x1
move v14, v9
move v10, v3
move v12, v8
move v9, v7
move v7, v4
goto :goto_26b
:cond_289
invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
check-cast v3, Landroid/support/v7/widget/af$a;
iget v8, v3, Landroid/support/v7/widget/af$a;->g:F
const/4 v4, 0x0
cmpl-float v4, v8, v4
if-lez v4, :cond_3e0
int-to-float v4, v6
mul-float/2addr v4, v8
div-float/2addr v4, v5
float-to-int v4, v4
sub-float/2addr v5, v8
sub-int/2addr v6, v4
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingLeft()I
move-result v8
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingRight()I
move-result v13
add-int/2addr v8, v13
iget v13, v3, Landroid/support/v7/widget/af$a;->leftMargin:I
add-int/2addr v8, v13
iget v13, v3, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v8, v13
iget v13, v3, Landroid/support/v7/widget/af$a;->width:I
move/from16 v0, p1
invoke-static {v0, v8, v13}, Landroid/support/v7/widget/af;->getChildMeasureSpec(III)I
move-result v8
iget v13, v3, Landroid/support/v7/widget/af$a;->height:I
if-nez v13, :cond_2bd
const/high16 v13, 0x4000
move/from16 v0, v23
if-eq v0, v13, :cond_331
:cond_2bd
invoke-virtual {v15}, Landroid/view/View;->getMeasuredHeight()I
move-result v13
add-int/2addr v4, v13
if-gez v4, :cond_2c5
const/4 v4, 0x0
:cond_2c5
const/high16 v13, 0x4000
invoke-static {v4, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v4
invoke-virtual {v15, v8, v4}, Landroid/view/View;->measure(II)V
:goto_2ce
invoke-static {v15}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v4
and-int/lit16 v4, v4, -0x100
invoke-static {v7, v4}, Landroid/support/v7/widget/au;->a(II)I
move-result v4
move/from16 v26, v5
move v5, v6
move v6, v4
move/from16 v4, v26
:goto_2de
iget v7, v3, Landroid/support/v7/widget/af$a;->leftMargin:I
iget v8, v3, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v7, v8
invoke-virtual {v15}, Landroid/view/View;->getMeasuredWidth()I
move-result v8
add-int/2addr v8, v7
invoke-static {v12, v8}, Ljava/lang/Math;->max(II)I
move-result v12
const/high16 v13, 0x4000
move/from16 v0, v22
if-eq v0, v13, :cond_33f
iget v13, v3, Landroid/support/v7/widget/af$a;->width:I
const/16 v17, -0x1
move/from16 v0, v17
if-ne v13, v0, :cond_33f
const/4 v13, 0x1
:goto_2fb
if-eqz v13, :cond_341
:goto_2fd
invoke-static {v10, v7}, Ljava/lang/Math;->max(II)I
move-result v8
if-eqz v9, :cond_343
iget v7, v3, Landroid/support/v7/widget/af$a;->width:I
const/4 v9, -0x1
if-ne v7, v9, :cond_343
const/4 v7, 0x1
:goto_309
move-object/from16 v0, p0
iget v9, v0, Landroid/support/v7/widget/af;->f:I
invoke-virtual {v15}, Landroid/view/View;->getMeasuredHeight()I
move-result v10
add-int/2addr v10, v9
iget v13, v3, Landroid/support/v7/widget/af$a;->topMargin:I
add-int/2addr v10, v13
iget v3, v3, Landroid/support/v7/widget/af$a;->bottomMargin:I
add-int/2addr v3, v10
move-object/from16 v0, p0
invoke-virtual {v0, v15}, Landroid/support/v7/widget/af;->b(Landroid/view/View;)I
move-result v10
add-int/2addr v3, v10
invoke-static {v9, v3}, Ljava/lang/Math;->max(II)I
move-result v3
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
move v3, v8
move v8, v12
move/from16 v26, v4
move v4, v6
move v6, v5
move/from16 v5, v26
goto/16 :goto_281
:cond_331
if-lez v4, :cond_33d
:goto_333
const/high16 v13, 0x4000
invoke-static {v4, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v4
invoke-virtual {v15, v8, v4}, Landroid/view/View;->measure(II)V
goto :goto_2ce
:cond_33d
const/4 v4, 0x0
goto :goto_333
:cond_33f
const/4 v13, 0x0
goto :goto_2fb
:cond_341
move v7, v8
goto :goto_2fd
:cond_343
const/4 v7, 0x0
goto :goto_309
:cond_345
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingTop()I
move-result v4
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingBottom()I
move-result v5
add-int/2addr v4, v5
add-int/2addr v3, v4
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
move/from16 v17, v9
move v3, v10
move/from16 v18, v7
move v4, v12
:goto_35d
if-nez v17, :cond_3d9
const/high16 v5, 0x4000
move/from16 v0, v22
if-eq v0, v5, :cond_3d9
:goto_365
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingLeft()I
move-result v4
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingRight()I
move-result v5
add-int/2addr v4, v5
add-int/2addr v3, v4
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getSuggestedMinimumWidth()I
move-result v4
invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I
move-result v3
move/from16 v0, p1
move/from16 v1, v18
invoke-static {v3, v0, v1}, Landroid/support/v4/f/af;->a(III)I
move-result v3
move-object/from16 v0, p0
move/from16 v1, v16
invoke-virtual {v0, v3, v1}, Landroid/support/v7/widget/af;->setMeasuredDimension(II)V
if-eqz v11, :cond_391
move-object/from16 v0, p0
move/from16 v1, v21
move/from16 v2, p2
invoke-direct {v0, v1, v2}, Landroid/support/v7/widget/af;->c(II)V
:cond_391
return-void
:cond_392
invoke-static {v13, v12}, Ljava/lang/Math;->max(II)I
move-result v10
if-eqz v25, :cond_3db
const/high16 v3, 0x4000
move/from16 v0, v23
if-eq v0, v3, :cond_3db
const/4 v3, 0x0
move v4, v3
:goto_3a0
move/from16 v0, v21
if-ge v4, v0, :cond_3db
move-object/from16 v0, p0
invoke-virtual {v0, v4}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v5
if-eqz v5, :cond_3b4
invoke-virtual {v5}, Landroid/view/View;->getVisibility()I
move-result v3
const/16 v6, 0x8
if-ne v3, v6, :cond_3b8
:cond_3b4
:goto_3b4
add-int/lit8 v3, v4, 0x1
move v4, v3
goto :goto_3a0
:cond_3b8
invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
check-cast v3, Landroid/support/v7/widget/af$a;
iget v3, v3, Landroid/support/v7/widget/af$a;->g:F
const/4 v6, 0x0
cmpl-float v3, v3, v6
if-lez v3, :cond_3b4
invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I
move-result v3
const/high16 v6, 0x4000
invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v3
const/high16 v6, 0x4000
invoke-static {v14, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v6
invoke-virtual {v5, v3, v6}, Landroid/view/View;->measure(II)V
goto :goto_3b4
:cond_3d9
move v3, v4
goto :goto_365
:cond_3db
move v3, v10
move/from16 v4, v19
goto/16 :goto_35d
:cond_3e0
move v4, v5
move v5, v6
move v6, v7
goto/16 :goto_2de
:cond_3e5
move v6, v11
goto/16 :goto_13e
.end method
.method  a(IIII)V
.registers 19
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingLeft()I
move-result v9
sub-int v0, p3, p1
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingRight()I
move-result v1
sub-int v10, v0, v1
sub-int/2addr v0, v9
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingRight()I
move-result v1
sub-int v11, v0, v1
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getVirtualChildCount()I
move-result v12
iget v0, p0, Landroid/support/v7/widget/af;->e:I
and-int/lit8 v0, v0, 0x70
iget v1, p0, Landroid/support/v7/widget/af;->e:I
const v2, 0x800007
and-int v7, v1, v2
sparse-switch v0, :sswitch_data_c4
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingTop()I
move-result v0
:goto_29
const/4 v8, 0x0
move v3, v0
:goto_2b
if-ge v8, v12, :cond_bd
invoke-virtual {p0, v8}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v1
if-nez v1, :cond_55
invoke-virtual {p0, v8}, Landroid/support/v7/widget/af;->d(I)I
move-result v0
add-int/2addr v3, v0
move v0, v8
:goto_39
add-int/lit8 v8, v0, 0x1
goto :goto_2b
:sswitch_3c
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingTop()I
move-result v0
add-int v0, v0, p4
sub-int v0, v0, p2
iget v1, p0, Landroid/support/v7/widget/af;->f:I
sub-int/2addr v0, v1
goto :goto_29
:sswitch_48
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingTop()I
move-result v0
sub-int v1, p4, p2
iget v2, p0, Landroid/support/v7/widget/af;->f:I
sub-int/2addr v1, v2
div-int/lit8 v1, v1, 0x2
add-int/2addr v0, v1
goto :goto_29
:cond_55
invoke-virtual {v1}, Landroid/view/View;->getVisibility()I
move-result v0
const/16 v2, 0x8
if-eq v0, v2, :cond_c0
invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I
move-result v4
invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I
move-result v5
invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
move-object v6, v0
check-cast v6, Landroid/support/v7/widget/af$a;
iget v0, v6, Landroid/support/v7/widget/af$a;->h:I
if-gez v0, :cond_71
move v0, v7
:cond_71
invoke-static {p0}, Landroid/support/v4/f/af;->d(Landroid/view/View;)I
move-result v2
invoke-static {v0, v2}, Landroid/support/v4/f/e;->a(II)I
move-result v0
and-int/lit8 v0, v0, 0x7
sparse-switch v0, :sswitch_data_ce
iget v0, v6, Landroid/support/v7/widget/af$a;->leftMargin:I
add-int v2, v9, v0
:goto_82
invoke-virtual {p0, v8}, Landroid/support/v7/widget/af;->c(I)Z
move-result v0
if-eqz v0, :cond_be
iget v0, p0, Landroid/support/v7/widget/af;->m:I
add-int/2addr v0, v3
:goto_8b
iget v3, v6, Landroid/support/v7/widget/af$a;->topMargin:I
add-int v13, v0, v3
invoke-virtual {p0, v1}, Landroid/support/v7/widget/af;->a(Landroid/view/View;)I
move-result v0
add-int v3, v13, v0
move-object v0, p0
invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/af;->a(Landroid/view/View;IIII)V
iget v0, v6, Landroid/support/v7/widget/af$a;->bottomMargin:I
add-int/2addr v0, v5
invoke-virtual {p0, v1}, Landroid/support/v7/widget/af;->b(Landroid/view/View;)I
move-result v2
add-int/2addr v0, v2
add-int v3, v13, v0
invoke-virtual {p0, v1, v8}, Landroid/support/v7/widget/af;->a(Landroid/view/View;I)I
move-result v0
add-int/2addr v0, v8
goto :goto_39
:sswitch_a9
sub-int v0, v11, v4
div-int/lit8 v0, v0, 0x2
add-int/2addr v0, v9
iget v2, v6, Landroid/support/v7/widget/af$a;->leftMargin:I
add-int/2addr v0, v2
iget v2, v6, Landroid/support/v7/widget/af$a;->rightMargin:I
sub-int v2, v0, v2
goto :goto_82
:sswitch_b6
sub-int v0, v10, v4
iget v2, v6, Landroid/support/v7/widget/af$a;->rightMargin:I
sub-int v2, v0, v2
goto :goto_82
:cond_bd
return-void
:cond_be
move v0, v3
goto :goto_8b
:cond_c0
move v0, v8
goto/16 :goto_39
nop
:sswitch_data_ce
.sparse-switch
0x1 -> :sswitch_a9
0x5 -> :sswitch_b6
.end sparse-switch
:sswitch_data_c4
.sparse-switch
0x10 -> :sswitch_48
0x50 -> :sswitch_3c
.end sparse-switch
.end method
.method  a(Landroid/graphics/Canvas;)V
.registers 7
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getVirtualChildCount()I
move-result v2
const/4 v0, 0x0
move v1, v0
:goto_6
if-ge v1, v2, :cond_34
invoke-virtual {p0, v1}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v3
if-eqz v3, :cond_30
invoke-virtual {v3}, Landroid/view/View;->getVisibility()I
move-result v0
const/16 v4, 0x8
if-eq v0, v4, :cond_30
invoke-virtual {p0, v1}, Landroid/support/v7/widget/af;->c(I)Z
move-result v0
if-eqz v0, :cond_30
invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/af$a;
invoke-virtual {v3}, Landroid/view/View;->getTop()I
move-result v3
iget v0, v0, Landroid/support/v7/widget/af$a;->topMargin:I
sub-int v0, v3, v0
iget v3, p0, Landroid/support/v7/widget/af;->m:I
sub-int/2addr v0, v3
invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/af;->a(Landroid/graphics/Canvas;I)V
:cond_30
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_6
:cond_34
invoke-virtual {p0, v2}, Landroid/support/v7/widget/af;->c(I)Z
move-result v0
if-eqz v0, :cond_51
add-int/lit8 v0, v2, -0x1
invoke-virtual {p0, v0}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v1
if-nez v1, :cond_52
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getHeight()I
move-result v0
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingBottom()I
move-result v1
sub-int/2addr v0, v1
iget v1, p0, Landroid/support/v7/widget/af;->m:I
sub-int/2addr v0, v1
:goto_4e
invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/af;->a(Landroid/graphics/Canvas;I)V
:cond_51
return-void
:cond_52
invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/af$a;
invoke-virtual {v1}, Landroid/view/View;->getBottom()I
move-result v1
iget v0, v0, Landroid/support/v7/widget/af$a;->bottomMargin:I
add-int/2addr v0, v1
goto :goto_4e
.end method
.method  a(Landroid/graphics/Canvas;I)V
.registers 7
iget-object v0, p0, Landroid/support/v7/widget/af;->k:Landroid/graphics/drawable/Drawable;
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingLeft()I
move-result v1
iget v2, p0, Landroid/support/v7/widget/af;->o:I
add-int/2addr v1, v2
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getWidth()I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingRight()I
move-result v3
sub-int/2addr v2, v3
iget v3, p0, Landroid/support/v7/widget/af;->o:I
sub-int/2addr v2, v3
iget v3, p0, Landroid/support/v7/widget/af;->m:I
add-int/2addr v3, p2
invoke-virtual {v0, v1, p2, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V
iget-object v0, p0, Landroid/support/v7/widget/af;->k:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
return-void
.end method
.method  a(Landroid/view/View;IIIII)V
.registers 13
move-object v0, p0
move-object v1, p1
move v2, p3
move v3, p4
move v4, p5
move v5, p6
invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/af;->measureChildWithMargins(Landroid/view/View;IIII)V
return-void
.end method
.method  b(Landroid/view/View;)I
.registers 3
const/4 v0, 0x0
return v0
.end method
.method public b(Landroid/util/AttributeSet;)Landroid/support/v7/widget/af$a;
.registers 4
new-instance v0, Landroid/support/v7/widget/af$a;
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getContext()Landroid/content/Context;
move-result-object v1
invoke-direct {v0, v1, p1}, Landroid/support/v7/widget/af$a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-object v0
.end method
.method protected b(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/af$a;
.registers 3
new-instance v0, Landroid/support/v7/widget/af$a;
invoke-direct {v0, p1}, Landroid/support/v7/widget/af$a;-><init>(Landroid/view/ViewGroup$LayoutParams;)V
return-object v0
.end method
.method  b(I)Landroid/view/View;
.registers 3
invoke-virtual {p0, p1}, Landroid/support/v7/widget/af;->getChildAt(I)Landroid/view/View;
move-result-object v0
return-object v0
.end method
.method  b(II)V
.registers 32
const/4 v3, 0x0
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
const/16 v20, 0x0
const/16 v19, 0x0
const/4 v14, 0x0
const/4 v13, 0x0
const/16 v18, 0x1
const/4 v6, 0x0
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getVirtualChildCount()I
move-result v22
invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I
move-result v23
invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I
move-result v24
const/4 v12, 0x0
const/16 v16, 0x0
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/af;->i:[I
if-eqz v3, :cond_29
move-object/from16 v0, p0
iget-object v3, v0, Landroid/support/v7/widget/af;->j:[I
if-nez v3, :cond_37
:cond_29
const/4 v3, 0x4
new-array v3, v3, [I
move-object/from16 v0, p0
iput-object v3, v0, Landroid/support/v7/widget/af;->i:[I
const/4 v3, 0x4
new-array v3, v3, [I
move-object/from16 v0, p0
iput-object v3, v0, Landroid/support/v7/widget/af;->j:[I
:cond_37
move-object/from16 v0, p0
iget-object v0, v0, Landroid/support/v7/widget/af;->i:[I
move-object/from16 v25, v0
move-object/from16 v0, p0
iget-object v0, v0, Landroid/support/v7/widget/af;->j:[I
move-object/from16 v26, v0
const/4 v3, 0x0
const/4 v4, 0x1
const/4 v5, 0x2
const/4 v7, 0x3
const/4 v8, -0x1
aput v8, v25, v7
aput v8, v25, v5
aput v8, v25, v4
aput v8, v25, v3
const/4 v3, 0x0
const/4 v4, 0x1
const/4 v5, 0x2
const/4 v7, 0x3
const/4 v8, -0x1
aput v8, v26, v7
aput v8, v26, v5
aput v8, v26, v4
aput v8, v26, v3
move-object/from16 v0, p0
iget-boolean v0, v0, Landroid/support/v7/widget/af;->a:Z
move/from16 v27, v0
move-object/from16 v0, p0
iget-boolean v0, v0, Landroid/support/v7/widget/af;->h:Z
move/from16 v28, v0
const/high16 v3, 0x4000
move/from16 v0, v23
if-ne v0, v3, :cond_a4
const/4 v3, 0x1
move v11, v3
:goto_71
const/high16 v15, -0x8000
const/4 v5, 0x0
:goto_74
move/from16 v0, v22
if-ge v5, v0, :cond_239
move-object/from16 v0, p0
invoke-virtual {v0, v5}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v4
if-nez v4, :cond_a7
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
move-object/from16 v0, p0
invoke-virtual {v0, v5}, Landroid/support/v7/widget/af;->d(I)I
move-result v4
add-int/2addr v3, v4
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
move v3, v15
move/from16 v4, v16
move/from16 v7, v18
move/from16 v8, v19
move/from16 v9, v20
:goto_98
add-int/lit8 v5, v5, 0x1
move v15, v3
move/from16 v16, v4
move/from16 v18, v7
move/from16 v19, v8
move/from16 v20, v9
goto :goto_74
:cond_a4
const/4 v3, 0x0
move v11, v3
goto :goto_71
:cond_a7
invoke-virtual {v4}, Landroid/view/View;->getVisibility()I
move-result v3
const/16 v7, 0x8
if-ne v3, v7, :cond_c0
move-object/from16 v0, p0
invoke-virtual {v0, v4, v5}, Landroid/support/v7/widget/af;->a(Landroid/view/View;I)I
move-result v3
add-int/2addr v5, v3
move v3, v15
move/from16 v4, v16
move/from16 v7, v18
move/from16 v8, v19
move/from16 v9, v20
goto :goto_98
:cond_c0
move-object/from16 v0, p0
invoke-virtual {v0, v5}, Landroid/support/v7/widget/af;->c(I)Z
move-result v3
if-eqz v3, :cond_d5
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
move-object/from16 v0, p0
iget v7, v0, Landroid/support/v7/widget/af;->l:I
add-int/2addr v3, v7
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
:cond_d5
invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
move-object v10, v3
check-cast v10, Landroid/support/v7/widget/af$a;
iget v3, v10, Landroid/support/v7/widget/af$a;->g:F
add-float v17, v6, v3
const/high16 v3, 0x4000
move/from16 v0, v23
if-ne v0, v3, :cond_1b1
iget v3, v10, Landroid/support/v7/widget/af$a;->width:I
if-nez v3, :cond_1b1
iget v3, v10, Landroid/support/v7/widget/af$a;->g:F
const/4 v6, 0x0
cmpl-float v3, v3, v6
if-lez v3, :cond_1b1
if-eqz v11, :cond_199
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
iget v6, v10, Landroid/support/v7/widget/af$a;->leftMargin:I
iget v7, v10, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v6, v7
add-int/2addr v3, v6
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
:goto_101
if-eqz v27, :cond_1ad
const/4 v3, 0x0
const/4 v6, 0x0
invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v3
invoke-virtual {v4, v3, v3}, Landroid/view/View;->measure(II)V
:cond_10c
:goto_10c
const/4 v3, 0x0
const/high16 v6, 0x4000
move/from16 v0, v24
if-eq v0, v6, :cond_5b8
iget v6, v10, Landroid/support/v7/widget/af$a;->height:I
const/4 v7, -0x1
if-ne v6, v7, :cond_5b8
const/4 v6, 0x1
const/4 v3, 0x1
:goto_11a
iget v7, v10, Landroid/support/v7/widget/af$a;->topMargin:I
iget v8, v10, Landroid/support/v7/widget/af$a;->bottomMargin:I
add-int/2addr v8, v7
invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I
move-result v7
add-int v9, v7, v8
invoke-static {v4}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v7
move/from16 v0, v19
invoke-static {v0, v7}, Landroid/support/v7/widget/au;->a(II)I
move-result v12
if-eqz v27, :cond_164
invoke-virtual {v4}, Landroid/view/View;->getBaseline()I
move-result v19
const/4 v7, -0x1
move/from16 v0, v19
if-eq v0, v7, :cond_164
iget v7, v10, Landroid/support/v7/widget/af$a;->h:I
if-gez v7, :cond_223
move-object/from16 v0, p0
iget v7, v0, Landroid/support/v7/widget/af;->e:I
:goto_142
and-int/lit8 v7, v7, 0x70
shr-int/lit8 v7, v7, 0x4
and-int/lit8 v7, v7, -0x2
shr-int/lit8 v7, v7, 0x1
aget v21, v25, v7
move/from16 v0, v21
move/from16 v1, v19
invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I
move-result v21
aput v21, v25, v7
aget v21, v26, v7
sub-int v19, v9, v19
move/from16 v0, v21
move/from16 v1, v19
invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I
move-result v19
aput v19, v26, v7
:cond_164
move/from16 v0, v20
invoke-static {v0, v9}, Ljava/lang/Math;->max(II)I
move-result v19
if-eqz v18, :cond_227
iget v7, v10, Landroid/support/v7/widget/af$a;->height:I
const/16 v18, -0x1
move/from16 v0, v18
if-ne v7, v0, :cond_227
const/4 v7, 0x1
:goto_175
iget v10, v10, Landroid/support/v7/widget/af$a;->g:F
const/16 v18, 0x0
cmpl-float v10, v10, v18
if-lez v10, :cond_22d
if-eqz v3, :cond_22a
move v3, v8
:goto_180
invoke-static {v13, v3}, Ljava/lang/Math;->max(II)I
move-result v3
move v8, v14
:goto_185
move-object/from16 v0, p0
invoke-virtual {v0, v4, v5}, Landroid/support/v7/widget/af;->a(Landroid/view/View;I)I
move-result v4
add-int/2addr v5, v4
move/from16 v4, v16
move v13, v3
move v14, v8
move/from16 v9, v19
move v3, v15
move v8, v12
move v12, v6
move/from16 v6, v17
goto/16 :goto_98
:cond_199
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
iget v6, v10, Landroid/support/v7/widget/af$a;->leftMargin:I
add-int/2addr v6, v3
iget v7, v10, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v6, v7
invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I
move-result v3
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
goto/16 :goto_101
:cond_1ad
const/16 v16, 0x1
goto/16 :goto_10c
:cond_1b1
const/high16 v3, -0x8000
iget v6, v10, Landroid/support/v7/widget/af$a;->width:I
if-nez v6, :cond_1c2
iget v6, v10, Landroid/support/v7/widget/af$a;->g:F
const/4 v7, 0x0
cmpl-float v6, v6, v7
if-lez v6, :cond_1c2
const/4 v3, 0x0
const/4 v6, -0x2
iput v6, v10, Landroid/support/v7/widget/af$a;->width:I
:cond_1c2
move/from16 v21, v3
const/4 v3, 0x0
cmpl-float v3, v17, v3
if-nez v3, :cond_205
move-object/from16 v0, p0
iget v7, v0, Landroid/support/v7/widget/af;->f:I
:goto_1cd
const/4 v9, 0x0
move-object/from16 v3, p0
move/from16 v6, p1
move/from16 v8, p2
invoke-virtual/range {v3 .. v9}, Landroid/support/v7/widget/af;->a(Landroid/view/View;IIIII)V
const/high16 v3, -0x8000
move/from16 v0, v21
if-eq v0, v3, :cond_1e1
move/from16 v0, v21
iput v0, v10, Landroid/support/v7/widget/af$a;->width:I
:cond_1e1
invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I
move-result v3
if-eqz v11, :cond_207
move-object/from16 v0, p0
iget v6, v0, Landroid/support/v7/widget/af;->f:I
iget v7, v10, Landroid/support/v7/widget/af$a;->leftMargin:I
add-int/2addr v7, v3
iget v8, v10, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v7, v8
move-object/from16 v0, p0
invoke-virtual {v0, v4}, Landroid/support/v7/widget/af;->b(Landroid/view/View;)I
move-result v8
add-int/2addr v7, v8
add-int/2addr v6, v7
move-object/from16 v0, p0
iput v6, v0, Landroid/support/v7/widget/af;->f:I
:goto_1fd
if-eqz v28, :cond_10c
invoke-static {v3, v15}, Ljava/lang/Math;->max(II)I
move-result v15
goto/16 :goto_10c
:cond_205
const/4 v7, 0x0
goto :goto_1cd
:cond_207
move-object/from16 v0, p0
iget v6, v0, Landroid/support/v7/widget/af;->f:I
add-int v7, v6, v3
iget v8, v10, Landroid/support/v7/widget/af$a;->leftMargin:I
add-int/2addr v7, v8
iget v8, v10, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v7, v8
move-object/from16 v0, p0
invoke-virtual {v0, v4}, Landroid/support/v7/widget/af;->b(Landroid/view/View;)I
move-result v8
add-int/2addr v7, v8
invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I
move-result v6
move-object/from16 v0, p0
iput v6, v0, Landroid/support/v7/widget/af;->f:I
goto :goto_1fd
:cond_223
iget v7, v10, Landroid/support/v7/widget/af$a;->h:I
goto/16 :goto_142
:cond_227
const/4 v7, 0x0
goto/16 :goto_175
:cond_22a
move v3, v9
goto/16 :goto_180
:cond_22d
if-eqz v3, :cond_237
:goto_22f
invoke-static {v14, v8}, Ljava/lang/Math;->max(II)I
move-result v3
move v8, v3
move v3, v13
goto/16 :goto_185
:cond_237
move v8, v9
goto :goto_22f
:cond_239
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
if-lez v3, :cond_256
move-object/from16 v0, p0
move/from16 v1, v22
invoke-virtual {v0, v1}, Landroid/support/v7/widget/af;->c(I)Z
move-result v3
if-eqz v3, :cond_256
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
move-object/from16 v0, p0
iget v4, v0, Landroid/support/v7/widget/af;->l:I
add-int/2addr v3, v4
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
:cond_256
const/4 v3, 0x1
aget v3, v25, v3
const/4 v4, -0x1
if-ne v3, v4, :cond_26e
const/4 v3, 0x0
aget v3, v25, v3
const/4 v4, -0x1
if-ne v3, v4, :cond_26e
const/4 v3, 0x2
aget v3, v25, v3
const/4 v4, -0x1
if-ne v3, v4, :cond_26e
const/4 v3, 0x3
aget v3, v25, v3
const/4 v4, -0x1
if-eq v3, v4, :cond_5b4
:cond_26e
const/4 v3, 0x3
aget v3, v25, v3
const/4 v4, 0x0
aget v4, v25, v4
const/4 v5, 0x1
aget v5, v25, v5
const/4 v7, 0x2
aget v7, v25, v7
invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I
move-result v5
invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I
move-result v4
invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I
move-result v3
const/4 v4, 0x3
aget v4, v26, v4
const/4 v5, 0x0
aget v5, v26, v5
const/4 v7, 0x1
aget v7, v26, v7
const/4 v8, 0x2
aget v8, v26, v8
invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I
move-result v7
invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I
move-result v5
invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I
move-result v4
add-int/2addr v3, v4
move/from16 v0, v20
invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I
move-result v5
:goto_2a5
if-eqz v28, :cond_321
const/high16 v3, -0x8000
move/from16 v0, v23
if-eq v0, v3, :cond_2af
if-nez v23, :cond_321
:cond_2af
const/4 v3, 0x0
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
const/4 v4, 0x0
:goto_2b5
move/from16 v0, v22
if-ge v4, v0, :cond_321
move-object/from16 v0, p0
invoke-virtual {v0, v4}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v7
if-nez v7, :cond_2d4
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
move-object/from16 v0, p0
invoke-virtual {v0, v4}, Landroid/support/v7/widget/af;->d(I)I
move-result v7
add-int/2addr v3, v7
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
move v3, v4
:goto_2d1
add-int/lit8 v4, v3, 0x1
goto :goto_2b5
:cond_2d4
invoke-virtual {v7}, Landroid/view/View;->getVisibility()I
move-result v3
const/16 v8, 0x8
if-ne v3, v8, :cond_2e4
move-object/from16 v0, p0
invoke-virtual {v0, v7, v4}, Landroid/support/v7/widget/af;->a(Landroid/view/View;I)I
move-result v3
add-int/2addr v3, v4
goto :goto_2d1
:cond_2e4
invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
check-cast v3, Landroid/support/v7/widget/af$a;
if-eqz v11, :cond_304
move-object/from16 v0, p0
iget v8, v0, Landroid/support/v7/widget/af;->f:I
iget v9, v3, Landroid/support/v7/widget/af$a;->leftMargin:I
add-int/2addr v9, v15
iget v3, v3, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v3, v9
move-object/from16 v0, p0
invoke-virtual {v0, v7}, Landroid/support/v7/widget/af;->b(Landroid/view/View;)I
move-result v7
add-int/2addr v3, v7
add-int/2addr v3, v8
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
move v3, v4
goto :goto_2d1
:cond_304
move-object/from16 v0, p0
iget v8, v0, Landroid/support/v7/widget/af;->f:I
add-int v9, v8, v15
iget v10, v3, Landroid/support/v7/widget/af$a;->leftMargin:I
add-int/2addr v9, v10
iget v3, v3, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v3, v9
move-object/from16 v0, p0
invoke-virtual {v0, v7}, Landroid/support/v7/widget/af;->b(Landroid/view/View;)I
move-result v7
add-int/2addr v3, v7
invoke-static {v8, v3}, Ljava/lang/Math;->max(II)I
move-result v3
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
move v3, v4
goto :goto_2d1
:cond_321
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingLeft()I
move-result v4
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingRight()I
move-result v7
add-int/2addr v4, v7
add-int/2addr v3, v4
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getSuggestedMinimumWidth()I
move-result v4
invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I
move-result v3
const/4 v4, 0x0
move/from16 v0, p1
invoke-static {v3, v0, v4}, Landroid/support/v4/f/af;->a(III)I
move-result v17
const v3, 0xffffff
and-int v3, v3, v17
move-object/from16 v0, p0
iget v4, v0, Landroid/support/v7/widget/af;->f:I
sub-int v4, v3, v4
if-nez v16, :cond_35a
if-eqz v4, :cond_55b
const/4 v3, 0x0
cmpl-float v3, v6, v3
if-lez v3, :cond_55b
:cond_35a
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->g:F
const/4 v5, 0x0
cmpl-float v3, v3, v5
if-lez v3, :cond_367
move-object/from16 v0, p0
iget v6, v0, Landroid/support/v7/widget/af;->g:F
:cond_367
const/4 v3, 0x0
const/4 v5, 0x1
const/4 v7, 0x2
const/4 v8, 0x3
const/4 v9, -0x1
aput v9, v25, v8
aput v9, v25, v7
aput v9, v25, v5
aput v9, v25, v3
const/4 v3, 0x0
const/4 v5, 0x1
const/4 v7, 0x2
const/4 v8, 0x3
const/4 v9, -0x1
aput v9, v26, v8
aput v9, v26, v7
aput v9, v26, v5
aput v9, v26, v3
const/4 v8, -0x1
const/4 v3, 0x0
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
const/4 v3, 0x0
move v15, v3
move v5, v6
move/from16 v9, v18
move v10, v14
move/from16 v7, v19
move v6, v4
move v14, v8
:goto_391
move/from16 v0, v22
if-ge v15, v0, :cond_4bd
move-object/from16 v0, p0
invoke-virtual {v0, v15}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v16
if-eqz v16, :cond_5ad
invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getVisibility()I
move-result v3
const/16 v4, 0x8
if-ne v3, v4, :cond_3b3
move v3, v5
move v4, v6
move v8, v14
move v6, v10
move v5, v9
:goto_3aa
add-int/lit8 v9, v15, 0x1
move v15, v9
move v10, v6
move v14, v8
move v9, v5
move v6, v4
move v5, v3
goto :goto_391
:cond_3b3
invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
check-cast v3, Landroid/support/v7/widget/af$a;
iget v8, v3, Landroid/support/v7/widget/af$a;->g:F
const/4 v4, 0x0
cmpl-float v4, v8, v4
if-lez v4, :cond_5a8
int-to-float v4, v6
mul-float/2addr v4, v8
div-float/2addr v4, v5
float-to-int v4, v4
sub-float/2addr v5, v8
sub-int v8, v6, v4
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingTop()I
move-result v6
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingBottom()I
move-result v13
add-int/2addr v6, v13
iget v13, v3, Landroid/support/v7/widget/af$a;->topMargin:I
add-int/2addr v6, v13
iget v13, v3, Landroid/support/v7/widget/af$a;->bottomMargin:I
add-int/2addr v6, v13
iget v13, v3, Landroid/support/v7/widget/af$a;->height:I
move/from16 v0, p2
invoke-static {v0, v6, v13}, Landroid/support/v7/widget/af;->getChildMeasureSpec(III)I
move-result v6
iget v13, v3, Landroid/support/v7/widget/af$a;->width:I
if-nez v13, :cond_3e8
const/high16 v13, 0x4000
move/from16 v0, v23
if-eq v0, v13, :cond_480
:cond_3e8
invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getMeasuredWidth()I
move-result v13
add-int/2addr v4, v13
if-gez v4, :cond_3f0
const/4 v4, 0x0
:cond_3f0
const/high16 v13, 0x4000
invoke-static {v4, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v4
move-object/from16 v0, v16
invoke-virtual {v0, v4, v6}, Landroid/view/View;->measure(II)V
:goto_3fb
invoke-static/range {v16 .. v16}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v4
const/high16 v6, -0x100
and-int/2addr v4, v6
invoke-static {v7, v4}, Landroid/support/v7/widget/au;->a(II)I
move-result v13
move v7, v5
:goto_407
if-eqz v11, :cond_491
move-object/from16 v0, p0
iget v4, v0, Landroid/support/v7/widget/af;->f:I
invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getMeasuredWidth()I
move-result v5
iget v6, v3, Landroid/support/v7/widget/af$a;->leftMargin:I
add-int/2addr v5, v6
iget v6, v3, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v5, v6
move-object/from16 v0, p0
move-object/from16 v1, v16
invoke-virtual {v0, v1}, Landroid/support/v7/widget/af;->b(Landroid/view/View;)I
move-result v6
add-int/2addr v5, v6
add-int/2addr v4, v5
move-object/from16 v0, p0
iput v4, v0, Landroid/support/v7/widget/af;->f:I
:goto_425
const/high16 v4, 0x4000
move/from16 v0, v24
if-eq v0, v4, :cond_4b3
iget v4, v3, Landroid/support/v7/widget/af$a;->height:I
const/4 v5, -0x1
if-ne v4, v5, :cond_4b3
const/4 v4, 0x1
:goto_431
iget v5, v3, Landroid/support/v7/widget/af$a;->topMargin:I
iget v6, v3, Landroid/support/v7/widget/af$a;->bottomMargin:I
add-int/2addr v5, v6
invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getMeasuredHeight()I
move-result v6
add-int/2addr v6, v5
invoke-static {v14, v6}, Ljava/lang/Math;->max(II)I
move-result v14
if-eqz v4, :cond_4b6
move v4, v5
:goto_442
invoke-static {v10, v4}, Ljava/lang/Math;->max(II)I
move-result v5
if-eqz v9, :cond_4b8
iget v4, v3, Landroid/support/v7/widget/af$a;->height:I
const/4 v9, -0x1
if-ne v4, v9, :cond_4b8
const/4 v4, 0x1
:goto_44e
if-eqz v27, :cond_478
invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getBaseline()I
move-result v9
const/4 v10, -0x1
if-eq v9, v10, :cond_478
iget v10, v3, Landroid/support/v7/widget/af$a;->h:I
if-gez v10, :cond_4ba
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->e:I
:goto_45f
and-int/lit8 v3, v3, 0x70
shr-int/lit8 v3, v3, 0x4
and-int/lit8 v3, v3, -0x2
shr-int/lit8 v3, v3, 0x1
aget v10, v25, v3
invoke-static {v10, v9}, Ljava/lang/Math;->max(II)I
move-result v10
aput v10, v25, v3
aget v10, v26, v3
sub-int/2addr v6, v9
invoke-static {v10, v6}, Ljava/lang/Math;->max(II)I
move-result v6
aput v6, v26, v3
:cond_478
move v3, v7
move v6, v5
move v5, v4
move v7, v13
move v4, v8
move v8, v14
goto/16 :goto_3aa
:cond_480
if-lez v4, :cond_48f
:goto_482
const/high16 v13, 0x4000
invoke-static {v4, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v4
move-object/from16 v0, v16
invoke-virtual {v0, v4, v6}, Landroid/view/View;->measure(II)V
goto/16 :goto_3fb
:cond_48f
const/4 v4, 0x0
goto :goto_482
:cond_491
move-object/from16 v0, p0
iget v4, v0, Landroid/support/v7/widget/af;->f:I
invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getMeasuredWidth()I
move-result v5
add-int/2addr v5, v4
iget v6, v3, Landroid/support/v7/widget/af$a;->leftMargin:I
add-int/2addr v5, v6
iget v6, v3, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v5, v6
move-object/from16 v0, p0
move-object/from16 v1, v16
invoke-virtual {v0, v1}, Landroid/support/v7/widget/af;->b(Landroid/view/View;)I
move-result v6
add-int/2addr v5, v6
invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I
move-result v4
move-object/from16 v0, p0
iput v4, v0, Landroid/support/v7/widget/af;->f:I
goto/16 :goto_425
:cond_4b3
const/4 v4, 0x0
goto/16 :goto_431
:cond_4b6
move v4, v6
goto :goto_442
:cond_4b8
const/4 v4, 0x0
goto :goto_44e
:cond_4ba
iget v3, v3, Landroid/support/v7/widget/af$a;->h:I
goto :goto_45f
:cond_4bd
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingLeft()I
move-result v4
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingRight()I
move-result v5
add-int/2addr v4, v5
add-int/2addr v3, v4
move-object/from16 v0, p0
iput v3, v0, Landroid/support/v7/widget/af;->f:I
const/4 v3, 0x1
aget v3, v25, v3
const/4 v4, -0x1
if-ne v3, v4, :cond_4e7
const/4 v3, 0x0
aget v3, v25, v3
const/4 v4, -0x1
if-ne v3, v4, :cond_4e7
const/4 v3, 0x2
aget v3, v25, v3
const/4 v4, -0x1
if-ne v3, v4, :cond_4e7
const/4 v3, 0x3
aget v3, v25, v3
const/4 v4, -0x1
if-eq v3, v4, :cond_51c
:cond_4e7
const/4 v3, 0x3
aget v3, v25, v3
const/4 v4, 0x0
aget v4, v25, v4
const/4 v5, 0x1
aget v5, v25, v5
const/4 v6, 0x2
aget v6, v25, v6
invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I
move-result v5
invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I
move-result v4
invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I
move-result v3
const/4 v4, 0x3
aget v4, v26, v4
const/4 v5, 0x0
aget v5, v26, v5
const/4 v6, 0x1
aget v6, v26, v6
const/4 v8, 0x2
aget v8, v26, v8
invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I
move-result v6
invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I
move-result v5
invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I
move-result v4
add-int/2addr v3, v4
invoke-static {v14, v3}, Ljava/lang/Math;->max(II)I
move-result v14
:cond_51c
move/from16 v18, v9
move v3, v10
move/from16 v19, v7
move v4, v14
:goto_522
if-nez v18, :cond_5a2
const/high16 v5, 0x4000
move/from16 v0, v24
if-eq v0, v5, :cond_5a2
:goto_52a
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingTop()I
move-result v4
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingBottom()I
move-result v5
add-int/2addr v4, v5
add-int/2addr v3, v4
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getSuggestedMinimumHeight()I
move-result v4
invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I
move-result v3
const/high16 v4, -0x100
and-int v4, v4, v19
or-int v4, v4, v17
shl-int/lit8 v5, v19, 0x10
move/from16 v0, p2
invoke-static {v3, v0, v5}, Landroid/support/v4/f/af;->a(III)I
move-result v3
move-object/from16 v0, p0
invoke-virtual {v0, v4, v3}, Landroid/support/v7/widget/af;->setMeasuredDimension(II)V
if-eqz v12, :cond_55a
move-object/from16 v0, p0
move/from16 v1, v22
move/from16 v2, p1
invoke-direct {v0, v1, v2}, Landroid/support/v7/widget/af;->d(II)V
:cond_55a
return-void
:cond_55b
invoke-static {v14, v13}, Ljava/lang/Math;->max(II)I
move-result v10
if-eqz v28, :cond_5a4
const/high16 v3, 0x4000
move/from16 v0, v23
if-eq v0, v3, :cond_5a4
const/4 v3, 0x0
move v4, v3
:goto_569
move/from16 v0, v22
if-ge v4, v0, :cond_5a4
move-object/from16 v0, p0
invoke-virtual {v0, v4}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v6
if-eqz v6, :cond_57d
invoke-virtual {v6}, Landroid/view/View;->getVisibility()I
move-result v3
const/16 v7, 0x8
if-ne v3, v7, :cond_581
:goto_57d
:cond_57d
add-int/lit8 v3, v4, 0x1
move v4, v3
goto :goto_569
:cond_581
invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
check-cast v3, Landroid/support/v7/widget/af$a;
iget v3, v3, Landroid/support/v7/widget/af$a;->g:F
const/4 v7, 0x0
cmpl-float v3, v3, v7
if-lez v3, :cond_57d
const/high16 v3, 0x4000
invoke-static {v15, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v3
invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I
move-result v7
const/high16 v8, 0x4000
invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v7
invoke-virtual {v6, v3, v7}, Landroid/view/View;->measure(II)V
goto :goto_57d
:cond_5a2
move v3, v4
goto :goto_52a
:cond_5a4
move v3, v10
move v4, v5
goto/16 :goto_522
:cond_5a8
move v8, v6
move v13, v7
move v7, v5
goto/16 :goto_407
:cond_5ad
move v3, v5
move v4, v6
move v8, v14
move v6, v10
move v5, v9
goto/16 :goto_3aa
:cond_5b4
move/from16 v5, v20
goto/16 :goto_2a5
:cond_5b8
move v6, v12
goto/16 :goto_11a
.end method
.method  b(IIII)V
.registers 27
invoke-static/range {p0 .. p0}, Landroid/support/v7/widget/au;->a(Landroid/view/View;)Z
move-result v4
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingTop()I
move-result v11
sub-int v2, p4, p2
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingBottom()I
move-result v3
sub-int v15, v2, v3
sub-int/2addr v2, v11
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingBottom()I
move-result v3
sub-int v16, v2, v3
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getVirtualChildCount()I
move-result v17
move-object/from16 v0, p0
iget v2, v0, Landroid/support/v7/widget/af;->e:I
const v3, 0x800007
and-int/2addr v2, v3
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->e:I
and-int/lit8 v14, v3, 0x70
move-object/from16 v0, p0
iget-boolean v0, v0, Landroid/support/v7/widget/af;->a:Z
move/from16 v18, v0
move-object/from16 v0, p0
iget-object v0, v0, Landroid/support/v7/widget/af;->i:[I
move-object/from16 v19, v0
move-object/from16 v0, p0
iget-object v0, v0, Landroid/support/v7/widget/af;->j:[I
move-object/from16 v20, v0
invoke-static/range {p0 .. p0}, Landroid/support/v4/f/af;->d(Landroid/view/View;)I
move-result v3
invoke-static {v2, v3}, Landroid/support/v4/f/e;->a(II)I
move-result v2
sparse-switch v2, :sswitch_data_134
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingLeft()I
move-result v12
:goto_4a
const/4 v3, 0x0
const/4 v2, 0x1
if-eqz v4, :cond_12f
add-int/lit8 v3, v17, -0x1
const/4 v2, -0x1
move v9, v2
move v10, v3
:goto_53
const/4 v13, 0x0
:goto_54
move/from16 v0, v17
if-ge v13, v0, :cond_127
mul-int v2, v9, v13
add-int v21, v10, v2
move-object/from16 v0, p0
move/from16 v1, v21
invoke-virtual {v0, v1}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v3
if-nez v3, :cond_92
move-object/from16 v0, p0
move/from16 v1, v21
invoke-virtual {v0, v1}, Landroid/support/v7/widget/af;->d(I)I
move-result v2
add-int/2addr v12, v2
move v2, v13
:goto_70
add-int/lit8 v13, v2, 0x1
goto :goto_54
:sswitch_73
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingLeft()I
move-result v2
add-int v2, v2, p3
sub-int v2, v2, p1
move-object/from16 v0, p0
iget v3, v0, Landroid/support/v7/widget/af;->f:I
sub-int v12, v2, v3
goto :goto_4a
:sswitch_82
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/af;->getPaddingLeft()I
move-result v2
sub-int v3, p3, p1
move-object/from16 v0, p0
iget v5, v0, Landroid/support/v7/widget/af;->f:I
sub-int/2addr v3, v5
div-int/lit8 v3, v3, 0x2
add-int v12, v2, v3
goto :goto_4a
:cond_92
invoke-virtual {v3}, Landroid/view/View;->getVisibility()I
move-result v2
const/16 v4, 0x8
if-eq v2, v4, :cond_12c
invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I
move-result v6
invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I
move-result v7
const/4 v4, -0x1
invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v2
move-object v8, v2
check-cast v8, Landroid/support/v7/widget/af$a;
if-eqz v18, :cond_12a
iget v2, v8, Landroid/support/v7/widget/af$a;->height:I
const/4 v5, -0x1
if-eq v2, v5, :cond_12a
invoke-virtual {v3}, Landroid/view/View;->getBaseline()I
move-result v2
:goto_b5
iget v4, v8, Landroid/support/v7/widget/af$a;->h:I
if-gez v4, :cond_ba
move v4, v14
:cond_ba
and-int/lit8 v4, v4, 0x70
sparse-switch v4, :sswitch_data_13e
move v5, v11
:goto_c0
:cond_c0
move-object/from16 v0, p0
move/from16 v1, v21
invoke-virtual {v0, v1}, Landroid/support/v7/widget/af;->c(I)Z
move-result v2
if-eqz v2, :cond_128
move-object/from16 v0, p0
iget v2, v0, Landroid/support/v7/widget/af;->l:I
add-int/2addr v2, v12
:goto_cf
iget v4, v8, Landroid/support/v7/widget/af$a;->leftMargin:I
add-int v12, v2, v4
move-object/from16 v0, p0
invoke-virtual {v0, v3}, Landroid/support/v7/widget/af;->a(Landroid/view/View;)I
move-result v2
add-int v4, v12, v2
move-object/from16 v2, p0
invoke-direct/range {v2 .. v7}, Landroid/support/v7/widget/af;->a(Landroid/view/View;IIII)V
iget v2, v8, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v2, v6
move-object/from16 v0, p0
invoke-virtual {v0, v3}, Landroid/support/v7/widget/af;->b(Landroid/view/View;)I
move-result v4
add-int/2addr v2, v4
add-int/2addr v12, v2
move-object/from16 v0, p0
move/from16 v1, v21
invoke-virtual {v0, v3, v1}, Landroid/support/v7/widget/af;->a(Landroid/view/View;I)I
move-result v2
add-int/2addr v2, v13
goto/16 :goto_70
:sswitch_f6
iget v4, v8, Landroid/support/v7/widget/af$a;->topMargin:I
add-int v5, v11, v4
const/4 v4, -0x1
if-eq v2, v4, :cond_c0
const/4 v4, 0x1
aget v4, v19, v4
sub-int v2, v4, v2
add-int/2addr v5, v2
goto :goto_c0
:sswitch_104
sub-int v2, v16, v7
div-int/lit8 v2, v2, 0x2
add-int/2addr v2, v11
iget v4, v8, Landroid/support/v7/widget/af$a;->topMargin:I
add-int/2addr v2, v4
iget v4, v8, Landroid/support/v7/widget/af$a;->bottomMargin:I
sub-int v5, v2, v4
goto :goto_c0
:sswitch_111
sub-int v4, v15, v7
iget v5, v8, Landroid/support/v7/widget/af$a;->bottomMargin:I
sub-int v5, v4, v5
const/4 v4, -0x1
if-eq v2, v4, :cond_c0
invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I
move-result v4
sub-int v2, v4, v2
const/4 v4, 0x2
aget v4, v20, v4
sub-int v2, v4, v2
sub-int/2addr v5, v2
goto :goto_c0
:cond_127
return-void
:cond_128
move v2, v12
goto :goto_cf
:cond_12a
move v2, v4
goto :goto_b5
:cond_12c
move v2, v13
goto/16 :goto_70
:cond_12f
move v9, v2
move v10, v3
goto/16 :goto_53
nop
:sswitch_data_13e
.sparse-switch
0x10 -> :sswitch_104
0x30 -> :sswitch_f6
0x50 -> :sswitch_111
.end sparse-switch
:sswitch_data_134
.sparse-switch
0x1 -> :sswitch_82
0x5 -> :sswitch_73
.end sparse-switch
.end method
.method  b(Landroid/graphics/Canvas;)V
.registers 8
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getVirtualChildCount()I
move-result v2
invoke-static {p0}, Landroid/support/v7/widget/au;->a(Landroid/view/View;)Z
move-result v3
const/4 v0, 0x0
move v1, v0
:goto_a
if-ge v1, v2, :cond_42
invoke-virtual {p0, v1}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v4
if-eqz v4, :cond_32
invoke-virtual {v4}, Landroid/view/View;->getVisibility()I
move-result v0
const/16 v5, 0x8
if-eq v0, v5, :cond_32
invoke-virtual {p0, v1}, Landroid/support/v7/widget/af;->c(I)Z
move-result v0
if-eqz v0, :cond_32
invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/af$a;
if-eqz v3, :cond_36
invoke-virtual {v4}, Landroid/view/View;->getRight()I
move-result v4
iget v0, v0, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v0, v4
:goto_2f
invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/af;->b(Landroid/graphics/Canvas;I)V
:cond_32
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_a
:cond_36
invoke-virtual {v4}, Landroid/view/View;->getLeft()I
move-result v4
iget v0, v0, Landroid/support/v7/widget/af$a;->leftMargin:I
sub-int v0, v4, v0
iget v4, p0, Landroid/support/v7/widget/af;->l:I
sub-int/2addr v0, v4
goto :goto_2f
:cond_42
invoke-virtual {p0, v2}, Landroid/support/v7/widget/af;->c(I)Z
move-result v0
if-eqz v0, :cond_59
add-int/lit8 v0, v2, -0x1
invoke-virtual {p0, v0}, Landroid/support/v7/widget/af;->b(I)Landroid/view/View;
move-result-object v1
if-nez v1, :cond_67
if-eqz v3, :cond_5a
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingLeft()I
move-result v0
:goto_56
invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/af;->b(Landroid/graphics/Canvas;I)V
:cond_59
return-void
:cond_5a
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getWidth()I
move-result v0
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingRight()I
move-result v1
sub-int/2addr v0, v1
iget v1, p0, Landroid/support/v7/widget/af;->l:I
sub-int/2addr v0, v1
goto :goto_56
:cond_67
invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/af$a;
if-eqz v3, :cond_7b
invoke-virtual {v1}, Landroid/view/View;->getLeft()I
move-result v1
iget v0, v0, Landroid/support/v7/widget/af$a;->leftMargin:I
sub-int v0, v1, v0
iget v1, p0, Landroid/support/v7/widget/af;->l:I
sub-int/2addr v0, v1
goto :goto_56
:cond_7b
invoke-virtual {v1}, Landroid/view/View;->getRight()I
move-result v1
iget v0, v0, Landroid/support/v7/widget/af$a;->rightMargin:I
add-int/2addr v0, v1
goto :goto_56
.end method
.method  b(Landroid/graphics/Canvas;I)V
.registers 8
iget-object v0, p0, Landroid/support/v7/widget/af;->k:Landroid/graphics/drawable/Drawable;
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingTop()I
move-result v1
iget v2, p0, Landroid/support/v7/widget/af;->o:I
add-int/2addr v1, v2
iget v2, p0, Landroid/support/v7/widget/af;->l:I
add-int/2addr v2, p2
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getHeight()I
move-result v3
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingBottom()I
move-result v4
sub-int/2addr v3, v4
iget v4, p0, Landroid/support/v7/widget/af;->o:I
sub-int/2addr v3, v4
invoke-virtual {v0, p2, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V
iget-object v0, p0, Landroid/support/v7/widget/af;->k:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
return-void
.end method
.method protected c(I)Z
.registers 7
const/4 v0, 0x1
const/4 v1, 0x0
if-nez p1, :cond_d
iget v2, p0, Landroid/support/v7/widget/af;->n:I
and-int/lit8 v2, v2, 0x1
if-eqz v2, :cond_b
:cond_a
:goto_a
return v0
:cond_b
move v0, v1
goto :goto_a
:cond_d
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getChildCount()I
move-result v2
if-ne p1, v2, :cond_1b
iget v2, p0, Landroid/support/v7/widget/af;->n:I
and-int/lit8 v2, v2, 0x4
if-nez v2, :cond_a
move v0, v1
goto :goto_a
:cond_1b
iget v2, p0, Landroid/support/v7/widget/af;->n:I
and-int/lit8 v2, v2, 0x2
if-eqz v2, :cond_34
add-int/lit8 v2, p1, -0x1
:goto_23
if-ltz v2, :cond_36
invoke-virtual {p0, v2}, Landroid/support/v7/widget/af;->getChildAt(I)Landroid/view/View;
move-result-object v3
invoke-virtual {v3}, Landroid/view/View;->getVisibility()I
move-result v3
const/16 v4, 0x8
if-ne v3, v4, :cond_a
add-int/lit8 v2, v2, -0x1
goto :goto_23
:cond_34
move v0, v1
goto :goto_a
:cond_36
move v0, v1
goto :goto_a
.end method
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
.registers 3
instance-of v0, p1, Landroid/support/v7/widget/af$a;
return v0
.end method
.method  d(I)I
.registers 3
const/4 v0, 0x0
return v0
.end method
.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/af;->j()Landroid/support/v7/widget/af$a;
move-result-object v0
return-object v0
.end method
.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
.registers 3
invoke-virtual {p0, p1}, Landroid/support/v7/widget/af;->b(Landroid/util/AttributeSet;)Landroid/support/v7/widget/af$a;
move-result-object v0
return-object v0
.end method
.method protected synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
.registers 3
invoke-virtual {p0, p1}, Landroid/support/v7/widget/af;->b(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/af$a;
move-result-object v0
return-object v0
.end method
.method public getBaseline()I
.registers 6
const/4 v0, -0x1
iget v1, p0, Landroid/support/v7/widget/af;->b:I
if-gez v1, :cond_a
invoke-super {p0}, Landroid/view/ViewGroup;->getBaseline()I
move-result v0
:cond_9
:goto_9
return v0
:cond_a
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getChildCount()I
move-result v1
iget v2, p0, Landroid/support/v7/widget/af;->b:I
if-gt v1, v2, :cond_1a
new-instance v0, Ljava/lang/RuntimeException;
const-string v1, "mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds."
invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V
throw v0
:cond_1a
iget v1, p0, Landroid/support/v7/widget/af;->b:I
invoke-virtual {p0, v1}, Landroid/support/v7/widget/af;->getChildAt(I)Landroid/view/View;
move-result-object v2
invoke-virtual {v2}, Landroid/view/View;->getBaseline()I
move-result v3
if-ne v3, v0, :cond_32
iget v1, p0, Landroid/support/v7/widget/af;->b:I
if-eqz v1, :cond_9
new-instance v0, Ljava/lang/RuntimeException;
const-string v1, "mBaselineAlignedChildIndex of LinearLayout points to a View that doesn\'t know how to get its baseline."
invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V
throw v0
:cond_32
iget v0, p0, Landroid/support/v7/widget/af;->c:I
iget v1, p0, Landroid/support/v7/widget/af;->d:I
const/4 v4, 0x1
if-ne v1, v4, :cond_44
iget v1, p0, Landroid/support/v7/widget/af;->e:I
and-int/lit8 v1, v1, 0x70
const/16 v4, 0x30
if-eq v1, v4, :cond_44
sparse-switch v1, :sswitch_data_7e
:cond_44
move v1, v0
:goto_45
invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/af$a;
iget v0, v0, Landroid/support/v7/widget/af$a;->topMargin:I
add-int/2addr v0, v1
add-int/2addr v0, v3
goto :goto_9
:sswitch_50
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getBottom()I
move-result v0
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getTop()I
move-result v1
sub-int/2addr v0, v1
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingBottom()I
move-result v1
sub-int/2addr v0, v1
iget v1, p0, Landroid/support/v7/widget/af;->f:I
sub-int/2addr v0, v1
move v1, v0
goto :goto_45
:sswitch_63
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getBottom()I
move-result v1
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getTop()I
move-result v4
sub-int/2addr v1, v4
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingTop()I
move-result v4
sub-int/2addr v1, v4
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getPaddingBottom()I
move-result v4
sub-int/2addr v1, v4
iget v4, p0, Landroid/support/v7/widget/af;->f:I
sub-int/2addr v1, v4
div-int/lit8 v1, v1, 0x2
add-int/2addr v0, v1
move v1, v0
goto :goto_45
:sswitch_data_7e
.sparse-switch
0x10 -> :sswitch_63
0x50 -> :sswitch_50
.end sparse-switch
.end method
.method public getBaselineAlignedChildIndex()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/af;->b:I
return v0
.end method
.method public getDividerDrawable()Landroid/graphics/drawable/Drawable;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/af;->k:Landroid/graphics/drawable/Drawable;
return-object v0
.end method
.method public getDividerPadding()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/af;->o:I
return v0
.end method
.method public getDividerWidth()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/af;->l:I
return v0
.end method
.method public getOrientation()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/af;->d:I
return v0
.end method
.method public getShowDividers()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/af;->n:I
return v0
.end method
.method  getVirtualChildCount()I
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getChildCount()I
move-result v0
return v0
.end method
.method public getWeightSum()F
.registers 2
iget v0, p0, Landroid/support/v7/widget/af;->g:F
return v0
.end method
.method protected j()Landroid/support/v7/widget/af$a;
.registers 4
const/4 v2, -0x2
iget v0, p0, Landroid/support/v7/widget/af;->d:I
if-nez v0, :cond_b
new-instance v0, Landroid/support/v7/widget/af$a;
invoke-direct {v0, v2, v2}, Landroid/support/v7/widget/af$a;-><init>(II)V
:goto_a
return-object v0
:cond_b
iget v0, p0, Landroid/support/v7/widget/af;->d:I
const/4 v1, 0x1
if-ne v0, v1, :cond_17
new-instance v0, Landroid/support/v7/widget/af$a;
const/4 v1, -0x1
invoke-direct {v0, v1, v2}, Landroid/support/v7/widget/af$a;-><init>(II)V
goto :goto_a
:cond_17
const/4 v0, 0x0
goto :goto_a
.end method
.method protected onDraw(Landroid/graphics/Canvas;)V
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/af;->k:Landroid/graphics/drawable/Drawable;
if-nez v0, :cond_5
:goto_4
return-void
:cond_5
iget v0, p0, Landroid/support/v7/widget/af;->d:I
const/4 v1, 0x1
if-ne v0, v1, :cond_e
invoke-virtual {p0, p1}, Landroid/support/v7/widget/af;->a(Landroid/graphics/Canvas;)V
goto :goto_4
:cond_e
invoke-virtual {p0, p1}, Landroid/support/v7/widget/af;->b(Landroid/graphics/Canvas;)V
goto :goto_4
.end method
.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
.registers 4
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0xe
if-lt v0, v1, :cond_12
invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
const-class v0, Landroid/support/v7/widget/af;
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V
:cond_12
return-void
.end method
.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
.registers 4
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0xe
if-lt v0, v1, :cond_12
invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
const-class v0, Landroid/support/v7/widget/af;
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V
:cond_12
return-void
.end method
.method protected onLayout(ZIIII)V
.registers 8
iget v0, p0, Landroid/support/v7/widget/af;->d:I
const/4 v1, 0x1
if-ne v0, v1, :cond_9
invoke-virtual {p0, p2, p3, p4, p5}, Landroid/support/v7/widget/af;->a(IIII)V
:goto_8
return-void
:cond_9
invoke-virtual {p0, p2, p3, p4, p5}, Landroid/support/v7/widget/af;->b(IIII)V
goto :goto_8
.end method
.method protected onMeasure(II)V
.registers 5
iget v0, p0, Landroid/support/v7/widget/af;->d:I
const/4 v1, 0x1
if-ne v0, v1, :cond_9
invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/af;->a(II)V
:goto_8
return-void
:cond_9
invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/af;->b(II)V
goto :goto_8
.end method
.method public setBaselineAligned(Z)V
.registers 2
iput-boolean p1, p0, Landroid/support/v7/widget/af;->a:Z
return-void
.end method
.method public setBaselineAlignedChildIndex(I)V
.registers 5
if-ltz p1, :cond_8
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getChildCount()I
move-result v0
if-lt p1, v0, :cond_2b
:cond_8
new-instance v0, Ljava/lang/IllegalArgumentException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "base aligned child index out of range (0, "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {p0}, Landroid/support/v7/widget/af;->getChildCount()I
move-result v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, ")"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_2b
iput p1, p0, Landroid/support/v7/widget/af;->b:I
return-void
.end method
.method public setDividerDrawable(Landroid/graphics/drawable/Drawable;)V
.registers 4
const/4 v0, 0x0
iget-object v1, p0, Landroid/support/v7/widget/af;->k:Landroid/graphics/drawable/Drawable;
if-ne p1, v1, :cond_6
:goto_5
return-void
:cond_6
iput-object p1, p0, Landroid/support/v7/widget/af;->k:Landroid/graphics/drawable/Drawable;
if-eqz p1, :cond_20
invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I
move-result v1
iput v1, p0, Landroid/support/v7/widget/af;->l:I
invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I
move-result v1
iput v1, p0, Landroid/support/v7/widget/af;->m:I
:goto_16
if-nez p1, :cond_19
const/4 v0, 0x1
:cond_19
invoke-virtual {p0, v0}, Landroid/support/v7/widget/af;->setWillNotDraw(Z)V
invoke-virtual {p0}, Landroid/support/v7/widget/af;->requestLayout()V
goto :goto_5
:cond_20
iput v0, p0, Landroid/support/v7/widget/af;->l:I
iput v0, p0, Landroid/support/v7/widget/af;->m:I
goto :goto_16
.end method
.method public setDividerPadding(I)V
.registers 2
iput p1, p0, Landroid/support/v7/widget/af;->o:I
return-void
.end method
.method public setGravity(I)V
.registers 4
iget v0, p0, Landroid/support/v7/widget/af;->e:I
if-eq v0, p1, :cond_19
const v0, 0x800007
and-int/2addr v0, p1
if-nez v0, :cond_1a
const v0, 0x800003
or-int/2addr v0, p1
:goto_e
and-int/lit8 v1, v0, 0x70
if-nez v1, :cond_14
or-int/lit8 v0, v0, 0x30
:cond_14
iput v0, p0, Landroid/support/v7/widget/af;->e:I
invoke-virtual {p0}, Landroid/support/v7/widget/af;->requestLayout()V
:cond_19
return-void
:cond_1a
move v0, p1
goto :goto_e
.end method
.method public setHorizontalGravity(I)V
.registers 5
const v2, 0x800007
and-int v0, p1, v2
iget v1, p0, Landroid/support/v7/widget/af;->e:I
and-int/2addr v1, v2
if-eq v1, v0, :cond_16
iget v1, p0, Landroid/support/v7/widget/af;->e:I
const v2, -0x800008
and-int/2addr v1, v2
or-int/2addr v0, v1
iput v0, p0, Landroid/support/v7/widget/af;->e:I
invoke-virtual {p0}, Landroid/support/v7/widget/af;->requestLayout()V
:cond_16
return-void
.end method
.method public setMeasureWithLargestChildEnabled(Z)V
.registers 2
iput-boolean p1, p0, Landroid/support/v7/widget/af;->h:Z
return-void
.end method
.method public setOrientation(I)V
.registers 3
iget v0, p0, Landroid/support/v7/widget/af;->d:I
if-eq v0, p1, :cond_9
iput p1, p0, Landroid/support/v7/widget/af;->d:I
invoke-virtual {p0}, Landroid/support/v7/widget/af;->requestLayout()V
:cond_9
return-void
.end method
.method public setShowDividers(I)V
.registers 3
iget v0, p0, Landroid/support/v7/widget/af;->n:I
if-eq p1, v0, :cond_7
invoke-virtual {p0}, Landroid/support/v7/widget/af;->requestLayout()V
:cond_7
iput p1, p0, Landroid/support/v7/widget/af;->n:I
return-void
.end method
.method public setVerticalGravity(I)V
.registers 4
and-int/lit8 v0, p1, 0x70
iget v1, p0, Landroid/support/v7/widget/af;->e:I
and-int/lit8 v1, v1, 0x70
if-eq v1, v0, :cond_12
iget v1, p0, Landroid/support/v7/widget/af;->e:I
and-int/lit8 v1, v1, -0x71
or-int/2addr v0, v1
iput v0, p0, Landroid/support/v7/widget/af;->e:I
invoke-virtual {p0}, Landroid/support/v7/widget/af;->requestLayout()V
:cond_12
return-void
.end method
.method public setWeightSum(F)V
.registers 3
const/4 v0, 0x0
invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F
move-result v0
iput v0, p0, Landroid/support/v7/widget/af;->g:F
return-void
.end method
.method public shouldDelayChildPressedState()Z
.registers 2
const/4 v0, 0x0
return v0
.end method