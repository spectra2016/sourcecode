.class public Landroid/support/v7/widget/Toolbar$b;
.super Landroid/support/v7/a/a$a;
.source "Toolbar.java"
.field  b:I
.method public constructor <init>(II)V
.registers 4
invoke-direct {p0, p1, p2}, Landroid/support/v7/a/a$a;-><init>(II)V
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/widget/Toolbar$b;->b:I
const v0, 0x800013
iput v0, p0, Landroid/support/v7/widget/Toolbar$b;->a:I
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 4
invoke-direct {p0, p1, p2}, Landroid/support/v7/a/a$a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/widget/Toolbar$b;->b:I
return-void
.end method
.method public constructor <init>(Landroid/support/v7/a/a$a;)V
.registers 3
invoke-direct {p0, p1}, Landroid/support/v7/a/a$a;-><init>(Landroid/support/v7/a/a$a;)V
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/widget/Toolbar$b;->b:I
return-void
.end method
.method public constructor <init>(Landroid/support/v7/widget/Toolbar$b;)V
.registers 3
invoke-direct {p0, p1}, Landroid/support/v7/a/a$a;-><init>(Landroid/support/v7/a/a$a;)V
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/widget/Toolbar$b;->b:I
iget v0, p1, Landroid/support/v7/widget/Toolbar$b;->b:I
iput v0, p0, Landroid/support/v7/widget/Toolbar$b;->b:I
return-void
.end method
.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
.registers 3
invoke-direct {p0, p1}, Landroid/support/v7/a/a$a;-><init>(Landroid/view/ViewGroup$LayoutParams;)V
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/widget/Toolbar$b;->b:I
return-void
.end method
.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
.registers 3
invoke-direct {p0, p1}, Landroid/support/v7/a/a$a;-><init>(Landroid/view/ViewGroup$LayoutParams;)V
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/widget/Toolbar$b;->b:I
invoke-virtual {p0, p1}, Landroid/support/v7/widget/Toolbar$b;->a(Landroid/view/ViewGroup$MarginLayoutParams;)V
return-void
.end method
.method  a(Landroid/view/ViewGroup$MarginLayoutParams;)V
.registers 3
iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I
iput v0, p0, Landroid/support/v7/widget/Toolbar$b;->leftMargin:I
iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I
iput v0, p0, Landroid/support/v7/widget/Toolbar$b;->topMargin:I
iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I
iput v0, p0, Landroid/support/v7/widget/Toolbar$b;->rightMargin:I
iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I
iput v0, p0, Landroid/support/v7/widget/Toolbar$b;->bottomMargin:I
return-void
.end method