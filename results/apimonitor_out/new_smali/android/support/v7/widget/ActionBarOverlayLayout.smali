.class public Landroid/support/v7/widget/ActionBarOverlayLayout;
.super Landroid/view/ViewGroup;
.source "ActionBarOverlayLayout.java"
.implements Landroid/support/v4/f/y;
.implements Landroid/support/v7/widget/ab;
.field static final a:[I
.field private final A:Ljava/lang/Runnable;
.field private final B:Landroid/support/v4/f/z;
.field private b:I
.field private c:I
.field private d:Landroid/support/v7/widget/ContentFrameLayout;
.field private e:Landroid/support/v7/widget/ActionBarContainer;
.field private f:Landroid/support/v7/widget/ac;
.field private g:Landroid/graphics/drawable/Drawable;
.field private h:Z
.field private i:Z
.field private j:Z
.field private k:Z
.field private l:Z
.field private m:I
.field private n:I
.field private final o:Landroid/graphics/Rect;
.field private final p:Landroid/graphics/Rect;
.field private final q:Landroid/graphics/Rect;
.field private final r:Landroid/graphics/Rect;
.field private final s:Landroid/graphics/Rect;
.field private final t:Landroid/graphics/Rect;
.field private u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
.field private final v:I
.field private w:Landroid/support/v4/widget/u;
.field private x:Landroid/support/v4/f/au;
.field private final y:Landroid/support/v4/f/ay;
.field private final z:Ljava/lang/Runnable;
.method static constructor <clinit>()V
.registers 3
const/4 v0, 0x2
new-array v0, v0, [I
const/4 v1, 0x0
sget v2, Landroid/support/v7/b/a$a;->actionBarSize:I
aput v2, v0, v1
const/4 v1, 0x1
const v2, 0x1010059
aput v2, v0, v1
sput-object v0, Landroid/support/v7/widget/ActionBarOverlayLayout;->a:[I
return-void
.end method
.method public constructor <init>(Landroid/content/Context;)V
.registers 3
const/4 v0, 0x0
invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 4
invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
const/4 v0, 0x0
iput v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->c:I
new-instance v0, Landroid/graphics/Rect;
invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->o:Landroid/graphics/Rect;
new-instance v0, Landroid/graphics/Rect;
invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->p:Landroid/graphics/Rect;
new-instance v0, Landroid/graphics/Rect;
invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->q:Landroid/graphics/Rect;
new-instance v0, Landroid/graphics/Rect;
invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->r:Landroid/graphics/Rect;
new-instance v0, Landroid/graphics/Rect;
invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->s:Landroid/graphics/Rect;
new-instance v0, Landroid/graphics/Rect;
invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->t:Landroid/graphics/Rect;
const/16 v0, 0x258
iput v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->v:I
new-instance v0, Landroid/support/v7/widget/ActionBarOverlayLayout$1;
invoke-direct {v0, p0}, Landroid/support/v7/widget/ActionBarOverlayLayout$1;-><init>(Landroid/support/v7/widget/ActionBarOverlayLayout;)V
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->y:Landroid/support/v4/f/ay;
new-instance v0, Landroid/support/v7/widget/ActionBarOverlayLayout$2;
invoke-direct {v0, p0}, Landroid/support/v7/widget/ActionBarOverlayLayout$2;-><init>(Landroid/support/v7/widget/ActionBarOverlayLayout;)V
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->z:Ljava/lang/Runnable;
new-instance v0, Landroid/support/v7/widget/ActionBarOverlayLayout$3;
invoke-direct {v0, p0}, Landroid/support/v7/widget/ActionBarOverlayLayout$3;-><init>(Landroid/support/v7/widget/ActionBarOverlayLayout;)V
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->A:Ljava/lang/Runnable;
invoke-direct {p0, p1}, Landroid/support/v7/widget/ActionBarOverlayLayout;->a(Landroid/content/Context;)V
new-instance v0, Landroid/support/v4/f/z;
invoke-direct {v0, p0}, Landroid/support/v4/f/z;-><init>(Landroid/view/ViewGroup;)V
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->B:Landroid/support/v4/f/z;
return-void
.end method
.method static synthetic a(Landroid/support/v7/widget/ActionBarOverlayLayout;Landroid/support/v4/f/au;)Landroid/support/v4/f/au;
.registers 2
iput-object p1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->x:Landroid/support/v4/f/au;
return-object p1
.end method
.method private a(Landroid/view/View;)Landroid/support/v7/widget/ac;
.registers 5
instance-of v0, p1, Landroid/support/v7/widget/ac;
if-eqz v0, :cond_7
check-cast p1, Landroid/support/v7/widget/ac;
:goto_6
return-object p1
:cond_7
instance-of v0, p1, Landroid/support/v7/widget/Toolbar;
if-eqz v0, :cond_12
check-cast p1, Landroid/support/v7/widget/Toolbar;
invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getWrapper()Landroid/support/v7/widget/ac;
move-result-object p1
goto :goto_6
:cond_12
new-instance v0, Ljava/lang/IllegalStateException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Can\'t make a decor toolbar out of "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
.end method
.method private a(Landroid/content/Context;)V
.registers 6
const/4 v1, 0x1
const/4 v2, 0x0
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getContext()Landroid/content/Context;
move-result-object v0
invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v0
sget-object v3, Landroid/support/v7/widget/ActionBarOverlayLayout;->a:[I
invoke-virtual {v0, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;
move-result-object v3
invoke-virtual {v3, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I
move-result v0
iput v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->b:I
invoke-virtual {v3, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->g:Landroid/graphics/drawable/Drawable;
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->g:Landroid/graphics/drawable/Drawable;
if-nez v0, :cond_3a
move v0, v1
:goto_21
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->setWillNotDraw(Z)V
invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V
invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;
move-result-object v0
iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I
const/16 v3, 0x13
if-ge v0, v3, :cond_3c
:goto_31
iput-boolean v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->h:Z
invoke-static {p1}, Landroid/support/v4/widget/u;->a(Landroid/content/Context;)Landroid/support/v4/widget/u;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->w:Landroid/support/v4/widget/u;
return-void
:cond_3a
move v0, v2
goto :goto_21
:cond_3c
move v1, v2
goto :goto_31
.end method
.method static synthetic a(Landroid/support/v7/widget/ActionBarOverlayLayout;)V
.registers 1
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->k()V
return-void
.end method
.method private a(FF)Z
.registers 12
const/4 v1, 0x0
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->w:Landroid/support/v4/widget/u;
float-to-int v4, p2
const/high16 v7, -0x8000
const v8, 0x7fffffff
move v2, v1
move v3, v1
move v5, v1
move v6, v1
invoke-virtual/range {v0 .. v8}, Landroid/support/v4/widget/u;->a(IIIIIIII)V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->w:Landroid/support/v4/widget/u;
invoke-virtual {v0}, Landroid/support/v4/widget/u;->d()I
move-result v0
iget-object v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v2}, Landroid/support/v7/widget/ActionBarContainer;->getHeight()I
move-result v2
if-le v0, v2, :cond_1f
const/4 v1, 0x1
:cond_1f
return v1
.end method
.method static synthetic a(Landroid/support/v7/widget/ActionBarOverlayLayout;Z)Z
.registers 2
iput-boolean p1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->l:Z
return p1
.end method
.method private a(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z
.registers 12
const/4 v1, 0x1
const/4 v2, 0x0
invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;
if-eqz p3, :cond_15
iget v3, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->leftMargin:I
iget v4, p2, Landroid/graphics/Rect;->left:I
if-eq v3, v4, :cond_15
iget v2, p2, Landroid/graphics/Rect;->left:I
iput v2, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->leftMargin:I
move v2, v1
:cond_15
if-eqz p4, :cond_22
iget v3, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->topMargin:I
iget v4, p2, Landroid/graphics/Rect;->top:I
if-eq v3, v4, :cond_22
iget v2, p2, Landroid/graphics/Rect;->top:I
iput v2, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->topMargin:I
move v2, v1
:cond_22
if-eqz p6, :cond_2f
iget v3, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->rightMargin:I
iget v4, p2, Landroid/graphics/Rect;->right:I
if-eq v3, v4, :cond_2f
iget v2, p2, Landroid/graphics/Rect;->right:I
iput v2, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->rightMargin:I
move v2, v1
:cond_2f
if-eqz p5, :cond_3d
iget v3, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->bottomMargin:I
iget v4, p2, Landroid/graphics/Rect;->bottom:I
if-eq v3, v4, :cond_3d
iget v2, p2, Landroid/graphics/Rect;->bottom:I
iput v2, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->bottomMargin:I
move v0, v1
:goto_3c
return v0
:cond_3d
move v0, v2
goto :goto_3c
.end method
.method static synthetic b(Landroid/support/v7/widget/ActionBarOverlayLayout;)Landroid/support/v4/f/ay;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->y:Landroid/support/v4/f/ay;
return-object v0
.end method
.method static synthetic c(Landroid/support/v7/widget/ActionBarOverlayLayout;)Landroid/support/v7/widget/ActionBarContainer;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
return-object v0
.end method
.method private k()V
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->z:Ljava/lang/Runnable;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->removeCallbacks(Ljava/lang/Runnable;)Z
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->A:Ljava/lang/Runnable;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->removeCallbacks(Ljava/lang/Runnable;)Z
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->x:Landroid/support/v4/f/au;
if-eqz v0, :cond_13
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->x:Landroid/support/v4/f/au;
invoke-virtual {v0}, Landroid/support/v4/f/au;->b()V
:cond_13
return-void
.end method
.method private l()V
.registers 5
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->k()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->z:Ljava/lang/Runnable;
const-wide/16 v2, 0x258
invoke-virtual {p0, v0, v2, v3}, Landroid/support/v7/widget/ActionBarOverlayLayout;->postDelayed(Ljava/lang/Runnable;J)Z
return-void
.end method
.method private m()V
.registers 5
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->k()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->A:Ljava/lang/Runnable;
const-wide/16 v2, 0x258
invoke-virtual {p0, v0, v2, v3}, Landroid/support/v7/widget/ActionBarOverlayLayout;->postDelayed(Ljava/lang/Runnable;J)Z
return-void
.end method
.method private n()V
.registers 2
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->k()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->z:Ljava/lang/Runnable;
invoke-interface {v0}, Ljava/lang/Runnable;->run()V
return-void
.end method
.method private o()V
.registers 2
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->k()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->A:Ljava/lang/Runnable;
invoke-interface {v0}, Ljava/lang/Runnable;->run()V
return-void
.end method
.method public a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/ActionBarOverlayLayout$b;
.registers 4
new-instance v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getContext()Landroid/content/Context;
move-result-object v1
invoke-direct {v0, v1, p1}, Landroid/support/v7/widget/ActionBarOverlayLayout$b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-object v0
.end method
.method public a(I)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
sparse-switch p1, :sswitch_data_18
:goto_6
return-void
:sswitch_7
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->f()V
goto :goto_6
:sswitch_d
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->g()V
goto :goto_6
:sswitch_13
const/4 v0, 0x1
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->setOverlayMode(Z)V
goto :goto_6
:sswitch_data_18
.sparse-switch
0x2 -> :sswitch_7
0x5 -> :sswitch_d
0x6d -> :sswitch_13
.end sparse-switch
.end method
.method public a(Landroid/view/Menu;Landroid/support/v7/view/menu/l$a;)V
.registers 4
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0, p1, p2}, Landroid/support/v7/widget/ac;->a(Landroid/view/Menu;Landroid/support/v7/view/menu/l$a;)V
return-void
.end method
.method public a()Z
.registers 2
iget-boolean v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->i:Z
return v0
.end method
.method protected b()Landroid/support/v7/widget/ActionBarOverlayLayout$b;
.registers 3
const/4 v1, -0x1
new-instance v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;
invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/ActionBarOverlayLayout$b;-><init>(II)V
return-object v0
.end method
.method  c()V
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->d:Landroid/support/v7/widget/ContentFrameLayout;
if-nez v0, :cond_24
sget v0, Landroid/support/v7/b/a$f;->action_bar_activity_content:I
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ContentFrameLayout;
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->d:Landroid/support/v7/widget/ContentFrameLayout;
sget v0, Landroid/support/v7/b/a$f;->action_bar_container:I
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionBarContainer;
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
sget v0, Landroid/support/v7/b/a$f;->action_bar:I
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->findViewById(I)Landroid/view/View;
move-result-object v0
invoke-direct {p0, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->a(Landroid/view/View;)Landroid/support/v7/widget/ac;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
:cond_24
return-void
.end method
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
.registers 3
instance-of v0, p1, Landroid/support/v7/widget/ActionBarOverlayLayout$b;
return v0
.end method
.method public d()Z
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->h()Z
move-result v0
return v0
.end method
.method public draw(Landroid/graphics/Canvas;)V
.registers 7
const/4 v1, 0x0
invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->g:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_3b
iget-boolean v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->h:Z
if-nez v0, :cond_3b
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContainer;->getVisibility()I
move-result v0
if-nez v0, :cond_3c
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContainer;->getBottom()I
move-result v0
int-to-float v0, v0
iget-object v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-static {v2}, Landroid/support/v4/f/af;->i(Landroid/view/View;)F
move-result v2
add-float/2addr v0, v2
const/high16 v2, 0x3f00
add-float/2addr v0, v2
float-to-int v0, v0
:goto_26
iget-object v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->g:Landroid/graphics/drawable/Drawable;
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getWidth()I
move-result v3
iget-object v4, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->g:Landroid/graphics/drawable/Drawable;
invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I
move-result v4
add-int/2addr v4, v0
invoke-virtual {v2, v1, v0, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->g:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
:cond_3b
return-void
:cond_3c
move v0, v1
goto :goto_26
.end method
.method public e()Z
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->i()Z
move-result v0
return v0
.end method
.method public f()Z
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->j()Z
move-result v0
return v0
.end method
.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
.registers 9
const/4 v3, 0x1
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
invoke-static {p0}, Landroid/support/v4/f/af;->m(Landroid/view/View;)I
move-result v0
and-int/lit16 v0, v0, 0x100
if-eqz v0, :cond_c
:cond_c
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
const/4 v5, 0x0
move-object v0, p0
move-object v2, p1
move v4, v3
move v6, v3
invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/ActionBarOverlayLayout;->a(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z
move-result v0
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->r:Landroid/graphics/Rect;
invoke-virtual {v1, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->r:Landroid/graphics/Rect;
iget-object v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->o:Landroid/graphics/Rect;
invoke-static {p0, v1, v2}, Landroid/support/v7/widget/au;->a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->p:Landroid/graphics/Rect;
iget-object v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->o:Landroid/graphics/Rect;
invoke-virtual {v1, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_35
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->p:Landroid/graphics/Rect;
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->o:Landroid/graphics/Rect;
invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
move v0, v3
:cond_35
if-eqz v0, :cond_3a
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->requestLayout()V
:cond_3a
return v3
.end method
.method public g()Z
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->k()Z
move-result v0
return v0
.end method
.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->b()Landroid/support/v7/widget/ActionBarOverlayLayout$b;
move-result-object v0
return-object v0
.end method
.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
.registers 3
invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionBarOverlayLayout;->a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/ActionBarOverlayLayout$b;
move-result-object v0
return-object v0
.end method
.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
.registers 3
new-instance v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;
invoke-direct {v0, p1}, Landroid/support/v7/widget/ActionBarOverlayLayout$b;-><init>(Landroid/view/ViewGroup$LayoutParams;)V
return-object v0
.end method
.method public getActionBarHideOffset()I
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
if-eqz v0, :cond_d
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-static {v0}, Landroid/support/v4/f/af;->i(Landroid/view/View;)F
move-result v0
float-to-int v0, v0
neg-int v0, v0
:goto_c
return v0
:cond_d
const/4 v0, 0x0
goto :goto_c
.end method
.method public getNestedScrollAxes()I
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->B:Landroid/support/v4/f/z;
invoke-virtual {v0}, Landroid/support/v4/f/z;->a()I
move-result v0
return v0
.end method
.method public getTitle()Ljava/lang/CharSequence;
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->e()Ljava/lang/CharSequence;
move-result-object v0
return-object v0
.end method
.method public h()Z
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->l()Z
move-result v0
return v0
.end method
.method public i()V
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->m()V
return-void
.end method
.method public j()V
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0}, Landroid/support/v7/widget/ac;->n()V
return-void
.end method
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
.registers 4
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x8
if-lt v0, v1, :cond_9
invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V
:cond_9
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getContext()Landroid/content/Context;
move-result-object v0
invoke-direct {p0, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->a(Landroid/content/Context;)V
invoke-static {p0}, Landroid/support/v4/f/af;->n(Landroid/view/View;)V
return-void
.end method
.method protected onDetachedFromWindow()V
.registers 1
invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->k()V
return-void
.end method
.method protected onLayout(ZIIII)V
.registers 15
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getChildCount()I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getPaddingLeft()I
move-result v3
sub-int v0, p4, p2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getPaddingRight()I
move-result v1
sub-int/2addr v0, v1
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getPaddingTop()I
move-result v4
sub-int v0, p5, p3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getPaddingBottom()I
move-result v1
sub-int/2addr v0, v1
const/4 v0, 0x0
move v1, v0
:goto_1c
if-ge v1, v2, :cond_47
invoke-virtual {p0, v1}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getChildAt(I)Landroid/view/View;
move-result-object v5
invoke-virtual {v5}, Landroid/view/View;->getVisibility()I
move-result v0
const/16 v6, 0x8
if-eq v0, v6, :cond_43
invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;
invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I
move-result v6
invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I
move-result v7
iget v8, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->leftMargin:I
add-int/2addr v8, v3
iget v0, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->topMargin:I
add-int/2addr v0, v4
add-int/2addr v6, v8
add-int/2addr v7, v0
invoke-virtual {v5, v8, v0, v6, v7}, Landroid/view/View;->layout(IIII)V
:cond_43
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_1c
:cond_47
return-void
.end method
.method protected onMeasure(II)V
.registers 14
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
const/4 v7, 0x0
const/4 v8, 0x0
const/4 v9, 0x0
const/4 v6, 0x0
const/4 v10, 0x0
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
const/4 v3, 0x0
const/4 v5, 0x0
move-object v0, p0
move v2, p1
move v4, p2
invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/ActionBarOverlayLayout;->measureChildWithMargins(Landroid/view/View;IIII)V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v1}, Landroid/support/v7/widget/ActionBarContainer;->getMeasuredWidth()I
move-result v1
iget v2, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->leftMargin:I
add-int/2addr v1, v2
iget v2, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->rightMargin:I
add-int/2addr v1, v2
invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I
move-result v8
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v1}, Landroid/support/v7/widget/ActionBarContainer;->getMeasuredHeight()I
move-result v1
iget v2, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->topMargin:I
add-int/2addr v1, v2
iget v0, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->bottomMargin:I
add-int/2addr v0, v1
invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I
move-result v7
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-static {v0}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v0
invoke-static {v9, v0}, Landroid/support/v7/widget/au;->a(II)I
move-result v9
invoke-static {p0}, Landroid/support/v4/f/af;->m(Landroid/view/View;)I
move-result v0
and-int/lit16 v0, v0, 0x100
if-eqz v0, :cond_115
const/4 v0, 0x1
move v1, v0
:goto_4e
if-eqz v1, :cond_119
iget v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->b:I
iget-boolean v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->j:Z
if-eqz v2, :cond_61
iget-object v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v2}, Landroid/support/v7/widget/ActionBarContainer;->getTabContainer()Landroid/view/View;
move-result-object v2
if-eqz v2, :cond_61
iget v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->b:I
add-int/2addr v0, v2
:goto_61
:cond_61
iget-object v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->q:Landroid/graphics/Rect;
iget-object v3, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->o:Landroid/graphics/Rect;
invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
iget-object v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->s:Landroid/graphics/Rect;
iget-object v3, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->r:Landroid/graphics/Rect;
invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
iget-boolean v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->i:Z
if-nez v2, :cond_12b
if-nez v1, :cond_12b
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->q:Landroid/graphics/Rect;
iget v2, v1, Landroid/graphics/Rect;->top:I
add-int/2addr v0, v2
iput v0, v1, Landroid/graphics/Rect;->top:I
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->q:Landroid/graphics/Rect;
iget v1, v0, Landroid/graphics/Rect;->bottom:I
add-int/2addr v1, v10
iput v1, v0, Landroid/graphics/Rect;->bottom:I
:goto_83
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->d:Landroid/support/v7/widget/ContentFrameLayout;
iget-object v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->q:Landroid/graphics/Rect;
const/4 v3, 0x1
const/4 v4, 0x1
const/4 v5, 0x1
const/4 v6, 0x1
move-object v0, p0
invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/ActionBarOverlayLayout;->a(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->t:Landroid/graphics/Rect;
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->s:Landroid/graphics/Rect;
invoke-virtual {v0, v1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z
move-result v0
if-nez v0, :cond_a7
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->t:Landroid/graphics/Rect;
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->s:Landroid/graphics/Rect;
invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->d:Landroid/support/v7/widget/ContentFrameLayout;
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->s:Landroid/graphics/Rect;
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ContentFrameLayout;->a(Landroid/graphics/Rect;)V
:cond_a7
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->d:Landroid/support/v7/widget/ContentFrameLayout;
const/4 v3, 0x0
const/4 v5, 0x0
move-object v0, p0
move v2, p1
move v4, p2
invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/ActionBarOverlayLayout;->measureChildWithMargins(Landroid/view/View;IIII)V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->d:Landroid/support/v7/widget/ContentFrameLayout;
invoke-virtual {v0}, Landroid/support/v7/widget/ContentFrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->d:Landroid/support/v7/widget/ContentFrameLayout;
invoke-virtual {v1}, Landroid/support/v7/widget/ContentFrameLayout;->getMeasuredWidth()I
move-result v1
iget v2, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->leftMargin:I
add-int/2addr v1, v2
iget v2, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->rightMargin:I
add-int/2addr v1, v2
invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I
move-result v1
iget-object v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->d:Landroid/support/v7/widget/ContentFrameLayout;
invoke-virtual {v2}, Landroid/support/v7/widget/ContentFrameLayout;->getMeasuredHeight()I
move-result v2
iget v3, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->topMargin:I
add-int/2addr v2, v3
iget v0, v0, Landroid/support/v7/widget/ActionBarOverlayLayout$b;->bottomMargin:I
add-int/2addr v0, v2
invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I
move-result v0
iget-object v2, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->d:Landroid/support/v7/widget/ContentFrameLayout;
invoke-static {v2}, Landroid/support/v4/f/af;->f(Landroid/view/View;)I
move-result v2
invoke-static {v9, v2}, Landroid/support/v7/widget/au;->a(II)I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getPaddingLeft()I
move-result v3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getPaddingRight()I
move-result v4
add-int/2addr v3, v4
add-int/2addr v1, v3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getPaddingTop()I
move-result v3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getPaddingBottom()I
move-result v4
add-int/2addr v3, v4
add-int/2addr v0, v3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getSuggestedMinimumHeight()I
move-result v3
invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I
move-result v0
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getSuggestedMinimumWidth()I
move-result v3
invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I
move-result v1
invoke-static {v1, p1, v2}, Landroid/support/v4/f/af;->a(III)I
move-result v1
shl-int/lit8 v2, v2, 0x10
invoke-static {v0, p2, v2}, Landroid/support/v4/f/af;->a(III)I
move-result v0
invoke-virtual {p0, v1, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->setMeasuredDimension(II)V
return-void
:cond_115
const/4 v0, 0x0
move v1, v0
goto/16 :goto_4e
:cond_119
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContainer;->getVisibility()I
move-result v0
const/16 v2, 0x8
if-eq v0, v2, :cond_13b
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContainer;->getMeasuredHeight()I
move-result v0
goto/16 :goto_61
:cond_12b
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->s:Landroid/graphics/Rect;
iget v2, v1, Landroid/graphics/Rect;->top:I
add-int/2addr v0, v2
iput v0, v1, Landroid/graphics/Rect;->top:I
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->s:Landroid/graphics/Rect;
iget v1, v0, Landroid/graphics/Rect;->bottom:I
add-int/2addr v1, v10
iput v1, v0, Landroid/graphics/Rect;->bottom:I
goto/16 :goto_83
:cond_13b
move v0, v6
goto/16 :goto_61
.end method
.method public onNestedFling(Landroid/view/View;FFZ)Z
.registers 7
const/4 v0, 0x1
iget-boolean v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->k:Z
if-eqz v1, :cond_7
if-nez p4, :cond_9
:cond_7
const/4 v0, 0x0
:goto_8
return v0
:cond_9
invoke-direct {p0, p2, p3}, Landroid/support/v7/widget/ActionBarOverlayLayout;->a(FF)Z
move-result v1
if-eqz v1, :cond_15
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->o()V
:goto_12
iput-boolean v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->l:Z
goto :goto_8
:cond_15
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->n()V
goto :goto_12
.end method
.method public onNestedPreFling(Landroid/view/View;FF)Z
.registers 5
const/4 v0, 0x0
return v0
.end method
.method public onNestedPreScroll(Landroid/view/View;II[I)V
.registers 5
return-void
.end method
.method public onNestedScroll(Landroid/view/View;IIII)V
.registers 7
iget v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->m:I
add-int/2addr v0, p3
iput v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->m:I
iget v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->m:I
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->setActionBarHideOffset(I)V
return-void
.end method
.method public onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
.registers 5
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->B:Landroid/support/v4/f/z;
invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/f/z;->a(Landroid/view/View;Landroid/view/View;I)V
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getActionBarHideOffset()I
move-result v0
iput v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->m:I
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->k()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
if-eqz v0, :cond_17
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
invoke-interface {v0}, Landroid/support/v7/widget/ActionBarOverlayLayout$a;->n()V
:cond_17
return-void
.end method
.method public onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
.registers 5
and-int/lit8 v0, p3, 0x2
if-eqz v0, :cond_c
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContainer;->getVisibility()I
move-result v0
if-eqz v0, :cond_e
:cond_c
const/4 v0, 0x0
:goto_d
return v0
:cond_e
iget-boolean v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->k:Z
goto :goto_d
.end method
.method public onStopNestedScroll(Landroid/view/View;)V
.registers 4
iget-boolean v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->k:Z
if-eqz v0, :cond_15
iget-boolean v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->l:Z
if-nez v0, :cond_15
iget v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->m:I
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v1}, Landroid/support/v7/widget/ActionBarContainer;->getHeight()I
move-result v1
if-gt v0, v1, :cond_1f
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->l()V
:cond_15
:goto_15
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
if-eqz v0, :cond_1e
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
invoke-interface {v0}, Landroid/support/v7/widget/ActionBarOverlayLayout$a;->o()V
:cond_1e
return-void
:cond_1f
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->m()V
goto :goto_15
.end method
.method public onWindowSystemUiVisibilityChanged(I)V
.registers 8
const/4 v1, 0x1
const/4 v2, 0x0
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v3, 0x10
if-lt v0, v3, :cond_b
invoke-super {p0, p1}, Landroid/view/ViewGroup;->onWindowSystemUiVisibilityChanged(I)V
:cond_b
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->n:I
xor-int v4, v0, p1
iput p1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->n:I
and-int/lit8 v0, p1, 0x4
if-nez v0, :cond_3e
move v3, v1
:goto_19
and-int/lit16 v0, p1, 0x100
if-eqz v0, :cond_40
move v0, v1
:goto_1e
iget-object v5, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
if-eqz v5, :cond_32
iget-object v5, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
if-nez v0, :cond_42
:goto_26
invoke-interface {v5, v1}, Landroid/support/v7/widget/ActionBarOverlayLayout$a;->g(Z)V
if-nez v3, :cond_2d
if-nez v0, :cond_44
:cond_2d
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
invoke-interface {v0}, Landroid/support/v7/widget/ActionBarOverlayLayout$a;->l()V
:goto_32
:cond_32
and-int/lit16 v0, v4, 0x100
if-eqz v0, :cond_3d
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
if-eqz v0, :cond_3d
invoke-static {p0}, Landroid/support/v4/f/af;->n(Landroid/view/View;)V
:cond_3d
return-void
:cond_3e
move v3, v2
goto :goto_19
:cond_40
move v0, v2
goto :goto_1e
:cond_42
move v1, v2
goto :goto_26
:cond_44
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
invoke-interface {v0}, Landroid/support/v7/widget/ActionBarOverlayLayout$a;->m()V
goto :goto_32
.end method
.method protected onWindowVisibilityChanged(I)V
.registers 3
invoke-super {p0, p1}, Landroid/view/ViewGroup;->onWindowVisibilityChanged(I)V
iput p1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->c:I
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
if-eqz v0, :cond_e
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
invoke-interface {v0, p1}, Landroid/support/v7/widget/ActionBarOverlayLayout$a;->a(I)V
:cond_e
return-void
.end method
.method public setActionBarHideOffset(I)V
.registers 4
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->k()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
invoke-virtual {v0}, Landroid/support/v7/widget/ActionBarContainer;->getHeight()I
move-result v0
const/4 v1, 0x0
invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I
move-result v0
invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I
move-result v0
iget-object v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->e:Landroid/support/v7/widget/ActionBarContainer;
neg-int v0, v0
int-to-float v0, v0
invoke-static {v1, v0}, Landroid/support/v4/f/af;->a(Landroid/view/View;F)V
return-void
.end method
.method public setActionBarVisibilityCallback(Landroid/support/v7/widget/ActionBarOverlayLayout$a;)V
.registers 4
iput-object p1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getWindowToken()Landroid/os/IBinder;
move-result-object v0
if-eqz v0, :cond_1b
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->u:Landroid/support/v7/widget/ActionBarOverlayLayout$a;
iget v1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->c:I
invoke-interface {v0, v1}, Landroid/support/v7/widget/ActionBarOverlayLayout$a;->a(I)V
iget v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->n:I
if-eqz v0, :cond_1b
iget v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->n:I
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->onWindowSystemUiVisibilityChanged(I)V
invoke-static {p0}, Landroid/support/v4/f/af;->n(Landroid/view/View;)V
:cond_1b
return-void
.end method
.method public setHasNonEmbeddedTabs(Z)V
.registers 2
iput-boolean p1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->j:Z
return-void
.end method
.method public setHideOnContentScrollEnabled(Z)V
.registers 3
iget-boolean v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->k:Z
if-eq p1, v0, :cond_f
iput-boolean p1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->k:Z
if-nez p1, :cond_f
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->k()V
const/4 v0, 0x0
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->setActionBarHideOffset(I)V
:cond_f
return-void
.end method
.method public setIcon(I)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0, p1}, Landroid/support/v7/widget/ac;->a(I)V
return-void
.end method
.method public setIcon(Landroid/graphics/drawable/Drawable;)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0, p1}, Landroid/support/v7/widget/ac;->a(Landroid/graphics/drawable/Drawable;)V
return-void
.end method
.method public setLogo(I)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0, p1}, Landroid/support/v7/widget/ac;->b(I)V
return-void
.end method
.method public setOverlayMode(Z)V
.registers 4
iput-boolean p1, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->i:Z
if-eqz p1, :cond_16
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->getContext()Landroid/content/Context;
move-result-object v0
invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;
move-result-object v0
iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I
const/16 v1, 0x13
if-ge v0, v1, :cond_16
const/4 v0, 0x1
:goto_13
iput-boolean v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->h:Z
return-void
:cond_16
const/4 v0, 0x0
goto :goto_13
.end method
.method public setShowingForActionMode(Z)V
.registers 2
return-void
.end method
.method public setUiOptions(I)V
.registers 2
return-void
.end method
.method public setWindowCallback(Landroid/view/Window$Callback;)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0, p1}, Landroid/support/v7/widget/ac;->a(Landroid/view/Window$Callback;)V
return-void
.end method
.method public setWindowTitle(Ljava/lang/CharSequence;)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarOverlayLayout;->c()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarOverlayLayout;->f:Landroid/support/v7/widget/ac;
invoke-interface {v0, p1}, Landroid/support/v7/widget/ac;->a(Ljava/lang/CharSequence;)V
return-void
.end method
.method public shouldDelayChildPressedState()Z
.registers 2
const/4 v0, 0x0
return v0
.end method