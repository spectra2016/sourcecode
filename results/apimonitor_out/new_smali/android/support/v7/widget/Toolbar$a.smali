.class  Landroid/support/v7/widget/Toolbar$a;
.super Ljava/lang/Object;
.source "Toolbar.java"
.implements Landroid/support/v7/view/menu/l;
.field  a:Landroid/support/v7/view/menu/f;
.field  b:Landroid/support/v7/view/menu/h;
.field final synthetic c:Landroid/support/v7/widget/Toolbar;
.method private constructor <init>(Landroid/support/v7/widget/Toolbar;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method synthetic constructor <init>(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/widget/Toolbar$1;)V
.registers 3
invoke-direct {p0, p1}, Landroid/support/v7/widget/Toolbar$a;-><init>(Landroid/support/v7/widget/Toolbar;)V
return-void
.end method
.method public a(Landroid/content/Context;Landroid/support/v7/view/menu/f;)V
.registers 5
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->a:Landroid/support/v7/view/menu/f;
if-eqz v0, :cond_f
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->b:Landroid/support/v7/view/menu/h;
if-eqz v0, :cond_f
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->a:Landroid/support/v7/view/menu/f;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar$a;->b:Landroid/support/v7/view/menu/h;
invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/f;->d(Landroid/support/v7/view/menu/h;)Z
:cond_f
iput-object p2, p0, Landroid/support/v7/widget/Toolbar$a;->a:Landroid/support/v7/view/menu/f;
return-void
.end method
.method public a(Landroid/support/v7/view/menu/f;Z)V
.registers 3
return-void
.end method
.method public a(Landroid/support/v7/view/menu/f;Landroid/support/v7/view/menu/h;)Z
.registers 7
const/4 v3, 0x1
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
invoke-static {v0}, Landroid/support/v7/widget/Toolbar;->b(Landroid/support/v7/widget/Toolbar;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
invoke-static {v0}, Landroid/support/v7/widget/Toolbar;->c(Landroid/support/v7/widget/Toolbar;)Landroid/widget/ImageButton;
move-result-object v0
invoke-virtual {v0}, Landroid/widget/ImageButton;->getParent()Landroid/view/ViewParent;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
if-eq v0, v1, :cond_1f
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
invoke-static {v1}, Landroid/support/v7/widget/Toolbar;->c(Landroid/support/v7/widget/Toolbar;)Landroid/widget/ImageButton;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V
:cond_1f
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
invoke-virtual {p2}, Landroid/support/v7/view/menu/h;->getActionView()Landroid/view/View;
move-result-object v1
iput-object v1, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
iput-object p2, p0, Landroid/support/v7/widget/Toolbar$a;->b:Landroid/support/v7/view/menu/h;
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
if-eq v0, v1, :cond_5c
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->i()Landroid/support/v7/widget/Toolbar$b;
move-result-object v0
const v1, 0x800003
iget-object v2, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
invoke-static {v2}, Landroid/support/v7/widget/Toolbar;->d(Landroid/support/v7/widget/Toolbar;)I
move-result v2
and-int/lit8 v2, v2, 0x70
or-int/2addr v1, v2
iput v1, v0, Landroid/support/v7/widget/Toolbar$b;->a:I
const/4 v1, 0x2
iput v1, v0, Landroid/support/v7/widget/Toolbar$b;->b:I
iget-object v1, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V
:cond_5c
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->j()V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->requestLayout()V
invoke-virtual {p2, v3}, Landroid/support/v7/view/menu/h;->e(Z)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
instance-of v0, v0, Landroid/support/v7/view/c;
if-eqz v0, :cond_7a
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
check-cast v0, Landroid/support/v7/view/c;
invoke-interface {v0}, Landroid/support/v7/view/c;->a()V
:cond_7a
return v3
.end method
.method public a(Landroid/support/v7/view/menu/p;)Z
.registers 3
const/4 v0, 0x0
return v0
.end method
.method public b(Z)V
.registers 7
const/4 v0, 0x0
iget-object v1, p0, Landroid/support/v7/widget/Toolbar$a;->b:Landroid/support/v7/view/menu/h;
if-eqz v1, :cond_26
iget-object v1, p0, Landroid/support/v7/widget/Toolbar$a;->a:Landroid/support/v7/view/menu/f;
if-eqz v1, :cond_1d
iget-object v1, p0, Landroid/support/v7/widget/Toolbar$a;->a:Landroid/support/v7/view/menu/f;
invoke-virtual {v1}, Landroid/support/v7/view/menu/f;->size()I
move-result v2
move v1, v0
:goto_10
if-ge v1, v2, :cond_1d
iget-object v3, p0, Landroid/support/v7/widget/Toolbar$a;->a:Landroid/support/v7/view/menu/f;
invoke-virtual {v3, v1}, Landroid/support/v7/view/menu/f;->getItem(I)Landroid/view/MenuItem;
move-result-object v3
iget-object v4, p0, Landroid/support/v7/widget/Toolbar$a;->b:Landroid/support/v7/view/menu/h;
if-ne v3, v4, :cond_27
const/4 v0, 0x1
:cond_1d
if-nez v0, :cond_26
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->a:Landroid/support/v7/view/menu/f;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar$a;->b:Landroid/support/v7/view/menu/h;
invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/Toolbar$a;->b(Landroid/support/v7/view/menu/f;Landroid/support/v7/view/menu/h;)Z
:cond_26
return-void
:cond_27
add-int/lit8 v1, v1, 0x1
goto :goto_10
.end method
.method public b()Z
.registers 2
const/4 v0, 0x0
return v0
.end method
.method public b(Landroid/support/v7/view/menu/f;Landroid/support/v7/view/menu/h;)Z
.registers 6
const/4 v2, 0x0
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
instance-of v0, v0, Landroid/support/v7/view/c;
if-eqz v0, :cond_12
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
check-cast v0, Landroid/support/v7/view/c;
invoke-interface {v0}, Landroid/support/v7/view/c;->b()V
:cond_12
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
iget-object v1, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
invoke-static {v1}, Landroid/support/v7/widget/Toolbar;->c(Landroid/support/v7/widget/Toolbar;)Landroid/widget/ImageButton;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
iput-object v2, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/view/View;
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->k()V
iput-object v2, p0, Landroid/support/v7/widget/Toolbar$a;->b:Landroid/support/v7/view/menu/h;
iget-object v0, p0, Landroid/support/v7/widget/Toolbar$a;->c:Landroid/support/v7/widget/Toolbar;
invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->requestLayout()V
const/4 v0, 0x0
invoke-virtual {p2, v0}, Landroid/support/v7/view/menu/h;->e(Z)V
const/4 v0, 0x1
return v0
.end method