.class abstract Landroid/support/v7/widget/a;
.super Landroid/view/ViewGroup;
.source "AbsActionBarView.java"
.field protected final a:Landroid/support/v7/widget/a$a;
.field protected final b:Landroid/content/Context;
.field protected c:Landroid/support/v7/widget/ActionMenuView;
.field protected d:Landroid/support/v7/widget/d;
.field protected e:I
.field protected f:Landroid/support/v4/f/au;
.field private g:Z
.field private h:Z
.method constructor <init>(Landroid/content/Context;)V
.registers 3
const/4 v0, 0x0
invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-void
.end method
.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 4
const/4 v0, 0x0
invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return-void
.end method
.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.registers 8
invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
new-instance v0, Landroid/support/v7/widget/a$a;
invoke-direct {v0, p0}, Landroid/support/v7/widget/a$a;-><init>(Landroid/support/v7/widget/a;)V
iput-object v0, p0, Landroid/support/v7/widget/a;->a:Landroid/support/v7/widget/a$a;
new-instance v0, Landroid/util/TypedValue;
invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V
invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v1
sget v2, Landroid/support/v7/b/a$a;->actionBarPopupTheme:I
const/4 v3, 0x1
invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z
move-result v1
if-eqz v1, :cond_2a
iget v1, v0, Landroid/util/TypedValue;->resourceId:I
if-eqz v1, :cond_2a
new-instance v1, Landroid/view/ContextThemeWrapper;
iget v0, v0, Landroid/util/TypedValue;->resourceId:I
invoke-direct {v1, p1, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V
iput-object v1, p0, Landroid/support/v7/widget/a;->b:Landroid/content/Context;
:goto_29
return-void
:cond_2a
iput-object p1, p0, Landroid/support/v7/widget/a;->b:Landroid/content/Context;
goto :goto_29
.end method
.method protected static a(IIZ)I
.registers 4
if-eqz p2, :cond_5
sub-int v0, p0, p1
:goto_4
return v0
:cond_5
add-int v0, p0, p1
goto :goto_4
.end method
.method static synthetic a(Landroid/support/v7/widget/a;I)V
.registers 2
invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V
return-void
.end method
.method static synthetic b(Landroid/support/v7/widget/a;I)V
.registers 2
invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V
return-void
.end method
.method protected a(Landroid/view/View;III)I
.registers 7
const/high16 v0, -0x8000
invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v0
invoke-virtual {p1, v0, p3}, Landroid/view/View;->measure(II)V
invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I
move-result v0
sub-int v0, p2, v0
sub-int/2addr v0, p4
const/4 v1, 0x0
invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I
move-result v0
return v0
.end method
.method protected a(Landroid/view/View;IIIZ)I
.registers 10
invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I
move-result v0
invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I
move-result v1
sub-int v2, p4, v1
div-int/lit8 v2, v2, 0x2
add-int/2addr v2, p3
if-eqz p5, :cond_19
sub-int v3, p2, v0
add-int/2addr v1, v2
invoke-virtual {p1, v3, v2, p2, v1}, Landroid/view/View;->layout(IIII)V
:goto_15
if-eqz p5, :cond_18
neg-int v0, v0
:cond_18
return v0
:cond_19
add-int v3, p2, v0
add-int/2addr v1, v2
invoke-virtual {p1, p2, v2, v3, v1}, Landroid/view/View;->layout(IIII)V
goto :goto_15
.end method
.method public a(IJ)Landroid/support/v4/f/au;
.registers 6
const/4 v1, 0x0
iget-object v0, p0, Landroid/support/v7/widget/a;->f:Landroid/support/v4/f/au;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/v7/widget/a;->f:Landroid/support/v4/f/au;
invoke-virtual {v0}, Landroid/support/v4/f/au;->b()V
:cond_a
if-nez p1, :cond_2c
invoke-virtual {p0}, Landroid/support/v7/widget/a;->getVisibility()I
move-result v0
if-eqz v0, :cond_15
invoke-static {p0, v1}, Landroid/support/v4/f/af;->b(Landroid/view/View;F)V
:cond_15
invoke-static {p0}, Landroid/support/v4/f/af;->k(Landroid/view/View;)Landroid/support/v4/f/au;
move-result-object v0
const/high16 v1, 0x3f80
invoke-virtual {v0, v1}, Landroid/support/v4/f/au;->a(F)Landroid/support/v4/f/au;
move-result-object v0
invoke-virtual {v0, p2, p3}, Landroid/support/v4/f/au;->a(J)Landroid/support/v4/f/au;
iget-object v1, p0, Landroid/support/v7/widget/a;->a:Landroid/support/v7/widget/a$a;
invoke-virtual {v1, v0, p1}, Landroid/support/v7/widget/a$a;->a(Landroid/support/v4/f/au;I)Landroid/support/v7/widget/a$a;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/ay;)Landroid/support/v4/f/au;
:goto_2b
return-object v0
:cond_2c
invoke-static {p0}, Landroid/support/v4/f/af;->k(Landroid/view/View;)Landroid/support/v4/f/au;
move-result-object v0
invoke-virtual {v0, v1}, Landroid/support/v4/f/au;->a(F)Landroid/support/v4/f/au;
move-result-object v0
invoke-virtual {v0, p2, p3}, Landroid/support/v4/f/au;->a(J)Landroid/support/v4/f/au;
iget-object v1, p0, Landroid/support/v7/widget/a;->a:Landroid/support/v7/widget/a$a;
invoke-virtual {v1, v0, p1}, Landroid/support/v7/widget/a$a;->a(Landroid/support/v4/f/au;I)Landroid/support/v7/widget/a$a;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/ay;)Landroid/support/v4/f/au;
goto :goto_2b
.end method
.method public a()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/a;->d:Landroid/support/v7/widget/d;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/widget/a;->d:Landroid/support/v7/widget/d;
invoke-virtual {v0}, Landroid/support/v7/widget/d;->d()Z
move-result v0
:goto_a
return v0
:cond_b
const/4 v0, 0x0
goto :goto_a
.end method
.method public getAnimatedVisibility()I
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/a;->f:Landroid/support/v4/f/au;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/widget/a;->a:Landroid/support/v7/widget/a$a;
iget v0, v0, Landroid/support/v7/widget/a$a;->a:I
:goto_8
return v0
:cond_9
invoke-virtual {p0}, Landroid/support/v7/widget/a;->getVisibility()I
move-result v0
goto :goto_8
.end method
.method public getContentHeight()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/a;->e:I
return v0
.end method
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
.registers 7
const/4 v4, 0x0
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x8
if-lt v0, v1, :cond_a
invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V
:cond_a
invoke-virtual {p0}, Landroid/support/v7/widget/a;->getContext()Landroid/content/Context;
move-result-object v0
const/4 v1, 0x0
sget-object v2, Landroid/support/v7/b/a$k;->ActionBar:[I
sget v3, Landroid/support/v7/b/a$a;->actionBarStyle:I
invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
move-result-object v0
sget v1, Landroid/support/v7/b/a$k;->ActionBar_height:I
invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I
move-result v1
invoke-virtual {p0, v1}, Landroid/support/v7/widget/a;->setContentHeight(I)V
invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
iget-object v0, p0, Landroid/support/v7/widget/a;->d:Landroid/support/v7/widget/d;
if-eqz v0, :cond_2c
iget-object v0, p0, Landroid/support/v7/widget/a;->d:Landroid/support/v7/widget/d;
invoke-virtual {v0, p1}, Landroid/support/v7/widget/d;->a(Landroid/content/res/Configuration;)V
:cond_2c
return-void
.end method
.method public onHoverEvent(Landroid/view/MotionEvent;)Z
.registers 7
const/16 v4, 0x9
const/4 v3, 0x1
const/4 v2, 0x0
invoke-static {p1}, Landroid/support/v4/f/s;->a(Landroid/view/MotionEvent;)I
move-result v0
if-ne v0, v4, :cond_c
iput-boolean v2, p0, Landroid/support/v7/widget/a;->h:Z
:cond_c
iget-boolean v1, p0, Landroid/support/v7/widget/a;->h:Z
if-nez v1, :cond_1a
invoke-super {p0, p1}, Landroid/view/ViewGroup;->onHoverEvent(Landroid/view/MotionEvent;)Z
move-result v1
if-ne v0, v4, :cond_1a
if-nez v1, :cond_1a
iput-boolean v3, p0, Landroid/support/v7/widget/a;->h:Z
:cond_1a
const/16 v1, 0xa
if-eq v0, v1, :cond_21
const/4 v1, 0x3
if-ne v0, v1, :cond_23
:cond_21
iput-boolean v2, p0, Landroid/support/v7/widget/a;->h:Z
:cond_23
return v3
.end method
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
.registers 6
const/4 v3, 0x0
const/4 v2, 0x1
invoke-static {p1}, Landroid/support/v4/f/s;->a(Landroid/view/MotionEvent;)I
move-result v0
if-nez v0, :cond_a
iput-boolean v3, p0, Landroid/support/v7/widget/a;->g:Z
:cond_a
iget-boolean v1, p0, Landroid/support/v7/widget/a;->g:Z
if-nez v1, :cond_18
invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z
move-result v1
if-nez v0, :cond_18
if-nez v1, :cond_18
iput-boolean v2, p0, Landroid/support/v7/widget/a;->g:Z
:cond_18
if-eq v0, v2, :cond_1d
const/4 v1, 0x3
if-ne v0, v1, :cond_1f
:cond_1d
iput-boolean v3, p0, Landroid/support/v7/widget/a;->g:Z
:cond_1f
return v2
.end method
.method public setContentHeight(I)V
.registers 2
iput p1, p0, Landroid/support/v7/widget/a;->e:I
invoke-virtual {p0}, Landroid/support/v7/widget/a;->requestLayout()V
return-void
.end method
.method public setVisibility(I)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/a;->getVisibility()I
move-result v0
if-eq p1, v0, :cond_12
iget-object v0, p0, Landroid/support/v7/widget/a;->f:Landroid/support/v4/f/au;
if-eqz v0, :cond_f
iget-object v0, p0, Landroid/support/v7/widget/a;->f:Landroid/support/v4/f/au;
invoke-virtual {v0}, Landroid/support/v4/f/au;->b()V
:cond_f
invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V
:cond_12
return-void
.end method