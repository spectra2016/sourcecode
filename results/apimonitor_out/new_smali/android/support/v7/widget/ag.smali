.class public Landroid/support/v7/widget/ag;
.super Ljava/lang/Object;
.source "ListPopupWindow.java"
.field private static a:Ljava/lang/reflect/Method;
.field private static c:Ljava/lang/reflect/Method;
.field private final A:Landroid/support/v7/widget/ag$c;
.field private B:Ljava/lang/Runnable;
.field private final C:Landroid/os/Handler;
.field private D:Landroid/graphics/Rect;
.field private E:Z
.field private F:I
.field  b:I
.field private d:Landroid/content/Context;
.field private e:Landroid/widget/PopupWindow;
.field private f:Landroid/widget/ListAdapter;
.field private g:Landroid/support/v7/widget/ag$a;
.field private h:I
.field private i:I
.field private j:I
.field private k:I
.field private l:I
.field private m:Z
.field private n:I
.field private o:Z
.field private p:Z
.field private q:Landroid/view/View;
.field private r:I
.field private s:Landroid/database/DataSetObserver;
.field private t:Landroid/view/View;
.field private u:Landroid/graphics/drawable/Drawable;
.field private v:Landroid/widget/AdapterView$OnItemClickListener;
.field private w:Landroid/widget/AdapterView$OnItemSelectedListener;
.field private final x:Landroid/support/v7/widget/ag$g;
.field private final y:Landroid/support/v7/widget/ag$f;
.field private final z:Landroid/support/v7/widget/ag$e;
.method static constructor <clinit>()V
.registers 5
:try_start_0
const-class v0, Landroid/widget/PopupWindow;
const-string v1, "setClipToScreenEnabled"
const/4 v2, 0x1
new-array v2, v2, [Ljava/lang/Class;
const/4 v3, 0x0
sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;
aput-object v4, v2, v3
invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
move-result-object v0
sput-object v0, Landroid/support/v7/widget/ag;->a:Ljava/lang/reflect/Method;
:try_start_12
:goto_12
:try_end_12
.catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_12} :catch_2f
const-class v0, Landroid/widget/PopupWindow;
const-string v1, "getMaxAvailableHeight"
const/4 v2, 0x3
new-array v2, v2, [Ljava/lang/Class;
const/4 v3, 0x0
const-class v4, Landroid/view/View;
aput-object v4, v2, v3
const/4 v3, 0x1
sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;
aput-object v4, v2, v3
const/4 v3, 0x2
sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;
aput-object v4, v2, v3
invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
move-result-object v0
sput-object v0, Landroid/support/v7/widget/ag;->c:Ljava/lang/reflect/Method;
:goto_2e
:try_end_2e
.catch Ljava/lang/NoSuchMethodException; {:try_start_12 .. :try_end_2e} :catch_38
return-void
:catch_2f
move-exception v0
const-string v0, "ListPopupWindow"
const-string v1, "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well."
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_12
:catch_38
move-exception v0
const-string v0, "ListPopupWindow"
const-string v1, "Could not find method getMaxAvailableHeight(View, int, boolean) on PopupWindow. Oh well."
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_2e
.end method
.method public constructor <init>(Landroid/content/Context;)V
.registers 4
const/4 v0, 0x0
sget v1, Landroid/support/v7/b/a$a;->listPopupWindowStyle:I
invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/widget/ag;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.registers 5
const/4 v0, 0x0
invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v7/widget/ag;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
.registers 9
const/4 v3, 0x1
const/4 v0, -0x2
const/4 v1, 0x0
const/4 v2, 0x0
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput v0, p0, Landroid/support/v7/widget/ag;->h:I
iput v0, p0, Landroid/support/v7/widget/ag;->i:I
const/16 v0, 0x3ea
iput v0, p0, Landroid/support/v7/widget/ag;->l:I
iput v2, p0, Landroid/support/v7/widget/ag;->n:I
iput-boolean v2, p0, Landroid/support/v7/widget/ag;->o:Z
iput-boolean v2, p0, Landroid/support/v7/widget/ag;->p:Z
const v0, 0x7fffffff
iput v0, p0, Landroid/support/v7/widget/ag;->b:I
iput v2, p0, Landroid/support/v7/widget/ag;->r:I
new-instance v0, Landroid/support/v7/widget/ag$g;
invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/ag$g;-><init>(Landroid/support/v7/widget/ag;Landroid/support/v7/widget/ag$1;)V
iput-object v0, p0, Landroid/support/v7/widget/ag;->x:Landroid/support/v7/widget/ag$g;
new-instance v0, Landroid/support/v7/widget/ag$f;
invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/ag$f;-><init>(Landroid/support/v7/widget/ag;Landroid/support/v7/widget/ag$1;)V
iput-object v0, p0, Landroid/support/v7/widget/ag;->y:Landroid/support/v7/widget/ag$f;
new-instance v0, Landroid/support/v7/widget/ag$e;
invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/ag$e;-><init>(Landroid/support/v7/widget/ag;Landroid/support/v7/widget/ag$1;)V
iput-object v0, p0, Landroid/support/v7/widget/ag;->z:Landroid/support/v7/widget/ag$e;
new-instance v0, Landroid/support/v7/widget/ag$c;
invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/ag$c;-><init>(Landroid/support/v7/widget/ag;Landroid/support/v7/widget/ag$1;)V
iput-object v0, p0, Landroid/support/v7/widget/ag;->A:Landroid/support/v7/widget/ag$c;
new-instance v0, Landroid/graphics/Rect;
invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/ag;->D:Landroid/graphics/Rect;
iput-object p1, p0, Landroid/support/v7/widget/ag;->d:Landroid/content/Context;
new-instance v0, Landroid/os/Handler;
invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;
move-result-object v1
invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V
iput-object v0, p0, Landroid/support/v7/widget/ag;->C:Landroid/os/Handler;
sget-object v0, Landroid/support/v7/b/a$k;->ListPopupWindow:[I
invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
move-result-object v0
sget v1, Landroid/support/v7/b/a$k;->ListPopupWindow_android_dropDownHorizontalOffset:I
invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/ag;->j:I
sget v1, Landroid/support/v7/b/a$k;->ListPopupWindow_android_dropDownVerticalOffset:I
invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/ag;->k:I
iget v1, p0, Landroid/support/v7/widget/ag;->k:I
if-eqz v1, :cond_68
iput-boolean v3, p0, Landroid/support/v7/widget/ag;->m:Z
:cond_68
invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
new-instance v0, Landroid/support/v7/widget/r;
invoke-direct {v0, p1, p2, p3}, Landroid/support/v7/widget/r;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
iput-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->d:Landroid/content/Context;
invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
move-result-object v0
iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;
invoke-static {v0}, Landroid/support/v4/d/d;->a(Ljava/util/Locale;)I
move-result v0
iput v0, p0, Landroid/support/v7/widget/ag;->F:I
return-void
.end method
.method private a(Landroid/view/View;IZ)I
.registers 9
sget-object v0, Landroid/support/v7/widget/ag;->c:Ljava/lang/reflect/Method;
if-eqz v0, :cond_2f
:try_start_4
sget-object v0, Landroid/support/v7/widget/ag;->c:Ljava/lang/reflect/Method;
iget-object v1, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
const/4 v2, 0x3
new-array v2, v2, [Ljava/lang/Object;
const/4 v3, 0x0
aput-object p1, v2, v3
const/4 v3, 0x1
invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v4
aput-object v4, v2, v3
const/4 v3, 0x2
invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v4
aput-object v4, v2, v3
invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Integer;
invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
:try_end_25
.catch Ljava/lang/Exception; {:try_start_4 .. :try_end_25} :catch_27
move-result v0
:goto_26
return v0
:catch_27
move-exception v0
const-string v0, "ListPopupWindow"
const-string v1, "Could not call getMaxAvailableHeightMethod(View, int, boolean) on PopupWindow. Using the public version."
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
:cond_2f
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0, p1, p2}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;I)I
move-result v0
goto :goto_26
.end method
.method static synthetic a(Landroid/support/v7/widget/ag;)Landroid/support/v7/widget/ag$a;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
return-object v0
.end method
.method private a()V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/ag;->q:Landroid/view/View;
if-eqz v0, :cond_15
iget-object v0, p0, Landroid/support/v7/widget/ag;->q:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;
move-result-object v0
instance-of v1, v0, Landroid/view/ViewGroup;
if-eqz v1, :cond_15
check-cast v0, Landroid/view/ViewGroup;
iget-object v1, p0, Landroid/support/v7/widget/ag;->q:Landroid/view/View;
invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V
:cond_15
return-void
.end method
.method private b()I
.registers 11
const/high16 v9, 0x4000
const/high16 v4, -0x8000
const/4 v3, -0x1
const/4 v1, 0x1
const/4 v2, 0x0
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
if-nez v0, :cond_10d
iget-object v5, p0, Landroid/support/v7/widget/ag;->d:Landroid/content/Context;
new-instance v0, Landroid/support/v7/widget/ag$2;
invoke-direct {v0, p0}, Landroid/support/v7/widget/ag$2;-><init>(Landroid/support/v7/widget/ag;)V
iput-object v0, p0, Landroid/support/v7/widget/ag;->B:Ljava/lang/Runnable;
new-instance v6, Landroid/support/v7/widget/ag$a;
iget-boolean v0, p0, Landroid/support/v7/widget/ag;->E:Z
if-nez v0, :cond_f9
move v0, v1
:goto_1b
invoke-direct {v6, v5, v0}, Landroid/support/v7/widget/ag$a;-><init>(Landroid/content/Context;Z)V
iput-object v6, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
iget-object v0, p0, Landroid/support/v7/widget/ag;->u:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_2b
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
iget-object v6, p0, Landroid/support/v7/widget/ag;->u:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, v6}, Landroid/support/v7/widget/ag$a;->setSelector(Landroid/graphics/drawable/Drawable;)V
:cond_2b
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
iget-object v6, p0, Landroid/support/v7/widget/ag;->f:Landroid/widget/ListAdapter;
invoke-virtual {v0, v6}, Landroid/support/v7/widget/ag$a;->setAdapter(Landroid/widget/ListAdapter;)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
iget-object v6, p0, Landroid/support/v7/widget/ag;->v:Landroid/widget/AdapterView$OnItemClickListener;
invoke-virtual {v0, v6}, Landroid/support/v7/widget/ag$a;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ag$a;->setFocusable(Z)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ag$a;->setFocusableInTouchMode(Z)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
new-instance v6, Landroid/support/v7/widget/ag$3;
invoke-direct {v6, p0}, Landroid/support/v7/widget/ag$3;-><init>(Landroid/support/v7/widget/ag;)V
invoke-virtual {v0, v6}, Landroid/support/v7/widget/ag$a;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
iget-object v6, p0, Landroid/support/v7/widget/ag;->z:Landroid/support/v7/widget/ag$e;
invoke-virtual {v0, v6}, Landroid/support/v7/widget/ag$a;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->w:Landroid/widget/AdapterView$OnItemSelectedListener;
if-eqz v0, :cond_5f
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
iget-object v6, p0, Landroid/support/v7/widget/ag;->w:Landroid/widget/AdapterView$OnItemSelectedListener;
invoke-virtual {v0, v6}, Landroid/support/v7/widget/ag$a;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
:cond_5f
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
iget-object v7, p0, Landroid/support/v7/widget/ag;->q:Landroid/view/View;
if-eqz v7, :cond_189
new-instance v6, Landroid/widget/LinearLayout;
invoke-direct {v6, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V
invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V
new-instance v5, Landroid/widget/LinearLayout$LayoutParams;
const/high16 v8, 0x3f80
invoke-direct {v5, v3, v2, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V
iget v8, p0, Landroid/support/v7/widget/ag;->r:I
packed-switch v8, :pswitch_data_18e
const-string v0, "ListPopupWindow"
new-instance v5, Ljava/lang/StringBuilder;
invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
const-string v8, "Invalid hint position "
invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v5
iget v8, p0, Landroid/support/v7/widget/ag;->r:I
invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v5
invoke-static {v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
:goto_93
iget v0, p0, Landroid/support/v7/widget/ag;->i:I
if-ltz v0, :cond_10a
iget v0, p0, Landroid/support/v7/widget/ag;->i:I
move v5, v0
move v0, v4
:goto_9b
invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v0
invoke-virtual {v7, v0, v2}, Landroid/view/View;->measure(II)V
invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/widget/LinearLayout$LayoutParams;
invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I
move-result v5
iget v7, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I
add-int/2addr v5, v7
iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I
add-int/2addr v0, v5
move-object v5, v6
:goto_b3
iget-object v6, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v6, v5}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V
move v6, v0
:goto_b9
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;
move-result-object v0
if-eqz v0, :cond_12b
iget-object v5, p0, Landroid/support/v7/widget/ag;->D:Landroid/graphics/Rect;
invoke-virtual {v0, v5}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z
iget-object v0, p0, Landroid/support/v7/widget/ag;->D:Landroid/graphics/Rect;
iget v0, v0, Landroid/graphics/Rect;->top:I
iget-object v5, p0, Landroid/support/v7/widget/ag;->D:Landroid/graphics/Rect;
iget v5, v5, Landroid/graphics/Rect;->bottom:I
add-int/2addr v0, v5
iget-boolean v5, p0, Landroid/support/v7/widget/ag;->m:Z
if-nez v5, :cond_183
iget-object v5, p0, Landroid/support/v7/widget/ag;->D:Landroid/graphics/Rect;
iget v5, v5, Landroid/graphics/Rect;->top:I
neg-int v5, v5
iput v5, p0, Landroid/support/v7/widget/ag;->k:I
move v7, v0
:goto_db
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I
move-result v0
const/4 v5, 0x2
if-ne v0, v5, :cond_132
:goto_e4
invoke-virtual {p0}, Landroid/support/v7/widget/ag;->e()Landroid/view/View;
move-result-object v0
iget v5, p0, Landroid/support/v7/widget/ag;->k:I
invoke-direct {p0, v0, v5, v1}, Landroid/support/v7/widget/ag;->a(Landroid/view/View;IZ)I
move-result v5
iget-boolean v0, p0, Landroid/support/v7/widget/ag;->o:Z
if-nez v0, :cond_f6
iget v0, p0, Landroid/support/v7/widget/ag;->h:I
if-ne v0, v3, :cond_134
:cond_f6
add-int v0, v5, v7
:goto_f8
return v0
:cond_f9
move v0, v2
goto/16 :goto_1b
:pswitch_fc
invoke-virtual {v6, v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V
goto :goto_93
:pswitch_103
invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V
invoke-virtual {v6, v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
goto :goto_93
:cond_10a
move v0, v2
move v5, v2
goto :goto_9b
:cond_10d
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup;
iget-object v5, p0, Landroid/support/v7/widget/ag;->q:Landroid/view/View;
if-eqz v5, :cond_186
invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/widget/LinearLayout$LayoutParams;
invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I
move-result v5
iget v6, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I
add-int/2addr v5, v6
iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I
add-int/2addr v0, v5
move v6, v0
goto :goto_b9
:cond_12b
iget-object v0, p0, Landroid/support/v7/widget/ag;->D:Landroid/graphics/Rect;
invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V
move v7, v2
goto :goto_db
:cond_132
move v1, v2
goto :goto_e4
:cond_134
iget v0, p0, Landroid/support/v7/widget/ag;->i:I
packed-switch v0, :pswitch_data_196
iget v0, p0, Landroid/support/v7/widget/ag;->i:I
invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v1
:goto_13f
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
sub-int v4, v5, v6
move v5, v3
invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/ag$a;->a(IIIII)I
move-result v0
if-lez v0, :cond_14b
add-int/2addr v6, v7
:cond_14b
add-int/2addr v0, v6
goto :goto_f8
:pswitch_14d
iget-object v0, p0, Landroid/support/v7/widget/ag;->d:Landroid/content/Context;
invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
move-result-object v0
iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I
iget-object v1, p0, Landroid/support/v7/widget/ag;->D:Landroid/graphics/Rect;
iget v1, v1, Landroid/graphics/Rect;->left:I
iget-object v8, p0, Landroid/support/v7/widget/ag;->D:Landroid/graphics/Rect;
iget v8, v8, Landroid/graphics/Rect;->right:I
add-int/2addr v1, v8
sub-int/2addr v0, v1
invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v1
goto :goto_13f
:pswitch_168
iget-object v0, p0, Landroid/support/v7/widget/ag;->d:Landroid/content/Context;
invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
move-result-object v0
iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I
iget-object v1, p0, Landroid/support/v7/widget/ag;->D:Landroid/graphics/Rect;
iget v1, v1, Landroid/graphics/Rect;->left:I
iget-object v4, p0, Landroid/support/v7/widget/ag;->D:Landroid/graphics/Rect;
iget v4, v4, Landroid/graphics/Rect;->right:I
add-int/2addr v1, v4
sub-int/2addr v0, v1
invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v1
goto :goto_13f
:cond_183
move v7, v0
goto/16 :goto_db
:cond_186
move v6, v2
goto/16 :goto_b9
:cond_189
move-object v5, v0
move v0, v2
goto/16 :goto_b3
nop
:pswitch_data_196
.packed-switch -0x2
:pswitch_14d
:pswitch_168
.end packed-switch
:pswitch_data_18e
.packed-switch 0x0
:pswitch_103
:pswitch_fc
.end packed-switch
.end method
.method static synthetic b(Landroid/support/v7/widget/ag;)Landroid/widget/PopupWindow;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
return-object v0
.end method
.method private b(Z)V
.registers 7
sget-object v0, Landroid/support/v7/widget/ag;->a:Ljava/lang/reflect/Method;
if-eqz v0, :cond_15
:try_start_4
sget-object v0, Landroid/support/v7/widget/ag;->a:Ljava/lang/reflect/Method;
iget-object v1, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
const/4 v2, 0x1
new-array v2, v2, [Ljava/lang/Object;
const/4 v3, 0x0
invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v4
aput-object v4, v2, v3
invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
:try_end_15
.catch Ljava/lang/Exception; {:try_start_4 .. :try_end_15} :catch_16
:goto_15
:cond_15
return-void
:catch_16
move-exception v0
const-string v0, "ListPopupWindow"
const-string v1, "Could not call setClipToScreenEnabled() on PopupWindow. Oh well."
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_15
.end method
.method static synthetic c(Landroid/support/v7/widget/ag;)Landroid/support/v7/widget/ag$g;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ag;->x:Landroid/support/v7/widget/ag$g;
return-object v0
.end method
.method static synthetic d(Landroid/support/v7/widget/ag;)Landroid/os/Handler;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ag;->C:Landroid/os/Handler;
return-object v0
.end method
.method public a(I)V
.registers 2
iput p1, p0, Landroid/support/v7/widget/ag;->r:I
return-void
.end method
.method public a(Landroid/graphics/drawable/Drawable;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
return-void
.end method
.method public a(Landroid/view/View;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/widget/ag;->t:Landroid/view/View;
return-void
.end method
.method public a(Landroid/widget/AdapterView$OnItemClickListener;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/widget/ag;->v:Landroid/widget/AdapterView$OnItemClickListener;
return-void
.end method
.method public a(Landroid/widget/ListAdapter;)V
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/ag;->s:Landroid/database/DataSetObserver;
if-nez v0, :cond_23
new-instance v0, Landroid/support/v7/widget/ag$d;
const/4 v1, 0x0
invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/ag$d;-><init>(Landroid/support/v7/widget/ag;Landroid/support/v7/widget/ag$1;)V
iput-object v0, p0, Landroid/support/v7/widget/ag;->s:Landroid/database/DataSetObserver;
:cond_c
:goto_c
iput-object p1, p0, Landroid/support/v7/widget/ag;->f:Landroid/widget/ListAdapter;
iget-object v0, p0, Landroid/support/v7/widget/ag;->f:Landroid/widget/ListAdapter;
if-eqz v0, :cond_17
iget-object v0, p0, Landroid/support/v7/widget/ag;->s:Landroid/database/DataSetObserver;
invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V
:cond_17
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
if-eqz v0, :cond_22
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
iget-object v1, p0, Landroid/support/v7/widget/ag;->f:Landroid/widget/ListAdapter;
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ag$a;->setAdapter(Landroid/widget/ListAdapter;)V
:cond_22
return-void
:cond_23
iget-object v0, p0, Landroid/support/v7/widget/ag;->f:Landroid/widget/ListAdapter;
if-eqz v0, :cond_c
iget-object v0, p0, Landroid/support/v7/widget/ag;->f:Landroid/widget/ListAdapter;
iget-object v1, p0, Landroid/support/v7/widget/ag;->s:Landroid/database/DataSetObserver;
invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
goto :goto_c
.end method
.method public a(Landroid/widget/PopupWindow$OnDismissListener;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
return-void
.end method
.method public a(Z)V
.registers 3
iput-boolean p1, p0, Landroid/support/v7/widget/ag;->E:Z
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setFocusable(Z)V
return-void
.end method
.method public b(I)V
.registers 2
iput p1, p0, Landroid/support/v7/widget/ag;->j:I
return-void
.end method
.method public c()V
.registers 9
const/4 v3, 0x1
const/4 v7, -0x2
const/4 v1, 0x0
const/4 v5, -0x1
invoke-direct {p0}, Landroid/support/v7/widget/ag;->b()I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/widget/ag;->l()Z
move-result v6
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
iget v4, p0, Landroid/support/v7/widget/ag;->l:I
invoke-static {v0, v4}, Landroid/support/v4/widget/o;->a(Landroid/widget/PopupWindow;I)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z
move-result v0
if-eqz v0, :cond_8e
iget v0, p0, Landroid/support/v7/widget/ag;->i:I
if-ne v0, v5, :cond_59
move v4, v5
:goto_20
iget v0, p0, Landroid/support/v7/widget/ag;->h:I
if-ne v0, v5, :cond_82
if-eqz v6, :cond_6b
:goto_26
if-eqz v6, :cond_6f
iget-object v6, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
iget v0, p0, Landroid/support/v7/widget/ag;->i:I
if-ne v0, v5, :cond_6d
move v0, v5
:goto_2f
invoke-virtual {v6, v0}, Landroid/widget/PopupWindow;->setWidth(I)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V
move v6, v2
:goto_38
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
iget-boolean v2, p0, Landroid/support/v7/widget/ag;->p:Z
if-nez v2, :cond_43
iget-boolean v2, p0, Landroid/support/v7/widget/ag;->o:Z
if-nez v2, :cond_43
move v1, v3
:cond_43
invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {p0}, Landroid/support/v7/widget/ag;->e()Landroid/view/View;
move-result-object v1
iget v2, p0, Landroid/support/v7/widget/ag;->j:I
iget v3, p0, Landroid/support/v7/widget/ag;->k:I
if-gez v4, :cond_53
move v4, v5
:cond_53
if-gez v6, :cond_8c
:goto_55
invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V
:cond_58
:goto_58
return-void
:cond_59
iget v0, p0, Landroid/support/v7/widget/ag;->i:I
if-ne v0, v7, :cond_67
invoke-virtual {p0}, Landroid/support/v7/widget/ag;->e()Landroid/view/View;
move-result-object v0
invoke-virtual {v0}, Landroid/view/View;->getWidth()I
move-result v0
move v4, v0
goto :goto_20
:cond_67
iget v0, p0, Landroid/support/v7/widget/ag;->i:I
move v4, v0
goto :goto_20
:cond_6b
move v2, v5
goto :goto_26
:cond_6d
move v0, v1
goto :goto_2f
:cond_6f
iget-object v6, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
iget v0, p0, Landroid/support/v7/widget/ag;->i:I
if-ne v0, v5, :cond_80
move v0, v5
:goto_76
invoke-virtual {v6, v0}, Landroid/widget/PopupWindow;->setWidth(I)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setHeight(I)V
move v6, v2
goto :goto_38
:cond_80
move v0, v1
goto :goto_76
:cond_82
iget v0, p0, Landroid/support/v7/widget/ag;->h:I
if-ne v0, v7, :cond_88
move v6, v2
goto :goto_38
:cond_88
iget v0, p0, Landroid/support/v7/widget/ag;->h:I
move v6, v0
goto :goto_38
:cond_8c
move v5, v6
goto :goto_55
:cond_8e
iget v0, p0, Landroid/support/v7/widget/ag;->i:I
if-ne v0, v5, :cond_e9
move v0, v5
:goto_93
iget v4, p0, Landroid/support/v7/widget/ag;->h:I
if-ne v4, v5, :cond_f9
move v2, v5
:cond_98
:goto_98
iget-object v4, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v4, v0}, Landroid/widget/PopupWindow;->setWidth(I)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setHeight(I)V
invoke-direct {p0, v3}, Landroid/support/v7/widget/ag;->b(Z)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
iget-boolean v2, p0, Landroid/support/v7/widget/ag;->p:Z
if-nez v2, :cond_100
iget-boolean v2, p0, Landroid/support/v7/widget/ag;->o:Z
if-nez v2, :cond_100
:goto_af
invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
iget-object v1, p0, Landroid/support/v7/widget/ag;->y:Landroid/support/v7/widget/ag$f;
invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {p0}, Landroid/support/v7/widget/ag;->e()Landroid/view/View;
move-result-object v1
iget v2, p0, Landroid/support/v7/widget/ag;->j:I
iget v3, p0, Landroid/support/v7/widget/ag;->k:I
iget v4, p0, Landroid/support/v7/widget/ag;->n:I
invoke-static {v0, v1, v2, v3, v4}, Landroid/support/v4/widget/o;->a(Landroid/widget/PopupWindow;Landroid/view/View;III)V
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
invoke-virtual {v0, v5}, Landroid/support/v7/widget/ag$a;->setSelection(I)V
iget-boolean v0, p0, Landroid/support/v7/widget/ag;->E:Z
if-eqz v0, :cond_d9
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
invoke-virtual {v0}, Landroid/support/v7/widget/ag$a;->isInTouchMode()Z
move-result v0
if-eqz v0, :cond_dc
:cond_d9
invoke-virtual {p0}, Landroid/support/v7/widget/ag;->j()V
:cond_dc
iget-boolean v0, p0, Landroid/support/v7/widget/ag;->E:Z
if-nez v0, :cond_58
iget-object v0, p0, Landroid/support/v7/widget/ag;->C:Landroid/os/Handler;
iget-object v1, p0, Landroid/support/v7/widget/ag;->A:Landroid/support/v7/widget/ag$c;
invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
goto/16 :goto_58
:cond_e9
iget v0, p0, Landroid/support/v7/widget/ag;->i:I
if-ne v0, v7, :cond_f6
invoke-virtual {p0}, Landroid/support/v7/widget/ag;->e()Landroid/view/View;
move-result-object v0
invoke-virtual {v0}, Landroid/view/View;->getWidth()I
move-result v0
goto :goto_93
:cond_f6
iget v0, p0, Landroid/support/v7/widget/ag;->i:I
goto :goto_93
:cond_f9
iget v4, p0, Landroid/support/v7/widget/ag;->h:I
if-eq v4, v7, :cond_98
iget v2, p0, Landroid/support/v7/widget/ag;->h:I
goto :goto_98
:cond_100
move v3, v1
goto :goto_af
.end method
.method public c(I)V
.registers 3
iput p1, p0, Landroid/support/v7/widget/ag;->k:I
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v7/widget/ag;->m:Z
return-void
.end method
.method public d()Landroid/graphics/drawable/Drawable;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;
move-result-object v0
return-object v0
.end method
.method public d(I)V
.registers 2
iput p1, p0, Landroid/support/v7/widget/ag;->n:I
return-void
.end method
.method public e()Landroid/view/View;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ag;->t:Landroid/view/View;
return-object v0
.end method
.method public e(I)V
.registers 2
iput p1, p0, Landroid/support/v7/widget/ag;->i:I
return-void
.end method
.method public f()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/ag;->j:I
return v0
.end method
.method public f(I)V
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;
move-result-object v0
if-eqz v0, :cond_1a
iget-object v1, p0, Landroid/support/v7/widget/ag;->D:Landroid/graphics/Rect;
invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z
iget-object v0, p0, Landroid/support/v7/widget/ag;->D:Landroid/graphics/Rect;
iget v0, v0, Landroid/graphics/Rect;->left:I
iget-object v1, p0, Landroid/support/v7/widget/ag;->D:Landroid/graphics/Rect;
iget v1, v1, Landroid/graphics/Rect;->right:I
add-int/2addr v0, v1
add-int/2addr v0, p1
iput v0, p0, Landroid/support/v7/widget/ag;->i:I
:goto_19
return-void
:cond_1a
invoke-virtual {p0, p1}, Landroid/support/v7/widget/ag;->e(I)V
goto :goto_19
.end method
.method public g()I
.registers 2
iget-boolean v0, p0, Landroid/support/v7/widget/ag;->m:Z
if-nez v0, :cond_6
const/4 v0, 0x0
:goto_5
return v0
:cond_6
iget v0, p0, Landroid/support/v7/widget/ag;->k:I
goto :goto_5
.end method
.method public g(I)V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V
return-void
.end method
.method public h()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/ag;->i:I
return v0
.end method
.method public h(I)V
.registers 5
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
invoke-virtual {p0}, Landroid/support/v7/widget/ag;->k()Z
move-result v1
if-eqz v1, :cond_21
if-eqz v0, :cond_21
const/4 v1, 0x0
invoke-static {v0, v1}, Landroid/support/v7/widget/ag$a;->a(Landroid/support/v7/widget/ag$a;Z)Z
invoke-virtual {v0, p1}, Landroid/support/v7/widget/ag$a;->setSelection(I)V
sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v2, 0xb
if-lt v1, v2, :cond_21
invoke-virtual {v0}, Landroid/support/v7/widget/ag$a;->getChoiceMode()I
move-result v1
if-eqz v1, :cond_21
const/4 v1, 0x1
invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/ag$a;->setItemChecked(IZ)V
:cond_21
return-void
.end method
.method public i()V
.registers 3
const/4 v1, 0x0
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V
invoke-direct {p0}, Landroid/support/v7/widget/ag;->a()V
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V
iput-object v1, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
iget-object v0, p0, Landroid/support/v7/widget/ag;->C:Landroid/os/Handler;
iget-object v1, p0, Landroid/support/v7/widget/ag;->x:Landroid/support/v7/widget/ag$g;
invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
return-void
.end method
.method public j()V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
if-eqz v0, :cond_b
const/4 v1, 0x1
invoke-static {v0, v1}, Landroid/support/v7/widget/ag$a;->a(Landroid/support/v7/widget/ag$a;Z)Z
invoke-virtual {v0}, Landroid/support/v7/widget/ag$a;->requestLayout()V
:cond_b
return-void
.end method
.method public k()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z
move-result v0
return v0
.end method
.method public l()Z
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/ag;->e:Landroid/widget/PopupWindow;
invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I
move-result v0
const/4 v1, 0x2
if-ne v0, v1, :cond_b
const/4 v0, 0x1
:goto_a
return v0
:cond_b
const/4 v0, 0x0
goto :goto_a
.end method
.method public m()Landroid/widget/ListView;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ag;->g:Landroid/support/v7/widget/ag$a;
return-object v0
.end method