.class public Landroid/support/v7/widget/ah;
.super Landroid/widget/ListView;
.source "ListViewCompat.java"
.field private static final g:[I
.field final a:Landroid/graphics/Rect;
.field  b:I
.field  c:I
.field  d:I
.field  e:I
.field protected f:I
.field private h:Ljava/lang/reflect/Field;
.field private i:Landroid/support/v7/widget/ah$a;
.method static constructor <clinit>()V
.registers 2
const/4 v1, 0x0
const/4 v0, 0x1
new-array v0, v0, [I
aput v1, v0, v1
sput-object v0, Landroid/support/v7/widget/ah;->g:[I
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.registers 6
const/4 v1, 0x0
invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
new-instance v0, Landroid/graphics/Rect;
invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/ah;->a:Landroid/graphics/Rect;
iput v1, p0, Landroid/support/v7/widget/ah;->b:I
iput v1, p0, Landroid/support/v7/widget/ah;->c:I
iput v1, p0, Landroid/support/v7/widget/ah;->d:I
iput v1, p0, Landroid/support/v7/widget/ah;->e:I
:try_start_13
const-class v0, Landroid/widget/AbsListView;
const-string v1, "mIsChildViewEnabled"
invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/widget/ah;->h:Ljava/lang/reflect/Field;
iget-object v0, p0, Landroid/support/v7/widget/ah;->h:Ljava/lang/reflect/Field;
const/4 v1, 0x1
invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
:goto_23
:try_end_23
.catch Ljava/lang/NoSuchFieldException; {:try_start_13 .. :try_end_23} :catch_24
return-void
:catch_24
move-exception v0
invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V
goto :goto_23
.end method
.method public a(IIIII)I
.registers 18
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->getListPaddingTop()I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->getListPaddingBottom()I
move-result v3
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->getListPaddingLeft()I
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->getListPaddingRight()I
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->getDividerHeight()I
move-result v1
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->getDivider()Landroid/graphics/drawable/Drawable;
move-result-object v4
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->getAdapter()Landroid/widget/ListAdapter;
move-result-object v8
if-nez v8, :cond_1f
add-int p4, v2, v3
:goto_1e
:cond_1e
return p4
:cond_1f
add-int/2addr v3, v2
if-lez v1, :cond_78
if-eqz v4, :cond_78
:goto_24
const/4 v4, 0x0
const/4 v6, 0x0
const/4 v5, 0x0
invoke-interface {v8}, Landroid/widget/ListAdapter;->getCount()I
move-result v9
const/4 v2, 0x0
move v7, v2
:goto_2d
if-ge v7, v9, :cond_8d
invoke-interface {v8, v7}, Landroid/widget/ListAdapter;->getItemViewType(I)I
move-result v2
if-eq v2, v5, :cond_90
const/4 v5, 0x0
move v11, v2
move-object v2, v5
move v5, v11
:goto_39
invoke-interface {v8, v7, v2, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
move-result-object v6
invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v2
if-nez v2, :cond_4a
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v2
invoke-virtual {v6, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
:cond_4a
iget v10, v2, Landroid/view/ViewGroup$LayoutParams;->height:I
if-lez v10, :cond_7a
iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I
const/high16 v10, 0x4000
invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v2
:goto_56
invoke-virtual {v6, p1, v2}, Landroid/view/View;->measure(II)V
invoke-virtual {v6}, Landroid/view/View;->forceLayout()V
if-lez v7, :cond_92
add-int v2, v3, v1
:goto_60
invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I
move-result v3
add-int/2addr v2, v3
move/from16 v0, p4
if-lt v2, v0, :cond_81
if-ltz p5, :cond_1e
move/from16 v0, p5
if-le v7, v0, :cond_1e
if-lez v4, :cond_1e
move/from16 v0, p4
if-eq v2, v0, :cond_1e
move/from16 p4, v4
goto :goto_1e
:cond_78
const/4 v1, 0x0
goto :goto_24
:cond_7a
const/4 v2, 0x0
const/4 v10, 0x0
invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v2
goto :goto_56
:cond_81
if-ltz p5, :cond_88
move/from16 v0, p5
if-lt v7, v0, :cond_88
move v4, v2
:cond_88
add-int/lit8 v3, v7, 0x1
move v7, v3
move v3, v2
goto :goto_2d
:cond_8d
move/from16 p4, v3
goto :goto_1e
:cond_90
move-object v2, v6
goto :goto_39
:cond_92
move v2, v3
goto :goto_60
.end method
.method protected a(ILandroid/view/View;)V
.registers 9
const/4 v0, 0x1
const/4 v1, 0x0
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->getSelector()Landroid/graphics/drawable/Drawable;
move-result-object v3
if-eqz v3, :cond_2d
const/4 v2, -0x1
if-eq p1, v2, :cond_2d
move v2, v0
:goto_c
if-eqz v2, :cond_11
invoke-virtual {v3, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z
:cond_11
invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/ah;->b(ILandroid/view/View;)V
if-eqz v2, :cond_2c
iget-object v2, p0, Landroid/support/v7/widget/ah;->a:Landroid/graphics/Rect;
invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F
move-result v4
invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterY()F
move-result v2
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->getVisibility()I
move-result v5
if-nez v5, :cond_2f
:goto_26
invoke-virtual {v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z
invoke-static {v3, v4, v2}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;FF)V
:cond_2c
return-void
:cond_2d
move v2, v1
goto :goto_c
:cond_2f
move v0, v1
goto :goto_26
.end method
.method protected a(ILandroid/view/View;FF)V
.registers 7
invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/ah;->a(ILandroid/view/View;)V
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->getSelector()Landroid/graphics/drawable/Drawable;
move-result-object v0
if-eqz v0, :cond_f
const/4 v1, -0x1
if-eq p1, v1, :cond_f
invoke-static {v0, p3, p4}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;FF)V
:cond_f
return-void
.end method
.method protected a(Landroid/graphics/Canvas;)V
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/ah;->a:Landroid/graphics/Rect;
invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z
move-result v0
if-nez v0, :cond_16
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->getSelector()Landroid/graphics/drawable/Drawable;
move-result-object v0
if-eqz v0, :cond_16
iget-object v1, p0, Landroid/support/v7/widget/ah;->a:Landroid/graphics/Rect;
invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V
invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
:cond_16
return-void
.end method
.method protected a()Z
.registers 2
const/4 v0, 0x0
return v0
.end method
.method protected b()V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->getSelector()Landroid/graphics/drawable/Drawable;
move-result-object v0
if-eqz v0, :cond_13
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->c()Z
move-result v1
if-eqz v1, :cond_13
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->getDrawableState()[I
move-result-object v1
invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z
:cond_13
return-void
.end method
.method protected b(ILandroid/view/View;)V
.registers 8
iget-object v0, p0, Landroid/support/v7/widget/ah;->a:Landroid/graphics/Rect;
invoke-virtual {p2}, Landroid/view/View;->getLeft()I
move-result v1
invoke-virtual {p2}, Landroid/view/View;->getTop()I
move-result v2
invoke-virtual {p2}, Landroid/view/View;->getRight()I
move-result v3
invoke-virtual {p2}, Landroid/view/View;->getBottom()I
move-result v4
invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V
iget v1, v0, Landroid/graphics/Rect;->left:I
iget v2, p0, Landroid/support/v7/widget/ah;->b:I
sub-int/2addr v1, v2
iput v1, v0, Landroid/graphics/Rect;->left:I
iget v1, v0, Landroid/graphics/Rect;->top:I
iget v2, p0, Landroid/support/v7/widget/ah;->c:I
sub-int/2addr v1, v2
iput v1, v0, Landroid/graphics/Rect;->top:I
iget v1, v0, Landroid/graphics/Rect;->right:I
iget v2, p0, Landroid/support/v7/widget/ah;->d:I
add-int/2addr v1, v2
iput v1, v0, Landroid/graphics/Rect;->right:I
iget v1, v0, Landroid/graphics/Rect;->bottom:I
iget v2, p0, Landroid/support/v7/widget/ah;->e:I
add-int/2addr v1, v2
iput v1, v0, Landroid/graphics/Rect;->bottom:I
:try_start_31
iget-object v0, p0, Landroid/support/v7/widget/ah;->h:Ljava/lang/reflect/Field;
invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z
move-result v0
invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z
move-result v1
if-eq v1, v0, :cond_4f
iget-object v1, p0, Landroid/support/v7/widget/ah;->h:Ljava/lang/reflect/Field;
if-nez v0, :cond_50
const/4 v0, 0x1
:goto_42
invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v0
invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
const/4 v0, -0x1
if-eq p1, v0, :cond_4f
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->refreshDrawableState()V
:try_end_4f
.catch Ljava/lang/IllegalAccessException; {:try_start_31 .. :try_end_4f} :catch_52
:goto_4f
:cond_4f
return-void
:cond_50
const/4 v0, 0x0
goto :goto_42
:catch_52
move-exception v0
invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V
goto :goto_4f
.end method
.method protected c()Z
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->a()Z
move-result v0
if-eqz v0, :cond_e
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->isPressed()Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
.registers 2
invoke-virtual {p0, p1}, Landroid/support/v7/widget/ah;->a(Landroid/graphics/Canvas;)V
invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V
return-void
.end method
.method protected drawableStateChanged()V
.registers 2
invoke-super {p0}, Landroid/widget/ListView;->drawableStateChanged()V
const/4 v0, 0x1
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ah;->setSelectorEnabled(Z)V
invoke-virtual {p0}, Landroid/support/v7/widget/ah;->b()V
return-void
.end method
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
.registers 4
invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
move-result v0
packed-switch v0, :pswitch_data_1e
:goto_7
invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z
move-result v0
return v0
:pswitch_c
invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F
move-result v0
float-to-int v0, v0
invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F
move-result v1
float-to-int v1, v1
invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/ah;->pointToPosition(II)I
move-result v0
iput v0, p0, Landroid/support/v7/widget/ah;->f:I
goto :goto_7
nop
:pswitch_data_1e
.packed-switch 0x0
:pswitch_c
.end packed-switch
.end method
.method public setSelector(Landroid/graphics/drawable/Drawable;)V
.registers 4
if-eqz p1, :cond_29
new-instance v0, Landroid/support/v7/widget/ah$a;
invoke-direct {v0, p1}, Landroid/support/v7/widget/ah$a;-><init>(Landroid/graphics/drawable/Drawable;)V
:goto_7
iput-object v0, p0, Landroid/support/v7/widget/ah;->i:Landroid/support/v7/widget/ah$a;
iget-object v0, p0, Landroid/support/v7/widget/ah;->i:Landroid/support/v7/widget/ah$a;
invoke-super {p0, v0}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V
new-instance v0, Landroid/graphics/Rect;
invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V
if-eqz p1, :cond_18
invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z
:cond_18
iget v1, v0, Landroid/graphics/Rect;->left:I
iput v1, p0, Landroid/support/v7/widget/ah;->b:I
iget v1, v0, Landroid/graphics/Rect;->top:I
iput v1, p0, Landroid/support/v7/widget/ah;->c:I
iget v1, v0, Landroid/graphics/Rect;->right:I
iput v1, p0, Landroid/support/v7/widget/ah;->d:I
iget v0, v0, Landroid/graphics/Rect;->bottom:I
iput v0, p0, Landroid/support/v7/widget/ah;->e:I
return-void
:cond_29
const/4 v0, 0x0
goto :goto_7
.end method
.method protected setSelectorEnabled(Z)V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/ah;->i:Landroid/support/v7/widget/ah$a;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/widget/ah;->i:Landroid/support/v7/widget/ah$a;
invoke-virtual {v0, p1}, Landroid/support/v7/widget/ah$a;->a(Z)V
:cond_9
return-void
.end method