.class  Landroid/support/v7/widget/ag$e;
.super Ljava/lang/Object;
.source "ListPopupWindow.java"
.implements Landroid/widget/AbsListView$OnScrollListener;
.field final synthetic a:Landroid/support/v7/widget/ag;
.method private constructor <init>(Landroid/support/v7/widget/ag;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/widget/ag$e;->a:Landroid/support/v7/widget/ag;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method synthetic constructor <init>(Landroid/support/v7/widget/ag;Landroid/support/v7/widget/ag$1;)V
.registers 3
invoke-direct {p0, p1}, Landroid/support/v7/widget/ag$e;-><init>(Landroid/support/v7/widget/ag;)V
return-void
.end method
.method public onScroll(Landroid/widget/AbsListView;III)V
.registers 5
return-void
.end method
.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
.registers 5
const/4 v0, 0x1
if-ne p2, v0, :cond_2f
iget-object v0, p0, Landroid/support/v7/widget/ag$e;->a:Landroid/support/v7/widget/ag;
invoke-virtual {v0}, Landroid/support/v7/widget/ag;->l()Z
move-result v0
if-nez v0, :cond_2f
iget-object v0, p0, Landroid/support/v7/widget/ag$e;->a:Landroid/support/v7/widget/ag;
invoke-static {v0}, Landroid/support/v7/widget/ag;->b(Landroid/support/v7/widget/ag;)Landroid/widget/PopupWindow;
move-result-object v0
invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;
move-result-object v0
if-eqz v0, :cond_2f
iget-object v0, p0, Landroid/support/v7/widget/ag$e;->a:Landroid/support/v7/widget/ag;
invoke-static {v0}, Landroid/support/v7/widget/ag;->d(Landroid/support/v7/widget/ag;)Landroid/os/Handler;
move-result-object v0
iget-object v1, p0, Landroid/support/v7/widget/ag$e;->a:Landroid/support/v7/widget/ag;
invoke-static {v1}, Landroid/support/v7/widget/ag;->c(Landroid/support/v7/widget/ag;)Landroid/support/v7/widget/ag$g;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
iget-object v0, p0, Landroid/support/v7/widget/ag$e;->a:Landroid/support/v7/widget/ag;
invoke-static {v0}, Landroid/support/v7/widget/ag;->c(Landroid/support/v7/widget/ag;)Landroid/support/v7/widget/ag$g;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v7/widget/ag$g;->run()V
:cond_2f
return-void
.end method