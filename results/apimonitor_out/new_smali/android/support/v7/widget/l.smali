.class public final Landroid/support/v7/widget/l;
.super Ljava/lang/Object;
.source "AppCompatDrawableManager.java"
.field private static final a:Landroid/graphics/PorterDuff$Mode;
.field private static b:Landroid/support/v7/widget/l;
.field private static final c:Landroid/support/v7/widget/l$b;
.field private static final d:[I
.field private static final e:[I
.field private static final f:[I
.field private static final g:[I
.field private static final h:[I
.field private static final i:[I
.field private j:Ljava/util/WeakHashMap;
.field private k:Landroid/support/v4/e/a;
.field private l:Landroid/util/SparseArray;
.field private final m:Ljava/lang/Object;
.field private final n:Ljava/util/WeakHashMap;
.field private o:Landroid/util/TypedValue;
.method static constructor <clinit>()V
.registers 8
const/4 v7, 0x4
const/4 v6, 0x3
const/4 v5, 0x2
const/4 v4, 0x1
const/4 v3, 0x0
sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;
sput-object v0, Landroid/support/v7/widget/l;->a:Landroid/graphics/PorterDuff$Mode;
new-instance v0, Landroid/support/v7/widget/l$b;
const/4 v1, 0x6
invoke-direct {v0, v1}, Landroid/support/v7/widget/l$b;-><init>(I)V
sput-object v0, Landroid/support/v7/widget/l;->c:Landroid/support/v7/widget/l$b;
new-array v0, v6, [I
sget v1, Landroid/support/v7/b/a$e;->abc_textfield_search_default_mtrl_alpha:I
aput v1, v0, v3
sget v1, Landroid/support/v7/b/a$e;->abc_textfield_default_mtrl_alpha:I
aput v1, v0, v4
sget v1, Landroid/support/v7/b/a$e;->abc_ab_share_pack_mtrl_alpha:I
aput v1, v0, v5
sput-object v0, Landroid/support/v7/widget/l;->d:[I
const/16 v0, 0xc
new-array v0, v0, [I
sget v1, Landroid/support/v7/b/a$e;->abc_ic_ab_back_mtrl_am_alpha:I
aput v1, v0, v3
sget v1, Landroid/support/v7/b/a$e;->abc_ic_go_search_api_mtrl_alpha:I
aput v1, v0, v4
sget v1, Landroid/support/v7/b/a$e;->abc_ic_search_api_mtrl_alpha:I
aput v1, v0, v5
sget v1, Landroid/support/v7/b/a$e;->abc_ic_commit_search_api_mtrl_alpha:I
aput v1, v0, v6
sget v1, Landroid/support/v7/b/a$e;->abc_ic_clear_mtrl_alpha:I
aput v1, v0, v7
const/4 v1, 0x5
sget v2, Landroid/support/v7/b/a$e;->abc_ic_menu_share_mtrl_alpha:I
aput v2, v0, v1
const/4 v1, 0x6
sget v2, Landroid/support/v7/b/a$e;->abc_ic_menu_copy_mtrl_am_alpha:I
aput v2, v0, v1
const/4 v1, 0x7
sget v2, Landroid/support/v7/b/a$e;->abc_ic_menu_cut_mtrl_alpha:I
aput v2, v0, v1
const/16 v1, 0x8
sget v2, Landroid/support/v7/b/a$e;->abc_ic_menu_selectall_mtrl_alpha:I
aput v2, v0, v1
const/16 v1, 0x9
sget v2, Landroid/support/v7/b/a$e;->abc_ic_menu_paste_mtrl_am_alpha:I
aput v2, v0, v1
const/16 v1, 0xa
sget v2, Landroid/support/v7/b/a$e;->abc_ic_menu_moreoverflow_mtrl_alpha:I
aput v2, v0, v1
const/16 v1, 0xb
sget v2, Landroid/support/v7/b/a$e;->abc_ic_voice_search_api_mtrl_alpha:I
aput v2, v0, v1
sput-object v0, Landroid/support/v7/widget/l;->e:[I
new-array v0, v7, [I
sget v1, Landroid/support/v7/b/a$e;->abc_textfield_activated_mtrl_alpha:I
aput v1, v0, v3
sget v1, Landroid/support/v7/b/a$e;->abc_textfield_search_activated_mtrl_alpha:I
aput v1, v0, v4
sget v1, Landroid/support/v7/b/a$e;->abc_cab_background_top_mtrl_alpha:I
aput v1, v0, v5
sget v1, Landroid/support/v7/b/a$e;->abc_text_cursor_material:I
aput v1, v0, v6
sput-object v0, Landroid/support/v7/widget/l;->f:[I
new-array v0, v6, [I
sget v1, Landroid/support/v7/b/a$e;->abc_popup_background_mtrl_mult:I
aput v1, v0, v3
sget v1, Landroid/support/v7/b/a$e;->abc_cab_background_internal_bg:I
aput v1, v0, v4
sget v1, Landroid/support/v7/b/a$e;->abc_menu_hardkey_panel_mtrl_mult:I
aput v1, v0, v5
sput-object v0, Landroid/support/v7/widget/l;->g:[I
const/16 v0, 0xa
new-array v0, v0, [I
sget v1, Landroid/support/v7/b/a$e;->abc_edit_text_material:I
aput v1, v0, v3
sget v1, Landroid/support/v7/b/a$e;->abc_tab_indicator_material:I
aput v1, v0, v4
sget v1, Landroid/support/v7/b/a$e;->abc_textfield_search_material:I
aput v1, v0, v5
sget v1, Landroid/support/v7/b/a$e;->abc_spinner_mtrl_am_alpha:I
aput v1, v0, v6
sget v1, Landroid/support/v7/b/a$e;->abc_spinner_textfield_background_material:I
aput v1, v0, v7
const/4 v1, 0x5
sget v2, Landroid/support/v7/b/a$e;->abc_ratingbar_full_material:I
aput v2, v0, v1
const/4 v1, 0x6
sget v2, Landroid/support/v7/b/a$e;->abc_switch_track_mtrl_alpha:I
aput v2, v0, v1
const/4 v1, 0x7
sget v2, Landroid/support/v7/b/a$e;->abc_switch_thumb_material:I
aput v2, v0, v1
const/16 v1, 0x8
sget v2, Landroid/support/v7/b/a$e;->abc_btn_default_mtrl_shape:I
aput v2, v0, v1
const/16 v1, 0x9
sget v2, Landroid/support/v7/b/a$e;->abc_btn_borderless_material:I
aput v2, v0, v1
sput-object v0, Landroid/support/v7/widget/l;->h:[I
new-array v0, v5, [I
sget v1, Landroid/support/v7/b/a$e;->abc_btn_check_material:I
aput v1, v0, v3
sget v1, Landroid/support/v7/b/a$e;->abc_btn_radio_material:I
aput v1, v0, v4
sput-object v0, Landroid/support/v7/widget/l;->i:[I
return-void
.end method
.method public constructor <init>()V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
new-instance v0, Ljava/lang/Object;
invoke-direct {v0}, Ljava/lang/Object;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/l;->m:Ljava/lang/Object;
new-instance v0, Ljava/util/WeakHashMap;
const/4 v1, 0x0
invoke-direct {v0, v1}, Ljava/util/WeakHashMap;-><init>(I)V
iput-object v0, p0, Landroid/support/v7/widget/l;->n:Ljava/util/WeakHashMap;
return-void
.end method
.method private static a(Landroid/util/TypedValue;)J
.registers 5
iget v0, p0, Landroid/util/TypedValue;->assetCookie:I
int-to-long v0, v0
const/16 v2, 0x20
shl-long/2addr v0, v2
iget v2, p0, Landroid/util/TypedValue;->data:I
int-to-long v2, v2
or-long/2addr v0, v2
return-wide v0
.end method
.method private a(Landroid/content/Context;)Landroid/content/res/ColorStateList;
.registers 8
const/4 v3, 0x7
sget v0, Landroid/support/v7/b/a$a;->colorControlNormal:I
invoke-static {p1, v0}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v0
sget v1, Landroid/support/v7/b/a$a;->colorControlActivated:I
invoke-static {p1, v1}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v1
new-array v2, v3, [[I
new-array v3, v3, [I
const/4 v4, 0x0
sget-object v5, Landroid/support/v7/widget/am;->a:[I
aput-object v5, v2, v4
sget v5, Landroid/support/v7/b/a$a;->colorControlNormal:I
invoke-static {p1, v5}, Landroid/support/v7/widget/am;->c(Landroid/content/Context;I)I
move-result v5
aput v5, v3, v4
const/4 v4, 0x1
sget-object v5, Landroid/support/v7/widget/am;->b:[I
aput-object v5, v2, v4
aput v1, v3, v4
const/4 v4, 0x2
sget-object v5, Landroid/support/v7/widget/am;->c:[I
aput-object v5, v2, v4
aput v1, v3, v4
const/4 v4, 0x3
sget-object v5, Landroid/support/v7/widget/am;->d:[I
aput-object v5, v2, v4
aput v1, v3, v4
const/4 v4, 0x4
sget-object v5, Landroid/support/v7/widget/am;->e:[I
aput-object v5, v2, v4
aput v1, v3, v4
const/4 v4, 0x5
sget-object v5, Landroid/support/v7/widget/am;->f:[I
aput-object v5, v2, v4
aput v1, v3, v4
const/4 v1, 0x6
sget-object v4, Landroid/support/v7/widget/am;->h:[I
aput-object v4, v2, v1
aput v0, v3, v1
new-instance v0, Landroid/content/res/ColorStateList;
invoke-direct {v0, v2, v3}, Landroid/content/res/ColorStateList;-><init>([[I[I)V
return-object v0
.end method
.method public static a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
.registers 4
sget-object v0, Landroid/support/v7/widget/l;->c:Landroid/support/v7/widget/l$b;
invoke-virtual {v0, p0, p1}, Landroid/support/v7/widget/l$b;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
move-result-object v0
if-nez v0, :cond_12
new-instance v0, Landroid/graphics/PorterDuffColorFilter;
invoke-direct {v0, p0, p1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V
sget-object v1, Landroid/support/v7/widget/l;->c:Landroid/support/v7/widget/l$b;
invoke-virtual {v1, p0, p1, v0}, Landroid/support/v7/widget/l$b;->a(ILandroid/graphics/PorterDuff$Mode;Landroid/graphics/PorterDuffColorFilter;)Landroid/graphics/PorterDuffColorFilter;
:cond_12
return-object v0
.end method
.method private static a(Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;[I)Landroid/graphics/PorterDuffColorFilter;
.registers 4
if-eqz p0, :cond_4
if-nez p1, :cond_6
:cond_4
const/4 v0, 0x0
:goto_5
return-object v0
:cond_6
const/4 v0, 0x0
invoke-virtual {p0, p2, v0}, Landroid/content/res/ColorStateList;->getColorForState([II)I
move-result v0
invoke-static {v0, p1}, Landroid/support/v7/widget/l;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
move-result-object v0
goto :goto_5
.end method
.method private a(Landroid/content/Context;IZLandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
.registers 11
const v5, 0x102000f
const v4, 0x102000d
const/high16 v1, 0x102
invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/l;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
move-result-object v0
if-eqz v0, :cond_29
invoke-static {p4}, Landroid/support/v7/widget/ad;->b(Landroid/graphics/drawable/Drawable;)Z
move-result v1
if-eqz v1, :cond_18
invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;
move-result-object p4
:cond_18
invoke-static {p4}, Landroid/support/v4/b/a/a;->f(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
move-result-object p4
invoke-static {p4, v0}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
invoke-virtual {p0, p2}, Landroid/support/v7/widget/l;->a(I)Landroid/graphics/PorterDuff$Mode;
move-result-object v0
if-eqz v0, :cond_28
invoke-static {p4, v0}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
:cond_28
:goto_28
return-object p4
:cond_29
sget v0, Landroid/support/v7/b/a$e;->abc_seekbar_track_material:I
if-ne p2, v0, :cond_5e
move-object v0, p4
check-cast v0, Landroid/graphics/drawable/LayerDrawable;
invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;
move-result-object v1
sget v2, Landroid/support/v7/b/a$a;->colorControlNormal:I
invoke-static {p1, v2}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v2
sget-object v3, Landroid/support/v7/widget/l;->a:Landroid/graphics/PorterDuff$Mode;
invoke-static {v1, v2, v3}, Landroid/support/v7/widget/l;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V
invoke-virtual {v0, v5}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;
move-result-object v1
sget v2, Landroid/support/v7/b/a$a;->colorControlNormal:I
invoke-static {p1, v2}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v2
sget-object v3, Landroid/support/v7/widget/l;->a:Landroid/graphics/PorterDuff$Mode;
invoke-static {v1, v2, v3}, Landroid/support/v7/widget/l;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V
invoke-virtual {v0, v4}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;
move-result-object v0
sget v1, Landroid/support/v7/b/a$a;->colorControlActivated:I
invoke-static {p1, v1}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v1
sget-object v2, Landroid/support/v7/widget/l;->a:Landroid/graphics/PorterDuff$Mode;
invoke-static {v0, v1, v2}, Landroid/support/v7/widget/l;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V
goto :goto_28
:cond_5e
sget v0, Landroid/support/v7/b/a$e;->abc_ratingbar_indicator_material:I
if-eq p2, v0, :cond_66
sget v0, Landroid/support/v7/b/a$e;->abc_ratingbar_small_material:I
if-ne p2, v0, :cond_97
:cond_66
move-object v0, p4
check-cast v0, Landroid/graphics/drawable/LayerDrawable;
invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;
move-result-object v1
sget v2, Landroid/support/v7/b/a$a;->colorControlNormal:I
invoke-static {p1, v2}, Landroid/support/v7/widget/am;->c(Landroid/content/Context;I)I
move-result v2
sget-object v3, Landroid/support/v7/widget/l;->a:Landroid/graphics/PorterDuff$Mode;
invoke-static {v1, v2, v3}, Landroid/support/v7/widget/l;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V
invoke-virtual {v0, v5}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;
move-result-object v1
sget v2, Landroid/support/v7/b/a$a;->colorControlActivated:I
invoke-static {p1, v2}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v2
sget-object v3, Landroid/support/v7/widget/l;->a:Landroid/graphics/PorterDuff$Mode;
invoke-static {v1, v2, v3}, Landroid/support/v7/widget/l;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V
invoke-virtual {v0, v4}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;
move-result-object v0
sget v1, Landroid/support/v7/b/a$a;->colorControlActivated:I
invoke-static {p1, v1}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v1
sget-object v2, Landroid/support/v7/widget/l;->a:Landroid/graphics/PorterDuff$Mode;
invoke-static {v0, v1, v2}, Landroid/support/v7/widget/l;->a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V
goto :goto_28
:cond_97
invoke-static {p1, p2, p4}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;)Z
move-result v0
if-nez v0, :cond_28
if-eqz p3, :cond_28
const/4 p4, 0x0
goto :goto_28
.end method
.method private a(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;
.registers 8
const/4 v2, 0x0
iget-object v3, p0, Landroid/support/v7/widget/l;->m:Ljava/lang/Object;
monitor-enter v3
:try_start_4
iget-object v0, p0, Landroid/support/v7/widget/l;->n:Ljava/util/WeakHashMap;
invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/e/e;
if-nez v0, :cond_11
monitor-exit v3
move-object v0, v2
:goto_10
return-object v0
:cond_11
invoke-virtual {v0, p2, p3}, Landroid/support/v4/e/e;->a(J)Ljava/lang/Object;
move-result-object v1
check-cast v1, Ljava/lang/ref/WeakReference;
if-eqz v1, :cond_31
invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v1
check-cast v1, Landroid/graphics/drawable/Drawable$ConstantState;
if-eqz v1, :cond_2e
invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
move-result-object v0
monitor-exit v3
goto :goto_10
:catchall_2b
move-exception v0
monitor-exit v3
:try_end_2d
.catchall {:try_start_4 .. :try_end_2d} :catchall_2b
throw v0
:try_start_2e
:cond_2e
invoke-virtual {v0, p2, p3}, Landroid/support/v4/e/e;->b(J)V
:cond_31
monitor-exit v3
:try_end_32
.catchall {:try_start_2e .. :try_end_32} :catchall_2b
move-object v0, v2
goto :goto_10
.end method
.method public static a()Landroid/support/v7/widget/l;
.registers 1
sget-object v0, Landroid/support/v7/widget/l;->b:Landroid/support/v7/widget/l;
if-nez v0, :cond_10
new-instance v0, Landroid/support/v7/widget/l;
invoke-direct {v0}, Landroid/support/v7/widget/l;-><init>()V
sput-object v0, Landroid/support/v7/widget/l;->b:Landroid/support/v7/widget/l;
sget-object v0, Landroid/support/v7/widget/l;->b:Landroid/support/v7/widget/l;
invoke-static {v0}, Landroid/support/v7/widget/l;->a(Landroid/support/v7/widget/l;)V
:cond_10
sget-object v0, Landroid/support/v7/widget/l;->b:Landroid/support/v7/widget/l;
return-object v0
.end method
.method private a(Landroid/content/Context;ILandroid/content/res/ColorStateList;)V
.registers 6
iget-object v0, p0, Landroid/support/v7/widget/l;->j:Ljava/util/WeakHashMap;
if-nez v0, :cond_b
new-instance v0, Ljava/util/WeakHashMap;
invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/l;->j:Ljava/util/WeakHashMap;
:cond_b
iget-object v0, p0, Landroid/support/v7/widget/l;->j:Ljava/util/WeakHashMap;
invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/util/SparseArray;
if-nez v0, :cond_1f
new-instance v0, Landroid/util/SparseArray;
invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V
iget-object v1, p0, Landroid/support/v7/widget/l;->j:Ljava/util/WeakHashMap;
invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_1f
invoke-virtual {v0, p2, p3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
return-void
.end method
.method private static a(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V
.registers 4
invoke-static {p0}, Landroid/support/v7/widget/ad;->b(Landroid/graphics/drawable/Drawable;)Z
move-result v0
if-eqz v0, :cond_a
invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;
move-result-object p0
:cond_a
if-nez p2, :cond_e
sget-object p2, Landroid/support/v7/widget/l;->a:Landroid/graphics/PorterDuff$Mode;
:cond_e
invoke-static {p1, p2}, Landroid/support/v7/widget/l;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V
return-void
.end method
.method public static a(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/ap;[I)V
.registers 5
invoke-static {p0}, Landroid/support/v7/widget/ad;->b(Landroid/graphics/drawable/Drawable;)Z
move-result v0
if-eqz v0, :cond_14
invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;
move-result-object v0
if-eq v0, p0, :cond_14
const-string v0, "AppCompatDrawableManager"
const-string v1, "Mutated drawable is not the same instance as the input."
invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
:cond_13
:goto_13
return-void
:cond_14
iget-boolean v0, p1, Landroid/support/v7/widget/ap;->d:Z
if-nez v0, :cond_1c
iget-boolean v0, p1, Landroid/support/v7/widget/ap;->c:Z
if-eqz v0, :cond_3e
:cond_1c
iget-boolean v0, p1, Landroid/support/v7/widget/ap;->d:Z
if-eqz v0, :cond_39
iget-object v0, p1, Landroid/support/v7/widget/ap;->a:Landroid/content/res/ColorStateList;
:goto_22
iget-boolean v1, p1, Landroid/support/v7/widget/ap;->c:Z
if-eqz v1, :cond_3b
iget-object v1, p1, Landroid/support/v7/widget/ap;->b:Landroid/graphics/PorterDuff$Mode;
:goto_28
invoke-static {v0, v1, p2}, Landroid/support/v7/widget/l;->a(Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;[I)Landroid/graphics/PorterDuffColorFilter;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V
:goto_2f
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x17
if-gt v0, v1, :cond_13
invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V
goto :goto_13
:cond_39
const/4 v0, 0x0
goto :goto_22
:cond_3b
sget-object v1, Landroid/support/v7/widget/l;->a:Landroid/graphics/PorterDuff$Mode;
goto :goto_28
:cond_3e
invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V
goto :goto_2f
.end method
.method private static a(Landroid/support/v7/widget/l;)V
.registers 5
const/4 v3, 0x0
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x17
if-ge v0, v1, :cond_1f
const-string v1, "vector"
new-instance v2, Landroid/support/v7/widget/l$d;
invoke-direct {v2, v3}, Landroid/support/v7/widget/l$d;-><init>(Landroid/support/v7/widget/l$1;)V
invoke-direct {p0, v1, v2}, Landroid/support/v7/widget/l;->a(Ljava/lang/String;Landroid/support/v7/widget/l$c;)V
const/16 v1, 0xb
if-lt v0, v1, :cond_1f
const-string v0, "animated-vector"
new-instance v1, Landroid/support/v7/widget/l$a;
invoke-direct {v1, v3}, Landroid/support/v7/widget/l$a;-><init>(Landroid/support/v7/widget/l$1;)V
invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/l;->a(Ljava/lang/String;Landroid/support/v7/widget/l$c;)V
:cond_1f
return-void
.end method
.method private a(Ljava/lang/String;Landroid/support/v7/widget/l$c;)V
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/l;->k:Landroid/support/v4/e/a;
if-nez v0, :cond_b
new-instance v0, Landroid/support/v4/e/a;
invoke-direct {v0}, Landroid/support/v4/e/a;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/l;->k:Landroid/support/v4/e/a;
:cond_b
iget-object v0, p0, Landroid/support/v7/widget/l;->k:Landroid/support/v4/e/a;
invoke-virtual {v0, p1, p2}, Landroid/support/v4/e/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
return-void
.end method
.method static a(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;)Z
.registers 10
const/4 v3, -0x1
const/4 v1, 0x0
const/4 v0, 0x1
sget-object v5, Landroid/support/v7/widget/l;->a:Landroid/graphics/PorterDuff$Mode;
sget-object v2, Landroid/support/v7/widget/l;->d:[I
invoke-static {v2, p1}, Landroid/support/v7/widget/l;->a([II)Z
move-result v2
if-eqz v2, :cond_30
sget v2, Landroid/support/v7/b/a$a;->colorControlNormal:I
move v4, v2
move-object v6, v5
move v5, v0
move v2, v3
:goto_13
if-eqz v5, :cond_62
invoke-static {p2}, Landroid/support/v7/widget/ad;->b(Landroid/graphics/drawable/Drawable;)Z
move-result v1
if-eqz v1, :cond_1f
invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;
move-result-object p2
:cond_1f
invoke-static {p0, v4}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v1
invoke-static {v1, v6}, Landroid/support/v7/widget/l;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
move-result-object v1
invoke-virtual {p2, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V
if-eq v2, v3, :cond_2f
invoke-virtual {p2, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V
:goto_2f
:cond_2f
return v0
:cond_30
sget-object v2, Landroid/support/v7/widget/l;->f:[I
invoke-static {v2, p1}, Landroid/support/v7/widget/l;->a([II)Z
move-result v2
if-eqz v2, :cond_3f
sget v2, Landroid/support/v7/b/a$a;->colorControlActivated:I
move v4, v2
move-object v6, v5
move v5, v0
move v2, v3
goto :goto_13
:cond_3f
sget-object v2, Landroid/support/v7/widget/l;->g:[I
invoke-static {v2, p1}, Landroid/support/v7/widget/l;->a([II)Z
move-result v2
if-eqz v2, :cond_51
const v2, 0x1010031
sget-object v4, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;
move v5, v0
move-object v6, v4
move v4, v2
move v2, v3
goto :goto_13
:cond_51
sget v2, Landroid/support/v7/b/a$e;->abc_list_divider_mtrl_alpha:I
if-ne p1, v2, :cond_64
const v4, 0x1010030
const v2, 0x42233333
invoke-static {v2}, Ljava/lang/Math;->round(F)I
move-result v2
move-object v6, v5
move v5, v0
goto :goto_13
:cond_62
move v0, v1
goto :goto_2f
:cond_64
move v2, v3
move v4, v1
move-object v6, v5
move v5, v1
goto :goto_13
.end method
.method private a(Landroid/content/Context;JLandroid/graphics/drawable/Drawable;)Z
.registers 9
invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
move-result-object v1
if-eqz v1, :cond_2b
iget-object v2, p0, Landroid/support/v7/widget/l;->m:Ljava/lang/Object;
monitor-enter v2
:try_start_9
iget-object v0, p0, Landroid/support/v7/widget/l;->n:Ljava/util/WeakHashMap;
invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/e/e;
if-nez v0, :cond_1d
new-instance v0, Landroid/support/v4/e/e;
invoke-direct {v0}, Landroid/support/v4/e/e;-><init>()V
iget-object v3, p0, Landroid/support/v7/widget/l;->n:Ljava/util/WeakHashMap;
invoke-virtual {v3, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_1d
new-instance v3, Ljava/lang/ref/WeakReference;
invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V
invoke-virtual {v0, p2, p3, v3}, Landroid/support/v4/e/e;->b(JLjava/lang/Object;)V
monitor-exit v2
const/4 v0, 0x1
:goto_27
return v0
:catchall_28
move-exception v0
monitor-exit v2
:try_end_2a
.catchall {:try_start_9 .. :try_end_2a} :catchall_28
throw v0
:cond_2b
const/4 v0, 0x0
goto :goto_27
.end method
.method private static a([II)Z
.registers 6
const/4 v0, 0x0
array-length v2, p0
move v1, v0
:goto_3
if-ge v1, v2, :cond_a
aget v3, p0, v1
if-ne v3, p1, :cond_b
const/4 v0, 0x1
:cond_a
return v0
:cond_b
add-int/lit8 v1, v1, 0x1
goto :goto_3
.end method
.method private b(Landroid/content/Context;)Landroid/content/res/ColorStateList;
.registers 6
const/4 v1, 0x3
new-array v0, v1, [[I
new-array v1, v1, [I
const/4 v2, 0x0
sget-object v3, Landroid/support/v7/widget/am;->a:[I
aput-object v3, v0, v2
sget v3, Landroid/support/v7/b/a$a;->colorControlNormal:I
invoke-static {p1, v3}, Landroid/support/v7/widget/am;->c(Landroid/content/Context;I)I
move-result v3
aput v3, v1, v2
const/4 v2, 0x1
sget-object v3, Landroid/support/v7/widget/am;->e:[I
aput-object v3, v0, v2
sget v3, Landroid/support/v7/b/a$a;->colorControlActivated:I
invoke-static {p1, v3}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v3
aput v3, v1, v2
const/4 v2, 0x2
sget-object v3, Landroid/support/v7/widget/am;->h:[I
aput-object v3, v0, v2
sget v3, Landroid/support/v7/b/a$a;->colorControlNormal:I
invoke-static {p1, v3}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v3
aput v3, v1, v2
new-instance v2, Landroid/content/res/ColorStateList;
invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V
return-object v2
.end method
.method private c(Landroid/content/Context;)Landroid/content/res/ColorStateList;
.registers 8
const v5, 0x1010030
const/4 v1, 0x3
const v4, 0x3e99999a
new-array v0, v1, [[I
new-array v1, v1, [I
const/4 v2, 0x0
sget-object v3, Landroid/support/v7/widget/am;->a:[I
aput-object v3, v0, v2
const v3, 0x3dcccccd
invoke-static {p1, v5, v3}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;IF)I
move-result v3
aput v3, v1, v2
const/4 v2, 0x1
sget-object v3, Landroid/support/v7/widget/am;->e:[I
aput-object v3, v0, v2
sget v3, Landroid/support/v7/b/a$a;->colorControlActivated:I
invoke-static {p1, v3, v4}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;IF)I
move-result v3
aput v3, v1, v2
const/4 v2, 0x2
sget-object v3, Landroid/support/v7/widget/am;->h:[I
aput-object v3, v0, v2
invoke-static {p1, v5, v4}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;IF)I
move-result v3
aput v3, v1, v2
new-instance v2, Landroid/content/res/ColorStateList;
invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V
return-object v2
.end method
.method private c(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
.registers 11
const/4 v7, 0x1
iget-object v0, p0, Landroid/support/v7/widget/l;->o:Landroid/util/TypedValue;
if-nez v0, :cond_c
new-instance v0, Landroid/util/TypedValue;
invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/l;->o:Landroid/util/TypedValue;
:cond_c
iget-object v1, p0, Landroid/support/v7/widget/l;->o:Landroid/util/TypedValue;
invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0, p2, v1, v7}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V
invoke-static {v1}, Landroid/support/v7/widget/l;->a(Landroid/util/TypedValue;)J
move-result-wide v2
invoke-direct {p0, p1, v2, v3}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;
move-result-object v0
if-eqz v0, :cond_20
:cond_1f
:goto_1f
return-object v0
:cond_20
sget v4, Landroid/support/v7/b/a$e;->abc_cab_background_top_material:I
if-ne p2, v4, :cond_3d
new-instance v0, Landroid/graphics/drawable/LayerDrawable;
const/4 v4, 0x2
new-array v4, v4, [Landroid/graphics/drawable/Drawable;
const/4 v5, 0x0
sget v6, Landroid/support/v7/b/a$e;->abc_cab_background_internal_bg:I
invoke-virtual {p0, p1, v6}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v6
aput-object v6, v4, v5
sget v5, Landroid/support/v7/b/a$e;->abc_cab_background_top_mtrl_alpha:I
invoke-virtual {p0, p1, v5}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v5
aput-object v5, v4, v7
invoke-direct {v0, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V
:cond_3d
if-eqz v0, :cond_1f
iget v1, v1, Landroid/util/TypedValue;->changingConfigurations:I
invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V
invoke-direct {p0, p1, v2, v3, v0}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;JLandroid/graphics/drawable/Drawable;)Z
goto :goto_1f
.end method
.method private d(Landroid/content/Context;)Landroid/content/res/ColorStateList;
.registers 9
const/4 v1, 0x3
const/4 v6, 0x2
const/4 v5, 0x1
const/4 v4, 0x0
new-array v0, v1, [[I
new-array v1, v1, [I
sget v2, Landroid/support/v7/b/a$a;->colorSwitchThumbNormal:I
invoke-static {p1, v2}, Landroid/support/v7/widget/am;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
move-result-object v2
if-eqz v2, :cond_3e
invoke-virtual {v2}, Landroid/content/res/ColorStateList;->isStateful()Z
move-result v3
if-eqz v3, :cond_3e
sget-object v3, Landroid/support/v7/widget/am;->a:[I
aput-object v3, v0, v4
aget-object v3, v0, v4
invoke-virtual {v2, v3, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I
move-result v3
aput v3, v1, v4
sget-object v3, Landroid/support/v7/widget/am;->e:[I
aput-object v3, v0, v5
sget v3, Landroid/support/v7/b/a$a;->colorControlActivated:I
invoke-static {p1, v3}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v3
aput v3, v1, v5
sget-object v3, Landroid/support/v7/widget/am;->h:[I
aput-object v3, v0, v6
invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I
move-result v2
aput v2, v1, v6
:goto_38
new-instance v2, Landroid/content/res/ColorStateList;
invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V
return-object v2
:cond_3e
sget-object v2, Landroid/support/v7/widget/am;->a:[I
aput-object v2, v0, v4
sget v2, Landroid/support/v7/b/a$a;->colorSwitchThumbNormal:I
invoke-static {p1, v2}, Landroid/support/v7/widget/am;->c(Landroid/content/Context;I)I
move-result v2
aput v2, v1, v4
sget-object v2, Landroid/support/v7/widget/am;->e:[I
aput-object v2, v0, v5
sget v2, Landroid/support/v7/b/a$a;->colorControlActivated:I
invoke-static {p1, v2}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v2
aput v2, v1, v5
sget-object v2, Landroid/support/v7/widget/am;->h:[I
aput-object v2, v0, v6
sget v2, Landroid/support/v7/b/a$a;->colorSwitchThumbNormal:I
invoke-static {p1, v2}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v2
aput v2, v1, v6
goto :goto_38
.end method
.method private d(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
.registers 12
const/4 v1, 0x0
const/4 v8, 0x2
const/4 v7, 0x1
iget-object v0, p0, Landroid/support/v7/widget/l;->k:Landroid/support/v4/e/a;
if-eqz v0, :cond_bf
iget-object v0, p0, Landroid/support/v7/widget/l;->k:Landroid/support/v4/e/a;
invoke-virtual {v0}, Landroid/support/v4/e/a;->isEmpty()Z
move-result v0
if-nez v0, :cond_bf
iget-object v0, p0, Landroid/support/v7/widget/l;->l:Landroid/util/SparseArray;
if-eqz v0, :cond_2f
iget-object v0, p0, Landroid/support/v7/widget/l;->l:Landroid/util/SparseArray;
invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
const-string v2, "appcompat_skip_skip"
invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v2
if-nez v2, :cond_2d
if-eqz v0, :cond_36
iget-object v2, p0, Landroid/support/v7/widget/l;->k:Landroid/support/v4/e/a;
invoke-virtual {v2, v0}, Landroid/support/v4/e/a;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
if-nez v0, :cond_36
:cond_2d
move-object v0, v1
:goto_2e
:cond_2e
return-object v0
:cond_2f
new-instance v0, Landroid/util/SparseArray;
invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/l;->l:Landroid/util/SparseArray;
:cond_36
iget-object v0, p0, Landroid/support/v7/widget/l;->o:Landroid/util/TypedValue;
if-nez v0, :cond_41
new-instance v0, Landroid/util/TypedValue;
invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V
iput-object v0, p0, Landroid/support/v7/widget/l;->o:Landroid/util/TypedValue;
:cond_41
iget-object v2, p0, Landroid/support/v7/widget/l;->o:Landroid/util/TypedValue;
invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0, p2, v2, v7}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V
invoke-static {v2}, Landroid/support/v7/widget/l;->a(Landroid/util/TypedValue;)J
move-result-wide v4
invoke-direct {p0, p1, v4, v5}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;
move-result-object v1
if-eqz v1, :cond_56
move-object v0, v1
goto :goto_2e
:cond_56
iget-object v3, v2, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;
if-eqz v3, :cond_8a
iget-object v3, v2, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;
invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
move-result-object v3
const-string v6, ".xml"
invoke-virtual {v3, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
move-result v3
if-eqz v3, :cond_8a
:try_start_68
invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;
move-result-object v3
invoke-static {v3}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
move-result-object v6
:cond_70
invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I
move-result v0
if-eq v0, v8, :cond_78
if-ne v0, v7, :cond_70
:cond_78
if-eq v0, v8, :cond_95
new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;
const-string v2, "No start tag found"
invoke-direct {v0, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V
throw v0
:try_end_82
.catch Ljava/lang/Exception; {:try_start_68 .. :try_end_82} :catch_82
:catch_82
move-exception v0
const-string v2, "AppCompatDrawableManager"
const-string v3, "Exception while inflating drawable"
invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
:cond_8a
move-object v0, v1
:goto_8b
if-nez v0, :cond_2e
iget-object v1, p0, Landroid/support/v7/widget/l;->l:Landroid/util/SparseArray;
const-string v2, "appcompat_skip_skip"
invoke-virtual {v1, p2, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
goto :goto_2e
:try_start_95
:cond_95
invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;
move-result-object v0
iget-object v7, p0, Landroid/support/v7/widget/l;->l:Landroid/util/SparseArray;
invoke-virtual {v7, p2, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
iget-object v7, p0, Landroid/support/v7/widget/l;->k:Landroid/support/v4/e/a;
invoke-virtual {v7, v0}, Landroid/support/v4/e/a;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/l$c;
if-eqz v0, :cond_b0
invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v7
invoke-interface {v0, p1, v3, v6, v7}, Landroid/support/v7/widget/l$c;->a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
move-result-object v1
:cond_b0
if-eqz v1, :cond_bd
iget v0, v2, Landroid/util/TypedValue;->changingConfigurations:I
invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V
invoke-direct {p0, p1, v4, v5, v1}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;JLandroid/graphics/drawable/Drawable;)Z
:try_end_ba
.catch Ljava/lang/Exception; {:try_start_95 .. :try_end_ba} :catch_82
move-result v0
if-eqz v0, :cond_bd
:cond_bd
move-object v0, v1
goto :goto_8b
:cond_bf
move-object v0, v1
goto/16 :goto_2e
.end method
.method private e(Landroid/content/Context;)Landroid/content/res/ColorStateList;
.registers 6
const/4 v1, 0x3
new-array v0, v1, [[I
new-array v1, v1, [I
const/4 v2, 0x0
sget-object v3, Landroid/support/v7/widget/am;->a:[I
aput-object v3, v0, v2
sget v3, Landroid/support/v7/b/a$a;->colorControlNormal:I
invoke-static {p1, v3}, Landroid/support/v7/widget/am;->c(Landroid/content/Context;I)I
move-result v3
aput v3, v1, v2
const/4 v2, 0x1
sget-object v3, Landroid/support/v7/widget/am;->g:[I
aput-object v3, v0, v2
sget v3, Landroid/support/v7/b/a$a;->colorControlNormal:I
invoke-static {p1, v3}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v3
aput v3, v1, v2
const/4 v2, 0x2
sget-object v3, Landroid/support/v7/widget/am;->h:[I
aput-object v3, v0, v2
sget v3, Landroid/support/v7/b/a$a;->colorControlActivated:I
invoke-static {p1, v3}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v3
aput v3, v1, v2
new-instance v2, Landroid/content/res/ColorStateList;
invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V
return-object v2
.end method
.method private e(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
.registers 5
const/4 v1, 0x0
iget-object v0, p0, Landroid/support/v7/widget/l;->j:Ljava/util/WeakHashMap;
if-eqz v0, :cond_18
iget-object v0, p0, Landroid/support/v7/widget/l;->j:Ljava/util/WeakHashMap;
invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/util/SparseArray;
if-eqz v0, :cond_16
invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/content/res/ColorStateList;
:goto_15
return-object v0
:cond_16
move-object v0, v1
goto :goto_15
:cond_18
move-object v0, v1
goto :goto_15
.end method
.method private f(Landroid/content/Context;)Landroid/content/res/ColorStateList;
.registers 3
sget v0, Landroid/support/v7/b/a$a;->colorButtonNormal:I
invoke-static {p1, v0}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v0
invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/l;->f(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
move-result-object v0
return-object v0
.end method
.method private f(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
.registers 8
const/4 v1, 0x4
new-array v0, v1, [[I
new-array v1, v1, [I
const/4 v2, 0x0
sget v3, Landroid/support/v7/b/a$a;->colorControlHighlight:I
invoke-static {p1, v3}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v3
sget-object v4, Landroid/support/v7/widget/am;->a:[I
aput-object v4, v0, v2
sget v4, Landroid/support/v7/b/a$a;->colorButtonNormal:I
invoke-static {p1, v4}, Landroid/support/v7/widget/am;->c(Landroid/content/Context;I)I
move-result v4
aput v4, v1, v2
const/4 v2, 0x1
sget-object v4, Landroid/support/v7/widget/am;->d:[I
aput-object v4, v0, v2
invoke-static {v3, p2}, Landroid/support/v4/b/a;->a(II)I
move-result v4
aput v4, v1, v2
const/4 v2, 0x2
sget-object v4, Landroid/support/v7/widget/am;->b:[I
aput-object v4, v0, v2
invoke-static {v3, p2}, Landroid/support/v4/b/a;->a(II)I
move-result v3
aput v3, v1, v2
const/4 v2, 0x3
sget-object v3, Landroid/support/v7/widget/am;->h:[I
aput-object v3, v0, v2
aput p2, v1, v2
new-instance v2, Landroid/content/res/ColorStateList;
invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V
return-object v2
.end method
.method private g(Landroid/content/Context;)Landroid/content/res/ColorStateList;
.registers 3
const/4 v0, 0x0
invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/l;->f(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
move-result-object v0
return-object v0
.end method
.method private h(Landroid/content/Context;)Landroid/content/res/ColorStateList;
.registers 3
sget v0, Landroid/support/v7/b/a$a;->colorAccent:I
invoke-static {p1, v0}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v0
invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/l;->f(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
move-result-object v0
return-object v0
.end method
.method private i(Landroid/content/Context;)Landroid/content/res/ColorStateList;
.registers 6
const/4 v1, 0x3
new-array v0, v1, [[I
new-array v1, v1, [I
const/4 v2, 0x0
sget-object v3, Landroid/support/v7/widget/am;->a:[I
aput-object v3, v0, v2
sget v3, Landroid/support/v7/b/a$a;->colorControlNormal:I
invoke-static {p1, v3}, Landroid/support/v7/widget/am;->c(Landroid/content/Context;I)I
move-result v3
aput v3, v1, v2
const/4 v2, 0x1
sget-object v3, Landroid/support/v7/widget/am;->g:[I
aput-object v3, v0, v2
sget v3, Landroid/support/v7/b/a$a;->colorControlNormal:I
invoke-static {p1, v3}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v3
aput v3, v1, v2
const/4 v2, 0x2
sget-object v3, Landroid/support/v7/widget/am;->h:[I
aput-object v3, v0, v2
sget v3, Landroid/support/v7/b/a$a;->colorControlActivated:I
invoke-static {p1, v3}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v3
aput v3, v1, v2
new-instance v2, Landroid/content/res/ColorStateList;
invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V
return-object v2
.end method
.method private j(Landroid/content/Context;)Landroid/content/res/ColorStateList;
.registers 6
const/4 v1, 0x2
new-array v0, v1, [[I
new-array v1, v1, [I
const/4 v2, 0x0
sget-object v3, Landroid/support/v7/widget/am;->a:[I
aput-object v3, v0, v2
sget v3, Landroid/support/v7/b/a$a;->colorControlActivated:I
invoke-static {p1, v3}, Landroid/support/v7/widget/am;->c(Landroid/content/Context;I)I
move-result v3
aput v3, v1, v2
const/4 v2, 0x1
sget-object v3, Landroid/support/v7/widget/am;->h:[I
aput-object v3, v0, v2
sget v3, Landroid/support/v7/b/a$a;->colorControlActivated:I
invoke-static {p1, v3}, Landroid/support/v7/widget/am;->a(Landroid/content/Context;I)I
move-result v3
aput v3, v1, v2
new-instance v2, Landroid/content/res/ColorStateList;
invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V
return-object v2
.end method
.method final a(I)Landroid/graphics/PorterDuff$Mode;
.registers 4
const/4 v0, 0x0
sget v1, Landroid/support/v7/b/a$e;->abc_switch_thumb_material:I
if-ne p1, v1, :cond_7
sget-object v0, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;
:cond_7
return-object v0
.end method
.method public a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
.registers 4
const/4 v0, 0x0
invoke-virtual {p0, p1, p2, v0}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;IZ)Landroid/graphics/drawable/Drawable;
move-result-object v0
return-object v0
.end method
.method public a(Landroid/content/Context;IZ)Landroid/graphics/drawable/Drawable;
.registers 5
invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/l;->d(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v0
if-nez v0, :cond_a
invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/l;->c(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v0
:cond_a
if-nez v0, :cond_10
invoke-static {p1, p2}, Landroid/support/v4/a/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v0
:cond_10
if-eqz v0, :cond_16
invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;IZLandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
move-result-object v0
:cond_16
if-eqz v0, :cond_1b
invoke-static {v0}, Landroid/support/v7/widget/ad;->a(Landroid/graphics/drawable/Drawable;)V
:cond_1b
return-object v0
.end method
.method public final a(Landroid/content/Context;Landroid/support/v7/widget/at;I)Landroid/graphics/drawable/Drawable;
.registers 6
invoke-direct {p0, p1, p3}, Landroid/support/v7/widget/l;->d(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v0
if-nez v0, :cond_a
invoke-virtual {p2, p3}, Landroid/support/v7/widget/at;->a(I)Landroid/graphics/drawable/Drawable;
move-result-object v0
:cond_a
if-eqz v0, :cond_12
const/4 v1, 0x0
invoke-direct {p0, p1, p3, v1, v0}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;IZLandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
move-result-object v0
:goto_11
return-object v0
:cond_12
const/4 v0, 0x0
goto :goto_11
.end method
.method public final b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
.registers 5
invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/l;->e(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
move-result-object v0
if-nez v0, :cond_13
sget v1, Landroid/support/v7/b/a$e;->abc_edit_text_material:I
if-ne p2, v1, :cond_14
invoke-direct {p0, p1}, Landroid/support/v7/widget/l;->e(Landroid/content/Context;)Landroid/content/res/ColorStateList;
move-result-object v0
:cond_e
:goto_e
if-eqz v0, :cond_13
invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;ILandroid/content/res/ColorStateList;)V
:cond_13
return-object v0
:cond_14
sget v1, Landroid/support/v7/b/a$e;->abc_switch_track_mtrl_alpha:I
if-ne p2, v1, :cond_1d
invoke-direct {p0, p1}, Landroid/support/v7/widget/l;->c(Landroid/content/Context;)Landroid/content/res/ColorStateList;
move-result-object v0
goto :goto_e
:cond_1d
sget v1, Landroid/support/v7/b/a$e;->abc_switch_thumb_material:I
if-ne p2, v1, :cond_26
invoke-direct {p0, p1}, Landroid/support/v7/widget/l;->d(Landroid/content/Context;)Landroid/content/res/ColorStateList;
move-result-object v0
goto :goto_e
:cond_26
sget v1, Landroid/support/v7/b/a$e;->abc_btn_default_mtrl_shape:I
if-ne p2, v1, :cond_2f
invoke-direct {p0, p1}, Landroid/support/v7/widget/l;->f(Landroid/content/Context;)Landroid/content/res/ColorStateList;
move-result-object v0
goto :goto_e
:cond_2f
sget v1, Landroid/support/v7/b/a$e;->abc_btn_borderless_material:I
if-ne p2, v1, :cond_38
invoke-direct {p0, p1}, Landroid/support/v7/widget/l;->g(Landroid/content/Context;)Landroid/content/res/ColorStateList;
move-result-object v0
goto :goto_e
:cond_38
sget v1, Landroid/support/v7/b/a$e;->abc_btn_colored_material:I
if-ne p2, v1, :cond_41
invoke-direct {p0, p1}, Landroid/support/v7/widget/l;->h(Landroid/content/Context;)Landroid/content/res/ColorStateList;
move-result-object v0
goto :goto_e
:cond_41
sget v1, Landroid/support/v7/b/a$e;->abc_spinner_mtrl_am_alpha:I
if-eq p2, v1, :cond_49
sget v1, Landroid/support/v7/b/a$e;->abc_spinner_textfield_background_material:I
if-ne p2, v1, :cond_4e
:cond_49
invoke-direct {p0, p1}, Landroid/support/v7/widget/l;->i(Landroid/content/Context;)Landroid/content/res/ColorStateList;
move-result-object v0
goto :goto_e
:cond_4e
sget-object v1, Landroid/support/v7/widget/l;->e:[I
invoke-static {v1, p2}, Landroid/support/v7/widget/l;->a([II)Z
move-result v1
if-eqz v1, :cond_5d
sget v0, Landroid/support/v7/b/a$a;->colorControlNormal:I
invoke-static {p1, v0}, Landroid/support/v7/widget/am;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
move-result-object v0
goto :goto_e
:cond_5d
sget-object v1, Landroid/support/v7/widget/l;->h:[I
invoke-static {v1, p2}, Landroid/support/v7/widget/l;->a([II)Z
move-result v1
if-eqz v1, :cond_6a
invoke-direct {p0, p1}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;)Landroid/content/res/ColorStateList;
move-result-object v0
goto :goto_e
:cond_6a
sget-object v1, Landroid/support/v7/widget/l;->i:[I
invoke-static {v1, p2}, Landroid/support/v7/widget/l;->a([II)Z
move-result v1
if-eqz v1, :cond_77
invoke-direct {p0, p1}, Landroid/support/v7/widget/l;->b(Landroid/content/Context;)Landroid/content/res/ColorStateList;
move-result-object v0
goto :goto_e
:cond_77
sget v1, Landroid/support/v7/b/a$e;->abc_seekbar_thumb_material:I
if-ne p2, v1, :cond_e
invoke-direct {p0, p1}, Landroid/support/v7/widget/l;->j(Landroid/content/Context;)Landroid/content/res/ColorStateList;
move-result-object v0
goto :goto_e
.end method