.class public final Landroid/support/v7/widget/ViewStubCompat;
.super Landroid/view/View;
.source "ViewStubCompat.java"
.field private a:I
.field private b:I
.field private c:Ljava/lang/ref/WeakReference;
.field private d:Landroid/view/LayoutInflater;
.field private e:Landroid/support/v7/widget/ViewStubCompat$a;
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 4
const/4 v0, 0x0
invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/ViewStubCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.registers 8
const/4 v3, -0x1
const/4 v2, 0x0
invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
iput v2, p0, Landroid/support/v7/widget/ViewStubCompat;->a:I
sget-object v0, Landroid/support/v7/b/a$k;->ViewStubCompat:[I
invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
move-result-object v0
sget v1, Landroid/support/v7/b/a$k;->ViewStubCompat_android_inflatedId:I
invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/ViewStubCompat;->b:I
sget v1, Landroid/support/v7/b/a$k;->ViewStubCompat_android_layout:I
invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/ViewStubCompat;->a:I
sget v1, Landroid/support/v7/b/a$k;->ViewStubCompat_android_id:I
invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v1
invoke-virtual {p0, v1}, Landroid/support/v7/widget/ViewStubCompat;->setId(I)V
invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
const/16 v0, 0x8
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ViewStubCompat;->setVisibility(I)V
const/4 v0, 0x1
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ViewStubCompat;->setWillNotDraw(Z)V
return-void
.end method
.method public a()Landroid/view/View;
.registers 5
invoke-virtual {p0}, Landroid/support/v7/widget/ViewStubCompat;->getParent()Landroid/view/ViewParent;
move-result-object v0
if-eqz v0, :cond_5d
instance-of v1, v0, Landroid/view/ViewGroup;
if-eqz v1, :cond_5d
iget v1, p0, Landroid/support/v7/widget/ViewStubCompat;->a:I
if-eqz v1, :cond_55
check-cast v0, Landroid/view/ViewGroup;
iget-object v1, p0, Landroid/support/v7/widget/ViewStubCompat;->d:Landroid/view/LayoutInflater;
if-eqz v1, :cond_48
iget-object v1, p0, Landroid/support/v7/widget/ViewStubCompat;->d:Landroid/view/LayoutInflater;
:goto_16
iget v2, p0, Landroid/support/v7/widget/ViewStubCompat;->a:I
const/4 v3, 0x0
invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
move-result-object v1
iget v2, p0, Landroid/support/v7/widget/ViewStubCompat;->b:I
const/4 v3, -0x1
if-eq v2, v3, :cond_27
iget v2, p0, Landroid/support/v7/widget/ViewStubCompat;->b:I
invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V
:cond_27
invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I
move-result v2
invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V
invoke-virtual {p0}, Landroid/support/v7/widget/ViewStubCompat;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v3
if-eqz v3, :cond_51
invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
:goto_37
new-instance v0, Ljava/lang/ref/WeakReference;
invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V
iput-object v0, p0, Landroid/support/v7/widget/ViewStubCompat;->c:Ljava/lang/ref/WeakReference;
iget-object v0, p0, Landroid/support/v7/widget/ViewStubCompat;->e:Landroid/support/v7/widget/ViewStubCompat$a;
if-eqz v0, :cond_47
iget-object v0, p0, Landroid/support/v7/widget/ViewStubCompat;->e:Landroid/support/v7/widget/ViewStubCompat$a;
invoke-interface {v0, p0, v1}, Landroid/support/v7/widget/ViewStubCompat$a;->a(Landroid/support/v7/widget/ViewStubCompat;Landroid/view/View;)V
:cond_47
return-object v1
:cond_48
invoke-virtual {p0}, Landroid/support/v7/widget/ViewStubCompat;->getContext()Landroid/content/Context;
move-result-object v1
invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v1
goto :goto_16
:cond_51
invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V
goto :goto_37
:cond_55
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "ViewStub must have a valid layoutResource"
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_5d
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "ViewStub must have a non-null ViewGroup viewParent"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
.end method
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
.registers 2
return-void
.end method
.method public draw(Landroid/graphics/Canvas;)V
.registers 2
return-void
.end method
.method public getInflatedId()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/ViewStubCompat;->b:I
return v0
.end method
.method public getLayoutInflater()Landroid/view/LayoutInflater;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ViewStubCompat;->d:Landroid/view/LayoutInflater;
return-object v0
.end method
.method public getLayoutResource()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/ViewStubCompat;->a:I
return v0
.end method
.method protected onMeasure(II)V
.registers 4
const/4 v0, 0x0
invoke-virtual {p0, v0, v0}, Landroid/support/v7/widget/ViewStubCompat;->setMeasuredDimension(II)V
return-void
.end method
.method public setInflatedId(I)V
.registers 2
iput p1, p0, Landroid/support/v7/widget/ViewStubCompat;->b:I
return-void
.end method
.method public setLayoutInflater(Landroid/view/LayoutInflater;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/widget/ViewStubCompat;->d:Landroid/view/LayoutInflater;
return-void
.end method
.method public setLayoutResource(I)V
.registers 2
iput p1, p0, Landroid/support/v7/widget/ViewStubCompat;->a:I
return-void
.end method
.method public setOnInflateListener(Landroid/support/v7/widget/ViewStubCompat$a;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/widget/ViewStubCompat;->e:Landroid/support/v7/widget/ViewStubCompat$a;
return-void
.end method
.method public setVisibility(I)V
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/ViewStubCompat;->c:Ljava/lang/ref/WeakReference;
if-eqz v0, :cond_1a
iget-object v0, p0, Landroid/support/v7/widget/ViewStubCompat;->c:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_12
invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V
:goto_11
:cond_11
return-void
:cond_12
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "setVisibility called on un-referenced view"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_1a
invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V
if-eqz p1, :cond_22
const/4 v0, 0x4
if-ne p1, v0, :cond_11
:cond_22
invoke-virtual {p0}, Landroid/support/v7/widget/ViewStubCompat;->a()Landroid/view/View;
goto :goto_11
.end method