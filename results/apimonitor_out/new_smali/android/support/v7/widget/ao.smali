.class public Landroid/support/v7/widget/ao;
.super Landroid/content/ContextWrapper;
.source "TintContextWrapper.java"
.field private static final a:Ljava/util/ArrayList;
.field private b:Landroid/content/res/Resources;
.field private final c:Landroid/content/res/Resources$Theme;
.method static constructor <clinit>()V
.registers 1
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
sput-object v0, Landroid/support/v7/widget/ao;->a:Ljava/util/ArrayList;
return-void
.end method
.method private constructor <init>(Landroid/content/Context;)V
.registers 4
invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V
invoke-static {}, Landroid/support/v7/widget/at;->a()Z
move-result v0
if-eqz v0, :cond_1d
invoke-virtual {p0}, Landroid/support/v7/widget/ao;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/widget/ao;->c:Landroid/content/res/Resources$Theme;
iget-object v0, p0, Landroid/support/v7/widget/ao;->c:Landroid/content/res/Resources$Theme;
invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V
:goto_1c
return-void
:cond_1d
const/4 v0, 0x0
iput-object v0, p0, Landroid/support/v7/widget/ao;->c:Landroid/content/res/Resources$Theme;
goto :goto_1c
.end method
.method public static a(Landroid/content/Context;)Landroid/content/Context;
.registers 5
invoke-static {p0}, Landroid/support/v7/widget/ao;->b(Landroid/content/Context;)Z
move-result v0
if-eqz v0, :cond_29
const/4 v0, 0x0
sget-object v1, Landroid/support/v7/widget/ao;->a:Ljava/util/ArrayList;
invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
move-result v2
move v1, v0
:goto_e
if-ge v1, v2, :cond_30
sget-object v0, Landroid/support/v7/widget/ao;->a:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/ref/WeakReference;
if-eqz v0, :cond_2a
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ao;
:goto_20
if-eqz v0, :cond_2c
invoke-virtual {v0}, Landroid/support/v7/widget/ao;->getBaseContext()Landroid/content/Context;
move-result-object v3
if-ne v3, p0, :cond_2c
move-object p0, v0
:cond_29
:goto_29
return-object p0
:cond_2a
const/4 v0, 0x0
goto :goto_20
:cond_2c
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_e
:cond_30
new-instance v0, Landroid/support/v7/widget/ao;
invoke-direct {v0, p0}, Landroid/support/v7/widget/ao;-><init>(Landroid/content/Context;)V
sget-object v1, Landroid/support/v7/widget/ao;->a:Ljava/util/ArrayList;
new-instance v2, Ljava/lang/ref/WeakReference;
invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V
invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
move-object p0, v0
goto :goto_29
.end method
.method private static b(Landroid/content/Context;)Z
.registers 4
const/4 v0, 0x0
instance-of v1, p0, Landroid/support/v7/widget/ao;
if-nez v1, :cond_15
invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v1
instance-of v1, v1, Landroid/support/v7/widget/aq;
if-nez v1, :cond_15
invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v1
instance-of v1, v1, Landroid/support/v7/widget/at;
if-eqz v1, :cond_16
:goto_15
:cond_15
return v0
:cond_16
invoke-static {}, Landroid/support/v7/a/g;->j()Z
move-result v1
if-eqz v1, :cond_22
sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v2, 0x14
if-gt v1, v2, :cond_15
:cond_22
const/4 v0, 0x1
goto :goto_15
.end method
.method public getResources()Landroid/content/res/Resources;
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/ao;->b:Landroid/content/res/Resources;
if-nez v0, :cond_13
iget-object v0, p0, Landroid/support/v7/widget/ao;->c:Landroid/content/res/Resources$Theme;
if-nez v0, :cond_16
new-instance v0, Landroid/support/v7/widget/aq;
invoke-super {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;
move-result-object v1
invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/aq;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V
:goto_11
iput-object v0, p0, Landroid/support/v7/widget/ao;->b:Landroid/content/res/Resources;
:cond_13
iget-object v0, p0, Landroid/support/v7/widget/ao;->b:Landroid/content/res/Resources;
return-object v0
:cond_16
new-instance v0, Landroid/support/v7/widget/at;
invoke-super {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;
move-result-object v1
invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/at;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V
goto :goto_11
.end method
.method public getTheme()Landroid/content/res/Resources$Theme;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ao;->c:Landroid/content/res/Resources$Theme;
if-nez v0, :cond_9
invoke-super {p0}, Landroid/content/ContextWrapper;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v0
:goto_8
return-object v0
:cond_9
iget-object v0, p0, Landroid/support/v7/widget/ao;->c:Landroid/content/res/Resources$Theme;
goto :goto_8
.end method
.method public setTheme(I)V
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/ao;->c:Landroid/content/res/Resources$Theme;
if-nez v0, :cond_8
invoke-super {p0, p1}, Landroid/content/ContextWrapper;->setTheme(I)V
:goto_7
return-void
:cond_8
iget-object v0, p0, Landroid/support/v7/widget/ao;->c:Landroid/content/res/Resources$Theme;
const/4 v1, 0x1
invoke-virtual {v0, p1, v1}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V
goto :goto_7
.end method