.class  Landroid/support/v7/widget/y;
.super Ljava/lang/Object;
.source "AppCompatTextHelper.java"
.field private static final b:[I
.field private static final c:[I
.field final a:Landroid/widget/TextView;
.field private d:Landroid/support/v7/widget/ap;
.field private e:Landroid/support/v7/widget/ap;
.field private f:Landroid/support/v7/widget/ap;
.field private g:Landroid/support/v7/widget/ap;
.method static constructor <clinit>()V
.registers 3
const/4 v0, 0x5
new-array v0, v0, [I
fill-array-data v0, :array_14
sput-object v0, Landroid/support/v7/widget/y;->b:[I
const/4 v0, 0x1
new-array v0, v0, [I
const/4 v1, 0x0
sget v2, Landroid/support/v7/b/a$a;->textAllCaps:I
aput v2, v0, v1
sput-object v0, Landroid/support/v7/widget/y;->c:[I
return-void
nop
:array_14
.array-data 0x4
0x34t 0x0t 0x1t 0x1t
0x6ft 0x1t 0x1t 0x1t
0x6dt 0x1t 0x1t 0x1t
0x70t 0x1t 0x1t 0x1t
0x6et 0x1t 0x1t 0x1t
.end array-data
.end method
.method constructor <init>(Landroid/widget/TextView;)V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Landroid/support/v7/widget/y;->a:Landroid/widget/TextView;
return-void
.end method
.method protected static a(Landroid/content/Context;Landroid/support/v7/widget/l;I)Landroid/support/v7/widget/ap;
.registers 6
invoke-virtual {p1, p0, p2}, Landroid/support/v7/widget/l;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
move-result-object v1
if-eqz v1, :cond_11
new-instance v0, Landroid/support/v7/widget/ap;
invoke-direct {v0}, Landroid/support/v7/widget/ap;-><init>()V
const/4 v2, 0x1
iput-boolean v2, v0, Landroid/support/v7/widget/ap;->d:Z
iput-object v1, v0, Landroid/support/v7/widget/ap;->a:Landroid/content/res/ColorStateList;
:goto_10
return-object v0
:cond_11
const/4 v0, 0x0
goto :goto_10
.end method
.method static a(Landroid/widget/TextView;)Landroid/support/v7/widget/y;
.registers 3
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x11
if-lt v0, v1, :cond_c
new-instance v0, Landroid/support/v7/widget/z;
invoke-direct {v0, p0}, Landroid/support/v7/widget/z;-><init>(Landroid/widget/TextView;)V
:goto_b
return-object v0
:cond_c
new-instance v0, Landroid/support/v7/widget/y;
invoke-direct {v0, p0}, Landroid/support/v7/widget/y;-><init>(Landroid/widget/TextView;)V
goto :goto_b
.end method
.method  a()V
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/y;->d:Landroid/support/v7/widget/ap;
if-nez v0, :cond_10
iget-object v0, p0, Landroid/support/v7/widget/y;->e:Landroid/support/v7/widget/ap;
if-nez v0, :cond_10
iget-object v0, p0, Landroid/support/v7/widget/y;->f:Landroid/support/v7/widget/ap;
if-nez v0, :cond_10
iget-object v0, p0, Landroid/support/v7/widget/y;->g:Landroid/support/v7/widget/ap;
if-eqz v0, :cond_36
:cond_10
iget-object v0, p0, Landroid/support/v7/widget/y;->a:Landroid/widget/TextView;
invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;
move-result-object v0
const/4 v1, 0x0
aget-object v1, v0, v1
iget-object v2, p0, Landroid/support/v7/widget/y;->d:Landroid/support/v7/widget/ap;
invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/y;->a(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/ap;)V
const/4 v1, 0x1
aget-object v1, v0, v1
iget-object v2, p0, Landroid/support/v7/widget/y;->e:Landroid/support/v7/widget/ap;
invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/y;->a(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/ap;)V
const/4 v1, 0x2
aget-object v1, v0, v1
iget-object v2, p0, Landroid/support/v7/widget/y;->f:Landroid/support/v7/widget/ap;
invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/y;->a(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/ap;)V
const/4 v1, 0x3
aget-object v0, v0, v1
iget-object v1, p0, Landroid/support/v7/widget/y;->g:Landroid/support/v7/widget/ap;
invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/y;->a(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/ap;)V
:cond_36
return-void
.end method
.method  a(Landroid/content/Context;I)V
.registers 6
const/4 v2, 0x0
sget-object v0, Landroid/support/v7/widget/y;->c:[I
invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
move-result-object v0
invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v1
if-eqz v1, :cond_14
invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z
move-result v1
invoke-virtual {p0, v1}, Landroid/support/v7/widget/y;->a(Z)V
:cond_14
invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
return-void
.end method
.method final a(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/ap;)V
.registers 4
if-eqz p1, :cond_d
if-eqz p2, :cond_d
iget-object v0, p0, Landroid/support/v7/widget/y;->a:Landroid/widget/TextView;
invoke-virtual {v0}, Landroid/widget/TextView;->getDrawableState()[I
move-result-object v0
invoke-static {p1, p2, v0}, Landroid/support/v7/widget/l;->a(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/ap;[I)V
:cond_d
return-void
.end method
.method  a(Landroid/util/AttributeSet;I)V
.registers 13
const/4 v9, 0x3
const/4 v8, 0x2
const/4 v7, -0x1
const/4 v1, 0x1
const/4 v2, 0x0
iget-object v0, p0, Landroid/support/v7/widget/y;->a:Landroid/widget/TextView;
invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;
move-result-object v4
invoke-static {}, Landroid/support/v7/widget/l;->a()Landroid/support/v7/widget/l;
move-result-object v0
sget-object v3, Landroid/support/v7/widget/y;->b:[I
invoke-virtual {v4, p1, v3, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
move-result-object v3
invoke-virtual {v3, v2, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v5
invoke-virtual {v3, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v6
if-eqz v6, :cond_29
invoke-virtual {v3, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v6
invoke-static {v4, v0, v6}, Landroid/support/v7/widget/y;->a(Landroid/content/Context;Landroid/support/v7/widget/l;I)Landroid/support/v7/widget/ap;
move-result-object v6
iput-object v6, p0, Landroid/support/v7/widget/y;->d:Landroid/support/v7/widget/ap;
:cond_29
invoke-virtual {v3, v8}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v6
if-eqz v6, :cond_39
invoke-virtual {v3, v8, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v6
invoke-static {v4, v0, v6}, Landroid/support/v7/widget/y;->a(Landroid/content/Context;Landroid/support/v7/widget/l;I)Landroid/support/v7/widget/ap;
move-result-object v6
iput-object v6, p0, Landroid/support/v7/widget/y;->e:Landroid/support/v7/widget/ap;
:cond_39
invoke-virtual {v3, v9}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v6
if-eqz v6, :cond_49
invoke-virtual {v3, v9, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v6
invoke-static {v4, v0, v6}, Landroid/support/v7/widget/y;->a(Landroid/content/Context;Landroid/support/v7/widget/l;I)Landroid/support/v7/widget/ap;
move-result-object v6
iput-object v6, p0, Landroid/support/v7/widget/y;->f:Landroid/support/v7/widget/ap;
:cond_49
const/4 v6, 0x4
invoke-virtual {v3, v6}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v6
if-eqz v6, :cond_5b
const/4 v6, 0x4
invoke-virtual {v3, v6, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I
move-result v6
invoke-static {v4, v0, v6}, Landroid/support/v7/widget/y;->a(Landroid/content/Context;Landroid/support/v7/widget/l;I)Landroid/support/v7/widget/ap;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/widget/y;->g:Landroid/support/v7/widget/ap;
:cond_5b
invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V
iget-object v0, p0, Landroid/support/v7/widget/y;->a:Landroid/widget/TextView;
invoke-virtual {v0}, Landroid/widget/TextView;->getTransformationMethod()Landroid/text/method/TransformationMethod;
move-result-object v0
instance-of v0, v0, Landroid/text/method/PasswordTransformationMethod;
if-nez v0, :cond_9b
if-eq v5, v7, :cond_a1
sget-object v0, Landroid/support/v7/b/a$k;->TextAppearance:[I
invoke-virtual {v4, v5, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
move-result-object v5
sget v0, Landroid/support/v7/b/a$k;->TextAppearance_textAllCaps:I
invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v0
if-eqz v0, :cond_9e
sget v0, Landroid/support/v7/b/a$k;->TextAppearance_textAllCaps:I
invoke-virtual {v5, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z
move-result v0
move v3, v0
move v0, v1
:goto_80
invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V
:goto_83
sget-object v5, Landroid/support/v7/widget/y;->c:[I
invoke-virtual {v4, p1, v5, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
move-result-object v4
invoke-virtual {v4, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z
move-result v5
if-eqz v5, :cond_9c
invoke-virtual {v4, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z
move-result v3
:goto_93
invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V
if-eqz v1, :cond_9b
invoke-virtual {p0, v3}, Landroid/support/v7/widget/y;->a(Z)V
:cond_9b
return-void
:cond_9c
move v1, v0
goto :goto_93
:cond_9e
move v0, v2
move v3, v2
goto :goto_80
:cond_a1
move v0, v2
move v3, v2
goto :goto_83
.end method
.method  a(Z)V
.registers 5
iget-object v1, p0, Landroid/support/v7/widget/y;->a:Landroid/widget/TextView;
if-eqz p1, :cond_13
new-instance v0, Landroid/support/v7/d/a;
iget-object v2, p0, Landroid/support/v7/widget/y;->a:Landroid/widget/TextView;
invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;
move-result-object v2
invoke-direct {v0, v2}, Landroid/support/v7/d/a;-><init>(Landroid/content/Context;)V
:goto_f
invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V
return-void
:cond_13
const/4 v0, 0x0
goto :goto_f
.end method