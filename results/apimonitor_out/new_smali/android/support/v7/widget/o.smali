.class public Landroid/support/v7/widget/o;
.super Ljava/lang/Object;
.source "AppCompatImageHelper.java"
.field private final a:Landroid/widget/ImageView;
.field private final b:Landroid/support/v7/widget/l;
.method public constructor <init>(Landroid/widget/ImageView;Landroid/support/v7/widget/l;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Landroid/support/v7/widget/o;->a:Landroid/widget/ImageView;
iput-object p2, p0, Landroid/support/v7/widget/o;->b:Landroid/support/v7/widget/l;
return-void
.end method
.method public a(I)V
.registers 4
if-eqz p1, :cond_28
iget-object v0, p0, Landroid/support/v7/widget/o;->b:Landroid/support/v7/widget/l;
if-eqz v0, :cond_1d
iget-object v0, p0, Landroid/support/v7/widget/o;->b:Landroid/support/v7/widget/l;
iget-object v1, p0, Landroid/support/v7/widget/o;->a:Landroid/widget/ImageView;
invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;
move-result-object v1
invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v0
:goto_12
if-eqz v0, :cond_17
invoke-static {v0}, Landroid/support/v7/widget/ad;->a(Landroid/graphics/drawable/Drawable;)V
:cond_17
iget-object v1, p0, Landroid/support/v7/widget/o;->a:Landroid/widget/ImageView;
invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
:goto_1c
return-void
:cond_1d
iget-object v0, p0, Landroid/support/v7/widget/o;->a:Landroid/widget/ImageView;
invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;
move-result-object v0
invoke-static {v0, p1}, Landroid/support/v4/a/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v0
goto :goto_12
:cond_28
iget-object v0, p0, Landroid/support/v7/widget/o;->a:Landroid/widget/ImageView;
const/4 v1, 0x0
invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
goto :goto_1c
.end method
.method public a(Landroid/util/AttributeSet;I)V
.registers 7
const/4 v3, -0x1
iget-object v0, p0, Landroid/support/v7/widget/o;->a:Landroid/widget/ImageView;
invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;
move-result-object v0
sget-object v1, Landroid/support/v7/b/a$k;->AppCompatImageView:[I
const/4 v2, 0x0
invoke-static {v0, p1, v1, p2, v2}, Landroid/support/v7/widget/ar;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/widget/ar;
move-result-object v1
:try_start_e
sget v0, Landroid/support/v7/b/a$k;->AppCompatImageView_android_src:I
invoke-virtual {v1, v0}, Landroid/support/v7/widget/ar;->b(I)Landroid/graphics/drawable/Drawable;
move-result-object v0
if-eqz v0, :cond_1b
iget-object v2, p0, Landroid/support/v7/widget/o;->a:Landroid/widget/ImageView;
invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
:cond_1b
sget v0, Landroid/support/v7/b/a$k;->AppCompatImageView_srcCompat:I
const/4 v2, -0x1
invoke-virtual {v1, v0, v2}, Landroid/support/v7/widget/ar;->g(II)I
move-result v0
if-eq v0, v3, :cond_37
iget-object v2, p0, Landroid/support/v7/widget/o;->b:Landroid/support/v7/widget/l;
iget-object v3, p0, Landroid/support/v7/widget/o;->a:Landroid/widget/ImageView;
invoke-virtual {v3}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;
move-result-object v3
invoke-virtual {v2, v3, v0}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v0
if-eqz v0, :cond_37
iget-object v2, p0, Landroid/support/v7/widget/o;->a:Landroid/widget/ImageView;
invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
:cond_37
iget-object v0, p0, Landroid/support/v7/widget/o;->a:Landroid/widget/ImageView;
invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;
move-result-object v0
if-eqz v0, :cond_42
invoke-static {v0}, Landroid/support/v7/widget/ad;->a(Landroid/graphics/drawable/Drawable;)V
:try_end_42
.catchall {:try_start_e .. :try_end_42} :catchall_46
:cond_42
invoke-virtual {v1}, Landroid/support/v7/widget/ar;->a()V
return-void
:catchall_46
move-exception v0
invoke-virtual {v1}, Landroid/support/v7/widget/ar;->a()V
throw v0
.end method