.class public Landroid/support/v7/widget/t;
.super Landroid/widget/RadioButton;
.source "AppCompatRadioButton.java"
.implements Landroid/support/v4/widget/x;
.field private a:Landroid/support/v7/widget/l;
.field private b:Landroid/support/v7/widget/k;
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 4
sget v0, Landroid/support/v7/b/a$a;->radioButtonStyle:I
invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/t;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.registers 6
invoke-static {p1}, Landroid/support/v7/widget/ao;->a(Landroid/content/Context;)Landroid/content/Context;
move-result-object v0
invoke-direct {p0, v0, p2, p3}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
invoke-static {}, Landroid/support/v7/widget/l;->a()Landroid/support/v7/widget/l;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/widget/t;->a:Landroid/support/v7/widget/l;
new-instance v0, Landroid/support/v7/widget/k;
iget-object v1, p0, Landroid/support/v7/widget/t;->a:Landroid/support/v7/widget/l;
invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/k;-><init>(Landroid/widget/CompoundButton;Landroid/support/v7/widget/l;)V
iput-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
iget-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/k;->a(Landroid/util/AttributeSet;I)V
return-void
.end method
.method public getCompoundPaddingLeft()I
.registers 3
invoke-super {p0}, Landroid/widget/RadioButton;->getCompoundPaddingLeft()I
move-result v0
iget-object v1, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
if-eqz v1, :cond_e
iget-object v1, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
invoke-virtual {v1, v0}, Landroid/support/v7/widget/k;->a(I)I
move-result v0
:cond_e
return v0
.end method
.method public getSupportButtonTintList()Landroid/content/res/ColorStateList;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
invoke-virtual {v0}, Landroid/support/v7/widget/k;->a()Landroid/content/res/ColorStateList;
move-result-object v0
:goto_a
return-object v0
:cond_b
const/4 v0, 0x0
goto :goto_a
.end method
.method public getSupportButtonTintMode()Landroid/graphics/PorterDuff$Mode;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
invoke-virtual {v0}, Landroid/support/v7/widget/k;->b()Landroid/graphics/PorterDuff$Mode;
move-result-object v0
:goto_a
return-object v0
:cond_b
const/4 v0, 0x0
goto :goto_a
.end method
.method public setButtonDrawable(I)V
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/t;->a:Landroid/support/v7/widget/l;
if-eqz v0, :cond_12
iget-object v0, p0, Landroid/support/v7/widget/t;->a:Landroid/support/v7/widget/l;
invoke-virtual {p0}, Landroid/support/v7/widget/t;->getContext()Landroid/content/Context;
move-result-object v1
invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/l;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v0
:goto_e
invoke-virtual {p0, v0}, Landroid/support/v7/widget/t;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
return-void
:cond_12
invoke-virtual {p0}, Landroid/support/v7/widget/t;->getContext()Landroid/content/Context;
move-result-object v0
invoke-static {v0, p1}, Landroid/support/v4/a/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v0
goto :goto_e
.end method
.method public setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
.registers 3
invoke-super {p0, p1}, Landroid/widget/RadioButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
iget-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
if-eqz v0, :cond_c
iget-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
invoke-virtual {v0}, Landroid/support/v7/widget/k;->c()V
:cond_c
return-void
.end method
.method public setSupportButtonTintList(Landroid/content/res/ColorStateList;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
invoke-virtual {v0, p1}, Landroid/support/v7/widget/k;->a(Landroid/content/res/ColorStateList;)V
:cond_9
return-void
.end method
.method public setSupportButtonTintMode(Landroid/graphics/PorterDuff$Mode;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/widget/t;->b:Landroid/support/v7/widget/k;
invoke-virtual {v0, p1}, Landroid/support/v7/widget/k;->a(Landroid/graphics/PorterDuff$Mode;)V
:cond_9
return-void
.end method