.class public Landroid/support/v7/widget/ActionBarContextView;
.super Landroid/support/v7/widget/a;
.source "ActionBarContextView.java"
.field private g:Ljava/lang/CharSequence;
.field private h:Ljava/lang/CharSequence;
.field private i:Landroid/view/View;
.field private j:Landroid/view/View;
.field private k:Landroid/widget/LinearLayout;
.field private l:Landroid/widget/TextView;
.field private m:Landroid/widget/TextView;
.field private n:I
.field private o:I
.field private p:Z
.field private q:I
.method public constructor <init>(Landroid/content/Context;)V
.registers 3
const/4 v0, 0x0
invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/ActionBarContextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 4
sget v0, Landroid/support/v7/b/a$a;->actionModeStyle:I
invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/ActionBarContextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.registers 7
const/4 v2, 0x0
invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
sget-object v0, Landroid/support/v7/b/a$k;->ActionMode:[I
invoke-static {p1, p2, v0, p3, v2}, Landroid/support/v7/widget/ar;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/widget/ar;
move-result-object v0
sget v1, Landroid/support/v7/b/a$k;->ActionMode_background:I
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->a(I)Landroid/graphics/drawable/Drawable;
move-result-object v1
invoke-virtual {p0, v1}, Landroid/support/v7/widget/ActionBarContextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
sget v1, Landroid/support/v7/b/a$k;->ActionMode_titleTextStyle:I
invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ar;->g(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/ActionBarContextView;->n:I
sget v1, Landroid/support/v7/b/a$k;->ActionMode_subtitleTextStyle:I
invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ar;->g(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/ActionBarContextView;->o:I
sget v1, Landroid/support/v7/b/a$k;->ActionMode_height:I
invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ar;->f(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/ActionBarContextView;->e:I
sget v1, Landroid/support/v7/b/a$k;->ActionMode_closeItemLayout:I
sget v2, Landroid/support/v7/b/a$h;->abc_action_mode_close_item_material:I
invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ar;->g(II)I
move-result v1
iput v1, p0, Landroid/support/v7/widget/ActionBarContextView;->q:I
invoke-virtual {v0}, Landroid/support/v7/widget/ar;->a()V
return-void
.end method
.method private e()V
.registers 7
const/16 v4, 0x8
const/4 v1, 0x1
const/4 v2, 0x0
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
if-nez v0, :cond_59
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getContext()Landroid/content/Context;
move-result-object v0
invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v0
sget v3, Landroid/support/v7/b/a$h;->abc_action_bar_title_item:I
invoke-virtual {v0, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getChildCount()I
move-result v0
add-int/lit8 v0, v0, -0x1
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarContextView;->getChildAt(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/LinearLayout;
iput-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
sget v3, Landroid/support/v7/b/a$f;->action_bar_title:I
invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/TextView;
iput-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->l:Landroid/widget/TextView;
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
sget v3, Landroid/support/v7/b/a$f;->action_bar_subtitle:I
invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/TextView;
iput-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->m:Landroid/widget/TextView;
iget v0, p0, Landroid/support/v7/widget/ActionBarContextView;->n:I
if-eqz v0, :cond_4a
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->l:Landroid/widget/TextView;
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getContext()Landroid/content/Context;
move-result-object v3
iget v5, p0, Landroid/support/v7/widget/ActionBarContextView;->n:I
invoke-virtual {v0, v3, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V
:cond_4a
iget v0, p0, Landroid/support/v7/widget/ActionBarContextView;->o:I
if-eqz v0, :cond_59
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->m:Landroid/widget/TextView;
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getContext()Landroid/content/Context;
move-result-object v3
iget v5, p0, Landroid/support/v7/widget/ActionBarContextView;->o:I
invoke-virtual {v0, v3, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V
:cond_59
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->l:Landroid/widget/TextView;
iget-object v3, p0, Landroid/support/v7/widget/ActionBarContextView;->g:Ljava/lang/CharSequence;
invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->m:Landroid/widget/TextView;
iget-object v3, p0, Landroid/support/v7/widget/ActionBarContextView;->h:Ljava/lang/CharSequence;
invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->g:Ljava/lang/CharSequence;
invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-nez v0, :cond_98
move v0, v1
:goto_70
iget-object v3, p0, Landroid/support/v7/widget/ActionBarContextView;->h:Ljava/lang/CharSequence;
invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v3
if-nez v3, :cond_9a
:goto_78
iget-object v5, p0, Landroid/support/v7/widget/ActionBarContextView;->m:Landroid/widget/TextView;
if-eqz v1, :cond_9c
move v3, v2
:goto_7d
invoke-virtual {v5, v3}, Landroid/widget/TextView;->setVisibility(I)V
iget-object v3, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
if-nez v0, :cond_86
if-eqz v1, :cond_87
:cond_86
move v4, v2
:cond_87
invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
invoke-virtual {v0}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;
move-result-object v0
if-nez v0, :cond_97
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarContextView;->addView(Landroid/view/View;)V
:cond_97
return-void
:cond_98
move v0, v2
goto :goto_70
:cond_9a
move v1, v2
goto :goto_78
:cond_9c
move v3, v4
goto :goto_7d
.end method
.method public bridge synthetic a(IJ)Landroid/support/v4/f/au;
.registers 6
invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/a;->a(IJ)Landroid/support/v4/f/au;
move-result-object v0
return-object v0
.end method
.method public a(Landroid/support/v7/view/b;)V
.registers 6
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
if-nez v0, :cond_6e
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getContext()Landroid/content/Context;
move-result-object v0
invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v0
iget v1, p0, Landroid/support/v7/widget/ActionBarContextView;->q:I
const/4 v2, 0x0
invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarContextView;->addView(Landroid/view/View;)V
:goto_1a
:cond_1a
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
sget v1, Landroid/support/v7/b/a$f;->action_mode_close_button:I
invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v0
new-instance v1, Landroid/support/v7/widget/ActionBarContextView$1;
invoke-direct {v1, p0, p1}, Landroid/support/v7/widget/ActionBarContextView$1;-><init>(Landroid/support/v7/widget/ActionBarContextView;Landroid/support/v7/view/b;)V
invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
invoke-virtual {p1}, Landroid/support/v7/view/b;->b()Landroid/view/Menu;
move-result-object v0
check-cast v0, Landroid/support/v7/view/menu/f;
iget-object v1, p0, Landroid/support/v7/widget/ActionBarContextView;->d:Landroid/support/v7/widget/d;
if-eqz v1, :cond_39
iget-object v1, p0, Landroid/support/v7/widget/ActionBarContextView;->d:Landroid/support/v7/widget/d;
invoke-virtual {v1}, Landroid/support/v7/widget/d;->f()Z
:cond_39
new-instance v1, Landroid/support/v7/widget/d;
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getContext()Landroid/content/Context;
move-result-object v2
invoke-direct {v1, v2}, Landroid/support/v7/widget/d;-><init>(Landroid/content/Context;)V
iput-object v1, p0, Landroid/support/v7/widget/ActionBarContextView;->d:Landroid/support/v7/widget/d;
iget-object v1, p0, Landroid/support/v7/widget/ActionBarContextView;->d:Landroid/support/v7/widget/d;
const/4 v2, 0x1
invoke-virtual {v1, v2}, Landroid/support/v7/widget/d;->c(Z)V
new-instance v1, Landroid/view/ViewGroup$LayoutParams;
const/4 v2, -0x2
const/4 v3, -0x1
invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V
iget-object v2, p0, Landroid/support/v7/widget/ActionBarContextView;->d:Landroid/support/v7/widget/d;
iget-object v3, p0, Landroid/support/v7/widget/ActionBarContextView;->b:Landroid/content/Context;
invoke-virtual {v0, v2, v3}, Landroid/support/v7/view/menu/f;->a(Landroid/support/v7/view/menu/l;Landroid/content/Context;)V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->d:Landroid/support/v7/widget/d;
invoke-virtual {v0, p0}, Landroid/support/v7/widget/d;->a(Landroid/view/ViewGroup;)Landroid/support/v7/view/menu/m;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionMenuView;
iput-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;
const/4 v2, 0x0
invoke-virtual {v0, v2}, Landroid/support/v7/widget/ActionMenuView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/ActionBarContextView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
return-void
:cond_6e
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;
move-result-object v0
if-nez v0, :cond_1a
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarContextView;->addView(Landroid/view/View;)V
goto :goto_1a
.end method
.method public a()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->d:Landroid/support/v7/widget/d;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->d:Landroid/support/v7/widget/d;
invoke-virtual {v0}, Landroid/support/v7/widget/d;->d()Z
move-result v0
:goto_a
return v0
:cond_b
const/4 v0, 0x0
goto :goto_a
.end method
.method public b()V
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
if-nez v0, :cond_7
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->c()V
:cond_7
return-void
.end method
.method public c()V
.registers 2
const/4 v0, 0x0
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->removeAllViews()V
iput-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->j:Landroid/view/View;
iput-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;
return-void
.end method
.method public d()Z
.registers 2
iget-boolean v0, p0, Landroid/support/v7/widget/ActionBarContextView;->p:Z
return v0
.end method
.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
.registers 4
new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;
const/4 v1, -0x1
const/4 v2, -0x2
invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V
return-object v0
.end method
.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
.registers 4
new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getContext()Landroid/content/Context;
move-result-object v1
invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-object v0
.end method
.method public bridge synthetic getAnimatedVisibility()I
.registers 2
invoke-super {p0}, Landroid/support/v7/widget/a;->getAnimatedVisibility()I
move-result v0
return v0
.end method
.method public bridge synthetic getContentHeight()I
.registers 2
invoke-super {p0}, Landroid/support/v7/widget/a;->getContentHeight()I
move-result v0
return v0
.end method
.method public getSubtitle()Ljava/lang/CharSequence;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->h:Ljava/lang/CharSequence;
return-object v0
.end method
.method public getTitle()Ljava/lang/CharSequence;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->g:Ljava/lang/CharSequence;
return-object v0
.end method
.method public onDetachedFromWindow()V
.registers 2
invoke-super {p0}, Landroid/support/v7/widget/a;->onDetachedFromWindow()V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->d:Landroid/support/v7/widget/d;
if-eqz v0, :cond_11
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->d:Landroid/support/v7/widget/d;
invoke-virtual {v0}, Landroid/support/v7/widget/d;->e()Z
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->d:Landroid/support/v7/widget/d;
invoke-virtual {v0}, Landroid/support/v7/widget/d;->g()Z
:cond_11
return-void
.end method
.method public bridge synthetic onHoverEvent(Landroid/view/MotionEvent;)Z
.registers 3
invoke-super {p0, p1}, Landroid/support/v7/widget/a;->onHoverEvent(Landroid/view/MotionEvent;)Z
move-result v0
return v0
.end method
.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
.registers 4
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0xe
if-lt v0, v1, :cond_2c
invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I
move-result v0
const/16 v1, 0x20
if-ne v0, v1, :cond_2d
invoke-virtual {p1, p0}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;)V
invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getContext()Landroid/content/Context;
move-result-object v0
invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v0
invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->g:Ljava/lang/CharSequence;
invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V
:cond_2c
:goto_2c
return-void
:cond_2d
invoke-super {p0, p1}, Landroid/support/v7/widget/a;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
goto :goto_2c
.end method
.method protected onLayout(ZIIII)V
.registers 14
const/16 v7, 0x8
invoke-static {p0}, Landroid/support/v7/widget/au;->a(Landroid/view/View;)Z
move-result v5
if-eqz v5, :cond_89
sub-int v0, p4, p2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getPaddingRight()I
move-result v1
sub-int/2addr v0, v1
move v1, v0
:goto_10
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getPaddingTop()I
move-result v3
sub-int v0, p5, p3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getPaddingTop()I
move-result v2
sub-int/2addr v0, v2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getPaddingBottom()I
move-result v2
sub-int v4, v0, v2
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
if-eqz v0, :cond_a1
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->getVisibility()I
move-result v0
if-eq v0, v7, :cond_a1
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;
if-eqz v5, :cond_8f
iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I
:goto_39
if-eqz v5, :cond_92
iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I
move v6, v0
:goto_3e
invoke-static {v1, v2, v5}, Landroid/support/v7/widget/ActionBarContextView;->a(IIZ)I
move-result v2
iget-object v1, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
move-object v0, p0
invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/ActionBarContextView;->a(Landroid/view/View;IIIZ)I
move-result v0
add-int/2addr v0, v2
invoke-static {v0, v6, v5}, Landroid/support/v7/widget/ActionBarContextView;->a(IIZ)I
move-result v1
move v2, v1
:goto_4f
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
if-eqz v0, :cond_67
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->j:Landroid/view/View;
if-nez v0, :cond_67
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I
move-result v0
if-eq v0, v7, :cond_67
iget-object v1, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
move-object v0, p0
invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/ActionBarContextView;->a(Landroid/view/View;IIIZ)I
move-result v0
add-int/2addr v2, v0
:cond_67
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->j:Landroid/view/View;
if-eqz v0, :cond_73
iget-object v1, p0, Landroid/support/v7/widget/ActionBarContextView;->j:Landroid/view/View;
move-object v0, p0
invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/ActionBarContextView;->a(Landroid/view/View;IIIZ)I
move-result v0
add-int/2addr v0, v2
:cond_73
if-eqz v5, :cond_96
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getPaddingLeft()I
move-result v2
:goto_79
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;
if-eqz v0, :cond_88
iget-object v1, p0, Landroid/support/v7/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;
if-nez v5, :cond_9f
const/4 v5, 0x1
:goto_82
move-object v0, p0
invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/ActionBarContextView;->a(Landroid/view/View;IIIZ)I
move-result v0
add-int/2addr v0, v2
:cond_88
return-void
:cond_89
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getPaddingLeft()I
move-result v0
move v1, v0
goto :goto_10
:cond_8f
iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I
goto :goto_39
:cond_92
iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I
move v6, v0
goto :goto_3e
:cond_96
sub-int v0, p4, p2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getPaddingRight()I
move-result v1
sub-int v2, v0, v1
goto :goto_79
:cond_9f
const/4 v5, 0x0
goto :goto_82
:cond_a1
move v2, v1
goto :goto_4f
.end method
.method protected onMeasure(II)V
.registers 15
const/4 v11, -0x2
const/high16 v4, 0x4000
const/high16 v5, -0x8000
const/4 v3, 0x0
invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I
move-result v0
if-eq v0, v4, :cond_33
new-instance v0, Ljava/lang/IllegalStateException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, " can only be used "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "with android:layout_width=\"match_parent\" (or fill_parent)"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_33
invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I
move-result v0
if-nez v0, :cond_60
new-instance v0, Ljava/lang/IllegalStateException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, " can only be used "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "with android:layout_height=\"wrap_content\""
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_60
invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I
move-result v7
iget v0, p0, Landroid/support/v7/widget/ActionBarContextView;->e:I
if-lez v0, :cond_128
iget v0, p0, Landroid/support/v7/widget/ActionBarContextView;->e:I
move v1, v0
:goto_6b
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getPaddingTop()I
move-result v0
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getPaddingBottom()I
move-result v2
add-int v8, v0, v2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getPaddingLeft()I
move-result v0
sub-int v0, v7, v0
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getPaddingRight()I
move-result v2
sub-int/2addr v0, v2
sub-int v6, v1, v8
invoke-static {v6, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v2
iget-object v9, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
if-eqz v9, :cond_9f
iget-object v9, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
invoke-virtual {p0, v9, v0, v2, v3}, Landroid/support/v7/widget/ActionBarContextView;->a(Landroid/view/View;III)I
move-result v9
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->i:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;
iget v10, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I
iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I
add-int/2addr v0, v10
sub-int v0, v9, v0
:cond_9f
iget-object v9, p0, Landroid/support/v7/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;
if-eqz v9, :cond_b1
iget-object v9, p0, Landroid/support/v7/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {v9}, Landroid/support/v7/widget/ActionMenuView;->getParent()Landroid/view/ViewParent;
move-result-object v9
if-ne v9, p0, :cond_b1
iget-object v9, p0, Landroid/support/v7/widget/ActionBarContextView;->c:Landroid/support/v7/widget/ActionMenuView;
invoke-virtual {p0, v9, v0, v2, v3}, Landroid/support/v7/widget/ActionBarContextView;->a(Landroid/view/View;III)I
move-result v0
:cond_b1
iget-object v9, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
if-eqz v9, :cond_da
iget-object v9, p0, Landroid/support/v7/widget/ActionBarContextView;->j:Landroid/view/View;
if-nez v9, :cond_da
iget-boolean v9, p0, Landroid/support/v7/widget/ActionBarContextView;->p:Z
if-eqz v9, :cond_134
invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v9
iget-object v10, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
invoke-virtual {v10, v9, v2}, Landroid/widget/LinearLayout;->measure(II)V
iget-object v2, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I
move-result v9
if-gt v9, v0, :cond_12f
const/4 v2, 0x1
:goto_cf
if-eqz v2, :cond_d2
sub-int/2addr v0, v9
:cond_d2
iget-object v9, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
if-eqz v2, :cond_131
move v2, v3
:goto_d7
invoke-virtual {v9, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V
:cond_da
:goto_da
iget-object v2, p0, Landroid/support/v7/widget/ActionBarContextView;->j:Landroid/view/View;
if-eqz v2, :cond_10e
iget-object v2, p0, Landroid/support/v7/widget/ActionBarContextView;->j:Landroid/view/View;
invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v9
iget v2, v9, Landroid/view/ViewGroup$LayoutParams;->width:I
if-eq v2, v11, :cond_13b
move v2, v4
:goto_e9
iget v10, v9, Landroid/view/ViewGroup$LayoutParams;->width:I
if-ltz v10, :cond_f3
iget v10, v9, Landroid/view/ViewGroup$LayoutParams;->width:I
invoke-static {v10, v0}, Ljava/lang/Math;->min(II)I
move-result v0
:cond_f3
iget v10, v9, Landroid/view/ViewGroup$LayoutParams;->height:I
if-eq v10, v11, :cond_13d
:goto_f7
iget v5, v9, Landroid/view/ViewGroup$LayoutParams;->height:I
if-ltz v5, :cond_13f
iget v5, v9, Landroid/view/ViewGroup$LayoutParams;->height:I
invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I
move-result v5
:goto_101
iget-object v6, p0, Landroid/support/v7/widget/ActionBarContextView;->j:Landroid/view/View;
invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v0
invoke-static {v5, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v2
invoke-virtual {v6, v0, v2}, Landroid/view/View;->measure(II)V
:cond_10e
iget v0, p0, Landroid/support/v7/widget/ActionBarContextView;->e:I
if-gtz v0, :cond_145
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->getChildCount()I
move-result v2
move v1, v3
:goto_117
if-ge v3, v2, :cond_141
invoke-virtual {p0, v3}, Landroid/support/v7/widget/ActionBarContextView;->getChildAt(I)Landroid/view/View;
move-result-object v0
invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I
move-result v0
add-int/2addr v0, v8
if-le v0, v1, :cond_149
:goto_124
add-int/lit8 v3, v3, 0x1
move v1, v0
goto :goto_117
:cond_128
invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I
move-result v0
move v1, v0
goto/16 :goto_6b
:cond_12f
move v2, v3
goto :goto_cf
:cond_131
const/16 v2, 0x8
goto :goto_d7
:cond_134
iget-object v9, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
invoke-virtual {p0, v9, v0, v2, v3}, Landroid/support/v7/widget/ActionBarContextView;->a(Landroid/view/View;III)I
move-result v0
goto :goto_da
:cond_13b
move v2, v5
goto :goto_e9
:cond_13d
move v4, v5
goto :goto_f7
:cond_13f
move v5, v6
goto :goto_101
:cond_141
invoke-virtual {p0, v7, v1}, Landroid/support/v7/widget/ActionBarContextView;->setMeasuredDimension(II)V
:goto_144
return-void
:cond_145
invoke-virtual {p0, v7, v1}, Landroid/support/v7/widget/ActionBarContextView;->setMeasuredDimension(II)V
goto :goto_144
:cond_149
move v0, v1
goto :goto_124
.end method
.method public bridge synthetic onTouchEvent(Landroid/view/MotionEvent;)Z
.registers 3
invoke-super {p0, p1}, Landroid/support/v7/widget/a;->onTouchEvent(Landroid/view/MotionEvent;)Z
move-result v0
return v0
.end method
.method public setContentHeight(I)V
.registers 2
iput p1, p0, Landroid/support/v7/widget/ActionBarContextView;->e:I
return-void
.end method
.method public setCustomView(Landroid/view/View;)V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->j:Landroid/view/View;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->j:Landroid/view/View;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarContextView;->removeView(Landroid/view/View;)V
:cond_9
iput-object p1, p0, Landroid/support/v7/widget/ActionBarContextView;->j:Landroid/view/View;
if-eqz p1, :cond_19
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
if-eqz v0, :cond_19
iget-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionBarContextView;->removeView(Landroid/view/View;)V
const/4 v0, 0x0
iput-object v0, p0, Landroid/support/v7/widget/ActionBarContextView;->k:Landroid/widget/LinearLayout;
:cond_19
if-eqz p1, :cond_1e
invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionBarContextView;->addView(Landroid/view/View;)V
:cond_1e
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->requestLayout()V
return-void
.end method
.method public setSubtitle(Ljava/lang/CharSequence;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/widget/ActionBarContextView;->h:Ljava/lang/CharSequence;
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarContextView;->e()V
return-void
.end method
.method public setTitle(Ljava/lang/CharSequence;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/widget/ActionBarContextView;->g:Ljava/lang/CharSequence;
invoke-direct {p0}, Landroid/support/v7/widget/ActionBarContextView;->e()V
return-void
.end method
.method public setTitleOptional(Z)V
.registers 3
iget-boolean v0, p0, Landroid/support/v7/widget/ActionBarContextView;->p:Z
if-eq p1, v0, :cond_7
invoke-virtual {p0}, Landroid/support/v7/widget/ActionBarContextView;->requestLayout()V
:cond_7
iput-boolean p1, p0, Landroid/support/v7/widget/ActionBarContextView;->p:Z
return-void
.end method
.method public bridge synthetic setVisibility(I)V
.registers 2
invoke-super {p0, p1}, Landroid/support/v7/widget/a;->setVisibility(I)V
return-void
.end method
.method public shouldDelayChildPressedState()Z
.registers 2
const/4 v0, 0x0
return v0
.end method