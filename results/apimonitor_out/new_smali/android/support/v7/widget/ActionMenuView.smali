.class public Landroid/support/v7/widget/ActionMenuView;
.super Landroid/support/v7/widget/af;
.source "ActionMenuView.java"
.implements Landroid/support/v7/view/menu/f$b;
.implements Landroid/support/v7/view/menu/m;
.field private a:Landroid/support/v7/view/menu/f;
.field private b:Landroid/content/Context;
.field private c:I
.field private d:Z
.field private e:Landroid/support/v7/widget/d;
.field private f:Landroid/support/v7/view/menu/l$a;
.field private g:Landroid/support/v7/view/menu/f$a;
.field private h:Z
.field private i:I
.field private j:I
.field private k:I
.field private l:Landroid/support/v7/widget/ActionMenuView$e;
.method public constructor <init>(Landroid/content/Context;)V
.registers 3
const/4 v0, 0x0
invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/ActionMenuView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 6
const/4 v2, 0x0
invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/af;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
invoke-virtual {p0, v2}, Landroid/support/v7/widget/ActionMenuView;->setBaselineAligned(Z)V
invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
move-result-object v0
iget v0, v0, Landroid/util/DisplayMetrics;->density:F
const/high16 v1, 0x4260
mul-float/2addr v1, v0
float-to-int v1, v1
iput v1, p0, Landroid/support/v7/widget/ActionMenuView;->j:I
const/high16 v1, 0x4080
mul-float/2addr v0, v1
float-to-int v0, v0
iput v0, p0, Landroid/support/v7/widget/ActionMenuView;->k:I
iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->b:Landroid/content/Context;
iput v2, p0, Landroid/support/v7/widget/ActionMenuView;->c:I
return-void
.end method
.method static a(Landroid/view/View;IIII)I
.registers 13
const/4 v4, 0x1
const/4 v3, 0x2
const/4 v2, 0x0
invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionMenuView$c;
invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I
move-result v1
sub-int/2addr v1, p4
invoke-static {p3}, Landroid/view/View$MeasureSpec;->getMode(I)I
move-result v5
invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v6
instance-of v1, p0, Landroid/support/v7/view/menu/ActionMenuItemView;
if-eqz v1, :cond_5e
move-object v1, p0
check-cast v1, Landroid/support/v7/view/menu/ActionMenuItemView;
:goto_1d
if-eqz v1, :cond_60
invoke-virtual {v1}, Landroid/support/v7/view/menu/ActionMenuItemView;->b()Z
move-result v1
if-eqz v1, :cond_60
move v5, v4
:goto_26
if-lez p2, :cond_62
if-eqz v5, :cond_2c
if-lt p2, v3, :cond_62
:cond_2c
mul-int v1, p1, p2
const/high16 v7, -0x8000
invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v1
invoke-virtual {p0, v1, v6}, Landroid/view/View;->measure(II)V
invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I
move-result v7
div-int v1, v7, p1
rem-int/2addr v7, p1
if-eqz v7, :cond_42
add-int/lit8 v1, v1, 0x1
:cond_42
if-eqz v5, :cond_47
if-ge v1, v3, :cond_47
move v1, v3
:goto_47
:cond_47
iget-boolean v3, v0, Landroid/support/v7/widget/ActionMenuView$c;->a:Z
if-nez v3, :cond_4e
if-eqz v5, :cond_4e
move v2, v4
:cond_4e
iput-boolean v2, v0, Landroid/support/v7/widget/ActionMenuView$c;->d:Z
iput v1, v0, Landroid/support/v7/widget/ActionMenuView$c;->b:I
mul-int v0, v1, p1
const/high16 v2, 0x4000
invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v0
invoke-virtual {p0, v0, v6}, Landroid/view/View;->measure(II)V
return v1
:cond_5e
const/4 v1, 0x0
goto :goto_1d
:cond_60
move v5, v2
goto :goto_26
:cond_62
move v1, v2
goto :goto_47
.end method
.method static synthetic a(Landroid/support/v7/widget/ActionMenuView;)Landroid/support/v7/widget/ActionMenuView$e;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->l:Landroid/support/v7/widget/ActionMenuView$e;
return-object v0
.end method
.method static synthetic b(Landroid/support/v7/widget/ActionMenuView;)Landroid/support/v7/view/menu/f$a;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->g:Landroid/support/v7/view/menu/f$a;
return-object v0
.end method
.method private c(II)V
.registers 37
invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I
move-result v23
invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I
move-result v6
invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I
move-result v17
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I
move-result v7
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I
move-result v8
add-int/2addr v7, v8
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingTop()I
move-result v8
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingBottom()I
move-result v9
add-int v19, v8, v9
const/4 v8, -0x2
move/from16 v0, p2
move/from16 v1, v19
invoke-static {v0, v1, v8}, Landroid/support/v7/widget/ActionMenuView;->getChildMeasureSpec(III)I
move-result v24
sub-int v25, v6, v7
move-object/from16 v0, p0
iget v6, v0, Landroid/support/v7/widget/ActionMenuView;->j:I
div-int v9, v25, v6
move-object/from16 v0, p0
iget v6, v0, Landroid/support/v7/widget/ActionMenuView;->j:I
rem-int v6, v25, v6
if-nez v9, :cond_41
const/4 v6, 0x0
move-object/from16 v0, p0
move/from16 v1, v25
invoke-virtual {v0, v1, v6}, Landroid/support/v7/widget/ActionMenuView;->setMeasuredDimension(II)V
:goto_40
return-void
:cond_41
move-object/from16 v0, p0
iget v7, v0, Landroid/support/v7/widget/ActionMenuView;->j:I
div-int/2addr v6, v9
add-int v26, v7, v6
const/16 v16, 0x0
const/4 v15, 0x0
const/4 v10, 0x0
const/4 v7, 0x0
const/4 v11, 0x0
const-wide/16 v12, 0x0
invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I
move-result v27
const/4 v6, 0x0
move/from16 v18, v6
:goto_57
move/from16 v0, v18
move/from16 v1, v27
if-ge v0, v1, :cond_103
move-object/from16 v0, p0
move/from16 v1, v18
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v8
invoke-virtual {v8}, Landroid/view/View;->getVisibility()I
move-result v6
const/16 v14, 0x8
if-ne v6, v14, :cond_7e
move v8, v7
move-wide v6, v12
move/from16 v12, v16
move v13, v9
move v9, v15
:goto_73
add-int/lit8 v14, v18, 0x1
move/from16 v18, v14
move v15, v9
move/from16 v16, v12
move v9, v13
move-wide v12, v6
move v7, v8
goto :goto_57
:cond_7e
instance-of v0, v8, Landroid/support/v7/view/menu/ActionMenuItemView;
move/from16 v20, v0
add-int/lit8 v14, v7, 0x1
if-eqz v20, :cond_9a
move-object/from16 v0, p0
iget v6, v0, Landroid/support/v7/widget/ActionMenuView;->k:I
const/4 v7, 0x0
move-object/from16 v0, p0
iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->k:I
move/from16 v21, v0
const/16 v22, 0x0
move/from16 v0, v21
move/from16 v1, v22
invoke-virtual {v8, v6, v7, v0, v1}, Landroid/view/View;->setPadding(IIII)V
:cond_9a
invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v6
check-cast v6, Landroid/support/v7/widget/ActionMenuView$c;
const/4 v7, 0x0
iput-boolean v7, v6, Landroid/support/v7/widget/ActionMenuView$c;->f:Z
const/4 v7, 0x0
iput v7, v6, Landroid/support/v7/widget/ActionMenuView$c;->c:I
const/4 v7, 0x0
iput v7, v6, Landroid/support/v7/widget/ActionMenuView$c;->b:I
const/4 v7, 0x0
iput-boolean v7, v6, Landroid/support/v7/widget/ActionMenuView$c;->d:Z
const/4 v7, 0x0
iput v7, v6, Landroid/support/v7/widget/ActionMenuView$c;->leftMargin:I
const/4 v7, 0x0
iput v7, v6, Landroid/support/v7/widget/ActionMenuView$c;->rightMargin:I
if-eqz v20, :cond_ff
move-object v7, v8
check-cast v7, Landroid/support/v7/view/menu/ActionMenuItemView;
invoke-virtual {v7}, Landroid/support/v7/view/menu/ActionMenuItemView;->b()Z
move-result v7
if-eqz v7, :cond_ff
const/4 v7, 0x1
:goto_be
iput-boolean v7, v6, Landroid/support/v7/widget/ActionMenuView$c;->e:Z
iget-boolean v7, v6, Landroid/support/v7/widget/ActionMenuView$c;->a:Z
if-eqz v7, :cond_101
const/4 v7, 0x1
:goto_c5
move/from16 v0, v26
move/from16 v1, v24
move/from16 v2, v19
invoke-static {v8, v0, v7, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/View;IIII)I
move-result v20
move/from16 v0, v20
invoke-static {v15, v0}, Ljava/lang/Math;->max(II)I
move-result v15
iget-boolean v7, v6, Landroid/support/v7/widget/ActionMenuView$c;->d:Z
if-eqz v7, :cond_321
add-int/lit8 v7, v10, 0x1
:goto_db
iget-boolean v6, v6, Landroid/support/v7/widget/ActionMenuView$c;->a:Z
if-eqz v6, :cond_31e
const/4 v6, 0x1
:goto_e0
sub-int v11, v9, v20
invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I
move-result v8
move/from16 v0, v16
invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I
move-result v10
const/4 v8, 0x1
move/from16 v0, v20
if-ne v0, v8, :cond_312
const/4 v8, 0x1
shl-int v8, v8, v18
int-to-long v8, v8
or-long/2addr v8, v12
move v12, v10
move v13, v11
move v10, v7
move v11, v6
move-wide v6, v8
move v9, v15
move v8, v14
goto/16 :goto_73
:cond_ff
const/4 v7, 0x0
goto :goto_be
:cond_101
move v7, v9
goto :goto_c5
:cond_103
if-eqz v11, :cond_140
const/4 v6, 0x2
if-ne v7, v6, :cond_140
const/4 v6, 0x1
move v8, v6
:goto_10a
const/16 v18, 0x0
move-wide/from16 v20, v12
move/from16 v19, v9
:goto_110
if-lez v10, :cond_30e
if-lez v19, :cond_30e
const v14, 0x7fffffff
const-wide/16 v12, 0x0
const/4 v9, 0x0
const/4 v6, 0x0
move/from16 v22, v6
:goto_11d
move/from16 v0, v22
move/from16 v1, v27
if-ge v0, v1, :cond_163
move-object/from16 v0, p0
move/from16 v1, v22
invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v6
invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v6
check-cast v6, Landroid/support/v7/widget/ActionMenuView$c;
iget-boolean v0, v6, Landroid/support/v7/widget/ActionMenuView$c;->d:Z
move/from16 v28, v0
if-nez v28, :cond_143
move v6, v9
move v9, v14
:goto_139
add-int/lit8 v14, v22, 0x1
move/from16 v22, v14
move v14, v9
move v9, v6
goto :goto_11d
:cond_140
const/4 v6, 0x0
move v8, v6
goto :goto_10a
:cond_143
iget v0, v6, Landroid/support/v7/widget/ActionMenuView$c;->b:I
move/from16 v28, v0
move/from16 v0, v28
if-ge v0, v14, :cond_153
iget v9, v6, Landroid/support/v7/widget/ActionMenuView$c;->b:I
const/4 v6, 0x1
shl-int v6, v6, v22
int-to-long v12, v6
const/4 v6, 0x1
goto :goto_139
:cond_153
iget v6, v6, Landroid/support/v7/widget/ActionMenuView$c;->b:I
if-ne v6, v14, :cond_30a
const/4 v6, 0x1
shl-int v6, v6, v22
int-to-long v0, v6
move-wide/from16 v28, v0
or-long v12, v12, v28
add-int/lit8 v6, v9, 0x1
move v9, v14
goto :goto_139
:cond_163
or-long v20, v20, v12
move/from16 v0, v19
if-le v9, v0, :cond_1ee
move-wide/from16 v12, v20
:goto_16b
if-nez v11, :cond_273
const/4 v6, 0x1
if-ne v7, v6, :cond_273
const/4 v6, 0x1
:goto_171
if-lez v19, :cond_2bf
const-wide/16 v8, 0x0
cmp-long v8, v12, v8
if-eqz v8, :cond_2bf
add-int/lit8 v7, v7, -0x1
move/from16 v0, v19
if-lt v0, v7, :cond_184
if-nez v6, :cond_184
const/4 v7, 0x1
if-le v15, v7, :cond_2bf
:cond_184
invoke-static {v12, v13}, Ljava/lang/Long;->bitCount(J)I
move-result v7
int-to-float v7, v7
if-nez v6, :cond_304
const-wide/16 v8, 0x1
and-long/2addr v8, v12
const-wide/16 v10, 0x0
cmp-long v6, v8, v10
if-eqz v6, :cond_1a8
const/4 v6, 0x0
move-object/from16 v0, p0
invoke-virtual {v0, v6}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v6
invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v6
check-cast v6, Landroid/support/v7/widget/ActionMenuView$c;
iget-boolean v6, v6, Landroid/support/v7/widget/ActionMenuView$c;->e:Z
if-nez v6, :cond_1a8
const/high16 v6, 0x3f00
sub-float/2addr v7, v6
:cond_1a8
const/4 v6, 0x1
add-int/lit8 v8, v27, -0x1
shl-int/2addr v6, v8
int-to-long v8, v6
and-long/2addr v8, v12
const-wide/16 v10, 0x0
cmp-long v6, v8, v10
if-eqz v6, :cond_304
add-int/lit8 v6, v27, -0x1
move-object/from16 v0, p0
invoke-virtual {v0, v6}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v6
invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v6
check-cast v6, Landroid/support/v7/widget/ActionMenuView$c;
iget-boolean v6, v6, Landroid/support/v7/widget/ActionMenuView$c;->e:Z
if-nez v6, :cond_304
const/high16 v6, 0x3f00
sub-float v6, v7, v6
:goto_1ca
const/4 v7, 0x0
cmpl-float v7, v6, v7
if-lez v7, :cond_276
mul-int v7, v19, v26
int-to-float v7, v7
div-float v6, v7, v6
float-to-int v6, v6
move v7, v6
:goto_1d6
const/4 v6, 0x0
move v9, v6
move/from16 v8, v18
:goto_1da
move/from16 v0, v27
if-ge v9, v0, :cond_2c1
const/4 v6, 0x1
shl-int/2addr v6, v9
int-to-long v10, v6
and-long/2addr v10, v12
const-wide/16 v14, 0x0
cmp-long v6, v10, v14
if-nez v6, :cond_27a
move v6, v8
:goto_1e9
add-int/lit8 v8, v9, 0x1
move v9, v8
move v8, v6
goto :goto_1da
:cond_1ee
add-int/lit8 v22, v14, 0x1
const/4 v6, 0x0
move v14, v6
move/from16 v9, v19
move-wide/from16 v18, v20
:goto_1f6
move/from16 v0, v27
if-ge v14, v0, :cond_26a
move-object/from16 v0, p0
invoke-virtual {v0, v14}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v20
invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v6
check-cast v6, Landroid/support/v7/widget/ActionMenuView$c;
const/16 v21, 0x1
shl-int v21, v21, v14
move/from16 v0, v21
int-to-long v0, v0
move-wide/from16 v28, v0
and-long v28, v28, v12
const-wide/16 v30, 0x0
cmp-long v21, v28, v30
if-nez v21, :cond_22a
iget v6, v6, Landroid/support/v7/widget/ActionMenuView$c;->b:I
move/from16 v0, v22
if-ne v6, v0, :cond_307
const/4 v6, 0x1
shl-int/2addr v6, v14
int-to-long v0, v6
move-wide/from16 v20, v0
or-long v18, v18, v20
move v6, v9
:goto_225
add-int/lit8 v9, v14, 0x1
move v14, v9
move v9, v6
goto :goto_1f6
:cond_22a
if-eqz v8, :cond_257
iget-boolean v0, v6, Landroid/support/v7/widget/ActionMenuView$c;->e:Z
move/from16 v21, v0
if-eqz v21, :cond_257
const/16 v21, 0x1
move/from16 v0, v21
if-ne v9, v0, :cond_257
move-object/from16 v0, p0
iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->k:I
move/from16 v21, v0
add-int v21, v21, v26
const/16 v28, 0x0
move-object/from16 v0, p0
iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->k:I
move/from16 v29, v0
const/16 v30, 0x0
move-object/from16 v0, v20
move/from16 v1, v21
move/from16 v2, v28
move/from16 v3, v29
move/from16 v4, v30
invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V
:cond_257
iget v0, v6, Landroid/support/v7/widget/ActionMenuView$c;->b:I
move/from16 v20, v0
add-int/lit8 v20, v20, 0x1
move/from16 v0, v20
iput v0, v6, Landroid/support/v7/widget/ActionMenuView$c;->b:I
const/16 v20, 0x1
move/from16 v0, v20
iput-boolean v0, v6, Landroid/support/v7/widget/ActionMenuView$c;->f:Z
add-int/lit8 v6, v9, -0x1
goto :goto_225
:cond_26a
const/4 v6, 0x1
move-wide/from16 v20, v18
move/from16 v18, v6
move/from16 v19, v9
goto/16 :goto_110
:cond_273
const/4 v6, 0x0
goto/16 :goto_171
:cond_276
const/4 v6, 0x0
move v7, v6
goto/16 :goto_1d6
:cond_27a
move-object/from16 v0, p0
invoke-virtual {v0, v9}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v10
invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v6
check-cast v6, Landroid/support/v7/widget/ActionMenuView$c;
instance-of v10, v10, Landroid/support/v7/view/menu/ActionMenuItemView;
if-eqz v10, :cond_29d
iput v7, v6, Landroid/support/v7/widget/ActionMenuView$c;->c:I
const/4 v8, 0x1
iput-boolean v8, v6, Landroid/support/v7/widget/ActionMenuView$c;->f:Z
if-nez v9, :cond_29a
iget-boolean v8, v6, Landroid/support/v7/widget/ActionMenuView$c;->e:Z
if-nez v8, :cond_29a
neg-int v8, v7
div-int/lit8 v8, v8, 0x2
iput v8, v6, Landroid/support/v7/widget/ActionMenuView$c;->leftMargin:I
:cond_29a
const/4 v6, 0x1
goto/16 :goto_1e9
:cond_29d
iget-boolean v10, v6, Landroid/support/v7/widget/ActionMenuView$c;->a:Z
if-eqz v10, :cond_2ae
iput v7, v6, Landroid/support/v7/widget/ActionMenuView$c;->c:I
const/4 v8, 0x1
iput-boolean v8, v6, Landroid/support/v7/widget/ActionMenuView$c;->f:Z
neg-int v8, v7
div-int/lit8 v8, v8, 0x2
iput v8, v6, Landroid/support/v7/widget/ActionMenuView$c;->rightMargin:I
const/4 v6, 0x1
goto/16 :goto_1e9
:cond_2ae
if-eqz v9, :cond_2b4
div-int/lit8 v10, v7, 0x2
iput v10, v6, Landroid/support/v7/widget/ActionMenuView$c;->leftMargin:I
:cond_2b4
add-int/lit8 v10, v27, -0x1
if-eq v9, v10, :cond_2bc
div-int/lit8 v10, v7, 0x2
iput v10, v6, Landroid/support/v7/widget/ActionMenuView$c;->rightMargin:I
:cond_2bc
move v6, v8
goto/16 :goto_1e9
:cond_2bf
move/from16 v8, v18
:cond_2c1
if-eqz v8, :cond_2f0
const/4 v6, 0x0
move v7, v6
:goto_2c5
move/from16 v0, v27
if-ge v7, v0, :cond_2f0
move-object/from16 v0, p0
invoke-virtual {v0, v7}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v8
invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v6
check-cast v6, Landroid/support/v7/widget/ActionMenuView$c;
iget-boolean v9, v6, Landroid/support/v7/widget/ActionMenuView$c;->f:Z
if-nez v9, :cond_2dd
:goto_2d9
add-int/lit8 v6, v7, 0x1
move v7, v6
goto :goto_2c5
:cond_2dd
iget v9, v6, Landroid/support/v7/widget/ActionMenuView$c;->b:I
mul-int v9, v9, v26
iget v6, v6, Landroid/support/v7/widget/ActionMenuView$c;->c:I
add-int/2addr v6, v9
const/high16 v9, 0x4000
invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I
move-result v6
move/from16 v0, v24
invoke-virtual {v8, v6, v0}, Landroid/view/View;->measure(II)V
goto :goto_2d9
:cond_2f0
const/high16 v6, 0x4000
move/from16 v0, v23
if-eq v0, v6, :cond_301
:goto_2f6
move-object/from16 v0, p0
move/from16 v1, v25
move/from16 v2, v16
invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->setMeasuredDimension(II)V
goto/16 :goto_40
:cond_301
move/from16 v16, v17
goto :goto_2f6
:cond_304
move v6, v7
goto/16 :goto_1ca
:cond_307
move v6, v9
goto/16 :goto_225
:cond_30a
move v6, v9
move v9, v14
goto/16 :goto_139
:cond_30e
move-wide/from16 v12, v20
goto/16 :goto_16b
:cond_312
move v8, v14
move v9, v15
move-wide/from16 v32, v12
move v12, v10
move v13, v11
move v11, v6
move v10, v7
move-wide/from16 v6, v32
goto/16 :goto_73
:cond_31e
move v6, v11
goto/16 :goto_e0
:cond_321
move v7, v10
goto/16 :goto_db
.end method
.method public a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/ActionMenuView$c;
.registers 4
new-instance v0, Landroid/support/v7/widget/ActionMenuView$c;
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;
move-result-object v1
invoke-direct {v0, v1, p1}, Landroid/support/v7/widget/ActionMenuView$c;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-object v0
.end method
.method protected a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/ActionMenuView$c;
.registers 4
if-eqz p1, :cond_1c
instance-of v0, p1, Landroid/support/v7/widget/ActionMenuView$c;
if-eqz v0, :cond_16
new-instance v0, Landroid/support/v7/widget/ActionMenuView$c;
check-cast p1, Landroid/support/v7/widget/ActionMenuView$c;
invoke-direct {v0, p1}, Landroid/support/v7/widget/ActionMenuView$c;-><init>(Landroid/support/v7/widget/ActionMenuView$c;)V
:goto_d
iget v1, v0, Landroid/support/v7/widget/ActionMenuView$c;->h:I
if-gtz v1, :cond_15
const/16 v1, 0x10
iput v1, v0, Landroid/support/v7/widget/ActionMenuView$c;->h:I
:cond_15
:goto_15
return-object v0
:cond_16
new-instance v0, Landroid/support/v7/widget/ActionMenuView$c;
invoke-direct {v0, p1}, Landroid/support/v7/widget/ActionMenuView$c;-><init>(Landroid/view/ViewGroup$LayoutParams;)V
goto :goto_d
:cond_1c
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->b()Landroid/support/v7/widget/ActionMenuView$c;
move-result-object v0
goto :goto_15
.end method
.method public a(Landroid/support/v7/view/menu/f;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->a:Landroid/support/v7/view/menu/f;
return-void
.end method
.method public a(Landroid/support/v7/view/menu/l$a;Landroid/support/v7/view/menu/f$a;)V
.registers 3
iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->f:Landroid/support/v7/view/menu/l$a;
iput-object p2, p0, Landroid/support/v7/widget/ActionMenuView;->g:Landroid/support/v7/view/menu/f$a;
return-void
.end method
.method public a()Z
.registers 2
iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->d:Z
return v0
.end method
.method protected a(I)Z
.registers 6
const/4 v2, 0x0
if-nez p1, :cond_5
move v0, v2
:goto_4
return v0
:cond_5
add-int/lit8 v0, p1, -0x1
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v0
invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v1
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I
move-result v3
if-ge p1, v3, :cond_20
instance-of v3, v0, Landroid/support/v7/widget/ActionMenuView$a;
if-eqz v3, :cond_20
check-cast v0, Landroid/support/v7/widget/ActionMenuView$a;
invoke-interface {v0}, Landroid/support/v7/widget/ActionMenuView$a;->d()Z
move-result v0
or-int/2addr v2, v0
:cond_20
if-lez p1, :cond_2f
instance-of v0, v1, Landroid/support/v7/widget/ActionMenuView$a;
if-eqz v0, :cond_2f
move-object v0, v1
check-cast v0, Landroid/support/v7/widget/ActionMenuView$a;
invoke-interface {v0}, Landroid/support/v7/widget/ActionMenuView$a;->c()Z
move-result v0
or-int/2addr v0, v2
goto :goto_4
:cond_2f
move v0, v2
goto :goto_4
.end method
.method public a(Landroid/support/v7/view/menu/h;)Z
.registers 4
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->a:Landroid/support/v7/view/menu/f;
const/4 v1, 0x0
invoke-virtual {v0, p1, v1}, Landroid/support/v7/view/menu/f;->a(Landroid/view/MenuItem;I)Z
move-result v0
return v0
.end method
.method protected b()Landroid/support/v7/widget/ActionMenuView$c;
.registers 3
const/4 v1, -0x2
new-instance v0, Landroid/support/v7/widget/ActionMenuView$c;
invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/ActionMenuView$c;-><init>(II)V
const/16 v1, 0x10
iput v1, v0, Landroid/support/v7/widget/ActionMenuView$c;->h:I
return-object v0
.end method
.method public synthetic b(Landroid/util/AttributeSet;)Landroid/support/v7/widget/af$a;
.registers 3
invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/ActionMenuView$c;
move-result-object v0
return-object v0
.end method
.method protected synthetic b(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/af$a;
.registers 3
invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/ActionMenuView$c;
move-result-object v0
return-object v0
.end method
.method public c()Landroid/support/v7/widget/ActionMenuView$c;
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->b()Landroid/support/v7/widget/ActionMenuView$c;
move-result-object v0
const/4 v1, 0x1
iput-boolean v1, v0, Landroid/support/v7/widget/ActionMenuView$c;->a:Z
return-object v0
.end method
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
.registers 3
if-eqz p1, :cond_8
instance-of v0, p1, Landroid/support/v7/widget/ActionMenuView$c;
if-eqz v0, :cond_8
const/4 v0, 0x1
:goto_7
return v0
:cond_8
const/4 v0, 0x0
goto :goto_7
.end method
.method public d()Landroid/support/v7/view/menu/f;
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->a:Landroid/support/v7/view/menu/f;
return-object v0
.end method
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
.registers 3
const/4 v0, 0x0
return v0
.end method
.method public e()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
if-eqz v0, :cond_e
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
invoke-virtual {v0}, Landroid/support/v7/widget/d;->d()Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method public f()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
if-eqz v0, :cond_e
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
invoke-virtual {v0}, Landroid/support/v7/widget/d;->e()Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method public g()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
if-eqz v0, :cond_e
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
invoke-virtual {v0}, Landroid/support/v7/widget/d;->h()Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->b()Landroid/support/v7/widget/ActionMenuView$c;
move-result-object v0
return-object v0
.end method
.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
.registers 3
invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/ActionMenuView$c;
move-result-object v0
return-object v0
.end method
.method protected synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
.registers 3
invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/ActionMenuView$c;
move-result-object v0
return-object v0
.end method
.method public getMenu()Landroid/view/Menu;
.registers 5
const/4 v3, 0x0
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->a:Landroid/support/v7/view/menu/f;
if-nez v0, :cond_40
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;
move-result-object v0
new-instance v1, Landroid/support/v7/view/menu/f;
invoke-direct {v1, v0}, Landroid/support/v7/view/menu/f;-><init>(Landroid/content/Context;)V
iput-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->a:Landroid/support/v7/view/menu/f;
iget-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->a:Landroid/support/v7/view/menu/f;
new-instance v2, Landroid/support/v7/widget/ActionMenuView$d;
invoke-direct {v2, p0, v3}, Landroid/support/v7/widget/ActionMenuView$d;-><init>(Landroid/support/v7/widget/ActionMenuView;Landroid/support/v7/widget/ActionMenuView$1;)V
invoke-virtual {v1, v2}, Landroid/support/v7/view/menu/f;->a(Landroid/support/v7/view/menu/f$a;)V
new-instance v1, Landroid/support/v7/widget/d;
invoke-direct {v1, v0}, Landroid/support/v7/widget/d;-><init>(Landroid/content/Context;)V
iput-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
const/4 v1, 0x1
invoke-virtual {v0, v1}, Landroid/support/v7/widget/d;->c(Z)V
iget-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:Landroid/support/v7/view/menu/l$a;
if-eqz v0, :cond_43
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:Landroid/support/v7/view/menu/l$a;
:goto_2f
invoke-virtual {v1, v0}, Landroid/support/v7/widget/d;->a(Landroid/support/v7/view/menu/l$a;)V
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->a:Landroid/support/v7/view/menu/f;
iget-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
iget-object v2, p0, Landroid/support/v7/widget/ActionMenuView;->b:Landroid/content/Context;
invoke-virtual {v0, v1, v2}, Landroid/support/v7/view/menu/f;->a(Landroid/support/v7/view/menu/l;Landroid/content/Context;)V
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
invoke-virtual {v0, p0}, Landroid/support/v7/widget/d;->a(Landroid/support/v7/widget/ActionMenuView;)V
:cond_40
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->a:Landroid/support/v7/view/menu/f;
return-object v0
:cond_43
new-instance v0, Landroid/support/v7/widget/ActionMenuView$b;
invoke-direct {v0, p0, v3}, Landroid/support/v7/widget/ActionMenuView$b;-><init>(Landroid/support/v7/widget/ActionMenuView;Landroid/support/v7/widget/ActionMenuView$1;)V
goto :goto_2f
.end method
.method public getOverflowIcon()Landroid/graphics/drawable/Drawable;
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getMenu()Landroid/view/Menu;
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
invoke-virtual {v0}, Landroid/support/v7/widget/d;->c()Landroid/graphics/drawable/Drawable;
move-result-object v0
return-object v0
.end method
.method public getPopupTheme()I
.registers 2
iget v0, p0, Landroid/support/v7/widget/ActionMenuView;->c:I
return v0
.end method
.method public getWindowAnimations()I
.registers 2
const/4 v0, 0x0
return v0
.end method
.method public h()Z
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
if-eqz v0, :cond_e
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
invoke-virtual {v0}, Landroid/support/v7/widget/d;->i()Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method public i()V
.registers 2
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
invoke-virtual {v0}, Landroid/support/v7/widget/d;->f()Z
:cond_9
return-void
.end method
.method protected synthetic j()Landroid/support/v7/widget/af$a;
.registers 2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->b()Landroid/support/v7/widget/ActionMenuView$c;
move-result-object v0
return-object v0
.end method
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
.registers 4
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x8
if-lt v0, v1, :cond_9
invoke-super {p0, p1}, Landroid/support/v7/widget/af;->onConfigurationChanged(Landroid/content/res/Configuration;)V
:cond_9
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
if-eqz v0, :cond_25
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
const/4 v1, 0x0
invoke-virtual {v0, v1}, Landroid/support/v7/widget/d;->b(Z)V
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
invoke-virtual {v0}, Landroid/support/v7/widget/d;->h()Z
move-result v0
if-eqz v0, :cond_25
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
invoke-virtual {v0}, Landroid/support/v7/widget/d;->e()Z
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
invoke-virtual {v0}, Landroid/support/v7/widget/d;->d()Z
:cond_25
return-void
.end method
.method public onDetachedFromWindow()V
.registers 1
invoke-super {p0}, Landroid/support/v7/widget/af;->onDetachedFromWindow()V
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->i()V
return-void
.end method
.method protected onLayout(ZIIII)V
.registers 21
iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->h:Z
if-nez v0, :cond_8
invoke-super/range {p0 .. p5}, Landroid/support/v7/widget/af;->onLayout(ZIIII)V
:goto_7
:cond_7
return-void
:cond_8
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I
move-result v7
sub-int v0, p5, p3
div-int/lit8 v8, v0, 0x2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getDividerWidth()I
move-result v9
const/4 v5, 0x0
const/4 v4, 0x0
sub-int v0, p4, p2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I
move-result v1
sub-int/2addr v0, v1
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I
move-result v1
sub-int v3, v0, v1
const/4 v1, 0x0
invoke-static {p0}, Landroid/support/v7/widget/au;->a(Landroid/view/View;)Z
move-result v10
const/4 v0, 0x0
move v6, v0
:goto_2a
if-ge v6, v7, :cond_a2
invoke-virtual {p0, v6}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v11
invoke-virtual {v11}, Landroid/view/View;->getVisibility()I
move-result v0
const/16 v2, 0x8
if-ne v0, v2, :cond_44
move v0, v1
move v2, v4
move v1, v3
move v3, v5
:goto_3c
add-int/lit8 v4, v6, 0x1
move v6, v4
move v5, v3
move v3, v1
move v4, v2
move v1, v0
goto :goto_2a
:cond_44
invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionMenuView$c;
iget-boolean v2, v0, Landroid/support/v7/widget/ActionMenuView$c;->a:Z
if-eqz v2, :cond_85
invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I
move-result v1
invoke-virtual {p0, v6}, Landroid/support/v7/widget/ActionMenuView;->a(I)Z
move-result v2
if-eqz v2, :cond_59
add-int/2addr v1, v9
:cond_59
invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I
move-result v12
if-eqz v10, :cond_76
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I
move-result v2
iget v0, v0, Landroid/support/v7/widget/ActionMenuView$c;->leftMargin:I
add-int/2addr v0, v2
add-int v2, v0, v1
:goto_68
div-int/lit8 v13, v12, 0x2
sub-int v13, v8, v13
add-int/2addr v12, v13
invoke-virtual {v11, v0, v13, v2, v12}, Landroid/view/View;->layout(IIII)V
sub-int v1, v3, v1
const/4 v0, 0x1
move v2, v4
move v3, v5
goto :goto_3c
:cond_76
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getWidth()I
move-result v2
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I
move-result v13
sub-int/2addr v2, v13
iget v0, v0, Landroid/support/v7/widget/ActionMenuView$c;->rightMargin:I
sub-int/2addr v2, v0
sub-int v0, v2, v1
goto :goto_68
:cond_85
invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I
move-result v2
iget v11, v0, Landroid/support/v7/widget/ActionMenuView$c;->leftMargin:I
add-int/2addr v2, v11
iget v0, v0, Landroid/support/v7/widget/ActionMenuView$c;->rightMargin:I
add-int/2addr v2, v0
add-int v0, v5, v2
sub-int v2, v3, v2
invoke-virtual {p0, v6}, Landroid/support/v7/widget/ActionMenuView;->a(I)Z
move-result v3
if-eqz v3, :cond_9a
add-int/2addr v0, v9
:cond_9a
add-int/lit8 v3, v4, 0x1
move v14, v1
move v1, v2
move v2, v3
move v3, v0
move v0, v14
goto :goto_3c
:cond_a2
const/4 v0, 0x1
if-ne v7, v0, :cond_c6
if-nez v1, :cond_c6
const/4 v0, 0x0
invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v0
invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I
move-result v1
invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I
move-result v2
sub-int v3, p4, p2
div-int/lit8 v3, v3, 0x2
div-int/lit8 v4, v1, 0x2
sub-int/2addr v3, v4
div-int/lit8 v4, v2, 0x2
sub-int v4, v8, v4
add-int/2addr v1, v3
add-int/2addr v2, v4
invoke-virtual {v0, v3, v4, v1, v2}, Landroid/view/View;->layout(IIII)V
goto/16 :goto_7
:cond_c6
if-eqz v1, :cond_100
const/4 v0, 0x0
:goto_c9
sub-int v0, v4, v0
const/4 v1, 0x0
if-lez v0, :cond_102
div-int v0, v3, v0
:goto_d0
invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I
move-result v3
if-eqz v10, :cond_120
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getWidth()I
move-result v0
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I
move-result v1
sub-int v1, v0, v1
const/4 v0, 0x0
move v2, v0
:goto_e2
if-ge v2, v7, :cond_7
invoke-virtual {p0, v2}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v4
invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionMenuView$c;
invoke-virtual {v4}, Landroid/view/View;->getVisibility()I
move-result v5
const/16 v6, 0x8
if-eq v5, v6, :cond_161
iget-boolean v5, v0, Landroid/support/v7/widget/ActionMenuView$c;->a:Z
if-eqz v5, :cond_104
move v0, v1
:goto_fb
add-int/lit8 v1, v2, 0x1
move v2, v1
move v1, v0
goto :goto_e2
:cond_100
const/4 v0, 0x1
goto :goto_c9
:cond_102
const/4 v0, 0x0
goto :goto_d0
:cond_104
iget v5, v0, Landroid/support/v7/widget/ActionMenuView$c;->rightMargin:I
sub-int/2addr v1, v5
invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I
move-result v5
invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I
move-result v6
div-int/lit8 v9, v6, 0x2
sub-int v9, v8, v9
sub-int v10, v1, v5
add-int/2addr v6, v9
invoke-virtual {v4, v10, v9, v1, v6}, Landroid/view/View;->layout(IIII)V
iget v0, v0, Landroid/support/v7/widget/ActionMenuView$c;->leftMargin:I
add-int/2addr v0, v5
add-int/2addr v0, v3
sub-int v0, v1, v0
goto :goto_fb
:cond_120
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I
move-result v1
const/4 v0, 0x0
move v2, v0
:goto_126
if-ge v2, v7, :cond_7
invoke-virtual {p0, v2}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v4
invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionMenuView$c;
invoke-virtual {v4}, Landroid/view/View;->getVisibility()I
move-result v5
const/16 v6, 0x8
if-eq v5, v6, :cond_15f
iget-boolean v5, v0, Landroid/support/v7/widget/ActionMenuView$c;->a:Z
if-eqz v5, :cond_144
move v0, v1
:goto_13f
add-int/lit8 v1, v2, 0x1
move v2, v1
move v1, v0
goto :goto_126
:cond_144
iget v5, v0, Landroid/support/v7/widget/ActionMenuView$c;->leftMargin:I
add-int/2addr v1, v5
invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I
move-result v5
invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I
move-result v6
div-int/lit8 v9, v6, 0x2
sub-int v9, v8, v9
add-int v10, v1, v5
add-int/2addr v6, v9
invoke-virtual {v4, v1, v9, v10, v6}, Landroid/view/View;->layout(IIII)V
iget v0, v0, Landroid/support/v7/widget/ActionMenuView$c;->rightMargin:I
add-int/2addr v0, v5
add-int/2addr v0, v3
add-int/2addr v0, v1
goto :goto_13f
:cond_15f
move v0, v1
goto :goto_13f
:cond_161
move v0, v1
goto :goto_fb
.end method
.method protected onMeasure(II)V
.registers 8
const/4 v1, 0x1
const/4 v2, 0x0
iget-boolean v3, p0, Landroid/support/v7/widget/ActionMenuView;->h:Z
invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I
move-result v0
const/high16 v4, 0x4000
if-ne v0, v4, :cond_3a
move v0, v1
:goto_d
iput-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->h:Z
iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->h:Z
if-eq v3, v0, :cond_15
iput v2, p0, Landroid/support/v7/widget/ActionMenuView;->i:I
:cond_15
invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I
move-result v0
iget-boolean v3, p0, Landroid/support/v7/widget/ActionMenuView;->h:Z
if-eqz v3, :cond_2c
iget-object v3, p0, Landroid/support/v7/widget/ActionMenuView;->a:Landroid/support/v7/view/menu/f;
if-eqz v3, :cond_2c
iget v3, p0, Landroid/support/v7/widget/ActionMenuView;->i:I
if-eq v0, v3, :cond_2c
iput v0, p0, Landroid/support/v7/widget/ActionMenuView;->i:I
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->a:Landroid/support/v7/view/menu/f;
invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/f;->b(Z)V
:cond_2c
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I
move-result v3
iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->h:Z
if-eqz v0, :cond_3c
if-lez v3, :cond_3c
invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ActionMenuView;->c(II)V
:goto_39
return-void
:cond_3a
move v0, v2
goto :goto_d
:cond_3c
move v1, v2
:goto_3d
if-ge v1, v3, :cond_51
invoke-virtual {p0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;
move-result-object v0
invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
move-result-object v0
check-cast v0, Landroid/support/v7/widget/ActionMenuView$c;
iput v2, v0, Landroid/support/v7/widget/ActionMenuView$c;->rightMargin:I
iput v2, v0, Landroid/support/v7/widget/ActionMenuView$c;->leftMargin:I
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_3d
:cond_51
invoke-super {p0, p1, p2}, Landroid/support/v7/widget/af;->onMeasure(II)V
goto :goto_39
.end method
.method public setExpandedActionViewsExclusive(Z)V
.registers 3
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
invoke-virtual {v0, p1}, Landroid/support/v7/widget/d;->d(Z)V
return-void
.end method
.method public setOnMenuItemClickListener(Landroid/support/v7/widget/ActionMenuView$e;)V
.registers 2
iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->l:Landroid/support/v7/widget/ActionMenuView$e;
return-void
.end method
.method public setOverflowIcon(Landroid/graphics/drawable/Drawable;)V
.registers 3
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getMenu()Landroid/view/Menu;
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
invoke-virtual {v0, p1}, Landroid/support/v7/widget/d;->a(Landroid/graphics/drawable/Drawable;)V
return-void
.end method
.method public setOverflowReserved(Z)V
.registers 2
iput-boolean p1, p0, Landroid/support/v7/widget/ActionMenuView;->d:Z
return-void
.end method
.method public setPopupTheme(I)V
.registers 4
iget v0, p0, Landroid/support/v7/widget/ActionMenuView;->c:I
if-eq v0, p1, :cond_e
iput p1, p0, Landroid/support/v7/widget/ActionMenuView;->c:I
if-nez p1, :cond_f
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;
move-result-object v0
iput-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->b:Landroid/content/Context;
:cond_e
:goto_e
return-void
:cond_f
new-instance v0, Landroid/view/ContextThemeWrapper;
invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;
move-result-object v1
invoke-direct {v0, v1, p1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V
iput-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->b:Landroid/content/Context;
goto :goto_e
.end method
.method public setPresenter(Landroid/support/v7/widget/d;)V
.registers 3
iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/d;
invoke-virtual {v0, p0}, Landroid/support/v7/widget/d;->a(Landroid/support/v7/widget/ActionMenuView;)V
return-void
.end method