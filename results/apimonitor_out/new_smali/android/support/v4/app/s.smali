.class  Landroid/support/v4/app/s;
.super Ljava/lang/Object;
.source "FragmentTransitionCompat21.java"
.method public static a(Ljava/lang/Object;)Ljava/lang/Object;
.registers 1
if-eqz p0, :cond_8
check-cast p0, Landroid/transition/Transition;
invoke-virtual {p0}, Landroid/transition/Transition;->clone()Landroid/transition/Transition;
move-result-object p0
:cond_8
return-object p0
.end method
.method public static a(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList;Ljava/util/Map;Landroid/view/View;)Ljava/lang/Object;
.registers 6
if-eqz p0, :cond_15
invoke-static {p2, p1}, Landroid/support/v4/app/s;->b(Ljava/util/ArrayList;Landroid/view/View;)V
if-eqz p3, :cond_e
invoke-interface {p3}, Ljava/util/Map;->values()Ljava/util/Collection;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z
:cond_e
invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z
move-result v0
if-eqz v0, :cond_16
const/4 p0, 0x0
:cond_15
:goto_15
return-object p0
:cond_16
invoke-virtual {p2, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
move-object v0, p0
check-cast v0, Landroid/transition/Transition;
invoke-static {v0, p2}, Landroid/support/v4/app/s;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V
goto :goto_15
.end method
.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
.registers 6
const/4 v0, 0x1
check-cast p0, Landroid/transition/Transition;
check-cast p1, Landroid/transition/Transition;
check-cast p2, Landroid/transition/Transition;
if-eqz p0, :cond_52
if-eqz p1, :cond_52
:goto_b
if-eqz p3, :cond_22
new-instance v0, Landroid/transition/TransitionSet;
invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V
if-eqz p0, :cond_17
invoke-virtual {v0, p0}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
:cond_17
if-eqz p1, :cond_1c
invoke-virtual {v0, p1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
:cond_1c
if-eqz p2, :cond_21
invoke-virtual {v0, p2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
:goto_21
:cond_21
return-object v0
:cond_22
const/4 v1, 0x0
if-eqz p1, :cond_48
if-eqz p0, :cond_48
new-instance v1, Landroid/transition/TransitionSet;
invoke-direct {v1}, Landroid/transition/TransitionSet;-><init>()V
invoke-virtual {v1, p1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
move-result-object v1
invoke-virtual {v1, p0}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
move-result-object v1
invoke-virtual {v1, v0}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;
move-result-object v1
:cond_38
:goto_38
if-eqz p2, :cond_50
new-instance v0, Landroid/transition/TransitionSet;
invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V
if-eqz v1, :cond_44
invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
:cond_44
invoke-virtual {v0, p2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
goto :goto_21
:cond_48
if-eqz p1, :cond_4c
move-object v1, p1
goto :goto_38
:cond_4c
if-eqz p0, :cond_38
move-object v1, p0
goto :goto_38
:cond_50
move-object v0, v1
goto :goto_21
:cond_52
move p3, v0
goto :goto_b
.end method
.method public static a(Landroid/view/View;)Ljava/lang/String;
.registers 2
invoke-virtual {p0}, Landroid/view/View;->getTransitionName()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method private static a(Landroid/transition/Transition;Landroid/support/v4/app/s$a;)V
.registers 3
if-eqz p0, :cond_a
new-instance v0, Landroid/support/v4/app/s$3;
invoke-direct {v0, p1}, Landroid/support/v4/app/s$3;-><init>(Landroid/support/v4/app/s$a;)V
invoke-virtual {p0, v0}, Landroid/transition/Transition;->setEpicenterCallback(Landroid/transition/Transition$EpicenterCallback;)V
:cond_a
return-void
.end method
.method public static a(Landroid/view/View;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/Map;)V
.registers 24
move-object v2, p2
check-cast v2, Landroid/transition/Transition;
move-object/from16 v4, p4
check-cast v4, Landroid/transition/Transition;
move-object/from16 v6, p6
check-cast v6, Landroid/transition/Transition;
move-object/from16 v10, p8
check-cast v10, Landroid/transition/Transition;
if-eqz v10, :cond_29
invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;
move-result-object v12
new-instance v0, Landroid/support/v4/app/s$4;
move-object v1, p0
move-object/from16 v3, p3
move-object/from16 v5, p5
move-object/from16 v7, p7
move-object/from16 v8, p10
move-object/from16 v9, p9
move-object v11, p1
invoke-direct/range {v0 .. v11}, Landroid/support/v4/app/s$4;-><init>(Landroid/view/View;Landroid/transition/Transition;Ljava/util/ArrayList;Landroid/transition/Transition;Ljava/util/ArrayList;Landroid/transition/Transition;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/ArrayList;Landroid/transition/Transition;Landroid/view/View;)V
invoke-virtual {v12, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
:cond_29
return-void
.end method
.method public static a(Landroid/view/ViewGroup;Ljava/lang/Object;)V
.registers 2
check-cast p1, Landroid/transition/Transition;
invoke-static {p0, p1}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;Landroid/transition/Transition;)V
return-void
.end method
.method public static a(Ljava/lang/Object;Landroid/view/View;)V
.registers 4
check-cast p0, Landroid/transition/Transition;
invoke-static {p1}, Landroid/support/v4/app/s;->c(Landroid/view/View;)Landroid/graphics/Rect;
move-result-object v0
new-instance v1, Landroid/support/v4/app/s$1;
invoke-direct {v1, v0}, Landroid/support/v4/app/s$1;-><init>(Landroid/graphics/Rect;)V
invoke-virtual {p0, v1}, Landroid/transition/Transition;->setEpicenterCallback(Landroid/transition/Transition$EpicenterCallback;)V
return-void
.end method
.method public static a(Ljava/lang/Object;Landroid/view/View;Ljava/util/Map;Ljava/util/ArrayList;)V
.registers 8
check-cast p0, Landroid/transition/TransitionSet;
invoke-virtual {p3}, Ljava/util/ArrayList;->clear()V
invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;
move-result-object v0
invoke-virtual {p3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
invoke-virtual {p0}, Landroid/transition/TransitionSet;->getTargets()Ljava/util/List;
move-result-object v2
invoke-interface {v2}, Ljava/util/List;->clear()V
invoke-virtual {p3}, Ljava/util/ArrayList;->size()I
move-result v3
const/4 v0, 0x0
move v1, v0
:goto_19
if-ge v1, v3, :cond_28
invoke-virtual {p3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
invoke-static {v2, v0}, Landroid/support/v4/app/s;->a(Ljava/util/List;Landroid/view/View;)V
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_19
:cond_28
invoke-virtual {p3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
invoke-static {p0, p3}, Landroid/support/v4/app/s;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V
return-void
.end method
.method public static a(Ljava/lang/Object;Landroid/view/View;Z)V
.registers 3
check-cast p0, Landroid/transition/Transition;
invoke-virtual {p0, p1, p2}, Landroid/transition/Transition;->excludeTarget(Landroid/view/View;Z)Landroid/transition/Transition;
return-void
.end method
.method public static a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;Landroid/support/v4/app/s$b;Landroid/view/View;Landroid/support/v4/app/s$a;Ljava/util/Map;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/Map;Ljava/util/ArrayList;)V
.registers 22
if-nez p0, :cond_4
if-eqz p1, :cond_31
:cond_4
move-object v4, p0
check-cast v4, Landroid/transition/Transition;
if-eqz v4, :cond_c
invoke-virtual {v4, p4}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;
:cond_c
if-eqz p1, :cond_15
move-object/from16 v0, p8
move-object/from16 v1, p10
invoke-static {p1, p4, v0, v1}, Landroid/support/v4/app/s;->a(Ljava/lang/Object;Landroid/view/View;Ljava/util/Map;Ljava/util/ArrayList;)V
:cond_15
if-eqz p3, :cond_2c
invoke-virtual {p2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;
move-result-object v10
new-instance v2, Landroid/support/v4/app/s$2;
move-object v3, p2
move-object v5, p4
move-object v6, p3
move-object/from16 v7, p6
move-object/from16 v8, p9
move-object/from16 v9, p7
invoke-direct/range {v2 .. v9}, Landroid/support/v4/app/s$2;-><init>(Landroid/view/View;Landroid/transition/Transition;Landroid/view/View;Landroid/support/v4/app/s$b;Ljava/util/Map;Ljava/util/Map;Ljava/util/ArrayList;)V
invoke-virtual {v10, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
:cond_2c
move-object/from16 v0, p5
invoke-static {v4, v0}, Landroid/support/v4/app/s;->a(Landroid/transition/Transition;Landroid/support/v4/app/s$a;)V
:cond_31
return-void
.end method
.method public static a(Ljava/lang/Object;Ljava/util/ArrayList;)V
.registers 5
check-cast p0, Landroid/transition/Transition;
instance-of v0, p0, Landroid/transition/TransitionSet;
if-eqz v0, :cond_19
check-cast p0, Landroid/transition/TransitionSet;
invoke-virtual {p0}, Landroid/transition/TransitionSet;->getTransitionCount()I
move-result v1
const/4 v0, 0x0
:goto_d
if-ge v0, v1, :cond_4b
invoke-virtual {p0, v0}, Landroid/transition/TransitionSet;->getTransitionAt(I)Landroid/transition/Transition;
move-result-object v2
invoke-static {v2, p1}, Landroid/support/v4/app/s;->a(Ljava/lang/Object;Ljava/util/ArrayList;)V
add-int/lit8 v0, v0, 0x1
goto :goto_d
:cond_19
invoke-static {p0}, Landroid/support/v4/app/s;->a(Landroid/transition/Transition;)Z
move-result v0
if-nez v0, :cond_4b
invoke-virtual {p0}, Landroid/transition/Transition;->getTargets()Ljava/util/List;
move-result-object v0
if-eqz v0, :cond_4b
invoke-interface {v0}, Ljava/util/List;->size()I
move-result v1
invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
move-result v2
if-ne v1, v2, :cond_4b
invoke-interface {v0, p1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z
move-result v0
if-eqz v0, :cond_4b
invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
move-result v0
add-int/lit8 v0, v0, -0x1
move v1, v0
:goto_3c
if-ltz v1, :cond_4b
invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
invoke-virtual {p0, v0}, Landroid/transition/Transition;->removeTarget(Landroid/view/View;)Landroid/transition/Transition;
add-int/lit8 v0, v1, -0x1
move v1, v0
goto :goto_3c
:cond_4b
return-void
.end method
.method static synthetic a(Ljava/util/ArrayList;Landroid/view/View;)V
.registers 2
invoke-static {p0, p1}, Landroid/support/v4/app/s;->b(Ljava/util/ArrayList;Landroid/view/View;)V
return-void
.end method
.method private static a(Ljava/util/List;Landroid/view/View;)V
.registers 9
invoke-interface {p0}, Ljava/util/List;->size()I
move-result v2
invoke-static {p0, p1, v2}, Landroid/support/v4/app/s;->a(Ljava/util/List;Landroid/view/View;I)Z
move-result v0
if-eqz v0, :cond_b
:cond_a
return-void
:cond_b
invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
move v1, v2
:goto_f
invoke-interface {p0}, Ljava/util/List;->size()I
move-result v0
if-ge v1, v0, :cond_a
invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
instance-of v3, v0, Landroid/view/ViewGroup;
if-eqz v3, :cond_38
check-cast v0, Landroid/view/ViewGroup;
invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I
move-result v4
const/4 v3, 0x0
:goto_26
if-ge v3, v4, :cond_38
invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;
move-result-object v5
invoke-static {p0, v5, v2}, Landroid/support/v4/app/s;->a(Ljava/util/List;Landroid/view/View;I)Z
move-result v6
if-nez v6, :cond_35
invoke-interface {p0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
:cond_35
add-int/lit8 v3, v3, 0x1
goto :goto_26
:cond_38
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_f
.end method
.method public static a(Ljava/util/Map;Landroid/view/View;)V
.registers 5
invoke-virtual {p1}, Landroid/view/View;->getVisibility()I
move-result v0
if-nez v0, :cond_26
invoke-virtual {p1}, Landroid/view/View;->getTransitionName()Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_f
invoke-interface {p0, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_f
instance-of v0, p1, Landroid/view/ViewGroup;
if-eqz v0, :cond_26
check-cast p1, Landroid/view/ViewGroup;
invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I
move-result v1
const/4 v0, 0x0
:goto_1a
if-ge v0, v1, :cond_26
invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;
move-result-object v2
invoke-static {p0, v2}, Landroid/support/v4/app/s;->a(Ljava/util/Map;Landroid/view/View;)V
add-int/lit8 v0, v0, 0x1
goto :goto_1a
:cond_26
return-void
.end method
.method private static a(Landroid/transition/Transition;)Z
.registers 2
invoke-virtual {p0}, Landroid/transition/Transition;->getTargetIds()Ljava/util/List;
move-result-object v0
invoke-static {v0}, Landroid/support/v4/app/s;->a(Ljava/util/List;)Z
move-result v0
if-eqz v0, :cond_1e
invoke-virtual {p0}, Landroid/transition/Transition;->getTargetNames()Ljava/util/List;
move-result-object v0
invoke-static {v0}, Landroid/support/v4/app/s;->a(Ljava/util/List;)Z
move-result v0
if-eqz v0, :cond_1e
invoke-virtual {p0}, Landroid/transition/Transition;->getTargetTypes()Ljava/util/List;
move-result-object v0
invoke-static {v0}, Landroid/support/v4/app/s;->a(Ljava/util/List;)Z
move-result v0
if-nez v0, :cond_20
:cond_1e
const/4 v0, 0x1
:goto_1f
return v0
:cond_20
const/4 v0, 0x0
goto :goto_1f
.end method
.method private static a(Ljava/util/List;)Z
.registers 2
if-eqz p0, :cond_8
invoke-interface {p0}, Ljava/util/List;->isEmpty()Z
move-result v0
if-eqz v0, :cond_a
:cond_8
const/4 v0, 0x1
:goto_9
return v0
:cond_a
const/4 v0, 0x0
goto :goto_9
.end method
.method private static a(Ljava/util/List;Landroid/view/View;I)Z
.registers 6
const/4 v0, 0x0
move v1, v0
:goto_2
if-ge v1, p2, :cond_b
invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v2
if-ne v2, p1, :cond_c
const/4 v0, 0x1
:cond_b
return v0
:cond_c
add-int/lit8 v1, v1, 0x1
goto :goto_2
.end method
.method static synthetic b(Landroid/view/View;)Landroid/graphics/Rect;
.registers 2
invoke-static {p0}, Landroid/support/v4/app/s;->c(Landroid/view/View;)Landroid/graphics/Rect;
move-result-object v0
return-object v0
.end method
.method public static b(Ljava/lang/Object;)Ljava/lang/Object;
.registers 2
const/4 v0, 0x0
if-nez p0, :cond_4
:cond_3
:goto_3
return-object v0
:cond_4
check-cast p0, Landroid/transition/Transition;
if-eqz p0, :cond_3
new-instance v0, Landroid/transition/TransitionSet;
invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V
invoke-virtual {v0, p0}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
goto :goto_3
.end method
.method public static b(Ljava/lang/Object;Ljava/util/ArrayList;)V
.registers 5
const/4 v0, 0x0
check-cast p0, Landroid/transition/Transition;
instance-of v1, p0, Landroid/transition/TransitionSet;
if-eqz v1, :cond_19
check-cast p0, Landroid/transition/TransitionSet;
invoke-virtual {p0}, Landroid/transition/TransitionSet;->getTransitionCount()I
move-result v1
:goto_d
if-ge v0, v1, :cond_3d
invoke-virtual {p0, v0}, Landroid/transition/TransitionSet;->getTransitionAt(I)Landroid/transition/Transition;
move-result-object v2
invoke-static {v2, p1}, Landroid/support/v4/app/s;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V
add-int/lit8 v0, v0, 0x1
goto :goto_d
:cond_19
invoke-static {p0}, Landroid/support/v4/app/s;->a(Landroid/transition/Transition;)Z
move-result v1
if-nez v1, :cond_3d
invoke-virtual {p0}, Landroid/transition/Transition;->getTargets()Ljava/util/List;
move-result-object v1
invoke-static {v1}, Landroid/support/v4/app/s;->a(Ljava/util/List;)Z
move-result v1
if-eqz v1, :cond_3d
invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
move-result v2
move v1, v0
:goto_2e
if-ge v1, v2, :cond_3d
invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
invoke-virtual {p0, v0}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_2e
:cond_3d
return-void
.end method
.method private static b(Ljava/util/ArrayList;Landroid/view/View;)V
.registers 5
invoke-virtual {p1}, Landroid/view/View;->getVisibility()I
move-result v0
if-nez v0, :cond_15
instance-of v0, p1, Landroid/view/ViewGroup;
if-eqz v0, :cond_27
check-cast p1, Landroid/view/ViewGroup;
invoke-virtual {p1}, Landroid/view/ViewGroup;->isTransitionGroup()Z
move-result v0
if-eqz v0, :cond_16
invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
:cond_15
:goto_15
return-void
:cond_16
invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I
move-result v1
const/4 v0, 0x0
:goto_1b
if-ge v0, v1, :cond_15
invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;
move-result-object v2
invoke-static {p0, v2}, Landroid/support/v4/app/s;->b(Ljava/util/ArrayList;Landroid/view/View;)V
add-int/lit8 v0, v0, 0x1
goto :goto_1b
:cond_27
invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
goto :goto_15
.end method
.method private static c(Landroid/view/View;)Landroid/graphics/Rect;
.registers 8
const/4 v6, 0x1
const/4 v4, 0x0
new-instance v0, Landroid/graphics/Rect;
invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V
const/4 v1, 0x2
new-array v1, v1, [I
invoke-virtual {p0, v1}, Landroid/view/View;->getLocationOnScreen([I)V
aget v2, v1, v4
aget v3, v1, v6
aget v4, v1, v4
invoke-virtual {p0}, Landroid/view/View;->getWidth()I
move-result v5
add-int/2addr v4, v5
aget v1, v1, v6
invoke-virtual {p0}, Landroid/view/View;->getHeight()I
move-result v5
add-int/2addr v1, v5
invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Rect;->set(IIII)V
return-object v0
.end method