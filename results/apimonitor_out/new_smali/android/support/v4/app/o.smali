.class public abstract Landroid/support/v4/app/o;
.super Landroid/support/v4/app/m;
.source "FragmentHostCallback.java"
.field private final a:Landroid/app/Activity;
.field final b:Landroid/content/Context;
.field final c:I
.field final d:Landroid/support/v4/app/q;
.field private final e:Landroid/os/Handler;
.field private f:Landroid/support/v4/e/h;
.field private g:Z
.field private h:Landroid/support/v4/app/u;
.field private i:Z
.field private j:Z
.method constructor <init>(Landroid/app/Activity;Landroid/content/Context;Landroid/os/Handler;I)V
.registers 6
invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V
new-instance v0, Landroid/support/v4/app/q;
invoke-direct {v0}, Landroid/support/v4/app/q;-><init>()V
iput-object v0, p0, Landroid/support/v4/app/o;->d:Landroid/support/v4/app/q;
iput-object p1, p0, Landroid/support/v4/app/o;->a:Landroid/app/Activity;
iput-object p2, p0, Landroid/support/v4/app/o;->b:Landroid/content/Context;
iput-object p3, p0, Landroid/support/v4/app/o;->e:Landroid/os/Handler;
iput p4, p0, Landroid/support/v4/app/o;->c:I
return-void
.end method
.method constructor <init>(Landroid/support/v4/app/l;)V
.registers 4
iget-object v0, p1, Landroid/support/v4/app/l;->a:Landroid/os/Handler;
const/4 v1, 0x0
invoke-direct {p0, p1, p1, v0, v1}, Landroid/support/v4/app/o;-><init>(Landroid/app/Activity;Landroid/content/Context;Landroid/os/Handler;I)V
return-void
.end method
.method  a(Ljava/lang/String;ZZ)Landroid/support/v4/app/u;
.registers 6
iget-object v0, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
if-nez v0, :cond_b
new-instance v0, Landroid/support/v4/e/h;
invoke-direct {v0}, Landroid/support/v4/e/h;-><init>()V
iput-object v0, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
:cond_b
iget-object v0, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
invoke-virtual {v0, p1}, Landroid/support/v4/e/h;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/app/u;
if-nez v0, :cond_22
if-eqz p3, :cond_21
new-instance v0, Landroid/support/v4/app/u;
invoke-direct {v0, p1, p0, p2}, Landroid/support/v4/app/u;-><init>(Ljava/lang/String;Landroid/support/v4/app/o;Z)V
iget-object v1, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
invoke-virtual {v1, p1, v0}, Landroid/support/v4/e/h;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_21
:goto_21
return-object v0
:cond_22
invoke-virtual {v0, p0}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/o;)V
goto :goto_21
.end method
.method public a(I)Landroid/view/View;
.registers 3
const/4 v0, 0x0
return-object v0
.end method
.method public a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V
.registers 7
const/4 v0, -0x1
if-eq p3, v0, :cond_b
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "Starting activity with a requestCode requires a FragmentActivity host"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_b
iget-object v0, p0, Landroid/support/v4/app/o;->b:Landroid/content/Context;
invoke-virtual {v0, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
return-void
.end method
.method  a(Landroid/support/v4/e/h;)V
.registers 2
iput-object p1, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
return-void
.end method
.method  a(Ljava/lang/String;)V
.registers 4
iget-object v0, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
if-eqz v0, :cond_1a
iget-object v0, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
invoke-virtual {v0, p1}, Landroid/support/v4/e/h;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/app/u;
if-eqz v0, :cond_1a
iget-boolean v1, v0, Landroid/support/v4/app/u;->f:Z
if-nez v1, :cond_1a
invoke-virtual {v0}, Landroid/support/v4/app/u;->h()V
iget-object v0, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
invoke-virtual {v0, p1}, Landroid/support/v4/e/h;->remove(Ljava/lang/Object;)Ljava/lang/Object;
:cond_1a
return-void
.end method
.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
.registers 5
return-void
.end method
.method  a(Z)V
.registers 3
iput-boolean p1, p0, Landroid/support/v4/app/o;->g:Z
iget-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
if-nez v0, :cond_7
:goto_6
:cond_6
return-void
:cond_7
iget-boolean v0, p0, Landroid/support/v4/app/o;->j:Z
if-eqz v0, :cond_6
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v4/app/o;->j:Z
if-eqz p1, :cond_16
iget-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
invoke-virtual {v0}, Landroid/support/v4/app/u;->d()V
goto :goto_6
:cond_16
iget-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
invoke-virtual {v0}, Landroid/support/v4/app/u;->c()V
goto :goto_6
.end method
.method public a()Z
.registers 2
const/4 v0, 0x1
return v0
.end method
.method public a(Landroid/support/v4/app/Fragment;)Z
.registers 3
const/4 v0, 0x1
return v0
.end method
.method public b()Landroid/view/LayoutInflater;
.registers 3
iget-object v0, p0, Landroid/support/v4/app/o;->b:Landroid/content/Context;
const-string v1, "layout_inflater"
invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/LayoutInflater;
return-object v0
.end method
.method  b(Landroid/support/v4/app/Fragment;)V
.registers 2
return-void
.end method
.method  b(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
.registers 8
invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, "mLoadersStarted="
invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget-boolean v0, p0, Landroid/support/v4/app/o;->j:Z
invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V
iget-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
if-eqz v0, :cond_43
invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, "Loader Manager "
invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I
move-result v0
invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, ":"
invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
iget-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "  "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1, p2, p3, p4}, Landroid/support/v4/app/u;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
:cond_43
return-void
.end method
.method public c()V
.registers 1
return-void
.end method
.method public d()Z
.registers 2
const/4 v0, 0x1
return v0
.end method
.method public e()I
.registers 2
iget v0, p0, Landroid/support/v4/app/o;->c:I
return v0
.end method
.method  f()Landroid/app/Activity;
.registers 2
iget-object v0, p0, Landroid/support/v4/app/o;->a:Landroid/app/Activity;
return-object v0
.end method
.method  g()Landroid/content/Context;
.registers 2
iget-object v0, p0, Landroid/support/v4/app/o;->b:Landroid/content/Context;
return-object v0
.end method
.method  h()Landroid/os/Handler;
.registers 2
iget-object v0, p0, Landroid/support/v4/app/o;->e:Landroid/os/Handler;
return-object v0
.end method
.method  i()Landroid/support/v4/app/q;
.registers 2
iget-object v0, p0, Landroid/support/v4/app/o;->d:Landroid/support/v4/app/q;
return-object v0
.end method
.method  j()Z
.registers 2
iget-boolean v0, p0, Landroid/support/v4/app/o;->g:Z
return v0
.end method
.method  k()V
.registers 5
const/4 v3, 0x1
iget-boolean v0, p0, Landroid/support/v4/app/o;->j:Z
if-eqz v0, :cond_6
:goto_5
return-void
:cond_6
iput-boolean v3, p0, Landroid/support/v4/app/o;->j:Z
iget-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
if-eqz v0, :cond_14
iget-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
invoke-virtual {v0}, Landroid/support/v4/app/u;->b()V
:goto_11
:cond_11
iput-boolean v3, p0, Landroid/support/v4/app/o;->i:Z
goto :goto_5
:cond_14
iget-boolean v0, p0, Landroid/support/v4/app/o;->i:Z
if-nez v0, :cond_11
const-string v0, "(root)"
iget-boolean v1, p0, Landroid/support/v4/app/o;->j:Z
const/4 v2, 0x0
invoke-virtual {p0, v0, v1, v2}, Landroid/support/v4/app/o;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/u;
move-result-object v0
iput-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
iget-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
if-eqz v0, :cond_11
iget-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
iget-boolean v0, v0, Landroid/support/v4/app/u;->e:Z
if-nez v0, :cond_11
iget-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
invoke-virtual {v0}, Landroid/support/v4/app/u;->b()V
goto :goto_11
.end method
.method  l()V
.registers 2
iget-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
if-nez v0, :cond_5
:goto_4
return-void
:cond_5
iget-object v0, p0, Landroid/support/v4/app/o;->h:Landroid/support/v4/app/u;
invoke-virtual {v0}, Landroid/support/v4/app/u;->h()V
goto :goto_4
.end method
.method  m()V
.registers 5
iget-object v0, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
if-eqz v0, :cond_2d
iget-object v0, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
invoke-virtual {v0}, Landroid/support/v4/e/h;->size()I
move-result v2
new-array v3, v2, [Landroid/support/v4/app/u;
add-int/lit8 v0, v2, -0x1
move v1, v0
:goto_f
if-ltz v1, :cond_1f
iget-object v0, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
invoke-virtual {v0, v1}, Landroid/support/v4/e/h;->c(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/app/u;
aput-object v0, v3, v1
add-int/lit8 v0, v1, -0x1
move v1, v0
goto :goto_f
:cond_1f
const/4 v0, 0x0
:goto_20
if-ge v0, v2, :cond_2d
aget-object v1, v3, v0
invoke-virtual {v1}, Landroid/support/v4/app/u;->e()V
invoke-virtual {v1}, Landroid/support/v4/app/u;->g()V
add-int/lit8 v0, v0, 0x1
goto :goto_20
:cond_2d
return-void
.end method
.method  n()Landroid/support/v4/e/h;
.registers 7
const/4 v1, 0x0
iget-object v0, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
if-eqz v0, :cond_38
iget-object v0, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
invoke-virtual {v0}, Landroid/support/v4/e/h;->size()I
move-result v3
new-array v4, v3, [Landroid/support/v4/app/u;
add-int/lit8 v0, v3, -0x1
move v2, v0
:goto_10
if-ltz v2, :cond_20
iget-object v0, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
invoke-virtual {v0, v2}, Landroid/support/v4/e/h;->c(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/app/u;
aput-object v0, v4, v2
add-int/lit8 v0, v2, -0x1
move v2, v0
goto :goto_10
:cond_20
move v0, v1
:goto_21
if-ge v1, v3, :cond_39
aget-object v2, v4, v1
iget-boolean v5, v2, Landroid/support/v4/app/u;->f:Z
if-eqz v5, :cond_2d
const/4 v0, 0x1
:goto_2a
add-int/lit8 v1, v1, 0x1
goto :goto_21
:cond_2d
invoke-virtual {v2}, Landroid/support/v4/app/u;->h()V
iget-object v5, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
iget-object v2, v2, Landroid/support/v4/app/u;->d:Ljava/lang/String;
invoke-virtual {v5, v2}, Landroid/support/v4/e/h;->remove(Ljava/lang/Object;)Ljava/lang/Object;
goto :goto_2a
:cond_38
move v0, v1
:cond_39
if-eqz v0, :cond_3e
iget-object v0, p0, Landroid/support/v4/app/o;->f:Landroid/support/v4/e/h;
:goto_3d
return-object v0
:cond_3e
const/4 v0, 0x0
goto :goto_3d
.end method