.class  Landroid/support/v4/app/ah;
.super Ljava/lang/Object;
.source "NotificationCompatJellybean.java"
.field private static final a:Ljava/lang/Object;
.field private static b:Ljava/lang/reflect/Field;
.field private static c:Z
.field private static final d:Ljava/lang/Object;
.method static constructor <clinit>()V
.registers 1
new-instance v0, Ljava/lang/Object;
invoke-direct {v0}, Ljava/lang/Object;-><init>()V
sput-object v0, Landroid/support/v4/app/ah;->a:Ljava/lang/Object;
new-instance v0, Ljava/lang/Object;
invoke-direct {v0}, Ljava/lang/Object;-><init>()V
sput-object v0, Landroid/support/v4/app/ah;->d:Ljava/lang/Object;
return-void
.end method
.method public static a(Landroid/app/Notification$Builder;Landroid/support/v4/app/ad$a;)Landroid/os/Bundle;
.registers 5
invoke-virtual {p1}, Landroid/support/v4/app/ad$a;->a()I
move-result v0
invoke-virtual {p1}, Landroid/support/v4/app/ad$a;->b()Ljava/lang/CharSequence;
move-result-object v1
invoke-virtual {p1}, Landroid/support/v4/app/ad$a;->c()Landroid/app/PendingIntent;
move-result-object v2
invoke-virtual {p0, v0, v1, v2}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
new-instance v0, Landroid/os/Bundle;
invoke-virtual {p1}, Landroid/support/v4/app/ad$a;->d()Landroid/os/Bundle;
move-result-object v1
invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V
invoke-virtual {p1}, Landroid/support/v4/app/ad$a;->f()[Landroid/support/v4/app/al$a;
move-result-object v1
if-eqz v1, :cond_2b
const-string v1, "android.support.remoteInputs"
invoke-virtual {p1}, Landroid/support/v4/app/ad$a;->f()[Landroid/support/v4/app/al$a;
move-result-object v2
invoke-static {v2}, Landroid/support/v4/app/am;->a([Landroid/support/v4/app/al$a;)[Landroid/os/Bundle;
move-result-object v2
invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
:cond_2b
return-object v0
.end method
.method public static a(Landroid/app/Notification;)Landroid/os/Bundle;
.registers 6
const/4 v1, 0x0
sget-object v2, Landroid/support/v4/app/ah;->a:Ljava/lang/Object;
monitor-enter v2
:try_start_4
sget-boolean v0, Landroid/support/v4/app/ah;->c:Z
if-eqz v0, :cond_b
monitor-exit v2
:try_end_9
.catchall {:try_start_4 .. :try_end_9} :catchall_4c
move-object v0, v1
:goto_a
return-object v0
:cond_b
:try_start_b
sget-object v0, Landroid/support/v4/app/ah;->b:Ljava/lang/reflect/Field;
if-nez v0, :cond_36
const-class v0, Landroid/app/Notification;
const-string v3, "extras"
invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
move-result-object v0
const-class v3, Landroid/os/Bundle;
invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;
move-result-object v4
invoke-virtual {v3, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
move-result v3
if-nez v3, :cond_30
const-string v0, "NotificationCompat"
const-string v3, "Notification.extras field is not of type Bundle"
invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const/4 v0, 0x1
sput-boolean v0, Landroid/support/v4/app/ah;->c:Z
:try_end_2d
.catchall {:try_start_b .. :try_end_2d} :catchall_4c
.catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_2d} :catch_4f
.catch Ljava/lang/NoSuchFieldException; {:try_start_b .. :try_end_2d} :catch_5d
:try_start_2d
monitor-exit v2
:try_end_2e
.catchall {:try_start_2d .. :try_end_2e} :catchall_4c
move-object v0, v1
goto :goto_a
:cond_30
const/4 v3, 0x1
:try_start_31
invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V
sput-object v0, Landroid/support/v4/app/ah;->b:Ljava/lang/reflect/Field;
:cond_36
sget-object v0, Landroid/support/v4/app/ah;->b:Ljava/lang/reflect/Field;
invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/os/Bundle;
if-nez v0, :cond_4a
new-instance v0, Landroid/os/Bundle;
invoke-direct {v0}, Landroid/os/Bundle;-><init>()V
sget-object v3, Landroid/support/v4/app/ah;->b:Ljava/lang/reflect/Field;
invoke-virtual {v3, p0, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
:try_end_4a
.catchall {:try_start_31 .. :try_end_4a} :catchall_4c
.catch Ljava/lang/IllegalAccessException; {:try_start_31 .. :try_end_4a} :catch_4f
.catch Ljava/lang/NoSuchFieldException; {:try_start_31 .. :try_end_4a} :catch_5d
:try_start_4a
:cond_4a
monitor-exit v2
goto :goto_a
:catchall_4c
move-exception v0
monitor-exit v2
:try_end_4e
.catchall {:try_start_4a .. :try_end_4e} :catchall_4c
throw v0
:catch_4f
move-exception v0
:try_start_50
const-string v3, "NotificationCompat"
const-string v4, "Unable to access notification extras"
invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
:goto_57
const/4 v0, 0x1
sput-boolean v0, Landroid/support/v4/app/ah;->c:Z
monitor-exit v2
move-object v0, v1
goto :goto_a
:catch_5d
move-exception v0
const-string v3, "NotificationCompat"
const-string v4, "Unable to access notification extras"
invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
:try_end_65
.catchall {:try_start_50 .. :try_end_65} :catchall_4c
goto :goto_57
.end method
.method public static a(Ljava/util/List;)Landroid/util/SparseArray;
.registers 5
const/4 v1, 0x0
const/4 v0, 0x0
invoke-interface {p0}, Ljava/util/List;->size()I
move-result v3
move v2, v0
:goto_7
if-ge v2, v3, :cond_1f
invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/os/Bundle;
if-eqz v0, :cond_1b
if-nez v1, :cond_18
new-instance v1, Landroid/util/SparseArray;
invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V
:cond_18
invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
:cond_1b
add-int/lit8 v0, v2, 0x1
move v2, v0
goto :goto_7
:cond_1f
return-object v1
.end method
.method public static a(Landroid/support/v4/app/z;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V
.registers 9
new-instance v0, Landroid/app/Notification$BigPictureStyle;
invoke-interface {p0}, Landroid/support/v4/app/z;->a()Landroid/app/Notification$Builder;
move-result-object v1
invoke-direct {v0, v1}, Landroid/app/Notification$BigPictureStyle;-><init>(Landroid/app/Notification$Builder;)V
invoke-virtual {v0, p1}, Landroid/app/Notification$BigPictureStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;
move-result-object v0
invoke-virtual {v0, p4}, Landroid/app/Notification$BigPictureStyle;->bigPicture(Landroid/graphics/Bitmap;)Landroid/app/Notification$BigPictureStyle;
move-result-object v0
if-eqz p6, :cond_16
invoke-virtual {v0, p5}, Landroid/app/Notification$BigPictureStyle;->bigLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$BigPictureStyle;
:cond_16
if-eqz p2, :cond_1b
invoke-virtual {v0, p3}, Landroid/app/Notification$BigPictureStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;
:cond_1b
return-void
.end method
.method public static a(Landroid/support/v4/app/z;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V
.registers 7
new-instance v0, Landroid/app/Notification$BigTextStyle;
invoke-interface {p0}, Landroid/support/v4/app/z;->a()Landroid/app/Notification$Builder;
move-result-object v1
invoke-direct {v0, v1}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V
invoke-virtual {v0, p1}, Landroid/app/Notification$BigTextStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;
move-result-object v0
invoke-virtual {v0, p4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;
move-result-object v0
if-eqz p2, :cond_16
invoke-virtual {v0, p3}, Landroid/app/Notification$BigTextStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;
:cond_16
return-void
.end method
.method public static a(Landroid/support/v4/app/z;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V
.registers 8
new-instance v0, Landroid/app/Notification$InboxStyle;
invoke-interface {p0}, Landroid/support/v4/app/z;->a()Landroid/app/Notification$Builder;
move-result-object v1
invoke-direct {v0, v1}, Landroid/app/Notification$InboxStyle;-><init>(Landroid/app/Notification$Builder;)V
invoke-virtual {v0, p1}, Landroid/app/Notification$InboxStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;
move-result-object v1
if-eqz p2, :cond_12
invoke-virtual {v1, p3}, Landroid/app/Notification$InboxStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;
:cond_12
invoke-virtual {p4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
move-result-object v2
:goto_16
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_26
invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/CharSequence;
invoke-virtual {v1, v0}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;
goto :goto_16
:cond_26
return-void
.end method