.class public Landroid/support/v4/app/aa$d;
.super Ljava/lang/Object;
.source "NotificationCompat.java"
.field  A:Landroid/app/Notification;
.field public B:Landroid/app/Notification;
.field public C:Ljava/util/ArrayList;
.field public a:Landroid/content/Context;
.field public b:Ljava/lang/CharSequence;
.field public c:Ljava/lang/CharSequence;
.field  d:Landroid/app/PendingIntent;
.field  e:Landroid/app/PendingIntent;
.field  f:Landroid/widget/RemoteViews;
.field public g:Landroid/graphics/Bitmap;
.field public h:Ljava/lang/CharSequence;
.field public i:I
.field  j:I
.field  k:Z
.field public l:Z
.field public m:Landroid/support/v4/app/aa$p;
.field public n:Ljava/lang/CharSequence;
.field  o:I
.field  p:I
.field  q:Z
.field  r:Ljava/lang/String;
.field  s:Z
.field  t:Ljava/lang/String;
.field public u:Ljava/util/ArrayList;
.field  v:Z
.field  w:Ljava/lang/String;
.field  x:Landroid/os/Bundle;
.field  y:I
.field  z:I
.method public constructor <init>(Landroid/content/Context;)V
.registers 7
const/4 v4, 0x0
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v4/app/aa$d;->k:Z
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Landroid/support/v4/app/aa$d;->u:Ljava/util/ArrayList;
iput-boolean v4, p0, Landroid/support/v4/app/aa$d;->v:Z
iput v4, p0, Landroid/support/v4/app/aa$d;->y:I
iput v4, p0, Landroid/support/v4/app/aa$d;->z:I
new-instance v0, Landroid/app/Notification;
invoke-direct {v0}, Landroid/app/Notification;-><init>()V
iput-object v0, p0, Landroid/support/v4/app/aa$d;->B:Landroid/app/Notification;
iput-object p1, p0, Landroid/support/v4/app/aa$d;->a:Landroid/content/Context;
iget-object v0, p0, Landroid/support/v4/app/aa$d;->B:Landroid/app/Notification;
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v2
iput-wide v2, v0, Landroid/app/Notification;->when:J
iget-object v0, p0, Landroid/support/v4/app/aa$d;->B:Landroid/app/Notification;
const/4 v1, -0x1
iput v1, v0, Landroid/app/Notification;->audioStreamType:I
iput v4, p0, Landroid/support/v4/app/aa$d;->j:I
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Landroid/support/v4/app/aa$d;->C:Ljava/util/ArrayList;
return-void
.end method
.method private a(IZ)V
.registers 6
if-eqz p2, :cond_a
iget-object v0, p0, Landroid/support/v4/app/aa$d;->B:Landroid/app/Notification;
iget v1, v0, Landroid/app/Notification;->flags:I
or-int/2addr v1, p1
iput v1, v0, Landroid/app/Notification;->flags:I
:goto_9
return-void
:cond_a
iget-object v0, p0, Landroid/support/v4/app/aa$d;->B:Landroid/app/Notification;
iget v1, v0, Landroid/app/Notification;->flags:I
xor-int/lit8 v2, p1, -0x1
and-int/2addr v1, v2
iput v1, v0, Landroid/app/Notification;->flags:I
goto :goto_9
.end method
.method protected static d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
.registers 3
const/16 v1, 0x1400
if-nez p0, :cond_5
:goto_4
:cond_4
return-object p0
:cond_5
invoke-interface {p0}, Ljava/lang/CharSequence;->length()I
move-result v0
if-le v0, v1, :cond_4
const/4 v0, 0x0
invoke-interface {p0, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;
move-result-object p0
goto :goto_4
.end method
.method public a()Landroid/app/Notification;
.registers 3
invoke-static {}, Landroid/support/v4/app/aa;->a()Landroid/support/v4/app/aa$g;
move-result-object v0
invoke-virtual {p0}, Landroid/support/v4/app/aa$d;->b()Landroid/support/v4/app/aa$e;
move-result-object v1
invoke-interface {v0, p0, v1}, Landroid/support/v4/app/aa$g;->a(Landroid/support/v4/app/aa$d;Landroid/support/v4/app/aa$e;)Landroid/app/Notification;
move-result-object v0
return-object v0
.end method
.method public a(I)Landroid/support/v4/app/aa$d;
.registers 3
iget-object v0, p0, Landroid/support/v4/app/aa$d;->B:Landroid/app/Notification;
iput p1, v0, Landroid/app/Notification;->icon:I
return-object p0
.end method
.method public a(J)Landroid/support/v4/app/aa$d;
.registers 4
iget-object v0, p0, Landroid/support/v4/app/aa$d;->B:Landroid/app/Notification;
iput-wide p1, v0, Landroid/app/Notification;->when:J
return-object p0
.end method
.method public a(Landroid/app/PendingIntent;)Landroid/support/v4/app/aa$d;
.registers 2
iput-object p1, p0, Landroid/support/v4/app/aa$d;->d:Landroid/app/PendingIntent;
return-object p0
.end method
.method public a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/aa$d;
.registers 2
iput-object p1, p0, Landroid/support/v4/app/aa$d;->g:Landroid/graphics/Bitmap;
return-object p0
.end method
.method public a(Landroid/net/Uri;)Landroid/support/v4/app/aa$d;
.registers 4
iget-object v0, p0, Landroid/support/v4/app/aa$d;->B:Landroid/app/Notification;
iput-object p1, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;
iget-object v0, p0, Landroid/support/v4/app/aa$d;->B:Landroid/app/Notification;
const/4 v1, -0x1
iput v1, v0, Landroid/app/Notification;->audioStreamType:I
return-object p0
.end method
.method public a(Landroid/support/v4/app/aa$p;)Landroid/support/v4/app/aa$d;
.registers 3
iget-object v0, p0, Landroid/support/v4/app/aa$d;->m:Landroid/support/v4/app/aa$p;
if-eq v0, p1, :cond_f
iput-object p1, p0, Landroid/support/v4/app/aa$d;->m:Landroid/support/v4/app/aa$p;
iget-object v0, p0, Landroid/support/v4/app/aa$d;->m:Landroid/support/v4/app/aa$p;
if-eqz v0, :cond_f
iget-object v0, p0, Landroid/support/v4/app/aa$d;->m:Landroid/support/v4/app/aa$p;
invoke-virtual {v0, p0}, Landroid/support/v4/app/aa$p;->a(Landroid/support/v4/app/aa$d;)V
:cond_f
return-object p0
.end method
.method public a(Ljava/lang/CharSequence;)Landroid/support/v4/app/aa$d;
.registers 3
invoke-static {p1}, Landroid/support/v4/app/aa$d;->d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
move-result-object v0
iput-object v0, p0, Landroid/support/v4/app/aa$d;->b:Ljava/lang/CharSequence;
return-object p0
.end method
.method public a(Z)Landroid/support/v4/app/aa$d;
.registers 3
const/16 v0, 0x10
invoke-direct {p0, v0, p1}, Landroid/support/v4/app/aa$d;->a(IZ)V
return-object p0
.end method
.method public b(I)Landroid/support/v4/app/aa$d;
.registers 2
iput p1, p0, Landroid/support/v4/app/aa$d;->y:I
return-object p0
.end method
.method public b(Landroid/app/PendingIntent;)Landroid/support/v4/app/aa$d;
.registers 3
iget-object v0, p0, Landroid/support/v4/app/aa$d;->B:Landroid/app/Notification;
iput-object p1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;
return-object p0
.end method
.method public b(Ljava/lang/CharSequence;)Landroid/support/v4/app/aa$d;
.registers 3
invoke-static {p1}, Landroid/support/v4/app/aa$d;->d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
move-result-object v0
iput-object v0, p0, Landroid/support/v4/app/aa$d;->c:Ljava/lang/CharSequence;
return-object p0
.end method
.method protected b()Landroid/support/v4/app/aa$e;
.registers 2
new-instance v0, Landroid/support/v4/app/aa$e;
invoke-direct {v0}, Landroid/support/v4/app/aa$e;-><init>()V
return-object v0
.end method
.method public c(Ljava/lang/CharSequence;)Landroid/support/v4/app/aa$d;
.registers 4
iget-object v0, p0, Landroid/support/v4/app/aa$d;->B:Landroid/app/Notification;
invoke-static {p1}, Landroid/support/v4/app/aa$d;->d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
move-result-object v1
iput-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;
return-object p0
.end method