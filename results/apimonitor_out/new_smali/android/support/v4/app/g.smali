.class final Landroid/support/v4/app/g;
.super Landroid/support/v4/app/r;
.source "BackStackRecord.java"
.implements Ljava/lang/Runnable;
.field static final a:Z
.field final b:Landroid/support/v4/app/q;
.field  c:Landroid/support/v4/app/g$a;
.field  d:Landroid/support/v4/app/g$a;
.field  e:I
.field  f:I
.field  g:I
.field  h:I
.field  i:I
.field  j:I
.field  k:I
.field  l:Z
.field  m:Z
.field  n:Ljava/lang/String;
.field  o:Z
.field  p:I
.field  q:I
.field  r:Ljava/lang/CharSequence;
.field  s:I
.field  t:Ljava/lang/CharSequence;
.field  u:Ljava/util/ArrayList;
.field  v:Ljava/util/ArrayList;
.method static constructor <clinit>()V
.registers 2
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x15
if-lt v0, v1, :cond_a
const/4 v0, 0x1
:goto_7
sput-boolean v0, Landroid/support/v4/app/g;->a:Z
return-void
:cond_a
const/4 v0, 0x0
goto :goto_7
.end method
.method public constructor <init>(Landroid/support/v4/app/q;)V
.registers 3
invoke-direct {p0}, Landroid/support/v4/app/r;-><init>()V
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v4/app/g;->m:Z
const/4 v0, -0x1
iput v0, p0, Landroid/support/v4/app/g;->p:I
iput-object p1, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
return-void
.end method
.method private a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)Landroid/support/v4/app/g$b;
.registers 14
const/4 v9, 0x1
const/4 v7, 0x0
new-instance v2, Landroid/support/v4/app/g$b;
invoke-direct {v2, p0}, Landroid/support/v4/app/g$b;-><init>(Landroid/support/v4/app/g;)V
new-instance v0, Landroid/view/View;
iget-object v1, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v1, v1, Landroid/support/v4/app/q;->o:Landroid/support/v4/app/o;
invoke-virtual {v1}, Landroid/support/v4/app/o;->g()Landroid/content/Context;
move-result-object v1
invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V
iput-object v0, v2, Landroid/support/v4/app/g$b;->d:Landroid/view/View;
move v6, v7
move v8, v7
:goto_18
invoke-virtual {p1}, Landroid/util/SparseArray;->size()I
move-result v0
if-ge v6, v0, :cond_32
invoke-virtual {p1, v6}, Landroid/util/SparseArray;->keyAt(I)I
move-result v1
move-object v0, p0
move v3, p3
move-object v4, p1
move-object v5, p2
invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/g;->a(ILandroid/support/v4/app/g$b;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z
move-result v0
if-eqz v0, :cond_54
move v1, v9
:goto_2d
add-int/lit8 v0, v6, 0x1
move v6, v0
move v8, v1
goto :goto_18
:goto_32
:cond_32
invoke-virtual {p2}, Landroid/util/SparseArray;->size()I
move-result v0
if-ge v7, v0, :cond_50
invoke-virtual {p2, v7}, Landroid/util/SparseArray;->keyAt(I)I
move-result v1
invoke-virtual {p1, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
move-result-object v0
if-nez v0, :cond_4d
move-object v0, p0
move v3, p3
move-object v4, p1
move-object v5, p2
invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/g;->a(ILandroid/support/v4/app/g$b;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z
move-result v0
if-eqz v0, :cond_4d
move v8, v9
:cond_4d
add-int/lit8 v7, v7, 0x1
goto :goto_32
:cond_50
if-nez v8, :cond_53
const/4 v2, 0x0
:cond_53
return-object v2
:cond_54
move v1, v8
goto :goto_2d
.end method
.method private a(Landroid/support/v4/app/g$b;Landroid/support/v4/app/Fragment;Z)Landroid/support/v4/e/a;
.registers 8
const/4 v3, 0x0
new-instance v0, Landroid/support/v4/e/a;
invoke-direct {v0}, Landroid/support/v4/e/a;-><init>()V
iget-object v1, p0, Landroid/support/v4/app/g;->u:Ljava/util/ArrayList;
if-eqz v1, :cond_18
invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->n()Landroid/view/View;
move-result-object v1
invoke-static {v0, v1}, Landroid/support/v4/app/s;->a(Ljava/util/Map;Landroid/view/View;)V
if-eqz p3, :cond_29
iget-object v1, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Landroid/support/v4/e/a;->a(Ljava/util/Collection;)Z
:cond_18
:goto_18
if-eqz p3, :cond_32
iget-object v1, p2, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/an;
if-eqz v1, :cond_25
iget-object v1, p2, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/an;
iget-object v2, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/an;->a(Ljava/util/List;Ljava/util/Map;)V
:cond_25
invoke-direct {p0, p1, v0, v3}, Landroid/support/v4/app/g;->a(Landroid/support/v4/app/g$b;Landroid/support/v4/e/a;Z)V
:goto_28
return-object v0
:cond_29
iget-object v1, p0, Landroid/support/v4/app/g;->u:Ljava/util/ArrayList;
iget-object v2, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
invoke-static {v1, v2, v0}, Landroid/support/v4/app/g;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/e/a;)Landroid/support/v4/e/a;
move-result-object v0
goto :goto_18
:cond_32
iget-object v1, p2, Landroid/support/v4/app/Fragment;->ah:Landroid/support/v4/app/an;
if-eqz v1, :cond_3d
iget-object v1, p2, Landroid/support/v4/app/Fragment;->ah:Landroid/support/v4/app/an;
iget-object v2, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/an;->a(Ljava/util/List;Ljava/util/Map;)V
:cond_3d
invoke-direct {p0, p1, v0, v3}, Landroid/support/v4/app/g;->b(Landroid/support/v4/app/g$b;Landroid/support/v4/e/a;Z)V
goto :goto_28
.end method
.method private a(Landroid/support/v4/app/g$b;ZLandroid/support/v4/app/Fragment;)Landroid/support/v4/e/a;
.registers 8
const/4 v3, 0x1
invoke-direct {p0, p1, p3, p2}, Landroid/support/v4/app/g;->b(Landroid/support/v4/app/g$b;Landroid/support/v4/app/Fragment;Z)Landroid/support/v4/e/a;
move-result-object v0
if-eqz p2, :cond_16
iget-object v1, p3, Landroid/support/v4/app/Fragment;->ah:Landroid/support/v4/app/an;
if-eqz v1, :cond_12
iget-object v1, p3, Landroid/support/v4/app/Fragment;->ah:Landroid/support/v4/app/an;
iget-object v2, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/an;->a(Ljava/util/List;Ljava/util/Map;)V
:cond_12
invoke-direct {p0, p1, v0, v3}, Landroid/support/v4/app/g;->a(Landroid/support/v4/app/g$b;Landroid/support/v4/e/a;Z)V
:goto_15
return-object v0
:cond_16
iget-object v1, p3, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/an;
if-eqz v1, :cond_21
iget-object v1, p3, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/an;
iget-object v2, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/an;->a(Ljava/util/List;Ljava/util/Map;)V
:cond_21
invoke-direct {p0, p1, v0, v3}, Landroid/support/v4/app/g;->b(Landroid/support/v4/app/g$b;Landroid/support/v4/e/a;Z)V
goto :goto_15
.end method
.method static synthetic a(Landroid/support/v4/app/g;Landroid/support/v4/app/g$b;ZLandroid/support/v4/app/Fragment;)Landroid/support/v4/e/a;
.registers 5
invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/app/g;->a(Landroid/support/v4/app/g$b;ZLandroid/support/v4/app/Fragment;)Landroid/support/v4/e/a;
move-result-object v0
return-object v0
.end method
.method private static a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/e/a;)Landroid/support/v4/e/a;
.registers 8
invoke-virtual {p2}, Landroid/support/v4/e/a;->isEmpty()Z
move-result v0
if-eqz v0, :cond_7
:goto_6
return-object p2
:cond_7
new-instance v1, Landroid/support/v4/e/a;
invoke-direct {v1}, Landroid/support/v4/e/a;-><init>()V
invoke-virtual {p0}, Ljava/util/ArrayList;->size()I
move-result v3
const/4 v0, 0x0
move v2, v0
:goto_12
if-ge v2, v3, :cond_2b
invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
invoke-virtual {p2, v0}, Landroid/support/v4/e/a;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_27
invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v4
invoke-virtual {v1, v4, v0}, Landroid/support/v4/e/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_27
add-int/lit8 v0, v2, 0x1
move v2, v0
goto :goto_12
:cond_2b
move-object p2, v1
goto :goto_6
.end method
.method private static a(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
.registers 4
if-eqz p0, :cond_4
if-nez p1, :cond_6
:cond_4
const/4 v0, 0x0
:goto_5
return-object v0
:cond_6
if-eqz p2, :cond_11
invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->y()Ljava/lang/Object;
move-result-object v0
:goto_c
invoke-static {v0}, Landroid/support/v4/app/s;->b(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
goto :goto_5
:cond_11
invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->x()Ljava/lang/Object;
move-result-object v0
goto :goto_c
.end method
.method private static a(Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
.registers 3
if-nez p0, :cond_4
const/4 v0, 0x0
:goto_3
return-object v0
:cond_4
if-eqz p1, :cond_f
invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->w()Ljava/lang/Object;
move-result-object v0
:goto_a
invoke-static {v0}, Landroid/support/v4/app/s;->a(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
goto :goto_3
:cond_f
invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->t()Ljava/lang/Object;
move-result-object v0
goto :goto_a
.end method
.method private static a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;Ljava/util/ArrayList;Landroid/support/v4/e/a;Landroid/view/View;)Ljava/lang/Object;
.registers 6
if-eqz p0, :cond_a
invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->n()Landroid/view/View;
move-result-object v0
invoke-static {p0, v0, p2, p3, p4}, Landroid/support/v4/app/s;->a(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList;Ljava/util/Map;Landroid/view/View;)Ljava/lang/Object;
move-result-object p0
:cond_a
return-object p0
.end method
.method private a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
.registers 8
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iput-object v0, p2, Landroid/support/v4/app/Fragment;->B:Landroid/support/v4/app/q;
if-eqz p3, :cond_43
iget-object v0, p2, Landroid/support/v4/app/Fragment;->H:Ljava/lang/String;
if-eqz v0, :cond_41
iget-object v0, p2, Landroid/support/v4/app/Fragment;->H:Ljava/lang/String;
invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-nez v0, :cond_41
new-instance v0, Ljava/lang/IllegalStateException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Can\'t change tag of fragment "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, ": was "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p2, Landroid/support/v4/app/Fragment;->H:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, " now "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_41
iput-object p3, p2, Landroid/support/v4/app/Fragment;->H:Ljava/lang/String;
:cond_43
if-eqz p1, :cond_80
iget v0, p2, Landroid/support/v4/app/Fragment;->F:I
if-eqz v0, :cond_7c
iget v0, p2, Landroid/support/v4/app/Fragment;->F:I
if-eq v0, p1, :cond_7c
new-instance v0, Ljava/lang/IllegalStateException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Can\'t change container ID of fragment "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, ": was "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget v2, p2, Landroid/support/v4/app/Fragment;->F:I
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, " now "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_7c
iput p1, p2, Landroid/support/v4/app/Fragment;->F:I
iput p1, p2, Landroid/support/v4/app/Fragment;->G:I
:cond_80
new-instance v0, Landroid/support/v4/app/g$a;
invoke-direct {v0}, Landroid/support/v4/app/g$a;-><init>()V
iput p4, v0, Landroid/support/v4/app/g$a;->c:I
iput-object p2, v0, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-virtual {p0, v0}, Landroid/support/v4/app/g;->a(Landroid/support/v4/app/g$a;)V
return-void
.end method
.method private a(Landroid/support/v4/app/g$b;ILjava/lang/Object;)V
.registers 9
const/4 v2, 0x0
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v0, v0, Landroid/support/v4/app/q;->g:Ljava/util/ArrayList;
if-eqz v0, :cond_54
move v1, v2
:goto_8
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v0, v0, Landroid/support/v4/app/q;->g:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
if-ge v1, v0, :cond_54
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v0, v0, Landroid/support/v4/app/q;->g:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/app/Fragment;
iget-object v3, v0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;
if-eqz v3, :cond_43
iget-object v3, v0, Landroid/support/v4/app/Fragment;->Q:Landroid/view/ViewGroup;
if-eqz v3, :cond_43
iget v3, v0, Landroid/support/v4/app/Fragment;->G:I
if-ne v3, p2, :cond_43
iget-boolean v3, v0, Landroid/support/v4/app/Fragment;->I:Z
if-eqz v3, :cond_47
iget-object v3, p1, Landroid/support/v4/app/g$b;->b:Ljava/util/ArrayList;
iget-object v4, v0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;
invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
move-result v3
if-nez v3, :cond_43
iget-object v3, v0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;
const/4 v4, 0x1
invoke-static {p3, v3, v4}, Landroid/support/v4/app/s;->a(Ljava/lang/Object;Landroid/view/View;Z)V
iget-object v3, p1, Landroid/support/v4/app/g$b;->b:Ljava/util/ArrayList;
iget-object v0, v0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;
invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
:goto_43
:cond_43
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_8
:cond_47
iget-object v3, v0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;
invoke-static {p3, v3, v2}, Landroid/support/v4/app/s;->a(Ljava/lang/Object;Landroid/view/View;Z)V
iget-object v3, p1, Landroid/support/v4/app/g$b;->b:Ljava/util/ArrayList;
iget-object v0, v0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;
invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
goto :goto_43
:cond_54
return-void
.end method
.method private a(Landroid/support/v4/app/g$b;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLandroid/support/v4/e/a;)V
.registers 10
if-eqz p4, :cond_1d
iget-object v0, p3, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/an;
:goto_4
if-eqz v0, :cond_1c
new-instance v1, Ljava/util/ArrayList;
invoke-virtual {p5}, Landroid/support/v4/e/a;->keySet()Ljava/util/Set;
move-result-object v2
invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
new-instance v2, Ljava/util/ArrayList;
invoke-virtual {p5}, Landroid/support/v4/e/a;->values()Ljava/util/Collection;
move-result-object v3
invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
const/4 v3, 0x0
invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/an;->b(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
:cond_1c
return-void
:cond_1d
iget-object v0, p2, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/an;
goto :goto_4
.end method
.method private a(Landroid/support/v4/app/g$b;Landroid/support/v4/e/a;Z)V
.registers 9
const/4 v0, 0x0
iget-object v1, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
if-nez v1, :cond_30
move v2, v0
:goto_6
move v3, v0
:goto_7
if-ge v3, v2, :cond_3e
iget-object v0, p0, Landroid/support/v4/app/g;->u:Ljava/util/ArrayList;
invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
iget-object v1, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v1
check-cast v1, Ljava/lang/String;
invoke-virtual {p2, v1}, Landroid/support/v4/e/a;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v1
check-cast v1, Landroid/view/View;
if-eqz v1, :cond_2c
invoke-static {v1}, Landroid/support/v4/app/s;->a(Landroid/view/View;)Ljava/lang/String;
move-result-object v1
if-eqz p3, :cond_38
iget-object v4, p1, Landroid/support/v4/app/g$b;->a:Landroid/support/v4/e/a;
invoke-static {v4, v0, v1}, Landroid/support/v4/app/g;->a(Landroid/support/v4/e/a;Ljava/lang/String;Ljava/lang/String;)V
:goto_2c
:cond_2c
add-int/lit8 v0, v3, 0x1
move v3, v0
goto :goto_7
:cond_30
iget-object v1, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
move-result v1
move v2, v1
goto :goto_6
:cond_38
iget-object v4, p1, Landroid/support/v4/app/g$b;->a:Landroid/support/v4/e/a;
invoke-static {v4, v1, v0}, Landroid/support/v4/app/g;->a(Landroid/support/v4/e/a;Ljava/lang/String;Ljava/lang/String;)V
goto :goto_2c
:cond_3e
return-void
.end method
.method private a(Landroid/support/v4/app/g$b;Landroid/view/View;Ljava/lang/Object;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLjava/util/ArrayList;)V
.registers 18
invoke-virtual {p2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;
move-result-object v9
new-instance v0, Landroid/support/v4/app/g$2;
move-object v1, p0
move-object v2, p2
move-object v3, p3
move-object/from16 v4, p7
move-object v5, p1
move/from16 v6, p6
move-object v7, p4
move-object v8, p5
invoke-direct/range {v0 .. v8}, Landroid/support/v4/app/g$2;-><init>(Landroid/support/v4/app/g;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Landroid/support/v4/app/g$b;ZLandroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;)V
invoke-virtual {v9, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
return-void
.end method
.method private static a(Landroid/support/v4/app/g$b;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
.registers 7
if-eqz p1, :cond_1f
const/4 v0, 0x0
move v2, v0
:goto_4
invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
move-result v0
if-ge v2, v0, :cond_1f
invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v1
check-cast v1, Ljava/lang/String;
iget-object v3, p0, Landroid/support/v4/app/g$b;->a:Landroid/support/v4/e/a;
invoke-static {v3, v0, v1}, Landroid/support/v4/app/g;->a(Landroid/support/v4/e/a;Ljava/lang/String;Ljava/lang/String;)V
add-int/lit8 v0, v2, 0x1
move v2, v0
goto :goto_4
:cond_1f
return-void
.end method
.method static synthetic a(Landroid/support/v4/app/g;Landroid/support/v4/app/g$b;ILjava/lang/Object;)V
.registers 4
invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/app/g;->a(Landroid/support/v4/app/g$b;ILjava/lang/Object;)V
return-void
.end method
.method static synthetic a(Landroid/support/v4/app/g;Landroid/support/v4/app/g$b;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLandroid/support/v4/e/a;)V
.registers 6
invoke-direct/range {p0 .. p5}, Landroid/support/v4/app/g;->a(Landroid/support/v4/app/g$b;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLandroid/support/v4/e/a;)V
return-void
.end method
.method static synthetic a(Landroid/support/v4/app/g;Landroid/support/v4/e/a;Landroid/support/v4/app/g$b;)V
.registers 3
invoke-direct {p0, p1, p2}, Landroid/support/v4/app/g;->a(Landroid/support/v4/e/a;Landroid/support/v4/app/g$b;)V
return-void
.end method
.method private a(Landroid/support/v4/e/a;Landroid/support/v4/app/g$b;)V
.registers 5
iget-object v0, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
if-eqz v0, :cond_1d
invoke-virtual {p1}, Landroid/support/v4/e/a;->isEmpty()Z
move-result v0
if-nez v0, :cond_1d
iget-object v0, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
const/4 v1, 0x0
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
invoke-virtual {p1, v0}, Landroid/support/v4/e/a;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_1d
iget-object v1, p2, Landroid/support/v4/app/g$b;->c:Landroid/support/v4/app/s$a;
iput-object v0, v1, Landroid/support/v4/app/s$a;->a:Landroid/view/View;
:cond_1d
return-void
.end method
.method private static a(Landroid/support/v4/e/a;Ljava/lang/String;Ljava/lang/String;)V
.registers 5
if-eqz p1, :cond_18
if-eqz p2, :cond_18
const/4 v0, 0x0
:goto_5
invoke-virtual {p0}, Landroid/support/v4/e/a;->size()I
move-result v1
if-ge v0, v1, :cond_1c
invoke-virtual {p0, v0}, Landroid/support/v4/e/a;->c(I)Ljava/lang/Object;
move-result-object v1
invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_19
invoke-virtual {p0, v0, p2}, Landroid/support/v4/e/a;->a(ILjava/lang/Object;)Ljava/lang/Object;
:goto_18
:cond_18
return-void
:cond_19
add-int/lit8 v0, v0, 0x1
goto :goto_5
:cond_1c
invoke-virtual {p0, p1, p2}, Landroid/support/v4/e/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
goto :goto_18
.end method
.method private static a(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
.registers 5
if-eqz p2, :cond_2a
iget v0, p2, Landroid/support/v4/app/Fragment;->G:I
if-eqz v0, :cond_2a
invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->m()Z
move-result v1
if-nez v1, :cond_2a
invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->l()Z
move-result v1
if-eqz v1, :cond_21
invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->n()Landroid/view/View;
move-result-object v1
if-eqz v1, :cond_21
invoke-virtual {p0, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
move-result-object v1
if-nez v1, :cond_21
invoke-virtual {p0, v0, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
:cond_21
invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
move-result-object v1
if-ne v1, p2, :cond_2a
invoke-virtual {p1, v0}, Landroid/util/SparseArray;->remove(I)V
:cond_2a
return-void
.end method
.method private a(Landroid/view/View;Landroid/support/v4/app/g$b;ILjava/lang/Object;)V
.registers 12
invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;
move-result-object v6
new-instance v0, Landroid/support/v4/app/g$3;
move-object v1, p0
move-object v2, p1
move-object v3, p2
move v4, p3
move-object v5, p4
invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/g$3;-><init>(Landroid/support/v4/app/g;Landroid/view/View;Landroid/support/v4/app/g$b;ILjava/lang/Object;)V
invoke-virtual {v6, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
return-void
.end method
.method private a(ILandroid/support/v4/app/g$b;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z
.registers 39
move-object/from16 v0, p0
iget-object v4, v0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v4, v4, Landroid/support/v4/app/q;->p:Landroid/support/v4/app/m;
move/from16 v0, p1
invoke-virtual {v4, v0}, Landroid/support/v4/app/m;->a(I)Landroid/view/View;
move-result-object v6
check-cast v6, Landroid/view/ViewGroup;
if-nez v6, :cond_12
const/4 v4, 0x0
:goto_11
return v4
:cond_12
move-object/from16 v0, p5
move/from16 v1, p1
invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
move-result-object v8
check-cast v8, Landroid/support/v4/app/Fragment;
move-object/from16 v0, p4
move/from16 v1, p1
invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
move-result-object v9
check-cast v9, Landroid/support/v4/app/Fragment;
move/from16 v0, p3
invoke-static {v8, v0}, Landroid/support/v4/app/g;->a(Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
move-result-object v12
move/from16 v0, p3
invoke-static {v8, v9, v0}, Landroid/support/v4/app/g;->a(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
move-result-object v7
move/from16 v0, p3
invoke-static {v9, v0}, Landroid/support/v4/app/g;->b(Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
move-result-object v14
const/16 v20, 0x0
new-instance v11, Ljava/util/ArrayList;
invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V
if-eqz v7, :cond_82
move-object/from16 v0, p0
move-object/from16 v1, p2
move/from16 v2, p3
invoke-direct {v0, v1, v9, v2}, Landroid/support/v4/app/g;->a(Landroid/support/v4/app/g$b;Landroid/support/v4/app/Fragment;Z)Landroid/support/v4/e/a;
move-result-object v20
invoke-virtual/range {v20 .. v20}, Landroid/support/v4/e/a;->isEmpty()Z
move-result v4
if-eqz v4, :cond_5d
const/4 v7, 0x0
const/16 v20, 0x0
move-object v13, v7
:goto_55
if-nez v12, :cond_87
if-nez v13, :cond_87
if-nez v14, :cond_87
const/4 v4, 0x0
goto :goto_11
:cond_5d
if-eqz p3, :cond_84
iget-object v4, v9, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/an;
:goto_61
if-eqz v4, :cond_79
new-instance v5, Ljava/util/ArrayList;
invoke-virtual/range {v20 .. v20}, Landroid/support/v4/e/a;->keySet()Ljava/util/Set;
move-result-object v10
invoke-direct {v5, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
new-instance v10, Ljava/util/ArrayList;
invoke-virtual/range {v20 .. v20}, Landroid/support/v4/e/a;->values()Ljava/util/Collection;
move-result-object v13
invoke-direct {v10, v13}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
const/4 v13, 0x0
invoke-virtual {v4, v5, v10, v13}, Landroid/support/v4/app/an;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
:cond_79
move-object/from16 v4, p0
move-object/from16 v5, p2
move/from16 v10, p3
invoke-direct/range {v4 .. v11}, Landroid/support/v4/app/g;->a(Landroid/support/v4/app/g$b;Landroid/view/View;Ljava/lang/Object;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLjava/util/ArrayList;)V
:cond_82
move-object v13, v7
goto :goto_55
:cond_84
iget-object v4, v8, Landroid/support/v4/app/Fragment;->ag:Landroid/support/v4/app/an;
goto :goto_61
:cond_87
new-instance v27, Ljava/util/ArrayList;
invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V
move-object/from16 v0, p2
iget-object v4, v0, Landroid/support/v4/app/g$b;->d:Landroid/view/View;
move-object/from16 v0, v27
move-object/from16 v1, v20
invoke-static {v14, v9, v0, v1, v4}, Landroid/support/v4/app/g;->a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;Ljava/util/ArrayList;Landroid/support/v4/e/a;Landroid/view/View;)Ljava/lang/Object;
move-result-object v26
move-object/from16 v0, p0
iget-object v4, v0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
if-eqz v4, :cond_bf
if-eqz v20, :cond_bf
move-object/from16 v0, p0
iget-object v4, v0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
const/4 v5, 0x0
invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v4
move-object/from16 v0, v20
invoke-virtual {v0, v4}, Landroid/support/v4/e/a;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v4
check-cast v4, Landroid/view/View;
if-eqz v4, :cond_bf
if-eqz v26, :cond_ba
move-object/from16 v0, v26
invoke-static {v0, v4}, Landroid/support/v4/app/s;->a(Ljava/lang/Object;Landroid/view/View;)V
:cond_ba
if-eqz v13, :cond_bf
invoke-static {v13, v4}, Landroid/support/v4/app/s;->a(Ljava/lang/Object;Landroid/view/View;)V
:cond_bf
new-instance v15, Landroid/support/v4/app/g$1;
move-object/from16 v0, p0
invoke-direct {v15, v0, v8}, Landroid/support/v4/app/g$1;-><init>(Landroid/support/v4/app/g;Landroid/support/v4/app/Fragment;)V
new-instance v19, Ljava/util/ArrayList;
invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V
new-instance v21, Landroid/support/v4/e/a;
invoke-direct/range {v21 .. v21}, Landroid/support/v4/e/a;-><init>()V
const/4 v4, 0x1
if-eqz v8, :cond_d9
if-eqz p3, :cond_13e
invoke-virtual {v8}, Landroid/support/v4/app/Fragment;->A()Z
move-result v4
:cond_d9
:goto_d9
move-object/from16 v0, v26
invoke-static {v12, v0, v13, v4}, Landroid/support/v4/app/s;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
move-result-object v30
if-eqz v30, :cond_139
move-object/from16 v0, p2
iget-object v0, v0, Landroid/support/v4/app/g$b;->d:Landroid/view/View;
move-object/from16 v16, v0
move-object/from16 v0, p2
iget-object v0, v0, Landroid/support/v4/app/g$b;->c:Landroid/support/v4/app/s$a;
move-object/from16 v17, v0
move-object/from16 v0, p2
iget-object v0, v0, Landroid/support/v4/app/g$b;->a:Landroid/support/v4/e/a;
move-object/from16 v18, v0
move-object v14, v6
move-object/from16 v22, v11
invoke-static/range {v12 .. v22}, Landroid/support/v4/app/s;->a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;Landroid/support/v4/app/s$b;Landroid/view/View;Landroid/support/v4/app/s$a;Ljava/util/Map;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/Map;Ljava/util/ArrayList;)V
move-object/from16 v0, p0
move-object/from16 v1, p2
move/from16 v2, p1
move-object/from16 v3, v30
invoke-direct {v0, v6, v1, v2, v3}, Landroid/support/v4/app/g;->a(Landroid/view/View;Landroid/support/v4/app/g$b;ILjava/lang/Object;)V
move-object/from16 v0, p2
iget-object v4, v0, Landroid/support/v4/app/g$b;->d:Landroid/view/View;
const/4 v5, 0x1
move-object/from16 v0, v30
invoke-static {v0, v4, v5}, Landroid/support/v4/app/s;->a(Ljava/lang/Object;Landroid/view/View;Z)V
move-object/from16 v0, p0
move-object/from16 v1, p2
move/from16 v2, p1
move-object/from16 v3, v30
invoke-direct {v0, v1, v2, v3}, Landroid/support/v4/app/g;->a(Landroid/support/v4/app/g$b;ILjava/lang/Object;)V
move-object/from16 v0, v30
invoke-static {v6, v0}, Landroid/support/v4/app/s;->a(Landroid/view/ViewGroup;Ljava/lang/Object;)V
move-object/from16 v0, p2
iget-object v0, v0, Landroid/support/v4/app/g$b;->d:Landroid/view/View;
move-object/from16 v23, v0
move-object/from16 v0, p2
iget-object v0, v0, Landroid/support/v4/app/g$b;->b:Ljava/util/ArrayList;
move-object/from16 v31, v0
move-object/from16 v22, v6
move-object/from16 v24, v12
move-object/from16 v25, v19
move-object/from16 v28, v13
move-object/from16 v29, v11
move-object/from16 v32, v21
invoke-static/range {v22 .. v32}, Landroid/support/v4/app/s;->a(Landroid/view/View;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/Map;)V
:cond_139
if-eqz v30, :cond_143
const/4 v4, 0x1
goto/16 :goto_11
:cond_13e
invoke-virtual {v8}, Landroid/support/v4/app/Fragment;->z()Z
move-result v4
goto :goto_d9
:cond_143
const/4 v4, 0x0
goto/16 :goto_11
.end method
.method private b(Landroid/support/v4/app/g$b;Landroid/support/v4/app/Fragment;Z)Landroid/support/v4/e/a;
.registers 7
new-instance v0, Landroid/support/v4/e/a;
invoke-direct {v0}, Landroid/support/v4/e/a;-><init>()V
invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->n()Landroid/view/View;
move-result-object v1
if-eqz v1, :cond_1c
iget-object v2, p0, Landroid/support/v4/app/g;->u:Ljava/util/ArrayList;
if-eqz v2, :cond_1c
invoke-static {v0, v1}, Landroid/support/v4/app/s;->a(Ljava/util/Map;Landroid/view/View;)V
if-eqz p3, :cond_1d
iget-object v1, p0, Landroid/support/v4/app/g;->u:Ljava/util/ArrayList;
iget-object v2, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
invoke-static {v1, v2, v0}, Landroid/support/v4/app/g;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/e/a;)Landroid/support/v4/e/a;
move-result-object v0
:cond_1c
:goto_1c
return-object v0
:cond_1d
iget-object v1, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Landroid/support/v4/e/a;->a(Ljava/util/Collection;)Z
goto :goto_1c
.end method
.method private static b(Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
.registers 3
if-nez p0, :cond_4
const/4 v0, 0x0
:goto_3
return-object v0
:cond_4
if-eqz p1, :cond_f
invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->u()Ljava/lang/Object;
move-result-object v0
:goto_a
invoke-static {v0}, Landroid/support/v4/app/s;->a(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
goto :goto_3
:cond_f
invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->v()Ljava/lang/Object;
move-result-object v0
goto :goto_a
.end method
.method private b(Landroid/support/v4/app/g$b;Landroid/support/v4/e/a;Z)V
.registers 9
invoke-virtual {p2}, Landroid/support/v4/e/a;->size()I
move-result v3
const/4 v0, 0x0
move v2, v0
:goto_6
if-ge v2, v3, :cond_29
invoke-virtual {p2, v2}, Landroid/support/v4/e/a;->b(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
invoke-virtual {p2, v2}, Landroid/support/v4/e/a;->c(I)Ljava/lang/Object;
move-result-object v1
check-cast v1, Landroid/view/View;
invoke-static {v1}, Landroid/support/v4/app/s;->a(Landroid/view/View;)Ljava/lang/String;
move-result-object v1
if-eqz p3, :cond_23
iget-object v4, p1, Landroid/support/v4/app/g$b;->a:Landroid/support/v4/e/a;
invoke-static {v4, v0, v1}, Landroid/support/v4/app/g;->a(Landroid/support/v4/e/a;Ljava/lang/String;Ljava/lang/String;)V
:goto_1f
add-int/lit8 v0, v2, 0x1
move v2, v0
goto :goto_6
:cond_23
iget-object v4, p1, Landroid/support/v4/app/g$b;->a:Landroid/support/v4/e/a;
invoke-static {v4, v1, v0}, Landroid/support/v4/app/g;->a(Landroid/support/v4/e/a;Ljava/lang/String;Ljava/lang/String;)V
goto :goto_1f
:cond_29
return-void
.end method
.method private b(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
.registers 9
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v0, v0, Landroid/support/v4/app/q;->p:Landroid/support/v4/app/m;
invoke-virtual {v0}, Landroid/support/v4/app/m;->a()Z
move-result v0
if-nez v0, :cond_b
:cond_a
return-void
:cond_b
iget-object v0, p0, Landroid/support/v4/app/g;->c:Landroid/support/v4/app/g$a;
move-object v3, v0
:goto_e
if-eqz v3, :cond_a
iget v0, v3, Landroid/support/v4/app/g$a;->c:I
packed-switch v0, :pswitch_data_7a
:goto_15
iget-object v0, v3, Landroid/support/v4/app/g$a;->a:Landroid/support/v4/app/g$a;
move-object v3, v0
goto :goto_e
:pswitch_19
iget-object v0, v3, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/app/g;->b(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
:pswitch_1f
iget-object v1, v3, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v0, v0, Landroid/support/v4/app/q;->g:Ljava/util/ArrayList;
if-eqz v0, :cond_56
const/4 v0, 0x0
move-object v2, v1
move v1, v0
:goto_2a
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v0, v0, Landroid/support/v4/app/q;->g:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
if-ge v1, v0, :cond_56
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v0, v0, Landroid/support/v4/app/q;->g:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/app/Fragment;
if-eqz v2, :cond_46
iget v4, v0, Landroid/support/v4/app/Fragment;->G:I
iget v5, v2, Landroid/support/v4/app/Fragment;->G:I
if-ne v4, v5, :cond_4e
:cond_46
if-ne v0, v2, :cond_52
const/4 v2, 0x0
iget v0, v0, Landroid/support/v4/app/Fragment;->G:I
invoke-virtual {p2, v0}, Landroid/util/SparseArray;->remove(I)V
:goto_4e
:cond_4e
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_2a
:cond_52
invoke-static {p1, p2, v0}, Landroid/support/v4/app/g;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_4e
:cond_56
iget-object v0, v3, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/app/g;->b(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
:pswitch_5c
iget-object v0, v3, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-static {p1, p2, v0}, Landroid/support/v4/app/g;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
:pswitch_62
iget-object v0, v3, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-static {p1, p2, v0}, Landroid/support/v4/app/g;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
:pswitch_68
iget-object v0, v3, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/app/g;->b(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
:pswitch_6e
iget-object v0, v3, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-static {p1, p2, v0}, Landroid/support/v4/app/g;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
:pswitch_74
iget-object v0, v3, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/app/g;->b(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
:pswitch_data_7a
.packed-switch 0x1
:pswitch_19
:pswitch_1f
:pswitch_5c
:pswitch_62
:pswitch_68
:pswitch_6e
:pswitch_74
.end packed-switch
.end method
.method private b(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
.registers 10
const/4 v2, 0x1
const/4 v3, 0x0
if-eqz p3, :cond_31
iget v0, p3, Landroid/support/v4/app/Fragment;->G:I
if-eqz v0, :cond_1a
invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->l()Z
move-result v1
if-nez v1, :cond_11
invoke-virtual {p2, v0, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
:cond_11
invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
move-result-object v1
if-ne v1, p3, :cond_1a
invoke-virtual {p1, v0}, Landroid/util/SparseArray;->remove(I)V
:cond_1a
iget v0, p3, Landroid/support/v4/app/Fragment;->k:I
if-ge v0, v2, :cond_31
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget v0, v0, Landroid/support/v4/app/q;->n:I
if-lt v0, v2, :cond_31
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-virtual {v0, p3}, Landroid/support/v4/app/q;->c(Landroid/support/v4/app/Fragment;)V
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
move-object v1, p3
move v4, v3
move v5, v3
invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/Fragment;IIIZ)V
:cond_31
return-void
.end method
.method public a()I
.registers 2
const/4 v0, 0x0
invoke-virtual {p0, v0}, Landroid/support/v4/app/g;->a(Z)I
move-result v0
return v0
.end method
.method  a(Z)I
.registers 6
const/4 v3, 0x0
iget-boolean v0, p0, Landroid/support/v4/app/g;->o:Z
if-eqz v0, :cond_d
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "commit already called"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_d
sget-boolean v0, Landroid/support/v4/app/q;->a:Z
if-eqz v0, :cond_3a
const-string v0, "FragmentManager"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Commit: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
new-instance v0, Landroid/support/v4/e/d;
const-string v1, "FragmentManager"
invoke-direct {v0, v1}, Landroid/support/v4/e/d;-><init>(Ljava/lang/String;)V
new-instance v1, Ljava/io/PrintWriter;
invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
const-string v0, "  "
invoke-virtual {p0, v0, v3, v1, v3}, Landroid/support/v4/app/g;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
:cond_3a
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v4/app/g;->o:Z
iget-boolean v0, p0, Landroid/support/v4/app/g;->l:Z
if-eqz v0, :cond_51
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-virtual {v0, p0}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/g;)I
move-result v0
iput v0, p0, Landroid/support/v4/app/g;->p:I
:goto_49
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-virtual {v0, p0, p1}, Landroid/support/v4/app/q;->a(Ljava/lang/Runnable;Z)V
iget v0, p0, Landroid/support/v4/app/g;->p:I
return v0
:cond_51
const/4 v0, -0x1
iput v0, p0, Landroid/support/v4/app/g;->p:I
goto :goto_49
.end method
.method public a(ZLandroid/support/v4/app/g$b;Landroid/util/SparseArray;Landroid/util/SparseArray;)Landroid/support/v4/app/g$b;
.registers 16
const/4 v10, -0x1
const/4 v4, 0x0
const/4 v9, 0x1
const/4 v2, 0x0
sget-boolean v0, Landroid/support/v4/app/q;->a:Z
if-eqz v0, :cond_31
const-string v0, "FragmentManager"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "popFromBackStack: "
invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
new-instance v0, Landroid/support/v4/e/d;
const-string v1, "FragmentManager"
invoke-direct {v0, v1}, Landroid/support/v4/e/d;-><init>(Ljava/lang/String;)V
new-instance v1, Ljava/io/PrintWriter;
invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
const-string v0, "  "
invoke-virtual {p0, v0, v4, v1, v4}, Landroid/support/v4/app/g;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
:cond_31
sget-boolean v0, Landroid/support/v4/app/g;->a:Z
if-eqz v0, :cond_4d
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget v0, v0, Landroid/support/v4/app/q;->n:I
if-lt v0, v9, :cond_4d
if-nez p2, :cond_81
invoke-virtual {p3}, Landroid/util/SparseArray;->size()I
move-result v0
if-nez v0, :cond_49
invoke-virtual {p4}, Landroid/util/SparseArray;->size()I
move-result v0
if-eqz v0, :cond_4d
:cond_49
invoke-direct {p0, p3, p4, v9}, Landroid/support/v4/app/g;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)Landroid/support/v4/app/g$b;
move-result-object p2
:cond_4d
:goto_4d
invoke-virtual {p0, v10}, Landroid/support/v4/app/g;->a(I)V
if-eqz p2, :cond_8b
move v7, v2
:goto_53
if-eqz p2, :cond_8f
move v1, v2
:goto_56
iget-object v0, p0, Landroid/support/v4/app/g;->d:Landroid/support/v4/app/g$a;
move-object v6, v0
:goto_59
if-eqz v6, :cond_11c
if-eqz p2, :cond_93
move v5, v2
:goto_5e
if-eqz p2, :cond_97
move v0, v2
:goto_61
iget v3, v6, Landroid/support/v4/app/g$a;->c:I
packed-switch v3, :pswitch_data_13a
new-instance v0, Ljava/lang/IllegalArgumentException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Unknown cmd: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget v2, v6, Landroid/support/v4/app/g$a;->c:I
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_81
if-nez p1, :cond_4d
iget-object v0, p0, Landroid/support/v4/app/g;->v:Ljava/util/ArrayList;
iget-object v1, p0, Landroid/support/v4/app/g;->u:Ljava/util/ArrayList;
invoke-static {p2, v0, v1}, Landroid/support/v4/app/g;->a(Landroid/support/v4/app/g$b;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
goto :goto_4d
:cond_8b
iget v0, p0, Landroid/support/v4/app/g;->k:I
move v7, v0
goto :goto_53
:cond_8f
iget v0, p0, Landroid/support/v4/app/g;->j:I
move v1, v0
goto :goto_56
:cond_93
iget v0, v6, Landroid/support/v4/app/g$a;->g:I
move v5, v0
goto :goto_5e
:cond_97
iget v0, v6, Landroid/support/v4/app/g$a;->h:I
goto :goto_61
:pswitch_9a
iget-object v3, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iput v0, v3, Landroid/support/v4/app/Fragment;->P:I
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-static {v1}, Landroid/support/v4/app/q;->c(I)I
move-result v5
invoke-virtual {v0, v3, v5, v7}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/Fragment;II)V
:goto_a7
:cond_a7
iget-object v0, v6, Landroid/support/v4/app/g$a;->b:Landroid/support/v4/app/g$a;
move-object v6, v0
goto :goto_59
:pswitch_ab
iget-object v3, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
if-eqz v3, :cond_ba
iput v0, v3, Landroid/support/v4/app/Fragment;->P:I
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-static {v1}, Landroid/support/v4/app/q;->c(I)I
move-result v8
invoke-virtual {v0, v3, v8, v7}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/Fragment;II)V
:cond_ba
iget-object v0, v6, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
if-eqz v0, :cond_a7
move v3, v2
:goto_bf
iget-object v0, v6, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
if-ge v3, v0, :cond_a7
iget-object v0, v6, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/app/Fragment;
iput v5, v0, Landroid/support/v4/app/Fragment;->P:I
iget-object v8, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-virtual {v8, v0, v2}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/Fragment;Z)V
add-int/lit8 v0, v3, 0x1
move v3, v0
goto :goto_bf
:pswitch_da
iget-object v0, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iput v5, v0, Landroid/support/v4/app/Fragment;->P:I
iget-object v3, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-virtual {v3, v0, v2}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/Fragment;Z)V
goto :goto_a7
:pswitch_e4
iget-object v0, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iput v5, v0, Landroid/support/v4/app/Fragment;->P:I
iget-object v3, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-static {v1}, Landroid/support/v4/app/q;->c(I)I
move-result v5
invoke-virtual {v3, v0, v5, v7}, Landroid/support/v4/app/q;->c(Landroid/support/v4/app/Fragment;II)V
goto :goto_a7
:pswitch_f2
iget-object v3, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iput v0, v3, Landroid/support/v4/app/Fragment;->P:I
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-static {v1}, Landroid/support/v4/app/q;->c(I)I
move-result v5
invoke-virtual {v0, v3, v5, v7}, Landroid/support/v4/app/q;->b(Landroid/support/v4/app/Fragment;II)V
goto :goto_a7
:pswitch_100
iget-object v0, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iput v5, v0, Landroid/support/v4/app/Fragment;->P:I
iget-object v3, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-static {v1}, Landroid/support/v4/app/q;->c(I)I
move-result v5
invoke-virtual {v3, v0, v5, v7}, Landroid/support/v4/app/q;->e(Landroid/support/v4/app/Fragment;II)V
goto :goto_a7
:pswitch_10e
iget-object v0, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iput v5, v0, Landroid/support/v4/app/Fragment;->P:I
iget-object v3, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-static {v1}, Landroid/support/v4/app/q;->c(I)I
move-result v5
invoke-virtual {v3, v0, v5, v7}, Landroid/support/v4/app/q;->d(Landroid/support/v4/app/Fragment;II)V
goto :goto_a7
:cond_11c
if-eqz p1, :cond_12c
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v2, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget v2, v2, Landroid/support/v4/app/q;->n:I
invoke-static {v1}, Landroid/support/v4/app/q;->c(I)I
move-result v1
invoke-virtual {v0, v2, v1, v7, v9}, Landroid/support/v4/app/q;->a(IIIZ)V
move-object p2, v4
:cond_12c
iget v0, p0, Landroid/support/v4/app/g;->p:I
if-ltz v0, :cond_139
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget v1, p0, Landroid/support/v4/app/g;->p:I
invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->b(I)V
iput v10, p0, Landroid/support/v4/app/g;->p:I
:cond_139
return-object p2
:pswitch_data_13a
.packed-switch 0x1
:pswitch_9a
:pswitch_ab
:pswitch_da
:pswitch_e4
:pswitch_f2
:pswitch_100
:pswitch_10e
.end packed-switch
.end method
.method public a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/r;
.registers 4
const/4 v0, 0x0
invoke-virtual {p0, p1, p2, v0}, Landroid/support/v4/app/g;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/r;
move-result-object v0
return-object v0
.end method
.method public a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/r;
.registers 6
if-nez p1, :cond_a
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "Must use non-zero containerViewId"
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_a
const/4 v0, 0x2
invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v4/app/g;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
return-object p0
.end method
.method public a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/r;
.registers 4
new-instance v0, Landroid/support/v4/app/g$a;
invoke-direct {v0}, Landroid/support/v4/app/g$a;-><init>()V
const/4 v1, 0x3
iput v1, v0, Landroid/support/v4/app/g$a;->c:I
iput-object p1, v0, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-virtual {p0, v0}, Landroid/support/v4/app/g;->a(Landroid/support/v4/app/g$a;)V
return-object p0
.end method
.method public a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/r;
.registers 5
const/4 v0, 0x0
const/4 v1, 0x1
invoke-direct {p0, v0, p1, p2, v1}, Landroid/support/v4/app/g;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
return-object p0
.end method
.method public a(Ljava/lang/String;)Landroid/support/v4/app/r;
.registers 4
iget-boolean v0, p0, Landroid/support/v4/app/g;->m:Z
if-nez v0, :cond_c
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "This FragmentTransaction is not allowed to be added to the back stack."
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_c
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v4/app/g;->l:Z
iput-object p1, p0, Landroid/support/v4/app/g;->n:Ljava/lang/String;
return-object p0
.end method
.method  a(I)V
.registers 8
iget-boolean v0, p0, Landroid/support/v4/app/g;->l:Z
if-nez v0, :cond_5
:cond_4
return-void
:cond_5
sget-boolean v0, Landroid/support/v4/app/q;->a:Z
if-eqz v0, :cond_2b
const-string v0, "FragmentManager"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Bump nesting in "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, " by "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_2b
iget-object v0, p0, Landroid/support/v4/app/g;->c:Landroid/support/v4/app/g$a;
move-object v2, v0
:goto_2e
if-eqz v2, :cond_4
iget-object v0, v2, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
if-eqz v0, :cond_67
iget-object v0, v2, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iget v1, v0, Landroid/support/v4/app/Fragment;->A:I
add-int/2addr v1, p1
iput v1, v0, Landroid/support/v4/app/Fragment;->A:I
sget-boolean v0, Landroid/support/v4/app/q;->a:Z
if-eqz v0, :cond_67
const-string v0, "FragmentManager"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Bump nesting of "
invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v3, v2, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v3, " to "
invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v3, v2, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iget v3, v3, Landroid/support/v4/app/Fragment;->A:I
invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_67
iget-object v0, v2, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
if-eqz v0, :cond_af
iget-object v0, v2, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
add-int/lit8 v0, v0, -0x1
move v1, v0
:goto_74
if-ltz v1, :cond_af
iget-object v0, v2, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/app/Fragment;
iget v3, v0, Landroid/support/v4/app/Fragment;->A:I
add-int/2addr v3, p1
iput v3, v0, Landroid/support/v4/app/Fragment;->A:I
sget-boolean v3, Landroid/support/v4/app/q;->a:Z
if-eqz v3, :cond_ab
const-string v3, "FragmentManager"
new-instance v4, Ljava/lang/StringBuilder;
invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
const-string v5, "Bump nesting of "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v4
const-string v5, " to "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
iget v0, v0, Landroid/support/v4/app/Fragment;->A:I
invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-static {v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_ab
add-int/lit8 v0, v1, -0x1
move v1, v0
goto :goto_74
:cond_af
iget-object v0, v2, Landroid/support/v4/app/g$a;->a:Landroid/support/v4/app/g$a;
move-object v2, v0
goto/16 :goto_2e
.end method
.method  a(Landroid/support/v4/app/g$a;)V
.registers 3
iget-object v0, p0, Landroid/support/v4/app/g;->c:Landroid/support/v4/app/g$a;
if-nez v0, :cond_1f
iput-object p1, p0, Landroid/support/v4/app/g;->d:Landroid/support/v4/app/g$a;
iput-object p1, p0, Landroid/support/v4/app/g;->c:Landroid/support/v4/app/g$a;
:goto_8
iget v0, p0, Landroid/support/v4/app/g;->f:I
iput v0, p1, Landroid/support/v4/app/g$a;->e:I
iget v0, p0, Landroid/support/v4/app/g;->g:I
iput v0, p1, Landroid/support/v4/app/g$a;->f:I
iget v0, p0, Landroid/support/v4/app/g;->h:I
iput v0, p1, Landroid/support/v4/app/g$a;->g:I
iget v0, p0, Landroid/support/v4/app/g;->i:I
iput v0, p1, Landroid/support/v4/app/g$a;->h:I
iget v0, p0, Landroid/support/v4/app/g;->e:I
add-int/lit8 v0, v0, 0x1
iput v0, p0, Landroid/support/v4/app/g;->e:I
return-void
:cond_1f
iget-object v0, p0, Landroid/support/v4/app/g;->d:Landroid/support/v4/app/g$a;
iput-object v0, p1, Landroid/support/v4/app/g$a;->b:Landroid/support/v4/app/g$a;
iget-object v0, p0, Landroid/support/v4/app/g;->d:Landroid/support/v4/app/g$a;
iput-object p1, v0, Landroid/support/v4/app/g$a;->a:Landroid/support/v4/app/g$a;
iput-object p1, p0, Landroid/support/v4/app/g;->d:Landroid/support/v4/app/g$a;
goto :goto_8
.end method
.method public a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
.registers 6
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v0, v0, Landroid/support/v4/app/q;->p:Landroid/support/v4/app/m;
invoke-virtual {v0}, Landroid/support/v4/app/m;->a()Z
move-result v0
if-nez v0, :cond_b
:cond_a
return-void
:cond_b
iget-object v0, p0, Landroid/support/v4/app/g;->d:Landroid/support/v4/app/g$a;
move-object v2, v0
:goto_e
if-eqz v2, :cond_a
iget v0, v2, Landroid/support/v4/app/g$a;->c:I
packed-switch v0, :pswitch_data_62
:goto_15
iget-object v0, v2, Landroid/support/v4/app/g$a;->b:Landroid/support/v4/app/g$a;
move-object v2, v0
goto :goto_e
:pswitch_19
iget-object v0, v2, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-static {p1, p2, v0}, Landroid/support/v4/app/g;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
:pswitch_1f
iget-object v0, v2, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
if-eqz v0, :cond_3d
iget-object v0, v2, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
add-int/lit8 v0, v0, -0x1
move v1, v0
:goto_2c
if-ltz v1, :cond_3d
iget-object v0, v2, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/app/Fragment;
invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/app/g;->b(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
add-int/lit8 v0, v1, -0x1
move v1, v0
goto :goto_2c
:cond_3d
iget-object v0, v2, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-static {p1, p2, v0}, Landroid/support/v4/app/g;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
:pswitch_43
iget-object v0, v2, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/app/g;->b(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
:pswitch_49
iget-object v0, v2, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/app/g;->b(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
:pswitch_4f
iget-object v0, v2, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-static {p1, p2, v0}, Landroid/support/v4/app/g;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
:pswitch_55
iget-object v0, v2, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/app/g;->b(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
:pswitch_5b
iget-object v0, v2, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-static {p1, p2, v0}, Landroid/support/v4/app/g;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto :goto_15
nop
:pswitch_data_62
.packed-switch 0x1
:pswitch_19
:pswitch_1f
:pswitch_43
:pswitch_49
:pswitch_4f
:pswitch_55
:pswitch_5b
.end packed-switch
.end method
.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
.registers 6
const/4 v0, 0x1
invoke-virtual {p0, p1, p3, v0}, Landroid/support/v4/app/g;->a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
return-void
.end method
.method public a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
.registers 11
const/4 v1, 0x0
if-eqz p3, :cond_db
invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, "mName="
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget-object v0, p0, Landroid/support/v4/app/g;->n:Ljava/lang/String;
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, " mIndex="
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget v0, p0, Landroid/support/v4/app/g;->p:I
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V
const-string v0, " mCommitted="
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget-boolean v0, p0, Landroid/support/v4/app/g;->o:Z
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
iget v0, p0, Landroid/support/v4/app/g;->j:I
if-eqz v0, :cond_47
invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, "mTransition=#"
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget v0, p0, Landroid/support/v4/app/g;->j:I
invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, " mTransitionStyle=#"
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget v0, p0, Landroid/support/v4/app/g;->k:I
invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
:cond_47
iget v0, p0, Landroid/support/v4/app/g;->f:I
if-nez v0, :cond_4f
iget v0, p0, Landroid/support/v4/app/g;->g:I
if-eqz v0, :cond_6e
:cond_4f
invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, "mEnterAnim=#"
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget v0, p0, Landroid/support/v4/app/g;->f:I
invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, " mExitAnim=#"
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget v0, p0, Landroid/support/v4/app/g;->g:I
invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
:cond_6e
iget v0, p0, Landroid/support/v4/app/g;->h:I
if-nez v0, :cond_76
iget v0, p0, Landroid/support/v4/app/g;->i:I
if-eqz v0, :cond_95
:cond_76
invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, "mPopEnterAnim=#"
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget v0, p0, Landroid/support/v4/app/g;->h:I
invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, " mPopExitAnim=#"
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget v0, p0, Landroid/support/v4/app/g;->i:I
invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
:cond_95
iget v0, p0, Landroid/support/v4/app/g;->q:I
if-nez v0, :cond_9d
iget-object v0, p0, Landroid/support/v4/app/g;->r:Ljava/lang/CharSequence;
if-eqz v0, :cond_b8
:cond_9d
invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, "mBreadCrumbTitleRes=#"
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget v0, p0, Landroid/support/v4/app/g;->q:I
invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, " mBreadCrumbTitleText="
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget-object v0, p0, Landroid/support/v4/app/g;->r:Ljava/lang/CharSequence;
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
:cond_b8
iget v0, p0, Landroid/support/v4/app/g;->s:I
if-nez v0, :cond_c0
iget-object v0, p0, Landroid/support/v4/app/g;->t:Ljava/lang/CharSequence;
if-eqz v0, :cond_db
:cond_c0
invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, "mBreadCrumbShortTitleRes=#"
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget v0, p0, Landroid/support/v4/app/g;->s:I
invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, " mBreadCrumbShortTitleText="
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget-object v0, p0, Landroid/support/v4/app/g;->t:Ljava/lang/CharSequence;
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
:cond_db
iget-object v0, p0, Landroid/support/v4/app/g;->c:Landroid/support/v4/app/g$a;
if-eqz v0, :cond_1f8
invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, "Operations:"
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
new-instance v0, Ljava/lang/StringBuilder;
invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v2, "    "
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v4
iget-object v0, p0, Landroid/support/v4/app/g;->c:Landroid/support/v4/app/g$a;
move v2, v1
move-object v3, v0
:goto_fe
if-eqz v3, :cond_1f8
iget v0, v3, Landroid/support/v4/app/g$a;->c:I
packed-switch v0, :pswitch_data_1fa
new-instance v0, Ljava/lang/StringBuilder;
invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
const-string v5, "cmd="
invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
iget v5, v3, Landroid/support/v4/app/g$a;->c:I
invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
:goto_11a
invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v5, "  Op #"
invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(I)V
const-string v5, ": "
invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, " "
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget-object v0, v3, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
if-eqz p3, :cond_187
iget v0, v3, Landroid/support/v4/app/g$a;->e:I
if-nez v0, :cond_141
iget v0, v3, Landroid/support/v4/app/g$a;->f:I
if-eqz v0, :cond_160
:cond_141
invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, "enterAnim=#"
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget v0, v3, Landroid/support/v4/app/g$a;->e:I
invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, " exitAnim=#"
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget v0, v3, Landroid/support/v4/app/g$a;->f:I
invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
:cond_160
iget v0, v3, Landroid/support/v4/app/g$a;->g:I
if-nez v0, :cond_168
iget v0, v3, Landroid/support/v4/app/g$a;->h:I
if-eqz v0, :cond_187
:cond_168
invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, "popEnterAnim=#"
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget v0, v3, Landroid/support/v4/app/g$a;->g:I
invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v0, " popExitAnim=#"
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget v0, v3, Landroid/support/v4/app/g$a;->h:I
invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
:cond_187
iget-object v0, v3, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
if-eqz v0, :cond_1f1
iget-object v0, v3, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
if-lez v0, :cond_1f1
move v0, v1
:goto_194
iget-object v5, v3, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
invoke-virtual {v5}, Ljava/util/ArrayList;->size()I
move-result v5
if-ge v0, v5, :cond_1f1
invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
iget-object v5, v3, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
invoke-virtual {v5}, Ljava/util/ArrayList;->size()I
move-result v5
const/4 v6, 0x1
if-ne v5, v6, :cond_1d9
const-string v5, "Removed: "
invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
:goto_1ad
iget-object v5, v3, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v5
invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
add-int/lit8 v0, v0, 0x1
goto :goto_194
:pswitch_1b9
const-string v0, "NULL"
goto/16 :goto_11a
:pswitch_1bd
const-string v0, "ADD"
goto/16 :goto_11a
:pswitch_1c1
const-string v0, "REPLACE"
goto/16 :goto_11a
:pswitch_1c5
const-string v0, "REMOVE"
goto/16 :goto_11a
:pswitch_1c9
const-string v0, "HIDE"
goto/16 :goto_11a
:pswitch_1cd
const-string v0, "SHOW"
goto/16 :goto_11a
:pswitch_1d1
const-string v0, "DETACH"
goto/16 :goto_11a
:pswitch_1d5
const-string v0, "ATTACH"
goto/16 :goto_11a
:cond_1d9
if-nez v0, :cond_1e0
const-string v5, "Removed:"
invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
:cond_1e0
invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
const-string v5, "  #"
invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V
const-string v5, ": "
invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
goto :goto_1ad
:cond_1f1
iget-object v3, v3, Landroid/support/v4/app/g$a;->a:Landroid/support/v4/app/g$a;
add-int/lit8 v0, v2, 0x1
move v2, v0
goto/16 :goto_fe
:cond_1f8
return-void
nop
:pswitch_data_1fa
.packed-switch 0x0
:pswitch_1b9
:pswitch_1bd
:pswitch_1c1
:pswitch_1c5
:pswitch_1c9
:pswitch_1cd
:pswitch_1d1
:pswitch_1d5
.end packed-switch
.end method
.method public b()I
.registers 2
const/4 v0, 0x1
invoke-virtual {p0, v0}, Landroid/support/v4/app/g;->a(Z)I
move-result v0
return v0
.end method
.method public c()Ljava/lang/String;
.registers 2
iget-object v0, p0, Landroid/support/v4/app/g;->n:Ljava/lang/String;
return-object v0
.end method
.method public run()V
.registers 14
sget-boolean v0, Landroid/support/v4/app/q;->a:Z
if-eqz v0, :cond_1c
const-string v0, "FragmentManager"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Run: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_1c
iget-boolean v0, p0, Landroid/support/v4/app/g;->l:Z
if-eqz v0, :cond_2c
iget v0, p0, Landroid/support/v4/app/g;->p:I
if-gez v0, :cond_2c
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "addToBackStack() called after commit()"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_2c
const/4 v0, 0x1
invoke-virtual {p0, v0}, Landroid/support/v4/app/g;->a(I)V
const/4 v0, 0x0
sget-boolean v1, Landroid/support/v4/app/g;->a:Z
if-eqz v1, :cond_199
iget-object v1, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget v1, v1, Landroid/support/v4/app/q;->n:I
const/4 v2, 0x1
if-lt v1, v2, :cond_199
new-instance v0, Landroid/util/SparseArray;
invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V
new-instance v1, Landroid/util/SparseArray;
invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V
invoke-direct {p0, v0, v1}, Landroid/support/v4/app/g;->b(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
const/4 v2, 0x0
invoke-direct {p0, v0, v1, v2}, Landroid/support/v4/app/g;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)Landroid/support/v4/app/g$b;
move-result-object v0
move-object v8, v0
:goto_4f
if-eqz v8, :cond_84
const/4 v0, 0x0
move v7, v0
:goto_53
if-eqz v8, :cond_88
const/4 v0, 0x0
move v1, v0
:goto_57
iget-object v0, p0, Landroid/support/v4/app/g;->c:Landroid/support/v4/app/g$a;
move-object v6, v0
:goto_5a
if-eqz v6, :cond_185
if-eqz v8, :cond_8c
const/4 v0, 0x0
move v5, v0
:goto_60
if-eqz v8, :cond_90
const/4 v0, 0x0
move v2, v0
:goto_64
iget v0, v6, Landroid/support/v4/app/g$a;->c:I
packed-switch v0, :pswitch_data_19c
new-instance v0, Ljava/lang/IllegalArgumentException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Unknown cmd: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget v2, v6, Landroid/support/v4/app/g$a;->c:I
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_84
iget v0, p0, Landroid/support/v4/app/g;->k:I
move v7, v0
goto :goto_53
:cond_88
iget v0, p0, Landroid/support/v4/app/g;->j:I
move v1, v0
goto :goto_57
:cond_8c
iget v0, v6, Landroid/support/v4/app/g$a;->e:I
move v5, v0
goto :goto_60
:cond_90
iget v0, v6, Landroid/support/v4/app/g$a;->f:I
move v2, v0
goto :goto_64
:pswitch_94
iget-object v0, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iput v5, v0, Landroid/support/v4/app/Fragment;->P:I
iget-object v2, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
const/4 v3, 0x0
invoke-virtual {v2, v0, v3}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/Fragment;Z)V
:cond_9e
:goto_9e
iget-object v0, v6, Landroid/support/v4/app/g$a;->a:Landroid/support/v4/app/g$a;
move-object v6, v0
goto :goto_5a
:pswitch_a2
iget-object v3, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iget v9, v3, Landroid/support/v4/app/Fragment;->G:I
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v0, v0, Landroid/support/v4/app/q;->g:Ljava/util/ArrayList;
if-eqz v0, :cond_142
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v0, v0, Landroid/support/v4/app/q;->g:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
add-int/lit8 v0, v0, -0x1
move v4, v0
:goto_b7
if-ltz v4, :cond_142
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v0, v0, Landroid/support/v4/app/q;->g:Ljava/util/ArrayList;
invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/v4/app/Fragment;
sget-boolean v10, Landroid/support/v4/app/q;->a:Z
if-eqz v10, :cond_e9
const-string v10, "FragmentManager"
new-instance v11, Ljava/lang/StringBuilder;
invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V
const-string v12, "OP_REPLACE: adding="
invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v11
invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v11
const-string v12, " old="
invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v11
invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v11
invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v11
invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_e9
iget v10, v0, Landroid/support/v4/app/Fragment;->G:I
if-ne v10, v9, :cond_140
if-ne v0, v3, :cond_f7
const/4 v0, 0x0
iput-object v0, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
:goto_f2
add-int/lit8 v3, v4, -0x1
move v4, v3
move-object v3, v0
goto :goto_b7
:cond_f7
iget-object v10, v6, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
if-nez v10, :cond_102
new-instance v10, Ljava/util/ArrayList;
invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V
iput-object v10, v6, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
:cond_102
iget-object v10, v6, Landroid/support/v4/app/g$a;->i:Ljava/util/ArrayList;
invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
iput v2, v0, Landroid/support/v4/app/Fragment;->P:I
iget-boolean v10, p0, Landroid/support/v4/app/g;->l:Z
if-eqz v10, :cond_13b
iget v10, v0, Landroid/support/v4/app/Fragment;->A:I
add-int/lit8 v10, v10, 0x1
iput v10, v0, Landroid/support/v4/app/Fragment;->A:I
sget-boolean v10, Landroid/support/v4/app/q;->a:Z
if-eqz v10, :cond_13b
const-string v10, "FragmentManager"
new-instance v11, Ljava/lang/StringBuilder;
invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V
const-string v12, "Bump nesting of "
invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v11
invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v11
const-string v12, " to "
invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v11
iget v12, v0, Landroid/support/v4/app/Fragment;->A:I
invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v11
invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v11
invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_13b
iget-object v10, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-virtual {v10, v0, v1, v7}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/Fragment;II)V
:cond_140
move-object v0, v3
goto :goto_f2
:cond_142
if-eqz v3, :cond_9e
iput v5, v3, Landroid/support/v4/app/Fragment;->P:I
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
const/4 v2, 0x0
invoke-virtual {v0, v3, v2}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/Fragment;Z)V
goto/16 :goto_9e
:pswitch_14e
iget-object v0, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iput v2, v0, Landroid/support/v4/app/Fragment;->P:I
iget-object v2, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-virtual {v2, v0, v1, v7}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/Fragment;II)V
goto/16 :goto_9e
:pswitch_159
iget-object v0, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iput v2, v0, Landroid/support/v4/app/Fragment;->P:I
iget-object v2, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-virtual {v2, v0, v1, v7}, Landroid/support/v4/app/q;->b(Landroid/support/v4/app/Fragment;II)V
goto/16 :goto_9e
:pswitch_164
iget-object v0, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iput v5, v0, Landroid/support/v4/app/Fragment;->P:I
iget-object v2, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-virtual {v2, v0, v1, v7}, Landroid/support/v4/app/q;->c(Landroid/support/v4/app/Fragment;II)V
goto/16 :goto_9e
:pswitch_16f
iget-object v0, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iput v2, v0, Landroid/support/v4/app/Fragment;->P:I
iget-object v2, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-virtual {v2, v0, v1, v7}, Landroid/support/v4/app/q;->d(Landroid/support/v4/app/Fragment;II)V
goto/16 :goto_9e
:pswitch_17a
iget-object v0, v6, Landroid/support/v4/app/g$a;->d:Landroid/support/v4/app/Fragment;
iput v5, v0, Landroid/support/v4/app/Fragment;->P:I
iget-object v2, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-virtual {v2, v0, v1, v7}, Landroid/support/v4/app/q;->e(Landroid/support/v4/app/Fragment;II)V
goto/16 :goto_9e
:cond_185
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget-object v2, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
iget v2, v2, Landroid/support/v4/app/q;->n:I
const/4 v3, 0x1
invoke-virtual {v0, v2, v1, v7, v3}, Landroid/support/v4/app/q;->a(IIIZ)V
iget-boolean v0, p0, Landroid/support/v4/app/g;->l:Z
if-eqz v0, :cond_198
iget-object v0, p0, Landroid/support/v4/app/g;->b:Landroid/support/v4/app/q;
invoke-virtual {v0, p0}, Landroid/support/v4/app/q;->b(Landroid/support/v4/app/g;)V
:cond_198
return-void
:cond_199
move-object v8, v0
goto/16 :goto_4f
:pswitch_data_19c
.packed-switch 0x1
:pswitch_94
:pswitch_a2
:pswitch_14e
:pswitch_159
:pswitch_164
:pswitch_16f
:pswitch_17a
.end packed-switch
.end method
.method public toString()Ljava/lang/String;
.registers 3
new-instance v0, Ljava/lang/StringBuilder;
const/16 v1, 0x80
invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V
const-string v1, "BackStackEntry{"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I
move-result v1
invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iget v1, p0, Landroid/support/v4/app/g;->p:I
if-ltz v1, :cond_25
const-string v1, " #"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iget v1, p0, Landroid/support/v4/app/g;->p:I
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
:cond_25
iget-object v1, p0, Landroid/support/v4/app/g;->n:Ljava/lang/String;
if-eqz v1, :cond_33
const-string v1, " "
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iget-object v1, p0, Landroid/support/v4/app/g;->n:Ljava/lang/String;
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
:cond_33
const-string v1, "}"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
return-object v0
.end method