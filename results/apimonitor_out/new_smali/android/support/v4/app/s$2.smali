.class final Landroid/support/v4/app/s$2;
.super Ljava/lang/Object;
.source "FragmentTransitionCompat21.java"
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;
.field final synthetic a:Landroid/view/View;
.field final synthetic b:Landroid/transition/Transition;
.field final synthetic c:Landroid/view/View;
.field final synthetic d:Landroid/support/v4/app/s$b;
.field final synthetic e:Ljava/util/Map;
.field final synthetic f:Ljava/util/Map;
.field final synthetic g:Ljava/util/ArrayList;
.method constructor <init>(Landroid/view/View;Landroid/transition/Transition;Landroid/view/View;Landroid/support/v4/app/s$b;Ljava/util/Map;Ljava/util/Map;Ljava/util/ArrayList;)V
.registers 8
iput-object p1, p0, Landroid/support/v4/app/s$2;->a:Landroid/view/View;
iput-object p2, p0, Landroid/support/v4/app/s$2;->b:Landroid/transition/Transition;
iput-object p3, p0, Landroid/support/v4/app/s$2;->c:Landroid/view/View;
iput-object p4, p0, Landroid/support/v4/app/s$2;->d:Landroid/support/v4/app/s$b;
iput-object p5, p0, Landroid/support/v4/app/s$2;->e:Ljava/util/Map;
iput-object p6, p0, Landroid/support/v4/app/s$2;->f:Ljava/util/Map;
iput-object p7, p0, Landroid/support/v4/app/s$2;->g:Ljava/util/ArrayList;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public onPreDraw()Z
.registers 6
iget-object v0, p0, Landroid/support/v4/app/s$2;->a:Landroid/view/View;
invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;
move-result-object v0
invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
iget-object v0, p0, Landroid/support/v4/app/s$2;->b:Landroid/transition/Transition;
if-eqz v0, :cond_14
iget-object v0, p0, Landroid/support/v4/app/s$2;->b:Landroid/transition/Transition;
iget-object v1, p0, Landroid/support/v4/app/s$2;->c:Landroid/view/View;
invoke-virtual {v0, v1}, Landroid/transition/Transition;->removeTarget(Landroid/view/View;)Landroid/transition/Transition;
:cond_14
iget-object v0, p0, Landroid/support/v4/app/s$2;->d:Landroid/support/v4/app/s$b;
invoke-interface {v0}, Landroid/support/v4/app/s$b;->a()Landroid/view/View;
move-result-object v2
if-eqz v2, :cond_8a
iget-object v0, p0, Landroid/support/v4/app/s$2;->e:Ljava/util/Map;
invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
move-result v0
if-nez v0, :cond_68
iget-object v0, p0, Landroid/support/v4/app/s$2;->f:Ljava/util/Map;
invoke-static {v0, v2}, Landroid/support/v4/app/s;->a(Ljava/util/Map;Landroid/view/View;)V
iget-object v0, p0, Landroid/support/v4/app/s$2;->f:Ljava/util/Map;
invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;
move-result-object v0
iget-object v1, p0, Landroid/support/v4/app/s$2;->e:Ljava/util/Map;
invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;
move-result-object v1
invoke-interface {v0, v1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z
iget-object v0, p0, Landroid/support/v4/app/s$2;->e:Ljava/util/Map;
invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;
move-result-object v0
invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
move-result-object v3
:cond_42
:goto_42
invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_68
invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/util/Map$Entry;
invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;
move-result-object v1
check-cast v1, Ljava/lang/String;
iget-object v4, p0, Landroid/support/v4/app/s$2;->f:Ljava/util/Map;
invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v1
check-cast v1, Landroid/view/View;
if-eqz v1, :cond_42
invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
invoke-virtual {v1, v0}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V
goto :goto_42
:cond_68
iget-object v0, p0, Landroid/support/v4/app/s$2;->b:Landroid/transition/Transition;
if-eqz v0, :cond_8a
iget-object v0, p0, Landroid/support/v4/app/s$2;->g:Ljava/util/ArrayList;
invoke-static {v0, v2}, Landroid/support/v4/app/s;->a(Ljava/util/ArrayList;Landroid/view/View;)V
iget-object v0, p0, Landroid/support/v4/app/s$2;->g:Ljava/util/ArrayList;
iget-object v1, p0, Landroid/support/v4/app/s$2;->f:Ljava/util/Map;
invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z
iget-object v0, p0, Landroid/support/v4/app/s$2;->g:Ljava/util/ArrayList;
iget-object v1, p0, Landroid/support/v4/app/s$2;->c:Landroid/view/View;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
iget-object v0, p0, Landroid/support/v4/app/s$2;->b:Landroid/transition/Transition;
iget-object v1, p0, Landroid/support/v4/app/s$2;->g:Ljava/util/ArrayList;
invoke-static {v0, v1}, Landroid/support/v4/app/s;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V
:cond_8a
const/4 v0, 0x1
return v0
.end method