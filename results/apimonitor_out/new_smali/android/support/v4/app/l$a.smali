.class  Landroid/support/v4/app/l$a;
.super Landroid/support/v4/app/o;
.source "FragmentActivity.java"
.field final synthetic a:Landroid/support/v4/app/l;
.method public constructor <init>(Landroid/support/v4/app/l;)V
.registers 2
iput-object p1, p0, Landroid/support/v4/app/l$a;->a:Landroid/support/v4/app/l;
invoke-direct {p0, p1}, Landroid/support/v4/app/o;-><init>(Landroid/support/v4/app/l;)V
return-void
.end method
.method public a(I)Landroid/view/View;
.registers 3
iget-object v0, p0, Landroid/support/v4/app/l$a;->a:Landroid/support/v4/app/l;
invoke-virtual {v0, p1}, Landroid/support/v4/app/l;->findViewById(I)Landroid/view/View;
move-result-object v0
return-object v0
.end method
.method public a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V
.registers 6
iget-object v0, p0, Landroid/support/v4/app/l$a;->a:Landroid/support/v4/app/l;
invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/app/l;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V
return-void
.end method
.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
.registers 6
iget-object v0, p0, Landroid/support/v4/app/l$a;->a:Landroid/support/v4/app/l;
invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/app/l;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
return-void
.end method
.method public a()Z
.registers 2
iget-object v0, p0, Landroid/support/v4/app/l$a;->a:Landroid/support/v4/app/l;
invoke-virtual {v0}, Landroid/support/v4/app/l;->getWindow()Landroid/view/Window;
move-result-object v0
if-eqz v0, :cond_10
invoke-virtual {v0}, Landroid/view/Window;->peekDecorView()Landroid/view/View;
move-result-object v0
if-eqz v0, :cond_10
const/4 v0, 0x1
:goto_f
return v0
:cond_10
const/4 v0, 0x0
goto :goto_f
.end method
.method public a(Landroid/support/v4/app/Fragment;)Z
.registers 3
iget-object v0, p0, Landroid/support/v4/app/l$a;->a:Landroid/support/v4/app/l;
invoke-virtual {v0}, Landroid/support/v4/app/l;->isFinishing()Z
move-result v0
if-nez v0, :cond_a
const/4 v0, 0x1
:goto_9
return v0
:cond_a
const/4 v0, 0x0
goto :goto_9
.end method
.method public b()Landroid/view/LayoutInflater;
.registers 3
iget-object v0, p0, Landroid/support/v4/app/l$a;->a:Landroid/support/v4/app/l;
invoke-virtual {v0}, Landroid/support/v4/app/l;->getLayoutInflater()Landroid/view/LayoutInflater;
move-result-object v0
iget-object v1, p0, Landroid/support/v4/app/l$a;->a:Landroid/support/v4/app/l;
invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;
move-result-object v0
return-object v0
.end method
.method public b(Landroid/support/v4/app/Fragment;)V
.registers 3
iget-object v0, p0, Landroid/support/v4/app/l$a;->a:Landroid/support/v4/app/l;
invoke-virtual {v0, p1}, Landroid/support/v4/app/l;->a(Landroid/support/v4/app/Fragment;)V
return-void
.end method
.method public c()V
.registers 2
iget-object v0, p0, Landroid/support/v4/app/l$a;->a:Landroid/support/v4/app/l;
invoke-virtual {v0}, Landroid/support/v4/app/l;->d()V
return-void
.end method
.method public d()Z
.registers 2
iget-object v0, p0, Landroid/support/v4/app/l$a;->a:Landroid/support/v4/app/l;
invoke-virtual {v0}, Landroid/support/v4/app/l;->getWindow()Landroid/view/Window;
move-result-object v0
if-eqz v0, :cond_a
const/4 v0, 0x1
:goto_9
return v0
:cond_a
const/4 v0, 0x0
goto :goto_9
.end method
.method public e()I
.registers 2
iget-object v0, p0, Landroid/support/v4/app/l$a;->a:Landroid/support/v4/app/l;
invoke-virtual {v0}, Landroid/support/v4/app/l;->getWindow()Landroid/view/Window;
move-result-object v0
if-nez v0, :cond_a
const/4 v0, 0x0
:goto_9
return v0
:cond_a
invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;
move-result-object v0
iget v0, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I
goto :goto_9
.end method