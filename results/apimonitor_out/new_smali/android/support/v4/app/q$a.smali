.class  Landroid/support/v4/app/q$a;
.super Ljava/lang/Object;
.source "FragmentManager.java"
.implements Landroid/view/animation/Animation$AnimationListener;
.field private a:Landroid/view/animation/Animation$AnimationListener;
.field private b:Z
.field private c:Landroid/view/View;
.method public constructor <init>(Landroid/view/View;Landroid/view/animation/Animation;)V
.registers 5
const/4 v1, 0x0
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object v1, p0, Landroid/support/v4/app/q$a;->a:Landroid/view/animation/Animation$AnimationListener;
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v4/app/q$a;->b:Z
iput-object v1, p0, Landroid/support/v4/app/q$a;->c:Landroid/view/View;
if-eqz p1, :cond_f
if-nez p2, :cond_10
:goto_f
:cond_f
return-void
:cond_10
iput-object p1, p0, Landroid/support/v4/app/q$a;->c:Landroid/view/View;
goto :goto_f
.end method
.method public constructor <init>(Landroid/view/View;Landroid/view/animation/Animation;Landroid/view/animation/Animation$AnimationListener;)V
.registers 6
const/4 v1, 0x0
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object v1, p0, Landroid/support/v4/app/q$a;->a:Landroid/view/animation/Animation$AnimationListener;
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v4/app/q$a;->b:Z
iput-object v1, p0, Landroid/support/v4/app/q$a;->c:Landroid/view/View;
if-eqz p1, :cond_f
if-nez p2, :cond_10
:goto_f
:cond_f
return-void
:cond_10
iput-object p3, p0, Landroid/support/v4/app/q$a;->a:Landroid/view/animation/Animation$AnimationListener;
iput-object p1, p0, Landroid/support/v4/app/q$a;->c:Landroid/view/View;
goto :goto_f
.end method
.method static synthetic a(Landroid/support/v4/app/q$a;)Landroid/view/View;
.registers 2
iget-object v0, p0, Landroid/support/v4/app/q$a;->c:Landroid/view/View;
return-object v0
.end method
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
.registers 4
iget-object v0, p0, Landroid/support/v4/app/q$a;->c:Landroid/view/View;
if-eqz v0, :cond_12
iget-boolean v0, p0, Landroid/support/v4/app/q$a;->b:Z
if-eqz v0, :cond_12
iget-object v0, p0, Landroid/support/v4/app/q$a;->c:Landroid/view/View;
new-instance v1, Landroid/support/v4/app/q$a$2;
invoke-direct {v1, p0}, Landroid/support/v4/app/q$a$2;-><init>(Landroid/support/v4/app/q$a;)V
invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z
:cond_12
iget-object v0, p0, Landroid/support/v4/app/q$a;->a:Landroid/view/animation/Animation$AnimationListener;
if-eqz v0, :cond_1b
iget-object v0, p0, Landroid/support/v4/app/q$a;->a:Landroid/view/animation/Animation$AnimationListener;
invoke-interface {v0, p1}, Landroid/view/animation/Animation$AnimationListener;->onAnimationEnd(Landroid/view/animation/Animation;)V
:cond_1b
return-void
.end method
.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
.registers 3
iget-object v0, p0, Landroid/support/v4/app/q$a;->a:Landroid/view/animation/Animation$AnimationListener;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v4/app/q$a;->a:Landroid/view/animation/Animation$AnimationListener;
invoke-interface {v0, p1}, Landroid/view/animation/Animation$AnimationListener;->onAnimationRepeat(Landroid/view/animation/Animation;)V
:cond_9
return-void
.end method
.method public onAnimationStart(Landroid/view/animation/Animation;)V
.registers 4
iget-object v0, p0, Landroid/support/v4/app/q$a;->c:Landroid/view/View;
if-eqz v0, :cond_1a
iget-object v0, p0, Landroid/support/v4/app/q$a;->c:Landroid/view/View;
invoke-static {v0, p1}, Landroid/support/v4/app/q;->a(Landroid/view/View;Landroid/view/animation/Animation;)Z
move-result v0
iput-boolean v0, p0, Landroid/support/v4/app/q$a;->b:Z
iget-boolean v0, p0, Landroid/support/v4/app/q$a;->b:Z
if-eqz v0, :cond_1a
iget-object v0, p0, Landroid/support/v4/app/q$a;->c:Landroid/view/View;
new-instance v1, Landroid/support/v4/app/q$a$1;
invoke-direct {v1, p0}, Landroid/support/v4/app/q$a$1;-><init>(Landroid/support/v4/app/q$a;)V
invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z
:cond_1a
iget-object v0, p0, Landroid/support/v4/app/q$a;->a:Landroid/view/animation/Animation$AnimationListener;
if-eqz v0, :cond_23
iget-object v0, p0, Landroid/support/v4/app/q$a;->a:Landroid/view/animation/Animation$AnimationListener;
invoke-interface {v0, p1}, Landroid/view/animation/Animation$AnimationListener;->onAnimationStart(Landroid/view/animation/Animation;)V
:cond_23
return-void
.end method