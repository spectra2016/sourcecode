.class public final Landroid/support/v4/app/ap;
.super Ljava/lang/Object;
.source "TaskStackBuilder.java"
.implements Ljava/lang/Iterable;
.field private static final a:Landroid/support/v4/app/ap$b;
.field private final b:Ljava/util/ArrayList;
.field private final c:Landroid/content/Context;
.method static constructor <clinit>()V
.registers 2
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0xb
if-lt v0, v1, :cond_e
new-instance v0, Landroid/support/v4/app/ap$d;
invoke-direct {v0}, Landroid/support/v4/app/ap$d;-><init>()V
sput-object v0, Landroid/support/v4/app/ap;->a:Landroid/support/v4/app/ap$b;
:goto_d
return-void
:cond_e
new-instance v0, Landroid/support/v4/app/ap$c;
invoke-direct {v0}, Landroid/support/v4/app/ap$c;-><init>()V
sput-object v0, Landroid/support/v4/app/ap;->a:Landroid/support/v4/app/ap$b;
goto :goto_d
.end method
.method private constructor <init>(Landroid/content/Context;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Landroid/support/v4/app/ap;->b:Ljava/util/ArrayList;
iput-object p1, p0, Landroid/support/v4/app/ap;->c:Landroid/content/Context;
return-void
.end method
.method public static a(Landroid/content/Context;)Landroid/support/v4/app/ap;
.registers 2
new-instance v0, Landroid/support/v4/app/ap;
invoke-direct {v0, p0}, Landroid/support/v4/app/ap;-><init>(Landroid/content/Context;)V
return-object v0
.end method
.method public a(Landroid/app/Activity;)Landroid/support/v4/app/ap;
.registers 4
const/4 v0, 0x0
instance-of v1, p1, Landroid/support/v4/app/ap$a;
if-eqz v1, :cond_c
move-object v0, p1
check-cast v0, Landroid/support/v4/app/ap$a;
invoke-interface {v0}, Landroid/support/v4/app/ap$a;->a()Landroid/content/Intent;
move-result-object v0
:cond_c
if-nez v0, :cond_2c
invoke-static {p1}, Landroid/support/v4/app/v;->a(Landroid/app/Activity;)Landroid/content/Intent;
move-result-object v0
move-object v1, v0
:goto_13
if-eqz v1, :cond_2b
invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
move-result-object v0
if-nez v0, :cond_25
iget-object v0, p0, Landroid/support/v4/app/ap;->c:Landroid/content/Context;
invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v0
invoke-virtual {v1, v0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;
move-result-object v0
:cond_25
invoke-virtual {p0, v0}, Landroid/support/v4/app/ap;->a(Landroid/content/ComponentName;)Landroid/support/v4/app/ap;
invoke-virtual {p0, v1}, Landroid/support/v4/app/ap;->a(Landroid/content/Intent;)Landroid/support/v4/app/ap;
:cond_2b
return-object p0
:cond_2c
move-object v1, v0
goto :goto_13
.end method
.method public a(Landroid/content/ComponentName;)Landroid/support/v4/app/ap;
.registers 5
iget-object v0, p0, Landroid/support/v4/app/ap;->b:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v1
:try_start_6
iget-object v0, p0, Landroid/support/v4/app/ap;->c:Landroid/content/Context;
invoke-static {v0, p1}, Landroid/support/v4/app/v;->a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Intent;
move-result-object v0
:goto_c
if-eqz v0, :cond_2c
iget-object v2, p0, Landroid/support/v4/app/ap;->b:Ljava/util/ArrayList;
invoke-virtual {v2, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
iget-object v2, p0, Landroid/support/v4/app/ap;->c:Landroid/content/Context;
invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
move-result-object v0
invoke-static {v2, v0}, Landroid/support/v4/app/v;->a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Intent;
:try_end_1c
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_1c} :catch_1e
move-result-object v0
goto :goto_c
:catch_1e
move-exception v0
const-string v1, "TaskStackBuilder"
const-string v2, "Bad ComponentName while traversing activity parent metadata"
invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
new-instance v1, Ljava/lang/IllegalArgumentException;
invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V
throw v1
:cond_2c
return-object p0
.end method
.method public a(Landroid/content/Intent;)Landroid/support/v4/app/ap;
.registers 3
iget-object v0, p0, Landroid/support/v4/app/ap;->b:Ljava/util/ArrayList;
invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
return-object p0
.end method
.method public a()V
.registers 2
const/4 v0, 0x0
invoke-virtual {p0, v0}, Landroid/support/v4/app/ap;->a(Landroid/os/Bundle;)V
return-void
.end method
.method public a(Landroid/os/Bundle;)V
.registers 6
const/4 v3, 0x0
iget-object v0, p0, Landroid/support/v4/app/ap;->b:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
move-result v0
if-eqz v0, :cond_11
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "No intents added to TaskStackBuilder; cannot startActivities"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_11
iget-object v0, p0, Landroid/support/v4/app/ap;->b:Ljava/util/ArrayList;
iget-object v1, p0, Landroid/support/v4/app/ap;->b:Ljava/util/ArrayList;
invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
move-result v1
new-array v1, v1, [Landroid/content/Intent;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
move-result-object v0
check-cast v0, [Landroid/content/Intent;
new-instance v1, Landroid/content/Intent;
aget-object v2, v0, v3
invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V
const v2, 0x1000c000
invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
move-result-object v1
aput-object v1, v0, v3
iget-object v1, p0, Landroid/support/v4/app/ap;->c:Landroid/content/Context;
invoke-static {v1, v0, p1}, Landroid/support/v4/a/a;->a(Landroid/content/Context;[Landroid/content/Intent;Landroid/os/Bundle;)Z
move-result v1
if-nez v1, :cond_4d
new-instance v1, Landroid/content/Intent;
array-length v2, v0
add-int/lit8 v2, v2, -0x1
aget-object v0, v0, v2
invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V
const/high16 v0, 0x1000
invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
iget-object v0, p0, Landroid/support/v4/app/ap;->c:Landroid/content/Context;
invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
:cond_4d
return-void
.end method
.method public iterator()Ljava/util/Iterator;
.registers 2
iget-object v0, p0, Landroid/support/v4/app/ap;->b:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
move-result-object v0
return-object v0
.end method