.class public Landroid/support/v4/app/a;
.super Landroid/support/v4/a/a;
.source "ActivityCompat.java"
.method public static a(Landroid/app/Activity;)V
.registers 3
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x10
if-lt v0, v1, :cond_a
invoke-static {p0}, Landroid/support/v4/app/d;->a(Landroid/app/Activity;)V
:goto_9
return-void
:cond_a
invoke-virtual {p0}, Landroid/app/Activity;->finish()V
goto :goto_9
.end method
.method public static a(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V
.registers 6
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x10
if-lt v0, v1, :cond_a
invoke-static {p0, p1, p2, p3}, Landroid/support/v4/app/d;->a(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V
:goto_9
return-void
:cond_a
invoke-virtual {p0, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
goto :goto_9
.end method
.method public static a(Landroid/app/Activity;[Ljava/lang/String;I)V
.registers 5
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x17
if-lt v0, v1, :cond_a
invoke-static {p0, p1, p2}, Landroid/support/v4/app/b;->a(Landroid/app/Activity;[Ljava/lang/String;I)V
:cond_9
:goto_9
return-void
:cond_a
instance-of v0, p0, Landroid/support/v4/app/a$a;
if-eqz v0, :cond_9
new-instance v0, Landroid/os/Handler;
invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;
move-result-object v1
invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V
new-instance v1, Landroid/support/v4/app/a$1;
invoke-direct {v1, p1, p0, p2}, Landroid/support/v4/app/a$1;-><init>([Ljava/lang/String;Landroid/app/Activity;I)V
invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
goto :goto_9
.end method