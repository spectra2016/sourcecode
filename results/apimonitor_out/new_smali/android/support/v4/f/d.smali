.class public abstract Landroid/support/v4/f/d;
.super Ljava/lang/Object;
.source "ActionProvider.java"
.field private final a:Landroid/content/Context;
.field private b:Landroid/support/v4/f/d$a;
.field private c:Landroid/support/v4/f/d$b;
.method public constructor <init>(Landroid/content/Context;)V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Landroid/support/v4/f/d;->a:Landroid/content/Context;
return-void
.end method
.method public abstract a()Landroid/view/View;
.end method
.method public a(Landroid/view/MenuItem;)Landroid/view/View;
.registers 3
invoke-virtual {p0}, Landroid/support/v4/f/d;->a()Landroid/view/View;
move-result-object v0
return-object v0
.end method
.method public a(Landroid/support/v4/f/d$a;)V
.registers 2
iput-object p1, p0, Landroid/support/v4/f/d;->b:Landroid/support/v4/f/d$a;
return-void
.end method
.method public a(Landroid/support/v4/f/d$b;)V
.registers 5
iget-object v0, p0, Landroid/support/v4/f/d;->c:Landroid/support/v4/f/d$b;
if-eqz v0, :cond_2c
if-eqz p1, :cond_2c
const-string v0, "ActionProvider(support)"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, " instance while it is still in use somewhere else?"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
:cond_2c
iput-object p1, p0, Landroid/support/v4/f/d;->c:Landroid/support/v4/f/d$b;
return-void
.end method
.method public a(Landroid/view/SubMenu;)V
.registers 2
return-void
.end method
.method public a(Z)V
.registers 3
iget-object v0, p0, Landroid/support/v4/f/d;->b:Landroid/support/v4/f/d$a;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/v4/f/d;->b:Landroid/support/v4/f/d$a;
invoke-interface {v0, p1}, Landroid/support/v4/f/d$a;->a(Z)V
:cond_9
return-void
.end method
.method public b()Z
.registers 2
const/4 v0, 0x0
return v0
.end method
.method public c()Z
.registers 2
const/4 v0, 0x1
return v0
.end method
.method public d()Z
.registers 2
const/4 v0, 0x0
return v0
.end method
.method public e()Z
.registers 2
const/4 v0, 0x0
return v0
.end method
.method public f()V
.registers 2
const/4 v0, 0x0
iput-object v0, p0, Landroid/support/v4/f/d;->c:Landroid/support/v4/f/d$b;
iput-object v0, p0, Landroid/support/v4/f/d;->b:Landroid/support/v4/f/d$a;
return-void
.end method