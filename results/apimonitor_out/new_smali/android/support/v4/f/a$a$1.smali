.class  Landroid/support/v4/f/a$a$1;
.super Ljava/lang/Object;
.source "AccessibilityDelegateCompat.java"
.implements Landroid/support/v4/f/b$a;
.field final synthetic a:Landroid/support/v4/f/a;
.field final synthetic b:Landroid/support/v4/f/a$a;
.method constructor <init>(Landroid/support/v4/f/a$a;Landroid/support/v4/f/a;)V
.registers 3
iput-object p1, p0, Landroid/support/v4/f/a$a$1;->b:Landroid/support/v4/f/a$a;
iput-object p2, p0, Landroid/support/v4/f/a$a$1;->a:Landroid/support/v4/f/a;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public a(Landroid/view/View;I)V
.registers 4
iget-object v0, p0, Landroid/support/v4/f/a$a$1;->a:Landroid/support/v4/f/a;
invoke-virtual {v0, p1, p2}, Landroid/support/v4/f/a;->a(Landroid/view/View;I)V
return-void
.end method
.method public a(Landroid/view/View;Ljava/lang/Object;)V
.registers 5
iget-object v0, p0, Landroid/support/v4/f/a$a$1;->a:Landroid/support/v4/f/a;
new-instance v1, Landroid/support/v4/f/a/b;
invoke-direct {v1, p2}, Landroid/support/v4/f/a/b;-><init>(Ljava/lang/Object;)V
invoke-virtual {v0, p1, v1}, Landroid/support/v4/f/a;->a(Landroid/view/View;Landroid/support/v4/f/a/b;)V
return-void
.end method
.method public a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
.registers 4
iget-object v0, p0, Landroid/support/v4/f/a$a$1;->a:Landroid/support/v4/f/a;
invoke-virtual {v0, p1, p2}, Landroid/support/v4/f/a;->b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
move-result v0
return v0
.end method
.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
.registers 5
iget-object v0, p0, Landroid/support/v4/f/a$a$1;->a:Landroid/support/v4/f/a;
invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/f/a;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
move-result v0
return v0
.end method
.method public b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
.registers 4
iget-object v0, p0, Landroid/support/v4/f/a$a$1;->a:Landroid/support/v4/f/a;
invoke-virtual {v0, p1, p2}, Landroid/support/v4/f/a;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return-void
.end method
.method public c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
.registers 4
iget-object v0, p0, Landroid/support/v4/f/a$a$1;->a:Landroid/support/v4/f/a;
invoke-virtual {v0, p1, p2}, Landroid/support/v4/f/a;->c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return-void
.end method
.method public d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
.registers 4
iget-object v0, p0, Landroid/support/v4/f/a$a$1;->a:Landroid/support/v4/f/a;
invoke-virtual {v0, p1, p2}, Landroid/support/v4/f/a;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return-void
.end method