.class final Landroid/support/v4/f/b$1;
.super Landroid/view/View$AccessibilityDelegate;
.source "AccessibilityDelegateCompatIcs.java"
.field final synthetic a:Landroid/support/v4/f/b$a;
.method constructor <init>(Landroid/support/v4/f/b$a;)V
.registers 2
iput-object p1, p0, Landroid/support/v4/f/b$1;->a:Landroid/support/v4/f/b$a;
invoke-direct {p0}, Landroid/view/View$AccessibilityDelegate;-><init>()V
return-void
.end method
.method public dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
.registers 4
iget-object v0, p0, Landroid/support/v4/f/b$1;->a:Landroid/support/v4/f/b$a;
invoke-interface {v0, p1, p2}, Landroid/support/v4/f/b$a;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
move-result v0
return v0
.end method
.method public onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
.registers 4
iget-object v0, p0, Landroid/support/v4/f/b$1;->a:Landroid/support/v4/f/b$a;
invoke-interface {v0, p1, p2}, Landroid/support/v4/f/b$a;->b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return-void
.end method
.method public onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
.registers 4
iget-object v0, p0, Landroid/support/v4/f/b$1;->a:Landroid/support/v4/f/b$a;
invoke-interface {v0, p1, p2}, Landroid/support/v4/f/b$a;->a(Landroid/view/View;Ljava/lang/Object;)V
return-void
.end method
.method public onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
.registers 4
iget-object v0, p0, Landroid/support/v4/f/b$1;->a:Landroid/support/v4/f/b$a;
invoke-interface {v0, p1, p2}, Landroid/support/v4/f/b$a;->c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return-void
.end method
.method public onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
.registers 5
iget-object v0, p0, Landroid/support/v4/f/b$1;->a:Landroid/support/v4/f/b$a;
invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/f/b$a;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
move-result v0
return v0
.end method
.method public sendAccessibilityEvent(Landroid/view/View;I)V
.registers 4
iget-object v0, p0, Landroid/support/v4/f/b$1;->a:Landroid/support/v4/f/b$a;
invoke-interface {v0, p1, p2}, Landroid/support/v4/f/b$a;->a(Landroid/view/View;I)V
return-void
.end method
.method public sendAccessibilityEventUnchecked(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
.registers 4
iget-object v0, p0, Landroid/support/v4/f/b$1;->a:Landroid/support/v4/f/b$a;
invoke-interface {v0, p1, p2}, Landroid/support/v4/f/b$a;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return-void
.end method