.class public final Landroid/support/v4/f/n;
.super Ljava/lang/Object;
.source "MarginLayoutParamsCompat.java"
.field static final a:Landroid/support/v4/f/n$a;
.method static constructor <clinit>()V
.registers 2
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x11
if-lt v0, v1, :cond_e
new-instance v0, Landroid/support/v4/f/n$c;
invoke-direct {v0}, Landroid/support/v4/f/n$c;-><init>()V
sput-object v0, Landroid/support/v4/f/n;->a:Landroid/support/v4/f/n$a;
:goto_d
return-void
:cond_e
new-instance v0, Landroid/support/v4/f/n$b;
invoke-direct {v0}, Landroid/support/v4/f/n$b;-><init>()V
sput-object v0, Landroid/support/v4/f/n;->a:Landroid/support/v4/f/n$a;
goto :goto_d
.end method
.method public static a(Landroid/view/ViewGroup$MarginLayoutParams;)I
.registers 2
sget-object v0, Landroid/support/v4/f/n;->a:Landroid/support/v4/f/n$a;
invoke-interface {v0, p0}, Landroid/support/v4/f/n$a;->a(Landroid/view/ViewGroup$MarginLayoutParams;)I
move-result v0
return v0
.end method
.method public static b(Landroid/view/ViewGroup$MarginLayoutParams;)I
.registers 2
sget-object v0, Landroid/support/v4/f/n;->a:Landroid/support/v4/f/n$a;
invoke-interface {v0, p0}, Landroid/support/v4/f/n$a;->b(Landroid/view/ViewGroup$MarginLayoutParams;)I
move-result v0
return v0
.end method