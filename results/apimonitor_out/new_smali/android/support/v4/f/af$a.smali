.class  Landroid/support/v4/f/af$a;
.super Ljava/lang/Object;
.source "ViewCompat.java"
.implements Landroid/support/v4/f/af$m;
.field  a:Ljava/util/WeakHashMap;
.method constructor <init>()V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, 0x0
iput-object v0, p0, Landroid/support/v4/f/af$a;->a:Ljava/util/WeakHashMap;
return-void
.end method
.method private a(Landroid/support/v4/f/ab;I)Z
.registers 8
const/4 v0, 0x1
const/4 v1, 0x0
invoke-interface {p1}, Landroid/support/v4/f/ab;->computeVerticalScrollOffset()I
move-result v2
invoke-interface {p1}, Landroid/support/v4/f/ab;->computeVerticalScrollRange()I
move-result v3
invoke-interface {p1}, Landroid/support/v4/f/ab;->computeVerticalScrollExtent()I
move-result v4
sub-int/2addr v3, v4
if-nez v3, :cond_13
move v0, v1
:cond_12
:goto_12
return v0
:cond_13
if-gez p2, :cond_19
if-gtz v2, :cond_12
move v0, v1
goto :goto_12
:cond_19
add-int/lit8 v3, v3, -0x1
if-lt v2, v3, :cond_12
move v0, v1
goto :goto_12
.end method
.method public a(III)I
.registers 5
invoke-static {p1, p2}, Landroid/view/View;->resolveSize(II)I
move-result v0
return v0
.end method
.method public a(Landroid/view/View;)I
.registers 3
const/4 v0, 0x2
return v0
.end method
.method  a()J
.registers 3
const-wide/16 v0, 0xa
return-wide v0
.end method
.method public a(Landroid/view/View;Landroid/support/v4/f/bb;)Landroid/support/v4/f/bb;
.registers 3
return-object p2
.end method
.method public a(Landroid/view/View;F)V
.registers 3
return-void
.end method
.method public a(Landroid/view/View;II)V
.registers 4
return-void
.end method
.method public a(Landroid/view/View;ILandroid/graphics/Paint;)V
.registers 4
return-void
.end method
.method public a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
.registers 3
invoke-static {p1, p2}, Landroid/support/v4/f/ag;->a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
return-void
.end method
.method public a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
.registers 3
invoke-static {p1, p2}, Landroid/support/v4/f/ag;->a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
return-void
.end method
.method public a(Landroid/view/View;Landroid/support/v4/f/a;)V
.registers 3
return-void
.end method
.method public a(Landroid/view/View;Landroid/support/v4/f/aa;)V
.registers 3
return-void
.end method
.method public a(Landroid/view/View;Ljava/lang/Runnable;)V
.registers 5
invoke-virtual {p0}, Landroid/support/v4/f/af$a;->a()J
move-result-wide v0
invoke-virtual {p1, p2, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z
return-void
.end method
.method public a(Landroid/view/View;Ljava/lang/Runnable;J)V
.registers 8
invoke-virtual {p0}, Landroid/support/v4/f/af$a;->a()J
move-result-wide v0
add-long/2addr v0, p3
invoke-virtual {p1, p2, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z
return-void
.end method
.method public a(Landroid/view/View;Z)V
.registers 3
return-void
.end method
.method public a(Landroid/view/View;I)Z
.registers 4
instance-of v0, p1, Landroid/support/v4/f/ab;
if-eqz v0, :cond_e
check-cast p1, Landroid/support/v4/f/ab;
invoke-direct {p0, p1, p2}, Landroid/support/v4/f/af$a;->a(Landroid/support/v4/f/ab;I)Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method public b(Landroid/view/View;)V
.registers 2
invoke-virtual {p1}, Landroid/view/View;->invalidate()V
return-void
.end method
.method public b(Landroid/view/View;F)V
.registers 3
return-void
.end method
.method public b(Landroid/view/View;Z)V
.registers 3
return-void
.end method
.method public c(Landroid/view/View;)I
.registers 3
const/4 v0, 0x0
return v0
.end method
.method public c(Landroid/view/View;F)V
.registers 3
return-void
.end method
.method public d(Landroid/view/View;)I
.registers 3
const/4 v0, 0x0
return v0
.end method
.method public d(Landroid/view/View;F)V
.registers 3
return-void
.end method
.method public e(Landroid/view/View;)I
.registers 3
invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I
move-result v0
return v0
.end method
.method public f(Landroid/view/View;)I
.registers 3
const/4 v0, 0x0
return v0
.end method
.method public g(Landroid/view/View;)I
.registers 3
invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I
move-result v0
return v0
.end method
.method public h(Landroid/view/View;)I
.registers 3
invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I
move-result v0
return v0
.end method
.method public i(Landroid/view/View;)Z
.registers 3
const/4 v0, 0x1
return v0
.end method
.method public j(Landroid/view/View;)F
.registers 3
const/4 v0, 0x0
return v0
.end method
.method public k(Landroid/view/View;)F
.registers 3
const/4 v0, 0x0
return v0
.end method
.method public l(Landroid/view/View;)I
.registers 3
invoke-static {p1}, Landroid/support/v4/f/ag;->d(Landroid/view/View;)I
move-result v0
return v0
.end method
.method public m(Landroid/view/View;)Landroid/support/v4/f/au;
.registers 3
new-instance v0, Landroid/support/v4/f/au;
invoke-direct {v0, p1}, Landroid/support/v4/f/au;-><init>(Landroid/view/View;)V
return-object v0
.end method
.method public n(Landroid/view/View;)I
.registers 3
const/4 v0, 0x0
return v0
.end method
.method public o(Landroid/view/View;)V
.registers 2
return-void
.end method
.method public p(Landroid/view/View;)V
.registers 2
return-void
.end method
.method public q(Landroid/view/View;)Landroid/content/res/ColorStateList;
.registers 3
invoke-static {p1}, Landroid/support/v4/f/ag;->a(Landroid/view/View;)Landroid/content/res/ColorStateList;
move-result-object v0
return-object v0
.end method
.method public r(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;
.registers 3
invoke-static {p1}, Landroid/support/v4/f/ag;->b(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;
move-result-object v0
return-object v0
.end method
.method public s(Landroid/view/View;)V
.registers 3
instance-of v0, p1, Landroid/support/v4/f/w;
if-eqz v0, :cond_9
check-cast p1, Landroid/support/v4/f/w;
invoke-interface {p1}, Landroid/support/v4/f/w;->stopNestedScroll()V
:cond_9
return-void
.end method
.method public t(Landroid/view/View;)Z
.registers 3
invoke-static {p1}, Landroid/support/v4/f/ag;->c(Landroid/view/View;)Z
move-result v0
return v0
.end method
.method public u(Landroid/view/View;)Z
.registers 3
invoke-static {p1}, Landroid/support/v4/f/ag;->e(Landroid/view/View;)Z
move-result v0
return v0
.end method
.method public v(Landroid/view/View;)Z
.registers 3
const/4 v0, 0x0
return v0
.end method