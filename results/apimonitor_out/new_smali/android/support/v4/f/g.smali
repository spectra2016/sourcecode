.class public final Landroid/support/v4/f/g;
.super Ljava/lang/Object;
.source "KeyEventCompat.java"
.field static final a:Landroid/support/v4/f/g$d;
.method static constructor <clinit>()V
.registers 2
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0xb
if-lt v0, v1, :cond_e
new-instance v0, Landroid/support/v4/f/g$c;
invoke-direct {v0}, Landroid/support/v4/f/g$c;-><init>()V
sput-object v0, Landroid/support/v4/f/g;->a:Landroid/support/v4/f/g$d;
:goto_d
return-void
:cond_e
new-instance v0, Landroid/support/v4/f/g$a;
invoke-direct {v0}, Landroid/support/v4/f/g$a;-><init>()V
sput-object v0, Landroid/support/v4/f/g;->a:Landroid/support/v4/f/g$d;
goto :goto_d
.end method
.method public static a(Landroid/view/KeyEvent;I)Z
.registers 4
sget-object v0, Landroid/support/v4/f/g;->a:Landroid/support/v4/f/g$d;
invoke-virtual {p0}, Landroid/view/KeyEvent;->getMetaState()I
move-result v1
invoke-interface {v0, v1, p1}, Landroid/support/v4/f/g$d;->a(II)Z
move-result v0
return v0
.end method