.class  Landroid/support/v4/f/bc;
.super Landroid/support/v4/f/bb;
.source "WindowInsetsCompatApi21.java"
.field private final a:Landroid/view/WindowInsets;
.method constructor <init>(Landroid/view/WindowInsets;)V
.registers 2
invoke-direct {p0}, Landroid/support/v4/f/bb;-><init>()V
iput-object p1, p0, Landroid/support/v4/f/bc;->a:Landroid/view/WindowInsets;
return-void
.end method
.method public a()I
.registers 2
iget-object v0, p0, Landroid/support/v4/f/bc;->a:Landroid/view/WindowInsets;
invoke-virtual {v0}, Landroid/view/WindowInsets;->getSystemWindowInsetLeft()I
move-result v0
return v0
.end method
.method public a(IIII)Landroid/support/v4/f/bb;
.registers 7
new-instance v0, Landroid/support/v4/f/bc;
iget-object v1, p0, Landroid/support/v4/f/bc;->a:Landroid/view/WindowInsets;
invoke-virtual {v1, p1, p2, p3, p4}, Landroid/view/WindowInsets;->replaceSystemWindowInsets(IIII)Landroid/view/WindowInsets;
move-result-object v1
invoke-direct {v0, v1}, Landroid/support/v4/f/bc;-><init>(Landroid/view/WindowInsets;)V
return-object v0
.end method
.method public b()I
.registers 2
iget-object v0, p0, Landroid/support/v4/f/bc;->a:Landroid/view/WindowInsets;
invoke-virtual {v0}, Landroid/view/WindowInsets;->getSystemWindowInsetTop()I
move-result v0
return v0
.end method
.method public c()I
.registers 2
iget-object v0, p0, Landroid/support/v4/f/bc;->a:Landroid/view/WindowInsets;
invoke-virtual {v0}, Landroid/view/WindowInsets;->getSystemWindowInsetRight()I
move-result v0
return v0
.end method
.method public d()I
.registers 2
iget-object v0, p0, Landroid/support/v4/f/bc;->a:Landroid/view/WindowInsets;
invoke-virtual {v0}, Landroid/view/WindowInsets;->getSystemWindowInsetBottom()I
move-result v0
return v0
.end method
.method  e()Landroid/view/WindowInsets;
.registers 2
iget-object v0, p0, Landroid/support/v4/f/bc;->a:Landroid/view/WindowInsets;
return-object v0
.end method