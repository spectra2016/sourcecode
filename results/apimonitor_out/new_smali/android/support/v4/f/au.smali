.class public final Landroid/support/v4/f/au;
.super Ljava/lang/Object;
.source "ViewPropertyAnimatorCompat.java"
.field static final a:Landroid/support/v4/f/au$g;
.field private b:Ljava/lang/ref/WeakReference;
.field private c:Ljava/lang/Runnable;
.field private d:Ljava/lang/Runnable;
.field private e:I
.method static constructor <clinit>()V
.registers 2
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x15
if-lt v0, v1, :cond_e
new-instance v0, Landroid/support/v4/f/au$f;
invoke-direct {v0}, Landroid/support/v4/f/au$f;-><init>()V
sput-object v0, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
:goto_d
return-void
:cond_e
const/16 v1, 0x13
if-lt v0, v1, :cond_1a
new-instance v0, Landroid/support/v4/f/au$e;
invoke-direct {v0}, Landroid/support/v4/f/au$e;-><init>()V
sput-object v0, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
goto :goto_d
:cond_1a
const/16 v1, 0x12
if-lt v0, v1, :cond_26
new-instance v0, Landroid/support/v4/f/au$c;
invoke-direct {v0}, Landroid/support/v4/f/au$c;-><init>()V
sput-object v0, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
goto :goto_d
:cond_26
const/16 v1, 0x10
if-lt v0, v1, :cond_32
new-instance v0, Landroid/support/v4/f/au$d;
invoke-direct {v0}, Landroid/support/v4/f/au$d;-><init>()V
sput-object v0, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
goto :goto_d
:cond_32
const/16 v1, 0xe
if-lt v0, v1, :cond_3e
new-instance v0, Landroid/support/v4/f/au$b;
invoke-direct {v0}, Landroid/support/v4/f/au$b;-><init>()V
sput-object v0, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
goto :goto_d
:cond_3e
new-instance v0, Landroid/support/v4/f/au$a;
invoke-direct {v0}, Landroid/support/v4/f/au$a;-><init>()V
sput-object v0, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
goto :goto_d
.end method
.method constructor <init>(Landroid/view/View;)V
.registers 3
const/4 v0, 0x0
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object v0, p0, Landroid/support/v4/f/au;->c:Ljava/lang/Runnable;
iput-object v0, p0, Landroid/support/v4/f/au;->d:Ljava/lang/Runnable;
const/4 v0, -0x1
iput v0, p0, Landroid/support/v4/f/au;->e:I
new-instance v0, Ljava/lang/ref/WeakReference;
invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V
iput-object v0, p0, Landroid/support/v4/f/au;->b:Ljava/lang/ref/WeakReference;
return-void
.end method
.method static synthetic a(Landroid/support/v4/f/au;I)I
.registers 2
iput p1, p0, Landroid/support/v4/f/au;->e:I
return p1
.end method
.method static synthetic a(Landroid/support/v4/f/au;)Ljava/lang/Runnable;
.registers 2
iget-object v0, p0, Landroid/support/v4/f/au;->c:Ljava/lang/Runnable;
return-object v0
.end method
.method static synthetic a(Landroid/support/v4/f/au;Ljava/lang/Runnable;)Ljava/lang/Runnable;
.registers 2
iput-object p1, p0, Landroid/support/v4/f/au;->d:Ljava/lang/Runnable;
return-object p1
.end method
.method static synthetic b(Landroid/support/v4/f/au;)Ljava/lang/Runnable;
.registers 2
iget-object v0, p0, Landroid/support/v4/f/au;->d:Ljava/lang/Runnable;
return-object v0
.end method
.method static synthetic b(Landroid/support/v4/f/au;Ljava/lang/Runnable;)Ljava/lang/Runnable;
.registers 2
iput-object p1, p0, Landroid/support/v4/f/au;->c:Ljava/lang/Runnable;
return-object p1
.end method
.method static synthetic c(Landroid/support/v4/f/au;)I
.registers 2
iget v0, p0, Landroid/support/v4/f/au;->e:I
return v0
.end method
.method public a()J
.registers 3
iget-object v0, p0, Landroid/support/v4/f/au;->b:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_11
sget-object v1, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
invoke-interface {v1, p0, v0}, Landroid/support/v4/f/au$g;->a(Landroid/support/v4/f/au;Landroid/view/View;)J
move-result-wide v0
:goto_10
return-wide v0
:cond_11
const-wide/16 v0, 0x0
goto :goto_10
.end method
.method public a(F)Landroid/support/v4/f/au;
.registers 4
iget-object v0, p0, Landroid/support/v4/f/au;->b:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_f
sget-object v1, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/f/au$g;->a(Landroid/support/v4/f/au;Landroid/view/View;F)V
:cond_f
return-object p0
.end method
.method public a(J)Landroid/support/v4/f/au;
.registers 6
iget-object v0, p0, Landroid/support/v4/f/au;->b:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_f
sget-object v1, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
invoke-interface {v1, p0, v0, p1, p2}, Landroid/support/v4/f/au$g;->a(Landroid/support/v4/f/au;Landroid/view/View;J)V
:cond_f
return-object p0
.end method
.method public a(Landroid/support/v4/f/ay;)Landroid/support/v4/f/au;
.registers 4
iget-object v0, p0, Landroid/support/v4/f/au;->b:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_f
sget-object v1, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/f/au$g;->a(Landroid/support/v4/f/au;Landroid/view/View;Landroid/support/v4/f/ay;)V
:cond_f
return-object p0
.end method
.method public a(Landroid/support/v4/f/ba;)Landroid/support/v4/f/au;
.registers 4
iget-object v0, p0, Landroid/support/v4/f/au;->b:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_f
sget-object v1, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/f/au$g;->a(Landroid/support/v4/f/au;Landroid/view/View;Landroid/support/v4/f/ba;)V
:cond_f
return-object p0
.end method
.method public a(Landroid/view/animation/Interpolator;)Landroid/support/v4/f/au;
.registers 4
iget-object v0, p0, Landroid/support/v4/f/au;->b:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_f
sget-object v1, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/f/au$g;->a(Landroid/support/v4/f/au;Landroid/view/View;Landroid/view/animation/Interpolator;)V
:cond_f
return-object p0
.end method
.method public b(F)Landroid/support/v4/f/au;
.registers 4
iget-object v0, p0, Landroid/support/v4/f/au;->b:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_f
sget-object v1, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/f/au$g;->b(Landroid/support/v4/f/au;Landroid/view/View;F)V
:cond_f
return-object p0
.end method
.method public b(J)Landroid/support/v4/f/au;
.registers 6
iget-object v0, p0, Landroid/support/v4/f/au;->b:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_f
sget-object v1, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
invoke-interface {v1, p0, v0, p1, p2}, Landroid/support/v4/f/au$g;->b(Landroid/support/v4/f/au;Landroid/view/View;J)V
:cond_f
return-object p0
.end method
.method public b()V
.registers 3
iget-object v0, p0, Landroid/support/v4/f/au;->b:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_f
sget-object v1, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
invoke-interface {v1, p0, v0}, Landroid/support/v4/f/au$g;->b(Landroid/support/v4/f/au;Landroid/view/View;)V
:cond_f
return-void
.end method
.method public c(F)Landroid/support/v4/f/au;
.registers 4
iget-object v0, p0, Landroid/support/v4/f/au;->b:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_f
sget-object v1, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/f/au$g;->c(Landroid/support/v4/f/au;Landroid/view/View;F)V
:cond_f
return-object p0
.end method
.method public c()V
.registers 3
iget-object v0, p0, Landroid/support/v4/f/au;->b:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_f
sget-object v1, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
invoke-interface {v1, p0, v0}, Landroid/support/v4/f/au$g;->c(Landroid/support/v4/f/au;Landroid/view/View;)V
:cond_f
return-void
.end method
.method public d(F)Landroid/support/v4/f/au;
.registers 4
iget-object v0, p0, Landroid/support/v4/f/au;->b:Ljava/lang/ref/WeakReference;
invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
if-eqz v0, :cond_f
sget-object v1, Landroid/support/v4/f/au;->a:Landroid/support/v4/f/au$g;
invoke-interface {v1, p0, v0, p1}, Landroid/support/v4/f/au$g;->d(Landroid/support/v4/f/au;Landroid/view/View;F)V
:cond_f
return-object p0
.end method