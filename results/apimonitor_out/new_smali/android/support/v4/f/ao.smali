.class  Landroid/support/v4/f/ao;
.super Ljava/lang/Object;
.source "ViewCompatLollipop.java"
.method public static a(Landroid/view/View;Landroid/support/v4/f/bb;)Landroid/support/v4/f/bb;
.registers 4
instance-of v0, p1, Landroid/support/v4/f/bc;
if-eqz v0, :cond_16
move-object v0, p1
check-cast v0, Landroid/support/v4/f/bc;
invoke-virtual {v0}, Landroid/support/v4/f/bc;->e()Landroid/view/WindowInsets;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/view/View;->onApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
move-result-object v1
if-eq v1, v0, :cond_16
new-instance p1, Landroid/support/v4/f/bc;
invoke-direct {p1, v1}, Landroid/support/v4/f/bc;-><init>(Landroid/view/WindowInsets;)V
:cond_16
return-object p1
.end method
.method public static a(Landroid/view/View;)V
.registers 1
invoke-virtual {p0}, Landroid/view/View;->requestApplyInsets()V
return-void
.end method
.method public static a(Landroid/view/View;F)V
.registers 2
invoke-virtual {p0, p1}, Landroid/view/View;->setElevation(F)V
return-void
.end method
.method static a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
.registers 4
invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x15
if-ne v0, v1, :cond_2e
invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;
move-result-object v1
invoke-virtual {p0}, Landroid/view/View;->getBackgroundTintList()Landroid/content/res/ColorStateList;
move-result-object v0
if-eqz v0, :cond_2f
invoke-virtual {p0}, Landroid/view/View;->getBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
move-result-object v0
if-eqz v0, :cond_2f
const/4 v0, 0x1
:goto_1a
if-eqz v1, :cond_2e
if-eqz v0, :cond_2e
invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z
move-result v0
if-eqz v0, :cond_2b
invoke-virtual {p0}, Landroid/view/View;->getDrawableState()[I
move-result-object v0
invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z
:cond_2b
invoke-virtual {p0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V
:cond_2e
return-void
:cond_2f
const/4 v0, 0x0
goto :goto_1a
.end method
.method static a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
.registers 4
invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x15
if-ne v0, v1, :cond_2e
invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;
move-result-object v1
invoke-virtual {p0}, Landroid/view/View;->getBackgroundTintList()Landroid/content/res/ColorStateList;
move-result-object v0
if-eqz v0, :cond_2f
invoke-virtual {p0}, Landroid/view/View;->getBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
move-result-object v0
if-eqz v0, :cond_2f
const/4 v0, 0x1
:goto_1a
if-eqz v1, :cond_2e
if-eqz v0, :cond_2e
invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z
move-result v0
if-eqz v0, :cond_2b
invoke-virtual {p0}, Landroid/view/View;->getDrawableState()[I
move-result-object v0
invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z
:cond_2b
invoke-virtual {p0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V
:cond_2e
return-void
:cond_2f
const/4 v0, 0x0
goto :goto_1a
.end method
.method public static a(Landroid/view/View;Landroid/support/v4/f/aa;)V
.registers 3
if-nez p1, :cond_7
const/4 v0, 0x0
invoke-virtual {p0, v0}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V
:goto_6
return-void
:cond_7
new-instance v0, Landroid/support/v4/f/ao$1;
invoke-direct {v0, p1}, Landroid/support/v4/f/ao$1;-><init>(Landroid/support/v4/f/aa;)V
invoke-virtual {p0, v0}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V
goto :goto_6
.end method
.method static b(Landroid/view/View;)Landroid/content/res/ColorStateList;
.registers 2
invoke-virtual {p0}, Landroid/view/View;->getBackgroundTintList()Landroid/content/res/ColorStateList;
move-result-object v0
return-object v0
.end method
.method static c(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;
.registers 2
invoke-virtual {p0}, Landroid/view/View;->getBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
move-result-object v0
return-object v0
.end method
.method public static d(Landroid/view/View;)V
.registers 1
invoke-virtual {p0}, Landroid/view/View;->stopNestedScroll()V
return-void
.end method