.class  Landroid/support/v4/f/au$a;
.super Ljava/lang/Object;
.source "ViewPropertyAnimatorCompat.java"
.implements Landroid/support/v4/f/au$g;
.field  a:Ljava/util/WeakHashMap;
.method constructor <init>()V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, 0x0
iput-object v0, p0, Landroid/support/v4/f/au$a;->a:Ljava/util/WeakHashMap;
return-void
.end method
.method static synthetic a(Landroid/support/v4/f/au$a;Landroid/support/v4/f/au;Landroid/view/View;)V
.registers 3
invoke-direct {p0, p1, p2}, Landroid/support/v4/f/au$a;->d(Landroid/support/v4/f/au;Landroid/view/View;)V
return-void
.end method
.method private a(Landroid/view/View;)V
.registers 3
iget-object v0, p0, Landroid/support/v4/f/au$a;->a:Ljava/util/WeakHashMap;
if-eqz v0, :cond_11
iget-object v0, p0, Landroid/support/v4/f/au$a;->a:Ljava/util/WeakHashMap;
invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Runnable;
if-eqz v0, :cond_11
invoke-virtual {p1, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z
:cond_11
return-void
.end method
.method private d(Landroid/support/v4/f/au;Landroid/view/View;)V
.registers 7
const/4 v1, 0x0
const/high16 v0, 0x7e00
invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;
move-result-object v0
instance-of v2, v0, Landroid/support/v4/f/ay;
if-eqz v2, :cond_37
check-cast v0, Landroid/support/v4/f/ay;
:goto_d
invoke-static {p1}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/au;)Ljava/lang/Runnable;
move-result-object v2
invoke-static {p1}, Landroid/support/v4/f/au;->b(Landroid/support/v4/f/au;)Ljava/lang/Runnable;
move-result-object v3
invoke-static {p1, v1}, Landroid/support/v4/f/au;->b(Landroid/support/v4/f/au;Ljava/lang/Runnable;)Ljava/lang/Runnable;
invoke-static {p1, v1}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/au;Ljava/lang/Runnable;)Ljava/lang/Runnable;
if-eqz v2, :cond_20
invoke-interface {v2}, Ljava/lang/Runnable;->run()V
:cond_20
if-eqz v0, :cond_28
invoke-interface {v0, p2}, Landroid/support/v4/f/ay;->a(Landroid/view/View;)V
invoke-interface {v0, p2}, Landroid/support/v4/f/ay;->b(Landroid/view/View;)V
:cond_28
if-eqz v3, :cond_2d
invoke-interface {v3}, Ljava/lang/Runnable;->run()V
:cond_2d
iget-object v0, p0, Landroid/support/v4/f/au$a;->a:Ljava/util/WeakHashMap;
if-eqz v0, :cond_36
iget-object v0, p0, Landroid/support/v4/f/au$a;->a:Ljava/util/WeakHashMap;
invoke-virtual {v0, p2}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
:cond_36
return-void
:cond_37
move-object v0, v1
goto :goto_d
.end method
.method private e(Landroid/support/v4/f/au;Landroid/view/View;)V
.registers 5
const/4 v1, 0x0
iget-object v0, p0, Landroid/support/v4/f/au$a;->a:Ljava/util/WeakHashMap;
if-eqz v0, :cond_2b
iget-object v0, p0, Landroid/support/v4/f/au$a;->a:Ljava/util/WeakHashMap;
invoke-virtual {v0, p2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Runnable;
:goto_d
if-nez v0, :cond_24
new-instance v0, Landroid/support/v4/f/au$a$a;
invoke-direct {v0, p0, p1, p2, v1}, Landroid/support/v4/f/au$a$a;-><init>(Landroid/support/v4/f/au$a;Landroid/support/v4/f/au;Landroid/view/View;Landroid/support/v4/f/au$1;)V
iget-object v1, p0, Landroid/support/v4/f/au$a;->a:Ljava/util/WeakHashMap;
if-nez v1, :cond_1f
new-instance v1, Ljava/util/WeakHashMap;
invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V
iput-object v1, p0, Landroid/support/v4/f/au$a;->a:Ljava/util/WeakHashMap;
:cond_1f
iget-object v1, p0, Landroid/support/v4/f/au$a;->a:Ljava/util/WeakHashMap;
invoke-virtual {v1, p2, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_24
invoke-virtual {p2, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z
invoke-virtual {p2, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z
return-void
:cond_2b
move-object v0, v1
goto :goto_d
.end method
.method public a(Landroid/support/v4/f/au;Landroid/view/View;)J
.registers 5
const-wide/16 v0, 0x0
return-wide v0
.end method
.method public a(Landroid/support/v4/f/au;Landroid/view/View;F)V
.registers 4
invoke-direct {p0, p1, p2}, Landroid/support/v4/f/au$a;->e(Landroid/support/v4/f/au;Landroid/view/View;)V
return-void
.end method
.method public a(Landroid/support/v4/f/au;Landroid/view/View;J)V
.registers 5
return-void
.end method
.method public a(Landroid/support/v4/f/au;Landroid/view/View;Landroid/support/v4/f/ay;)V
.registers 5
const/high16 v0, 0x7e00
invoke-virtual {p2, v0, p3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V
return-void
.end method
.method public a(Landroid/support/v4/f/au;Landroid/view/View;Landroid/support/v4/f/ba;)V
.registers 4
return-void
.end method
.method public a(Landroid/support/v4/f/au;Landroid/view/View;Landroid/view/animation/Interpolator;)V
.registers 4
return-void
.end method
.method public b(Landroid/support/v4/f/au;Landroid/view/View;)V
.registers 3
invoke-direct {p0, p1, p2}, Landroid/support/v4/f/au$a;->e(Landroid/support/v4/f/au;Landroid/view/View;)V
return-void
.end method
.method public b(Landroid/support/v4/f/au;Landroid/view/View;F)V
.registers 4
invoke-direct {p0, p1, p2}, Landroid/support/v4/f/au$a;->e(Landroid/support/v4/f/au;Landroid/view/View;)V
return-void
.end method
.method public b(Landroid/support/v4/f/au;Landroid/view/View;J)V
.registers 5
return-void
.end method
.method public c(Landroid/support/v4/f/au;Landroid/view/View;)V
.registers 3
invoke-direct {p0, p2}, Landroid/support/v4/f/au$a;->a(Landroid/view/View;)V
invoke-direct {p0, p1, p2}, Landroid/support/v4/f/au$a;->d(Landroid/support/v4/f/au;Landroid/view/View;)V
return-void
.end method
.method public c(Landroid/support/v4/f/au;Landroid/view/View;F)V
.registers 4
invoke-direct {p0, p1, p2}, Landroid/support/v4/f/au$a;->e(Landroid/support/v4/f/au;Landroid/view/View;)V
return-void
.end method
.method public d(Landroid/support/v4/f/au;Landroid/view/View;F)V
.registers 4
invoke-direct {p0, p1, p2}, Landroid/support/v4/f/au$a;->e(Landroid/support/v4/f/au;Landroid/view/View;)V
return-void
.end method