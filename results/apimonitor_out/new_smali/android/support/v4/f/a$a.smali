.class  Landroid/support/v4/f/a$a;
.super Landroid/support/v4/f/a$d;
.source "AccessibilityDelegateCompat.java"
.method constructor <init>()V
.registers 1
invoke-direct {p0}, Landroid/support/v4/f/a$d;-><init>()V
return-void
.end method
.method public a()Ljava/lang/Object;
.registers 2
invoke-static {}, Landroid/support/v4/f/b;->a()Ljava/lang/Object;
move-result-object v0
return-object v0
.end method
.method public a(Landroid/support/v4/f/a;)Ljava/lang/Object;
.registers 3
new-instance v0, Landroid/support/v4/f/a$a$1;
invoke-direct {v0, p0, p1}, Landroid/support/v4/f/a$a$1;-><init>(Landroid/support/v4/f/a$a;Landroid/support/v4/f/a;)V
invoke-static {v0}, Landroid/support/v4/f/b;->a(Landroid/support/v4/f/b$a;)Ljava/lang/Object;
move-result-object v0
return-object v0
.end method
.method public a(Ljava/lang/Object;Landroid/view/View;I)V
.registers 4
invoke-static {p1, p2, p3}, Landroid/support/v4/f/b;->a(Ljava/lang/Object;Landroid/view/View;I)V
return-void
.end method
.method public a(Ljava/lang/Object;Landroid/view/View;Landroid/support/v4/f/a/b;)V
.registers 5
invoke-virtual {p3}, Landroid/support/v4/f/a/b;->a()Ljava/lang/Object;
move-result-object v0
invoke-static {p1, p2, v0}, Landroid/support/v4/f/b;->a(Ljava/lang/Object;Landroid/view/View;Ljava/lang/Object;)V
return-void
.end method
.method public a(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
.registers 5
invoke-static {p1, p2, p3}, Landroid/support/v4/f/b;->a(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
move-result v0
return v0
.end method
.method public a(Ljava/lang/Object;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
.registers 6
invoke-static {p1, p2, p3, p4}, Landroid/support/v4/f/b;->a(Ljava/lang/Object;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
move-result v0
return v0
.end method
.method public b(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
.registers 4
invoke-static {p1, p2, p3}, Landroid/support/v4/f/b;->b(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return-void
.end method
.method public c(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
.registers 4
invoke-static {p1, p2, p3}, Landroid/support/v4/f/b;->c(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return-void
.end method
.method public d(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
.registers 4
invoke-static {p1, p2, p3}, Landroid/support/v4/f/b;->d(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return-void
.end method