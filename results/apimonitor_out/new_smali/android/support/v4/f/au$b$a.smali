.class  Landroid/support/v4/f/au$b$a;
.super Ljava/lang/Object;
.source "ViewPropertyAnimatorCompat.java"
.implements Landroid/support/v4/f/ay;
.field  a:Landroid/support/v4/f/au;
.field  b:Z
.method constructor <init>(Landroid/support/v4/f/au;)V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Landroid/support/v4/f/au$b$a;->a:Landroid/support/v4/f/au;
return-void
.end method
.method public a(Landroid/view/View;)V
.registers 5
const/4 v1, 0x0
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v4/f/au$b$a;->b:Z
iget-object v0, p0, Landroid/support/v4/f/au$b$a;->a:Landroid/support/v4/f/au;
invoke-static {v0}, Landroid/support/v4/f/au;->c(Landroid/support/v4/f/au;)I
move-result v0
if-ltz v0, :cond_10
const/4 v0, 0x2
invoke-static {p1, v0, v1}, Landroid/support/v4/f/af;->a(Landroid/view/View;ILandroid/graphics/Paint;)V
:cond_10
iget-object v0, p0, Landroid/support/v4/f/au$b$a;->a:Landroid/support/v4/f/au;
invoke-static {v0}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/au;)Ljava/lang/Runnable;
move-result-object v0
if-eqz v0, :cond_26
iget-object v0, p0, Landroid/support/v4/f/au$b$a;->a:Landroid/support/v4/f/au;
invoke-static {v0}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/au;)Ljava/lang/Runnable;
move-result-object v0
iget-object v2, p0, Landroid/support/v4/f/au$b$a;->a:Landroid/support/v4/f/au;
invoke-static {v2, v1}, Landroid/support/v4/f/au;->b(Landroid/support/v4/f/au;Ljava/lang/Runnable;)Ljava/lang/Runnable;
invoke-interface {v0}, Ljava/lang/Runnable;->run()V
:cond_26
const/high16 v0, 0x7e00
invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;
move-result-object v0
instance-of v2, v0, Landroid/support/v4/f/ay;
if-eqz v2, :cond_38
check-cast v0, Landroid/support/v4/f/ay;
:goto_32
if-eqz v0, :cond_37
invoke-interface {v0, p1}, Landroid/support/v4/f/ay;->a(Landroid/view/View;)V
:cond_37
return-void
:cond_38
move-object v0, v1
goto :goto_32
.end method
.method public b(Landroid/view/View;)V
.registers 5
const/4 v1, 0x0
iget-object v0, p0, Landroid/support/v4/f/au$b$a;->a:Landroid/support/v4/f/au;
invoke-static {v0}, Landroid/support/v4/f/au;->c(Landroid/support/v4/f/au;)I
move-result v0
if-ltz v0, :cond_18
iget-object v0, p0, Landroid/support/v4/f/au$b$a;->a:Landroid/support/v4/f/au;
invoke-static {v0}, Landroid/support/v4/f/au;->c(Landroid/support/v4/f/au;)I
move-result v0
invoke-static {p1, v0, v1}, Landroid/support/v4/f/af;->a(Landroid/view/View;ILandroid/graphics/Paint;)V
iget-object v0, p0, Landroid/support/v4/f/au$b$a;->a:Landroid/support/v4/f/au;
const/4 v2, -0x1
invoke-static {v0, v2}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/au;I)I
:cond_18
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v2, 0x10
if-ge v0, v2, :cond_22
iget-boolean v0, p0, Landroid/support/v4/f/au$b$a;->b:Z
if-nez v0, :cond_4c
:cond_22
iget-object v0, p0, Landroid/support/v4/f/au$b$a;->a:Landroid/support/v4/f/au;
invoke-static {v0}, Landroid/support/v4/f/au;->b(Landroid/support/v4/f/au;)Ljava/lang/Runnable;
move-result-object v0
if-eqz v0, :cond_38
iget-object v0, p0, Landroid/support/v4/f/au$b$a;->a:Landroid/support/v4/f/au;
invoke-static {v0}, Landroid/support/v4/f/au;->b(Landroid/support/v4/f/au;)Ljava/lang/Runnable;
move-result-object v0
iget-object v2, p0, Landroid/support/v4/f/au$b$a;->a:Landroid/support/v4/f/au;
invoke-static {v2, v1}, Landroid/support/v4/f/au;->a(Landroid/support/v4/f/au;Ljava/lang/Runnable;)Ljava/lang/Runnable;
invoke-interface {v0}, Ljava/lang/Runnable;->run()V
:cond_38
const/high16 v0, 0x7e00
invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;
move-result-object v0
instance-of v2, v0, Landroid/support/v4/f/ay;
if-eqz v2, :cond_4d
check-cast v0, Landroid/support/v4/f/ay;
:goto_44
if-eqz v0, :cond_49
invoke-interface {v0, p1}, Landroid/support/v4/f/ay;->b(Landroid/view/View;)V
:cond_49
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v4/f/au$b$a;->b:Z
:cond_4c
return-void
:cond_4d
move-object v0, v1
goto :goto_44
.end method
.method public c(Landroid/view/View;)V
.registers 5
const/high16 v0, 0x7e00
invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;
move-result-object v0
const/4 v1, 0x0
instance-of v2, v0, Landroid/support/v4/f/ay;
if-eqz v2, :cond_13
check-cast v0, Landroid/support/v4/f/ay;
:goto_d
if-eqz v0, :cond_12
invoke-interface {v0, p1}, Landroid/support/v4/f/ay;->c(Landroid/view/View;)V
:cond_12
return-void
:cond_13
move-object v0, v1
goto :goto_d
.end method