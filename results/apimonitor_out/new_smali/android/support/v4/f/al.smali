.class  Landroid/support/v4/f/al;
.super Ljava/lang/Object;
.source "ViewCompatJB.java"
.method public static a(Landroid/view/View;)V
.registers 1
invoke-virtual {p0}, Landroid/view/View;->postInvalidateOnAnimation()V
return-void
.end method
.method public static a(Landroid/view/View;Ljava/lang/Runnable;)V
.registers 2
invoke-virtual {p0, p1}, Landroid/view/View;->postOnAnimation(Ljava/lang/Runnable;)V
return-void
.end method
.method public static a(Landroid/view/View;Ljava/lang/Runnable;J)V
.registers 4
invoke-virtual {p0, p1, p2, p3}, Landroid/view/View;->postOnAnimationDelayed(Ljava/lang/Runnable;J)V
return-void
.end method
.method public static b(Landroid/view/View;)I
.registers 2
invoke-virtual {p0}, Landroid/view/View;->getMinimumHeight()I
move-result v0
return v0
.end method
.method public static c(Landroid/view/View;)V
.registers 1
invoke-virtual {p0}, Landroid/view/View;->requestFitSystemWindows()V
return-void
.end method
.method public static d(Landroid/view/View;)Z
.registers 2
invoke-virtual {p0}, Landroid/view/View;->hasOverlappingRendering()Z
move-result v0
return v0
.end method