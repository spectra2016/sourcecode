.class  Landroid/support/v4/d/b;
.super Ljava/lang/Object;
.source "ICUCompatApi23.java"
.field private static a:Ljava/lang/reflect/Method;
.method static constructor <clinit>()V
.registers 5
:try_start_0
const-string v0, "libcore.icu.ICU"
invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
const-string v1, "addLikelySubtags"
const/4 v2, 0x1
new-array v2, v2, [Ljava/lang/Class;
const/4 v3, 0x0
const-class v4, Ljava/util/Locale;
aput-object v4, v2, v3
invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
move-result-object v0
sput-object v0, Landroid/support/v4/d/b;->a:Ljava/lang/reflect/Method;
:try_end_16
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_16} :catch_17
return-void
:catch_17
move-exception v0
new-instance v1, Ljava/lang/IllegalStateException;
invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V
throw v1
.end method
.method public static a(Ljava/util/Locale;)Ljava/lang/String;
.registers 4
const/4 v0, 0x1
:try_start_1
new-array v0, v0, [Ljava/lang/Object;
const/4 v1, 0x0
aput-object p0, v0, v1
sget-object v1, Landroid/support/v4/d/b;->a:Ljava/lang/reflect/Method;
const/4 v2, 0x0
invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/util/Locale;
invoke-virtual {v0}, Ljava/util/Locale;->getScript()Ljava/lang/String;
:try_end_12
.catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_12} :catch_14
.catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_12} :catch_1f
move-result-object v0
:goto_13
return-object v0
:catch_14
move-exception v0
const-string v1, "ICUCompatIcs"
invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I
:goto_1a
invoke-virtual {p0}, Ljava/util/Locale;->getScript()Ljava/lang/String;
move-result-object v0
goto :goto_13
:catch_1f
move-exception v0
const-string v1, "ICUCompatIcs"
invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I
goto :goto_1a
.end method