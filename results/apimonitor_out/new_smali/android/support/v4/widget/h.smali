.class public final Landroid/support/v4/widget/h;
.super Ljava/lang/Object;
.source "EdgeEffectCompat.java"
.field private static final b:Landroid/support/v4/widget/h$c;
.field private a:Ljava/lang/Object;
.method static constructor <clinit>()V
.registers 2
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x15
if-lt v0, v1, :cond_e
new-instance v0, Landroid/support/v4/widget/h$d;
invoke-direct {v0}, Landroid/support/v4/widget/h$d;-><init>()V
sput-object v0, Landroid/support/v4/widget/h;->b:Landroid/support/v4/widget/h$c;
:goto_d
return-void
:cond_e
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0xe
if-lt v0, v1, :cond_1c
new-instance v0, Landroid/support/v4/widget/h$b;
invoke-direct {v0}, Landroid/support/v4/widget/h$b;-><init>()V
sput-object v0, Landroid/support/v4/widget/h;->b:Landroid/support/v4/widget/h$c;
goto :goto_d
:cond_1c
new-instance v0, Landroid/support/v4/widget/h$a;
invoke-direct {v0}, Landroid/support/v4/widget/h$a;-><init>()V
sput-object v0, Landroid/support/v4/widget/h;->b:Landroid/support/v4/widget/h$c;
goto :goto_d
.end method
.method public constructor <init>(Landroid/content/Context;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
sget-object v0, Landroid/support/v4/widget/h;->b:Landroid/support/v4/widget/h$c;
invoke-interface {v0, p1}, Landroid/support/v4/widget/h$c;->a(Landroid/content/Context;)Ljava/lang/Object;
move-result-object v0
iput-object v0, p0, Landroid/support/v4/widget/h;->a:Ljava/lang/Object;
return-void
.end method
.method public a(II)V
.registers 5
sget-object v0, Landroid/support/v4/widget/h;->b:Landroid/support/v4/widget/h$c;
iget-object v1, p0, Landroid/support/v4/widget/h;->a:Ljava/lang/Object;
invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/widget/h$c;->a(Ljava/lang/Object;II)V
return-void
.end method
.method public a()Z
.registers 3
sget-object v0, Landroid/support/v4/widget/h;->b:Landroid/support/v4/widget/h$c;
iget-object v1, p0, Landroid/support/v4/widget/h;->a:Ljava/lang/Object;
invoke-interface {v0, v1}, Landroid/support/v4/widget/h$c;->a(Ljava/lang/Object;)Z
move-result v0
return v0
.end method
.method public a(FF)Z
.registers 5
sget-object v0, Landroid/support/v4/widget/h;->b:Landroid/support/v4/widget/h$c;
iget-object v1, p0, Landroid/support/v4/widget/h;->a:Ljava/lang/Object;
invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/widget/h$c;->a(Ljava/lang/Object;FF)Z
move-result v0
return v0
.end method
.method public a(I)Z
.registers 4
sget-object v0, Landroid/support/v4/widget/h;->b:Landroid/support/v4/widget/h$c;
iget-object v1, p0, Landroid/support/v4/widget/h;->a:Ljava/lang/Object;
invoke-interface {v0, v1, p1}, Landroid/support/v4/widget/h$c;->a(Ljava/lang/Object;I)Z
move-result v0
return v0
.end method
.method public a(Landroid/graphics/Canvas;)Z
.registers 4
sget-object v0, Landroid/support/v4/widget/h;->b:Landroid/support/v4/widget/h$c;
iget-object v1, p0, Landroid/support/v4/widget/h;->a:Ljava/lang/Object;
invoke-interface {v0, v1, p1}, Landroid/support/v4/widget/h$c;->a(Ljava/lang/Object;Landroid/graphics/Canvas;)Z
move-result v0
return v0
.end method
.method public b()Z
.registers 3
sget-object v0, Landroid/support/v4/widget/h;->b:Landroid/support/v4/widget/h$c;
iget-object v1, p0, Landroid/support/v4/widget/h;->a:Ljava/lang/Object;
invoke-interface {v0, v1}, Landroid/support/v4/widget/h$c;->b(Ljava/lang/Object;)Z
move-result v0
return v0
.end method