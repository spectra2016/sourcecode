.class  Landroid/support/v4/widget/e;
.super Ljava/lang/Object;
.source "CompoundButtonCompatLollipop.java"
.method static a(Landroid/widget/CompoundButton;Landroid/content/res/ColorStateList;)V
.registers 2
invoke-virtual {p0, p1}, Landroid/widget/CompoundButton;->setButtonTintList(Landroid/content/res/ColorStateList;)V
return-void
.end method
.method static a(Landroid/widget/CompoundButton;Landroid/graphics/PorterDuff$Mode;)V
.registers 2
invoke-virtual {p0, p1}, Landroid/widget/CompoundButton;->setButtonTintMode(Landroid/graphics/PorterDuff$Mode;)V
return-void
.end method