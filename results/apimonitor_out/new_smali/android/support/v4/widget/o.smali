.class public final Landroid/support/v4/widget/o;
.super Ljava/lang/Object;
.source "PopupWindowCompat.java"
.field static final a:Landroid/support/v4/widget/o$f;
.method static constructor <clinit>()V
.registers 2
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x17
if-lt v0, v1, :cond_e
new-instance v0, Landroid/support/v4/widget/o$b;
invoke-direct {v0}, Landroid/support/v4/widget/o$b;-><init>()V
sput-object v0, Landroid/support/v4/widget/o;->a:Landroid/support/v4/widget/o$f;
:goto_d
return-void
:cond_e
const/16 v1, 0x15
if-lt v0, v1, :cond_1a
new-instance v0, Landroid/support/v4/widget/o$a;
invoke-direct {v0}, Landroid/support/v4/widget/o$a;-><init>()V
sput-object v0, Landroid/support/v4/widget/o;->a:Landroid/support/v4/widget/o$f;
goto :goto_d
:cond_1a
const/16 v1, 0x13
if-lt v0, v1, :cond_26
new-instance v0, Landroid/support/v4/widget/o$e;
invoke-direct {v0}, Landroid/support/v4/widget/o$e;-><init>()V
sput-object v0, Landroid/support/v4/widget/o;->a:Landroid/support/v4/widget/o$f;
goto :goto_d
:cond_26
const/16 v1, 0x9
if-lt v0, v1, :cond_32
new-instance v0, Landroid/support/v4/widget/o$d;
invoke-direct {v0}, Landroid/support/v4/widget/o$d;-><init>()V
sput-object v0, Landroid/support/v4/widget/o;->a:Landroid/support/v4/widget/o$f;
goto :goto_d
:cond_32
new-instance v0, Landroid/support/v4/widget/o$c;
invoke-direct {v0}, Landroid/support/v4/widget/o$c;-><init>()V
sput-object v0, Landroid/support/v4/widget/o;->a:Landroid/support/v4/widget/o$f;
goto :goto_d
.end method
.method public static a(Landroid/widget/PopupWindow;I)V
.registers 3
sget-object v0, Landroid/support/v4/widget/o;->a:Landroid/support/v4/widget/o$f;
invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/o$f;->a(Landroid/widget/PopupWindow;I)V
return-void
.end method
.method public static a(Landroid/widget/PopupWindow;Landroid/view/View;III)V
.registers 11
sget-object v0, Landroid/support/v4/widget/o;->a:Landroid/support/v4/widget/o$f;
move-object v1, p0
move-object v2, p1
move v3, p2
move v4, p3
move v5, p4
invoke-interface/range {v0 .. v5}, Landroid/support/v4/widget/o$f;->a(Landroid/widget/PopupWindow;Landroid/view/View;III)V
return-void
.end method
.method public static a(Landroid/widget/PopupWindow;Z)V
.registers 3
sget-object v0, Landroid/support/v4/widget/o;->a:Landroid/support/v4/widget/o$f;
invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/o$f;->a(Landroid/widget/PopupWindow;Z)V
return-void
.end method