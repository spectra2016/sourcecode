.class  Landroid/support/v4/widget/u$b;
.super Ljava/lang/Object;
.source "ScrollerCompat.java"
.implements Landroid/support/v4/widget/u$a;
.method constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Ljava/lang/Object;
.registers 4
if-eqz p2, :cond_8
new-instance v0, Landroid/widget/Scroller;
invoke-direct {v0, p1, p2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
:goto_7
return-object v0
:cond_8
new-instance v0, Landroid/widget/Scroller;
invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V
goto :goto_7
.end method
.method public a(Ljava/lang/Object;IIII)V
.registers 6
check-cast p1, Landroid/widget/Scroller;
invoke-virtual {p1, p2, p3, p4, p5}, Landroid/widget/Scroller;->startScroll(IIII)V
return-void
.end method
.method public a(Ljava/lang/Object;IIIIIIII)V
.registers 19
move-object v0, p1
check-cast v0, Landroid/widget/Scroller;
move v1, p2
move v2, p3
move v3, p4
move v4, p5
move v5, p6
move/from16 v6, p7
move/from16 v7, p8
move/from16 v8, p9
invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V
return-void
.end method
.method public a(Ljava/lang/Object;IIIIIIIIII)V
.registers 21
move-object v0, p1
check-cast v0, Landroid/widget/Scroller;
move v1, p2
move v2, p3
move v3, p4
move v4, p5
move v5, p6
move/from16 v6, p7
move/from16 v7, p8
move/from16 v8, p9
invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V
return-void
.end method
.method public a(Ljava/lang/Object;)Z
.registers 3
check-cast p1, Landroid/widget/Scroller;
invoke-virtual {p1}, Landroid/widget/Scroller;->isFinished()Z
move-result v0
return v0
.end method
.method public a(Ljava/lang/Object;IIIIII)Z
.registers 9
const/4 v0, 0x0
return v0
.end method
.method public b(Ljava/lang/Object;)I
.registers 3
check-cast p1, Landroid/widget/Scroller;
invoke-virtual {p1}, Landroid/widget/Scroller;->getCurrX()I
move-result v0
return v0
.end method
.method public c(Ljava/lang/Object;)I
.registers 3
check-cast p1, Landroid/widget/Scroller;
invoke-virtual {p1}, Landroid/widget/Scroller;->getCurrY()I
move-result v0
return v0
.end method
.method public d(Ljava/lang/Object;)F
.registers 3
const/4 v0, 0x0
return v0
.end method
.method public e(Ljava/lang/Object;)Z
.registers 3
check-cast p1, Landroid/widget/Scroller;
invoke-virtual {p1}, Landroid/widget/Scroller;->computeScrollOffset()Z
move-result v0
return v0
.end method
.method public f(Ljava/lang/Object;)V
.registers 2
check-cast p1, Landroid/widget/Scroller;
invoke-virtual {p1}, Landroid/widget/Scroller;->abortAnimation()V
return-void
.end method
.method public g(Ljava/lang/Object;)I
.registers 3
check-cast p1, Landroid/widget/Scroller;
invoke-virtual {p1}, Landroid/widget/Scroller;->getFinalY()I
move-result v0
return v0
.end method