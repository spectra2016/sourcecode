.class  Landroid/support/v4/b/a/g;
.super Ljava/lang/Object;
.source "DrawableCompatKitKat.java"
.method public static a(Landroid/graphics/drawable/Drawable;Z)V
.registers 2
invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V
return-void
.end method
.method public static a(Landroid/graphics/drawable/Drawable;)Z
.registers 2
invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->isAutoMirrored()Z
move-result v0
return v0
.end method
.method public static b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
.registers 2
instance-of v0, p0, Landroid/support/v4/b/a/o;
if-nez v0, :cond_a
new-instance v0, Landroid/support/v4/b/a/m;
invoke-direct {v0, p0}, Landroid/support/v4/b/a/m;-><init>(Landroid/graphics/drawable/Drawable;)V
move-object p0, v0
:cond_a
return-object p0
.end method
.method public static c(Landroid/graphics/drawable/Drawable;)I
.registers 2
invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getAlpha()I
move-result v0
return v0
.end method