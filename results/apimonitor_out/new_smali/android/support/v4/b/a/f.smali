.class  Landroid/support/v4/b/a/f;
.super Ljava/lang/Object;
.source "DrawableCompatJellybeanMr1.java"
.field private static a:Ljava/lang/reflect/Method;
.field private static b:Z
.method public static a(Landroid/graphics/drawable/Drawable;)I
.registers 5
const/4 v3, 0x1
sget-boolean v0, Landroid/support/v4/b/a/f;->b:Z
if-nez v0, :cond_1a
:try_start_5
const-class v0, Landroid/graphics/drawable/Drawable;
const-string v1, "getLayoutDirection"
const/4 v2, 0x0
new-array v2, v2, [Ljava/lang/Class;
invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
move-result-object v0
sput-object v0, Landroid/support/v4/b/a/f;->a:Ljava/lang/reflect/Method;
sget-object v0, Landroid/support/v4/b/a/f;->a:Ljava/lang/reflect/Method;
const/4 v1, 0x1
invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
:goto_18
:try_end_18
.catch Ljava/lang/NoSuchMethodException; {:try_start_5 .. :try_end_18} :catch_2e
sput-boolean v3, Landroid/support/v4/b/a/f;->b:Z
:cond_1a
sget-object v0, Landroid/support/v4/b/a/f;->a:Ljava/lang/reflect/Method;
if-eqz v0, :cond_42
:try_start_1e
sget-object v0, Landroid/support/v4/b/a/f;->a:Ljava/lang/reflect/Method;
const/4 v1, 0x0
new-array v1, v1, [Ljava/lang/Object;
invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Integer;
invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
:try_end_2c
.catch Ljava/lang/Exception; {:try_start_1e .. :try_end_2c} :catch_37
move-result v0
:goto_2d
return v0
:catch_2e
move-exception v0
const-string v1, "DrawableCompatJellybeanMr1"
const-string v2, "Failed to retrieve getLayoutDirection() method"
invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
goto :goto_18
:catch_37
move-exception v0
const-string v1, "DrawableCompatJellybeanMr1"
const-string v2, "Failed to invoke getLayoutDirection() via reflection"
invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
const/4 v0, 0x0
sput-object v0, Landroid/support/v4/b/a/f;->a:Ljava/lang/reflect/Method;
:cond_42
const/4 v0, -0x1
goto :goto_2d
.end method