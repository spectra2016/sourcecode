.class  Landroid/support/v4/b/a/h;
.super Ljava/lang/Object;
.source "DrawableCompatLollipop.java"
.method public static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
.registers 2
instance-of v0, p0, Landroid/support/v4/b/a/o;
if-nez v0, :cond_a
new-instance v0, Landroid/support/v4/b/a/n;
invoke-direct {v0, p0}, Landroid/support/v4/b/a/n;-><init>(Landroid/graphics/drawable/Drawable;)V
move-object p0, v0
:cond_a
return-object p0
.end method
.method public static a(Landroid/graphics/drawable/Drawable;FF)V
.registers 3
invoke-virtual {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V
return-void
.end method
.method public static a(Landroid/graphics/drawable/Drawable;I)V
.registers 2
invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->setTint(I)V
return-void
.end method
.method public static a(Landroid/graphics/drawable/Drawable;IIII)V
.registers 5
invoke-virtual {p0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setHotspotBounds(IIII)V
return-void
.end method
.method public static a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
.registers 2
invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->setTintList(Landroid/content/res/ColorStateList;)V
return-void
.end method
.method public static a(Landroid/graphics/drawable/Drawable;Landroid/content/res/Resources$Theme;)V
.registers 2
invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->applyTheme(Landroid/content/res/Resources$Theme;)V
return-void
.end method
.method public static a(Landroid/graphics/drawable/Drawable;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
.registers 5
invoke-virtual {p0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
return-void
.end method
.method public static a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
.registers 2
invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V
return-void
.end method
.method public static b(Landroid/graphics/drawable/Drawable;)Z
.registers 2
invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->canApplyTheme()Z
move-result v0
return v0
.end method
.method public static c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/ColorFilter;
.registers 2
invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getColorFilter()Landroid/graphics/ColorFilter;
move-result-object v0
return-object v0
.end method