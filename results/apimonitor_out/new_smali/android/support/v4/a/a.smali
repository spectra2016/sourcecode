.class public Landroid/support/v4/a/a;
.super Ljava/lang/Object;
.source "ContextCompat.java"
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public static a(Landroid/content/Context;Ljava/lang/String;)I
.registers 4
if-nez p1, :cond_a
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "permission is null"
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_a
invoke-static {}, Landroid/os/Process;->myPid()I
move-result v0
invoke-static {}, Landroid/os/Process;->myUid()I
move-result v1
invoke-virtual {p0, p1, v0, v1}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I
move-result v0
return v0
.end method
.method public static final a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
.registers 4
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x15
if-lt v0, v1, :cond_b
invoke-static {p0, p1}, Landroid/support/v4/a/b;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v0
:goto_a
return-object v0
:cond_b
invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
move-result-object v0
goto :goto_a
.end method
.method private static declared-synchronized a(Ljava/io/File;)Ljava/io/File;
.registers 5
const-class v1, Landroid/support/v4/a/a;
monitor-enter v1
:try_start_3
invoke-virtual {p0}, Ljava/io/File;->exists()Z
move-result v0
if-nez v0, :cond_15
invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z
move-result v0
if-nez v0, :cond_15
invoke-virtual {p0}, Ljava/io/File;->exists()Z
:try_end_12
.catchall {:try_start_3 .. :try_end_12} :catchall_35
move-result v0
if-eqz v0, :cond_17
:cond_15
:goto_15
monitor-exit v1
return-object p0
:cond_17
:try_start_17
const-string v0, "ContextCompat"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Unable to create files subdir "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
:try_end_33
.catchall {:try_start_17 .. :try_end_33} :catchall_35
const/4 p0, 0x0
goto :goto_15
:catchall_35
move-exception v0
monitor-exit v1
throw v0
.end method
.method public static a(Landroid/content/Context;[Landroid/content/Intent;Landroid/os/Bundle;)Z
.registers 6
const/4 v0, 0x1
sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v2, 0x10
if-lt v1, v2, :cond_b
invoke-static {p0, p1, p2}, Landroid/support/v4/a/e;->a(Landroid/content/Context;[Landroid/content/Intent;Landroid/os/Bundle;)V
:goto_a
return v0
:cond_b
const/16 v2, 0xb
if-lt v1, v2, :cond_13
invoke-static {p0, p1}, Landroid/support/v4/a/d;->a(Landroid/content/Context;[Landroid/content/Intent;)V
goto :goto_a
:cond_13
const/4 v0, 0x0
goto :goto_a
.end method
.method public static final b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
.registers 4
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x17
if-lt v0, v1, :cond_b
invoke-static {p0, p1}, Landroid/support/v4/a/c;->a(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
move-result-object v0
:goto_a
return-object v0
:cond_b
invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;
move-result-object v0
goto :goto_a
.end method
.method public static final c(Landroid/content/Context;I)I
.registers 4
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x17
if-lt v0, v1, :cond_b
invoke-static {p0, p1}, Landroid/support/v4/a/c;->b(Landroid/content/Context;I)I
move-result v0
:goto_a
return v0
:cond_b
invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I
move-result v0
goto :goto_a
.end method
.method public final a(Landroid/content/Context;)Ljava/io/File;
.registers 5
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x15
if-lt v0, v1, :cond_b
invoke-static {p1}, Landroid/support/v4/a/b;->a(Landroid/content/Context;)Ljava/io/File;
move-result-object v0
:goto_a
return-object v0
:cond_b
invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;
move-result-object v0
new-instance v1, Ljava/io/File;
iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;
const-string v2, "no_backup"
invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V
invoke-static {v1}, Landroid/support/v4/a/a;->a(Ljava/io/File;)Ljava/io/File;
move-result-object v0
goto :goto_a
.end method