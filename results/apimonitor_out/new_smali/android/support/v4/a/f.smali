.class public final Landroid/support/v4/a/f;
.super Ljava/lang/Object;
.source "IntentCompat.java"
.field private static final a:Landroid/support/v4/a/f$a;
.method static constructor <clinit>()V
.registers 2
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0xf
if-lt v0, v1, :cond_e
new-instance v0, Landroid/support/v4/a/f$d;
invoke-direct {v0}, Landroid/support/v4/a/f$d;-><init>()V
sput-object v0, Landroid/support/v4/a/f;->a:Landroid/support/v4/a/f$a;
:goto_d
return-void
:cond_e
const/16 v1, 0xb
if-lt v0, v1, :cond_1a
new-instance v0, Landroid/support/v4/a/f$c;
invoke-direct {v0}, Landroid/support/v4/a/f$c;-><init>()V
sput-object v0, Landroid/support/v4/a/f;->a:Landroid/support/v4/a/f$a;
goto :goto_d
:cond_1a
new-instance v0, Landroid/support/v4/a/f$b;
invoke-direct {v0}, Landroid/support/v4/a/f$b;-><init>()V
sput-object v0, Landroid/support/v4/a/f;->a:Landroid/support/v4/a/f$a;
goto :goto_d
.end method
.method public static a(Landroid/content/ComponentName;)Landroid/content/Intent;
.registers 2
sget-object v0, Landroid/support/v4/a/f;->a:Landroid/support/v4/a/f$a;
invoke-interface {v0, p0}, Landroid/support/v4/a/f$a;->a(Landroid/content/ComponentName;)Landroid/content/Intent;
move-result-object v0
return-object v0
.end method