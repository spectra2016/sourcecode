.class public Landroid/support/a/a/f;
.super Landroid/support/a/a/e;
.source "VectorDrawableCompat.java"
.field static final b:Landroid/graphics/PorterDuff$Mode;
.field private c:Landroid/support/a/a/f$f;
.field private d:Landroid/graphics/PorterDuffColorFilter;
.field private e:Landroid/graphics/ColorFilter;
.field private f:Z
.field private g:Z
.field private h:Landroid/graphics/drawable/Drawable$ConstantState;
.field private final i:[F
.field private final j:Landroid/graphics/Matrix;
.field private final k:Landroid/graphics/Rect;
.method static constructor <clinit>()V
.registers 1
sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;
sput-object v0, Landroid/support/a/a/f;->b:Landroid/graphics/PorterDuff$Mode;
return-void
.end method
.method private constructor <init>()V
.registers 2
invoke-direct {p0}, Landroid/support/a/a/e;-><init>()V
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/a/a/f;->g:Z
const/16 v0, 0x9
new-array v0, v0, [F
iput-object v0, p0, Landroid/support/a/a/f;->i:[F
new-instance v0, Landroid/graphics/Matrix;
invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V
iput-object v0, p0, Landroid/support/a/a/f;->j:Landroid/graphics/Matrix;
new-instance v0, Landroid/graphics/Rect;
invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V
iput-object v0, p0, Landroid/support/a/a/f;->k:Landroid/graphics/Rect;
new-instance v0, Landroid/support/a/a/f$f;
invoke-direct {v0}, Landroid/support/a/a/f$f;-><init>()V
iput-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
return-void
.end method
.method synthetic constructor <init>(Landroid/support/a/a/f$1;)V
.registers 2
invoke-direct {p0}, Landroid/support/a/a/f;-><init>()V
return-void
.end method
.method private constructor <init>(Landroid/support/a/a/f$f;)V
.registers 5
invoke-direct {p0}, Landroid/support/a/a/e;-><init>()V
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/a/a/f;->g:Z
const/16 v0, 0x9
new-array v0, v0, [F
iput-object v0, p0, Landroid/support/a/a/f;->i:[F
new-instance v0, Landroid/graphics/Matrix;
invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V
iput-object v0, p0, Landroid/support/a/a/f;->j:Landroid/graphics/Matrix;
new-instance v0, Landroid/graphics/Rect;
invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V
iput-object v0, p0, Landroid/support/a/a/f;->k:Landroid/graphics/Rect;
iput-object p1, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v0, p0, Landroid/support/a/a/f;->d:Landroid/graphics/PorterDuffColorFilter;
iget-object v1, p1, Landroid/support/a/a/f$f;->c:Landroid/content/res/ColorStateList;
iget-object v2, p1, Landroid/support/a/a/f$f;->d:Landroid/graphics/PorterDuff$Mode;
invoke-virtual {p0, v0, v1, v2}, Landroid/support/a/a/f;->a(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
move-result-object v0
iput-object v0, p0, Landroid/support/a/a/f;->d:Landroid/graphics/PorterDuffColorFilter;
return-void
.end method
.method synthetic constructor <init>(Landroid/support/a/a/f$f;Landroid/support/a/a/f$1;)V
.registers 3
invoke-direct {p0, p1}, Landroid/support/a/a/f;-><init>(Landroid/support/a/a/f$f;)V
return-void
.end method
.method static synthetic a(IF)I
.registers 3
invoke-static {p0, p1}, Landroid/support/a/a/f;->b(IF)I
move-result v0
return v0
.end method
.method private static a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;
.registers 2
packed-switch p0, :pswitch_data_16
:goto_3
:pswitch_3
return-object p1
:pswitch_4
sget-object p1, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;
goto :goto_3
:pswitch_7
sget-object p1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;
goto :goto_3
:pswitch_a
sget-object p1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;
goto :goto_3
:pswitch_d
sget-object p1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;
goto :goto_3
:pswitch_10
sget-object p1, Landroid/graphics/PorterDuff$Mode;->SCREEN:Landroid/graphics/PorterDuff$Mode;
goto :goto_3
:pswitch_13
sget-object p1, Landroid/graphics/PorterDuff$Mode;->ADD:Landroid/graphics/PorterDuff$Mode;
goto :goto_3
:pswitch_data_16
.packed-switch 0x3
:pswitch_4
:pswitch_3
:pswitch_7
:pswitch_3
:pswitch_3
:pswitch_3
:pswitch_a
:pswitch_3
:pswitch_3
:pswitch_3
:pswitch_3
:pswitch_d
:pswitch_10
:pswitch_13
.end packed-switch
.end method
.method public static a(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/support/a/a/f;
.registers 8
const/4 v4, 0x2
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x17
if-lt v0, v1, :cond_20
new-instance v0, Landroid/support/a/a/f;
invoke-direct {v0}, Landroid/support/a/a/f;-><init>()V
invoke-static {p0, p1, p2}, Landroid/support/v4/a/a/a;->a(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
move-result-object v1
iput-object v1, v0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
new-instance v1, Landroid/support/a/a/f$g;
iget-object v2, v0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
move-result-object v2
invoke-direct {v1, v2}, Landroid/support/a/a/f$g;-><init>(Landroid/graphics/drawable/Drawable$ConstantState;)V
iput-object v1, v0, Landroid/support/a/a/f;->h:Landroid/graphics/drawable/Drawable$ConstantState;
:goto_1f
return-object v0
:cond_20
:try_start_20
invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;
move-result-object v0
invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
move-result-object v1
:cond_28
invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I
move-result v2
if-eq v2, v4, :cond_31
const/4 v3, 0x1
if-ne v2, v3, :cond_28
:cond_31
if-eq v2, v4, :cond_45
new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;
const-string v1, "No start tag found"
invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V
throw v0
:try_end_3b
.catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_20 .. :try_end_3b} :catch_3b
.catch Ljava/io/IOException; {:try_start_20 .. :try_end_3b} :catch_4a
:catch_3b
move-exception v0
const-string v1, "VectorDrawableCompat"
const-string v2, "parser error"
invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
:goto_43
const/4 v0, 0x0
goto :goto_1f
:try_start_45
:cond_45
invoke-static {p0, v0, v1, p2}, Landroid/support/a/a/f;->a(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/support/a/a/f;
:try_end_48
.catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_45 .. :try_end_48} :catch_3b
.catch Ljava/io/IOException; {:try_start_45 .. :try_end_48} :catch_4a
move-result-object v0
goto :goto_1f
:catch_4a
move-exception v0
const-string v1, "VectorDrawableCompat"
const-string v2, "parser error"
invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
goto :goto_43
.end method
.method public static a(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/support/a/a/f;
.registers 5
new-instance v0, Landroid/support/a/a/f;
invoke-direct {v0}, Landroid/support/a/a/f;-><init>()V
invoke-virtual {v0, p0, p1, p2, p3}, Landroid/support/a/a/f;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
return-object v0
.end method
.method private a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)V
.registers 9
const/4 v5, 0x0
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v1, v0, Landroid/support/a/a/f$f;->b:Landroid/support/a/a/f$e;
const-string v2, "tintMode"
const/4 v3, 0x6
const/4 v4, -0x1
invoke-static {p1, p2, v2, v3, v4}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;II)I
move-result v2
sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;
invoke-static {v2, v3}, Landroid/support/a/a/f;->a(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;
move-result-object v2
iput-object v2, v0, Landroid/support/a/a/f$f;->d:Landroid/graphics/PorterDuff$Mode;
const/4 v2, 0x1
invoke-virtual {p1, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;
move-result-object v2
if-eqz v2, :cond_1e
iput-object v2, v0, Landroid/support/a/a/f$f;->c:Landroid/content/res/ColorStateList;
:cond_1e
const-string v2, "autoMirrored"
const/4 v3, 0x5
iget-boolean v4, v0, Landroid/support/a/a/f$f;->e:Z
invoke-static {p1, p2, v2, v3, v4}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IZ)Z
move-result v2
iput-boolean v2, v0, Landroid/support/a/a/f$f;->e:Z
const-string v0, "viewportWidth"
const/4 v2, 0x7
iget v3, v1, Landroid/support/a/a/f$e;->c:F
invoke-static {p1, p2, v0, v2, v3}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F
move-result v0
iput v0, v1, Landroid/support/a/a/f$e;->c:F
const-string v0, "viewportHeight"
const/16 v2, 0x8
iget v3, v1, Landroid/support/a/a/f$e;->d:F
invoke-static {p1, p2, v0, v2, v3}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F
move-result v0
iput v0, v1, Landroid/support/a/a/f$e;->d:F
iget v0, v1, Landroid/support/a/a/f$e;->c:F
cmpg-float v0, v0, v5
if-gtz v0, :cond_63
new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {p1}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "<vector> tag requires viewportWidth > 0"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V
throw v0
:cond_63
iget v0, v1, Landroid/support/a/a/f$e;->d:F
cmpg-float v0, v0, v5
if-gtz v0, :cond_86
new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {p1}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "<vector> tag requires viewportHeight > 0"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V
throw v0
:cond_86
const/4 v0, 0x3
iget v2, v1, Landroid/support/a/a/f$e;->a:F
invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F
move-result v0
iput v0, v1, Landroid/support/a/a/f$e;->a:F
const/4 v0, 0x2
iget v2, v1, Landroid/support/a/a/f$e;->b:F
invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F
move-result v0
iput v0, v1, Landroid/support/a/a/f$e;->b:F
iget v0, v1, Landroid/support/a/a/f$e;->a:F
cmpg-float v0, v0, v5
if-gtz v0, :cond_bb
new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {p1}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "<vector> tag requires width > 0"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V
throw v0
:cond_bb
iget v0, v1, Landroid/support/a/a/f$e;->b:F
cmpg-float v0, v0, v5
if-gtz v0, :cond_de
new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {p1}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "<vector> tag requires height > 0"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V
throw v0
:cond_de
const-string v0, "alpha"
const/4 v2, 0x4
invoke-virtual {v1}, Landroid/support/a/a/f$e;->b()F
move-result v3
invoke-static {p1, p2, v0, v2, v3}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F
move-result v0
invoke-virtual {v1, v0}, Landroid/support/a/a/f$e;->a(F)V
const/4 v0, 0x0
invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_fa
iput-object v0, v1, Landroid/support/a/a/f$e;->f:Ljava/lang/String;
iget-object v2, v1, Landroid/support/a/a/f$e;->g:Landroid/support/v4/e/a;
invoke-virtual {v2, v0, v1}, Landroid/support/v4/e/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_fa
return-void
.end method
.method private a()Z
.registers 2
const/4 v0, 0x0
return v0
.end method
.method private static b(IF)I
.registers 4
invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I
move-result v0
const v1, 0xffffff
and-int/2addr v1, p0
int-to-float v0, v0
mul-float/2addr v0, p1
float-to-int v0, v0
shl-int/lit8 v0, v0, 0x18
or-int/2addr v0, v1
return v0
.end method
.method private b(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
.registers 13
const/4 v2, 0x1
iget-object v3, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v4, v3, Landroid/support/a/a/f$f;->b:Landroid/support/a/a/f$e;
new-instance v5, Ljava/util/Stack;
invoke-direct {v5}, Ljava/util/Stack;-><init>()V
invoke-static {v4}, Landroid/support/a/a/f$e;->a(Landroid/support/a/a/f$e;)Landroid/support/a/a/f$c;
move-result-object v0
invoke-virtual {v5, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
move-result v0
move v1, v2
:goto_16
if-eq v0, v2, :cond_c9
const/4 v6, 0x2
if-ne v0, v6, :cond_b6
invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;
move-result-object v6
invoke-virtual {v5}, Ljava/util/Stack;->peek()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/support/a/a/f$c;
const-string v7, "path"
invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v7
if-eqz v7, :cond_57
new-instance v1, Landroid/support/a/a/f$b;
invoke-direct {v1}, Landroid/support/a/a/f$b;-><init>()V
invoke-virtual {v1, p1, p3, p4, p2}, Landroid/support/a/a/f$b;->a(Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;Lorg/xmlpull/v1/XmlPullParser;)V
iget-object v0, v0, Landroid/support/a/a/f$c;->a:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
invoke-virtual {v1}, Landroid/support/a/a/f$b;->b()Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_49
iget-object v0, v4, Landroid/support/a/a/f$e;->g:Landroid/support/v4/e/a;
invoke-virtual {v1}, Landroid/support/a/a/f$b;->b()Ljava/lang/String;
move-result-object v6
invoke-virtual {v0, v6, v1}, Landroid/support/v4/e/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_49
const/4 v0, 0x0
iget v6, v3, Landroid/support/a/a/f$f;->a:I
iget v1, v1, Landroid/support/a/a/f$b;->o:I
or-int/2addr v1, v6
iput v1, v3, Landroid/support/a/a/f$f;->a:I
:goto_51
move v1, v0
:goto_52
:cond_52
invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I
move-result v0
goto :goto_16
:cond_57
const-string v7, "clip-path"
invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v7
if-eqz v7, :cond_84
new-instance v6, Landroid/support/a/a/f$a;
invoke-direct {v6}, Landroid/support/a/a/f$a;-><init>()V
invoke-virtual {v6, p1, p3, p4, p2}, Landroid/support/a/a/f$a;->a(Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;Lorg/xmlpull/v1/XmlPullParser;)V
iget-object v0, v0, Landroid/support/a/a/f$c;->a:Ljava/util/ArrayList;
invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
invoke-virtual {v6}, Landroid/support/a/a/f$a;->b()Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_7b
iget-object v0, v4, Landroid/support/a/a/f$e;->g:Landroid/support/v4/e/a;
invoke-virtual {v6}, Landroid/support/a/a/f$a;->b()Ljava/lang/String;
move-result-object v7
invoke-virtual {v0, v7, v6}, Landroid/support/v4/e/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_7b
iget v0, v3, Landroid/support/a/a/f$f;->a:I
iget v6, v6, Landroid/support/a/a/f$a;->o:I
or-int/2addr v0, v6
iput v0, v3, Landroid/support/a/a/f$f;->a:I
move v0, v1
goto :goto_51
:cond_84
const-string v7, "group"
invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v6
if-eqz v6, :cond_b4
new-instance v6, Landroid/support/a/a/f$c;
invoke-direct {v6}, Landroid/support/a/a/f$c;-><init>()V
invoke-virtual {v6, p1, p3, p4, p2}, Landroid/support/a/a/f$c;->a(Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;Lorg/xmlpull/v1/XmlPullParser;)V
iget-object v0, v0, Landroid/support/a/a/f$c;->a:Ljava/util/ArrayList;
invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
invoke-virtual {v5, v6}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
invoke-virtual {v6}, Landroid/support/a/a/f$c;->a()Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_ab
iget-object v0, v4, Landroid/support/a/a/f$e;->g:Landroid/support/v4/e/a;
invoke-virtual {v6}, Landroid/support/a/a/f$c;->a()Ljava/lang/String;
move-result-object v7
invoke-virtual {v0, v7, v6}, Landroid/support/v4/e/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_ab
iget v0, v3, Landroid/support/a/a/f$f;->a:I
invoke-static {v6}, Landroid/support/a/a/f$c;->a(Landroid/support/a/a/f$c;)I
move-result v6
or-int/2addr v0, v6
iput v0, v3, Landroid/support/a/a/f$f;->a:I
:cond_b4
move v0, v1
goto :goto_51
:cond_b6
const/4 v6, 0x3
if-ne v0, v6, :cond_52
invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;
move-result-object v0
const-string v6, "group"
invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_52
invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;
goto :goto_52
:cond_c9
if-eqz v1, :cond_ff
new-instance v0, Ljava/lang/StringBuffer;
invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V
invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I
move-result v1
if-lez v1, :cond_db
const-string v1, " or "
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
:cond_db
const-string v1, "path"
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "no "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v2, " defined"
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-direct {v1, v0}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V
throw v1
:cond_ff
return-void
.end method
.method  a(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
.registers 6
if-eqz p2, :cond_4
if-nez p3, :cond_6
:cond_4
const/4 v0, 0x0
:goto_5
return-object v0
:cond_6
invoke-virtual {p0}, Landroid/support/a/a/f;->getState()[I
move-result-object v0
const/4 v1, 0x0
invoke-virtual {p2, v0, v1}, Landroid/content/res/ColorStateList;->getColorForState([II)I
move-result v1
new-instance v0, Landroid/graphics/PorterDuffColorFilter;
invoke-direct {v0, v1, p3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V
goto :goto_5
.end method
.method  a(Ljava/lang/String;)Ljava/lang/Object;
.registers 3
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v0, v0, Landroid/support/a/a/f$f;->b:Landroid/support/a/a/f$e;
iget-object v0, v0, Landroid/support/a/a/f$e;->g:Landroid/support/v4/e/a;
invoke-virtual {v0, p1}, Landroid/support/v4/e/a;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
return-object v0
.end method
.method  a(Z)V
.registers 2
iput-boolean p1, p0, Landroid/support/a/a/f;->g:Z
return-void
.end method
.method public bridge synthetic applyTheme(Landroid/content/res/Resources$Theme;)V
.registers 2
invoke-super {p0, p1}, Landroid/support/a/a/e;->applyTheme(Landroid/content/res/Resources$Theme;)V
return-void
.end method
.method public canApplyTheme()Z
.registers 2
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-static {v0}, Landroid/support/v4/b/a/a;->d(Landroid/graphics/drawable/Drawable;)Z
:cond_9
const/4 v0, 0x0
return v0
.end method
.method public bridge synthetic clearColorFilter()V
.registers 1
invoke-super {p0}, Landroid/support/a/a/e;->clearColorFilter()V
return-void
.end method
.method public draw(Landroid/graphics/Canvas;)V
.registers 12
const/16 v9, 0x800
const/4 v8, 0x0
const/high16 v2, 0x3f80
const/4 v7, 0x0
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_10
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
:cond_f
:goto_f
return-void
:cond_10
iget-object v0, p0, Landroid/support/a/a/f;->k:Landroid/graphics/Rect;
invoke-virtual {p0, v0}, Landroid/support/a/a/f;->copyBounds(Landroid/graphics/Rect;)V
iget-object v0, p0, Landroid/support/a/a/f;->k:Landroid/graphics/Rect;
invoke-virtual {v0}, Landroid/graphics/Rect;->width()I
move-result v0
if-lez v0, :cond_f
iget-object v0, p0, Landroid/support/a/a/f;->k:Landroid/graphics/Rect;
invoke-virtual {v0}, Landroid/graphics/Rect;->height()I
move-result v0
if-lez v0, :cond_f
iget-object v0, p0, Landroid/support/a/a/f;->e:Landroid/graphics/ColorFilter;
if-nez v0, :cond_c7
iget-object v0, p0, Landroid/support/a/a/f;->d:Landroid/graphics/PorterDuffColorFilter;
:goto_2b
iget-object v1, p0, Landroid/support/a/a/f;->j:Landroid/graphics/Matrix;
invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->getMatrix(Landroid/graphics/Matrix;)V
iget-object v1, p0, Landroid/support/a/a/f;->j:Landroid/graphics/Matrix;
iget-object v3, p0, Landroid/support/a/a/f;->i:[F
invoke-virtual {v1, v3}, Landroid/graphics/Matrix;->getValues([F)V
iget-object v1, p0, Landroid/support/a/a/f;->i:[F
aget v1, v1, v8
invoke-static {v1}, Ljava/lang/Math;->abs(F)F
move-result v3
iget-object v1, p0, Landroid/support/a/a/f;->i:[F
const/4 v4, 0x4
aget v1, v1, v4
invoke-static {v1}, Ljava/lang/Math;->abs(F)F
move-result v1
iget-object v4, p0, Landroid/support/a/a/f;->i:[F
const/4 v5, 0x1
aget v4, v4, v5
invoke-static {v4}, Ljava/lang/Math;->abs(F)F
move-result v4
iget-object v5, p0, Landroid/support/a/a/f;->i:[F
const/4 v6, 0x3
aget v5, v5, v6
invoke-static {v5}, Ljava/lang/Math;->abs(F)F
move-result v5
cmpl-float v4, v4, v7
if-nez v4, :cond_62
cmpl-float v4, v5, v7
if-eqz v4, :cond_64
:cond_62
move v1, v2
move v3, v2
:cond_64
iget-object v4, p0, Landroid/support/a/a/f;->k:Landroid/graphics/Rect;
invoke-virtual {v4}, Landroid/graphics/Rect;->width()I
move-result v4
int-to-float v4, v4
mul-float/2addr v3, v4
float-to-int v3, v3
iget-object v4, p0, Landroid/support/a/a/f;->k:Landroid/graphics/Rect;
invoke-virtual {v4}, Landroid/graphics/Rect;->height()I
move-result v4
int-to-float v4, v4
mul-float/2addr v1, v4
float-to-int v1, v1
invoke-static {v9, v3}, Ljava/lang/Math;->min(II)I
move-result v3
invoke-static {v9, v1}, Ljava/lang/Math;->min(II)I
move-result v1
if-lez v3, :cond_f
if-lez v1, :cond_f
invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I
move-result v4
iget-object v5, p0, Landroid/support/a/a/f;->k:Landroid/graphics/Rect;
iget v5, v5, Landroid/graphics/Rect;->left:I
int-to-float v5, v5
iget-object v6, p0, Landroid/support/a/a/f;->k:Landroid/graphics/Rect;
iget v6, v6, Landroid/graphics/Rect;->top:I
int-to-float v6, v6
invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V
invoke-direct {p0}, Landroid/support/a/a/f;->a()Z
move-result v5
if-eqz v5, :cond_a8
iget-object v5, p0, Landroid/support/a/a/f;->k:Landroid/graphics/Rect;
invoke-virtual {v5}, Landroid/graphics/Rect;->width()I
move-result v5
int-to-float v5, v5
invoke-virtual {p1, v5, v7}, Landroid/graphics/Canvas;->translate(FF)V
const/high16 v5, -0x4080
invoke-virtual {p1, v5, v2}, Landroid/graphics/Canvas;->scale(FF)V
:cond_a8
iget-object v2, p0, Landroid/support/a/a/f;->k:Landroid/graphics/Rect;
invoke-virtual {v2, v8, v8}, Landroid/graphics/Rect;->offsetTo(II)V
iget-object v2, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
invoke-virtual {v2, v3, v1}, Landroid/support/a/a/f$f;->b(II)V
iget-boolean v2, p0, Landroid/support/a/a/f;->g:Z
if-nez v2, :cond_cb
iget-object v2, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
invoke-virtual {v2, v3, v1}, Landroid/support/a/a/f$f;->a(II)V
:goto_bb
:cond_bb
iget-object v1, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v2, p0, Landroid/support/a/a/f;->k:Landroid/graphics/Rect;
invoke-virtual {v1, p1, v0, v2}, Landroid/support/a/a/f$f;->a(Landroid/graphics/Canvas;Landroid/graphics/ColorFilter;Landroid/graphics/Rect;)V
invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V
goto/16 :goto_f
:cond_c7
iget-object v0, p0, Landroid/support/a/a/f;->e:Landroid/graphics/ColorFilter;
goto/16 :goto_2b
:cond_cb
iget-object v2, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
invoke-virtual {v2}, Landroid/support/a/a/f$f;->b()Z
move-result v2
if-nez v2, :cond_bb
iget-object v2, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
invoke-virtual {v2, v3, v1}, Landroid/support/a/a/f$f;->a(II)V
iget-object v1, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
invoke-virtual {v1}, Landroid/support/a/a/f$f;->c()V
goto :goto_bb
.end method
.method public getAlpha()I
.registers 2
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-static {v0}, Landroid/support/v4/b/a/a;->c(Landroid/graphics/drawable/Drawable;)I
move-result v0
:goto_a
return v0
:cond_b
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v0, v0, Landroid/support/a/a/f$f;->b:Landroid/support/a/a/f$e;
invoke-virtual {v0}, Landroid/support/a/a/f$e;->a()I
move-result v0
goto :goto_a
.end method
.method public getChangingConfigurations()I
.registers 3
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I
move-result v0
:goto_a
return v0
:cond_b
invoke-super {p0}, Landroid/support/a/a/e;->getChangingConfigurations()I
move-result v0
iget-object v1, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
invoke-virtual {v1}, Landroid/support/a/a/f$f;->getChangingConfigurations()I
move-result v1
or-int/2addr v0, v1
goto :goto_a
.end method
.method public bridge synthetic getColorFilter()Landroid/graphics/ColorFilter;
.registers 2
invoke-super {p0}, Landroid/support/a/a/e;->getColorFilter()Landroid/graphics/ColorFilter;
move-result-object v0
return-object v0
.end method
.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
.registers 3
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_10
new-instance v0, Landroid/support/a/a/f$g;
iget-object v1, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
move-result-object v1
invoke-direct {v0, v1}, Landroid/support/a/a/f$g;-><init>(Landroid/graphics/drawable/Drawable$ConstantState;)V
:goto_f
return-object v0
:cond_10
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
invoke-virtual {p0}, Landroid/support/a/a/f;->getChangingConfigurations()I
move-result v1
iput v1, v0, Landroid/support/a/a/f$f;->a:I
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
goto :goto_f
.end method
.method public bridge synthetic getCurrent()Landroid/graphics/drawable/Drawable;
.registers 2
invoke-super {p0}, Landroid/support/a/a/e;->getCurrent()Landroid/graphics/drawable/Drawable;
move-result-object v0
return-object v0
.end method
.method public getIntrinsicHeight()I
.registers 2
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I
move-result v0
:goto_a
return v0
:cond_b
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v0, v0, Landroid/support/a/a/f$f;->b:Landroid/support/a/a/f$e;
iget v0, v0, Landroid/support/a/a/f$e;->b:F
float-to-int v0, v0
goto :goto_a
.end method
.method public getIntrinsicWidth()I
.registers 2
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I
move-result v0
:goto_a
return v0
:cond_b
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v0, v0, Landroid/support/a/a/f$f;->b:Landroid/support/a/a/f$e;
iget v0, v0, Landroid/support/a/a/f$e;->a:F
float-to-int v0, v0
goto :goto_a
.end method
.method public bridge synthetic getLayoutDirection()I
.registers 2
invoke-super {p0}, Landroid/support/a/a/e;->getLayoutDirection()I
move-result v0
return v0
.end method
.method public bridge synthetic getMinimumHeight()I
.registers 2
invoke-super {p0}, Landroid/support/a/a/e;->getMinimumHeight()I
move-result v0
return v0
.end method
.method public bridge synthetic getMinimumWidth()I
.registers 2
invoke-super {p0}, Landroid/support/a/a/e;->getMinimumWidth()I
move-result v0
return v0
.end method
.method public getOpacity()I
.registers 2
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I
move-result v0
:goto_a
return v0
:cond_b
const/4 v0, -0x3
goto :goto_a
.end method
.method public bridge synthetic getPadding(Landroid/graphics/Rect;)Z
.registers 3
invoke-super {p0, p1}, Landroid/support/a/a/e;->getPadding(Landroid/graphics/Rect;)Z
move-result v0
return v0
.end method
.method public bridge synthetic getState()[I
.registers 2
invoke-super {p0}, Landroid/support/a/a/e;->getState()[I
move-result-object v0
return-object v0
.end method
.method public bridge synthetic getTransparentRegion()Landroid/graphics/Region;
.registers 2
invoke-super {p0}, Landroid/support/a/a/e;->getTransparentRegion()Landroid/graphics/Region;
move-result-object v0
return-object v0
.end method
.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
.registers 5
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, p1, p2, p3}, Landroid/graphics/drawable/Drawable;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
:goto_9
return-void
:cond_a
const/4 v0, 0x0
invoke-virtual {p0, p1, p2, p3, v0}, Landroid/support/a/a/f;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
goto :goto_9
.end method
.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
.registers 8
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-static {v0, p1, p2, p3, p4}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
:goto_9
return-void
:cond_a
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
new-instance v1, Landroid/support/a/a/f$e;
invoke-direct {v1}, Landroid/support/a/a/f$e;-><init>()V
iput-object v1, v0, Landroid/support/a/a/f$f;->b:Landroid/support/a/a/f$e;
sget-object v1, Landroid/support/a/a/a;->a:[I
invoke-static {p1, p4, p3, v1}, Landroid/support/a/a/f;->b(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
move-result-object v1
invoke-direct {p0, v1, p2}, Landroid/support/a/a/f;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)V
invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V
invoke-virtual {p0}, Landroid/support/a/a/f;->getChangingConfigurations()I
move-result v1
iput v1, v0, Landroid/support/a/a/f$f;->a:I
const/4 v1, 0x1
iput-boolean v1, v0, Landroid/support/a/a/f$f;->k:Z
invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/a/a/f;->b(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
iget-object v1, p0, Landroid/support/a/a/f;->d:Landroid/graphics/PorterDuffColorFilter;
iget-object v2, v0, Landroid/support/a/a/f$f;->c:Landroid/content/res/ColorStateList;
iget-object v0, v0, Landroid/support/a/a/f$f;->d:Landroid/graphics/PorterDuff$Mode;
invoke-virtual {p0, v1, v2, v0}, Landroid/support/a/a/f;->a(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
move-result-object v0
iput-object v0, p0, Landroid/support/a/a/f;->d:Landroid/graphics/PorterDuffColorFilter;
goto :goto_9
.end method
.method public invalidateSelf()V
.registers 2
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V
:goto_9
return-void
:cond_a
invoke-super {p0}, Landroid/support/a/a/e;->invalidateSelf()V
goto :goto_9
.end method
.method public bridge synthetic isAutoMirrored()Z
.registers 2
invoke-super {p0}, Landroid/support/a/a/e;->isAutoMirrored()Z
move-result v0
return v0
.end method
.method public isStateful()Z
.registers 2
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z
move-result v0
:goto_a
return v0
:cond_b
invoke-super {p0}, Landroid/support/a/a/e;->isStateful()Z
move-result v0
if-nez v0, :cond_25
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
if-eqz v0, :cond_27
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v0, v0, Landroid/support/a/a/f$f;->c:Landroid/content/res/ColorStateList;
if-eqz v0, :cond_27
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v0, v0, Landroid/support/a/a/f$f;->c:Landroid/content/res/ColorStateList;
invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z
move-result v0
if-eqz v0, :cond_27
:cond_25
const/4 v0, 0x1
goto :goto_a
:cond_27
const/4 v0, 0x0
goto :goto_a
.end method
.method public bridge synthetic jumpToCurrentState()V
.registers 1
invoke-super {p0}, Landroid/support/a/a/e;->jumpToCurrentState()V
return-void
.end method
.method public mutate()Landroid/graphics/drawable/Drawable;
.registers 3
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;
:cond_9
:goto_9
return-object p0
:cond_a
iget-boolean v0, p0, Landroid/support/a/a/f;->f:Z
if-nez v0, :cond_9
invoke-super {p0}, Landroid/support/a/a/e;->mutate()Landroid/graphics/drawable/Drawable;
move-result-object v0
if-ne v0, p0, :cond_9
new-instance v0, Landroid/support/a/a/f$f;
iget-object v1, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
invoke-direct {v0, v1}, Landroid/support/a/a/f$f;-><init>(Landroid/support/a/a/f$f;)V
iput-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/a/a/f;->f:Z
goto :goto_9
.end method
.method protected onBoundsChange(Landroid/graphics/Rect;)V
.registers 3
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_9
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V
:cond_9
return-void
.end method
.method protected onStateChange([I)Z
.registers 5
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z
move-result v0
:goto_a
return v0
:cond_b
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v1, v0, Landroid/support/a/a/f$f;->c:Landroid/content/res/ColorStateList;
if-eqz v1, :cond_26
iget-object v1, v0, Landroid/support/a/a/f$f;->d:Landroid/graphics/PorterDuff$Mode;
if-eqz v1, :cond_26
iget-object v1, p0, Landroid/support/a/a/f;->d:Landroid/graphics/PorterDuffColorFilter;
iget-object v2, v0, Landroid/support/a/a/f$f;->c:Landroid/content/res/ColorStateList;
iget-object v0, v0, Landroid/support/a/a/f$f;->d:Landroid/graphics/PorterDuff$Mode;
invoke-virtual {p0, v1, v2, v0}, Landroid/support/a/a/f;->a(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
move-result-object v0
iput-object v0, p0, Landroid/support/a/a/f;->d:Landroid/graphics/PorterDuffColorFilter;
invoke-virtual {p0}, Landroid/support/a/a/f;->invalidateSelf()V
const/4 v0, 0x1
goto :goto_a
:cond_26
const/4 v0, 0x0
goto :goto_a
.end method
.method public scheduleSelf(Ljava/lang/Runnable;J)V
.registers 6
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, p1, p2, p3}, Landroid/graphics/drawable/Drawable;->scheduleSelf(Ljava/lang/Runnable;J)V
:goto_9
return-void
:cond_a
invoke-super {p0, p1, p2, p3}, Landroid/support/a/a/e;->scheduleSelf(Ljava/lang/Runnable;J)V
goto :goto_9
.end method
.method public setAlpha(I)V
.registers 3
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V
:cond_9
:goto_9
return-void
:cond_a
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v0, v0, Landroid/support/a/a/f$f;->b:Landroid/support/a/a/f$e;
invoke-virtual {v0}, Landroid/support/a/a/f$e;->a()I
move-result v0
if-eq v0, p1, :cond_9
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v0, v0, Landroid/support/a/a/f$f;->b:Landroid/support/a/a/f$e;
invoke-virtual {v0, p1}, Landroid/support/a/a/f$e;->a(I)V
invoke-virtual {p0}, Landroid/support/a/a/f;->invalidateSelf()V
goto :goto_9
.end method
.method public bridge synthetic setAutoMirrored(Z)V
.registers 2
invoke-super {p0, p1}, Landroid/support/a/a/e;->setAutoMirrored(Z)V
return-void
.end method
.method public bridge synthetic setChangingConfigurations(I)V
.registers 2
invoke-super {p0, p1}, Landroid/support/a/a/e;->setChangingConfigurations(I)V
return-void
.end method
.method public bridge synthetic setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
.registers 3
invoke-super {p0, p1, p2}, Landroid/support/a/a/e;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
return-void
.end method
.method public setColorFilter(Landroid/graphics/ColorFilter;)V
.registers 3
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V
:goto_9
return-void
:cond_a
iput-object p1, p0, Landroid/support/a/a/f;->e:Landroid/graphics/ColorFilter;
invoke-virtual {p0}, Landroid/support/a/a/f;->invalidateSelf()V
goto :goto_9
.end method
.method public bridge synthetic setFilterBitmap(Z)V
.registers 2
invoke-super {p0, p1}, Landroid/support/a/a/e;->setFilterBitmap(Z)V
return-void
.end method
.method public bridge synthetic setHotspot(FF)V
.registers 3
invoke-super {p0, p1, p2}, Landroid/support/a/a/e;->setHotspot(FF)V
return-void
.end method
.method public bridge synthetic setHotspotBounds(IIII)V
.registers 5
invoke-super {p0, p1, p2, p3, p4}, Landroid/support/a/a/e;->setHotspotBounds(IIII)V
return-void
.end method
.method public bridge synthetic setState([I)Z
.registers 3
invoke-super {p0, p1}, Landroid/support/a/a/e;->setState([I)Z
move-result v0
return v0
.end method
.method public setTint(I)V
.registers 3
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-static {v0, p1}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;I)V
:goto_9
return-void
:cond_a
invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;
move-result-object v0
invoke-virtual {p0, v0}, Landroid/support/a/a/f;->setTintList(Landroid/content/res/ColorStateList;)V
goto :goto_9
.end method
.method public setTintList(Landroid/content/res/ColorStateList;)V
.registers 4
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-static {v0, p1}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
:cond_9
:goto_9
return-void
:cond_a
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v1, v0, Landroid/support/a/a/f$f;->c:Landroid/content/res/ColorStateList;
if-eq v1, p1, :cond_9
iput-object p1, v0, Landroid/support/a/a/f$f;->c:Landroid/content/res/ColorStateList;
iget-object v1, p0, Landroid/support/a/a/f;->d:Landroid/graphics/PorterDuffColorFilter;
iget-object v0, v0, Landroid/support/a/a/f$f;->d:Landroid/graphics/PorterDuff$Mode;
invoke-virtual {p0, v1, p1, v0}, Landroid/support/a/a/f;->a(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
move-result-object v0
iput-object v0, p0, Landroid/support/a/a/f;->d:Landroid/graphics/PorterDuffColorFilter;
invoke-virtual {p0}, Landroid/support/a/a/f;->invalidateSelf()V
goto :goto_9
.end method
.method public setTintMode(Landroid/graphics/PorterDuff$Mode;)V
.registers 4
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-static {v0, p1}, Landroid/support/v4/b/a/a;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
:cond_9
:goto_9
return-void
:cond_a
iget-object v0, p0, Landroid/support/a/a/f;->c:Landroid/support/a/a/f$f;
iget-object v1, v0, Landroid/support/a/a/f$f;->d:Landroid/graphics/PorterDuff$Mode;
if-eq v1, p1, :cond_9
iput-object p1, v0, Landroid/support/a/a/f$f;->d:Landroid/graphics/PorterDuff$Mode;
iget-object v1, p0, Landroid/support/a/a/f;->d:Landroid/graphics/PorterDuffColorFilter;
iget-object v0, v0, Landroid/support/a/a/f$f;->c:Landroid/content/res/ColorStateList;
invoke-virtual {p0, v1, v0, p1}, Landroid/support/a/a/f;->a(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
move-result-object v0
iput-object v0, p0, Landroid/support/a/a/f;->d:Landroid/graphics/PorterDuffColorFilter;
invoke-virtual {p0}, Landroid/support/a/a/f;->invalidateSelf()V
goto :goto_9
.end method
.method public setVisible(ZZ)Z
.registers 4
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_b
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z
move-result v0
:goto_a
return v0
:cond_b
invoke-super {p0, p1, p2}, Landroid/support/a/a/e;->setVisible(ZZ)Z
move-result v0
goto :goto_a
.end method
.method public unscheduleSelf(Ljava/lang/Runnable;)V
.registers 3
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
if-eqz v0, :cond_a
iget-object v0, p0, Landroid/support/a/a/f;->a:Landroid/graphics/drawable/Drawable;
invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->unscheduleSelf(Ljava/lang/Runnable;)V
:goto_9
return-void
:cond_a
invoke-super {p0, p1}, Landroid/support/a/a/e;->unscheduleSelf(Ljava/lang/Runnable;)V
goto :goto_9
.end method