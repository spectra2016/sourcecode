.class  Landroid/support/a/a/f$b;
.super Landroid/support/a/a/f$d;
.source "VectorDrawableCompat.java"
.field  a:I
.field  b:F
.field  c:I
.field  d:F
.field  e:I
.field  f:F
.field  g:F
.field  h:F
.field  i:F
.field  j:Landroid/graphics/Paint$Cap;
.field  k:Landroid/graphics/Paint$Join;
.field  l:F
.field private p:[I
.method public constructor <init>()V
.registers 4
const/4 v2, 0x0
const/high16 v1, 0x3f80
const/4 v0, 0x0
invoke-direct {p0}, Landroid/support/a/a/f$d;-><init>()V
iput v2, p0, Landroid/support/a/a/f$b;->a:I
iput v0, p0, Landroid/support/a/a/f$b;->b:F
iput v2, p0, Landroid/support/a/a/f$b;->c:I
iput v1, p0, Landroid/support/a/a/f$b;->d:F
iput v1, p0, Landroid/support/a/a/f$b;->f:F
iput v0, p0, Landroid/support/a/a/f$b;->g:F
iput v1, p0, Landroid/support/a/a/f$b;->h:F
iput v0, p0, Landroid/support/a/a/f$b;->i:F
sget-object v0, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;
iput-object v0, p0, Landroid/support/a/a/f$b;->j:Landroid/graphics/Paint$Cap;
sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;
iput-object v0, p0, Landroid/support/a/a/f$b;->k:Landroid/graphics/Paint$Join;
const/high16 v0, 0x4080
iput v0, p0, Landroid/support/a/a/f$b;->l:F
return-void
.end method
.method public constructor <init>(Landroid/support/a/a/f$b;)V
.registers 5
const/4 v2, 0x0
const/high16 v1, 0x3f80
const/4 v0, 0x0
invoke-direct {p0, p1}, Landroid/support/a/a/f$d;-><init>(Landroid/support/a/a/f$d;)V
iput v2, p0, Landroid/support/a/a/f$b;->a:I
iput v0, p0, Landroid/support/a/a/f$b;->b:F
iput v2, p0, Landroid/support/a/a/f$b;->c:I
iput v1, p0, Landroid/support/a/a/f$b;->d:F
iput v1, p0, Landroid/support/a/a/f$b;->f:F
iput v0, p0, Landroid/support/a/a/f$b;->g:F
iput v1, p0, Landroid/support/a/a/f$b;->h:F
iput v0, p0, Landroid/support/a/a/f$b;->i:F
sget-object v0, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;
iput-object v0, p0, Landroid/support/a/a/f$b;->j:Landroid/graphics/Paint$Cap;
sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;
iput-object v0, p0, Landroid/support/a/a/f$b;->k:Landroid/graphics/Paint$Join;
const/high16 v0, 0x4080
iput v0, p0, Landroid/support/a/a/f$b;->l:F
iget-object v0, p1, Landroid/support/a/a/f$b;->p:[I
iput-object v0, p0, Landroid/support/a/a/f$b;->p:[I
iget v0, p1, Landroid/support/a/a/f$b;->a:I
iput v0, p0, Landroid/support/a/a/f$b;->a:I
iget v0, p1, Landroid/support/a/a/f$b;->b:F
iput v0, p0, Landroid/support/a/a/f$b;->b:F
iget v0, p1, Landroid/support/a/a/f$b;->d:F
iput v0, p0, Landroid/support/a/a/f$b;->d:F
iget v0, p1, Landroid/support/a/a/f$b;->c:I
iput v0, p0, Landroid/support/a/a/f$b;->c:I
iget v0, p1, Landroid/support/a/a/f$b;->e:I
iput v0, p0, Landroid/support/a/a/f$b;->e:I
iget v0, p1, Landroid/support/a/a/f$b;->f:F
iput v0, p0, Landroid/support/a/a/f$b;->f:F
iget v0, p1, Landroid/support/a/a/f$b;->g:F
iput v0, p0, Landroid/support/a/a/f$b;->g:F
iget v0, p1, Landroid/support/a/a/f$b;->h:F
iput v0, p0, Landroid/support/a/a/f$b;->h:F
iget v0, p1, Landroid/support/a/a/f$b;->i:F
iput v0, p0, Landroid/support/a/a/f$b;->i:F
iget-object v0, p1, Landroid/support/a/a/f$b;->j:Landroid/graphics/Paint$Cap;
iput-object v0, p0, Landroid/support/a/a/f$b;->j:Landroid/graphics/Paint$Cap;
iget-object v0, p1, Landroid/support/a/a/f$b;->k:Landroid/graphics/Paint$Join;
iput-object v0, p0, Landroid/support/a/a/f$b;->k:Landroid/graphics/Paint$Join;
iget v0, p1, Landroid/support/a/a/f$b;->l:F
iput v0, p0, Landroid/support/a/a/f$b;->l:F
return-void
.end method
.method private a(ILandroid/graphics/Paint$Cap;)Landroid/graphics/Paint$Cap;
.registers 3
packed-switch p1, :pswitch_data_e
:goto_3
return-object p2
:pswitch_4
sget-object p2, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;
goto :goto_3
:pswitch_7
sget-object p2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;
goto :goto_3
:pswitch_a
sget-object p2, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;
goto :goto_3
nop
:pswitch_data_e
.packed-switch 0x0
:pswitch_4
:pswitch_7
:pswitch_a
.end packed-switch
.end method
.method private a(ILandroid/graphics/Paint$Join;)Landroid/graphics/Paint$Join;
.registers 3
packed-switch p1, :pswitch_data_e
:goto_3
return-object p2
:pswitch_4
sget-object p2, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;
goto :goto_3
:pswitch_7
sget-object p2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;
goto :goto_3
:pswitch_a
sget-object p2, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;
goto :goto_3
nop
:pswitch_data_e
.packed-switch 0x0
:pswitch_4
:pswitch_7
:pswitch_a
.end packed-switch
.end method
.method private a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)V
.registers 7
const/4 v3, -0x1
const/4 v0, 0x0
iput-object v0, p0, Landroid/support/a/a/f$b;->p:[I
const-string v0, "pathData"
invoke-static {p2, v0}, Landroid/support/a/a/d;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z
move-result v0
if-nez v0, :cond_d
:goto_c
return-void
:cond_d
const/4 v0, 0x0
invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_16
iput-object v0, p0, Landroid/support/a/a/f$b;->n:Ljava/lang/String;
:cond_16
const/4 v0, 0x2
invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_23
invoke-static {v0}, Landroid/support/a/a/c;->a(Ljava/lang/String;)[Landroid/support/a/a/c$b;
move-result-object v0
iput-object v0, p0, Landroid/support/a/a/f$b;->m:[Landroid/support/a/a/c$b;
:cond_23
const-string v0, "fillColor"
const/4 v1, 0x1
iget v2, p0, Landroid/support/a/a/f$b;->c:I
invoke-static {p1, p2, v0, v1, v2}, Landroid/support/a/a/d;->b(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;II)I
move-result v0
iput v0, p0, Landroid/support/a/a/f$b;->c:I
const-string v0, "fillAlpha"
const/16 v1, 0xc
iget v2, p0, Landroid/support/a/a/f$b;->f:F
invoke-static {p1, p2, v0, v1, v2}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F
move-result v0
iput v0, p0, Landroid/support/a/a/f$b;->f:F
const-string v0, "strokeLineCap"
const/16 v1, 0x8
invoke-static {p1, p2, v0, v1, v3}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;II)I
move-result v0
iget-object v1, p0, Landroid/support/a/a/f$b;->j:Landroid/graphics/Paint$Cap;
invoke-direct {p0, v0, v1}, Landroid/support/a/a/f$b;->a(ILandroid/graphics/Paint$Cap;)Landroid/graphics/Paint$Cap;
move-result-object v0
iput-object v0, p0, Landroid/support/a/a/f$b;->j:Landroid/graphics/Paint$Cap;
const-string v0, "strokeLineJoin"
const/16 v1, 0x9
invoke-static {p1, p2, v0, v1, v3}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;II)I
move-result v0
iget-object v1, p0, Landroid/support/a/a/f$b;->k:Landroid/graphics/Paint$Join;
invoke-direct {p0, v0, v1}, Landroid/support/a/a/f$b;->a(ILandroid/graphics/Paint$Join;)Landroid/graphics/Paint$Join;
move-result-object v0
iput-object v0, p0, Landroid/support/a/a/f$b;->k:Landroid/graphics/Paint$Join;
const-string v0, "strokeMiterLimit"
const/16 v1, 0xa
iget v2, p0, Landroid/support/a/a/f$b;->l:F
invoke-static {p1, p2, v0, v1, v2}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F
move-result v0
iput v0, p0, Landroid/support/a/a/f$b;->l:F
const-string v0, "strokeColor"
const/4 v1, 0x3
iget v2, p0, Landroid/support/a/a/f$b;->a:I
invoke-static {p1, p2, v0, v1, v2}, Landroid/support/a/a/d;->b(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;II)I
move-result v0
iput v0, p0, Landroid/support/a/a/f$b;->a:I
const-string v0, "strokeAlpha"
const/16 v1, 0xb
iget v2, p0, Landroid/support/a/a/f$b;->d:F
invoke-static {p1, p2, v0, v1, v2}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F
move-result v0
iput v0, p0, Landroid/support/a/a/f$b;->d:F
const-string v0, "strokeWidth"
const/4 v1, 0x4
iget v2, p0, Landroid/support/a/a/f$b;->b:F
invoke-static {p1, p2, v0, v1, v2}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F
move-result v0
iput v0, p0, Landroid/support/a/a/f$b;->b:F
const-string v0, "trimPathEnd"
const/4 v1, 0x6
iget v2, p0, Landroid/support/a/a/f$b;->h:F
invoke-static {p1, p2, v0, v1, v2}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F
move-result v0
iput v0, p0, Landroid/support/a/a/f$b;->h:F
const-string v0, "trimPathOffset"
const/4 v1, 0x7
iget v2, p0, Landroid/support/a/a/f$b;->i:F
invoke-static {p1, p2, v0, v1, v2}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F
move-result v0
iput v0, p0, Landroid/support/a/a/f$b;->i:F
const-string v0, "trimPathStart"
const/4 v1, 0x5
iget v2, p0, Landroid/support/a/a/f$b;->g:F
invoke-static {p1, p2, v0, v1, v2}, Landroid/support/a/a/d;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F
move-result v0
iput v0, p0, Landroid/support/a/a/f$b;->g:F
goto/16 :goto_c
.end method
.method public a(Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;Lorg/xmlpull/v1/XmlPullParser;)V
.registers 6
sget-object v0, Landroid/support/a/a/a;->c:[I
invoke-static {p1, p3, p2, v0}, Landroid/support/a/a/e;->b(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
move-result-object v0
invoke-direct {p0, v0, p4}, Landroid/support/a/a/f$b;->a(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)V
invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V
return-void
.end method