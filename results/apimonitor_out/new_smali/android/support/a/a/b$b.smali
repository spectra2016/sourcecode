.class  Landroid/support/a/a/b$b;
.super Landroid/graphics/drawable/Drawable$ConstantState;
.source "AnimatedVectorDrawableCompat.java"
.field private final a:Landroid/graphics/drawable/Drawable$ConstantState;
.method public constructor <init>(Landroid/graphics/drawable/Drawable$ConstantState;)V
.registers 2
invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V
iput-object p1, p0, Landroid/support/a/a/b$b;->a:Landroid/graphics/drawable/Drawable$ConstantState;
return-void
.end method
.method public canApplyTheme()Z
.registers 2
iget-object v0, p0, Landroid/support/a/a/b$b;->a:Landroid/graphics/drawable/Drawable$ConstantState;
invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->canApplyTheme()Z
move-result v0
return v0
.end method
.method public getChangingConfigurations()I
.registers 2
iget-object v0, p0, Landroid/support/a/a/b$b;->a:Landroid/graphics/drawable/Drawable$ConstantState;
invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->getChangingConfigurations()I
move-result v0
return v0
.end method
.method public newDrawable()Landroid/graphics/drawable/Drawable;
.registers 4
new-instance v0, Landroid/support/a/a/b;
const/4 v1, 0x0
invoke-direct {v0, v1}, Landroid/support/a/a/b;-><init>(Landroid/support/a/a/b$1;)V
iget-object v1, p0, Landroid/support/a/a/b$b;->a:Landroid/graphics/drawable/Drawable$ConstantState;
invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;
move-result-object v1
iput-object v1, v0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;
iget-object v1, v0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;
invoke-static {v0}, Landroid/support/a/a/b;->a(Landroid/support/a/a/b;)Landroid/graphics/drawable/Drawable$Callback;
move-result-object v2
invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
return-object v0
.end method
.method public newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
.registers 5
new-instance v0, Landroid/support/a/a/b;
const/4 v1, 0x0
invoke-direct {v0, v1}, Landroid/support/a/a/b;-><init>(Landroid/support/a/a/b$1;)V
iget-object v1, p0, Landroid/support/a/a/b$b;->a:Landroid/graphics/drawable/Drawable$ConstantState;
invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
move-result-object v1
iput-object v1, v0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;
iget-object v1, v0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;
invoke-static {v0}, Landroid/support/a/a/b;->a(Landroid/support/a/a/b;)Landroid/graphics/drawable/Drawable$Callback;
move-result-object v2
invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
return-object v0
.end method
.method public newDrawable(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
.registers 6
new-instance v0, Landroid/support/a/a/b;
const/4 v1, 0x0
invoke-direct {v0, v1}, Landroid/support/a/a/b;-><init>(Landroid/support/a/a/b$1;)V
iget-object v1, p0, Landroid/support/a/a/b$b;->a:Landroid/graphics/drawable/Drawable$ConstantState;
invoke-virtual {v1, p1, p2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
move-result-object v1
iput-object v1, v0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;
iget-object v1, v0, Landroid/support/a/a/b;->a:Landroid/graphics/drawable/Drawable;
invoke-static {v0}, Landroid/support/a/a/b;->a(Landroid/support/a/a/b;)Landroid/graphics/drawable/Drawable$Callback;
move-result-object v2
invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
return-object v0
.end method