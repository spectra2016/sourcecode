.class  Landroid/support/a/a/b$a;
.super Landroid/graphics/drawable/Drawable$ConstantState;
.source "AnimatedVectorDrawableCompat.java"
.field  a:I
.field  b:Landroid/support/a/a/f;
.field  c:Ljava/util/ArrayList;
.field  d:Landroid/support/v4/e/a;
.method public constructor <init>(Landroid/content/Context;Landroid/support/a/a/b$a;Landroid/graphics/drawable/Drawable$Callback;Landroid/content/res/Resources;)V
.registers 10
const/4 v1, 0x0
invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V
if-eqz p2, :cond_8b
iget v0, p2, Landroid/support/a/a/b$a;->a:I
iput v0, p0, Landroid/support/a/a/b$a;->a:I
iget-object v0, p2, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;
if-eqz v0, :cond_3d
iget-object v0, p2, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;
invoke-virtual {v0}, Landroid/support/a/a/f;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
move-result-object v0
if-eqz p4, :cond_82
invoke-virtual {v0, p4}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
move-result-object v0
check-cast v0, Landroid/support/a/a/f;
iput-object v0, p0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;
:goto_1e
iget-object v0, p0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;
invoke-virtual {v0}, Landroid/support/a/a/f;->mutate()Landroid/graphics/drawable/Drawable;
move-result-object v0
check-cast v0, Landroid/support/a/a/f;
iput-object v0, p0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;
iget-object v0, p0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;
invoke-virtual {v0, p3}, Landroid/support/a/a/f;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
iget-object v0, p0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;
iget-object v2, p2, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;
invoke-virtual {v2}, Landroid/support/a/a/f;->getBounds()Landroid/graphics/Rect;
move-result-object v2
invoke-virtual {v0, v2}, Landroid/support/a/a/f;->setBounds(Landroid/graphics/Rect;)V
iget-object v0, p0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;
invoke-virtual {v0, v1}, Landroid/support/a/a/f;->a(Z)V
:cond_3d
iget-object v0, p2, Landroid/support/a/a/b$a;->c:Ljava/util/ArrayList;
if-eqz v0, :cond_8b
iget-object v0, p2, Landroid/support/a/a/b$a;->c:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v2
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V
iput-object v0, p0, Landroid/support/a/a/b$a;->c:Ljava/util/ArrayList;
new-instance v0, Landroid/support/v4/e/a;
invoke-direct {v0, v2}, Landroid/support/v4/e/a;-><init>(I)V
iput-object v0, p0, Landroid/support/a/a/b$a;->d:Landroid/support/v4/e/a;
:goto_55
if-ge v1, v2, :cond_8b
iget-object v0, p2, Landroid/support/a/a/b$a;->c:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/animation/Animator;
invoke-virtual {v0}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;
move-result-object v3
iget-object v4, p2, Landroid/support/a/a/b$a;->d:Landroid/support/v4/e/a;
invoke-virtual {v4, v0}, Landroid/support/v4/e/a;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
iget-object v4, p0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;
invoke-virtual {v4, v0}, Landroid/support/a/a/f;->a(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v4
invoke-virtual {v3, v4}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V
iget-object v4, p0, Landroid/support/a/a/b$a;->c:Ljava/util/ArrayList;
invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
iget-object v4, p0, Landroid/support/a/a/b$a;->d:Landroid/support/v4/e/a;
invoke-virtual {v4, v3, v0}, Landroid/support/v4/e/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_55
:cond_82
invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;
move-result-object v0
check-cast v0, Landroid/support/a/a/f;
iput-object v0, p0, Landroid/support/a/a/b$a;->b:Landroid/support/a/a/f;
goto :goto_1e
:cond_8b
return-void
.end method
.method public getChangingConfigurations()I
.registers 2
iget v0, p0, Landroid/support/a/a/b$a;->a:I
return v0
.end method
.method public newDrawable()Landroid/graphics/drawable/Drawable;
.registers 3
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "No constant state support for SDK < 23."
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
.end method
.method public newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
.registers 4
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "No constant state support for SDK < 23."
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
.end method