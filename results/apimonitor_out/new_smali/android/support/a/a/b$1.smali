.class  Landroid/support/a/a/b$1;
.super Ljava/lang/Object;
.source "AnimatedVectorDrawableCompat.java"
.implements Landroid/graphics/drawable/Drawable$Callback;
.field final synthetic a:Landroid/support/a/a/b;
.method constructor <init>(Landroid/support/a/a/b;)V
.registers 2
iput-object p1, p0, Landroid/support/a/a/b$1;->a:Landroid/support/a/a/b;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
.registers 3
iget-object v0, p0, Landroid/support/a/a/b$1;->a:Landroid/support/a/a/b;
invoke-virtual {v0}, Landroid/support/a/a/b;->invalidateSelf()V
return-void
.end method
.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
.registers 6
iget-object v0, p0, Landroid/support/a/a/b$1;->a:Landroid/support/a/a/b;
invoke-virtual {v0, p2, p3, p4}, Landroid/support/a/a/b;->scheduleSelf(Ljava/lang/Runnable;J)V
return-void
.end method
.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
.registers 4
iget-object v0, p0, Landroid/support/a/a/b$1;->a:Landroid/support/a/a/b;
invoke-virtual {v0, p2}, Landroid/support/a/a/b;->unscheduleSelf(Ljava/lang/Runnable;)V
return-void
.end method