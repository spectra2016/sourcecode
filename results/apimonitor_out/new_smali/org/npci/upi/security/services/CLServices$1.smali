.class  Lorg/npci/upi/security/services/CLServices$1;
.super Ljava/lang/Object;
.source "CLServices.java"
.implements Landroid/content/ServiceConnection;
.field final synthetic a:Lorg/npci/upi/security/services/CLServices;
.method constructor <init>(Lorg/npci/upi/security/services/CLServices;)V
.registers 2
iput-object p1, p0, Lorg/npci/upi/security/services/CLServices$1;->a:Lorg/npci/upi/security/services/CLServices;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.registers 5
iget-object v0, p0, Lorg/npci/upi/security/services/CLServices$1;->a:Lorg/npci/upi/security/services/CLServices;
invoke-static {p2}, Lorg/npci/upi/security/services/b;->a(Landroid/os/IBinder;)Lorg/npci/upi/security/services/a;
move-result-object v1
#setter for: Lorg/npci/upi/security/services/CLServices;->clRemoteService:Lorg/npci/upi/security/services/a;
invoke-static {v0, v1}, Lorg/npci/upi/security/services/CLServices;->access$002(Lorg/npci/upi/security/services/CLServices;Lorg/npci/upi/security/services/a;)Lorg/npci/upi/security/services/a;
iget-object v0, p0, Lorg/npci/upi/security/services/CLServices$1;->a:Lorg/npci/upi/security/services/CLServices;
#getter for: Lorg/npci/upi/security/services/CLServices;->notifier:Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;
invoke-static {v0}, Lorg/npci/upi/security/services/CLServices;->access$200(Lorg/npci/upi/security/services/CLServices;)Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;
move-result-object v0
invoke-static {}, Lorg/npci/upi/security/services/CLServices;->access$100()Lorg/npci/upi/security/services/CLServices;
move-result-object v1
invoke-interface {v0, v1}, Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;->serviceConnected(Lorg/npci/upi/security/services/CLServices;)V
const-string v0, "Remote Service"
const-string v1, "Service Connected"
invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
return-void
.end method
.method public onServiceDisconnected(Landroid/content/ComponentName;)V
.registers 4
iget-object v0, p0, Lorg/npci/upi/security/services/CLServices$1;->a:Lorg/npci/upi/security/services/CLServices;
const/4 v1, 0x0
#setter for: Lorg/npci/upi/security/services/CLServices;->clRemoteService:Lorg/npci/upi/security/services/a;
invoke-static {v0, v1}, Lorg/npci/upi/security/services/CLServices;->access$002(Lorg/npci/upi/security/services/CLServices;Lorg/npci/upi/security/services/a;)Lorg/npci/upi/security/services/a;
iget-object v0, p0, Lorg/npci/upi/security/services/CLServices$1;->a:Lorg/npci/upi/security/services/CLServices;
#getter for: Lorg/npci/upi/security/services/CLServices;->notifier:Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;
invoke-static {v0}, Lorg/npci/upi/security/services/CLServices;->access$200(Lorg/npci/upi/security/services/CLServices;)Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;
move-result-object v0
invoke-interface {v0}, Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;->serviceDisconnected()V
const-string v0, "Remote Service"
const-string v1, "Service Disconnected"
invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
return-void
.end method