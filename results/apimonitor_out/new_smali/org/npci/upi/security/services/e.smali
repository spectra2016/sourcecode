.class public abstract Lorg/npci/upi/security/services/e;
.super Landroid/os/Binder;
.implements Lorg/npci/upi/security/services/d;
.method public constructor <init>()V
.registers 2
invoke-direct {p0}, Landroid/os/Binder;-><init>()V
const-string v0, "org.npci.upi.security.services.CLResultReceiver"
invoke-virtual {p0, p0, v0}, Lorg/npci/upi/security/services/e;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return-void
.end method
.method public static a(Landroid/os/IBinder;)Lorg/npci/upi/security/services/d;
.registers 3
if-nez p0, :cond_4
const/4 v0, 0x0
:goto_3
return-object v0
:cond_4
const-string v0, "org.npci.upi.security.services.CLResultReceiver"
invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;
move-result-object v0
if-eqz v0, :cond_13
instance-of v1, v0, Lorg/npci/upi/security/services/d;
if-eqz v1, :cond_13
check-cast v0, Lorg/npci/upi/security/services/d;
goto :goto_3
:cond_13
new-instance v0, Lorg/npci/upi/security/services/f;
invoke-direct {v0, p0}, Lorg/npci/upi/security/services/f;-><init>(Landroid/os/IBinder;)V
goto :goto_3
.end method
.method public asBinder()Landroid/os/IBinder;
.registers 1
return-object p0
.end method
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.registers 8
const/4 v0, 0x0
const/4 v1, 0x1
sparse-switch p1, :sswitch_data_48
invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
move-result v0
:goto_9
return v0
:sswitch_a
const-string v0, "org.npci.upi.security.services.CLResultReceiver"
invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
move v0, v1
goto :goto_9
:sswitch_11
const-string v2, "org.npci.upi.security.services.CLResultReceiver"
invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
move-result v2
if-eqz v2, :cond_24
sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;
invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/os/Bundle;
:cond_24
invoke-virtual {p0, v0}, Lorg/npci/upi/security/services/e;->a(Landroid/os/Bundle;)V
invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
move v0, v1
goto :goto_9
:sswitch_2c
const-string v2, "org.npci.upi.security.services.CLResultReceiver"
invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
move-result v2
if-eqz v2, :cond_3f
sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;
invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/os/Bundle;
:cond_3f
invoke-virtual {p0, v0}, Lorg/npci/upi/security/services/e;->b(Landroid/os/Bundle;)V
invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
move v0, v1
goto :goto_9
nop
:sswitch_data_48
.sparse-switch
0x1 -> :sswitch_11
0x2 -> :sswitch_2c
0x5f4e5446 -> :sswitch_a
.end sparse-switch
.end method