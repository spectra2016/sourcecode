.class public Lorg/npci/upi/security/pinactivitycomponent/d;
.super Ljava/lang/Exception;
.field  a:Ljava/lang/String;
.field private b:Ljava/lang/String;
.field private c:Ljava/lang/String;
.field private d:Landroid/content/Context;
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
.registers 5
invoke-direct {p0}, Ljava/lang/Exception;-><init>()V
const-string v0, "CLException"
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/d;->a:Ljava/lang/String;
iput-object p2, p0, Lorg/npci/upi/security/pinactivitycomponent/d;->b:Ljava/lang/String;
iput-object p3, p0, Lorg/npci/upi/security/pinactivitycomponent/d;->c:Ljava/lang/String;
iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/d;->d:Landroid/content/Context;
invoke-virtual {p0, p1, p3}, Lorg/npci/upi/security/pinactivitycomponent/d;->a(Landroid/content/Context;Ljava/lang/String;)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.registers 6
invoke-direct {p0, p4}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V
const-string v0, "CLException"
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/d;->a:Ljava/lang/String;
iput-object p2, p0, Lorg/npci/upi/security/pinactivitycomponent/d;->b:Ljava/lang/String;
iput-object p3, p0, Lorg/npci/upi/security/pinactivitycomponent/d;->c:Ljava/lang/String;
iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/d;->d:Landroid/content/Context;
invoke-virtual {p0, p1, p3}, Lorg/npci/upi/security/pinactivitycomponent/d;->a(Landroid/content/Context;Ljava/lang/String;)V
return-void
.end method
.method public a()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/d;->c:Ljava/lang/String;
return-object v0
.end method
.method public a(Landroid/content/Context;Ljava/lang/String;)V
.registers 7
new-instance v2, Ljava/util/Properties;
invoke-direct {v2}, Ljava/util/Properties;-><init>()V
invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;
move-result-object v0
const/4 v1, 0x0
:try_start_a
const-string v3, "cl-messages_en_us.properties"
invoke-virtual {v0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
:try_end_f
.catch Ljava/io/IOException; {:try_start_a .. :try_end_f} :catch_56
move-result-object v0
:try_start_10
:goto_10
invoke-virtual {v2, v0}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
:try_end_13
.catch Ljava/io/IOException; {:try_start_10 .. :try_end_13} :catch_62
:goto_13
invoke-virtual {v2, p2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/d;->a:Ljava/lang/String;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "ErrorMsg: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v0
sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->error_msg:I
invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
move-result-object v2
move-object v0, p1
check-cast v0, Landroid/app/Activity;
sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$d;->error_layout:I
invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/RelativeLayout;
check-cast p1, Landroid/app/Activity;
sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$d;->error_message:I
invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;
move-result-object v1
check-cast v1, Landroid/widget/TextView;
const/4 v3, 0x0
invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V
invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
return-void
:catch_56
move-exception v0
iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/d;->a:Ljava/lang/String;
invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;
move-result-object v0
invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
move-object v0, v1
goto :goto_10
:catch_62
move-exception v0
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/d;->a:Ljava/lang/String;
invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;
move-result-object v0
invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_13
.end method