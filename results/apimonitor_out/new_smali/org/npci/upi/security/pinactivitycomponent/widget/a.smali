.class public Lorg/npci/upi/security/pinactivitycomponent/widget/a;
.super Landroid/widget/FrameLayout;
.implements Lorg/npci/upi/security/pinactivitycomponent/widget/c;
.field private a:Ljava/util/ArrayList;
.field private b:I
.field private c:I
.field private d:Ljava/lang/Object;
.method public constructor <init>(Landroid/content/Context;)V
.registers 2
invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V
return-void
.end method
.method private a(Landroid/view/View;)V
.registers 5
const/4 v2, 0x0
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0xc
if-lt v0, v1, :cond_f
invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;
move-result-object v0
invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;
:goto_e
return-void
:cond_f
invoke-static {p1, v2}, Landroid/support/v4/f/af;->c(Landroid/view/View;F)V
goto :goto_e
.end method
.method private a(Landroid/view/View;Z)V
.registers 6
if-eqz p2, :cond_15
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->c:I
mul-int/lit8 v0, v0, -0x1
:goto_6
sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v2, 0xc
if-lt v1, v2, :cond_18
invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;
move-result-object v1
int-to-float v0, v0
invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;
:goto_14
return-void
:cond_15
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->c:I
goto :goto_6
:cond_18
int-to-float v0, v0
invoke-static {p1, v0}, Landroid/support/v4/f/af;->c(Landroid/view/View;F)V
goto :goto_14
.end method
.method private e()V
.registers 4
const/4 v0, 0x0
move v1, v0
:goto_2
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
if-ge v1, v0, :cond_1b
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
const-string v2, ""
invoke-virtual {v0, v2}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setText(Ljava/lang/String;)V
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_2
:cond_1b
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b()Z
return-void
.end method
.method public a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;IZZ)V
.registers 15
const/4 v0, 0x0
move v7, v0
:goto_2
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
if-ge v7, v0, :cond_1f
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
move-object v1, p1
move-object v2, p2
move-object v3, p3
move v4, p4
move v5, p5
move v6, p6
invoke-virtual/range {v0 .. v6}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;IZZ)V
add-int/lit8 v0, v7, 0x1
move v7, v0
goto :goto_2
:cond_1f
return-void
.end method
.method public a(Ljava/util/ArrayList;Lorg/npci/upi/security/pinactivitycomponent/widget/o;)V
.registers 6
const/4 v1, 0x0
iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/view/View;
invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->addView(Landroid/view/View;)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0, p2}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setFormItemListener(Lorg/npci/upi/security/pinactivitycomponent/widget/o;)V
iput v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
move-result-object v0
iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I
iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->c:I
const/4 v0, 0x1
move v1, v0
:goto_29
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
if-ge v1, v0, :cond_49
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0, p2}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setFormItemListener(Lorg/npci/upi/security/pinactivitycomponent/widget/o;)V
iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->c:I
int-to-float v2, v2
invoke-static {v0, v2}, Landroid/support/v4/f/af;->c(Landroid/view/View;F)V
invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->addView(Landroid/view/View;)V
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_29
:cond_49
return-void
.end method
.method public a()Z
.registers 4
const/4 v1, 0x1
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
move-result v2
add-int/lit8 v2, v2, -0x1
if-lt v0, v2, :cond_f
const/4 v0, 0x0
:goto_e
return v0
:cond_f
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-direct {p0, v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a(Landroid/view/View;Z)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
add-int/lit8 v2, v2, 0x1
invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-direct {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a(Landroid/view/View;)V
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
add-int/lit8 v0, v0, 0x1
iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->requestFocus()Z
move v0, v1
goto :goto_e
.end method
.method public b()Z
.registers 4
const/4 v1, 0x0
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
if-nez v0, :cond_7
move v0, v1
:goto_6
return v0
:cond_7
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
iget v2, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-direct {p0, v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a(Landroid/view/View;Z)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
add-int/lit8 v1, v1, -0x1
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-direct {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a(Landroid/view/View;)V
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
add-int/lit8 v0, v0, -0x1
iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->requestFocus()Z
const/4 v0, 0x1
goto :goto_6
.end method
.method public c()Z
.registers 3
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->c()Z
move-result v0
return v0
.end method
.method public d()Z
.registers 6
const/4 v3, 0x1
const/4 v2, 0x0
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputValue()Ljava/lang/String;
move-result-object v4
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputLength()I
move-result v0
invoke-virtual {v4}, Ljava/lang/String;->length()I
move-result v1
if-ne v0, v1, :cond_83
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
move-result v1
add-int/lit8 v1, v1, -0x1
if-ne v0, v1, :cond_7b
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->requestFocus()Z
move v1, v2
:goto_3e
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
if-ge v1, v0, :cond_79
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputValue()Ljava/lang/String;
move-result-object v0
invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-nez v0, :cond_75
invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->e()V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getFormItemListener()Lorg/npci/upi/security/pinactivitycomponent/widget/o;
move-result-object v0
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->getContext()Landroid/content/Context;
move-result-object v1
sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$f;->info_pins_dont_match:I
invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;
move-result-object v1
invoke-interface {v0, p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/o;->a(Landroid/view/View;Ljava/lang/String;)V
:goto_74
:cond_74
return v2
:cond_75
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_3e
:cond_79
move v2, v3
goto :goto_74
:cond_7b
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a()Z
move-result v0
if-nez v0, :cond_74
move v2, v3
goto :goto_74
:cond_83
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->b:I
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->requestFocus()Z
goto :goto_74
.end method
.method public getFormDataTag()Ljava/lang/Object;
.registers 3
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->d:Ljava/lang/Object;
if-nez v0, :cond_12
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
const/4 v1, 0x0
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getFormDataTag()Ljava/lang/Object;
move-result-object v0
:goto_11
return-object v0
:cond_12
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->d:Ljava/lang/Object;
goto :goto_11
.end method
.method public getInputValue()Ljava/lang/String;
.registers 3
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
const/4 v1, 0x0
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputValue()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public setFormDataTag(Ljava/lang/Object;)V
.registers 2
iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->d:Ljava/lang/Object;
return-void
.end method
.method public setText(Ljava/lang/String;)V
.registers 4
const/4 v0, 0x0
move v1, v0
:goto_2
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
if-ge v1, v0, :cond_19
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0, p1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setText(Ljava/lang/String;)V
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_2
:cond_19
return-void
.end method