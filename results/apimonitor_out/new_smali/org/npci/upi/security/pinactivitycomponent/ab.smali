.class public Lorg/npci/upi/security/pinactivitycomponent/ab;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.method public constructor <init>(Landroid/content/Context;)V
.registers 5
const-string v0, "contactsManager"
const/4 v1, 0x0
const/4 v2, 0x1
invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
return-void
.end method
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.registers 7
const/4 v0, 0x0
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/ab;->a()Ljava/util/List;
move-result-object v1
if-eqz v1, :cond_18
invoke-interface {v1}, Ljava/util/List;->isEmpty()Z
move-result v2
if-nez v2, :cond_18
const/4 v0, 0x0
invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/u;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/u;->a()Ljava/lang/String;
move-result-object v0
:cond_18
return-object v0
.end method
.method public a()Ljava/util/List;
.registers 5
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
const-string v1, "SELECT  * FROM contacts"
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/ab;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v2
const/4 v3, 0x0
invoke-virtual {v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
move-result-object v1
invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
move-result v2
if-eqz v2, :cond_3c
:cond_16
new-instance v2, Lorg/npci/upi/security/pinactivitycomponent/u;
invoke-direct {v2}, Lorg/npci/upi/security/pinactivitycomponent/u;-><init>()V
const/4 v3, 0x1
invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Lorg/npci/upi/security/pinactivitycomponent/u;->a(Ljava/lang/String;)V
const/4 v3, 0x2
invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Lorg/npci/upi/security/pinactivitycomponent/u;->b(Ljava/lang/String;)V
const/4 v3, 0x3
invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Lorg/npci/upi/security/pinactivitycomponent/u;->c(Ljava/lang/String;)V
invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
move-result v2
if-nez v2, :cond_16
:cond_3c
return-object v0
.end method
.method  a(Lorg/npci/upi/security/pinactivitycomponent/u;)V
.registers 6
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/ab;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
new-instance v1, Landroid/content/ContentValues;
invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V
const-string v2, "k0"
invoke-virtual {p1}, Lorg/npci/upi/security/pinactivitycomponent/u;->a()Ljava/lang/String;
move-result-object v3
invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v2, "token"
invoke-virtual {p1}, Lorg/npci/upi/security/pinactivitycomponent/u;->b()Ljava/lang/String;
move-result-object v3
invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v2, "date"
invoke-virtual {p1}, Lorg/npci/upi/security/pinactivitycomponent/u;->c()Ljava/lang/String;
move-result-object v3
invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v2, "contacts"
const/4 v3, 0x0
invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
return-void
.end method
.method public b(Lorg/npci/upi/security/pinactivitycomponent/u;)I
.registers 9
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/ab;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
new-instance v1, Landroid/content/ContentValues;
invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V
const-string v2, "k0"
invoke-virtual {p1}, Lorg/npci/upi/security/pinactivitycomponent/u;->a()Ljava/lang/String;
move-result-object v3
invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v2, "token"
invoke-virtual {p1}, Lorg/npci/upi/security/pinactivitycomponent/u;->b()Ljava/lang/String;
move-result-object v3
invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v2, "date"
invoke-virtual {p1}, Lorg/npci/upi/security/pinactivitycomponent/u;->c()Ljava/lang/String;
move-result-object v3
invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v2, "contacts"
const-string v3, "k0 = ?"
const/4 v4, 0x1
new-array v4, v4, [Ljava/lang/String;
const/4 v5, 0x0
invoke-virtual {p1}, Lorg/npci/upi/security/pinactivitycomponent/u;->a()Ljava/lang/String;
move-result-object v6
aput-object v6, v4, v5
invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
move-result v0
return v0
.end method
.method public b()Ljava/lang/String;
.registers 4
const-string v0, ""
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/ab;->a()Ljava/util/List;
move-result-object v1
if-eqz v1, :cond_19
invoke-interface {v1}, Ljava/util/List;->isEmpty()Z
move-result v2
if-nez v2, :cond_19
const/4 v0, 0x0
invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/u;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/u;->b()Ljava/lang/String;
move-result-object v0
:cond_19
return-object v0
.end method
.method public c()V
.registers 3
const-string v0, "DB Handler"
const-string v1, "Deleting all"
invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/ab;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
const-string v1, "delete from contacts"
invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
return-void
.end method
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
.registers 4
const-string v0, "CREATE TABLE contacts(id INTEGER PRIMARY KEY,k0 TEXT,token TEXT,date TEXT)"
invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
const-string v0, "Dynamic DB"
const-string v1, "tables created"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
return-void
.end method
.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
.registers 5
const-string v0, "DROP TABLE IF EXISTS contacts"
invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
invoke-virtual {p0, p1}, Lorg/npci/upi/security/pinactivitycomponent/ab;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
return-void
.end method