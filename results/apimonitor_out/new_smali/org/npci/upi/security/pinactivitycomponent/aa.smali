.class public Lorg/npci/upi/security/pinactivitycomponent/aa;
.super Ljava/lang/Object;
.field  a:Ljavax/crypto/Cipher;
.field  b:[B
.field  c:[B
.method public constructor <init>()V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const-string v0, "AES/CBC/PKCS5Padding"
invoke-static {v0}, Ldroidbox/javax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/aa;->a:Ljavax/crypto/Cipher;
const/16 v0, 0x20
new-array v0, v0, [B
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/aa;->b:[B
const/16 v0, 0x10
new-array v0, v0, [B
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/aa;->c:[B
return-void
.end method
.method public a(Ljava/lang/String;Ljava/security/PublicKey;)Ljava/lang/String;
.registers 7
const-string v0, ""
invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
move-result-object v1
const/4 v0, 0x0
:try_start_7
const-string v2, "RSA/ECB/PKCS1Padding"
invoke-static {v2}, Ldroidbox/javax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
move-result-object v2
const/4 v3, 0x1
invoke-static {v2, v3, p2}, Ldroidbox/javax/crypto/Cipher;->init(Ljavax/crypto/Cipher;ILjava/security/Key;)V
invoke-virtual {v2, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B
:try_end_14
.catch Ljava/lang/Exception; {:try_start_7 .. :try_end_14} :catch_1b
move-result-object v0
:goto_15
const/4 v1, 0x2
invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
move-result-object v0
return-object v0
:catch_1b
move-exception v1
invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
goto :goto_15
.end method
.method public a([B)Ljava/lang/String;
.registers 9
const/4 v1, 0x0
new-instance v2, Ljava/lang/StringBuilder;
array-length v0, p1
mul-int/lit8 v0, v0, 0x2
invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V
array-length v3, p1
move v0, v1
:goto_b
if-ge v0, v3, :cond_26
aget-byte v4, p1, v0
const-string v5, "%02x"
const/4 v6, 0x1
new-array v6, v6, [Ljava/lang/Object;
and-int/lit16 v4, v4, 0xff
invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v4
aput-object v4, v6, v1
invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
move-result-object v4
invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
add-int/lit8 v0, v0, 0x1
goto :goto_b
:cond_26
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public a(Ljava/lang/String;)[B
.registers 4
const-string v0, "SHA-256"
invoke-static {v0}, Ldroidbox/java/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
move-result-object v0
const-string v1, "UTF-8"
invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
move-result-object v1
invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V
invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B
move-result-object v0
return-object v0
.end method
.method public a([B[B)[B
.registers 7
new-instance v0, Ljavax/crypto/spec/SecretKeySpec;
const-string v1, "AES"
invoke-static {p2, v1}, Ldroidbox/javax/crypto/spec/SecretKeySpec;->droidbox_cons([BLjava/lang/String;)V
invoke-direct {v0, p2, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
const/16 v1, 0x10
new-array v1, v1, [B
new-instance v2, Ljavax/crypto/spec/IvParameterSpec;
invoke-static {v1}, Ldroidbox/javax/crypto/spec/IvParameterSpec;->droidbox_cons([B)V
invoke-direct {v2, v1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V
const-string v1, "AES/CBC/PKCS5Padding"
invoke-static {v1}, Ldroidbox/javax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
move-result-object v1
const/4 v3, 0x1
invoke-static {v1, v3, v0, v2}, Ldroidbox/javax/crypto/Cipher;->init(Ljavax/crypto/Cipher;ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
move-result-object v0
return-object v0
.end method
.method public b(Ljava/lang/String;)[B
.registers 6
invoke-virtual {p1}, Ljava/lang/String;->length()I
move-result v0
div-int/lit8 v0, v0, 0x2
new-array v1, v0, [B
const/4 v0, 0x0
:goto_9
array-length v2, v1
if-ge v0, v2, :cond_20
mul-int/lit8 v2, v0, 0x2
add-int/lit8 v3, v2, 0x2
invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
move-result-object v2
const/16 v3, 0x10
invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
move-result v2
int-to-byte v2, v2
aput-byte v2, v1, v0
add-int/lit8 v0, v0, 0x1
goto :goto_9
:cond_20
return-object v1
.end method
.method public b([B[B)[B
.registers 7
new-instance v0, Ljavax/crypto/spec/SecretKeySpec;
const-string v1, "AES"
invoke-static {p2, v1}, Ldroidbox/javax/crypto/spec/SecretKeySpec;->droidbox_cons([BLjava/lang/String;)V
invoke-direct {v0, p2, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
const/16 v1, 0x10
new-array v1, v1, [B
new-instance v2, Ljavax/crypto/spec/IvParameterSpec;
invoke-static {v1}, Ldroidbox/javax/crypto/spec/IvParameterSpec;->droidbox_cons([B)V
invoke-direct {v2, v1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V
const-string v1, "AES/CBC/PKCS5Padding"
invoke-static {v1}, Ldroidbox/javax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
move-result-object v1
const/4 v3, 0x2
invoke-static {v1, v3, v0, v2}, Ldroidbox/javax/crypto/Cipher;->init(Ljavax/crypto/Cipher;ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
invoke-virtual {v1, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B
move-result-object v0
return-object v0
.end method