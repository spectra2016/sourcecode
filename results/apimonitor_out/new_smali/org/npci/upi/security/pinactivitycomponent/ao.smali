.class public Lorg/npci/upi/security/pinactivitycomponent/ao;
.super Ljava/lang/Object;
.method public static a(Ljava/lang/Object;)Ljava/lang/String;
.registers 2
new-instance v0, Ljava/lang/StringBuilder;
invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
invoke-static {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/ao;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method private static a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
.registers 8
const/4 v0, 0x0
if-nez p0, :cond_9
const-string v0, "null"
invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
:goto_8
return-void
:cond_9
invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z
move-result v2
if-eqz v2, :cond_3d
const-string v1, "["
invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
:goto_18
invoke-static {p0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I
move-result v1
if-ge v0, v1, :cond_2d
invoke-static {p0, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;
move-result-object v1
invoke-static {v1, p1}, Lorg/npci/upi/security/pinactivitycomponent/ao;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
const-string v1, ","
invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
add-int/lit8 v0, v0, 0x1
goto :goto_18
:cond_2d
invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I
move-result v0
add-int/lit8 v0, v0, -0x1
invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I
move-result v1
const-string v2, "]"
invoke-virtual {p1, v0, v1, v2}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;
goto :goto_8
:cond_3d
const-class v2, Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
move-result v2
if-eqz v2, :cond_55
const-string v0, "\""
invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, "\""
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
goto :goto_8
:cond_55
invoke-virtual {v1}, Ljava/lang/Class;->isPrimitive()Z
move-result v2
if-nez v2, :cond_8b
const-class v2, Ljava/lang/Integer;
invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
move-result v2
if-nez v2, :cond_8b
const-class v2, Ljava/lang/Long;
invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
move-result v2
if-nez v2, :cond_8b
const-class v2, Ljava/lang/Short;
invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
move-result v2
if-nez v2, :cond_8b
const-class v2, Ljava/lang/Double;
invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
move-result v2
if-nez v2, :cond_8b
const-class v2, Ljava/lang/Float;
invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
move-result v2
if-nez v2, :cond_8b
const-class v2, Ljava/math/BigDecimal;
invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
move-result v2
if-eqz v2, :cond_94
:cond_8b
invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
move-result-object v0
invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
goto/16 :goto_8
:cond_94
:try_start_94
const-string v2, "{"
invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;
move-result-object v1
array-length v2, v1
:goto_9e
if-ge v0, v2, :cond_d8
aget-object v3, v1, v0
invoke-virtual {v3}, Ljava/lang/reflect/Field;->getModifiers()I
move-result v4
invoke-static {v4}, Ljava/lang/reflect/Modifier;->isStatic(I)Z
move-result v4
if-nez v4, :cond_d5
const/4 v4, 0x1
invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V
const-string v4, "\""
invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;
move-result-object v5
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
const-string v5, "\""
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
const-string v5, ":"
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invoke-virtual {v3, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v3
invoke-static {v3, p1}, Lorg/npci/upi/security/pinactivitycomponent/ao;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
const-string v3, ","
invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
:cond_d5
add-int/lit8 v0, v0, 0x1
goto :goto_9e
:cond_d8
invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I
move-result v0
add-int/lit8 v0, v0, -0x1
invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I
move-result v1
const-string v2, "}"
invoke-virtual {p1, v0, v1, v2}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;
:try_end_e7
.catch Ljava/lang/Exception; {:try_start_94 .. :try_end_e7} :catch_e9
goto/16 :goto_8
:catch_e9
move-exception v0
invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
goto/16 :goto_8
.end method