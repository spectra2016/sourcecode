.class public Lorg/npci/upi/security/pinactivitycomponent/w;
.super Ljava/lang/Object;
.field private a:Ljava/util/Map;
.field private b:Landroid/content/Context;
.field private c:Ljava/util/Properties;
.field private d:Ljava/util/Properties;
.field private e:Ljava/util/Properties;
.field private f:Lorg/npci/upi/security/pinactivitycomponent/t;
.field private g:Lorg/npci/upi/security/pinactivitycomponent/ac;
.field private h:Ljava/util/Locale;
.field private i:Lin/org/npci/commonlibrary/e;
.field private j:Landroid/app/Activity;
.field private k:Lorg/npci/upi/security/pinactivitycomponent/an;
.field private l:Lorg/npci/upi/security/pinactivitycomponent/ab;
.method public constructor <init>(Landroid/content/Context;Lorg/npci/upi/security/pinactivitycomponent/an;Landroid/app/Activity;)V
.registers 9
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
new-instance v0, Ljava/util/HashMap;
invoke-direct {v0}, Ljava/util/HashMap;-><init>()V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->a:Ljava/util/Map;
iput-object p2, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->k:Lorg/npci/upi/security/pinactivitycomponent/an;
invoke-virtual {p2}, Lorg/npci/upi/security/pinactivitycomponent/an;->b()Ljava/util/Locale;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->h:Ljava/util/Locale;
iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->b:Landroid/content/Context;
invoke-virtual {p2}, Lorg/npci/upi/security/pinactivitycomponent/an;->c()Lin/org/npci/commonlibrary/e;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->i:Lin/org/npci/commonlibrary/e;
iput-object p3, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->j:Landroid/app/Activity;
const-string v0, "cl-app.properties"
invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/w;->a(Ljava/lang/String;)Ljava/util/Properties;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->e:Ljava/util/Properties;
const-string v0, "validation.properties"
invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/w;->a(Ljava/lang/String;)Ljava/util/Properties;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->c:Ljava/util/Properties;
const-string v0, "version.properties"
invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/w;->a(Ljava/lang/String;)Ljava/util/Properties;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->d:Ljava/util/Properties;
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->h:Ljava/util/Locale;
if-eqz v0, :cond_91
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->a:Ljava/util/Map;
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->h:Ljava/util/Locale;
invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;
move-result-object v1
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "cl-messages_"
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->h:Ljava/util/Locale;
invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, ".properties"
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-virtual {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/w;->a(Ljava/lang/String;)Ljava/util/Properties;
move-result-object v2
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:goto_66
invoke-virtual {p2}, Lorg/npci/upi/security/pinactivitycomponent/an;->d()Lorg/npci/upi/security/pinactivitycomponent/ab;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->l:Lorg/npci/upi/security/pinactivitycomponent/ab;
new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/t;
invoke-direct {v0, p0}, Lorg/npci/upi/security/pinactivitycomponent/t;-><init>(Lorg/npci/upi/security/pinactivitycomponent/w;)V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->f:Lorg/npci/upi/security/pinactivitycomponent/t;
if-eqz p2, :cond_90
invoke-virtual {p2}, Lorg/npci/upi/security/pinactivitycomponent/an;->c()Lin/org/npci/commonlibrary/e;
move-result-object v0
if-eqz v0, :cond_90
invoke-virtual {p2}, Lorg/npci/upi/security/pinactivitycomponent/an;->a()Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_90
new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/ac;
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->i:Lin/org/npci/commonlibrary/e;
iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->l:Lorg/npci/upi/security/pinactivitycomponent/ab;
invoke-virtual {p2}, Lorg/npci/upi/security/pinactivitycomponent/an;->a()Ljava/lang/String;
move-result-object v3
invoke-direct {v0, v1, v2, v3}, Lorg/npci/upi/security/pinactivitycomponent/ac;-><init>(Lin/org/npci/commonlibrary/e;Lorg/npci/upi/security/pinactivitycomponent/ab;Ljava/lang/String;)V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->g:Lorg/npci/upi/security/pinactivitycomponent/ac;
:cond_90
return-void
:cond_91
new-instance v0, Ljava/util/Locale;
const-string v1, "en_US"
invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->a:Ljava/util/Map;
invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;
move-result-object v2
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "cl-messages_"
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;
move-result-object v0
invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v3, ".properties"
invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/w;->a(Ljava/lang/String;)Ljava/util/Properties;
move-result-object v0
invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
goto :goto_66
.end method
.method public a(Ljava/lang/String;)Ljava/util/Properties;
.registers 5
new-instance v1, Ljava/util/Properties;
invoke-direct {v1}, Ljava/util/Properties;-><init>()V
:try_start_5
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->b:Landroid/content/Context;
invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
move-result-object v0
invoke-virtual {v1, v0}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
:goto_12
:try_end_12
.catch Ljava/io/IOException; {:try_start_5 .. :try_end_12} :catch_13
return-object v1
:catch_13
move-exception v0
const-string v2, "AssetsPropertyReader"
invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;
move-result-object v0
invoke-static {v2, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/String;)V
goto :goto_12
.end method
.method public a()Lorg/npci/upi/security/pinactivitycomponent/t;
.registers 2
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->f:Lorg/npci/upi/security/pinactivitycomponent/t;
return-object v0
.end method
.method public b(Ljava/lang/String;)Ljava/lang/String;
.registers 3
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->d:Ljava/util/Properties;
if-eqz v0, :cond_b
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->d:Ljava/util/Properties;
invoke-virtual {v0, p1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
:goto_a
return-object v0
:cond_b
const/4 v0, 0x0
goto :goto_a
.end method
.method public b()Lorg/npci/upi/security/pinactivitycomponent/ac;
.registers 6
const/4 v1, 0x0
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->g:Lorg/npci/upi/security/pinactivitycomponent/ac;
if-nez v0, :cond_2a
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->k:Lorg/npci/upi/security/pinactivitycomponent/an;
if-eqz v0, :cond_2a
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->k:Lorg/npci/upi/security/pinactivitycomponent/an;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/an;->c()Lin/org/npci/commonlibrary/e;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->i:Lin/org/npci/commonlibrary/e;
new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/ac;
iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->k:Lorg/npci/upi/security/pinactivitycomponent/an;
invoke-virtual {v2}, Lorg/npci/upi/security/pinactivitycomponent/an;->c()Lin/org/npci/commonlibrary/e;
move-result-object v2
iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->k:Lorg/npci/upi/security/pinactivitycomponent/an;
invoke-virtual {v3}, Lorg/npci/upi/security/pinactivitycomponent/an;->d()Lorg/npci/upi/security/pinactivitycomponent/ab;
move-result-object v3
iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->k:Lorg/npci/upi/security/pinactivitycomponent/an;
invoke-virtual {v4}, Lorg/npci/upi/security/pinactivitycomponent/an;->a()Ljava/lang/String;
move-result-object v4
invoke-direct {v0, v2, v3, v4}, Lorg/npci/upi/security/pinactivitycomponent/ac;-><init>(Lin/org/npci/commonlibrary/e;Lorg/npci/upi/security/pinactivitycomponent/ab;Ljava/lang/String;)V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->g:Lorg/npci/upi/security/pinactivitycomponent/ac;
:cond_2a
const-string v0, "Common Library"
const-string v2, "get Encryptor"
invoke-static {v0, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
const-string v0, "Common Library"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Input Analyzer :"
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->k:Lorg/npci/upi/security/pinactivitycomponent/an;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v0, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
const-string v2, "Common Library"
new-instance v0, Ljava/lang/StringBuilder;
invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Input Analyzer Key Code:"
invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->k:Lorg/npci/upi/security/pinactivitycomponent/an;
invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
if-nez v0, :cond_87
move-object v0, v1
:goto_65
invoke-static {v2, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
const-string v0, "Common Library"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Input Analyzer Common Library:"
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->k:Lorg/npci/upi/security/pinactivitycomponent/an;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
if-nez v2, :cond_8e
:goto_81
invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->g:Lorg/npci/upi/security/pinactivitycomponent/ac;
return-object v0
:cond_87
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->k:Lorg/npci/upi/security/pinactivitycomponent/an;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/an;->a()Ljava/lang/String;
move-result-object v0
goto :goto_65
:cond_8e
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->k:Lorg/npci/upi/security/pinactivitycomponent/an;
invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/an;->c()Lin/org/npci/commonlibrary/e;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
move-result-object v1
goto :goto_81
.end method
.method public c()Landroid/app/Activity;
.registers 2
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->j:Landroid/app/Activity;
return-object v0
.end method
.method public d()Landroid/os/ResultReceiver;
.registers 2
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->k:Lorg/npci/upi/security/pinactivitycomponent/an;
if-nez v0, :cond_6
const/4 v0, 0x0
:goto_5
return-object v0
:cond_6
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/w;->k:Lorg/npci/upi/security/pinactivitycomponent/an;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/an;->e()Landroid/os/ResultReceiver;
move-result-object v0
goto :goto_5
.end method