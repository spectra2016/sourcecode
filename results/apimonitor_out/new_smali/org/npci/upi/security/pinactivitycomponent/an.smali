.class public Lorg/npci/upi/security/pinactivitycomponent/an;
.super Ljava/lang/Object;
.field private static k:Landroid/os/ResultReceiver;
.field private a:Ljava/lang/String;
.field private b:Ljava/lang/String;
.field private c:Lorg/json/JSONObject;
.field private d:Lorg/json/JSONObject;
.field private e:Lorg/json/JSONObject;
.field private f:Lorg/json/JSONArray;
.field private g:Ljava/util/Locale;
.field private h:Lin/org/npci/commonlibrary/e;
.field private i:Ljava/lang/String;
.field private j:Lorg/npci/upi/security/pinactivitycomponent/ab;
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method private a(Landroid/content/Context;)V
.registers 11
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->e:Lorg/json/JSONObject;
const-string v1, "txnId"
invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->e:Lorg/json/JSONObject;
const-string v2, "txnAmount"
invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->e:Lorg/json/JSONObject;
const-string v3, "appId"
invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v2
iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->e:Lorg/json/JSONObject;
const-string v4, "deviceId"
invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v3
iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->e:Lorg/json/JSONObject;
const-string v5, "mobileNumber"
invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v4
iget-object v5, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->e:Lorg/json/JSONObject;
const-string v6, "payerAddr"
invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v5
iget-object v6, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->e:Lorg/json/JSONObject;
const-string v7, "payeeAddr"
invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v6
:try_start_38
new-instance v7, Ljava/lang/StringBuilder;
const/16 v8, 0x64
invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V
if-eqz v1, :cond_50
invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
move-result v8
if-nez v8, :cond_50
invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v8, "|"
invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
:cond_50
if-eqz v0, :cond_61
invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
move-result v1
if-nez v1, :cond_61
invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, "|"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
:cond_61
if-eqz v5, :cond_72
invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z
move-result v0
if-nez v0, :cond_72
invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, "|"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
:cond_72
if-eqz v6, :cond_83
invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z
move-result v0
if-nez v0, :cond_83
invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, "|"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
:cond_83
if-eqz v2, :cond_94
invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z
move-result v0
if-nez v0, :cond_94
invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, "|"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
:cond_94
if-eqz v4, :cond_a5
invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z
move-result v0
if-nez v0, :cond_a5
invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, "|"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
:cond_a5
if-eqz v3, :cond_b0
invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z
move-result v0
if-nez v0, :cond_b0
invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
:cond_b0
const-string v0, "|"
invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I
move-result v0
const/4 v1, -0x1
if-eq v0, v1, :cond_c4
invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I
move-result v1
add-int/lit8 v1, v1, -0x1
if-ne v0, v1, :cond_c4
invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;
:cond_c4
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->j:Lorg/npci/upi/security/pinactivitycomponent/ab;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/ab;->b()Ljava/lang/String;
move-result-object v0
const-string v1, "CL Trust Token"
invoke-static {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "CL Trust Param Message"
invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->h:Lin/org/npci/commonlibrary/e;
iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->i:Ljava/lang/String;
invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v3
invoke-virtual {v1, v2, v3, v0}, Lin/org/npci/commonlibrary/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
:try_end_e3
.catch Ljava/lang/Exception; {:try_start_38 .. :try_end_e3} :catch_e4
return-void
:catch_e4
move-exception v0
invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v1, "L20"
const-string v2, "l20.message"
invoke-direct {v0, p1, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
throw v0
.end method
.method public static a(Lorg/npci/upi/security/pinactivitycomponent/CLServerResultReceiver;)V
.registers 1
sput-object p0, Lorg/npci/upi/security/pinactivitycomponent/an;->k:Landroid/os/ResultReceiver;
return-void
.end method
.method public a()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->a:Ljava/lang/String;
return-object v0
.end method
.method public a(Landroid/os/Bundle;Landroid/content/Context;)V
.registers 7
new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/ab;
invoke-direct {v0, p2}, Lorg/npci/upi/security/pinactivitycomponent/ab;-><init>(Landroid/content/Context;)V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->j:Lorg/npci/upi/security/pinactivitycomponent/ab;
:try_start_7
const-string v0, "keyCode"
invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->a:Ljava/lang/String;
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->a:Ljava/lang/String;
if-eqz v0, :cond_1b
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->a:Ljava/lang/String;
invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
move-result v0
if-eqz v0, :cond_27
:cond_1b
new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v1, "L06"
const-string v2, "l06.message"
invoke-direct {v0, p2, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
throw v0
:try_end_25
.catch Lorg/npci/upi/security/pinactivitycomponent/d; {:try_start_7 .. :try_end_25} :catch_25
.catch Ljava/lang/Exception; {:try_start_7 .. :try_end_25} :catch_60
:catch_25
move-exception v0
throw v0
:try_start_27
:cond_27
const-string v0, "Common Library"
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->a:Ljava/lang/String;
invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
:try_start_2e
:try_end_2e
.catch Lorg/npci/upi/security/pinactivitycomponent/d; {:try_start_27 .. :try_end_2e} :catch_25
.catch Ljava/lang/Exception; {:try_start_27 .. :try_end_2e} :catch_60
const-string v0, "keyXmlPayload"
invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->b:Ljava/lang/String;
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->b:Ljava/lang/String;
if-eqz v0, :cond_42
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->b:Ljava/lang/String;
invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
move-result v0
if-eqz v0, :cond_6b
:cond_42
new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v1, "L08"
const-string v2, "l08.message"
invoke-direct {v0, p2, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
throw v0
:try_end_4c
.catch Lin/org/npci/commonlibrary/f; {:try_start_2e .. :try_end_4c} :catch_4c
.catch Lorg/npci/upi/security/pinactivitycomponent/d; {:try_start_2e .. :try_end_4c} :catch_ef
.catch Ljava/lang/Exception; {:try_start_2e .. :try_end_4c} :catch_f1
:catch_4c
move-exception v0
const-string v1, "CommonLibraryException"
invoke-virtual {v0}, Lin/org/npci/commonlibrary/f;->getMessage()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/String;)V
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v2, "L05"
const-string v3, "l05.message"
invoke-direct {v1, p2, v2, v3, v0}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
throw v1
:catch_60
move-exception v0
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v2, "L07"
const-string v3, "l07.message"
invoke-direct {v1, p2, v2, v3, v0}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
throw v1
:cond_6b
:try_start_6b
const-string v0, "Common Library"
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->b:Ljava/lang/String;
invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
new-instance v0, Lin/org/npci/commonlibrary/e;
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->b:Ljava/lang/String;
invoke-direct {v0, v1}, Lin/org/npci/commonlibrary/e;-><init>(Ljava/lang/String;)V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->h:Lin/org/npci/commonlibrary/e;
:try_end_7b
.catch Lin/org/npci/commonlibrary/f; {:try_start_6b .. :try_end_7b} :catch_4c
.catch Lorg/npci/upi/security/pinactivitycomponent/d; {:try_start_6b .. :try_end_7b} :catch_ef
.catch Ljava/lang/Exception; {:try_start_6b .. :try_end_7b} :catch_f1
:try_start_7b
const-string v0, "controls"
invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_fc
invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
move-result v1
if-nez v1, :cond_fc
const-string v1, "Common Library"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Controls received: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
new-instance v1, Lorg/json/JSONObject;
invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->c:Lorg/json/JSONObject;
:try_end_a8
.catch Ljava/lang/Exception; {:try_start_7b .. :try_end_a8} :catch_13b
:goto_a8
:try_start_a8
const-string v0, "configuration"
invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_146
invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
move-result v1
if-nez v1, :cond_146
const-string v1, "Common Library"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Configuration received: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
new-instance v1, Lorg/json/JSONObject;
invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->d:Lorg/json/JSONObject;
:try_end_d5
.catch Ljava/lang/Exception; {:try_start_a8 .. :try_end_d5} :catch_14e
:goto_d5
:try_start_d5
const-string v0, "salt"
invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_e3
invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
move-result v1
if-eqz v1, :cond_159
:cond_e3
new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v1, "L12"
const-string v2, "l12.message"
invoke-direct {v0, p2, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
throw v0
:catch_ed
:try_end_ed
.catch Lorg/npci/upi/security/pinactivitycomponent/d; {:try_start_d5 .. :try_end_ed} :catch_ed
.catch Ljava/lang/Exception; {:try_start_d5 .. :try_end_ed} :catch_185
move-exception v0
throw v0
:catch_ef
move-exception v0
throw v0
:catch_f1
move-exception v0
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v2, "L09"
const-string v3, "l09.message"
invoke-direct {v1, p2, v2, v3, v0}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
throw v1
:try_start_fc
:cond_fc
const-string v0, "Common Library"
const-string v1, "Controls is not received. Setting MPIN as default. "
invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
new-instance v0, Lorg/json/JSONObject;
invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V
const-string v1, "type"
const-string v2, "PIN"
invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
const-string v1, "subtype"
const-string v2, "MPIN"
invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
const-string v1, "dType"
const-string v2, "NUM|ALPH"
invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
const-string v1, "dLength"
const/4 v2, 0x6
invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
new-instance v1, Lorg/json/JSONArray;
invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V
invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
new-instance v0, Lorg/json/JSONObject;
invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->c:Lorg/json/JSONObject;
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->c:Lorg/json/JSONObject;
const-string v2, "CredAllowed"
invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
:try_end_139
.catch Ljava/lang/Exception; {:try_start_fc .. :try_end_139} :catch_13b
goto/16 :goto_a8
:catch_13b
move-exception v0
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v2, "L10"
const-string v3, "l10.message"
invoke-direct {v1, p2, v2, v3, v0}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
throw v1
:cond_146
:try_start_146
const-string v0, "Common Library"
const-string v1, "Configuration is not received"
invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
:try_end_14d
.catch Ljava/lang/Exception; {:try_start_146 .. :try_end_14d} :catch_14e
goto :goto_d5
:catch_14e
move-exception v0
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v2, "L11"
const-string v3, "l11.message"
invoke-direct {v1, p2, v2, v3, v0}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
throw v1
:try_start_159
:cond_159
const-string v1, "Common Library"
invoke-static {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
new-instance v1, Lorg/json/JSONObject;
invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->e:Lorg/json/JSONObject;
:try_end_165
.catch Lorg/npci/upi/security/pinactivitycomponent/d; {:try_start_159 .. :try_end_165} :catch_ed
.catch Ljava/lang/Exception; {:try_start_159 .. :try_end_165} :catch_185
:try_start_165
const-string v0, "trust"
invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->i:Ljava/lang/String;
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->i:Ljava/lang/String;
if-eqz v0, :cond_179
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->i:Ljava/lang/String;
invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
move-result v0
if-eqz v0, :cond_190
:cond_179
new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v1, "L17"
const-string v2, "l17.message"
invoke-direct {v0, p2, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
throw v0
:try_end_183
.catch Lorg/npci/upi/security/pinactivitycomponent/d; {:try_start_165 .. :try_end_183} :catch_183
.catch Ljava/lang/Exception; {:try_start_165 .. :try_end_183} :catch_1e3
:catch_183
move-exception v0
throw v0
:catch_185
move-exception v0
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v2, "L13"
const-string v3, "l13.message"
invoke-direct {v1, p2, v2, v3, v0}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
throw v1
:try_start_190
:cond_190
const-string v0, "Common Library"
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->i:Ljava/lang/String;
invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
invoke-direct {p0, p2}, Lorg/npci/upi/security/pinactivitycomponent/an;->a(Landroid/content/Context;)V
:try_end_19a
.catch Lorg/npci/upi/security/pinactivitycomponent/d; {:try_start_190 .. :try_end_19a} :catch_183
.catch Ljava/lang/Exception; {:try_start_190 .. :try_end_19a} :catch_1e3
:try_start_19a
const-string v0, "payInfo"
invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_1ee
invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
move-result v1
if-nez v1, :cond_1ee
const-string v1, "Common Library"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Pay Info Received"
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
new-instance v1, Lorg/json/JSONArray;
invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->f:Lorg/json/JSONArray;
:try_start_1c7
:goto_1c7
:try_end_1c7
.catch Ljava/lang/Exception; {:try_start_19a .. :try_end_1c7} :catch_1f6
const-string v0, "languagePref"
invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
new-instance v2, Ljava/util/Locale;
if-eqz v1, :cond_201
invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
move-result v0
if-nez v0, :cond_201
move-object v0, v1
:goto_1d8
invoke-direct {v2, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V
iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->g:Ljava/util/Locale;
const-string v0, "Common Library"
invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
:try_end_1e2
.catch Ljava/lang/Exception; {:try_start_1c7 .. :try_end_1e2} :catch_204
return-void
:catch_1e3
move-exception v0
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v2, "L09"
const-string v3, "l09.message"
invoke-direct {v1, p2, v2, v3, v0}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
throw v1
:try_start_1ee
:cond_1ee
const-string v0, "Common Library"
const-string v1, "Pay Info not received"
invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
:try_end_1f5
.catch Ljava/lang/Exception; {:try_start_1ee .. :try_end_1f5} :catch_1f6
goto :goto_1c7
:catch_1f6
move-exception v0
new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v1, "L14"
const-string v2, "l14.message"
invoke-direct {v0, p2, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
throw v0
:cond_201
:try_start_201
const-string v0, "en_US"
:try_end_203
.catch Ljava/lang/Exception; {:try_start_201 .. :try_end_203} :catch_204
goto :goto_1d8
:catch_204
move-exception v0
new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/d;
const-string v1, "L15"
const-string v2, "l15.message"
invoke-direct {v0, p2, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
throw v0
.end method
.method public b()Ljava/util/Locale;
.registers 2
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->g:Ljava/util/Locale;
return-object v0
.end method
.method public c()Lin/org/npci/commonlibrary/e;
.registers 2
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->h:Lin/org/npci/commonlibrary/e;
return-object v0
.end method
.method public d()Lorg/npci/upi/security/pinactivitycomponent/ab;
.registers 2
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/an;->j:Lorg/npci/upi/security/pinactivitycomponent/ab;
return-object v0
.end method
.method public e()Landroid/os/ResultReceiver;
.registers 2
sget-object v0, Lorg/npci/upi/security/pinactivitycomponent/an;->k:Landroid/os/ResultReceiver;
return-object v0
.end method