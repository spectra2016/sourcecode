.class public Lorg/npci/upi/security/pinactivitycomponent/t;
.super Ljava/lang/Object;
.field private a:Ljava/lang/String;
.field private b:Lorg/npci/upi/security/pinactivitycomponent/w;
.method constructor <init>(Lorg/npci/upi/security/pinactivitycomponent/w;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/t;->b:Lorg/npci/upi/security/pinactivitycomponent/w;
const-string v0, "MS03LTItNA=="
invoke-direct {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/t;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/t;->a:Ljava/lang/String;
return-void
.end method
.method private a(Ljava/lang/String;)Ljava/lang/String;
.registers 9
const/4 v6, -0x1
const-string v1, ""
new-instance v0, Ljava/lang/String;
invoke-static {p1, v6}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
move-result-object v2
invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V
const-string v2, "-"
invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
move-result-object v2
array-length v3, v2
const/4 v0, 0x0
:goto_14
if-ge v0, v3, :cond_32
aget-object v4, v2, v0
new-instance v5, Ljava/lang/StringBuilder;
invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v5, p0, Lorg/npci/upi/security/pinactivitycomponent/t;->b:Lorg/npci/upi/security/pinactivitycomponent/w;
invoke-virtual {v5, v4}, Lorg/npci/upi/security/pinactivitycomponent/w;->b(Ljava/lang/String;)Ljava/lang/String;
move-result-object v4
invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
add-int/lit8 v0, v0, 0x1
goto :goto_14
:cond_32
new-instance v0, Ljava/lang/String;
invoke-static {v1, v6}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
return-object v0
.end method
.method private b(Lorg/json/JSONObject;)V
.registers 2
return-void
.end method
.method public a(Lorg/json/JSONObject;)Ljava/lang/String;
.registers 7
:try_start_0
invoke-direct {p0, p1}, Lorg/npci/upi/security/pinactivitycomponent/t;->b(Lorg/json/JSONObject;)V
new-instance v0, Ljava/lang/StringBuilder;
invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
const-string v0, "Common Library"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Salt Format: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/t;->a:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/t;->a:Ljava/lang/String;
const-string v1, "Common Library"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Temp Salt Format: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
if-eqz v0, :cond_7a
invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
move-result v1
if-nez v1, :cond_7a
const-string v1, "\\[([^\\]]*)\\]"
invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
move-result-object v1
invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
move-result-object v0
new-instance v1, Ljava/lang/StringBuffer;
const/16 v2, 0x3e8
invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V
:goto_55
invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z
move-result v2
if-eqz v2, :cond_7c
invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;
move-result-object v2
const/4 v3, 0x1
invoke-virtual {v2}, Ljava/lang/String;->length()I
move-result v4
add-int/lit8 v4, v4, -0x1
invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
move-result-object v2
invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v2
invoke-static {v2}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;
move-result-object v2
invoke-virtual {v0, v1, v2}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;
:try_end_75
.catch Lorg/json/JSONException; {:try_start_0 .. :try_end_75} :catch_76
goto :goto_55
:catch_76
move-exception v0
invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
:cond_7a
const/4 v0, 0x0
:goto_7b
return-object v0
:try_start_7c
:cond_7c
invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
const-string v1, "Common Library"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Output Salt: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->b(Ljava/lang/String;Ljava/lang/String;)V
:try_end_9b
.catch Lorg/json/JSONException; {:try_start_7c .. :try_end_9b} :catch_76
goto :goto_7b
.end method