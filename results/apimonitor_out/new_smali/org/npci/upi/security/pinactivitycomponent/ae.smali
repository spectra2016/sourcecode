.class public Lorg/npci/upi/security/pinactivitycomponent/ae;
.super Ljava/lang/Object;
.method private static a(Ljava/io/ByteArrayOutputStream;Ljava/io/InputStream;)Ljava/io/ByteArrayOutputStream;
.registers 5
const/16 v0, 0x1000
new-array v0, v0, [B
:goto_4
invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I
move-result v1
const/4 v2, -0x1
if-eq v1, v2, :cond_10
const/4 v2, 0x0
invoke-virtual {p0, v0, v2, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V
goto :goto_4
:cond_10
invoke-virtual {p0}, Ljava/io/ByteArrayOutputStream;->close()V
invoke-virtual {p1}, Ljava/io/InputStream;->close()V
return-object p0
.end method
.method public static a(Ljava/lang/String;Landroid/content/Context;)[B
.registers 6
:try_start_0
new-instance v0, Ljava/io/ByteArrayOutputStream;
invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V
invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;
move-result-object v1
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "npci/"
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
move-result-object v1
invoke-static {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/ae;->a(Ljava/io/ByteArrayOutputStream;Ljava/io/InputStream;)Ljava/io/ByteArrayOutputStream;
move-result-object v0
invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
:try_end_27
.catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_27} :catch_29
.catch Ljava/io/IOException; {:try_start_0 .. :try_end_27} :catch_30
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_27} :catch_37
move-result-object v0
return-object v0
:catch_29
move-exception v0
new-instance v1, Ljava/lang/RuntimeException;
invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V
throw v1
:catch_30
move-exception v0
new-instance v1, Ljava/lang/RuntimeException;
invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V
throw v1
:catch_37
move-exception v0
new-instance v1, Ljava/lang/RuntimeException;
invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V
throw v1
.end method