.class  Lorg/npci/upi/security/pinactivitycomponent/al;
.super Landroid/animation/AnimatorListenerAdapter;
.field final synthetic a:Z
.field final synthetic b:I
.field final synthetic c:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
.method constructor <init>(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;ZI)V
.registers 4
iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->c:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
iput-boolean p2, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->a:Z
iput p3, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->b:I
invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V
return-void
.end method
.method public onAnimationEnd(Landroid/animation/Animator;)V
.registers 4
const/16 v1, 0x8
invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V
iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->a:Z
if-nez v0, :cond_24
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->c:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->e(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/view/View;
move-result-object v0
invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->c:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->f(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/view/View;
move-result-object v0
invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->c:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->d(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/graphics/drawable/TransitionDrawable;
move-result-object v0
invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V
:cond_24
return-void
.end method
.method public onAnimationStart(Landroid/animation/Animator;)V
.registers 5
const/16 v2, 0x12c
const/4 v1, 0x0
invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V
iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->a:Z
if-eqz v0, :cond_43
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->c:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->d(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/graphics/drawable/TransitionDrawable;
move-result-object v0
invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->c:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->e(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/view/View;
move-result-object v0
invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->c:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->f(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/view/View;
move-result-object v0
invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->c:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->e(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/view/View;
move-result-object v0
invoke-static {v0}, Landroid/support/v4/f/af;->l(Landroid/view/View;)F
move-result v0
const/4 v1, 0x0
cmpl-float v0, v0, v1
if-nez v0, :cond_42
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->c:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->e(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/view/View;
move-result-object v0
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->b:I
mul-int/lit8 v1, v1, -0x1
int-to-float v1, v1
invoke-virtual {v0, v1}, Landroid/view/View;->setY(F)V
:goto_42
:cond_42
return-void
:cond_43
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/al;->c:Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->d(Lorg/npci/upi/security/pinactivitycomponent/GetCredential;)Landroid/graphics/drawable/TransitionDrawable;
move-result-object v0
invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V
goto :goto_42
.end method