.class public Lorg/npci/upi/security/pinactivitycomponent/r;
.super Lorg/npci/upi/security/pinactivitycomponent/h;
.implements Lorg/npci/upi/security/pinactivitycomponent/widget/o;
.field private static final ar:Ljava/lang/String;
.field  ap:Landroid/widget/LinearLayout;
.field  aq:Landroid/widget/LinearLayout;
.field private as:I
.field private at:Ljava/util/Timer;
.field private au:Ljava/lang/Boolean;
.field private av:Ljava/util/HashMap;
.field private aw:Z
.method static constructor <clinit>()V
.registers 1
const-class v0, Lorg/npci/upi/security/pinactivitycomponent/h;
invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v0
sput-object v0, Lorg/npci/upi/security/pinactivitycomponent/r;->ar:Ljava/lang/String;
return-void
.end method
.method public constructor <init>()V
.registers 3
const/4 v0, 0x0
const/4 v1, 0x0
invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;-><init>()V
iput v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->as:I
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->at:Ljava/util/Timer;
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->au:Ljava/lang/Boolean;
new-instance v0, Ljava/util/HashMap;
invoke-direct {v0}, Ljava/util/HashMap;-><init>()V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->av:Ljava/util/HashMap;
iput-boolean v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->aw:Z
return-void
.end method
.method private M()V
.registers 8
const/4 v6, 0x1
const/4 v2, 0x0
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
const/4 v1, -0x1
if-eq v0, v1, :cond_37
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
instance-of v0, v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
if-eqz v0, :cond_37
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputValue()Ljava/lang/String;
move-result-object v1
if-eqz v1, :cond_2d
invoke-virtual {v1}, Ljava/lang/String;->length()I
move-result v1
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputLength()I
move-result v3
if-eq v1, v3, :cond_37
:cond_2d
sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->invalid_otp:I
invoke-virtual {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(I)Ljava/lang/String;
move-result-object v1
invoke-virtual {p0, v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/r;->b(Landroid/view/View;Ljava/lang/String;)V
:goto_36
:cond_36
return-void
:cond_37
move v1, v2
:goto_38
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
if-ge v1, v0, :cond_6e
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
instance-of v0, v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
if-eqz v0, :cond_6a
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputValue()Ljava/lang/String;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/String;->length()I
move-result v3
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getInputLength()I
move-result v4
if-eq v3, v4, :cond_6a
sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->componentMessage:I
invoke-virtual {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(I)Ljava/lang/String;
move-result-object v1
invoke-virtual {p0, v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/r;->b(Landroid/view/View;Ljava/lang/String;)V
goto :goto_36
:cond_6a
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_38
:cond_6e
iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->aw:Z
if-nez v0, :cond_36
iput-boolean v6, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->aw:Z
:goto_74
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
if-ge v2, v0, :cond_e2
:try_start_7c
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/c;
invoke-interface {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/c;->getFormDataTag()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/json/JSONObject;
const-string v1, "type"
invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
const-string v3, "subtype"
invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v3
iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->b:Lorg/json/JSONObject;
const-string v5, "credential"
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/c;
invoke-interface {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/c;->getInputValue()Ljava/lang/String;
move-result-object v0
invoke-virtual {v4, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->ao:Landroid/content/Context;
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o()Lorg/npci/upi/security/pinactivitycomponent/w;
move-result-object v0
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/w;->a()Lorg/npci/upi/security/pinactivitycomponent/t;
move-result-object v0
iget-object v4, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->b:Lorg/json/JSONObject;
invoke-virtual {v0, v4}, Lorg/npci/upi/security/pinactivitycomponent/t;->a(Lorg/json/JSONObject;)Ljava/lang/String;
move-result-object v4
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->ao:Landroid/content/Context;
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o()Lorg/npci/upi/security/pinactivitycomponent/w;
move-result-object v0
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/w;->b()Lorg/npci/upi/security/pinactivitycomponent/ac;
move-result-object v0
iget-object v5, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->b:Lorg/json/JSONObject;
invoke-virtual {v0, v4, v1, v3, v5}, Lorg/npci/upi/security/pinactivitycomponent/ac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Lin/org/npci/commonlibrary/Message;
move-result-object v0
if-eqz v0, :cond_d8
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->av:Ljava/util/HashMap;
invoke-static {v0}, Lorg/npci/upi/security/pinactivitycomponent/ao;->a(Ljava/lang/Object;)Ljava/lang/String;
move-result-object v0
invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:try_end_d8
.catch Ljava/lang/Exception; {:try_start_7c .. :try_end_d8} :catch_db
:goto_d8
:cond_d8
add-int/lit8 v2, v2, 0x1
goto :goto_74
:catch_db
move-exception v0
sget-object v1, Lorg/npci/upi/security/pinactivitycomponent/r;->ar:Ljava/lang/String;
invoke-static {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/Exception;)V
goto :goto_d8
:cond_e2
new-instance v1, Landroid/os/Bundle;
invoke-direct {v1}, Landroid/os/Bundle;-><init>()V
const-string v0, "credBlocks"
iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->av:Ljava/util/HashMap;
invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->ao:Landroid/content/Context;
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o()Lorg/npci/upi/security/pinactivitycomponent/w;
move-result-object v0
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/w;->d()Landroid/os/ResultReceiver;
move-result-object v0
invoke-virtual {v0, v6, v1}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->ao:Landroid/content/Context;
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->o()Lorg/npci/upi/security/pinactivitycomponent/w;
move-result-object v0
invoke-virtual {v0}, Lorg/npci/upi/security/pinactivitycomponent/w;->c()Landroid/app/Activity;
move-result-object v0
invoke-virtual {v0}, Landroid/app/Activity;->finish()V
goto/16 :goto_36
.end method
.method private N()Z
.registers 6
const/4 v1, 0x0
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->au:Ljava/lang/Boolean;
if-eqz v0, :cond_c
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->au:Ljava/lang/Boolean;
invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
move-result v0
:goto_b
return v0
:cond_c
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->c:Lorg/json/JSONArray;
if-eqz v0, :cond_77
new-instance v3, Ljava/util/ArrayList;
invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V
move v0, v1
:goto_16
iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->c:Lorg/json/JSONArray;
invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
move-result v2
if-ge v0, v2, :cond_39
:try_start_1e
iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->c:Lorg/json/JSONArray;
invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
move-result-object v2
const-string v4, "subtype"
invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v2
if-eqz v2, :cond_2f
invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
:try_end_2f
.catch Ljava/lang/Exception; {:try_start_1e .. :try_end_2f} :catch_32
:goto_2f
:cond_2f
add-int/lit8 v0, v0, 0x1
goto :goto_16
:catch_32
move-exception v2
sget-object v4, Lorg/npci/upi/security/pinactivitycomponent/r;->ar:Ljava/lang/String;
invoke-static {v4, v2}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/Exception;)V
goto :goto_2f
:cond_39
const-string v0, "OTP"
invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
move-result v0
if-nez v0, :cond_69
const-string v0, "SMS"
invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
move-result v0
if-nez v0, :cond_69
const-string v0, "EMAIL"
invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
move-result v0
if-nez v0, :cond_69
const-string v0, "HOTP"
invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
move-result v0
if-nez v0, :cond_69
const-string v0, "TOTP"
invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_77
const-string v0, "MPIN"
invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_77
:cond_69
const/4 v0, 0x1
invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->au:Ljava/lang/Boolean;
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->au:Ljava/lang/Boolean;
invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
move-result v0
goto :goto_b
:cond_77
invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v0
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->au:Ljava/lang/Boolean;
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->au:Ljava/lang/Boolean;
invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
move-result v0
goto :goto_b
.end method
.method private O()V
.registers 18
move-object/from16 v0, p0
iget v1, v0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
const/4 v2, -0x1
if-eq v1, v2, :cond_2d
move-object/from16 v0, p0
iget-object v1, v0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
move-object/from16 v0, p0
iget v2, v0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v1
instance-of v1, v1, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
if-eqz v1, :cond_2d
move-object/from16 v0, p0
iget-object v1, v0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
move-object/from16 v0, p0
iget v2, v0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v1
check-cast v1, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
move-object/from16 v0, p0
invoke-virtual {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)V
invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a()V
:cond_2d
const/4 v1, 0x0
move-object/from16 v0, p0
iget-object v2, v0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
move-result v16
move v15, v1
:goto_37
move/from16 v0, v16
if-ge v15, v0, :cond_84
move-object/from16 v0, p0
iget v1, v0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
if-eq v15, v1, :cond_80
move-object/from16 v0, p0
iget-object v1, v0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v3
check-cast v3, Lorg/npci/upi/security/pinactivitycomponent/widget/c;
invoke-virtual/range {p0 .. p0}, Lorg/npci/upi/security/pinactivitycomponent/r;->h()Landroid/support/v4/app/l;
move-result-object v1
sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$c;->ic_visibility_on:I
invoke-static {v1, v2}, Landroid/support/v4/a/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v7
invoke-virtual/range {p0 .. p0}, Lorg/npci/upi/security/pinactivitycomponent/r;->h()Landroid/support/v4/app/l;
move-result-object v1
sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$c;->ic_visibility_off:I
invoke-static {v1, v2}, Landroid/support/v4/a/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v6
sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->action_hide:I
move-object/from16 v0, p0
invoke-virtual {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(I)Ljava/lang/String;
move-result-object v4
sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->action_show:I
move-object/from16 v0, p0
invoke-virtual {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(I)Ljava/lang/String;
move-result-object v5
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/s;
move-object/from16 v2, p0
invoke-direct/range {v1 .. v7}, Lorg/npci/upi/security/pinactivitycomponent/s;-><init>(Lorg/npci/upi/security/pinactivitycomponent/r;Lorg/npci/upi/security/pinactivitycomponent/widget/c;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
const/4 v12, 0x0
const/4 v13, 0x1
const/4 v14, 0x1
move-object v8, v3
move-object v9, v5
move-object v10, v7
move-object v11, v1
invoke-interface/range {v8 .. v14}, Lorg/npci/upi/security/pinactivitycomponent/widget/c;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;IZZ)V
:cond_80
add-int/lit8 v1, v15, 0x1
move v15, v1
goto :goto_37
:cond_84
return-void
.end method
.method private a(Landroid/view/View;)V
.registers 9
sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->main_inner_layout:I
invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/LinearLayout;
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->c:Lorg/json/JSONArray;
if-eqz v1, :cond_147
const/4 v1, 0x0
move v2, v1
:goto_e
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->c:Lorg/json/JSONArray;
invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
move-result v1
if-ge v2, v1, :cond_147
:try_start_16
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->c:Lorg/json/JSONArray;
invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
move-result-object v5
const-string v1, "subtype"
invoke-virtual {v5, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v3
const-string v1, "dLength"
invoke-virtual {v5, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
move-result v1
if-nez v1, :cond_bd
const/4 v1, 0x6
move v4, v1
:goto_2c
const-string v1, "MPIN"
invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_6c
const-string v1, "NMPIN"
invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_6c
const-string v1, "ATMPIN"
invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_6c
const-string v1, "OTP"
invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_6c
const-string v1, "SMS"
invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_6c
const-string v1, "EMAIL"
invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_6c
const-string v1, "HOTP"
invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_6c
const-string v1, "TOTP"
invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_b8
:cond_6c
const-string v1, "NMPIN"
invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_82
const-string v1, "MPIN"
invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_c6
invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/r;->N()Z
move-result v1
if-eqz v1, :cond_c6
:cond_82
sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->npci_set_mpin_title:I
invoke-virtual {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(I)Ljava/lang/String;
move-result-object v1
invoke-virtual {p0, v1, v2, v4}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(Ljava/lang/String;II)Lorg/npci/upi/security/pinactivitycomponent/widget/b;
move-result-object v1
sget v3, Lorg/npci/upi/security/pinactivitycomponent/a$f;->npci_confirm_mpin_title:I
invoke-virtual {p0, v3}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(I)Ljava/lang/String;
move-result-object v3
invoke-virtual {p0, v3, v2, v4}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(Ljava/lang/String;II)Lorg/npci/upi/security/pinactivitycomponent/widget/b;
move-result-object v3
new-instance v4, Ljava/util/ArrayList;
invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V
invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/widget/a;
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/r;->h()Landroid/support/v4/app/l;
move-result-object v3
invoke-direct {v1, v3}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;-><init>(Landroid/content/Context;)V
invoke-virtual {v1, v4, p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->a(Ljava/util/ArrayList;Lorg/npci/upi/security/pinactivitycomponent/widget/o;)V
invoke-virtual {v1, v5}, Lorg/npci/upi/security/pinactivitycomponent/widget/a;->setFormDataTag(Ljava/lang/Object;)V
iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V
:goto_b8
:cond_b8
add-int/lit8 v1, v2, 0x1
move v2, v1
goto/16 :goto_e
:cond_bd
const-string v1, "dLength"
invoke-virtual {v5, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
move-result v1
move v4, v1
goto/16 :goto_2c
:cond_c6
const-string v1, ""
const-string v6, "MPIN"
invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v6
if-eqz v6, :cond_ef
sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->npci_mpin_title:I
invoke-virtual {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(I)Ljava/lang/String;
move-result-object v1
:goto_d6
:cond_d6
invoke-virtual {p0, v1, v2, v4}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(Ljava/lang/String;II)Lorg/npci/upi/security/pinactivitycomponent/widget/b;
move-result-object v1
invoke-virtual {v1, v5}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setFormDataTag(Ljava/lang/Object;)V
iget-object v3, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V
:try_end_e5
.catch Ljava/lang/Exception; {:try_start_16 .. :try_end_e5} :catch_e6
goto :goto_b8
:catch_e6
move-exception v1
sget-object v3, Lorg/npci/upi/security/pinactivitycomponent/r;->ar:Ljava/lang/String;
const-string v4, "Error while inflating Layout"
invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
goto :goto_b8
:try_start_ef
:cond_ef
const-string v6, "OTP"
invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v6
if-nez v6, :cond_117
const-string v6, "SMS"
invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v6
if-nez v6, :cond_117
const-string v6, "EMAIL"
invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v6
if-nez v6, :cond_117
const-string v6, "HOTP"
invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v6
if-nez v6, :cond_117
const-string v6, "TOTP"
invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v6
if-eqz v6, :cond_138
:cond_117
sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->npci_otp_title:I
invoke-virtual {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(I)Ljava/lang/String;
move-result-object v3
iput v2, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/r;->h()Landroid/support/v4/app/l;
move-result-object v1
instance-of v1, v1, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
if-eqz v1, :cond_148
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/r;->h()Landroid/support/v4/app/l;
move-result-object v1
check-cast v1, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->n()Z
move-result v1
if-eqz v1, :cond_148
invoke-virtual {p0, v4}, Lorg/npci/upi/security/pinactivitycomponent/r;->c(I)V
move-object v1, v3
goto :goto_d6
:cond_138
const-string v6, "ATMPIN"
invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v3
if-eqz v3, :cond_d6
sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$f;->npci_atm_title:I
invoke-virtual {p0, v1}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(I)Ljava/lang/String;
:try_end_145
.catch Ljava/lang/Exception; {:try_start_ef .. :try_end_145} :catch_e6
move-result-object v1
goto :goto_d6
:cond_147
return-void
:cond_148
move-object v1, v3
goto :goto_d6
.end method
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.registers 6
sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$e;->fragment_pin:I
const/4 v1, 0x0
invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
move-result-object v0
return-object v0
.end method
.method public a()V
.registers 3
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->as:I
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
move-result v1
add-int/lit8 v1, v1, -0x1
if-ge v0, v1, :cond_34
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->as:I
add-int/lit8 v1, v1, 0x1
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/c;
invoke-interface {v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/c;->d()Z
move-result v0
if-eqz v0, :cond_33
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->as:I
add-int/lit8 v0, v0, 0x1
iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->as:I
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->as:I
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
move-result v1
add-int/lit8 v1, v1, -0x1
if-lt v0, v1, :cond_33
invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/r;->M()V
:goto_33
:cond_33
return-void
:cond_34
invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/r;->M()V
goto :goto_33
.end method
.method public a(ILjava/lang/String;)V
.registers 7
const/4 v3, 0x0
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
const/4 v1, -0x1
if-eq v0, v1, :cond_50
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
if-ne v0, p1, :cond_50
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
instance-of v0, v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
if-eqz v0, :cond_50
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->at:Ljava/util/Timer;
invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(Ljava/util/Timer;)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {v0, v3}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Z)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
const-string v1, ""
const/4 v2, 0x0
invoke-virtual {v0, v1, v2, v3, v3}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;ZZ)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->g:I
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/r;->h()Landroid/support/v4/app/l;
move-result-object v1
sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$c;->ic_tick_ok:I
invoke-static {v1, v2}, Landroid/support/v4/a/a;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
move-result-object v1
const/4 v2, 0x1
invoke-virtual {v0, v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Landroid/graphics/drawable/Drawable;Z)V
:cond_50
return-void
.end method
.method public a(Landroid/view/View;Landroid/os/Bundle;)V
.registers 4
invoke-super {p0, p1, p2}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(Landroid/view/View;Landroid/os/Bundle;)V
sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->main_inner_layout:I
invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/LinearLayout;
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->aq:Landroid/widget/LinearLayout;
sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->main_layout:I
invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/LinearLayout;
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->ap:Landroid/widget/LinearLayout;
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/r;->K()V
invoke-direct {p0, p1}, Lorg/npci/upi/security/pinactivitycomponent/r;->a(Landroid/view/View;)V
invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/r;->O()V
return-void
.end method
.method public a(Landroid/view/View;Ljava/lang/String;)V
.registers 3
invoke-virtual {p0, p1, p2}, Lorg/npci/upi/security/pinactivitycomponent/r;->b(Landroid/view/View;Ljava/lang/String;)V
return-void
.end method
.method public b(I)V
.registers 3
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->f:Ljava/util/ArrayList;
invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
instance-of v0, v0, Lorg/npci/upi/security/pinactivitycomponent/widget/a;
if-nez v0, :cond_c
iput p1, p0, Lorg/npci/upi/security/pinactivitycomponent/r;->as:I
:cond_c
return-void
.end method