.class public abstract Lorg/npci/upi/security/pinactivitycomponent/h;
.super Landroid/support/v4/app/Fragment;
.implements Lorg/npci/upi/security/pinactivitycomponent/widget/o;
.field protected a:Lorg/json/JSONObject;
.field protected ai:Ljava/util/Timer;
.field protected aj:Landroid/os/Handler;
.field protected ak:Ljava/lang/Runnable;
.field protected al:Lorg/json/JSONObject;
.field protected am:Lorg/json/JSONArray;
.field protected an:J
.field protected ao:Landroid/content/Context;
.field private ap:Z
.field protected b:Lorg/json/JSONObject;
.field protected c:Lorg/json/JSONArray;
.field protected d:Ljava/util/Timer;
.field protected e:J
.field protected f:Ljava/util/ArrayList;
.field protected g:I
.field protected h:Landroid/widget/PopupWindow;
.field protected i:Ljava/util/Timer;
.method public constructor <init>()V
.registers 4
const/4 v2, 0x0
invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V
iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->a:Lorg/json/JSONObject;
iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->b:Lorg/json/JSONObject;
iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->c:Lorg/json/JSONArray;
iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->d:Ljava/util/Timer;
const-wide/32 v0, 0xafc8
iput-wide v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->e:J
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->f:Ljava/util/ArrayList;
const/4 v0, -0x1
iput v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->g:I
iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->i:Ljava/util/Timer;
iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->al:Lorg/json/JSONObject;
new-instance v0, Lorg/json/JSONArray;
invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->am:Lorg/json/JSONArray;
const-wide/16 v0, 0xbb8
iput-wide v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->an:J
const/4 v0, 0x0
iput-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->ap:Z
return-void
.end method
.method private M()V
.registers 12
const/4 v9, 0x2
const/4 v1, 0x0
new-instance v6, Ljava/util/ArrayList;
invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V
const/4 v0, 0x0
move v5, v0
move-object v2, v1
move-object v3, v1
move-object v4, v1
:goto_c
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->c:Lorg/json/JSONArray;
invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
move-result v0
if-ge v5, v0, :cond_76
:try_start_14
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->c:Lorg/json/JSONArray;
invoke-virtual {v0, v5}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/json/JSONObject;
const-string v7, "subtype"
const-string v8, ""
invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
const-string v7, "ATMPIN"
invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v7
if-eqz v7, :cond_32
iget-object v7, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->c:Lorg/json/JSONArray;
invoke-virtual {v7, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
move-result-object v4
:cond_32
const-string v7, "OTP|SMS|HOTP|TOTP"
invoke-virtual {v0, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z
move-result v7
if-eqz v7, :cond_40
iget-object v7, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->c:Lorg/json/JSONArray;
invoke-virtual {v7, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
move-result-object v3
:cond_40
const-string v7, "MPIN"
invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v7
if-eqz v7, :cond_4e
iget-object v7, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->c:Lorg/json/JSONArray;
invoke-virtual {v7, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
move-result-object v2
:cond_4e
const-string v7, "NMPIN"
invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_c0
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->c:Lorg/json/JSONArray;
invoke-virtual {v0, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
:try_end_5b
.catch Ljava/lang/Exception; {:try_start_14 .. :try_end_5b} :catch_67
move-result-object v0
:goto_5c
move-object v1, v2
move-object v2, v3
move-object v3, v4
:goto_5f
add-int/lit8 v4, v5, 0x1
move v5, v4
move-object v4, v3
move-object v3, v2
move-object v2, v1
move-object v1, v0
goto :goto_c
:catch_67
move-exception v0
move-object v10, v0
move-object v0, v2
move-object v2, v3
move-object v3, v4
move-object v4, v10
const-string v7, "NPCIFragment"
invoke-static {v7, v4}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/Exception;)V
move-object v10, v1
move-object v1, v0
move-object v0, v10
goto :goto_5f
:cond_76
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->c:Lorg/json/JSONArray;
invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
move-result v0
const/4 v5, 0x3
if-ne v0, v5, :cond_8e
if-eqz v4, :cond_8e
if-eqz v3, :cond_8e
if-eqz v2, :cond_8e
invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
:cond_8e
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->c:Lorg/json/JSONArray;
invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
move-result v0
if-ne v0, v9, :cond_a0
if-eqz v3, :cond_a0
if-eqz v2, :cond_a0
invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
:cond_a0
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->c:Lorg/json/JSONArray;
invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
move-result v0
if-ne v0, v9, :cond_b2
if-eqz v2, :cond_b2
if-eqz v1, :cond_b2
invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
:cond_b2
invoke-virtual {v6}, Ljava/util/ArrayList;->size()I
move-result v0
if-lez v0, :cond_bf
new-instance v0, Lorg/json/JSONArray;
invoke-direct {v0, v6}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->c:Lorg/json/JSONArray;
:cond_bf
return-void
:cond_c0
move-object v0, v1
goto :goto_5c
.end method
.method protected K()V
.registers 4
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;->g()Landroid/os/Bundle;
move-result-object v0
if-eqz v0, :cond_56
:try_start_6
const-string v1, "configuration"
invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
if-eqz v1, :cond_15
new-instance v2, Lorg/json/JSONObject;
invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->a:Lorg/json/JSONObject;
:cond_15
const-string v1, "controls"
invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
if-eqz v1, :cond_38
new-instance v2, Lorg/json/JSONObject;
invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->al:Lorg/json/JSONObject;
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->al:Lorg/json/JSONObject;
const-string v2, "CredAllowed"
invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
if-eqz v1, :cond_38
new-instance v2, Lorg/json/JSONArray;
invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->c:Lorg/json/JSONArray;
invoke-direct {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;->M()V
:cond_38
const-string v1, "salt"
invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
if-eqz v1, :cond_47
new-instance v2, Lorg/json/JSONObject;
invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
iput-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->b:Lorg/json/JSONObject;
:cond_47
const-string v1, "payInfo"
invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_56
new-instance v1, Lorg/json/JSONArray;
invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
iput-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->am:Lorg/json/JSONArray;
:goto_56
:try_end_56
.catch Ljava/lang/Exception; {:try_start_6 .. :try_end_56} :catch_57
:cond_56
return-void
:catch_57
move-exception v0
const-string v1, "NPCIFragment"
const-string v2, "Error while reading Arguments"
invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
goto :goto_56
.end method
.method protected L()V
.registers 4
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->a:Lorg/json/JSONObject;
const-string v1, "resendOTPFeature"
const-string v2, "false"
invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
const-string v1, "false"
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-nez v0, :cond_16
iget-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->ap:Z
if-eqz v0, :cond_17
:cond_16
:goto_16
return-void
:cond_17
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;->h()Landroid/support/v4/app/l;
move-result-object v0
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/m;
invoke-direct {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/m;-><init>(Lorg/npci/upi/security/pinactivitycomponent/h;)V
invoke-virtual {v0, v1}, Landroid/support/v4/app/l;->runOnUiThread(Ljava/lang/Runnable;)V
goto :goto_16
.end method
.method  a(F)I
.registers 3
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;->i()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
move-result-object v0
iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I
div-int/lit16 v0, v0, 0xa0
int-to-float v0, v0
mul-float/2addr v0, p1
float-to-int v0, v0
return v0
.end method
.method protected a(Ljava/lang/String;II)Lorg/npci/upi/security/pinactivitycomponent/widget/b;
.registers 11
const/high16 v4, 0x4200
const/4 v6, 0x1
const/4 v5, 0x0
new-instance v0, Landroid/widget/LinearLayout$LayoutParams;
const/4 v1, -0x1
const/4 v2, -0x2
invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/widget/b;
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;->h()Landroid/support/v4/app/l;
move-result-object v2
invoke-direct {v1, v2}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;-><init>(Landroid/content/Context;)V
iget-object v2, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->c:Lorg/json/JSONArray;
invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
move-result v2
if-ne v2, v6, :cond_9a
invoke-virtual {v1, v6}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setActionBarPositionTop(Z)V
const/high16 v2, 0x4370
invoke-virtual {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(F)I
move-result v2
iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I
const/high16 v2, 0x4220
invoke-virtual {p0, v2}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(F)I
move-result v2
iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I
invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getFormInputView()Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;
move-result-object v2
const/4 v3, 0x0
invoke-virtual {v2, v3}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setCharSize(F)V
invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getFormInputView()Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;
move-result-object v2
const/high16 v3, 0x4170
invoke-virtual {p0, v3}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(F)I
move-result v3
int-to-float v3, v3
invoke-virtual {v2, v3}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setSpace(F)V
invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getFormInputView()Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;
move-result-object v2
invoke-virtual {p0, v4}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(F)I
move-result v3
int-to-float v3, v3
invoke-virtual {v2, v3}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setFontSize(F)V
invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getFormInputView()Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;
move-result-object v2
invoke-virtual {p0, v4}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(F)I
move-result v3
invoke-virtual {v2, v5, v3, v5, v5}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setPadding(IIII)V
invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getFormInputView()Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;
move-result-object v2
const/4 v3, 0x4
new-array v3, v3, [I
aput v5, v3, v5
invoke-virtual {p0, v4}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(F)I
move-result v4
aput v4, v3, v6
const/4 v4, 0x2
aput v5, v3, v4
const/4 v4, 0x3
aput v5, v3, v4
invoke-virtual {v2, v3}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setMargin([I)V
invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getFormInputView()Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;
move-result-object v2
invoke-virtual {v2, v6}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setLineStrokeCentered(Z)V
invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getFormInputView()Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;
move-result-object v2
const/high16 v3, 0x4000
invoke-virtual {p0, v3}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(F)I
move-result v3
int-to-float v3, v3
invoke-virtual {v2, v3}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setLineStrokeSelected(F)V
invoke-virtual {v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->getFormInputView()Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;
move-result-object v2
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;->h()Landroid/support/v4/app/l;
move-result-object v3
sget v4, Lorg/npci/upi/security/pinactivitycomponent/a$b;->form_item_input_colors_transparent:I
invoke-static {v3, v4}, Landroid/support/v4/a/a;->b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
move-result-object v3
invoke-virtual {v2, v3}, Lorg/npci/upi/security/pinactivitycomponent/widget/FormItemEditText;->setColorStates(Landroid/content/res/ColorStateList;)V
:cond_9a
invoke-virtual {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
invoke-virtual {v1, p3}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setInputLength(I)V
invoke-virtual {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setFormItemListener(Lorg/npci/upi/security/pinactivitycomponent/widget/o;)V
invoke-virtual {v1, p1}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setTitle(Ljava/lang/String;)V
invoke-virtual {v1, p2}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->setFormItemTag(I)V
return-object v1
.end method
.method public abstract a()V
.end method
.method public a(Landroid/content/Context;)V
.registers 2
invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Context;)V
iput-object p1, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->ao:Landroid/content/Context;
return-void
.end method
.method public a(Landroid/view/View;Landroid/os/Bundle;)V
.registers 4
invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/View;Landroid/os/Bundle;)V
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;->h()Landroid/support/v4/app/l;
move-result-object v0
instance-of v0, v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
if-eqz v0, :cond_14
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;->h()Landroid/support/v4/app/l;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-virtual {v0, p0}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->a(Lorg/npci/upi/security/pinactivitycomponent/h;)V
:cond_14
return-void
.end method
.method protected a(Ljava/util/Timer;)V
.registers 4
if-eqz p1, :cond_5
:try_start_2
invoke-virtual {p1}, Ljava/util/Timer;->cancel()V
:try_end_5
.catch Ljava/lang/Exception; {:try_start_2 .. :try_end_5} :catch_6
:cond_5
:goto_5
return-void
:catch_6
move-exception v0
const-string v1, "NPCIFragment"
invoke-static {v1, v0}, Lorg/npci/upi/security/pinactivitycomponent/g;->a(Ljava/lang/String;Ljava/lang/Exception;)V
goto :goto_5
.end method
.method protected a(Lorg/npci/upi/security/pinactivitycomponent/p;)V
.registers 4
:try_start_0
iget v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->g:I
const/4 v1, -0x1
if-eq v0, v1, :cond_19
const/4 v0, 0x1
iput-boolean v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->ap:Z
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->f:Ljava/util/ArrayList;
iget v1, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->g:I
invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/widget/c;
invoke-virtual {p1}, Lorg/npci/upi/security/pinactivitycomponent/p;->b()Ljava/lang/String;
move-result-object v1
invoke-interface {v0, v1}, Lorg/npci/upi/security/pinactivitycomponent/widget/c;->setText(Ljava/lang/String;)V
:try_end_19
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_19} :catch_1a
:goto_19
:cond_19
return-void
:catch_1a
move-exception v0
goto :goto_19
.end method
.method protected a(Lorg/npci/upi/security/pinactivitycomponent/widget/b;)V
.registers 11
const/4 v8, 0x1
const/4 v2, 0x0
const/4 v4, 0x0
new-instance v0, Ljava/util/Timer;
invoke-direct {v0}, Ljava/util/Timer;-><init>()V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->i:Ljava/util/Timer;
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->i:Ljava/util/Timer;
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/o;
invoke-direct {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/o;-><init>(Lorg/npci/upi/security/pinactivitycomponent/h;)V
iget-wide v6, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->e:J
invoke-virtual {v0, v1, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
const-string v1, ""
move-object v0, p1
move-object v3, v2
move v5, v4
move v6, v4
invoke-virtual/range {v0 .. v6}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;IZZ)V
invoke-virtual {p1, v2, v4}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Landroid/graphics/drawable/Drawable;Z)V
sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$f;->detecting_otp:I
invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p1, v0, v2, v8, v4}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;ZZ)V
invoke-virtual {p1, v8}, Lorg/npci/upi/security/pinactivitycomponent/widget/b;->a(Z)V
return-void
.end method
.method protected b(Landroid/view/View;Ljava/lang/String;)V
.registers 8
const/4 v4, 0x0
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->h:Landroid/widget/PopupWindow;
if-eqz v0, :cond_a
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->h:Landroid/widget/PopupWindow;
invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V
:cond_a
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;->h()Landroid/support/v4/app/l;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v4/app/l;->getLayoutInflater()Landroid/view/LayoutInflater;
move-result-object v0
sget v1, Lorg/npci/upi/security/pinactivitycomponent/a$e;->layout_popup_view:I
const/4 v2, 0x0
invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
move-result-object v1
sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->popup_text:I
invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/TextView;
invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
new-instance v0, Landroid/widget/PopupWindow;
const/4 v2, -0x2
const/high16 v3, 0x4270
invoke-virtual {p0, v3}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(F)I
move-result v3
invoke-direct {v0, v1, v2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->h:Landroid/widget/PopupWindow;
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->h:Landroid/widget/PopupWindow;
sget v2, Lorg/npci/upi/security/pinactivitycomponent/a$g;->PopupAnimation:I
invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->h:Landroid/widget/PopupWindow;
const/16 v2, 0x11
invoke-virtual {v0, p1, v2, v4, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V
sget v0, Lorg/npci/upi/security/pinactivitycomponent/a$d;->popup_button:I
invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;
move-result-object v0
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/k;
invoke-direct {v1, p0}, Lorg/npci/upi/security/pinactivitycomponent/k;-><init>(Lorg/npci/upi/security/pinactivitycomponent/h;)V
invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
new-instance v0, Ljava/util/Timer;
invoke-direct {v0}, Ljava/util/Timer;-><init>()V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->ai:Ljava/util/Timer;
new-instance v0, Landroid/os/Handler;
invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;
move-result-object v1
invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->aj:Landroid/os/Handler;
new-instance v0, Lorg/npci/upi/security/pinactivitycomponent/l;
invoke-direct {v0, p0}, Lorg/npci/upi/security/pinactivitycomponent/l;-><init>(Lorg/npci/upi/security/pinactivitycomponent/h;)V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->ak:Ljava/lang/Runnable;
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->aj:Landroid/os/Handler;
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->ak:Ljava/lang/Runnable;
iget-wide v2, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->an:J
invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
return-void
.end method
.method public b(Lorg/npci/upi/security/pinactivitycomponent/p;)V
.registers 2
invoke-virtual {p0, p1}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(Lorg/npci/upi/security/pinactivitycomponent/p;)V
return-void
.end method
.method public c(I)V
.registers 8
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;->h()Landroid/support/v4/app/l;
move-result-object v0
if-eqz v0, :cond_17
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;->h()Landroid/support/v4/app/l;
move-result-object v0
instance-of v0, v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
if-eqz v0, :cond_17
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;->h()Landroid/support/v4/app/l;
move-result-object v0
check-cast v0, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;
invoke-virtual {v0, p1}, Lorg/npci/upi/security/pinactivitycomponent/GetCredential;->b(I)V
:cond_17
new-instance v2, Lorg/npci/upi/security/pinactivitycomponent/q;
invoke-virtual {p0}, Lorg/npci/upi/security/pinactivitycomponent/h;->h()Landroid/support/v4/app/l;
move-result-object v0
invoke-direct {v2, v0}, Lorg/npci/upi/security/pinactivitycomponent/q;-><init>(Landroid/content/Context;)V
new-instance v0, Ljava/util/Timer;
invoke-direct {v0}, Ljava/util/Timer;-><init>()V
iput-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->d:Ljava/util/Timer;
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->d:Ljava/util/Timer;
new-instance v1, Lorg/npci/upi/security/pinactivitycomponent/i;
invoke-direct {v1, p0, v2, p1}, Lorg/npci/upi/security/pinactivitycomponent/i;-><init>(Lorg/npci/upi/security/pinactivitycomponent/h;Lorg/npci/upi/security/pinactivitycomponent/q;I)V
const-wide/16 v2, 0x64
const-wide/16 v4, 0x7d0
invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V
return-void
.end method
.method public q()V
.registers 3
invoke-super {p0}, Landroid/support/v4/app/Fragment;->q()V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->d:Ljava/util/Timer;
invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(Ljava/util/Timer;)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->i:Ljava/util/Timer;
invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(Ljava/util/Timer;)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->ai:Ljava/util/Timer;
invoke-virtual {p0, v0}, Lorg/npci/upi/security/pinactivitycomponent/h;->a(Ljava/util/Timer;)V
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->aj:Landroid/os/Handler;
if-eqz v0, :cond_21
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->ak:Ljava/lang/Runnable;
if-eqz v0, :cond_21
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->aj:Landroid/os/Handler;
iget-object v1, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->ak:Ljava/lang/Runnable;
invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
:cond_21
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->h:Landroid/widget/PopupWindow;
if-eqz v0, :cond_2a
iget-object v0, p0, Lorg/npci/upi/security/pinactivitycomponent/h;->h:Landroid/widget/PopupWindow;
invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V
:cond_2a
return-void
.end method