.class public Lorg/npci/upi/security/pinactivitycomponent/CLConstants;
.super Ljava/lang/Object;
.field public static final BTN_SUBMIT:Ljava/lang/String; = "Submit"
.field public static final CL_MESSAGES:Ljava/lang/String; = "cl-messages"
.field public static final CL_PROPERTIES:Ljava/lang/String; = "cl-app.properties"
.field public static final CL_VERSION:Ljava/lang/String; = "2.0"
.field public static final CONFIGURATION_RESEND_OTP_FEATURE:Ljava/lang/String; = "resendOTPFeature"
.field public static final CREDTYPE_ATMPIN:Ljava/lang/String; = "ATMPIN"
.field public static final CREDTYPE_EMAIL:Ljava/lang/String; = "EMAIL"
.field public static final CREDTYPE_HOTP:Ljava/lang/String; = "HOTP"
.field public static final CREDTYPE_MPIN:Ljava/lang/String; = "MPIN"
.field public static final CREDTYPE_NMPIN:Ljava/lang/String; = "NMPIN"
.field public static final CREDTYPE_OTP:Ljava/lang/String; = "OTP"
.field public static final CREDTYPE_SMS:Ljava/lang/String; = "SMS"
.field public static final CREDTYPE_TOTP:Ljava/lang/String; = "TOTP"
.field public static final DEFAULT_LANGUAGE_PREFERENCE:Ljava/lang/String; = "en_US"
.field public static final ERROR_CONFIG_PARSE:Ljava/lang/String; = "L11"
.field public static final ERROR_CONTROLS_PARSE:Ljava/lang/String; = "L10"
.field public static final ERROR_GENERIC:Ljava/lang/String; = "L16"
.field public static final ERROR_KEY_CODE_MISSING:Ljava/lang/String; = "L06"
.field public static final ERROR_KEY_CODE_PARSE:Ljava/lang/String; = "L07"
.field public static final ERROR_KEY_MANDATORY_SALT_VALUE_MISSING:Ljava/lang/String; = "L18"
.field public static final ERROR_KEY_MANDATORY_SALT_VALUE_PARSE:Ljava/lang/String; = "L19"
.field public static final ERROR_KEY_TRUST_MISSING:Ljava/lang/String; = "L17"
.field public static final ERROR_KEY_TRUST_NOT_VALID:Ljava/lang/String; = "L20"
.field public static final ERROR_KEY_XML_PAYLOAD_MISSING:Ljava/lang/String; = "L08"
.field public static final ERROR_LOCALE_PARSE:Ljava/lang/String; = "L15"
.field public static final ERROR_MSG_CONFIG_PARSE:Ljava/lang/String; = "l11.message"
.field public static final ERROR_MSG_CONTROLS_PARSE:Ljava/lang/String; = "l10.message"
.field public static final ERROR_MSG_GENERIC:Ljava/lang/String; = "l16.message"
.field public static final ERROR_MSG_KEY_CODE_MISSING:Ljava/lang/String; = "l06.message"
.field public static final ERROR_MSG_KEY_CODE_PARSE:Ljava/lang/String; = "l07.message"
.field public static final ERROR_MSG_KEY_MANDATORY_SALT_VALUE_MISSING:Ljava/lang/String; = "l18.message"
.field public static final ERROR_MSG_KEY_MANDATORY_SALT_VALUE_PARSE:Ljava/lang/String; = "l19.message"
.field public static final ERROR_MSG_KEY_TRUST_MISSING:Ljava/lang/String; = "l17.message"
.field public static final ERROR_MSG_KEY_TRUST_NOT_VALID:Ljava/lang/String; = "l20.message"
.field public static final ERROR_MSG_KEY_XML_PAYLOAD_MISSING:Ljava/lang/String; = "l08.message"
.field public static final ERROR_MSG_LOCALE_PARSE:Ljava/lang/String; = "l15.message"
.field public static final ERROR_MSG_PAY_INFO_PARSE:Ljava/lang/String; = "l14.message"
.field public static final ERROR_MSG_RESULT_RECEIVER_PARSE:Ljava/lang/String; = "l21.message"
.field public static final ERROR_MSG_SALT_MISSING:Ljava/lang/String; = "l12.message"
.field public static final ERROR_MSG_SALT_PARSE:Ljava/lang/String; = "l13.message"
.field public static final ERROR_MSG_XMLPAYLOAD_VALIDATE:Ljava/lang/String; = "l05.message"
.field public static final ERROR_MSG_XML_PAYLOAD_PARSE:Ljava/lang/String; = "l09.message"
.field public static final ERROR_PAY_INFO_PARSE:Ljava/lang/String; = "L14"
.field public static final ERROR_RESULT_RECEIVER_PARSE:Ljava/lang/String; = "L21"
.field public static final ERROR_SALT_MISSING:Ljava/lang/String; = "L12"
.field public static final ERROR_SALT_PARSE:Ljava/lang/String; = "L13"
.field public static final ERROR_XMLPAYLOAD_VALIDATE:Ljava/lang/String; = "L05"
.field public static final ERROR_XML_PAYLOAD_PARSE:Ljava/lang/String; = "L09"
.field public static final FIELD_BG_COLOR:Ljava/lang/String; = "backgroundColor"
.field public static final FIELD_CODE:Ljava/lang/String; = "code"
.field public static final FIELD_CRED_ALLOWED:Ljava/lang/String; = "CredAllowed"
.field public static final FIELD_DATA:Ljava/lang/String; = "data"
.field public static final FIELD_DLENGTH:Ljava/lang/String; = "dLength"
.field public static final FIELD_DTYPE:Ljava/lang/String; = "dType"
.field public static final FIELD_ERROR_CODE:Ljava/lang/String; = "errorCode"
.field public static final FIELD_ERROR_TEXT:Ljava/lang/String; = "errorText"
.field public static final FIELD_FONT_COLOR:Ljava/lang/String; = "color"
.field public static final FIELD_HMAC:Ljava/lang/String; = "Hmac"
.field public static final FIELD_KI:Ljava/lang/String; = "ki"
.field public static final FIELD_PAYER_BANK_NAME:Ljava/lang/String; = "payerBankName"
.field public static final FIELD_PAY_INFO_NAME:Ljava/lang/String; = "name"
.field public static final FIELD_PAY_INFO_VALUE:Ljava/lang/String; = "value"
.field public static final FIELD_PID:Ljava/lang/String; = "PID"
.field public static final FIELD_SKEY:Ljava/lang/String; = "Skey"
.field public static final FIELD_SUBTYPE:Ljava/lang/String; = "subtype"
.field public static final FIELD_TXN_AMOUNT:Ljava/lang/String; = "txnAmount"
.field public static final FIELD_TXN_CCY:Ljava/lang/String; = "txnCurr"
.field public static final FIELD_TXN_ID:Ljava/lang/String; = "txnID"
.field public static final FIELD_TYPE:Ljava/lang/String; = "type"
.field public static final INPUT_KEY_CONFIGURATION:Ljava/lang/String; = "configuration"
.field public static final INPUT_KEY_CONTROLS:Ljava/lang/String; = "controls"
.field public static final INPUT_KEY_KEY_CODE:Ljava/lang/String; = "keyCode"
.field public static final INPUT_KEY_LANGUAGE_PREFERENCE:Ljava/lang/String; = "languagePref"
.field public static final INPUT_KEY_PAY_INFO:Ljava/lang/String; = "payInfo"
.field public static final INPUT_KEY_RESULT_RECEIVER:Ljava/lang/String; = "resultReceiver"
.field public static final INPUT_KEY_SALT:Ljava/lang/String; = "salt"
.field public static final INPUT_KEY_TRUST:Ljava/lang/String; = "trust"
.field public static final INPUT_KEY_XML_PAYLOAD:Ljava/lang/String; = "keyXmlPayload"
.field public static final LABEL_ACCOUNT:Ljava/lang/String; = "payinfo.account.label"
.field public static final LABEL_MOBILE_NUMBER:Ljava/lang/String; = "payinfo.mobilenumber.label"
.field static final LABEL_MPIN:Ljava/lang/String; = "Enter your MPIN"
.field static final LABEL_NMPIN:Ljava/lang/String; = "Enter your new MPIN"
.field public static final LABEL_NOTE:Ljava/lang/String; = "payinfo.note.label"
.field static final LABEL_OTP:Ljava/lang/String; = "Enter your OTP"
.field public static final LABEL_PAYEE_NAME:Ljava/lang/String; = "payinfo.payeename.label"
.field public static final LABEL_REF_ID:Ljava/lang/String; = "payinfo.refid.label"
.field public static final LABEL_REF_URL:Ljava/lang/String; = "payinfo.refurl.label"
.field public static final LABEL_TXN_AMOUNT:Ljava/lang/String; = "payinfo.txnamount.label"
.field public static final MGS_ID_PREFERENCES:Ljava/lang/String; = "msgID"
.field public static final MODE_INITIAL:Ljava/lang/String; = "initial"
.field public static final MODE_ROTATE:Ljava/lang/String; = "rotate"
.field public static final OTP:Ljava/lang/String; = "otp"
.field public static final OUTPUT_KEY_ACTION:Ljava/lang/String; = "action"
.field public static final OUTPUT_KEY_CRED:Ljava/lang/String; = "cred"
.field public static final OUTPUT_KEY_ERROR:Ljava/lang/String; = "error"
.field public static final SALT_DELIMETER:Ljava/lang/String; = "|"
.field public static final SALT_FIELD_APP_ID:Ljava/lang/String; = "appId"
.field public static final SALT_FIELD_CREDENTIAL:Ljava/lang/String; = "credential"
.field public static final SALT_FIELD_DEVICE_ID:Ljava/lang/String; = "deviceId"
.field public static final SALT_FIELD_MOBILE_NUMBER:Ljava/lang/String; = "mobileNumber"
.field public static final SALT_FIELD_PAYEE_ADDR:Ljava/lang/String; = "payeeAddr"
.field public static final SALT_FIELD_PAYER_ADDR:Ljava/lang/String; = "payerAddr"
.field public static final SALT_FIELD_TXN_AMOUNT:Ljava/lang/String; = "txnAmount"
.field public static final SALT_FIELD_TXN_ID:Ljava/lang/String; = "txnId"
.field static final VALIDATION_ALPHA:Ljava/lang/String; = "ALPH"
.field static final VALIDATION_ALPHANUM_ERROR:Ljava/lang/String; = "validation.alphanum.error.message"
.field static final VALIDATION_ALPHANUM_REGEX:Ljava/lang/String; = "validation.alphanum.regex"
.field static final VALIDATION_ALPHA_ERROR:Ljava/lang/String; = "validation.alpha.error.message"
.field static final VALIDATION_ALPHA_REGEX:Ljava/lang/String; = "validation.alpha.regex"
.field public static final VALIDATION_ERR_MSG:Ljava/lang/String; = "validation.error.displaymessage"
.field static final VALIDATION_NUM_ERROR:Ljava/lang/String; = "validation.num.error.message"
.field static final VALIDATION_NUM_REGEX:Ljava/lang/String; = "validation.num.regex"
.field public static final VALIDATION_PROPERTIES:Ljava/lang/String; = "validation.properties"
.field public static final VERSION_PROPERTIES:Ljava/lang/String; = "version.properties"
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method