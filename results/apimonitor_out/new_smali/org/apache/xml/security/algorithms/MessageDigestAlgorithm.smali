.class public Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;
.super Lorg/apache/xml/security/algorithms/Algorithm;
.field static b:Ljava/lang/ThreadLocal;
.field  a:Ljava/security/MessageDigest;
.method static constructor <clinit>()V
.registers 1
new-instance v0, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm$1;
invoke-direct {v0}, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm$1;-><init>()V
sput-object v0, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->b:Ljava/lang/ThreadLocal;
return-void
.end method
.method private constructor <init>(Lorg/w3c/dom/Document;Ljava/security/MessageDigest;Ljava/lang/String;)V
.registers 5
invoke-direct {p0, p1, p3}, Lorg/apache/xml/security/algorithms/Algorithm;-><init>(Lorg/w3c/dom/Document;Ljava/lang/String;)V
const/4 v0, 0x0
iput-object v0, p0, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->a:Ljava/security/MessageDigest;
iput-object p2, p0, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->a:Ljava/security/MessageDigest;
return-void
.end method
.method public static a(Lorg/w3c/dom/Document;Ljava/lang/String;)Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;
.registers 4
invoke-static {p1}, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->b(Ljava/lang/String;)Ljava/security/MessageDigest;
move-result-object v0
new-instance v1, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;
invoke-direct {v1, p0, v0, p1}, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;-><init>(Lorg/w3c/dom/Document;Ljava/security/MessageDigest;Ljava/lang/String;)V
return-object v1
.end method
.method public static a([B[B)Z
.registers 3
invoke-static {p0, p1}, Ljava/security/MessageDigest;->isEqual([B[B)Z
move-result v0
return v0
.end method
.method private static b(Ljava/lang/String;)Ljava/security/MessageDigest;
.registers 6
const/4 v2, 0x2
const/4 v4, 0x1
const/4 v3, 0x0
sget-object v0, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->b:Ljava/lang/ThreadLocal;
invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/util/Map;
invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/security/MessageDigest;
if-eqz v0, :cond_14
:goto_13
return-object v0
:cond_14
invoke-static {p0}, Lorg/apache/xml/security/algorithms/JCEMapper;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
if-nez v1, :cond_26
new-array v0, v4, [Ljava/lang/Object;
aput-object p0, v0, v3
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "algorithms.NoSuchMap"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v1
:cond_26
invoke-static {}, Lorg/apache/xml/security/algorithms/JCEMapper;->a()Ljava/lang/String;
move-result-object v0
if-nez v0, :cond_3e
:try_start_2c
invoke-static {v1}, Ldroidbox/java/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
:try_end_2f
.catch Ljava/security/NoSuchAlgorithmException; {:try_start_2c .. :try_end_2f} :catch_44
.catch Ljava/security/NoSuchProviderException; {:try_start_2c .. :try_end_2f} :catch_57
move-result-object v0
move-object v1, v0
:goto_31
sget-object v0, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->b:Ljava/lang/ThreadLocal;
invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/util/Map;
invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
move-object v0, v1
goto :goto_13
:try_start_3e
:cond_3e
invoke-static {v1, v0}, Ldroidbox/java/security/MessageDigest;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/MessageDigest;
:try_end_41
.catch Ljava/security/NoSuchAlgorithmException; {:try_start_3e .. :try_end_41} :catch_44
.catch Ljava/security/NoSuchProviderException; {:try_start_3e .. :try_end_41} :catch_57
move-result-object v0
move-object v1, v0
goto :goto_31
:catch_44
move-exception v0
new-array v2, v2, [Ljava/lang/Object;
aput-object v1, v2, v3
invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getLocalizedMessage()Ljava/lang/String;
move-result-object v0
aput-object v0, v2, v4
new-instance v0, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v1, "algorithms.NoSuchAlgorithm"
invoke-direct {v0, v1, v2}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
:catch_57
move-exception v0
new-array v2, v2, [Ljava/lang/Object;
aput-object v1, v2, v3
invoke-virtual {v0}, Ljava/security/NoSuchProviderException;->getLocalizedMessage()Ljava/lang/String;
move-result-object v0
aput-object v0, v2, v4
new-instance v0, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v1, "algorithms.NoSuchAlgorithm"
invoke-direct {v0, v1, v2}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
.end method
.method public a(B)V
.registers 3
iget-object v0, p0, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->a:Ljava/security/MessageDigest;
invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update(B)V
return-void
.end method
.method public a([BII)V
.registers 5
iget-object v0, p0, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->a:Ljava/security/MessageDigest;
invoke-virtual {v0, p1, p2, p3}, Ljava/security/MessageDigest;->update([BII)V
return-void
.end method
.method public b()[B
.registers 2
iget-object v0, p0, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->a:Ljava/security/MessageDigest;
invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B
move-result-object v0
return-object v0
.end method
.method public c()V
.registers 2
iget-object v0, p0, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->a:Ljava/security/MessageDigest;
invoke-virtual {v0}, Ljava/security/MessageDigest;->reset()V
return-void
.end method
.method public d()Ljava/lang/String;
.registers 2
const-string v0, "http://www.w3.org/2000/09/xmldsig#"
return-object v0
.end method
.method public e()Ljava/lang/String;
.registers 2
const-string v0, "DigestMethod"
return-object v0
.end method