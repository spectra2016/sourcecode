.class public abstract Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;
.super Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
.field static a:Lorg/apache/commons/logging/Log;
.field static c:Ljava/lang/Class;
.field static d:Ljava/lang/Class;
.field  b:I
.field private e:Ljavax/crypto/Mac;
.field private f:Z
.method static constructor <clinit>()V
.registers 1
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->c:Ljava/lang/Class;
if-nez v0, :cond_17
const-string v0, "org.apache.xml.security.algorithms.implementations.IntegrityHmac$IntegrityHmacSHA1"
invoke-static {v0}, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->c:Ljava/lang/Class;
:goto_c
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->a:Lorg/apache/commons/logging/Log;
return-void
:cond_17
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->c:Ljava/lang/Class;
goto :goto_c
.end method
.method public constructor <init>()V
.registers 6
const/4 v4, 0x0
invoke-direct {p0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;-><init>()V
const/4 v0, 0x0
iput-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
iput v4, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->b:I
iput-boolean v4, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->f:Z
invoke-virtual {p0}, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->d()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/algorithms/JCEMapper;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v0}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v0
if-eqz v0, :cond_33
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->a:Lorg/apache/commons/logging/Log;
new-instance v2, Ljava/lang/StringBuffer;
invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V
const-string v3, "Created IntegrityHmacSHA1 using "
invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v2
invoke-interface {v0, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:try_start_33
:cond_33
invoke-static {v1}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;
move-result-object v0
iput-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
:try_end_39
.catch Ljava/security/NoSuchAlgorithmException; {:try_start_33 .. :try_end_39} :catch_3a
return-void
:catch_3a
move-exception v0
const/4 v2, 0x2
new-array v2, v2, [Ljava/lang/Object;
aput-object v1, v2, v4
const/4 v1, 0x1
invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getLocalizedMessage()Ljava/lang/String;
move-result-object v0
aput-object v0, v2, v1
new-instance v0, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v1, "algorithms.NoSuchAlgorithm"
invoke-direct {v0, v1, v2}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
.end method
.method static a(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method protected a()Ljava/lang/String;
.registers 3
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->a:Lorg/apache/commons/logging/Log;
const-string v1, "engineGetJCEAlgorithmString()"
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
invoke-virtual {v0}, Ljavax/crypto/Mac;->getAlgorithm()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method protected a(B)V
.registers 5
:try_start_0
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
invoke-virtual {v0, p1}, Ljavax/crypto/Mac;->update(B)V
:try_end_5
.catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_5} :catch_6
return-void
:catch_6
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method
.method protected a(Ljava/security/Key;)V
.registers 8
instance-of v0, p1, Ljavax/crypto/SecretKey;
if-nez v0, :cond_30
invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v1
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->d:Ljava/lang/Class;
if-nez v0, :cond_2d
const-string v0, "javax.crypto.SecretKey"
invoke-static {v0}, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->d:Ljava/lang/Class;
:goto_18
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
const/4 v2, 0x2
new-array v2, v2, [Ljava/lang/Object;
const/4 v3, 0x0
aput-object v1, v2, v3
const/4 v1, 0x1
aput-object v0, v2, v1
new-instance v0, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v1, "algorithms.WrongKeyForThisOperation"
invoke-direct {v0, v1, v2}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
:cond_2d
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->d:Ljava/lang/Class;
goto :goto_18
:cond_30
:try_start_30
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
invoke-virtual {v0, p1}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V
:try_end_35
.catch Ljava/security/InvalidKeyException; {:try_start_30 .. :try_end_35} :catch_36
return-void
:catch_36
move-exception v0
iget-object v2, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
:try_start_39
iget-object v1, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
invoke-virtual {v1}, Ljavax/crypto/Mac;->getAlgorithm()Ljava/lang/String;
move-result-object v1
invoke-static {v1}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;
move-result-object v1
iput-object v1, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
:goto_45
:try_end_45
.catch Ljava/lang/Exception; {:try_start_39 .. :try_end_45} :catch_4d
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:catch_4d
move-exception v1
sget-object v3, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v3}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v3
if-eqz v3, :cond_6e
sget-object v3, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->a:Lorg/apache/commons/logging/Log;
new-instance v4, Ljava/lang/StringBuffer;
invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V
const-string v5, "Exception when reinstantiating Mac:"
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v1
invoke-interface {v3, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_6e
iput-object v2, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
goto :goto_45
.end method
.method protected a(Lorg/w3c/dom/Element;)V
.registers 5
invoke-super {p0, p1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->a(Lorg/w3c/dom/Element;)V
if-nez p1, :cond_d
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "element null"
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_d
invoke-interface {p1}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v0
const-string v1, "HMACOutputLength"
const/4 v2, 0x0
invoke-static {v0, v1, v2}, Lorg/apache/xml/security/utils/XMLUtils;->b(Lorg/w3c/dom/Node;Ljava/lang/String;I)Lorg/w3c/dom/Text;
move-result-object v0
if-eqz v0, :cond_27
invoke-interface {v0}, Lorg/w3c/dom/Text;->getData()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
move-result v0
iput v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->b:I
const/4 v0, 0x1
iput-boolean v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->f:Z
:cond_27
return-void
.end method
.method protected a([B)V
.registers 5
:try_start_0
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
invoke-virtual {v0, p1}, Ljavax/crypto/Mac;->update([B)V
:try_end_5
.catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_5} :catch_6
return-void
:catch_6
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method
.method protected a([BII)V
.registers 7
:try_start_0
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
invoke-virtual {v0, p1, p2, p3}, Ljavax/crypto/Mac;->update([BII)V
:try_end_5
.catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_5} :catch_6
return-void
:catch_6
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method
.method protected b()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
invoke-virtual {v0}, Ljavax/crypto/Mac;->getProvider()Ljava/security/Provider;
move-result-object v0
invoke-virtual {v0}, Ljava/security/Provider;->getName()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method protected b([B)Z
.registers 5
:try_start_0
iget-boolean v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->f:Z
if-eqz v0, :cond_4f
iget v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->b:I
invoke-virtual {p0}, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e()I
move-result v1
if-ge v0, v1, :cond_4f
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v0}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v0
if-eqz v0, :cond_30
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->a:Lorg/apache/commons/logging/Log;
new-instance v1, Ljava/lang/StringBuffer;
invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V
const-string v2, "HMACOutputLength must not be less than "
invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {p0}, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e()I
move-result v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v1
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_30
const/4 v0, 0x1
new-array v0, v0, [Ljava/lang/Object;
const/4 v1, 0x0
invoke-virtual {p0}, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e()I
move-result v2
invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
move-result-object v2
aput-object v2, v0, v1
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "algorithms.HMACOutputLengthMin"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v1
:try_end_46
.catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_46} :catch_46
:catch_46
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:cond_4f
:try_start_4f
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
invoke-virtual {v0}, Ljavax/crypto/Mac;->doFinal()[B
move-result-object v0
invoke-static {v0, p1}, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->a([B[B)Z
:try_end_58
.catch Ljava/lang/IllegalStateException; {:try_start_4f .. :try_end_58} :catch_46
move-result v0
return v0
.end method
.method public c()V
.registers 2
const/4 v0, 0x0
iput v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->b:I
iput-boolean v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->f:Z
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/IntegrityHmac;->e:Ljavax/crypto/Mac;
invoke-virtual {v0}, Ljavax/crypto/Mac;->reset()V
return-void
.end method
.method public abstract d()Ljava/lang/String;
.end method
.method abstract e()I
.end method