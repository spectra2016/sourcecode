.class public Lorg/apache/xml/security/algorithms/JCEMapper;
.super Ljava/lang/Object;
.field static a:Lorg/apache/commons/logging/Log;
.field static b:Ljava/lang/Class;
.field private static c:Ljava/util/Map;
.field private static d:Ljava/util/Map;
.field private static e:Ljava/lang/String;
.method static constructor <clinit>()V
.registers 1
sget-object v0, Lorg/apache/xml/security/algorithms/JCEMapper;->b:Ljava/lang/Class;
if-nez v0, :cond_1a
const-string v0, "org.apache.xml.security.algorithms.JCEMapper"
invoke-static {v0}, Lorg/apache/xml/security/algorithms/JCEMapper;->b(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/algorithms/JCEMapper;->b:Ljava/lang/Class;
:goto_c
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/algorithms/JCEMapper;->a:Lorg/apache/commons/logging/Log;
const/4 v0, 0x0
sput-object v0, Lorg/apache/xml/security/algorithms/JCEMapper;->e:Ljava/lang/String;
return-void
:cond_1a
sget-object v0, Lorg/apache/xml/security/algorithms/JCEMapper;->b:Ljava/lang/Class;
goto :goto_c
.end method
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public static a()Ljava/lang/String;
.registers 1
sget-object v0, Lorg/apache/xml/security/algorithms/JCEMapper;->e:Ljava/lang/String;
return-object v0
.end method
.method public static a(Ljava/lang/String;)Ljava/lang/String;
.registers 4
sget-object v0, Lorg/apache/xml/security/algorithms/JCEMapper;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v0}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v0
if-eqz v0, :cond_20
sget-object v0, Lorg/apache/xml/security/algorithms/JCEMapper;->a:Lorg/apache/commons/logging/Log;
new-instance v1, Ljava/lang/StringBuffer;
invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V
const-string v2, "Request for URI "
invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v1
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_20
sget-object v0, Lorg/apache/xml/security/algorithms/JCEMapper;->c:Ljava/util/Map;
invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
return-object v0
.end method
.method public static a(Lorg/w3c/dom/Element;)V
.registers 3
const-string v0, "Algorithms"
invoke-interface {p0, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;
move-result-object v0
const/4 v1, 0x0
invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;
move-result-object v0
check-cast v0, Lorg/w3c/dom/Element;
invoke-static {v0}, Lorg/apache/xml/security/algorithms/JCEMapper;->b(Lorg/w3c/dom/Element;)V
return-void
.end method
.method static b(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method static b(Lorg/w3c/dom/Element;)V
.registers 7
invoke-interface {p0}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v0
const-string v1, "http://www.xmlsecurity.org/NS/#configuration"
const-string v2, "Algorithm"
invoke-static {v0, v1, v2}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;
move-result-object v1
new-instance v0, Ljava/util/HashMap;
array-length v2, v1
mul-int/lit8 v2, v2, 0x2
invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V
sput-object v0, Lorg/apache/xml/security/algorithms/JCEMapper;->c:Ljava/util/Map;
new-instance v0, Ljava/util/HashMap;
array-length v2, v1
mul-int/lit8 v2, v2, 0x2
invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V
sput-object v0, Lorg/apache/xml/security/algorithms/JCEMapper;->d:Ljava/util/Map;
const/4 v0, 0x0
:goto_21
array-length v2, v1
if-ge v0, v2, :cond_44
aget-object v2, v1, v0
const-string v3, "URI"
invoke-interface {v2, v3}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;
move-result-object v3
const-string v4, "JCEName"
invoke-interface {v2, v4}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;
move-result-object v4
sget-object v5, Lorg/apache/xml/security/algorithms/JCEMapper;->c:Ljava/util/Map;
invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
sget-object v4, Lorg/apache/xml/security/algorithms/JCEMapper;->d:Ljava/util/Map;
new-instance v5, Lorg/apache/xml/security/algorithms/JCEMapper$Algorithm;
invoke-direct {v5, v2}, Lorg/apache/xml/security/algorithms/JCEMapper$Algorithm;-><init>(Lorg/w3c/dom/Element;)V
invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
add-int/lit8 v0, v0, 0x1
goto :goto_21
:cond_44
return-void
.end method