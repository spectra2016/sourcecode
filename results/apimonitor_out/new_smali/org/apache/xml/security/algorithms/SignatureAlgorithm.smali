.class public Lorg/apache/xml/security/algorithms/SignatureAlgorithm;
.super Lorg/apache/xml/security/algorithms/Algorithm;
.field static a:Lorg/apache/commons/logging/Log;
.field static b:Z
.field static c:Ljava/util/HashMap;
.field static d:Ljava/lang/ThreadLocal;
.field static e:Ljava/lang/ThreadLocal;
.field static f:Ljava/lang/ThreadLocal;
.field static g:Ljava/lang/ThreadLocal;
.field static i:Ljava/lang/Class;
.field protected h:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
.field private q:Ljava/lang/String;
.method static constructor <clinit>()V
.registers 1
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->i:Ljava/lang/Class;
if-nez v0, :cond_39
const-string v0, "org.apache.xml.security.algorithms.SignatureAlgorithm"
invoke-static {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->b(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->i:Ljava/lang/Class;
:goto_c
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->a:Lorg/apache/commons/logging/Log;
const/4 v0, 0x0
sput-boolean v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->b:Z
const/4 v0, 0x0
sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->c:Ljava/util/HashMap;
new-instance v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm$1;
invoke-direct {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm$1;-><init>()V
sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->d:Ljava/lang/ThreadLocal;
new-instance v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm$2;
invoke-direct {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm$2;-><init>()V
sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->e:Ljava/lang/ThreadLocal;
new-instance v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm$3;
invoke-direct {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm$3;-><init>()V
sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->f:Ljava/lang/ThreadLocal;
new-instance v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm$4;
invoke-direct {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm$4;-><init>()V
sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->g:Ljava/lang/ThreadLocal;
return-void
:cond_39
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->i:Ljava/lang/Class;
goto :goto_c
.end method
.method public constructor <init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
.registers 4
invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/algorithms/Algorithm;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
const/4 v0, 0x0
iput-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->h:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
invoke-virtual {p0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->g()Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->q:Ljava/lang/String;
return-void
.end method
.method public static a(Ljava/lang/String;Ljava/lang/String;)V
.registers 8
const/4 v5, 0x2
const/4 v4, 0x1
const/4 v3, 0x0
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v0}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v0
if-eqz v0, :cond_2d
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->a:Lorg/apache/commons/logging/Log;
new-instance v1, Ljava/lang/StringBuffer;
invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V
const-string v2, "Try to register "
invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
const-string v2, " "
invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v1
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_2d
invoke-static {p0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->g(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
if-eqz v0, :cond_4d
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_4d
invoke-virtual {v0}, Ljava/lang/String;->length()I
move-result v1
if-eqz v1, :cond_4d
new-array v1, v5, [Ljava/lang/Object;
aput-object p0, v1, v3
aput-object v0, v1, v4
new-instance v0, Lorg/apache/xml/security/exceptions/AlgorithmAlreadyRegisteredException;
const-string v2, "algorithm.alreadyRegistered"
invoke-direct {v0, v2, v1}, Lorg/apache/xml/security/exceptions/AlgorithmAlreadyRegisteredException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
:cond_4d
:try_start_4d
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->c:Ljava/util/HashMap;
invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v1
invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:try_end_56
.catch Ljava/lang/ClassNotFoundException; {:try_start_4d .. :try_end_56} :catch_57
.catch Ljava/lang/NullPointerException; {:try_start_4d .. :try_end_56} :catch_6a
return-void
:catch_57
move-exception v0
new-array v1, v5, [Ljava/lang/Object;
aput-object p0, v1, v3
invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;
move-result-object v2
aput-object v2, v1, v4
new-instance v2, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v3, "algorithms.NoSuchAlgorithm"
invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V
throw v2
:catch_6a
move-exception v0
new-array v1, v5, [Ljava/lang/Object;
aput-object p0, v1, v3
invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;
move-result-object v2
aput-object v2, v1, v4
new-instance v2, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v3, "algorithms.NoSuchAlgorithm"
invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V
throw v2
.end method
.method private a(Z)V
.registers 4
if-eqz p1, :cond_12
iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->q:Ljava/lang/String;
invoke-static {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->d(Ljava/lang/String;)Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
move-result-object v0
:goto_8
iput-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->h:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->h:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
iget-object v1, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->k:Lorg/w3c/dom/Element;
invoke-virtual {v0, v1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->a(Lorg/w3c/dom/Element;)V
return-void
:cond_12
iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->q:Ljava/lang/String;
invoke-static {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->e(Ljava/lang/String;)Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
move-result-object v0
goto :goto_8
.end method
.method static b(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method private static d(Ljava/lang/String;)Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
.registers 3
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->d:Ljava/lang/ThreadLocal;
invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/util/Map;
invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
if-eqz v0, :cond_14
invoke-virtual {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->c()V
:goto_13
return-object v0
:cond_14
invoke-static {p0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->f(Ljava/lang/String;)Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
move-result-object v1
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->d:Ljava/lang/ThreadLocal;
invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/util/Map;
invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
move-object v0, v1
goto :goto_13
.end method
.method private static e(Ljava/lang/String;)Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
.registers 3
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->e:Ljava/lang/ThreadLocal;
invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/util/Map;
invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
if-eqz v0, :cond_14
invoke-virtual {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->c()V
:goto_13
return-object v0
:cond_14
invoke-static {p0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->f(Ljava/lang/String;)Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
move-result-object v1
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->e:Ljava/lang/ThreadLocal;
invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/util/Map;
invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
move-object v0, v1
goto :goto_13
.end method
.method private static f(Ljava/lang/String;)Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
.registers 8
const/4 v6, 0x2
const/4 v5, 0x1
const/4 v4, 0x0
:try_start_3
invoke-static {p0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->g(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sget-object v1, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v1}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v1
if-eqz v1, :cond_37
sget-object v1, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->a:Lorg/apache/commons/logging/Log;
new-instance v2, Ljava/lang/StringBuffer;
invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V
const-string v3, "Create URI \""
invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
const-string v3, "\" class \""
invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
move-result-object v2
const-string v3, "\""
invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_37
invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
:try_end_3d
.catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3d} :catch_3e
.catch Ljava/lang/InstantiationException; {:try_start_3 .. :try_end_3d} :catch_51
.catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3d} :catch_64
return-object v0
:catch_3e
move-exception v0
new-array v1, v6, [Ljava/lang/Object;
aput-object p0, v1, v4
invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;
move-result-object v2
aput-object v2, v1, v5
new-instance v2, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v3, "algorithms.NoSuchAlgorithm"
invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V
throw v2
:catch_51
move-exception v0
new-array v1, v6, [Ljava/lang/Object;
aput-object p0, v1, v4
invoke-virtual {v0}, Ljava/lang/InstantiationException;->getMessage()Ljava/lang/String;
move-result-object v2
aput-object v2, v1, v5
new-instance v2, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v3, "algorithms.NoSuchAlgorithm"
invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V
throw v2
:catch_64
move-exception v0
new-array v1, v6, [Ljava/lang/Object;
aput-object p0, v1, v4
invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;
move-result-object v2
aput-object v2, v1, v5
new-instance v2, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v3, "algorithms.NoSuchAlgorithm"
invoke-direct {v2, v3, v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V
throw v2
.end method
.method private static g(Ljava/lang/String;)Ljava/lang/Class;
.registers 2
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->c:Ljava/util/HashMap;
if-nez v0, :cond_6
const/4 v0, 0x0
:goto_5
return-object v0
:cond_6
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->c:Ljava/util/HashMap;
invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Class;
goto :goto_5
.end method
.method public static h()V
.registers 2
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->a:Lorg/apache/commons/logging/Log;
if-nez v0, :cond_1a
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->i:Ljava/lang/Class;
if-nez v0, :cond_32
const-string v0, "org.apache.xml.security.algorithms.SignatureAlgorithm"
invoke-static {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->b(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->i:Ljava/lang/Class;
:goto_10
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->a:Lorg/apache/commons/logging/Log;
:cond_1a
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->a:Lorg/apache/commons/logging/Log;
const-string v1, "Init() called"
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-boolean v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->b:Z
if-nez v0, :cond_31
new-instance v0, Ljava/util/HashMap;
const/16 v1, 0xa
invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V
sput-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->c:Ljava/util/HashMap;
const/4 v0, 0x1
sput-boolean v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->b:Z
:cond_31
return-void
:cond_32
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->i:Ljava/lang/Class;
goto :goto_10
.end method
.method public a(B)V
.registers 3
iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->h:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
invoke-virtual {v0, p1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->a(B)V
return-void
.end method
.method public a(Ljava/security/Key;)V
.registers 4
const/4 v0, 0x0
invoke-direct {p0, v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->a(Z)V
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->g:Ljava/lang/ThreadLocal;
invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/util/Map;
iget-object v1, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->q:Ljava/lang/String;
invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v1
if-ne v1, p1, :cond_15
:goto_14
return-void
:cond_15
iget-object v1, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->q:Ljava/lang/String;
invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->h:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
invoke-virtual {v0, p1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->a(Ljava/security/Key;)V
goto :goto_14
.end method
.method public a([B)V
.registers 3
iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->h:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
invoke-virtual {v0, p1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->a([B)V
return-void
.end method
.method public a([BII)V
.registers 5
iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->h:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
invoke-virtual {v0, p1, p2, p3}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->a([BII)V
return-void
.end method
.method public b()Ljava/lang/String;
.registers 2
:try_start_0
iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->q:Ljava/lang/String;
invoke-static {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->e(Ljava/lang/String;)Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
move-result-object v0
invoke-virtual {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->a()Ljava/lang/String;
:try_end_9
.catch Lorg/apache/xml/security/signature/XMLSignatureException; {:try_start_0 .. :try_end_9} :catch_b
move-result-object v0
:goto_a
return-object v0
:catch_b
move-exception v0
const/4 v0, 0x0
goto :goto_a
.end method
.method public b([B)Z
.registers 3
iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->h:Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
invoke-virtual {v0, p1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->b([B)Z
move-result v0
return v0
.end method
.method public c()Ljava/lang/String;
.registers 2
:try_start_0
iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->q:Ljava/lang/String;
invoke-static {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->e(Ljava/lang/String;)Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
move-result-object v0
invoke-virtual {v0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;->b()Ljava/lang/String;
:try_end_9
.catch Lorg/apache/xml/security/signature/XMLSignatureException; {:try_start_0 .. :try_end_9} :catch_b
move-result-object v0
:goto_a
return-object v0
:catch_b
move-exception v0
const/4 v0, 0x0
goto :goto_a
.end method
.method public d()Ljava/lang/String;
.registers 2
const-string v0, "http://www.w3.org/2000/09/xmldsig#"
return-object v0
.end method
.method public e()Ljava/lang/String;
.registers 2
const-string v0, "SignatureMethod"
return-object v0
.end method
.method public f()V
.registers 2
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->g:Ljava/lang/ThreadLocal;
invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/util/Map;
invoke-interface {v0}, Ljava/util/Map;->clear()V
sget-object v0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->e:Ljava/lang/ThreadLocal;
invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/util/Map;
invoke-interface {v0}, Ljava/util/Map;->clear()V
return-void
.end method
.method public final g()Ljava/lang/String;
.registers 4
iget-object v0, p0, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->k:Lorg/w3c/dom/Element;
const/4 v1, 0x0
const-string v2, "Algorithm"
invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method