.class public abstract Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;
.super Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;
.field static a:Lorg/apache/commons/logging/Log;
.field static b:Ljava/lang/Class;
.field static c:Ljava/lang/Class;
.field private d:Ljava/security/Signature;
.method static constructor <clinit>()V
.registers 1
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->b:Ljava/lang/Class;
if-nez v0, :cond_17
const-string v0, "org.apache.xml.security.algorithms.implementations.SignatureECDSA"
invoke-static {v0}, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->b:Ljava/lang/Class;
:goto_c
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->a:Lorg/apache/commons/logging/Log;
return-void
:cond_17
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->b:Ljava/lang/Class;
goto :goto_c
.end method
.method public constructor <init>()V
.registers 8
const/4 v6, 0x2
const/4 v5, 0x1
const/4 v4, 0x0
invoke-direct {p0}, Lorg/apache/xml/security/algorithms/SignatureAlgorithmSpi;-><init>()V
const/4 v0, 0x0
iput-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
invoke-virtual {p0}, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/algorithms/JCEMapper;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v0}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v0
if-eqz v0, :cond_31
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->a:Lorg/apache/commons/logging/Log;
new-instance v2, Ljava/lang/StringBuffer;
invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V
const-string v3, "Created SignatureECDSA using "
invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v2
invoke-interface {v0, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_31
invoke-static {}, Lorg/apache/xml/security/algorithms/JCEMapper;->a()Ljava/lang/String;
move-result-object v0
if-nez v0, :cond_3e
:try_start_37
invoke-static {v1}, Ldroidbox/java/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;
move-result-object v0
iput-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
:goto_3d
return-void
:cond_3e
invoke-static {v1, v0}, Ldroidbox/java/security/Signature;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;
move-result-object v0
iput-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
:try_end_44
.catch Ljava/security/NoSuchAlgorithmException; {:try_start_37 .. :try_end_44} :catch_45
.catch Ljava/security/NoSuchProviderException; {:try_start_37 .. :try_end_44} :catch_58
goto :goto_3d
:catch_45
move-exception v0
new-array v2, v6, [Ljava/lang/Object;
aput-object v1, v2, v4
invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getLocalizedMessage()Ljava/lang/String;
move-result-object v0
aput-object v0, v2, v5
new-instance v0, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v1, "algorithms.NoSuchAlgorithm"
invoke-direct {v0, v1, v2}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
:catch_58
move-exception v0
new-array v2, v6, [Ljava/lang/Object;
aput-object v1, v2, v4
invoke-virtual {v0}, Ljava/security/NoSuchProviderException;->getLocalizedMessage()Ljava/lang/String;
move-result-object v0
aput-object v0, v2, v5
new-instance v0, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v1, "algorithms.NoSuchAlgorithm"
invoke-direct {v0, v1, v2}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
.end method
.method static a(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method private static c([B)[B
.registers 10
const/16 v7, 0x30
const/4 v8, 0x2
array-length v0, p0
if-ge v0, v7, :cond_e
new-instance v0, Ljava/io/IOException;
const-string v1, "Invalid XMLDSIG format of ECDSA signature"
invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V
throw v0
:cond_e
array-length v0, p0
div-int/lit8 v4, v0, 0x2
move v1, v4
:goto_12
if-lez v1, :cond_1d
sub-int v0, v4, v1
aget-byte v0, p0, v0
if-nez v0, :cond_1d
add-int/lit8 v1, v1, -0x1
goto :goto_12
:cond_1d
sub-int v0, v4, v1
aget-byte v0, p0, v0
if-gez v0, :cond_6e
add-int/lit8 v0, v1, 0x1
:goto_25
move v3, v4
:goto_26
if-lez v3, :cond_32
mul-int/lit8 v2, v4, 0x2
sub-int/2addr v2, v3
aget-byte v2, p0, v2
if-nez v2, :cond_32
add-int/lit8 v3, v3, -0x1
goto :goto_26
:cond_32
mul-int/lit8 v2, v4, 0x2
sub-int/2addr v2, v3
aget-byte v2, p0, v2
if-gez v2, :cond_6c
add-int/lit8 v2, v3, 0x1
:goto_3b
add-int/lit8 v5, v0, 0x6
add-int/2addr v5, v2
new-array v5, v5, [B
const/4 v6, 0x0
aput-byte v7, v5, v6
const/4 v6, 0x1
add-int/lit8 v7, v0, 0x4
add-int/2addr v7, v2
int-to-byte v7, v7
aput-byte v7, v5, v6
aput-byte v8, v5, v8
const/4 v6, 0x3
int-to-byte v7, v0
aput-byte v7, v5, v6
sub-int v6, v4, v1
add-int/lit8 v7, v0, 0x4
sub-int/2addr v7, v1
invoke-static {p0, v6, v5, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
add-int/lit8 v1, v0, 0x4
aput-byte v8, v5, v1
add-int/lit8 v1, v0, 0x5
int-to-byte v6, v2
aput-byte v6, v5, v1
mul-int/lit8 v1, v4, 0x2
sub-int/2addr v1, v3
add-int/lit8 v0, v0, 0x6
add-int/2addr v0, v2
sub-int/2addr v0, v3
invoke-static {p0, v1, v5, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
return-object v5
:cond_6c
move v2, v3
goto :goto_3b
:cond_6e
move v0, v1
goto :goto_25
.end method
.method protected a()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
invoke-virtual {v0}, Ljava/security/Signature;->getAlgorithm()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method protected a(B)V
.registers 5
:try_start_0
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
invoke-virtual {v0, p1}, Ljava/security/Signature;->update(B)V
:try_end_5
.catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_5} :catch_6
return-void
:catch_6
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method
.method protected a(Ljava/security/Key;)V
.registers 8
instance-of v0, p1, Ljava/security/PublicKey;
if-nez v0, :cond_30
invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v1
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->c:Ljava/lang/Class;
if-nez v0, :cond_2d
const-string v0, "java.security.PublicKey"
invoke-static {v0}, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->c:Ljava/lang/Class;
:goto_18
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
const/4 v2, 0x2
new-array v2, v2, [Ljava/lang/Object;
const/4 v3, 0x0
aput-object v1, v2, v3
const/4 v1, 0x1
aput-object v0, v2, v1
new-instance v0, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v1, "algorithms.WrongKeyForThisOperation"
invoke-direct {v0, v1, v2}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
:cond_2d
sget-object v0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->c:Ljava/lang/Class;
goto :goto_18
:cond_30
:try_start_30
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
check-cast p1, Ljava/security/PublicKey;
invoke-virtual {v0, p1}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V
:try_end_37
.catch Ljava/security/InvalidKeyException; {:try_start_30 .. :try_end_37} :catch_38
return-void
:catch_38
move-exception v0
iget-object v2, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
:try_start_3b
iget-object v1, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
invoke-virtual {v1}, Ljava/security/Signature;->getAlgorithm()Ljava/lang/String;
move-result-object v1
invoke-static {v1}, Ldroidbox/java/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;
move-result-object v1
iput-object v1, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
:goto_47
:try_end_47
.catch Ljava/lang/Exception; {:try_start_3b .. :try_end_47} :catch_4f
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:catch_4f
move-exception v1
sget-object v3, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v3}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v3
if-eqz v3, :cond_70
sget-object v3, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->a:Lorg/apache/commons/logging/Log;
new-instance v4, Ljava/lang/StringBuffer;
invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V
const-string v5, "Exception when reinstantiating Signature:"
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v1
invoke-interface {v3, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_70
iput-object v2, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
goto :goto_47
.end method
.method protected a([B)V
.registers 5
:try_start_0
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
invoke-virtual {v0, p1}, Ljava/security/Signature;->update([B)V
:try_end_5
.catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_5} :catch_6
return-void
:catch_6
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method
.method protected a([BII)V
.registers 7
:try_start_0
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
invoke-virtual {v0, p1, p2, p3}, Ljava/security/Signature;->update([BII)V
:try_end_5
.catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_5} :catch_6
return-void
:catch_6
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method
.method protected b()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
invoke-virtual {v0}, Ljava/security/Signature;->getProvider()Ljava/security/Provider;
move-result-object v0
invoke-virtual {v0}, Ljava/security/Provider;->getName()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method protected b([B)Z
.registers 6
:try_start_0
invoke-static {p1}, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->c([B)[B
move-result-object v0
sget-object v1, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v1}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v1
if-eqz v1, :cond_28
sget-object v1, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->a:Lorg/apache/commons/logging/Log;
new-instance v2, Ljava/lang/StringBuffer;
invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V
const-string v3, "Called ECDSA.verify() on "
invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
invoke-static {p1}, Lorg/apache/xml/security/utils/Base64;->b([B)Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_28
iget-object v1, p0, Lorg/apache/xml/security/algorithms/implementations/SignatureECDSA;->d:Ljava/security/Signature;
invoke-virtual {v1, v0}, Ljava/security/Signature;->verify([B)Z
:try_end_2d
.catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_2d} :catch_2f
.catch Ljava/io/IOException; {:try_start_0 .. :try_end_2d} :catch_38
move-result v0
return v0
:catch_2f
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:catch_38
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method
.method public abstract d()Ljava/lang/String;
.end method