.class public Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;
.super Lorg/apache/xml/security/utils/SignatureElementProxy;
.implements Lorg/apache/xml/security/keys/content/x509/XMLX509DataContent;
.method public a()Ljava/lang/String;
.registers 2
invoke-virtual {p0}, Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;->o()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/RFC2253Parser;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public e()Ljava/lang/String;
.registers 2
const-string v0, "X509SubjectName"
return-object v0
.end method
.method public equals(Ljava/lang/Object;)Z
.registers 4
instance-of v0, p1, Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;
if-nez v0, :cond_6
const/4 v0, 0x0
:goto_5
return v0
:cond_6
check-cast p1, Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;
invoke-virtual {p1}, Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;->a()Ljava/lang/String;
move-result-object v0
invoke-virtual {p0}, Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;->a()Ljava/lang/String;
move-result-object v1
invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
goto :goto_5
.end method
.method public hashCode()I
.registers 2
invoke-virtual {p0}, Lorg/apache/xml/security/keys/content/x509/XMLX509SubjectName;->a()Ljava/lang/String;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
move-result v0
add-int/lit16 v0, v0, 0x20f
return v0
.end method