.class public Lorg/apache/xml/security/keys/KeyInfo;
.super Lorg/apache/xml/security/utils/SignatureElementProxy;
.field static a:Lorg/apache/commons/logging/Log;
.field static final d:Ljava/util/List;
.field static g:Z
.field static h:Ljava/lang/Class;
.field  b:Ljava/util/List;
.field  c:Ljava/util/List;
.field  e:Ljava/util/List;
.field  f:Ljava/util/List;
.method static constructor <clinit>()V
.registers 2
sget-object v0, Lorg/apache/xml/security/keys/KeyInfo;->h:Ljava/lang/Class;
if-nez v0, :cond_29
const-string v0, "org.apache.xml.security.keys.KeyInfo"
invoke-static {v0}, Lorg/apache/xml/security/keys/KeyInfo;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/keys/KeyInfo;->h:Ljava/lang/Class;
:goto_c
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/keys/KeyInfo;->a:Lorg/apache/commons/logging/Log;
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
const/4 v1, 0x0
invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/keys/KeyInfo;->d:Ljava/util/List;
const/4 v0, 0x0
sput-boolean v0, Lorg/apache/xml/security/keys/KeyInfo;->g:Z
return-void
:cond_29
sget-object v0, Lorg/apache/xml/security/keys/KeyInfo;->h:Ljava/lang/Class;
goto :goto_c
.end method
.method public constructor <init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
.registers 4
const/4 v0, 0x0
invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/utils/SignatureElementProxy;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
iput-object v0, p0, Lorg/apache/xml/security/keys/KeyInfo;->b:Ljava/util/List;
iput-object v0, p0, Lorg/apache/xml/security/keys/KeyInfo;->c:Ljava/util/List;
iput-object v0, p0, Lorg/apache/xml/security/keys/KeyInfo;->e:Ljava/util/List;
sget-object v0, Lorg/apache/xml/security/keys/KeyInfo;->d:Ljava/util/List;
iput-object v0, p0, Lorg/apache/xml/security/keys/KeyInfo;->f:Ljava/util/List;
return-void
.end method
.method static a(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method public static a()V
.registers 2
sget-boolean v0, Lorg/apache/xml/security/keys/KeyInfo;->g:Z
if-nez v0, :cond_28
sget-object v0, Lorg/apache/xml/security/keys/KeyInfo;->a:Lorg/apache/commons/logging/Log;
if-nez v0, :cond_25
sget-object v0, Lorg/apache/xml/security/keys/KeyInfo;->h:Ljava/lang/Class;
if-nez v0, :cond_29
const-string v0, "org.apache.xml.security.keys.KeyInfo"
invoke-static {v0}, Lorg/apache/xml/security/keys/KeyInfo;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/keys/KeyInfo;->h:Ljava/lang/Class;
:goto_14
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/keys/KeyInfo;->a:Lorg/apache/commons/logging/Log;
sget-object v0, Lorg/apache/xml/security/keys/KeyInfo;->a:Lorg/apache/commons/logging/Log;
const-string v1, "Had to assign log in the init() function"
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->error(Ljava/lang/Object;)V
:cond_25
const/4 v0, 0x1
sput-boolean v0, Lorg/apache/xml/security/keys/KeyInfo;->g:Z
:cond_28
return-void
:cond_29
sget-object v0, Lorg/apache/xml/security/keys/KeyInfo;->h:Ljava/lang/Class;
goto :goto_14
.end method
.method public e()Ljava/lang/String;
.registers 2
const-string v0, "KeyInfo"
return-object v0
.end method