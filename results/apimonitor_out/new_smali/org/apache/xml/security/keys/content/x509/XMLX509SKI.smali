.class public Lorg/apache/xml/security/keys/content/x509/XMLX509SKI;
.super Lorg/apache/xml/security/utils/SignatureElementProxy;
.implements Lorg/apache/xml/security/keys/content/x509/XMLX509DataContent;
.field static a:Lorg/apache/commons/logging/Log;
.field static b:Ljava/lang/Class;
.method static constructor <clinit>()V
.registers 1
sget-object v0, Lorg/apache/xml/security/keys/content/x509/XMLX509SKI;->b:Ljava/lang/Class;
if-nez v0, :cond_17
const-string v0, "org.apache.xml.security.keys.content.x509.XMLX509SKI"
invoke-static {v0}, Lorg/apache/xml/security/keys/content/x509/XMLX509SKI;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/keys/content/x509/XMLX509SKI;->b:Ljava/lang/Class;
:goto_c
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/keys/content/x509/XMLX509SKI;->a:Lorg/apache/commons/logging/Log;
return-void
:cond_17
sget-object v0, Lorg/apache/xml/security/keys/content/x509/XMLX509SKI;->b:Ljava/lang/Class;
goto :goto_c
.end method
.method static a(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method public a()[B
.registers 2
invoke-virtual {p0}, Lorg/apache/xml/security/keys/content/x509/XMLX509SKI;->n()[B
move-result-object v0
return-object v0
.end method
.method public e()Ljava/lang/String;
.registers 2
const-string v0, "X509SKI"
return-object v0
.end method
.method public equals(Ljava/lang/Object;)Z
.registers 5
const/4 v0, 0x0
instance-of v1, p1, Lorg/apache/xml/security/keys/content/x509/XMLX509SKI;
if-nez v1, :cond_6
:goto_5
return v0
:cond_6
check-cast p1, Lorg/apache/xml/security/keys/content/x509/XMLX509SKI;
:try_start_8
invoke-virtual {p1}, Lorg/apache/xml/security/keys/content/x509/XMLX509SKI;->a()[B
move-result-object v1
invoke-virtual {p0}, Lorg/apache/xml/security/keys/content/x509/XMLX509SKI;->a()[B
move-result-object v2
invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z
:try_end_13
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_8 .. :try_end_13} :catch_15
move-result v0
goto :goto_5
:catch_15
move-exception v1
goto :goto_5
.end method
.method public hashCode()I
.registers 5
const/16 v0, 0x11
:try_start_2
invoke-virtual {p0}, Lorg/apache/xml/security/keys/content/x509/XMLX509SKI;->a()[B
move-result-object v3
const/4 v1, 0x0
:goto_7
array-length v2, v3
if-ge v1, v2, :cond_15
mul-int/lit8 v2, v0, 0x1f
aget-byte v0, v3, v1
:try_end_e
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_2 .. :try_end_e} :catch_14
add-int/2addr v2, v0
add-int/lit8 v0, v1, 0x1
move v1, v0
move v0, v2
goto :goto_7
:catch_14
move-exception v1
:cond_15
return v0
.end method