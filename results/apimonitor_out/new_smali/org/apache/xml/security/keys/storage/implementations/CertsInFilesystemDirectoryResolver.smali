.class public Lorg/apache/xml/security/keys/storage/implementations/CertsInFilesystemDirectoryResolver;
.super Lorg/apache/xml/security/keys/storage/StorageResolverSpi;
.field static a:Lorg/apache/commons/logging/Log;
.field static b:Ljava/lang/Class;
.field private c:Ljava/util/List;
.method static constructor <clinit>()V
.registers 1
sget-object v0, Lorg/apache/xml/security/keys/storage/implementations/CertsInFilesystemDirectoryResolver;->b:Ljava/lang/Class;
if-nez v0, :cond_17
const-string v0, "org.apache.xml.security.keys.storage.implementations.CertsInFilesystemDirectoryResolver"
invoke-static {v0}, Lorg/apache/xml/security/keys/storage/implementations/CertsInFilesystemDirectoryResolver;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/keys/storage/implementations/CertsInFilesystemDirectoryResolver;->b:Ljava/lang/Class;
:goto_c
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/keys/storage/implementations/CertsInFilesystemDirectoryResolver;->a:Lorg/apache/commons/logging/Log;
return-void
:cond_17
sget-object v0, Lorg/apache/xml/security/keys/storage/implementations/CertsInFilesystemDirectoryResolver;->b:Ljava/lang/Class;
goto :goto_c
.end method
.method static a(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method public a()Ljava/util/Iterator;
.registers 3
new-instance v0, Lorg/apache/xml/security/keys/storage/implementations/CertsInFilesystemDirectoryResolver$FilesystemIterator;
iget-object v1, p0, Lorg/apache/xml/security/keys/storage/implementations/CertsInFilesystemDirectoryResolver;->c:Ljava/util/List;
invoke-direct {v0, v1}, Lorg/apache/xml/security/keys/storage/implementations/CertsInFilesystemDirectoryResolver$FilesystemIterator;-><init>(Ljava/util/List;)V
return-object v0
.end method