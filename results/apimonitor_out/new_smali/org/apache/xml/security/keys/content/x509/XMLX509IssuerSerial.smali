.class public Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;
.super Lorg/apache/xml/security/utils/SignatureElementProxy;
.implements Lorg/apache/xml/security/keys/content/x509/XMLX509DataContent;
.field static a:Lorg/apache/commons/logging/Log;
.field static b:Ljava/lang/Class;
.method static constructor <clinit>()V
.registers 1
sget-object v0, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->b:Ljava/lang/Class;
if-nez v0, :cond_17
const-string v0, "org.apache.xml.security.keys.content.x509.XMLX509IssuerSerial"
invoke-static {v0}, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->b:Ljava/lang/Class;
:goto_c
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->a:Lorg/apache/commons/logging/Log;
return-void
:cond_17
sget-object v0, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->b:Ljava/lang/Class;
goto :goto_c
.end method
.method static a(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method public a()Ljava/math/BigInteger;
.registers 5
const-string v0, "X509SerialNumber"
const-string v1, "http://www.w3.org/2000/09/xmldsig#"
invoke-virtual {p0, v0, v1}, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
sget-object v1, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v1}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v1
if-eqz v1, :cond_28
sget-object v1, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->a:Lorg/apache/commons/logging/Log;
new-instance v2, Ljava/lang/StringBuffer;
invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V
const-string v3, "X509SerialNumber text: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_28
new-instance v1, Ljava/math/BigInteger;
invoke-direct {v1, v0}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V
return-object v1
.end method
.method public b()Ljava/lang/String;
.registers 3
const-string v0, "X509IssuerName"
const-string v1, "http://www.w3.org/2000/09/xmldsig#"
invoke-virtual {p0, v0, v1}, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/RFC2253Parser;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public e()Ljava/lang/String;
.registers 2
const-string v0, "X509IssuerSerial"
return-object v0
.end method
.method public equals(Ljava/lang/Object;)Z
.registers 5
const/4 v0, 0x0
instance-of v1, p1, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;
if-nez v1, :cond_6
:cond_5
:goto_5
return v0
:cond_6
check-cast p1, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;
invoke-virtual {p0}, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->a()Ljava/math/BigInteger;
move-result-object v1
invoke-virtual {p1}, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->a()Ljava/math/BigInteger;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_5
invoke-virtual {p0}, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->b()Ljava/lang/String;
move-result-object v1
invoke-virtual {p1}, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->b()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_5
const/4 v0, 0x1
goto :goto_5
.end method
.method public hashCode()I
.registers 3
invoke-virtual {p0}, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->a()Ljava/math/BigInteger;
move-result-object v0
invoke-virtual {v0}, Ljava/math/BigInteger;->hashCode()I
move-result v0
add-int/lit16 v0, v0, 0x20f
mul-int/lit8 v0, v0, 0x1f
invoke-virtual {p0}, Lorg/apache/xml/security/keys/content/x509/XMLX509IssuerSerial;->b()Ljava/lang/String;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
move-result v1
add-int/2addr v0, v1
return v0
.end method