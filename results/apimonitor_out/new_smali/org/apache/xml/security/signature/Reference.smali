.class public Lorg/apache/xml/security/signature/Reference;
.super Lorg/apache/xml/security/utils/SignatureElementProxy;
.field static a:Lorg/apache/commons/logging/Log;
.field static d:Ljava/lang/Class;
.field private static e:Z
.field  b:Lorg/apache/xml/security/signature/Manifest;
.field  c:Lorg/apache/xml/security/signature/XMLSignatureInput;
.field private f:Lorg/apache/xml/security/transforms/Transforms;
.field private g:Lorg/w3c/dom/Element;
.field private h:Lorg/w3c/dom/Element;
.method static constructor <clinit>()V
.registers 1
new-instance v0, Lorg/apache/xml/security/signature/Reference$1;
invoke-direct {v0}, Lorg/apache/xml/security/signature/Reference$1;-><init>()V
invoke-static {v0}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedAction;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Boolean;
invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
move-result v0
sput-boolean v0, Lorg/apache/xml/security/signature/Reference;->e:Z
sget-object v0, Lorg/apache/xml/security/signature/Reference;->d:Ljava/lang/Class;
if-nez v0, :cond_28
const-string v0, "org.apache.xml.security.signature.Reference"
invoke-static {v0}, Lorg/apache/xml/security/signature/Reference;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/signature/Reference;->d:Ljava/lang/Class;
:goto_1d
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/signature/Reference;->a:Lorg/apache/commons/logging/Log;
return-void
:cond_28
sget-object v0, Lorg/apache/xml/security/signature/Reference;->d:Ljava/lang/Class;
goto :goto_1d
.end method
.method protected constructor <init>(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/signature/Manifest;)V
.registers 7
invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/utils/SignatureElementProxy;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
const/4 v0, 0x0
iput-object v0, p0, Lorg/apache/xml/security/signature/Reference;->b:Lorg/apache/xml/security/signature/Manifest;
iput-object p2, p0, Lorg/apache/xml/security/signature/Reference;->l:Ljava/lang/String;
invoke-interface {p1}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Element;
move-result-object v0
const-string v1, "Transforms"
invoke-interface {v0}, Lorg/w3c/dom/Element;->getLocalName()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_39
const-string v1, "http://www.w3.org/2000/09/xmldsig#"
invoke-interface {v0}, Lorg/w3c/dom/Element;->getNamespaceURI()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_39
new-instance v1, Lorg/apache/xml/security/transforms/Transforms;
iget-object v2, p0, Lorg/apache/xml/security/signature/Reference;->l:Ljava/lang/String;
invoke-direct {v1, v0, v2}, Lorg/apache/xml/security/transforms/Transforms;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
iput-object v1, p0, Lorg/apache/xml/security/signature/Reference;->f:Lorg/apache/xml/security/transforms/Transforms;
invoke-interface {v0}, Lorg/w3c/dom/Element;->getNextSibling()Lorg/w3c/dom/Node;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Element;
move-result-object v0
:cond_39
iput-object v0, p0, Lorg/apache/xml/security/signature/Reference;->g:Lorg/w3c/dom/Element;
iget-object v0, p0, Lorg/apache/xml/security/signature/Reference;->g:Lorg/w3c/dom/Element;
invoke-interface {v0}, Lorg/w3c/dom/Element;->getNextSibling()Lorg/w3c/dom/Node;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Element;
move-result-object v0
iput-object v0, p0, Lorg/apache/xml/security/signature/Reference;->h:Lorg/w3c/dom/Element;
iput-object p3, p0, Lorg/apache/xml/security/signature/Reference;->b:Lorg/apache/xml/security/signature/Manifest;
return-void
.end method
.method static a(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method private a(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;)Lorg/apache/xml/security/signature/XMLSignatureInput;
.registers 6
:try_start_0
invoke-virtual {p0}, Lorg/apache/xml/security/signature/Reference;->h()Lorg/apache/xml/security/transforms/Transforms;
move-result-object v0
if-eqz v0, :cond_c
invoke-virtual {v0, p1, p2}, Lorg/apache/xml/security/transforms/Transforms;->a(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;)Lorg/apache/xml/security/signature/XMLSignatureInput;
move-result-object p1
iput-object p1, p0, Lorg/apache/xml/security/signature/Reference;->c:Lorg/apache/xml/security/signature/XMLSignatureInput;
:cond_c
:try_end_c
.catch Lorg/apache/xml/security/utils/resolver/ResourceResolverException; {:try_start_0 .. :try_end_c} :catch_d
.catch Lorg/apache/xml/security/c14n/CanonicalizationException; {:try_start_0 .. :try_end_c} :catch_16
.catch Lorg/apache/xml/security/c14n/InvalidCanonicalizerException; {:try_start_0 .. :try_end_c} :catch_1f
.catch Lorg/apache/xml/security/transforms/TransformationException; {:try_start_0 .. :try_end_c} :catch_28
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_0 .. :try_end_c} :catch_31
return-object p1
:catch_d
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:catch_16
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:catch_1f
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:catch_28
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:catch_31
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method
.method private a(Z)[B
.registers 8
:try_start_0
invoke-virtual {p0}, Lorg/apache/xml/security/signature/Reference;->a()Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;
move-result-object v0
invoke-virtual {v0}, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->c()V
new-instance v1, Lorg/apache/xml/security/utils/DigesterOutputStream;
invoke-direct {v1, v0}, Lorg/apache/xml/security/utils/DigesterOutputStream;-><init>(Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;)V
new-instance v0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;
invoke-direct {v0, v1}, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
invoke-virtual {p0, v0}, Lorg/apache/xml/security/signature/Reference;->a(Ljava/io/OutputStream;)Lorg/apache/xml/security/signature/XMLSignatureInput;
move-result-object v2
sget-boolean v3, Lorg/apache/xml/security/signature/Reference;->e:Z
if-eqz v3, :cond_54
if-nez p1, :cond_54
invoke-virtual {v2}, Lorg/apache/xml/security/signature/XMLSignatureInput;->i()Z
move-result v3
if-nez v3, :cond_54
invoke-virtual {v2}, Lorg/apache/xml/security/signature/XMLSignatureInput;->h()Z
move-result v3
if-nez v3, :cond_54
iget-object v3, p0, Lorg/apache/xml/security/signature/Reference;->f:Lorg/apache/xml/security/transforms/Transforms;
if-nez v3, :cond_41
new-instance v3, Lorg/apache/xml/security/transforms/Transforms;
iget-object v4, p0, Lorg/apache/xml/security/signature/Reference;->m:Lorg/w3c/dom/Document;
invoke-direct {v3, v4}, Lorg/apache/xml/security/transforms/Transforms;-><init>(Lorg/w3c/dom/Document;)V
iput-object v3, p0, Lorg/apache/xml/security/signature/Reference;->f:Lorg/apache/xml/security/transforms/Transforms;
iget-object v3, p0, Lorg/apache/xml/security/signature/Reference;->k:Lorg/w3c/dom/Element;
iget-object v4, p0, Lorg/apache/xml/security/signature/Reference;->f:Lorg/apache/xml/security/transforms/Transforms;
invoke-virtual {v4}, Lorg/apache/xml/security/transforms/Transforms;->k()Lorg/w3c/dom/Element;
move-result-object v4
iget-object v5, p0, Lorg/apache/xml/security/signature/Reference;->g:Lorg/w3c/dom/Element;
invoke-interface {v3, v4, v5}, Lorg/w3c/dom/Element;->insertBefore(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;
:cond_41
iget-object v3, p0, Lorg/apache/xml/security/signature/Reference;->f:Lorg/apache/xml/security/transforms/Transforms;
const-string v4, "http://www.w3.org/2006/12/xml-c14n11"
invoke-virtual {v3, v4}, Lorg/apache/xml/security/transforms/Transforms;->a(Ljava/lang/String;)V
const/4 v3, 0x1
invoke-virtual {v2, v0, v3}, Lorg/apache/xml/security/signature/XMLSignatureInput;->a(Ljava/io/OutputStream;Z)V
:goto_4c
invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
invoke-virtual {v1}, Lorg/apache/xml/security/utils/DigesterOutputStream;->a()[B
move-result-object v0
return-object v0
:cond_54
invoke-virtual {v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureInput;->a(Ljava/io/OutputStream;)V
:try_end_57
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_0 .. :try_end_57} :catch_58
.catch Ljava/io/IOException; {:try_start_0 .. :try_end_57} :catch_61
goto :goto_4c
:catch_58
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:catch_61
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method
.method public a()Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;
.registers 4
const/4 v0, 0x0
iget-object v1, p0, Lorg/apache/xml/security/signature/Reference;->g:Lorg/w3c/dom/Element;
if-nez v1, :cond_6
:cond_5
:goto_5
return-object v0
:cond_6
iget-object v1, p0, Lorg/apache/xml/security/signature/Reference;->g:Lorg/w3c/dom/Element;
const-string v2, "Algorithm"
invoke-interface {v1, v0, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
if-eqz v1, :cond_5
iget-object v0, p0, Lorg/apache/xml/security/signature/Reference;->m:Lorg/w3c/dom/Document;
invoke-static {v0, v1}, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->a(Lorg/w3c/dom/Document;Ljava/lang/String;)Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;
move-result-object v0
goto :goto_5
.end method
.method protected a(Ljava/io/OutputStream;)Lorg/apache/xml/security/signature/XMLSignatureInput;
.registers 5
:try_start_0
invoke-virtual {p0}, Lorg/apache/xml/security/signature/Reference;->g()Lorg/apache/xml/security/signature/XMLSignatureInput;
move-result-object v0
invoke-direct {p0, v0, p1}, Lorg/apache/xml/security/signature/Reference;->a(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;)Lorg/apache/xml/security/signature/XMLSignatureInput;
move-result-object v0
iput-object v0, p0, Lorg/apache/xml/security/signature/Reference;->c:Lorg/apache/xml/security/signature/XMLSignatureInput;
:try_end_a
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_0 .. :try_end_a} :catch_b
return-object v0
:catch_b
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method
.method public b()Ljava/lang/String;
.registers 4
iget-object v0, p0, Lorg/apache/xml/security/signature/Reference;->k:Lorg/w3c/dom/Element;
const/4 v1, 0x0
const-string v2, "URI"
invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public c()Ljava/lang/String;
.registers 4
iget-object v0, p0, Lorg/apache/xml/security/signature/Reference;->k:Lorg/w3c/dom/Element;
const/4 v1, 0x0
const-string v2, "Type"
invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public e()Ljava/lang/String;
.registers 2
const-string v0, "Reference"
return-object v0
.end method
.method public f()Z
.registers 3
const-string v0, "http://www.w3.org/2000/09/xmldsig#Manifest"
invoke-virtual {p0}, Lorg/apache/xml/security/signature/Reference;->c()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_e
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method public g()Lorg/apache/xml/security/signature/XMLSignatureInput;
.registers 5
const/4 v0, 0x0
:try_start_1
iget-object v1, p0, Lorg/apache/xml/security/signature/Reference;->k:Lorg/w3c/dom/Element;
const/4 v2, 0x0
const-string v3, "URI"
invoke-interface {v1, v2, v3}, Lorg/w3c/dom/Element;->getAttributeNodeNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Attr;
move-result-object v1
if-nez v1, :cond_2f
:goto_c
iget-object v2, p0, Lorg/apache/xml/security/signature/Reference;->l:Ljava/lang/String;
iget-object v3, p0, Lorg/apache/xml/security/signature/Reference;->b:Lorg/apache/xml/security/signature/Manifest;
iget-object v3, v3, Lorg/apache/xml/security/signature/Manifest;->e:Ljava/util/List;
invoke-static {v1, v2, v3}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a(Lorg/w3c/dom/Attr;Ljava/lang/String;Ljava/util/List;)Lorg/apache/xml/security/utils/resolver/ResourceResolver;
move-result-object v2
if-nez v2, :cond_34
const/4 v1, 0x1
new-array v1, v1, [Ljava/lang/Object;
const/4 v2, 0x0
aput-object v0, v1, v2
new-instance v0, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;
const-string v2, "signature.Verification.Reference.NoInput"
invoke-direct {v0, v2, v1}, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
:catch_26
:try_end_26
.catch Lorg/apache/xml/security/utils/resolver/ResourceResolverException; {:try_start_1 .. :try_end_26} :catch_26
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_1 .. :try_end_26} :catch_42
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:try_start_2f
:cond_2f
invoke-interface {v1}, Lorg/w3c/dom/Attr;->getNodeValue()Ljava/lang/String;
move-result-object v0
goto :goto_c
:cond_34
iget-object v0, p0, Lorg/apache/xml/security/signature/Reference;->b:Lorg/apache/xml/security/signature/Manifest;
iget-object v0, v0, Lorg/apache/xml/security/signature/Manifest;->d:Ljava/util/HashMap;
invoke-virtual {v2, v0}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a(Ljava/util/Map;)V
iget-object v0, p0, Lorg/apache/xml/security/signature/Reference;->l:Ljava/lang/String;
invoke-virtual {v2, v1, v0}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->b(Lorg/w3c/dom/Attr;Ljava/lang/String;)Lorg/apache/xml/security/signature/XMLSignatureInput;
:try_end_40
.catch Lorg/apache/xml/security/utils/resolver/ResourceResolverException; {:try_start_2f .. :try_end_40} :catch_26
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_2f .. :try_end_40} :catch_42
move-result-object v0
return-object v0
:catch_42
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/ReferenceNotInitializedException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method
.method public h()Lorg/apache/xml/security/transforms/Transforms;
.registers 2
iget-object v0, p0, Lorg/apache/xml/security/signature/Reference;->f:Lorg/apache/xml/security/transforms/Transforms;
return-object v0
.end method
.method public i()[B
.registers 4
iget-object v0, p0, Lorg/apache/xml/security/signature/Reference;->h:Lorg/w3c/dom/Element;
if-nez v0, :cond_19
const/4 v0, 0x2
new-array v0, v0, [Ljava/lang/Object;
const/4 v1, 0x0
const-string v2, "DigestValue"
aput-object v2, v0, v1
const/4 v1, 0x1
const-string v2, "http://www.w3.org/2000/09/xmldsig#"
aput-object v2, v0, v1
new-instance v1, Lorg/apache/xml/security/exceptions/XMLSecurityException;
const-string v2, "signature.Verification.NoSignatureElement"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/exceptions/XMLSecurityException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v1
:cond_19
iget-object v0, p0, Lorg/apache/xml/security/signature/Reference;->h:Lorg/w3c/dom/Element;
invoke-static {v0}, Lorg/apache/xml/security/utils/Base64;->a(Lorg/w3c/dom/Element;)[B
move-result-object v0
return-object v0
.end method
.method public j()Z
.registers 7
invoke-virtual {p0}, Lorg/apache/xml/security/signature/Reference;->i()[B
move-result-object v0
const/4 v1, 0x1
invoke-direct {p0, v1}, Lorg/apache/xml/security/signature/Reference;->a(Z)[B
move-result-object v1
invoke-static {v0, v1}, Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;->a([B[B)Z
move-result v2
if-nez v2, :cond_6a
sget-object v3, Lorg/apache/xml/security/signature/Reference;->a:Lorg/apache/commons/logging/Log;
new-instance v4, Ljava/lang/StringBuffer;
invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V
const-string v5, "Verification failed for URI \""
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-virtual {p0}, Lorg/apache/xml/security/signature/Reference;->b()Ljava/lang/String;
move-result-object v5
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
const-string v5, "\""
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v4
invoke-interface {v3, v4}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V
sget-object v3, Lorg/apache/xml/security/signature/Reference;->a:Lorg/apache/commons/logging/Log;
new-instance v4, Ljava/lang/StringBuffer;
invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V
const-string v5, "Expected Digest: "
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-static {v0}, Lorg/apache/xml/security/utils/Base64;->b([B)Ljava/lang/String;
move-result-object v0
invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
invoke-interface {v3, v0}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V
sget-object v0, Lorg/apache/xml/security/signature/Reference;->a:Lorg/apache/commons/logging/Log;
new-instance v3, Ljava/lang/StringBuffer;
invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V
const-string v4, "Actual Digest: "
invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v3
invoke-static {v1}, Lorg/apache/xml/security/utils/Base64;->b([B)Ljava/lang/String;
move-result-object v1
invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v1
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V
:goto_69
return v2
:cond_6a
sget-object v0, Lorg/apache/xml/security/signature/Reference;->a:Lorg/apache/commons/logging/Log;
new-instance v1, Ljava/lang/StringBuffer;
invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V
const-string v3, "Verification successful for URI \""
invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {p0}, Lorg/apache/xml/security/signature/Reference;->b()Ljava/lang/String;
move-result-object v3
invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
const-string v3, "\""
invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v1
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
goto :goto_69
.end method