.class public Lorg/apache/xml/security/signature/MissingResourceFailureException;
.super Lorg/apache/xml/security/signature/XMLSignatureException;
.field  c:Lorg/apache/xml/security/signature/Reference;
.method public constructor <init>(Ljava/lang/String;Lorg/apache/xml/security/signature/Reference;)V
.registers 4
invoke-direct {p0, p1}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;)V
const/4 v0, 0x0
iput-object v0, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->c:Lorg/apache/xml/security/signature/Reference;
iput-object p2, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->c:Lorg/apache/xml/security/signature/Reference;
return-void
.end method
.method public constructor <init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;Lorg/apache/xml/security/signature/Reference;)V
.registers 6
invoke-direct {p0, p1, p2, p3}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V
const/4 v0, 0x0
iput-object v0, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->c:Lorg/apache/xml/security/signature/Reference;
iput-object p4, p0, Lorg/apache/xml/security/signature/MissingResourceFailureException;->c:Lorg/apache/xml/security/signature/Reference;
return-void
.end method