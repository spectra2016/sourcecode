.class public Lorg/apache/xml/security/signature/ReferenceNotInitializedException;
.super Lorg/apache/xml/security/signature/XMLSignatureException;
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>()V
return-void
.end method
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Exception;)V
.registers 3
invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
return-void
.end method
.method public constructor <init>(Ljava/lang/String;[Ljava/lang/Object;)V
.registers 3
invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
return-void
.end method