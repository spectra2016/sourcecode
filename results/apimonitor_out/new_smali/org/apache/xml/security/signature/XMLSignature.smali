.class public final Lorg/apache/xml/security/signature/XMLSignature;
.super Lorg/apache/xml/security/utils/SignatureElementProxy;
.field static a:Lorg/apache/commons/logging/Log;
.field static b:Ljava/lang/Class;
.field private c:Lorg/apache/xml/security/signature/SignedInfo;
.field private d:Lorg/apache/xml/security/keys/KeyInfo;
.field private e:Z
.field private f:Lorg/w3c/dom/Element;
.field private g:I
.method static constructor <clinit>()V
.registers 1
sget-object v0, Lorg/apache/xml/security/signature/XMLSignature;->b:Ljava/lang/Class;
if-nez v0, :cond_17
const-string v0, "org.apache.xml.security.signature.XMLSignature"
invoke-static {v0}, Lorg/apache/xml/security/signature/XMLSignature;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/signature/XMLSignature;->b:Ljava/lang/Class;
:goto_c
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/signature/XMLSignature;->a:Lorg/apache/commons/logging/Log;
return-void
:cond_17
sget-object v0, Lorg/apache/xml/security/signature/XMLSignature;->b:Ljava/lang/Class;
goto :goto_c
.end method
.method public constructor <init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
.registers 8
const/4 v0, 0x0
const/4 v4, 0x2
const/4 v3, 0x1
const/4 v2, 0x0
invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/utils/SignatureElementProxy;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
iput-object v0, p0, Lorg/apache/xml/security/signature/XMLSignature;->c:Lorg/apache/xml/security/signature/SignedInfo;
iput-object v0, p0, Lorg/apache/xml/security/signature/XMLSignature;->d:Lorg/apache/xml/security/keys/KeyInfo;
iput-boolean v2, p0, Lorg/apache/xml/security/signature/XMLSignature;->e:Z
iput v2, p0, Lorg/apache/xml/security/signature/XMLSignature;->g:I
invoke-interface {p1}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Element;
move-result-object v0
if-nez v0, :cond_2b
new-array v0, v4, [Ljava/lang/Object;
const-string v1, "SignedInfo"
aput-object v1, v0, v2
const-string v1, "Signature"
aput-object v1, v0, v3
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "xml.WrongContent"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v1
:cond_2b
new-instance v1, Lorg/apache/xml/security/signature/SignedInfo;
invoke-direct {v1, v0, p2}, Lorg/apache/xml/security/signature/SignedInfo;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
iput-object v1, p0, Lorg/apache/xml/security/signature/XMLSignature;->c:Lorg/apache/xml/security/signature/SignedInfo;
invoke-interface {p1}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Element;
move-result-object v0
invoke-interface {v0}, Lorg/w3c/dom/Element;->getNextSibling()Lorg/w3c/dom/Node;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Element;
move-result-object v0
iput-object v0, p0, Lorg/apache/xml/security/signature/XMLSignature;->f:Lorg/w3c/dom/Element;
iget-object v0, p0, Lorg/apache/xml/security/signature/XMLSignature;->f:Lorg/w3c/dom/Element;
if-nez v0, :cond_5a
new-array v0, v4, [Ljava/lang/Object;
const-string v1, "SignatureValue"
aput-object v1, v0, v2
const-string v1, "Signature"
aput-object v1, v0, v3
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "xml.WrongContent"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v1
:cond_5a
iget-object v0, p0, Lorg/apache/xml/security/signature/XMLSignature;->f:Lorg/w3c/dom/Element;
invoke-interface {v0}, Lorg/w3c/dom/Element;->getNextSibling()Lorg/w3c/dom/Node;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Element;
move-result-object v0
if-eqz v0, :cond_85
invoke-interface {v0}, Lorg/w3c/dom/Element;->getNamespaceURI()Ljava/lang/String;
move-result-object v1
const-string v2, "http://www.w3.org/2000/09/xmldsig#"
invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_85
invoke-interface {v0}, Lorg/w3c/dom/Element;->getLocalName()Ljava/lang/String;
move-result-object v1
const-string v2, "KeyInfo"
invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_85
new-instance v1, Lorg/apache/xml/security/keys/KeyInfo;
invoke-direct {v1, v0, p2}, Lorg/apache/xml/security/keys/KeyInfo;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
iput-object v1, p0, Lorg/apache/xml/security/signature/XMLSignature;->d:Lorg/apache/xml/security/keys/KeyInfo;
:cond_85
iput v3, p0, Lorg/apache/xml/security/signature/XMLSignature;->g:I
return-void
.end method
.method static a(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method public a()Lorg/apache/xml/security/signature/SignedInfo;
.registers 2
iget-object v0, p0, Lorg/apache/xml/security/signature/XMLSignature;->c:Lorg/apache/xml/security/signature/SignedInfo;
return-object v0
.end method
.method public a(Ljava/security/Key;)Z
.registers 8
const/4 v0, 0x0
if-nez p1, :cond_12
const/4 v1, 0x1
new-array v1, v1, [Ljava/lang/Object;
const-string v2, "Didn\'t get a key"
aput-object v2, v1, v0
new-instance v0, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v0, v2, v1}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
:cond_12
:try_start_12
invoke-virtual {p0}, Lorg/apache/xml/security/signature/XMLSignature;->a()Lorg/apache/xml/security/signature/SignedInfo;
move-result-object v2
invoke-virtual {v2}, Lorg/apache/xml/security/signature/SignedInfo;->c()Lorg/apache/xml/security/algorithms/SignatureAlgorithm;
move-result-object v3
sget-object v1, Lorg/apache/xml/security/signature/XMLSignature;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v1}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v1
if-eqz v1, :cond_8e
sget-object v1, Lorg/apache/xml/security/signature/XMLSignature;->a:Lorg/apache/commons/logging/Log;
new-instance v4, Ljava/lang/StringBuffer;
invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V
const-string v5, "SignatureMethodURI = "
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-virtual {v3}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->a()Ljava/lang/String;
move-result-object v5
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v4
invoke-interface {v1, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v1, Lorg/apache/xml/security/signature/XMLSignature;->a:Lorg/apache/commons/logging/Log;
new-instance v4, Ljava/lang/StringBuffer;
invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V
const-string v5, "jceSigAlgorithm    = "
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-virtual {v3}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->b()Ljava/lang/String;
move-result-object v5
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v4
invoke-interface {v1, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v1, Lorg/apache/xml/security/signature/XMLSignature;->a:Lorg/apache/commons/logging/Log;
new-instance v4, Ljava/lang/StringBuffer;
invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V
const-string v5, "jceSigProvider     = "
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-virtual {v3}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->c()Ljava/lang/String;
move-result-object v5
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v4
invoke-interface {v1, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v1, Lorg/apache/xml/security/signature/XMLSignature;->a:Lorg/apache/commons/logging/Log;
new-instance v4, Ljava/lang/StringBuffer;
invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V
const-string v5, "PublicKey = "
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v4
invoke-interface {v1, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:try_end_8e
.catch Lorg/apache/xml/security/signature/XMLSignatureException; {:try_start_12 .. :try_end_8e} :catch_b9
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_12 .. :try_end_8e} :catch_c0
:cond_8e
const/4 v1, 0x0
:try_start_8f
invoke-virtual {v3, p1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->a(Ljava/security/Key;)V
new-instance v4, Lorg/apache/xml/security/utils/SignerOutputStream;
invoke-direct {v4, v3}, Lorg/apache/xml/security/utils/SignerOutputStream;-><init>(Lorg/apache/xml/security/algorithms/SignatureAlgorithm;)V
new-instance v5, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;
invoke-direct {v5, v4}, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
invoke-virtual {v2, v5}, Lorg/apache/xml/security/signature/SignedInfo;->a(Ljava/io/OutputStream;)V
invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
invoke-virtual {p0}, Lorg/apache/xml/security/signature/XMLSignature;->b()[B
:try_end_a5
.catch Ljava/io/IOException; {:try_start_8f .. :try_end_a5} :catch_b4
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_8f .. :try_end_a5} :catch_bb
.catch Lorg/apache/xml/security/signature/XMLSignatureException; {:try_start_8f .. :try_end_a5} :catch_b9
move-result-object v1
:try_start_a6
:goto_a6
invoke-virtual {v3, v1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->b([B)Z
move-result v1
if-nez v1, :cond_c9
sget-object v1, Lorg/apache/xml/security/signature/XMLSignature;->a:Lorg/apache/commons/logging/Log;
const-string v2, "Signature verification failed."
invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V
:goto_b3
return v0
:catch_b4
move-exception v4
invoke-virtual {v3}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->f()V
:try_end_b8
.catch Lorg/apache/xml/security/signature/XMLSignatureException; {:try_start_a6 .. :try_end_b8} :catch_b9
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_a6 .. :try_end_b8} :catch_c0
goto :goto_a6
:catch_b9
move-exception v0
throw v0
:catch_bb
move-exception v0
:try_start_bc
invoke-virtual {v3}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->f()V
throw v0
:try_end_c0
.catch Lorg/apache/xml/security/signature/XMLSignatureException; {:try_start_bc .. :try_end_c0} :catch_b9
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_bc .. :try_end_c0} :catch_c0
:catch_c0
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:try_start_c9
:cond_c9
iget-boolean v0, p0, Lorg/apache/xml/security/signature/XMLSignature;->e:Z
invoke-virtual {v2, v0}, Lorg/apache/xml/security/signature/SignedInfo;->b(Z)Z
:try_end_ce
.catch Lorg/apache/xml/security/signature/XMLSignatureException; {:try_start_c9 .. :try_end_ce} :catch_b9
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_c9 .. :try_end_ce} :catch_c0
move-result v0
goto :goto_b3
.end method
.method public b()[B
.registers 4
:try_start_0
iget-object v0, p0, Lorg/apache/xml/security/signature/XMLSignature;->f:Lorg/w3c/dom/Element;
invoke-static {v0}, Lorg/apache/xml/security/utils/Base64;->a(Lorg/w3c/dom/Element;)[B
:try_end_5
.catch Lorg/apache/xml/security/exceptions/Base64DecodingException; {:try_start_0 .. :try_end_5} :catch_7
move-result-object v0
return-object v0
:catch_7
move-exception v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/signature/XMLSignatureException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method
.method public e()Ljava/lang/String;
.registers 2
const-string v0, "Signature"
return-object v0
.end method