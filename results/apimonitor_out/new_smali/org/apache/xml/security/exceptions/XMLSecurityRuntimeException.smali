.class public Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;
.super Ljava/lang/RuntimeException;
.field protected a:Ljava/lang/Exception;
.field protected b:Ljava/lang/String;
.method public constructor <init>()V
.registers 3
const/4 v1, 0x0
const-string v0, "Missing message string"
invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V
iput-object v1, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
iput-object v1, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->b:Ljava/lang/String;
iput-object v1, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
return-void
.end method
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Exception;)V
.registers 4
invoke-static {p1, p2}, Lorg/apache/xml/security/utils/I18n;->a(Ljava/lang/String;Ljava/lang/Exception;)Ljava/lang/String;
move-result-object v0
invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V
const/4 v0, 0x0
iput-object v0, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
iput-object p1, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->b:Ljava/lang/String;
iput-object p2, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
return-void
.end method
.method public constructor <init>(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V
.registers 5
invoke-static {p1}, Lorg/apache/xml/security/utils/I18n;->b(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
invoke-static {v0, p2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
move-result-object v0
invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V
const/4 v0, 0x0
iput-object v0, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
iput-object p1, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->b:Ljava/lang/String;
iput-object p3, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
return-void
.end method
.method public printStackTrace()V
.registers 4
sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;
monitor-enter v1
:try_start_3
sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;
invoke-super {p0, v0}, Ljava/lang/RuntimeException;->printStackTrace(Ljava/io/PrintStream;)V
iget-object v0, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
if-eqz v0, :cond_13
iget-object v0, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;
invoke-virtual {v0, v2}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V
:cond_13
monitor-exit v1
return-void
:catchall_15
move-exception v0
monitor-exit v1
:try_end_17
.catchall {:try_start_3 .. :try_end_17} :catchall_15
throw v0
.end method
.method public printStackTrace(Ljava/io/PrintStream;)V
.registers 3
invoke-super {p0, p1}, Ljava/lang/RuntimeException;->printStackTrace(Ljava/io/PrintStream;)V
iget-object v0, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
if-eqz v0, :cond_c
iget-object v0, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
invoke-virtual {v0, p1}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V
:cond_c
return-void
.end method
.method public printStackTrace(Ljava/io/PrintWriter;)V
.registers 3
invoke-super {p0, p1}, Ljava/lang/RuntimeException;->printStackTrace(Ljava/io/PrintWriter;)V
iget-object v0, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
if-eqz v0, :cond_c
iget-object v0, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
invoke-virtual {v0, p1}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V
:cond_c
return-void
.end method
.method public toString()Ljava/lang/String;
.registers 4
invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-super {p0}, Ljava/lang/RuntimeException;->getLocalizedMessage()Ljava/lang/String;
move-result-object v1
if-eqz v1, :cond_25
new-instance v2, Ljava/lang/StringBuffer;
invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V
invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
const-string v2, ": "
invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
:cond_25
iget-object v1, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
if-eqz v1, :cond_46
new-instance v1, Ljava/lang/StringBuffer;
invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
const-string v1, "\nOriginal Exception was "
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
iget-object v1, p0, Lorg/apache/xml/security/exceptions/XMLSecurityRuntimeException;->a:Ljava/lang/Exception;
invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
:cond_46
return-object v0
.end method