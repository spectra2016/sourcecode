.class public Lorg/apache/xml/security/utils/RFC2253Parser;
.super Ljava/lang/Object;
.field static a:Z
.field static b:I
.method static constructor <clinit>()V
.registers 1
const/4 v0, 0x1
sput-boolean v0, Lorg/apache/xml/security/utils/RFC2253Parser;->a:Z
const/4 v0, 0x0
sput v0, Lorg/apache/xml/security/utils/RFC2253Parser;->b:I
return-void
.end method
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method private static a(Ljava/lang/String;II)I
.registers 6
const/4 v0, 0x0
:goto_1
if-ge p1, p2, :cond_10
invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C
move-result v1
const/16 v2, 0x22
if-ne v1, v2, :cond_d
add-int/lit8 v0, v0, 0x1
:cond_d
add-int/lit8 p1, p1, 0x1
goto :goto_1
:cond_10
return v0
.end method
.method public static a(Ljava/lang/String;)Ljava/lang/String;
.registers 10
const/4 v1, 0x0
if-eqz p0, :cond_b
const-string v0, ""
invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_e
:cond_b
const-string p0, ""
:goto_d
return-object p0
:cond_e
:try_start_e
invoke-static {p0}, Lorg/apache/xml/security/utils/RFC2253Parser;->f(Ljava/lang/String;)Ljava/lang/String;
move-result-object v4
new-instance v5, Ljava/lang/StringBuffer;
invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V
move v0, v1
move v3, v1
move v2, v1
:goto_1a
const-string v6, ","
invoke-virtual {v4, v6, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I
move-result v6
if-ltz v6, :cond_64
invoke-static {v4, v0, v6}, Lorg/apache/xml/security/utils/RFC2253Parser;->a(Ljava/lang/String;II)I
move-result v0
add-int/2addr v0, v3
if-lez v6, :cond_5e
add-int/lit8 v3, v6, -0x1
invoke-virtual {v4, v3}, Ljava/lang/String;->charAt(I)C
move-result v3
const/16 v7, 0x5c
if-eq v3, v7, :cond_5e
rem-int/lit8 v3, v0, 0x2
const/4 v7, 0x1
if-eq v3, v7, :cond_5e
new-instance v0, Ljava/lang/StringBuffer;
invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V
invoke-virtual {v4, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
move-result-object v2
invoke-static {v2}, Lorg/apache/xml/security/utils/RFC2253Parser;->b(Ljava/lang/String;)Ljava/lang/String;
move-result-object v2
invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
const-string v2, ","
invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
add-int/lit8 v0, v6, 0x1
move v2, v0
move v0, v1
:cond_5e
add-int/lit8 v3, v6, 0x1
move v8, v3
move v3, v0
move v0, v8
goto :goto_1a
:cond_64
invoke-virtual {v4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/RFC2253Parser;->g(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/RFC2253Parser;->b(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
:try_end_76
.catch Ljava/io/IOException; {:try_start_e .. :try_end_76} :catch_78
move-result-object p0
goto :goto_d
:catch_78
move-exception v0
goto :goto_d
.end method
.method static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.registers 10
const/4 v1, 0x0
new-instance v4, Ljava/lang/StringBuffer;
invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V
move v0, v1
move v2, v1
move v3, v1
:goto_9
invoke-virtual {p0, p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I
move-result v5
if-ltz v5, :cond_47
invoke-static {p0, v0, v5}, Lorg/apache/xml/security/utils/RFC2253Parser;->a(Ljava/lang/String;II)I
move-result v0
add-int/2addr v2, v0
if-lez v5, :cond_44
add-int/lit8 v0, v5, -0x1
invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C
move-result v0
const/16 v6, 0x5c
if-eq v0, v6, :cond_44
rem-int/lit8 v0, v2, 0x2
const/4 v6, 0x1
if-eq v0, v6, :cond_44
new-instance v0, Ljava/lang/StringBuffer;
invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V
invoke-virtual {p0, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
move-result-object v2
invoke-static {v2}, Lorg/apache/xml/security/utils/RFC2253Parser;->g(Ljava/lang/String;)Ljava/lang/String;
move-result-object v2
invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
add-int/lit8 v3, v5, 0x1
move v2, v1
:cond_44
add-int/lit8 v0, v5, 0x1
goto :goto_9
:cond_47
invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/RFC2253Parser;->g(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method static b(Ljava/lang/String;)Ljava/lang/String;
.registers 8
const/4 v1, 0x0
new-instance v4, Ljava/lang/StringBuffer;
invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V
move v0, v1
move v2, v1
move v3, v1
:goto_9
const-string v5, "+"
invoke-virtual {p0, v5, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I
move-result v5
if-ltz v5, :cond_4f
invoke-static {p0, v0, v5}, Lorg/apache/xml/security/utils/RFC2253Parser;->a(Ljava/lang/String;II)I
move-result v0
add-int/2addr v2, v0
if-lez v5, :cond_4c
add-int/lit8 v0, v5, -0x1
invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C
move-result v0
const/16 v6, 0x5c
if-eq v0, v6, :cond_4c
rem-int/lit8 v0, v2, 0x2
const/4 v6, 0x1
if-eq v0, v6, :cond_4c
new-instance v0, Ljava/lang/StringBuffer;
invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V
invoke-virtual {p0, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
move-result-object v2
invoke-static {v2}, Lorg/apache/xml/security/utils/RFC2253Parser;->g(Ljava/lang/String;)Ljava/lang/String;
move-result-object v2
invoke-static {v2}, Lorg/apache/xml/security/utils/RFC2253Parser;->c(Ljava/lang/String;)Ljava/lang/String;
move-result-object v2
invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
const-string v2, "+"
invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
add-int/lit8 v3, v5, 0x1
move v2, v1
:cond_4c
add-int/lit8 v0, v5, 0x1
goto :goto_9
:cond_4f
invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/RFC2253Parser;->g(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/RFC2253Parser;->c(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method static c(Ljava/lang/String;)Ljava/lang/String;
.registers 6
const/4 v4, 0x0
const-string v0, "="
invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
move-result v0
const/4 v1, -0x1
if-eq v0, v1, :cond_16
if-lez v0, :cond_17
add-int/lit8 v1, v0, -0x1
invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C
move-result v1
const/16 v2, 0x5c
if-ne v1, v2, :cond_17
:cond_16
:goto_16
return-object p0
:cond_17
invoke-virtual {p0, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;
move-result-object v1
invoke-static {v1}, Lorg/apache/xml/security/utils/RFC2253Parser;->d(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C
move-result v2
const/16 v3, 0x30
if-lt v2, v3, :cond_4d
invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C
move-result v2
const/16 v3, 0x39
if-gt v2, v3, :cond_4d
add-int/lit8 v0, v0, 0x1
invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;
move-result-object v0
:goto_35
new-instance v2, Ljava/lang/StringBuffer;
invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V
invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
const-string v2, "="
invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object p0
goto :goto_16
:cond_4d
add-int/lit8 v0, v0, 0x1
invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/RFC2253Parser;->e(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
goto :goto_35
.end method
.method static d(Ljava/lang/String;)Ljava/lang/String;
.registers 3
invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
move-result-object v0
const-string v1, "OID"
invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
move-result v1
if-eqz v1, :cond_15
const/4 v1, 0x3
invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
move-result-object v0
:cond_15
return-object v0
.end method
.method static e(Ljava/lang/String;)Ljava/lang/String;
.registers 7
const/16 v5, 0x5c
const/4 v4, 0x1
invoke-static {p0}, Lorg/apache/xml/security/utils/RFC2253Parser;->g(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
const-string v1, "\""
invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
move-result v1
if-eqz v1, :cond_56
new-instance v1, Ljava/lang/StringBuffer;
invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V
new-instance v2, Ljava/io/StringReader;
invoke-virtual {v0}, Ljava/lang/String;->length()I
move-result v3
add-int/lit8 v3, v3, -0x1
invoke-virtual {v0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
move-result-object v0
invoke-direct {v2, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
:goto_23
invoke-virtual {v2}, Ljava/io/StringReader;->read()I
move-result v0
const/4 v3, -0x1
if-le v0, v3, :cond_4e
int-to-char v0, v0
const/16 v3, 0x2c
if-eq v0, v3, :cond_47
const/16 v3, 0x3d
if-eq v0, v3, :cond_47
const/16 v3, 0x2b
if-eq v0, v3, :cond_47
const/16 v3, 0x3c
if-eq v0, v3, :cond_47
const/16 v3, 0x3e
if-eq v0, v3, :cond_47
const/16 v3, 0x23
if-eq v0, v3, :cond_47
const/16 v3, 0x3b
if-ne v0, v3, :cond_4a
:cond_47
invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
:cond_4a
invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
goto :goto_23
:cond_4e
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/RFC2253Parser;->g(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
:cond_56
sget-boolean v1, Lorg/apache/xml/security/utils/RFC2253Parser;->a:Z
if-ne v1, v4, :cond_74
const-string v1, "#"
invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
move-result v1
if-eqz v1, :cond_73
new-instance v1, Ljava/lang/StringBuffer;
invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V
invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
:goto_73
:cond_73
return-object v0
:cond_74
const-string v1, "\\#"
invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
move-result v1
if-eqz v1, :cond_73
invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;
move-result-object v0
goto :goto_73
.end method
.method static f(Ljava/lang/String;)Ljava/lang/String;
.registers 3
const-string v0, ";"
const-string v1, ","
invoke-static {p0, v0, v1}, Lorg/apache/xml/security/utils/RFC2253Parser;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method static g(Ljava/lang/String;)Ljava/lang/String;
.registers 4
invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;
move-result-object v0
invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
move-result v1
invoke-virtual {v0}, Ljava/lang/String;->length()I
move-result v2
add-int/2addr v1, v2
invoke-virtual {p0}, Ljava/lang/String;->length()I
move-result v2
if-le v2, v1, :cond_3e
const-string v2, "\\"
invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
move-result v2
if-eqz v2, :cond_3e
const-string v2, "\\\\"
invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
move-result v2
if-nez v2, :cond_3e
invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C
move-result v1
const/16 v2, 0x20
if-ne v1, v2, :cond_3e
new-instance v1, Ljava/lang/StringBuffer;
invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
const-string v1, " "
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
:cond_3e
return-object v0
.end method