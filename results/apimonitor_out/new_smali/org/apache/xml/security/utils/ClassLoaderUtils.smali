.class public final Lorg/apache/xml/security/utils/ClassLoaderUtils;
.super Ljava/lang/Object;
.field static a:Ljava/lang/Class;
.method private constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method static a(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method public static a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Thread;->getContextClassLoader()Ljava/lang/ClassLoader;
move-result-object v0
if-eqz v0, :cond_10
invoke-virtual {v0, p0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
:try_end_d
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_d} :catch_f
move-result-object v0
:goto_e
return-object v0
:catch_f
move-exception v0
:cond_10
invoke-static {p0, p1}, Lorg/apache/xml/security/utils/ClassLoaderUtils;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Class;
move-result-object v0
goto :goto_e
.end method
.method private static b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Class;
.registers 4
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
:goto_4
return-object v0
:catch_5
move-exception v1
:try_start_6
sget-object v0, Lorg/apache/xml/security/utils/ClassLoaderUtils;->a:Ljava/lang/Class;
if-nez v0, :cond_2d
const-string v0, "org.apache.xml.security.utils.ClassLoaderUtils"
invoke-static {v0}, Lorg/apache/xml/security/utils/ClassLoaderUtils;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/utils/ClassLoaderUtils;->a:Ljava/lang/Class;
:goto_12
invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;
move-result-object v0
if-eqz v0, :cond_45
sget-object v0, Lorg/apache/xml/security/utils/ClassLoaderUtils;->a:Ljava/lang/Class;
if-nez v0, :cond_30
const-string v0, "org.apache.xml.security.utils.ClassLoaderUtils"
invoke-static {v0}, Lorg/apache/xml/security/utils/ClassLoaderUtils;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/utils/ClassLoaderUtils;->a:Ljava/lang/Class;
:goto_24
invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;
move-result-object v0
invoke-virtual {v0, p0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
goto :goto_4
:cond_2d
sget-object v0, Lorg/apache/xml/security/utils/ClassLoaderUtils;->a:Ljava/lang/Class;
goto :goto_12
:cond_30
sget-object v0, Lorg/apache/xml/security/utils/ClassLoaderUtils;->a:Ljava/lang/Class;
:try_end_32
.catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_32} :catch_33
goto :goto_24
:catch_33
move-exception v0
if-eqz p1, :cond_45
invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;
move-result-object v0
if-eqz v0, :cond_45
invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;
move-result-object v0
invoke-virtual {v0, p0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
goto :goto_4
:cond_45
throw v1
.end method