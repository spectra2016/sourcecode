.class public Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;
.super Ljava/io/OutputStream;
.field private static d:Ljava/lang/ThreadLocal;
.field final a:Ljava/io/OutputStream;
.field final b:[B
.field  c:I
.method static constructor <clinit>()V
.registers 1
new-instance v0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream$1;
invoke-direct {v0}, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream$1;-><init>()V
sput-object v0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->d:Ljava/lang/ThreadLocal;
return-void
.end method
.method public constructor <init>(Ljava/io/OutputStream;)V
.registers 3
invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V
const/4 v0, 0x0
iput v0, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->c:I
sget-object v0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->d:Ljava/lang/ThreadLocal;
invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
move-result-object v0
check-cast v0, [B
iput-object v0, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->b:[B
iput-object p1, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->a:Ljava/io/OutputStream;
return-void
.end method
.method private final a()V
.registers 5
const/4 v3, 0x0
iget v0, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->c:I
if-lez v0, :cond_e
iget-object v0, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->a:Ljava/io/OutputStream;
iget-object v1, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->b:[B
iget v2, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->c:I
invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V
:cond_e
iput v3, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->c:I
return-void
.end method
.method public close()V
.registers 1
invoke-virtual {p0}, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->flush()V
return-void
.end method
.method public flush()V
.registers 2
invoke-direct {p0}, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->a()V
iget-object v0, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->a:Ljava/io/OutputStream;
invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
return-void
.end method
.method public write(I)V
.registers 5
iget v0, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->c:I
const/16 v1, 0x2000
if-lt v0, v1, :cond_9
invoke-direct {p0}, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->a()V
:cond_9
iget-object v0, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->b:[B
iget v1, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->c:I
add-int/lit8 v2, v1, 0x1
iput v2, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->c:I
int-to-byte v2, p1
aput-byte v2, v0, v1
return-void
.end method
.method public write([B)V
.registers 4
const/4 v0, 0x0
array-length v1, p1
invoke-virtual {p0, p1, v0, v1}, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->write([BII)V
return-void
.end method
.method public write([BII)V
.registers 7
const/16 v1, 0x2000
iget v0, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->c:I
add-int/2addr v0, p3
if-le v0, v1, :cond_13
invoke-direct {p0}, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->a()V
if-le p3, v1, :cond_12
iget-object v0, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->a:Ljava/io/OutputStream;
invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
:goto_11
return-void
:cond_12
move v0, p3
:cond_13
iget-object v1, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->b:[B
iget v2, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->c:I
invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iput v0, p0, Lorg/apache/xml/security/utils/UnsyncBufferedOutputStream;->c:I
goto :goto_11
.end method