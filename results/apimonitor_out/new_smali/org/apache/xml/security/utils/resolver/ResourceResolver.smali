.class public Lorg/apache/xml/security/utils/resolver/ResourceResolver;
.super Ljava/lang/Object;
.field static a:Lorg/apache/commons/logging/Log;
.field static b:Z
.field static c:Ljava/util/List;
.field static d:Z
.field static f:Ljava/lang/Class;
.field protected e:Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
.method static constructor <clinit>()V
.registers 1
sget-object v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->f:Ljava/lang/Class;
if-nez v0, :cond_20
const-string v0, "org.apache.xml.security.utils.resolver.ResourceResolver"
invoke-static {v0}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->b(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->f:Ljava/lang/Class;
:goto_c
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a:Lorg/apache/commons/logging/Log;
const/4 v0, 0x0
sput-boolean v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->b:Z
const/4 v0, 0x0
sput-object v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->c:Ljava/util/List;
const/4 v0, 0x1
sput-boolean v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->d:Z
return-void
:cond_20
sget-object v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->f:Ljava/lang/Class;
goto :goto_c
.end method
.method private constructor <init>(Ljava/lang/String;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, 0x0
iput-object v0, p0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->e:Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
iput-object v0, p0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->e:Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
return-void
.end method
.method public constructor <init>(Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, 0x0
iput-object v0, p0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->e:Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
iput-object p1, p0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->e:Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
return-void
.end method
.method public static final a(Lorg/w3c/dom/Attr;Ljava/lang/String;)Lorg/apache/xml/security/utils/resolver/ResourceResolver;
.registers 10
const/4 v4, 0x0
sget-object v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->c:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->size()I
move-result v5
move v3, v4
:goto_8
if-ge v3, v5, :cond_8f
sget-object v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->c:Ljava/util/List;
invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;
:try_start_12
sget-boolean v1, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->d:Z
if-nez v1, :cond_1e
iget-object v1, v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->e:Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
invoke-virtual {v1}, Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;->a()Z
:try_end_1b
.catch Ljava/lang/InstantiationException; {:try_start_12 .. :try_end_1b} :catch_78
.catch Ljava/lang/IllegalAccessException; {:try_start_12 .. :try_end_1b} :catch_81
move-result v1
if-eqz v1, :cond_66
:cond_1e
move-object v2, v0
:goto_1f
sget-object v1, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v1}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v1
if-eqz v1, :cond_49
sget-object v1, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a:Lorg/apache/commons/logging/Log;
new-instance v6, Ljava/lang/StringBuffer;
invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V
const-string v7, "check resolvability by class "
invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v6
iget-object v7, v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->e:Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v7
invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v7
invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v6
invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v6
invoke-interface {v1, v6}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_49
if-eqz v0, :cond_8a
invoke-direct {v2, p0, p1}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->c(Lorg/w3c/dom/Attr;Ljava/lang/String;)Z
move-result v1
if-eqz v1, :cond_8a
if-eqz v3, :cond_65
sget-object v1, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->c:Ljava/util/List;
check-cast v1, Ljava/util/ArrayList;
invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;
move-result-object v1
check-cast v1, Ljava/util/List;
invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;
invoke-interface {v1, v4, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V
sput-object v1, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->c:Ljava/util/List;
:cond_65
return-object v2
:cond_66
:try_start_66
new-instance v2, Lorg/apache/xml/security/utils/resolver/ResourceResolver;
iget-object v1, v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->e:Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
move-result-object v1
check-cast v1, Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
invoke-direct {v2, v1}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;-><init>(Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;)V
:try_end_77
.catch Ljava/lang/InstantiationException; {:try_start_66 .. :try_end_77} :catch_78
.catch Ljava/lang/IllegalAccessException; {:try_start_66 .. :try_end_77} :catch_81
goto :goto_1f
:catch_78
move-exception v0
new-instance v1, Lorg/apache/xml/security/utils/resolver/ResourceResolverException;
const-string v2, ""
invoke-direct {v1, v2, v0, p0, p1}, Lorg/apache/xml/security/utils/resolver/ResourceResolverException;-><init>(Ljava/lang/String;Ljava/lang/Exception;Lorg/w3c/dom/Attr;Ljava/lang/String;)V
throw v1
:catch_81
move-exception v0
new-instance v1, Lorg/apache/xml/security/utils/resolver/ResourceResolverException;
const-string v2, ""
invoke-direct {v1, v2, v0, p0, p1}, Lorg/apache/xml/security/utils/resolver/ResourceResolverException;-><init>(Ljava/lang/String;Ljava/lang/Exception;Lorg/w3c/dom/Attr;Ljava/lang/String;)V
throw v1
:cond_8a
add-int/lit8 v0, v3, 0x1
move v3, v0
goto/16 :goto_8
:cond_8f
const/4 v0, 0x2
new-array v1, v0, [Ljava/lang/Object;
if-eqz p0, :cond_a5
invoke-interface {p0}, Lorg/w3c/dom/Attr;->getNodeValue()Ljava/lang/String;
move-result-object v0
:goto_98
aput-object v0, v1, v4
const/4 v0, 0x1
aput-object p1, v1, v0
new-instance v0, Lorg/apache/xml/security/utils/resolver/ResourceResolverException;
const-string v2, "utils.resolver.noClass"
invoke-direct {v0, v2, v1, p0, p1}, Lorg/apache/xml/security/utils/resolver/ResourceResolverException;-><init>(Ljava/lang/String;[Ljava/lang/Object;Lorg/w3c/dom/Attr;Ljava/lang/String;)V
throw v0
:cond_a5
const-string v0, "null"
goto :goto_98
.end method
.method public static final a(Lorg/w3c/dom/Attr;Ljava/lang/String;Ljava/util/List;)Lorg/apache/xml/security/utils/resolver/ResourceResolver;
.registers 10
const/4 v1, 0x0
sget-object v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v0}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v0
if-eqz v0, :cond_48
sget-object v2, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a:Lorg/apache/commons/logging/Log;
new-instance v0, Ljava/lang/StringBuffer;
invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V
const-string v3, "I was asked to create a ResourceResolver and got "
invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v3
if-nez p2, :cond_8b
move v0, v1
:goto_19
invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
invoke-interface {v2, v0}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a:Lorg/apache/commons/logging/Log;
new-instance v2, Ljava/lang/StringBuffer;
invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V
const-string v3, " extra resolvers to my existing "
invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
sget-object v3, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->c:Ljava/util/List;
invoke-interface {v3}, Ljava/util/List;->size()I
move-result v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v2
const-string v3, " system-wide resolvers"
invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v2
invoke-interface {v0, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_48
if-eqz p2, :cond_93
invoke-interface {p2}, Ljava/util/List;->size()I
move-result v2
if-lez v2, :cond_93
:goto_50
if-ge v1, v2, :cond_93
invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;
if-eqz v0, :cond_90
iget-object v3, v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->e:Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v3
sget-object v4, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v4}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v4
if-eqz v4, :cond_84
sget-object v4, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a:Lorg/apache/commons/logging/Log;
new-instance v5, Ljava/lang/StringBuffer;
invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V
const-string v6, "check resolvability by class "
invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v3
invoke-interface {v4, v3}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_84
invoke-direct {v0, p0, p1}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->c(Lorg/w3c/dom/Attr;Ljava/lang/String;)Z
move-result v3
if-eqz v3, :cond_90
:goto_8a
return-object v0
:cond_8b
invoke-interface {p2}, Ljava/util/List;->size()I
move-result v0
goto :goto_19
:cond_90
add-int/lit8 v1, v1, 0x1
goto :goto_50
:cond_93
invoke-static {p0, p1}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a(Lorg/w3c/dom/Attr;Ljava/lang/String;)Lorg/apache/xml/security/utils/resolver/ResourceResolver;
move-result-object v0
goto :goto_8a
.end method
.method public static a()V
.registers 2
sget-boolean v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->b:Z
if-nez v0, :cond_10
new-instance v0, Ljava/util/ArrayList;
const/16 v1, 0xa
invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V
sput-object v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->c:Ljava/util/List;
const/4 v0, 0x1
sput-boolean v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->b:Z
:cond_10
return-void
.end method
.method public static a(Ljava/lang/String;)V
.registers 2
const/4 v0, 0x0
invoke-static {p0, v0}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a(Ljava/lang/String;Z)V
return-void
.end method
.method private static a(Ljava/lang/String;Z)V
.registers 5
:try_start_0
new-instance v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;
invoke-direct {v0, p0}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;-><init>(Ljava/lang/String;)V
if-eqz p1, :cond_20
sget-object v1, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->c:Ljava/util/List;
const/4 v2, 0x0
invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V
sget-object v1, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a:Lorg/apache/commons/logging/Log;
const-string v2, "registered resolver"
invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:goto_14
iget-object v0, v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->e:Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
invoke-virtual {v0}, Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;->a()Z
move-result v0
if-nez v0, :cond_1f
const/4 v0, 0x0
sput-boolean v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->d:Z
:goto_1f
:cond_1f
return-void
:cond_20
sget-object v1, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->c:Ljava/util/List;
invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
:try_end_25
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_25} :catch_26
.catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_25} :catch_46
goto :goto_14
:catch_26
move-exception v0
sget-object v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a:Lorg/apache/commons/logging/Log;
new-instance v1, Ljava/lang/StringBuffer;
invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V
const-string v2, "Error loading resolver "
invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
const-string v2, " disabling it"
invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v1
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V
goto :goto_1f
:catch_46
move-exception v0
sget-object v0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a:Lorg/apache/commons/logging/Log;
new-instance v1, Ljava/lang/StringBuffer;
invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V
const-string v2, "Error loading resolver "
invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
const-string v2, " disabling it"
invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v1
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V
goto :goto_1f
.end method
.method static b(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method private c(Lorg/w3c/dom/Attr;Ljava/lang/String;)Z
.registers 4
iget-object v0, p0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->e:Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
invoke-virtual {v0, p1, p2}, Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;->b(Lorg/w3c/dom/Attr;Ljava/lang/String;)Z
move-result v0
return v0
.end method
.method public a(Ljava/util/Map;)V
.registers 3
iget-object v0, p0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->e:Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
invoke-virtual {v0, p1}, Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;->a(Ljava/util/Map;)V
return-void
.end method
.method public b(Lorg/w3c/dom/Attr;Ljava/lang/String;)Lorg/apache/xml/security/signature/XMLSignatureInput;
.registers 4
iget-object v0, p0, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->e:Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
invoke-virtual {v0, p1, p2}, Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;->a(Lorg/w3c/dom/Attr;Ljava/lang/String;)Lorg/apache/xml/security/signature/XMLSignatureInput;
move-result-object v0
return-object v0
.end method