.class public Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;
.super Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;
.field static d:Lorg/apache/commons/logging/Log;
.field static e:Ljava/lang/Class;
.field private static final f:[Ljava/lang/String;
.method static constructor <clinit>()V
.registers 3
sget-object v0, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->e:Ljava/lang/Class;
if-nez v0, :cond_3a
const-string v0, "org.apache.xml.security.utils.resolver.implementations.ResolverDirectHTTP"
invoke-static {v0}, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->c(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->e:Ljava/lang/Class;
:goto_c
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->d:Lorg/apache/commons/logging/Log;
const/4 v0, 0x6
new-array v0, v0, [Ljava/lang/String;
const/4 v1, 0x0
const-string v2, "http.proxy.host"
aput-object v2, v0, v1
const/4 v1, 0x1
const-string v2, "http.proxy.port"
aput-object v2, v0, v1
const/4 v1, 0x2
const-string v2, "http.proxy.username"
aput-object v2, v0, v1
const/4 v1, 0x3
const-string v2, "http.proxy.password"
aput-object v2, v0, v1
const/4 v1, 0x4
const-string v2, "http.basic.username"
aput-object v2, v0, v1
const/4 v1, 0x5
const-string v2, "http.basic.password"
aput-object v2, v0, v1
sput-object v0, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->f:[Ljava/lang/String;
return-void
:cond_3a
sget-object v0, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->e:Ljava/lang/Class;
goto :goto_c
.end method
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Lorg/apache/xml/security/utils/resolver/ResourceResolverSpi;-><init>()V
return-void
.end method
.method private a(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/xml/utils/URI;
.registers 5
if-eqz p2, :cond_a
const-string v0, ""
invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_10
:cond_a
new-instance v0, Lorg/apache/xml/utils/URI;
invoke-direct {v0, p1}, Lorg/apache/xml/utils/URI;-><init>(Ljava/lang/String;)V
:goto_f
return-object v0
:cond_10
new-instance v0, Lorg/apache/xml/utils/URI;
new-instance v1, Lorg/apache/xml/utils/URI;
invoke-direct {v1, p2}, Lorg/apache/xml/utils/URI;-><init>(Ljava/lang/String;)V
invoke-direct {v0, v1, p1}, Lorg/apache/xml/utils/URI;-><init>(Lorg/apache/xml/utils/URI;Ljava/lang/String;)V
goto :goto_f
.end method
.method static c(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method public a(Lorg/w3c/dom/Attr;Ljava/lang/String;)Lorg/apache/xml/security/signature/XMLSignatureInput;
.registers 16
const/4 v1, 0x1
const/4 v2, 0x0
const/4 v0, 0x0
:try_start_3
sget-object v3, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->f:[Ljava/lang/String;
const/4 v4, 0x0
aget-object v3, v3, v4
invoke-virtual {p0, v3}, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v5
sget-object v3, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->f:[Ljava/lang/String;
const/4 v4, 0x1
aget-object v3, v3, v4
invoke-virtual {p0, v3}, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v7
if-eqz v5, :cond_1b2
if-eqz v7, :cond_1b2
move v6, v1
:goto_1a
if-eqz v6, :cond_1ad
sget-object v2, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->d:Lorg/apache/commons/logging/Log;
invoke-interface {v2}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v2
if-eqz v2, :cond_46
sget-object v2, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->d:Lorg/apache/commons/logging/Log;
new-instance v3, Ljava/lang/StringBuffer;
invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V
const-string v4, "Use of HTTP proxy enabled: "
invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v3
invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v3
const-string v4, ":"
invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v3
invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v3
invoke-interface {v2, v3}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_46
const-string v2, "http.proxySet"
invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;
move-result-object v4
const-string v2, "http.proxyHost"
invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;
move-result-object v3
const-string v2, "http.proxyPort"
invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;
move-result-object v2
const-string v8, "http.proxySet"
const-string v9, "true"
invoke-static {v8, v9}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
const-string v8, "http.proxyHost"
invoke-static {v8, v5}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
const-string v5, "http.proxyPort"
invoke-static {v5, v7}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-object v5, v4
move-object v4, v3
move-object v3, v2
:goto_6c
if-eqz v5, :cond_14b
if-eqz v4, :cond_14b
if-eqz v3, :cond_14b
move v2, v1
:goto_73
invoke-interface {p1}, Lorg/w3c/dom/Attr;->getNodeValue()Ljava/lang/String;
move-result-object v1
invoke-direct {p0, v1, p2}, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->a(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/xml/utils/URI;
move-result-object v7
new-instance v1, Lorg/apache/xml/utils/URI;
invoke-direct {v1, v7}, Lorg/apache/xml/utils/URI;-><init>(Lorg/apache/xml/utils/URI;)V
const/4 v8, 0x0
invoke-virtual {v1, v8}, Lorg/apache/xml/utils/URI;->setFragment(Ljava/lang/String;)V
new-instance v8, Ljava/net/URL;
invoke-virtual {v1}, Lorg/apache/xml/utils/URI;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v8, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
invoke-virtual {v8}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;
move-result-object v1
sget-object v9, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->f:[Ljava/lang/String;
const/4 v10, 0x2
aget-object v9, v9, v10
invoke-virtual {p0, v9}, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v9
sget-object v10, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->f:[Ljava/lang/String;
const/4 v11, 0x3
aget-object v10, v10, v11
invoke-virtual {p0, v10}, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v10
if-eqz v9, :cond_cb
if-eqz v10, :cond_cb
new-instance v11, Ljava/lang/StringBuffer;
invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V
invoke-virtual {v11, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v9
const-string v11, ":"
invoke-virtual {v9, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v9
invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v9
invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v9
invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B
move-result-object v9
invoke-static {v9}, Lorg/apache/xml/security/utils/Base64;->b([B)Ljava/lang/String;
move-result-object v9
const-string v10, "Proxy-Authorization"
invoke-virtual {v1, v10, v9}, Ljava/net/URLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
:cond_cb
const-string v9, "WWW-Authenticate"
invoke-virtual {v1, v9}, Ljava/net/URLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;
move-result-object v9
if-eqz v9, :cond_12c
const-string v10, "Basic"
invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
move-result v9
if-eqz v9, :cond_12c
sget-object v9, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->f:[Ljava/lang/String;
const/4 v10, 0x4
aget-object v9, v9, v10
invoke-virtual {p0, v9}, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v9
sget-object v10, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->f:[Ljava/lang/String;
const/4 v11, 0x5
aget-object v10, v10, v11
invoke-virtual {p0, v10}, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v10
if-eqz v9, :cond_12c
if-eqz v10, :cond_12c
invoke-virtual {v8}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;
move-result-object v1
new-instance v8, Ljava/lang/StringBuffer;
invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V
invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v8
const-string v9, ":"
invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v8
invoke-virtual {v8, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v8
invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v8
invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B
move-result-object v8
invoke-static {v8}, Lorg/apache/xml/security/utils/Base64;->b([B)Ljava/lang/String;
move-result-object v8
const-string v9, "Authorization"
new-instance v10, Ljava/lang/StringBuffer;
invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V
const-string v11, "Basic "
invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v10
invoke-virtual {v10, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v8
invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v8
invoke-virtual {v1, v9, v8}, Ljava/net/URLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
:cond_12c
const-string v8, "Content-Type"
invoke-virtual {v1, v8}, Ljava/net/URLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;
move-result-object v8
invoke-virtual {v1}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;
move-result-object v1
new-instance v9, Ljava/io/ByteArrayOutputStream;
invoke-direct {v9}, Ljava/io/ByteArrayOutputStream;-><init>()V
const/16 v10, 0x1000
new-array v10, v10, [B
:goto_13f
invoke-virtual {v1, v10}, Ljava/io/InputStream;->read([B)I
move-result v11
if-ltz v11, :cond_14e
const/4 v12, 0x0
invoke-virtual {v9, v10, v12, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V
add-int/2addr v0, v11
goto :goto_13f
:cond_14b
move v2, v0
goto/16 :goto_73
:cond_14e
sget-object v1, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->d:Lorg/apache/commons/logging/Log;
new-instance v10, Ljava/lang/StringBuffer;
invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V
const-string v11, "Fetched "
invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v10
invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v0
const-string v10, " bytes from URI "
invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v7}, Lorg/apache/xml/utils/URI;->toString()Ljava/lang/String;
move-result-object v10
invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
invoke-interface {v1, v0}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
new-instance v0, Lorg/apache/xml/security/signature/XMLSignatureInput;
invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
move-result-object v1
invoke-direct {v0, v1}, Lorg/apache/xml/security/signature/XMLSignatureInput;-><init>([B)V
invoke-virtual {v7}, Lorg/apache/xml/utils/URI;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->b(Ljava/lang/String;)V
invoke-virtual {v0, v8}, Lorg/apache/xml/security/signature/XMLSignatureInput;->a(Ljava/lang/String;)V
if-eqz v6, :cond_19a
if-eqz v2, :cond_19a
const-string v1, "http.proxySet"
invoke-static {v1, v5}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
const-string v1, "http.proxyHost"
invoke-static {v1, v4}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
const-string v1, "http.proxyPort"
invoke-static {v1, v3}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
:try_end_19a
.catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_19a} :catch_19b
.catch Ljava/io/IOException; {:try_start_3 .. :try_end_19a} :catch_1a4
:cond_19a
return-object v0
:catch_19b
move-exception v0
new-instance v1, Lorg/apache/xml/security/utils/resolver/ResourceResolverException;
const-string v2, "generic.EmptyMessage"
invoke-direct {v1, v2, v0, p1, p2}, Lorg/apache/xml/security/utils/resolver/ResourceResolverException;-><init>(Ljava/lang/String;Ljava/lang/Exception;Lorg/w3c/dom/Attr;Ljava/lang/String;)V
throw v1
:catch_1a4
move-exception v0
new-instance v1, Lorg/apache/xml/security/utils/resolver/ResourceResolverException;
const-string v2, "generic.EmptyMessage"
invoke-direct {v1, v2, v0, p1, p2}, Lorg/apache/xml/security/utils/resolver/ResourceResolverException;-><init>(Ljava/lang/String;Ljava/lang/Exception;Lorg/w3c/dom/Attr;Ljava/lang/String;)V
throw v1
:cond_1ad
move-object v3, v2
move-object v4, v2
move-object v5, v2
goto/16 :goto_6c
:cond_1b2
move v6, v0
goto/16 :goto_1a
.end method
.method public a()Z
.registers 2
const/4 v0, 0x1
return v0
.end method
.method public b(Lorg/w3c/dom/Attr;Ljava/lang/String;)Z
.registers 8
const/4 v0, 0x0
if-nez p1, :cond_b
sget-object v1, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->d:Lorg/apache/commons/logging/Log;
const-string v2, "quick fail, uri == null"
invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_a
:goto_a
return v0
:cond_b
invoke-interface {p1}, Lorg/w3c/dom/Attr;->getNodeValue()Ljava/lang/String;
move-result-object v1
const-string v2, ""
invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v2
if-nez v2, :cond_1f
invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C
move-result v2
const/16 v3, 0x23
if-ne v2, v3, :cond_27
:cond_1f
sget-object v1, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->d:Lorg/apache/commons/logging/Log;
const-string v2, "quick fail for empty URIs and local ones"
invoke-interface {v1, v2}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
goto :goto_a
:cond_27
sget-object v2, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->d:Lorg/apache/commons/logging/Log;
invoke-interface {v2}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v2
if-eqz v2, :cond_47
sget-object v2, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->d:Lorg/apache/commons/logging/Log;
new-instance v3, Ljava/lang/StringBuffer;
invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V
const-string v4, "I was asked whether I can resolve "
invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v3
invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v3
invoke-interface {v2, v3}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_47
const-string v2, "http:"
invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
move-result v2
if-nez v2, :cond_59
if-eqz p2, :cond_7b
const-string v2, "http:"
invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
move-result v2
if-eqz v2, :cond_7b
:cond_59
sget-object v0, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->d:Lorg/apache/commons/logging/Log;
invoke-interface {v0}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v0
if-eqz v0, :cond_79
sget-object v0, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->d:Lorg/apache/commons/logging/Log;
new-instance v2, Ljava/lang/StringBuffer;
invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V
const-string v3, "I state that I can resolve "
invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v2
invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v1
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_79
const/4 v0, 0x1
goto :goto_a
:cond_7b
sget-object v2, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->d:Lorg/apache/commons/logging/Log;
invoke-interface {v2}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v2
if-eqz v2, :cond_a
sget-object v2, Lorg/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP;->d:Lorg/apache/commons/logging/Log;
new-instance v3, Ljava/lang/StringBuffer;
invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V
const-string v4, "I state that I can\'t resolve "
invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v3
invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v1
invoke-interface {v2, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
goto/16 :goto_a
.end method