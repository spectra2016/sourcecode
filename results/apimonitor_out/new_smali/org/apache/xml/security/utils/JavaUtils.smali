.class public Lorg/apache/xml/security/utils/JavaUtils;
.super Ljava/lang/Object;
.field static a:Lorg/apache/commons/logging/Log;
.field static b:Ljava/lang/Class;
.method static constructor <clinit>()V
.registers 1
sget-object v0, Lorg/apache/xml/security/utils/JavaUtils;->b:Ljava/lang/Class;
if-nez v0, :cond_17
const-string v0, "org.apache.xml.security.utils.JavaUtils"
invoke-static {v0}, Lorg/apache/xml/security/utils/JavaUtils;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/utils/JavaUtils;->b:Ljava/lang/Class;
:goto_c
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/utils/JavaUtils;->a:Lorg/apache/commons/logging/Log;
return-void
:cond_17
sget-object v0, Lorg/apache/xml/security/utils/JavaUtils;->b:Ljava/lang/Class;
goto :goto_c
.end method
.method private constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method static a(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method public static a(Ljava/io/InputStream;)[B
.registers 5
new-instance v0, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;
invoke-direct {v0}, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;-><init>()V
const/16 v1, 0x400
new-array v1, v1, [B
:goto_9
invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I
move-result v2
if-lez v2, :cond_14
const/4 v3, 0x0
invoke-virtual {v0, v1, v3, v2}, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;->write([BII)V
goto :goto_9
:cond_14
invoke-virtual {v0}, Lorg/apache/xml/security/utils/UnsyncByteArrayOutputStream;->a()[B
move-result-object v0
return-object v0
.end method