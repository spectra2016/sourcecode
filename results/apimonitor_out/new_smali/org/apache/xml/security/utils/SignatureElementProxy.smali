.class public abstract Lorg/apache/xml/security/utils/SignatureElementProxy;
.super Lorg/apache/xml/security/utils/ElementProxy;
.method protected constructor <init>()V
.registers 1
invoke-direct {p0}, Lorg/apache/xml/security/utils/ElementProxy;-><init>()V
return-void
.end method
.method public constructor <init>(Lorg/w3c/dom/Document;)V
.registers 4
invoke-direct {p0}, Lorg/apache/xml/security/utils/ElementProxy;-><init>()V
if-nez p1, :cond_d
new-instance v0, Ljava/lang/RuntimeException;
const-string v1, "Document is null"
invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V
throw v0
:cond_d
iput-object p1, p0, Lorg/apache/xml/security/utils/SignatureElementProxy;->m:Lorg/w3c/dom/Document;
iget-object v0, p0, Lorg/apache/xml/security/utils/SignatureElementProxy;->m:Lorg/w3c/dom/Document;
invoke-virtual {p0}, Lorg/apache/xml/security/utils/SignatureElementProxy;->e()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Document;Ljava/lang/String;)Lorg/w3c/dom/Element;
move-result-object v0
iput-object v0, p0, Lorg/apache/xml/security/utils/SignatureElementProxy;->k:Lorg/w3c/dom/Element;
return-void
.end method
.method public constructor <init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
.registers 3
invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/utils/ElementProxy;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
return-void
.end method
.method public d()Ljava/lang/String;
.registers 2
const-string v0, "http://www.w3.org/2000/09/xmldsig#"
return-object v0
.end method