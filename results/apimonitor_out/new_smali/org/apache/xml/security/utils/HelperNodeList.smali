.class public Lorg/apache/xml/security/utils/HelperNodeList;
.super Ljava/lang/Object;
.implements Lorg/w3c/dom/NodeList;
.field  a:Ljava/util/ArrayList;
.field  b:Z
.method public constructor <init>()V
.registers 2
const/4 v0, 0x0
invoke-direct {p0, v0}, Lorg/apache/xml/security/utils/HelperNodeList;-><init>(Z)V
return-void
.end method
.method public constructor <init>(Z)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
new-instance v0, Ljava/util/ArrayList;
const/16 v1, 0x14
invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V
iput-object v0, p0, Lorg/apache/xml/security/utils/HelperNodeList;->a:Ljava/util/ArrayList;
const/4 v0, 0x0
iput-boolean v0, p0, Lorg/apache/xml/security/utils/HelperNodeList;->b:Z
iput-boolean p1, p0, Lorg/apache/xml/security/utils/HelperNodeList;->b:Z
return-void
.end method
.method public getLength()I
.registers 2
iget-object v0, p0, Lorg/apache/xml/security/utils/HelperNodeList;->a:Ljava/util/ArrayList;
invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
move-result v0
return v0
.end method
.method public item(I)Lorg/w3c/dom/Node;
.registers 3
iget-object v0, p0, Lorg/apache/xml/security/utils/HelperNodeList;->a:Ljava/util/ArrayList;
invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/w3c/dom/Node;
return-object v0
.end method