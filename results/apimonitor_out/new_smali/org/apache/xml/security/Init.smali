.class public Lorg/apache/xml/security/Init;
.super Ljava/lang/Object;
.field static a:Lorg/apache/commons/logging/Log;
.field static b:Ljava/lang/Class;
.field private static c:Z
.method static constructor <clinit>()V
.registers 1
sget-object v0, Lorg/apache/xml/security/Init;->b:Ljava/lang/Class;
if-nez v0, :cond_1a
const-string v0, "org.apache.xml.security.Init"
invoke-static {v0}, Lorg/apache/xml/security/Init;->a(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/Init;->b:Ljava/lang/Class;
:goto_c
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/commons/logging/LogFactory;->getLog(Ljava/lang/String;)Lorg/apache/commons/logging/Log;
move-result-object v0
sput-object v0, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
const/4 v0, 0x0
sput-boolean v0, Lorg/apache/xml/security/Init;->c:Z
return-void
:cond_1a
sget-object v0, Lorg/apache/xml/security/Init;->b:Ljava/lang/Class;
goto :goto_c
.end method
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method static a(Ljava/lang/String;)Ljava/lang/Class;
.registers 3
:try_start_0
invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
:try_end_3
.catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_3} :catch_5
move-result-object v0
return-object v0
:catch_5
move-exception v0
new-instance v1, Ljava/lang/NoClassDefFoundError;
invoke-direct {v1}, Ljava/lang/NoClassDefFoundError;-><init>()V
invoke-virtual {v1, v0}, Ljava/lang/NoClassDefFoundError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
move-result-object v0
throw v0
.end method
.method public static final a()Z
.registers 1
sget-boolean v0, Lorg/apache/xml/security/Init;->c:Z
return v0
.end method
.method public static declared-synchronized b()V
.registers 58
const-class v38, Lorg/apache/xml/security/Init;
monitor-enter v38
:try_start_3
sget-boolean v4, Lorg/apache/xml/security/Init;->c:Z
:try_end_5
.catchall {:try_start_3 .. :try_end_5} :catchall_a6
if-eqz v4, :cond_9
:goto_7
monitor-exit v38
return-void
:cond_9
const-wide/16 v36, 0x0
const-wide/16 v34, 0x0
const-wide/16 v32, 0x0
const-wide/16 v30, 0x0
const-wide/16 v28, 0x0
const-wide/16 v26, 0x0
const-wide/16 v24, 0x0
const-wide/16 v22, 0x0
const-wide/16 v20, 0x0
const-wide/16 v18, 0x0
:try_start_1d
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v40
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v42
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v44
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v46
invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;
move-result-object v4
const/4 v5, 0x1
invoke-virtual {v4, v5}, Ljavax/xml/parsers/DocumentBuilderFactory;->setNamespaceAware(Z)V
const/4 v5, 0x0
invoke-virtual {v4, v5}, Ljavax/xml/parsers/DocumentBuilderFactory;->setValidating(Z)V
invoke-virtual {v4}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
move-result-object v5
new-instance v4, Lorg/apache/xml/security/Init$1;
invoke-direct {v4}, Lorg/apache/xml/security/Init$1;-><init>()V
invoke-static {v4}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedAction;)Ljava/lang/Object;
move-result-object v4
check-cast v4, Ljava/io/InputStream;
invoke-virtual {v5, v4}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
move-result-object v4
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v48
const-wide/16 v16, 0x0
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
:try_end_55
.catchall {:try_start_1d .. :try_end_55} :catchall_a6
.catch Ljava/lang/Exception; {:try_start_1d .. :try_end_55} :catch_96
move-result-wide v50
:try_start_56
invoke-static {}, Lorg/apache/xml/security/keys/KeyInfo;->a()V
:try_start_59
:try_end_59
.catchall {:try_start_56 .. :try_end_59} :catchall_a6
.catch Ljava/lang/Exception; {:try_start_56 .. :try_end_59} :catch_91
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v52
const-wide/16 v14, 0x0
const-wide/16 v12, 0x0
const-wide/16 v10, 0x0
const-wide/16 v8, 0x0
const-wide/16 v6, 0x0
invoke-interface {v4}, Lorg/w3c/dom/Document;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v4
:goto_6b
if-eqz v4, :cond_7b
const-string v5, "Configuration"
invoke-interface {v4}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;
move-result-object v39
move-object/from16 v0, v39
invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v5
if-eqz v5, :cond_a9
:cond_7b
invoke-interface {v4}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v5
:goto_7f
if-eqz v5, :cond_53e
if-eqz v5, :cond_8c
const/4 v4, 0x1
invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeType()S
move-result v39
move/from16 v0, v39
if-eq v4, v0, :cond_ae
:goto_8c
:cond_8c
invoke-interface {v5}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;
move-result-object v5
goto :goto_7f
:catch_91
move-exception v4
invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
throw v4
:catch_96
:try_end_96
.catchall {:try_start_59 .. :try_end_96} :catchall_a6
.catch Ljava/lang/Exception; {:try_start_59 .. :try_end_96} :catch_96
move-exception v4
:try_start_97
sget-object v5, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
const-string v6, "Bad: "
invoke-interface {v5, v6, v4}, Lorg/apache/commons/logging/Log;->fatal(Ljava/lang/Object;Ljava/lang/Throwable;)V
invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
:goto_a1
:cond_a1
const/4 v4, 0x1
sput-boolean v4, Lorg/apache/xml/security/Init;->c:Z
:try_end_a4
.catchall {:try_start_97 .. :try_end_a4} :catchall_a6
goto/16 :goto_7
:catchall_a6
move-exception v4
monitor-exit v38
throw v4
:try_start_a9
:cond_a9
invoke-interface {v4}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;
move-result-object v4
goto :goto_6b
:cond_ae
invoke-interface {v5}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;
move-result-object v39
const-string v4, "ResourceBundles"
move-object/from16 v0, v39
invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v4
if-eqz v4, :cond_e5
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v16
move-object v0, v5
check-cast v0, Lorg/w3c/dom/Element;
move-object v4, v0
const-string v36, "defaultLanguageCode"
move-object/from16 v0, v36
invoke-interface {v4, v0}, Lorg/w3c/dom/Element;->getAttributeNode(Ljava/lang/String;)Lorg/w3c/dom/Attr;
move-result-object v36
const-string v37, "defaultCountryCode"
move-object/from16 v0, v37
invoke-interface {v4, v0}, Lorg/w3c/dom/Element;->getAttributeNode(Ljava/lang/String;)Lorg/w3c/dom/Attr;
move-result-object v37
if-nez v36, :cond_16f
const/4 v4, 0x0
move-object/from16 v36, v4
:goto_d9
if-nez v37, :cond_177
const/4 v4, 0x0
:goto_dc
move-object/from16 v0, v36
invoke-static {v0, v4}, Lorg/apache/xml/security/utils/I18n;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v36
:cond_e5
const-string v4, "CanonicalizationMethods"
move-object/from16 v0, v39
invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v4
if-eqz v4, :cond_1a2
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v34
invoke-static {}, Lorg/apache/xml/security/c14n/Canonicalizer;->a()V
invoke-interface {v5}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v4
const-string v32, "http://www.xmlsecurity.org/NS/#configuration"
const-string v33, "CanonicalizationMethod"
move-object/from16 v0, v32
move-object/from16 v1, v33
invoke-static {v4, v0, v1}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;
move-result-object v32
const/4 v4, 0x0
:goto_107
move-object/from16 v0, v32
array-length v0, v0
move/from16 v33, v0
move/from16 v0, v33
if-ge v4, v0, :cond_19e
aget-object v33, v32, v4
const/16 v54, 0x0
const-string v55, "URI"
move-object/from16 v0, v33
move-object/from16 v1, v54
move-object/from16 v2, v55
invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v33
aget-object v54, v32, v4
const/16 v55, 0x0
const-string v56, "JAVACLASS"
invoke-interface/range {v54 .. v56}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
:try_end_129
.catchall {:try_start_a9 .. :try_end_129} :catchall_a6
.catch Ljava/lang/Exception; {:try_start_a9 .. :try_end_129} :catch_96
move-result-object v54
:try_start_12a
invoke-static/range {v54 .. v54}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
sget-object v55, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
invoke-interface/range {v55 .. v55}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v55
if-eqz v55, :cond_165
sget-object v55, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v56, Ljava/lang/StringBuffer;
invoke-direct/range {v56 .. v56}, Ljava/lang/StringBuffer;-><init>()V
const-string v57, "Canonicalizer.register("
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
move-object/from16 v0, v56
move-object/from16 v1, v33
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
const-string v57, ", "
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
move-object/from16 v0, v56
move-object/from16 v1, v54
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
const-string v57, ")"
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
invoke-virtual/range {v56 .. v56}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v56
invoke-interface/range {v55 .. v56}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_165
move-object/from16 v0, v33
move-object/from16 v1, v54
invoke-static {v0, v1}, Lorg/apache/xml/security/c14n/Canonicalizer;->a(Ljava/lang/String;Ljava/lang/String;)V
:goto_16c
:try_end_16c
.catchall {:try_start_12a .. :try_end_16c} :catchall_a6
.catch Ljava/lang/ClassNotFoundException; {:try_start_12a .. :try_end_16c} :catch_17d
.catch Ljava/lang/Exception; {:try_start_12a .. :try_end_16c} :catch_96
add-int/lit8 v4, v4, 0x1
goto :goto_107
:try_start_16f
:cond_16f
invoke-interface/range {v36 .. v36}, Lorg/w3c/dom/Attr;->getNodeValue()Ljava/lang/String;
move-result-object v4
move-object/from16 v36, v4
goto/16 :goto_d9
:cond_177
invoke-interface/range {v37 .. v37}, Lorg/w3c/dom/Attr;->getNodeValue()Ljava/lang/String;
move-result-object v4
goto/16 :goto_dc
:catch_17d
move-exception v55
const/16 v55, 0x2
move/from16 v0, v55
new-array v0, v0, [Ljava/lang/Object;
move-object/from16 v55, v0
const/16 v56, 0x0
aput-object v33, v55, v56
const/16 v33, 0x1
aput-object v54, v55, v33
sget-object v33, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
const-string v54, "algorithm.classDoesNotExist"
invoke-static/range {v54 .. v55}, Lorg/apache/xml/security/utils/I18n;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
move-result-object v54
move-object/from16 v0, v33
move-object/from16 v1, v54
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->fatal(Ljava/lang/Object;)V
goto :goto_16c
:cond_19e
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v32
:cond_1a2
const-string v4, "TransformAlgorithms"
move-object/from16 v0, v39
invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v4
if-eqz v4, :cond_25e
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v14
invoke-static {}, Lorg/apache/xml/security/transforms/Transform;->a()V
invoke-interface {v5}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v4
const-string v20, "http://www.xmlsecurity.org/NS/#configuration"
const-string v21, "TransformAlgorithm"
move-object/from16 v0, v20
move-object/from16 v1, v21
invoke-static {v4, v0, v1}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;
move-result-object v20
const/4 v4, 0x0
:goto_1c4
move-object/from16 v0, v20
array-length v0, v0
move/from16 v21, v0
move/from16 v0, v21
if-ge v4, v0, :cond_25a
aget-object v21, v20, v4
const/16 v54, 0x0
const-string v55, "URI"
move-object/from16 v0, v21
move-object/from16 v1, v54
move-object/from16 v2, v55
invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v21
aget-object v54, v20, v4
const/16 v55, 0x0
const-string v56, "JAVACLASS"
invoke-interface/range {v54 .. v56}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
:try_end_1e6
.catchall {:try_start_16f .. :try_end_1e6} :catchall_a6
.catch Ljava/lang/Exception; {:try_start_16f .. :try_end_1e6} :catch_96
move-result-object v54
:try_start_1e7
invoke-static/range {v54 .. v54}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
sget-object v55, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
invoke-interface/range {v55 .. v55}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v55
if-eqz v55, :cond_222
sget-object v55, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v56, Ljava/lang/StringBuffer;
invoke-direct/range {v56 .. v56}, Ljava/lang/StringBuffer;-><init>()V
const-string v57, "Transform.register("
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
move-object/from16 v0, v56
move-object/from16 v1, v21
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
const-string v57, ", "
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
move-object/from16 v0, v56
move-object/from16 v1, v54
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
const-string v57, ")"
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
invoke-virtual/range {v56 .. v56}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v56
invoke-interface/range {v55 .. v56}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_222
move-object/from16 v0, v21
move-object/from16 v1, v54
invoke-static {v0, v1}, Lorg/apache/xml/security/transforms/Transform;->a(Ljava/lang/String;Ljava/lang/String;)V
:try_end_229
.catchall {:try_start_1e7 .. :try_end_229} :catchall_a6
.catch Ljava/lang/ClassNotFoundException; {:try_start_1e7 .. :try_end_229} :catch_22c
.catch Ljava/lang/NoClassDefFoundError; {:try_start_1e7 .. :try_end_229} :catch_24d
.catch Ljava/lang/Exception; {:try_start_1e7 .. :try_end_229} :catch_96
:goto_229
add-int/lit8 v4, v4, 0x1
goto :goto_1c4
:catch_22c
move-exception v55
const/16 v55, 0x2
:try_start_22f
move/from16 v0, v55
new-array v0, v0, [Ljava/lang/Object;
move-object/from16 v55, v0
const/16 v56, 0x0
aput-object v21, v55, v56
const/16 v21, 0x1
aput-object v54, v55, v21
sget-object v21, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
const-string v54, "algorithm.classDoesNotExist"
invoke-static/range {v54 .. v55}, Lorg/apache/xml/security/utils/I18n;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
move-result-object v54
move-object/from16 v0, v21
move-object/from16 v1, v54
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->fatal(Ljava/lang/Object;)V
goto :goto_229
:catch_24d
move-exception v21
sget-object v21, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
const-string v54, "Not able to found dependecies for algorithm, I\'m keep working."
move-object/from16 v0, v21
move-object/from16 v1, v54
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;)V
goto :goto_229
:cond_25a
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v20
:cond_25e
const-string v4, "JCEAlgorithmMappings"
move-object/from16 v0, v39
invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v4
if-eqz v4, :cond_277
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v12
move-object v0, v5
check-cast v0, Lorg/w3c/dom/Element;
move-object v4, v0
invoke-static {v4}, Lorg/apache/xml/security/algorithms/JCEMapper;->a(Lorg/w3c/dom/Element;)V
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v30
:cond_277
const-string v4, "SignatureAlgorithms"
move-object/from16 v0, v39
invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v4
if-eqz v4, :cond_326
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v10
invoke-static {}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->h()V
invoke-interface {v5}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v4
const-string v22, "http://www.xmlsecurity.org/NS/#configuration"
const-string v23, "SignatureAlgorithm"
move-object/from16 v0, v22
move-object/from16 v1, v23
invoke-static {v4, v0, v1}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;
move-result-object v22
const/4 v4, 0x0
:goto_299
move-object/from16 v0, v22
array-length v0, v0
move/from16 v23, v0
move/from16 v0, v23
if-ge v4, v0, :cond_322
aget-object v23, v22, v4
const/16 v54, 0x0
const-string v55, "URI"
move-object/from16 v0, v23
move-object/from16 v1, v54
move-object/from16 v2, v55
invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v23
aget-object v54, v22, v4
const/16 v55, 0x0
const-string v56, "JAVACLASS"
invoke-interface/range {v54 .. v56}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
:try_end_2bb
.catchall {:try_start_22f .. :try_end_2bb} :catchall_a6
.catch Ljava/lang/Exception; {:try_start_22f .. :try_end_2bb} :catch_96
move-result-object v54
:try_start_2bc
invoke-static/range {v54 .. v54}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
sget-object v55, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
invoke-interface/range {v55 .. v55}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v55
if-eqz v55, :cond_2f7
sget-object v55, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v56, Ljava/lang/StringBuffer;
invoke-direct/range {v56 .. v56}, Ljava/lang/StringBuffer;-><init>()V
const-string v57, "SignatureAlgorithm.register("
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
move-object/from16 v0, v56
move-object/from16 v1, v23
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
const-string v57, ", "
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
move-object/from16 v0, v56
move-object/from16 v1, v54
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
const-string v57, ")"
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
invoke-virtual/range {v56 .. v56}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v56
invoke-interface/range {v55 .. v56}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_2f7
move-object/from16 v0, v23
move-object/from16 v1, v54
invoke-static {v0, v1}, Lorg/apache/xml/security/algorithms/SignatureAlgorithm;->a(Ljava/lang/String;Ljava/lang/String;)V
:try_end_2fe
.catchall {:try_start_2bc .. :try_end_2fe} :catchall_a6
.catch Ljava/lang/ClassNotFoundException; {:try_start_2bc .. :try_end_2fe} :catch_301
.catch Ljava/lang/Exception; {:try_start_2bc .. :try_end_2fe} :catch_96
:goto_2fe
add-int/lit8 v4, v4, 0x1
goto :goto_299
:catch_301
move-exception v55
const/16 v55, 0x2
:try_start_304
move/from16 v0, v55
new-array v0, v0, [Ljava/lang/Object;
move-object/from16 v55, v0
const/16 v56, 0x0
aput-object v23, v55, v56
const/16 v23, 0x1
aput-object v54, v55, v23
sget-object v23, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
const-string v54, "algorithm.classDoesNotExist"
invoke-static/range {v54 .. v55}, Lorg/apache/xml/security/utils/I18n;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
move-result-object v54
move-object/from16 v0, v23
move-object/from16 v1, v54
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->fatal(Ljava/lang/Object;)V
goto :goto_2fe
:cond_322
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v22
:cond_326
const-string v4, "ResourceResolvers"
move-object/from16 v0, v39
invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v4
if-eqz v4, :cond_3fd
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v24
invoke-static {}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a()V
invoke-interface {v5}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v4
const-string v54, "http://www.xmlsecurity.org/NS/#configuration"
const-string v55, "Resolver"
move-object/from16 v0, v54
move-object/from16 v1, v55
invoke-static {v4, v0, v1}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;
move-result-object v54
const/4 v4, 0x0
:goto_348
move-object/from16 v0, v54
array-length v0, v0
move/from16 v55, v0
move/from16 v0, v55
if-ge v4, v0, :cond_3fd
aget-object v8, v54, v4
const/4 v9, 0x0
const-string v55, "JAVACLASS"
move-object/from16 v0, v55
invoke-interface {v8, v9, v0}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v8
aget-object v9, v54, v4
const/16 v55, 0x0
const-string v56, "DESCRIPTION"
move-object/from16 v0, v55
move-object/from16 v1, v56
invoke-interface {v9, v0, v1}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v9
if-eqz v9, :cond_3ac
invoke-virtual {v9}, Ljava/lang/String;->length()I
move-result v55
if-lez v55, :cond_3ac
sget-object v55, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
invoke-interface/range {v55 .. v55}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v55
if-eqz v55, :cond_3a2
sget-object v55, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v56, Ljava/lang/StringBuffer;
invoke-direct/range {v56 .. v56}, Ljava/lang/StringBuffer;-><init>()V
const-string v57, "Register Resolver: "
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
move-object/from16 v0, v56
invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
const-string v57, ": "
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
move-object/from16 v0, v56
invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v9
invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v9
move-object/from16 v0, v55
invoke-interface {v0, v9}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:goto_3a2
:try_end_3a2
.catchall {:try_start_304 .. :try_end_3a2} :catchall_a6
.catch Ljava/lang/Exception; {:try_start_304 .. :try_end_3a2} :catch_96
:cond_3a2
:try_start_3a2
invoke-static {v8}, Lorg/apache/xml/security/utils/resolver/ResourceResolver;->a(Ljava/lang/String;)V
:try_start_3a5
:try_end_3a5
.catchall {:try_start_3a2 .. :try_end_3a5} :catchall_a6
.catch Ljava/lang/Throwable; {:try_start_3a2 .. :try_end_3a5} :catch_3d7
.catch Ljava/lang/Exception; {:try_start_3a2 .. :try_end_3a5} :catch_96
:goto_3a5
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v8
add-int/lit8 v4, v4, 0x1
goto :goto_348
:cond_3ac
sget-object v9, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v9}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v9
if-eqz v9, :cond_3a2
sget-object v9, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v55, Ljava/lang/StringBuffer;
invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V
const-string v56, "Register Resolver: "
invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v55
move-object/from16 v0, v55
invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v55
const-string v56, ": For unknown purposes"
invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v55
invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v55
move-object/from16 v0, v55
invoke-interface {v9, v0}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
goto :goto_3a2
:catch_3d7
move-exception v9
sget-object v55, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v56, Ljava/lang/StringBuffer;
invoke-direct/range {v56 .. v56}, Ljava/lang/StringBuffer;-><init>()V
const-string v57, "Cannot register:"
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
move-object/from16 v0, v56
invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v8
const-string v56, " perhaps some needed jars are not installed"
move-object/from16 v0, v56
invoke-virtual {v8, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v8
invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v8
move-object/from16 v0, v55
invoke-interface {v0, v8, v9}, Lorg/apache/commons/logging/Log;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V
goto :goto_3a5
:cond_3fd
const-string v4, "KeyResolver"
move-object/from16 v0, v39
invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v4
if-eqz v4, :cond_4b5
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v18
invoke-static {}, Lorg/apache/xml/security/keys/keyresolver/KeyResolver;->a()V
invoke-interface {v5}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v4
const-string v28, "http://www.xmlsecurity.org/NS/#configuration"
const-string v29, "Resolver"
move-object/from16 v0, v28
move-object/from16 v1, v29
invoke-static {v4, v0, v1}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;
move-result-object v28
const/4 v4, 0x0
:goto_41f
move-object/from16 v0, v28
array-length v0, v0
move/from16 v29, v0
move/from16 v0, v29
if-ge v4, v0, :cond_4b1
aget-object v29, v28, v4
const/16 v54, 0x0
const-string v55, "JAVACLASS"
move-object/from16 v0, v29
move-object/from16 v1, v54
move-object/from16 v2, v55
invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v29
aget-object v54, v28, v4
const/16 v55, 0x0
const-string v56, "DESCRIPTION"
invoke-interface/range {v54 .. v56}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v54
if-eqz v54, :cond_486
invoke-virtual/range {v54 .. v54}, Ljava/lang/String;->length()I
move-result v55
if-lez v55, :cond_486
sget-object v55, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
invoke-interface/range {v55 .. v55}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v55
if-eqz v55, :cond_480
sget-object v55, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v56, Ljava/lang/StringBuffer;
invoke-direct/range {v56 .. v56}, Ljava/lang/StringBuffer;-><init>()V
const-string v57, "Register Resolver: "
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
move-object/from16 v0, v56
move-object/from16 v1, v29
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
const-string v57, ": "
invoke-virtual/range {v56 .. v57}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v56
move-object/from16 v0, v56
move-object/from16 v1, v54
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v54
invoke-virtual/range {v54 .. v54}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v54
move-object/from16 v0, v55
move-object/from16 v1, v54
invoke-interface {v0, v1}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_480
:goto_480
invoke-static/range {v29 .. v29}, Lorg/apache/xml/security/keys/keyresolver/KeyResolver;->a(Ljava/lang/String;)V
add-int/lit8 v4, v4, 0x1
goto :goto_41f
:cond_486
sget-object v54, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
invoke-interface/range {v54 .. v54}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v54
if-eqz v54, :cond_480
sget-object v54, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v55, Ljava/lang/StringBuffer;
invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V
const-string v56, "Register Resolver: "
invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v55
move-object/from16 v0, v55
move-object/from16 v1, v29
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v55
const-string v56, ": For unknown purposes"
invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v55
invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v55
invoke-interface/range {v54 .. v55}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
goto :goto_480
:cond_4b1
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v28
:cond_4b5
const-string v4, "PrefixMappings"
move-object/from16 v0, v39
invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v4
if-eqz v4, :cond_8c
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v26
sget-object v4, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
invoke-interface {v4}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v4
if-eqz v4, :cond_4d2
sget-object v4, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
const-string v6, "Now I try to bind prefixes:"
invoke-interface {v4, v6}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_4d2
invoke-interface {v5}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v4
const-string v6, "http://www.xmlsecurity.org/NS/#configuration"
const-string v7, "PrefixMapping"
invoke-static {v4, v6, v7}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;
move-result-object v6
const/4 v4, 0x0
:goto_4df
array-length v7, v6
if-ge v4, v7, :cond_538
aget-object v7, v6, v4
const/16 v39, 0x0
const-string v54, "namespace"
move-object/from16 v0, v39
move-object/from16 v1, v54
invoke-interface {v7, v0, v1}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v7
aget-object v39, v6, v4
const/16 v54, 0x0
const-string v55, "prefix"
move-object/from16 v0, v39
move-object/from16 v1, v54
move-object/from16 v2, v55
invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v39
sget-object v54, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
invoke-interface/range {v54 .. v54}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v54
if-eqz v54, :cond_530
sget-object v54, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v55, Ljava/lang/StringBuffer;
invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuffer;-><init>()V
const-string v56, "Now I try to bind "
invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v55
move-object/from16 v0, v55
move-object/from16 v1, v39
invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v55
const-string v56, " to "
invoke-virtual/range {v55 .. v56}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v55
move-object/from16 v0, v55
invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v55
invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v55
invoke-interface/range {v54 .. v55}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:cond_530
move-object/from16 v0, v39
invoke-static {v7, v0}, Lorg/apache/xml/security/utils/ElementProxy;->d(Ljava/lang/String;Ljava/lang/String;)V
add-int/lit8 v4, v4, 0x1
goto :goto_4df
:cond_538
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v6
goto/16 :goto_8c
:cond_53e
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v4
sget-object v39, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
invoke-interface/range {v39 .. v39}, Lorg/apache/commons/logging/Log;->isDebugEnabled()Z
move-result v39
if-eqz v39, :cond_a1
sget-object v39, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v54, Ljava/lang/StringBuffer;
invoke-direct/range {v54 .. v54}, Ljava/lang/StringBuffer;-><init>()V
const-string v55, "XX_init                             "
invoke-virtual/range {v54 .. v55}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v54
sub-long v4, v4, v40
long-to-int v4, v4
move-object/from16 v0, v54
invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v4
const-string v5, " ms"
invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v4
move-object/from16 v0, v39
invoke-interface {v0, v4}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v4, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v5, Ljava/lang/StringBuffer;
invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V
const-string v39, "  XX_prng                           "
move-object/from16 v0, v39
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
sub-long v40, v44, v42
move-wide/from16 v0, v40
long-to-int v0, v0
move/from16 v39, v0
move/from16 v0, v39
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v5
const-string v39, " ms"
move-object/from16 v0, v39
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v5
invoke-interface {v4, v5}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v4, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v5, Ljava/lang/StringBuffer;
invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V
const-string v39, "  XX_parsing                        "
move-object/from16 v0, v39
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
sub-long v40, v48, v46
move-wide/from16 v0, v40
long-to-int v0, v0
move/from16 v39, v0
move/from16 v0, v39
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v5
const-string v39, " ms"
move-object/from16 v0, v39
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v5
invoke-interface {v4, v5}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v4, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v5, Ljava/lang/StringBuffer;
invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V
const-string v39, "  XX_configure_i18n                 "
move-object/from16 v0, v39
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
sub-long v16, v36, v16
move-wide/from16 v0, v16
long-to-int v0, v0
move/from16 v16, v0
move/from16 v0, v16
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v5
const-string v16, " ms"
move-object/from16 v0, v16
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v5
invoke-interface {v4, v5}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v4, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v5, Ljava/lang/StringBuffer;
invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V
const-string v16, "  XX_configure_reg_c14n             "
move-object/from16 v0, v16
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
sub-long v16, v32, v34
move-wide/from16 v0, v16
long-to-int v0, v0
move/from16 v16, v0
move/from16 v0, v16
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v5
const-string v16, " ms"
move-object/from16 v0, v16
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v5
invoke-interface {v4, v5}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v4, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v5, Ljava/lang/StringBuffer;
invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V
const-string v16, "  XX_configure_reg_jcemapper        "
move-object/from16 v0, v16
invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
sub-long v12, v30, v12
long-to-int v12, v12
invoke-virtual {v5, v12}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v5
const-string v12, " ms"
invoke-virtual {v5, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v5
invoke-interface {v4, v5}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v4, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v5, Ljava/lang/StringBuffer;
invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V
const-string v12, "  XX_configure_reg_keyInfo          "
invoke-virtual {v5, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
sub-long v12, v52, v50
long-to-int v12, v12
invoke-virtual {v5, v12}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v5
const-string v12, " ms"
invoke-virtual {v5, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v5
invoke-interface {v4, v5}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v4, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v5, Ljava/lang/StringBuffer;
invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V
const-string v12, "  XX_configure_reg_keyResolver      "
invoke-virtual {v5, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
sub-long v12, v28, v18
long-to-int v12, v12
invoke-virtual {v5, v12}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v5
const-string v12, " ms"
invoke-virtual {v5, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v5
invoke-interface {v4, v5}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v4, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v5, Ljava/lang/StringBuffer;
invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V
const-string v12, "  XX_configure_reg_prefixes         "
invoke-virtual {v5, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
sub-long v6, v6, v26
long-to-int v6, v6
invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v5
const-string v6, " ms"
invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v5
invoke-interface {v4, v5}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v4, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v5, Ljava/lang/StringBuffer;
invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V
const-string v6, "  XX_configure_reg_resourceresolver "
invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
sub-long v6, v8, v24
long-to-int v6, v6
invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v5
const-string v6, " ms"
invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v5
invoke-interface {v4, v5}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v4, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v5, Ljava/lang/StringBuffer;
invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V
const-string v6, "  XX_configure_reg_sigalgos         "
invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
sub-long v6, v22, v10
long-to-int v6, v6
invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v5
const-string v6, " ms"
invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v5
invoke-interface {v4, v5}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
sget-object v4, Lorg/apache/xml/security/Init;->a:Lorg/apache/commons/logging/Log;
new-instance v5, Ljava/lang/StringBuffer;
invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V
const-string v6, "  XX_configure_reg_transforms       "
invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
sub-long v6, v20, v14
long-to-int v6, v6
invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
move-result-object v5
const-string v6, " ms"
invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v5
invoke-interface {v4, v5}, Lorg/apache/commons/logging/Log;->debug(Ljava/lang/Object;)V
:try_end_704
.catchall {:try_start_3a5 .. :try_end_704} :catchall_a6
.catch Ljava/lang/Exception; {:try_start_3a5 .. :try_end_704} :catch_96
goto/16 :goto_a1
.end method