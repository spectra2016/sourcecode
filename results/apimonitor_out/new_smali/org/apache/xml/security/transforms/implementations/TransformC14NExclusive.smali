.class public Lorg/apache/xml/security/transforms/implementations/TransformC14NExclusive;
.super Lorg/apache/xml/security/transforms/TransformSpi;
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Lorg/apache/xml/security/transforms/TransformSpi;-><init>()V
return-void
.end method
.method protected a(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;
.registers 8
const/4 v0, 0x0
:try_start_1
const-string v1, "http://www.w3.org/2001/10/xml-exc-c14n#"
const-string v2, "InclusiveNamespaces"
invoke-virtual {p3, v1, v2}, Lorg/apache/xml/security/transforms/Transform;->c(Ljava/lang/String;Ljava/lang/String;)I
move-result v1
const/4 v2, 0x1
if-ne v1, v2, :cond_2a
invoke-virtual {p3}, Lorg/apache/xml/security/transforms/Transform;->k()Lorg/w3c/dom/Element;
move-result-object v0
invoke-interface {v0}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v0
const-string v1, "http://www.w3.org/2001/10/xml-exc-c14n#"
const-string v2, "InclusiveNamespaces"
const/4 v3, 0x0
invoke-static {v0, v1, v2, v3}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;I)Lorg/w3c/dom/Element;
move-result-object v0
new-instance v1, Lorg/apache/xml/security/transforms/params/InclusiveNamespaces;
invoke-virtual {p3}, Lorg/apache/xml/security/transforms/Transform;->l()Ljava/lang/String;
move-result-object v2
invoke-direct {v1, v0, v2}, Lorg/apache/xml/security/transforms/params/InclusiveNamespaces;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
invoke-virtual {v1}, Lorg/apache/xml/security/transforms/params/InclusiveNamespaces;->a()Ljava/lang/String;
move-result-object v0
:cond_2a
new-instance v1, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315ExclOmitComments;
invoke-direct {v1}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315ExclOmitComments;-><init>()V
if-eqz p2, :cond_34
invoke-virtual {v1, p2}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315ExclOmitComments;->a(Ljava/io/OutputStream;)V
:cond_34
invoke-virtual {v1, p1, v0}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315ExclOmitComments;->a(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/lang/String;)[B
move-result-object v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureInput;
invoke-direct {v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureInput;-><init>([B)V
if-eqz p2, :cond_42
invoke-virtual {v1, p2}, Lorg/apache/xml/security/signature/XMLSignatureInput;->b(Ljava/io/OutputStream;)V
:try_end_42
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_1 .. :try_end_42} :catch_43
:cond_42
return-object v1
:catch_43
move-exception v0
new-instance v1, Lorg/apache/xml/security/c14n/CanonicalizationException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method
.method protected a(Lorg/apache/xml/security/signature/XMLSignatureInput;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;
.registers 4
const/4 v0, 0x0
invoke-virtual {p0, p1, v0, p2}, Lorg/apache/xml/security/transforms/implementations/TransformC14NExclusive;->a(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;
move-result-object v0
return-object v0
.end method