.class public Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;
.super Lorg/apache/xml/security/utils/ElementProxy;
.implements Lorg/apache/xml/security/transforms/TransformParam;
.method private constructor <init>()V
.registers 1
invoke-direct {p0}, Lorg/apache/xml/security/utils/ElementProxy;-><init>()V
return-void
.end method
.method private constructor <init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
.registers 7
invoke-direct {p0, p1, p2}, Lorg/apache/xml/security/utils/ElementProxy;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
iget-object v0, p0, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;->k:Lorg/w3c/dom/Element;
const/4 v1, 0x0
const-string v2, "Filter"
invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
const-string v1, "intersect"
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_3c
const-string v1, "subtract"
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_3c
const-string v1, "union"
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_3c
const/4 v1, 0x3
new-array v1, v1, [Ljava/lang/Object;
const/4 v2, 0x0
const-string v3, "Filter"
aput-object v3, v1, v2
const/4 v2, 0x1
aput-object v0, v1, v2
const/4 v0, 0x2
const-string v2, "intersect, subtract or union"
aput-object v2, v1, v0
new-instance v0, Lorg/apache/xml/security/exceptions/XMLSecurityException;
const-string v2, "attributeValueIllegal"
invoke-direct {v0, v2, v1}, Lorg/apache/xml/security/exceptions/XMLSecurityException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
:cond_3c
return-void
.end method
.method public static a(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;
.registers 3
new-instance v0, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;
invoke-direct {v0, p0, p1}, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
return-object v0
.end method
.method public a()Z
.registers 4
iget-object v0, p0, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;->k:Lorg/w3c/dom/Element;
const/4 v1, 0x0
const-string v2, "Filter"
invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
const-string v1, "intersect"
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
return v0
.end method
.method public b()Z
.registers 4
iget-object v0, p0, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;->k:Lorg/w3c/dom/Element;
const/4 v1, 0x0
const-string v2, "Filter"
invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
const-string v1, "subtract"
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
return v0
.end method
.method public c()Z
.registers 4
iget-object v0, p0, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;->k:Lorg/w3c/dom/Element;
const/4 v1, 0x0
const-string v2, "Filter"
invoke-interface {v0, v1, v2}, Lorg/w3c/dom/Element;->getAttributeNS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
const-string v1, "union"
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
return v0
.end method
.method public final d()Ljava/lang/String;
.registers 2
const-string v0, "http://www.w3.org/2002/06/xmldsig-filter2"
return-object v0
.end method
.method public final e()Ljava/lang/String;
.registers 2
const-string v0, "XPath"
return-object v0
.end method
.method public f()Lorg/w3c/dom/Node;
.registers 6
iget-object v0, p0, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;->k:Lorg/w3c/dom/Element;
invoke-interface {v0}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;
move-result-object v1
invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I
move-result v2
const/4 v0, 0x0
:goto_b
if-ge v0, v2, :cond_20
invoke-interface {v1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;
move-result-object v3
invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeType()S
move-result v3
const/4 v4, 0x3
if-ne v3, v4, :cond_1d
invoke-interface {v1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;
move-result-object v0
:goto_1c
return-object v0
:cond_1d
add-int/lit8 v0, v0, 0x1
goto :goto_b
:cond_20
const/4 v0, 0x0
goto :goto_1c
.end method