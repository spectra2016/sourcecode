.class public Lorg/apache/xml/security/transforms/implementations/TransformXPointer;
.super Lorg/apache/xml/security/transforms/TransformSpi;
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Lorg/apache/xml/security/transforms/TransformSpi;-><init>()V
return-void
.end method
.method protected a(Lorg/apache/xml/security/signature/XMLSignatureInput;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;
.registers 6
const/4 v0, 0x1
new-array v0, v0, [Ljava/lang/Object;
const/4 v1, 0x0
const-string v2, "http://www.w3.org/TR/2001/WD-xptr-20010108"
aput-object v2, v0, v1
new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;
const-string v2, "signature.Transform.NotYetImplemented"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v1
.end method