.class  Lorg/apache/xml/security/transforms/implementations/TransformEnvelopedSignature$EnvelopedNodeFilter;
.super Ljava/lang/Object;
.implements Lorg/apache/xml/security/signature/NodeFilter;
.field  a:Lorg/w3c/dom/Node;
.method constructor <init>(Lorg/w3c/dom/Node;)V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lorg/apache/xml/security/transforms/implementations/TransformEnvelopedSignature$EnvelopedNodeFilter;->a:Lorg/w3c/dom/Node;
return-void
.end method
.method public a(Lorg/w3c/dom/Node;)I
.registers 3
iget-object v0, p0, Lorg/apache/xml/security/transforms/implementations/TransformEnvelopedSignature$EnvelopedNodeFilter;->a:Lorg/w3c/dom/Node;
if-eq p1, v0, :cond_c
iget-object v0, p0, Lorg/apache/xml/security/transforms/implementations/TransformEnvelopedSignature$EnvelopedNodeFilter;->a:Lorg/w3c/dom/Node;
invoke-static {v0, p1}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)Z
move-result v0
if-eqz v0, :cond_e
:cond_c
const/4 v0, -0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x1
goto :goto_d
.end method
.method public a(Lorg/w3c/dom/Node;I)I
.registers 4
iget-object v0, p0, Lorg/apache/xml/security/transforms/implementations/TransformEnvelopedSignature$EnvelopedNodeFilter;->a:Lorg/w3c/dom/Node;
if-ne p1, v0, :cond_6
const/4 v0, -0x1
:goto_5
return v0
:cond_6
const/4 v0, 0x1
goto :goto_5
.end method