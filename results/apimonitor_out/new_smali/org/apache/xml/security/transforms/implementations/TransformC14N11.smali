.class public Lorg/apache/xml/security/transforms/implementations/TransformC14N11;
.super Lorg/apache/xml/security/transforms/TransformSpi;
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Lorg/apache/xml/security/transforms/TransformSpi;-><init>()V
return-void
.end method
.method protected a(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;
.registers 6
new-instance v0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer11_OmitComments;
invoke-direct {v0}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer11_OmitComments;-><init>()V
if-eqz p2, :cond_a
invoke-virtual {v0, p2}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer11_OmitComments;->a(Ljava/io/OutputStream;)V
:cond_a
invoke-virtual {v0, p1}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer11_OmitComments;->b(Lorg/apache/xml/security/signature/XMLSignatureInput;)[B
move-result-object v0
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureInput;
invoke-direct {v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureInput;-><init>([B)V
if-eqz p2, :cond_18
invoke-virtual {v1, p2}, Lorg/apache/xml/security/signature/XMLSignatureInput;->b(Ljava/io/OutputStream;)V
:cond_18
return-object v1
.end method
.method protected a(Lorg/apache/xml/security/signature/XMLSignatureInput;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;
.registers 4
const/4 v0, 0x0
invoke-virtual {p0, p1, v0, p2}, Lorg/apache/xml/security/transforms/implementations/TransformC14N11;->a(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;
move-result-object v0
return-object v0
.end method