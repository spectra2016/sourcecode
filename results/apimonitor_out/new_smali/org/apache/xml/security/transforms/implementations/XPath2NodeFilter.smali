.class  Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;
.super Ljava/lang/Object;
.implements Lorg/apache/xml/security/signature/NodeFilter;
.field  a:Z
.field  b:Z
.field  c:Z
.field  d:Ljava/util/Set;
.field  e:Ljava/util/Set;
.field  f:Ljava/util/Set;
.field  g:I
.field  h:I
.field  i:I
.method constructor <init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
.registers 7
const/4 v1, 0x1
const/4 v2, 0x0
const/4 v0, -0x1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput v0, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->g:I
iput v0, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->h:I
iput v0, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->i:I
iput-object p1, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->d:Ljava/util/Set;
invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z
move-result v0
if-nez v0, :cond_2d
move v0, v1
:goto_15
iput-boolean v0, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->a:Z
iput-object p2, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->e:Ljava/util/Set;
invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z
move-result v0
if-nez v0, :cond_2f
move v0, v1
:goto_20
iput-boolean v0, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->b:Z
iput-object p3, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->f:Ljava/util/Set;
invoke-interface {p3}, Ljava/util/Set;->isEmpty()Z
move-result v0
if-nez v0, :cond_31
:goto_2a
iput-boolean v1, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->c:Z
return-void
:cond_2d
move v0, v2
goto :goto_15
:cond_2f
move v0, v2
goto :goto_20
:cond_31
move v1, v2
goto :goto_2a
.end method
.method static a(Lorg/w3c/dom/Node;Ljava/util/Set;)Z
.registers 5
const/4 v1, 0x1
invoke-interface {p1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_9
move v0, v1
:goto_8
return v0
:cond_9
invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
move-result-object v2
:cond_d
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_21
invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/w3c/dom/Node;
invoke-static {v0, p0}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)Z
move-result v0
if-eqz v0, :cond_d
move v0, v1
goto :goto_8
:cond_21
const/4 v0, 0x0
goto :goto_8
.end method
.method static b(Lorg/w3c/dom/Node;Ljava/util/Set;)Z
.registers 3
invoke-interface {p1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
move-result v0
return v0
.end method
.method public a(Lorg/w3c/dom/Node;)I
.registers 6
const/4 v0, 0x0
const/4 v2, 0x1
iget-boolean v1, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->b:Z
if-eqz v1, :cond_13
iget-object v1, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->e:Ljava/util/Set;
invoke-static {p1, v1}, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->a(Lorg/w3c/dom/Node;Ljava/util/Set;)Z
move-result v1
if-eqz v1, :cond_13
const/4 v1, -0x1
:goto_f
if-ne v1, v2, :cond_21
move v0, v2
:cond_12
:goto_12
return v0
:cond_13
iget-boolean v1, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->c:Z
if-eqz v1, :cond_31
iget-object v1, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->f:Ljava/util/Set;
invoke-static {p1, v1}, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->a(Lorg/w3c/dom/Node;Ljava/util/Set;)Z
move-result v1
if-nez v1, :cond_31
move v1, v0
goto :goto_f
:cond_21
iget-boolean v3, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->a:Z
if-eqz v3, :cond_2f
iget-object v1, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->d:Ljava/util/Set;
invoke-static {p1, v1}, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->a(Lorg/w3c/dom/Node;Ljava/util/Set;)Z
move-result v1
if-eqz v1, :cond_12
move v0, v2
goto :goto_12
:cond_2f
move v0, v1
goto :goto_12
:cond_31
move v1, v2
goto :goto_f
.end method
.method public a(Lorg/w3c/dom/Node;I)I
.registers 8
const/4 v1, 0x0
const/4 v2, 0x1
const/4 v3, -0x1
iget-boolean v0, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->b:Z
if-eqz v0, :cond_61
iget v0, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->g:I
if-eq v0, v3, :cond_f
iget v0, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->g:I
if-gt p2, v0, :cond_19
:cond_f
iget-object v0, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->e:Ljava/util/Set;
invoke-static {p1, v0}, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->b(Lorg/w3c/dom/Node;Ljava/util/Set;)Z
move-result v0
if-eqz v0, :cond_41
iput p2, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->g:I
:cond_19
:goto_19
iget v0, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->g:I
if-eq v0, v3, :cond_61
move v0, v3
:goto_1e
if-eq v0, v3, :cond_37
iget-boolean v4, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->c:Z
if-eqz v4, :cond_37
iget v4, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->h:I
if-eq v4, v3, :cond_2c
iget v4, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->h:I
if-gt p2, v4, :cond_37
:cond_2c
iget-object v4, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->f:Ljava/util/Set;
invoke-static {p1, v4}, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->b(Lorg/w3c/dom/Node;Ljava/util/Set;)Z
move-result v4
if-nez v4, :cond_44
iput v3, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->h:I
move v0, v1
:goto_37
:cond_37
iget v4, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->i:I
if-gt p2, v4, :cond_3d
iput v3, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->i:I
:cond_3d
if-ne v0, v2, :cond_47
move v1, v2
:goto_40
:cond_40
return v1
:cond_41
iput v3, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->g:I
goto :goto_19
:cond_44
iput p2, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->h:I
goto :goto_37
:cond_47
iget-boolean v4, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->a:Z
if-eqz v4, :cond_5f
iget v0, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->i:I
if-ne v0, v3, :cond_59
iget-object v0, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->d:Ljava/util/Set;
invoke-static {p1, v0}, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->b(Lorg/w3c/dom/Node;Ljava/util/Set;)Z
move-result v0
if-eqz v0, :cond_59
iput p2, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->i:I
:cond_59
iget v0, p0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;->i:I
if-eq v0, v3, :cond_40
move v1, v2
goto :goto_40
:cond_5f
move v1, v0
goto :goto_40
:cond_61
move v0, v2
goto :goto_1e
.end method