.class public Lorg/apache/xml/security/transforms/implementations/TransformBase64Decode;
.super Lorg/apache/xml/security/transforms/TransformSpi;
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Lorg/apache/xml/security/transforms/TransformSpi;-><init>()V
return-void
.end method
.method protected a(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;
.registers 7
:try_start_0
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->g()Z
move-result v0
if-eqz v0, :cond_47
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->m()Lorg/w3c/dom/Node;
move-result-object v0
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->m()Lorg/w3c/dom/Node;
move-result-object v1
invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeType()S
move-result v1
const/4 v2, 0x3
if-ne v1, v2, :cond_19
invoke-interface {v0}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;
move-result-object v0
:cond_19
new-instance v1, Ljava/lang/StringBuffer;
invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V
check-cast v0, Lorg/w3c/dom/Element;
invoke-virtual {p0, v0, v1}, Lorg/apache/xml/security/transforms/implementations/TransformBase64Decode;->a(Lorg/w3c/dom/Element;Ljava/lang/StringBuffer;)V
if-nez p2, :cond_33
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/Base64;->a(Ljava/lang/String;)[B
move-result-object v1
new-instance v0, Lorg/apache/xml/security/signature/XMLSignatureInput;
invoke-direct {v0, v1}, Lorg/apache/xml/security/signature/XMLSignatureInput;-><init>([B)V
:goto_32
return-object v0
:cond_33
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
invoke-static {v0, p2}, Lorg/apache/xml/security/utils/Base64;->a(Ljava/lang/String;Ljava/io/OutputStream;)V
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureInput;
const/4 v0, 0x0
check-cast v0, [B
invoke-direct {v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureInput;-><init>([B)V
invoke-virtual {v1, p2}, Lorg/apache/xml/security/signature/XMLSignatureInput;->b(Ljava/io/OutputStream;)V
move-object v0, v1
goto :goto_32
:cond_47
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->h()Z
move-result v0
if-nez v0, :cond_53
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->f()Z
move-result v0
if-eqz v0, :cond_99
:cond_53
if-nez p2, :cond_6c
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->e()[B
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/Base64;->a([B)[B
move-result-object v1
new-instance v0, Lorg/apache/xml/security/signature/XMLSignatureInput;
invoke-direct {v0, v1}, Lorg/apache/xml/security/signature/XMLSignatureInput;-><init>([B)V
:try_end_62
.catch Lorg/apache/xml/security/exceptions/Base64DecodingException; {:try_start_0 .. :try_end_62} :catch_63
goto :goto_32
:catch_63
move-exception v0
new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;
const-string v2, "Base64Decoding"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:try_start_6c
:cond_6c
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->j()Z
move-result v0
if-nez v0, :cond_78
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->f()Z
move-result v0
if-eqz v0, :cond_8c
:cond_78
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->e()[B
move-result-object v0
invoke-static {v0, p2}, Lorg/apache/xml/security/utils/Base64;->a([BLjava/io/OutputStream;)V
:goto_7f
new-instance v1, Lorg/apache/xml/security/signature/XMLSignatureInput;
const/4 v0, 0x0
check-cast v0, [B
invoke-direct {v1, v0}, Lorg/apache/xml/security/signature/XMLSignatureInput;-><init>([B)V
invoke-virtual {v1, p2}, Lorg/apache/xml/security/signature/XMLSignatureInput;->b(Ljava/io/OutputStream;)V
move-object v0, v1
goto :goto_32
:cond_8c
new-instance v0, Ljava/io/BufferedInputStream;
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->d()Ljava/io/InputStream;
move-result-object v1
invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
invoke-static {v0, p2}, Lorg/apache/xml/security/utils/Base64;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
:try_end_98
.catch Lorg/apache/xml/security/exceptions/Base64DecodingException; {:try_start_6c .. :try_end_98} :catch_63
goto :goto_7f
:cond_99
:try_start_99
invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;
move-result-object v0
invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
move-result-object v0
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->c()Ljava/io/InputStream;
move-result-object v1
invoke-virtual {v0, v1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
move-result-object v0
invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;
move-result-object v0
new-instance v1, Ljava/lang/StringBuffer;
invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V
invoke-virtual {p0, v0, v1}, Lorg/apache/xml/security/transforms/implementations/TransformBase64Decode;->a(Lorg/w3c/dom/Element;Ljava/lang/StringBuffer;)V
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/Base64;->a(Ljava/lang/String;)[B
move-result-object v1
new-instance v0, Lorg/apache/xml/security/signature/XMLSignatureInput;
invoke-direct {v0, v1}, Lorg/apache/xml/security/signature/XMLSignatureInput;-><init>([B)V
:try_end_c2
.catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_99 .. :try_end_c2} :catch_c4
.catch Lorg/xml/sax/SAXException; {:try_start_99 .. :try_end_c2} :catch_cd
.catch Lorg/apache/xml/security/exceptions/Base64DecodingException; {:try_start_99 .. :try_end_c2} :catch_63
goto/16 :goto_32
:catch_c4
move-exception v0
:try_start_c5
new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;
const-string v2, "c14n.Canonicalizer.Exception"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:catch_cd
move-exception v0
new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;
const-string v2, "SAX exception"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:try_end_d6
.catch Lorg/apache/xml/security/exceptions/Base64DecodingException; {:try_start_c5 .. :try_end_d6} :catch_63
.end method
.method protected a(Lorg/apache/xml/security/signature/XMLSignatureInput;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;
.registers 4
const/4 v0, 0x0
invoke-virtual {p0, p1, v0, p2}, Lorg/apache/xml/security/transforms/implementations/TransformBase64Decode;->a(Lorg/apache/xml/security/signature/XMLSignatureInput;Ljava/io/OutputStream;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;
move-result-object v0
return-object v0
.end method
.method  a(Lorg/w3c/dom/Element;Ljava/lang/StringBuffer;)V
.registers 5
invoke-interface {p1}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v1
:goto_4
if-eqz v1, :cond_24
invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeType()S
move-result v0
packed-switch v0, :pswitch_data_26
:pswitch_d
:goto_d
invoke-interface {v1}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;
move-result-object v1
goto :goto_4
:pswitch_12
move-object v0, v1
check-cast v0, Lorg/w3c/dom/Element;
invoke-virtual {p0, v0, p2}, Lorg/apache/xml/security/transforms/implementations/TransformBase64Decode;->a(Lorg/w3c/dom/Element;Ljava/lang/StringBuffer;)V
goto :goto_d
:pswitch_19
move-object v0, v1
check-cast v0, Lorg/w3c/dom/Text;
invoke-interface {v0}, Lorg/w3c/dom/Text;->getData()Ljava/lang/String;
move-result-object v0
invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
goto :goto_d
:cond_24
return-void
nop
:pswitch_data_26
.packed-switch 0x1
:pswitch_12
:pswitch_d
:pswitch_19
.end packed-switch
.end method