.class public Lorg/apache/xml/security/transforms/implementations/TransformXPath2Filter;
.super Lorg/apache/xml/security/transforms/TransformSpi;
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Lorg/apache/xml/security/transforms/TransformSpi;-><init>()V
return-void
.end method
.method static a(Ljava/util/List;)Ljava/util/Set;
.registers 8
const/4 v2, 0x0
new-instance v4, Ljava/util/HashSet;
invoke-direct {v4}, Ljava/util/HashSet;-><init>()V
move v1, v2
:goto_7
invoke-interface {p0}, Ljava/util/List;->size()I
move-result v0
if-ge v1, v0, :cond_28
invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/w3c/dom/NodeList;
invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I
move-result v5
move v3, v2
:goto_18
if-ge v3, v5, :cond_24
invoke-interface {v0, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;
move-result-object v6
invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
add-int/lit8 v3, v3, 0x1
goto :goto_18
:cond_24
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_7
:cond_28
return-object v4
.end method
.method protected a(Lorg/apache/xml/security/signature/XMLSignatureInput;Lorg/apache/xml/security/transforms/Transform;)Lorg/apache/xml/security/signature/XMLSignatureInput;
.registers 14
const/4 v1, 0x0
invoke-virtual {p2}, Lorg/apache/xml/security/transforms/Transform;->k()Lorg/w3c/dom/Element;
move-result-object v0
invoke-interface {v0}, Lorg/w3c/dom/Element;->getOwnerDocument()Lorg/w3c/dom/Document;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/CachedXPathAPIHolder;->a(Lorg/w3c/dom/Document;)V
:try_start_c
new-instance v2, Ljava/util/ArrayList;
invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
new-instance v3, Ljava/util/ArrayList;
invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V
new-instance v4, Ljava/util/ArrayList;
invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V
new-instance v5, Lorg/apache/xml/security/utils/CachedXPathFuncHereAPI;
invoke-static {}, Lorg/apache/xml/security/utils/CachedXPathAPIHolder;->a()Lorg/apache/xpath/CachedXPathAPI;
move-result-object v0
invoke-direct {v5, v0}, Lorg/apache/xml/security/utils/CachedXPathFuncHereAPI;-><init>(Lorg/apache/xpath/CachedXPathAPI;)V
invoke-virtual {p2}, Lorg/apache/xml/security/transforms/Transform;->k()Lorg/w3c/dom/Element;
move-result-object v0
invoke-interface {v0}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v0
const-string v6, "http://www.w3.org/2002/06/xmldsig-filter2"
const-string v7, "XPath"
invoke-static {v0, v6, v7}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)[Lorg/w3c/dom/Element;
move-result-object v0
array-length v6, v0
if-nez v6, :cond_55
const/4 v0, 0x2
new-array v0, v0, [Ljava/lang/Object;
const/4 v1, 0x0
const-string v2, "http://www.w3.org/2002/06/xmldsig-filter2"
aput-object v2, v0, v1
const/4 v1, 0x1
const-string v2, "XPath"
aput-object v2, v0, v1
new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;
const-string v2, "xml.WrongContent"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v1
:try_end_4c
.catch Ljavax/xml/transform/TransformerException; {:try_start_c .. :try_end_4c} :catch_4c
.catch Lorg/w3c/dom/DOMException; {:try_start_c .. :try_end_4c} :catch_b0
.catch Lorg/apache/xml/security/c14n/CanonicalizationException; {:try_start_c .. :try_end_4c} :catch_c3
.catch Lorg/apache/xml/security/c14n/InvalidCanonicalizerException; {:try_start_c .. :try_end_4c} :catch_e5
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_c .. :try_end_4c} :catch_ee
.catch Lorg/xml/sax/SAXException; {:try_start_c .. :try_end_4c} :catch_f7
.catch Ljava/io/IOException; {:try_start_c .. :try_end_4c} :catch_100
.catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_c .. :try_end_4c} :catch_109
:catch_4c
move-exception v0
new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:cond_55
:try_start_55
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->m()Lorg/w3c/dom/Node;
move-result-object v0
if-eqz v0, :cond_9d
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->m()Lorg/w3c/dom/Node;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->b(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Document;
move-result-object v0
:goto_63
if-ge v1, v6, :cond_cc
invoke-virtual {p2}, Lorg/apache/xml/security/transforms/Transform;->k()Lorg/w3c/dom/Element;
move-result-object v7
invoke-interface {v7}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;
move-result-object v7
const-string v8, "http://www.w3.org/2002/06/xmldsig-filter2"
const-string v9, "XPath"
invoke-static {v7, v8, v9, v1}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;I)Lorg/w3c/dom/Element;
move-result-object v7
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->k()Ljava/lang/String;
move-result-object v8
invoke-static {v7, v8}, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;->a(Lorg/w3c/dom/Element;Ljava/lang/String;)Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;
move-result-object v7
invoke-virtual {v7}, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;->f()Lorg/w3c/dom/Node;
move-result-object v8
invoke-virtual {v7}, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;->f()Lorg/w3c/dom/Node;
move-result-object v9
invoke-static {v9}, Lorg/apache/xml/security/utils/CachedXPathFuncHereAPI;->a(Lorg/w3c/dom/Node;)Ljava/lang/String;
move-result-object v9
invoke-virtual {v7}, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;->k()Lorg/w3c/dom/Element;
move-result-object v10
invoke-virtual {v5, v0, v8, v9, v10}, Lorg/apache/xml/security/utils/CachedXPathFuncHereAPI;->a(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Ljava/lang/String;Lorg/w3c/dom/Node;)Lorg/w3c/dom/NodeList;
move-result-object v8
invoke-virtual {v7}, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;->a()Z
move-result v9
if-eqz v9, :cond_a6
invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
:goto_9a
:cond_9a
add-int/lit8 v1, v1, 0x1
goto :goto_63
:cond_9d
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->b()Ljava/util/Set;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->a(Ljava/util/Set;)Lorg/w3c/dom/Document;
move-result-object v0
goto :goto_63
:cond_a6
invoke-virtual {v7}, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;->b()Z
move-result v9
if-eqz v9, :cond_b9
invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
:try_end_af
.catch Ljavax/xml/transform/TransformerException; {:try_start_55 .. :try_end_af} :catch_4c
.catch Lorg/w3c/dom/DOMException; {:try_start_55 .. :try_end_af} :catch_b0
.catch Lorg/apache/xml/security/c14n/CanonicalizationException; {:try_start_55 .. :try_end_af} :catch_c3
.catch Lorg/apache/xml/security/c14n/InvalidCanonicalizerException; {:try_start_55 .. :try_end_af} :catch_e5
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_55 .. :try_end_af} :catch_ee
.catch Lorg/xml/sax/SAXException; {:try_start_55 .. :try_end_af} :catch_f7
.catch Ljava/io/IOException; {:try_start_55 .. :try_end_af} :catch_100
.catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_55 .. :try_end_af} :catch_109
goto :goto_9a
:catch_b0
move-exception v0
new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:cond_b9
:try_start_b9
invoke-virtual {v7}, Lorg/apache/xml/security/transforms/params/XPath2FilterContainer;->c()Z
move-result v7
if-eqz v7, :cond_9a
invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
:try_end_c2
.catch Ljavax/xml/transform/TransformerException; {:try_start_b9 .. :try_end_c2} :catch_4c
.catch Lorg/w3c/dom/DOMException; {:try_start_b9 .. :try_end_c2} :catch_b0
.catch Lorg/apache/xml/security/c14n/CanonicalizationException; {:try_start_b9 .. :try_end_c2} :catch_c3
.catch Lorg/apache/xml/security/c14n/InvalidCanonicalizerException; {:try_start_b9 .. :try_end_c2} :catch_e5
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_b9 .. :try_end_c2} :catch_ee
.catch Lorg/xml/sax/SAXException; {:try_start_b9 .. :try_end_c2} :catch_f7
.catch Ljava/io/IOException; {:try_start_b9 .. :try_end_c2} :catch_100
.catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_b9 .. :try_end_c2} :catch_109
goto :goto_9a
:catch_c3
move-exception v0
new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:cond_cc
:try_start_cc
new-instance v0, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;
invoke-static {v2}, Lorg/apache/xml/security/transforms/implementations/TransformXPath2Filter;->a(Ljava/util/List;)Ljava/util/Set;
move-result-object v1
invoke-static {v3}, Lorg/apache/xml/security/transforms/implementations/TransformXPath2Filter;->a(Ljava/util/List;)Ljava/util/Set;
move-result-object v2
invoke-static {v4}, Lorg/apache/xml/security/transforms/implementations/TransformXPath2Filter;->a(Ljava/util/List;)Ljava/util/Set;
move-result-object v3
invoke-direct {v0, v1, v2, v3}, Lorg/apache/xml/security/transforms/implementations/XPath2NodeFilter;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
invoke-virtual {p1, v0}, Lorg/apache/xml/security/signature/XMLSignatureInput;->a(Lorg/apache/xml/security/signature/NodeFilter;)V
const/4 v0, 0x1
invoke-virtual {p1, v0}, Lorg/apache/xml/security/signature/XMLSignatureInput;->d(Z)V
:try_end_e4
.catch Ljavax/xml/transform/TransformerException; {:try_start_cc .. :try_end_e4} :catch_4c
.catch Lorg/w3c/dom/DOMException; {:try_start_cc .. :try_end_e4} :catch_b0
.catch Lorg/apache/xml/security/c14n/CanonicalizationException; {:try_start_cc .. :try_end_e4} :catch_c3
.catch Lorg/apache/xml/security/c14n/InvalidCanonicalizerException; {:try_start_cc .. :try_end_e4} :catch_e5
.catch Lorg/apache/xml/security/exceptions/XMLSecurityException; {:try_start_cc .. :try_end_e4} :catch_ee
.catch Lorg/xml/sax/SAXException; {:try_start_cc .. :try_end_e4} :catch_f7
.catch Ljava/io/IOException; {:try_start_cc .. :try_end_e4} :catch_100
.catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_cc .. :try_end_e4} :catch_109
return-object p1
:catch_e5
move-exception v0
new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:catch_ee
move-exception v0
new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:catch_f7
move-exception v0
new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:catch_100
move-exception v0
new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
:catch_109
move-exception v0
new-instance v1, Lorg/apache/xml/security/transforms/TransformationException;
const-string v2, "empty"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/transforms/TransformationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V
throw v1
.end method