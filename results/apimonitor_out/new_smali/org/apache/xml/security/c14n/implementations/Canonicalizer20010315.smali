.class public abstract Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;
.super Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;
.field  b:Z
.field final c:Ljava/util/SortedSet;
.field  d:Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;
.method public constructor <init>(Z)V
.registers 4
invoke-direct {p0, p1}, Lorg/apache/xml/security/c14n/implementations/CanonicalizerBase;-><init>(Z)V
const/4 v0, 0x1
iput-boolean v0, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->b:Z
new-instance v0, Ljava/util/TreeSet;
sget-object v1, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->g:Lorg/apache/xml/security/c14n/helper/AttrCompare;
invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V
iput-object v0, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->c:Ljava/util/SortedSet;
new-instance v0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;
invoke-direct {v0}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;-><init>()V
iput-object v0, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->d:Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;
return-void
.end method
.method  a(Lorg/w3c/dom/Element;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;)Ljava/util/Iterator;
.registers 12
const/4 v2, 0x0
invoke-interface {p1}, Lorg/w3c/dom/Element;->hasAttributes()Z
move-result v0
if-nez v0, :cond_d
iget-boolean v0, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->b:Z
if-nez v0, :cond_d
const/4 v0, 0x0
:goto_c
return-object v0
:cond_d
iget-object v3, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->c:Ljava/util/SortedSet;
invoke-interface {v3}, Ljava/util/SortedSet;->clear()V
invoke-interface {p1}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;
move-result-object v4
invoke-interface {v4}, Lorg/w3c/dom/NamedNodeMap;->getLength()I
move-result v5
move v1, v2
:goto_1b
if-ge v1, v5, :cond_78
invoke-interface {v4, v1}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;
move-result-object v0
check-cast v0, Lorg/w3c/dom/Attr;
invoke-interface {v0}, Lorg/w3c/dom/Attr;->getNamespaceURI()Ljava/lang/String;
move-result-object v6
const-string v7, "http://www.w3.org/2000/xmlns/"
invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v6
if-nez v6, :cond_36
invoke-interface {v3, v0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z
:cond_32
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_1b
:cond_36
invoke-interface {v0}, Lorg/w3c/dom/Attr;->getLocalName()Ljava/lang/String;
move-result-object v6
invoke-interface {v0}, Lorg/w3c/dom/Attr;->getValue()Ljava/lang/String;
move-result-object v7
const-string v8, "xml"
invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v8
if-eqz v8, :cond_4e
const-string v8, "http://www.w3.org/XML/1998/namespace"
invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v8
if-nez v8, :cond_32
:cond_4e
invoke-virtual {p2, v6, v7, v0}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->b(Ljava/lang/String;Ljava/lang/String;Lorg/w3c/dom/Attr;)Lorg/w3c/dom/Node;
move-result-object v7
if-eqz v7, :cond_32
invoke-interface {v3, v7}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z
invoke-static {v0}, Lorg/apache/xml/security/c14n/helper/C14nHelper;->a(Lorg/w3c/dom/Attr;)Z
move-result v7
if-eqz v7, :cond_32
const/4 v1, 0x3
new-array v1, v1, [Ljava/lang/Object;
invoke-interface {p1}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;
move-result-object v3
aput-object v3, v1, v2
const/4 v2, 0x1
aput-object v6, v1, v2
const/4 v2, 0x2
invoke-interface {v0}, Lorg/w3c/dom/Attr;->getNodeValue()Ljava/lang/String;
move-result-object v0
aput-object v0, v1, v2
new-instance v0, Lorg/apache/xml/security/c14n/CanonicalizationException;
const-string v2, "c14n.Canonicalizer.RelativeNamespace"
invoke-direct {v0, v2, v1}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
:cond_78
iget-boolean v0, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->b:Z
if-eqz v0, :cond_86
invoke-virtual {p2, v3}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->a(Ljava/util/Collection;)V
iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->d:Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;
invoke-virtual {v0, v3}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;->a(Ljava/util/Collection;)V
iput-boolean v2, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->b:Z
:cond_86
invoke-interface {v3}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;
move-result-object v0
goto :goto_c
.end method
.method  a(Lorg/apache/xml/security/signature/XMLSignatureInput;)V
.registers 3
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->a()Z
move-result v0
if-nez v0, :cond_7
:goto_6
return-void
:cond_7
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->m()Lorg/w3c/dom/Node;
move-result-object v0
if-eqz v0, :cond_19
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->m()Lorg/w3c/dom/Node;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->b(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Document;
move-result-object v0
:goto_15
invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->a(Lorg/w3c/dom/Document;)V
goto :goto_6
:cond_19
invoke-virtual {p1}, Lorg/apache/xml/security/signature/XMLSignatureInput;->b()Ljava/util/Set;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/utils/XMLUtils;->a(Ljava/util/Set;)Lorg/w3c/dom/Document;
move-result-object v0
goto :goto_15
.end method
.method public a(Lorg/w3c/dom/Node;Ljava/lang/String;)[B
.registers 5
new-instance v0, Lorg/apache/xml/security/c14n/CanonicalizationException;
const-string v1, "c14n.Canonicalizer.UnsupportedOperation"
invoke-direct {v0, v1}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;)V
throw v0
.end method
.method  b(Lorg/w3c/dom/Element;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;)Ljava/util/Iterator;
.registers 15
const/4 v6, 0x0
const/4 v2, 0x1
const/4 v3, 0x0
iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->d:Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;
invoke-virtual {p2}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->f()I
move-result v1
invoke-virtual {v0, v1}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;->a(I)V
invoke-virtual {p2}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->f()I
move-result v0
invoke-virtual {p0, p1, v0}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->a(Lorg/w3c/dom/Node;I)I
move-result v0
if-ne v0, v2, :cond_52
move v1, v2
:goto_17
invoke-interface {p1}, Lorg/w3c/dom/Element;->hasAttributes()Z
move-result v0
if-eqz v0, :cond_f0
invoke-interface {p1}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;
move-result-object v4
invoke-interface {v4}, Lorg/w3c/dom/NamedNodeMap;->getLength()I
move-result v0
move-object v5, v4
move v4, v0
:goto_27
iget-object v8, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->c:Ljava/util/SortedSet;
invoke-interface {v8}, Ljava/util/SortedSet;->clear()V
move v7, v3
:goto_2d
if-ge v7, v4, :cond_bb
invoke-interface {v5, v7}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;
move-result-object v0
check-cast v0, Lorg/w3c/dom/Attr;
invoke-interface {v0}, Lorg/w3c/dom/Attr;->getNamespaceURI()Ljava/lang/String;
move-result-object v9
const-string v10, "http://www.w3.org/2000/xmlns/"
invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v10
if-nez v10, :cond_5a
const-string v10, "http://www.w3.org/XML/1998/namespace"
invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v9
if-eqz v9, :cond_54
iget-object v9, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->d:Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;
invoke-virtual {v9, v0}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;->a(Lorg/w3c/dom/Attr;)V
:cond_4e
:goto_4e
add-int/lit8 v0, v7, 0x1
move v7, v0
goto :goto_2d
:cond_52
move v1, v3
goto :goto_17
:cond_54
if-eqz v1, :cond_4e
invoke-interface {v8, v0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z
goto :goto_4e
:cond_5a
invoke-interface {v0}, Lorg/w3c/dom/Attr;->getLocalName()Ljava/lang/String;
move-result-object v9
invoke-interface {v0}, Lorg/w3c/dom/Attr;->getValue()Ljava/lang/String;
move-result-object v10
const-string v11, "xml"
invoke-virtual {v11, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v11
if-eqz v11, :cond_72
const-string v11, "http://www.w3.org/XML/1998/namespace"
invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v11
if-nez v11, :cond_4e
:cond_72
invoke-virtual {p0, v0}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->c(Lorg/w3c/dom/Node;)Z
move-result v11
if-eqz v11, :cond_a9
if-nez v1, :cond_80
invoke-virtual {p2, v9}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->e(Ljava/lang/String;)Z
move-result v11
if-nez v11, :cond_4e
:cond_80
invoke-virtual {p2, v9, v10, v0}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->b(Ljava/lang/String;Ljava/lang/String;Lorg/w3c/dom/Attr;)Lorg/w3c/dom/Node;
move-result-object v10
if-eqz v10, :cond_4e
invoke-interface {v8, v10}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z
invoke-static {v0}, Lorg/apache/xml/security/c14n/helper/C14nHelper;->a(Lorg/w3c/dom/Attr;)Z
move-result v10
if-eqz v10, :cond_4e
const/4 v1, 0x3
new-array v1, v1, [Ljava/lang/Object;
invoke-interface {p1}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;
move-result-object v4
aput-object v4, v1, v3
aput-object v9, v1, v2
const/4 v2, 0x2
invoke-interface {v0}, Lorg/w3c/dom/Attr;->getNodeValue()Ljava/lang/String;
move-result-object v0
aput-object v0, v1, v2
new-instance v0, Lorg/apache/xml/security/c14n/CanonicalizationException;
const-string v2, "c14n.Canonicalizer.RelativeNamespace"
invoke-direct {v0, v2, v1}, Lorg/apache/xml/security/c14n/CanonicalizationException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
:cond_a9
if-eqz v1, :cond_b7
const-string v11, "xmlns"
invoke-virtual {v11, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v11
if-nez v11, :cond_b7
invoke-virtual {p2, v9}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->c(Ljava/lang/String;)V
goto :goto_4e
:cond_b7
invoke-virtual {p2, v9, v10, v0}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->a(Ljava/lang/String;Ljava/lang/String;Lorg/w3c/dom/Attr;)Z
goto :goto_4e
:cond_bb
if-eqz v1, :cond_da
const-string v0, "http://www.w3.org/2000/xmlns/"
const-string v1, "xmlns"
invoke-interface {p1, v0, v1}, Lorg/w3c/dom/Element;->getAttributeNodeNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Attr;
move-result-object v0
if-nez v0, :cond_df
const-string v0, "xmlns"
invoke-virtual {p2, v0}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->a(Ljava/lang/String;)Lorg/w3c/dom/Attr;
move-result-object v6
:goto_cd
:cond_cd
if-eqz v6, :cond_d2
invoke-interface {v8, v6}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z
:cond_d2
iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->d:Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;
invoke-virtual {v0, v8}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;->a(Ljava/util/Collection;)V
invoke-virtual {p2, v8}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->a(Ljava/util/Collection;)V
:cond_da
invoke-interface {v8}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;
move-result-object v0
return-object v0
:cond_df
invoke-virtual {p0, v0}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->c(Lorg/w3c/dom/Node;)Z
move-result v0
if-nez v0, :cond_cd
const-string v0, "xmlns"
const-string v1, ""
sget-object v2, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->i:Lorg/w3c/dom/Attr;
invoke-virtual {p2, v0, v1, v2}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->b(Ljava/lang/String;Ljava/lang/String;Lorg/w3c/dom/Attr;)Lorg/w3c/dom/Node;
move-result-object v6
goto :goto_cd
:cond_f0
move v4, v3
move-object v5, v6
goto/16 :goto_27
.end method
.method  c(Lorg/w3c/dom/Element;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;)V
.registers 10
invoke-interface {p1}, Lorg/w3c/dom/Element;->hasAttributes()Z
move-result v0
if-nez v0, :cond_7
:cond_6
return-void
:cond_7
iget-object v0, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->d:Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;
const/4 v1, -0x1
invoke-virtual {v0, v1}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;->a(I)V
invoke-interface {p1}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;
move-result-object v2
invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I
move-result v3
const/4 v0, 0x0
move v1, v0
:goto_17
if-ge v1, v3, :cond_6
invoke-interface {v2, v1}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;
move-result-object v0
check-cast v0, Lorg/w3c/dom/Attr;
const-string v4, "http://www.w3.org/2000/xmlns/"
invoke-interface {v0}, Lorg/w3c/dom/Attr;->getNamespaceURI()Ljava/lang/String;
move-result-object v5
invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v4
if-nez v4, :cond_40
const-string v4, "http://www.w3.org/XML/1998/namespace"
invoke-interface {v0}, Lorg/w3c/dom/Attr;->getNamespaceURI()Ljava/lang/String;
move-result-object v5
invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v4
if-eqz v4, :cond_3c
iget-object v4, p0, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315;->d:Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;
invoke-virtual {v4, v0}, Lorg/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;->a(Lorg/w3c/dom/Attr;)V
:goto_3c
:cond_3c
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_17
:cond_40
invoke-interface {v0}, Lorg/w3c/dom/Attr;->getLocalName()Ljava/lang/String;
move-result-object v4
invoke-interface {v0}, Lorg/w3c/dom/Attr;->getNodeValue()Ljava/lang/String;
move-result-object v5
const-string v6, "xml"
invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v6
if-eqz v6, :cond_58
const-string v6, "http://www.w3.org/XML/1998/namespace"
invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v6
if-nez v6, :cond_3c
:cond_58
invoke-virtual {p2, v4, v5, v0}, Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;->a(Ljava/lang/String;Ljava/lang/String;Lorg/w3c/dom/Attr;)Z
goto :goto_3c
.end method