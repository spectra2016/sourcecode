.class public Lorg/apache/xml/security/c14n/Canonicalizer;
.super Ljava/lang/Object;
.field static a:Z
.field static b:Ljava/util/Map;
.field protected c:Lorg/apache/xml/security/c14n/CanonicalizerSpi;
.method static constructor <clinit>()V
.registers 1
const/4 v0, 0x0
sput-boolean v0, Lorg/apache/xml/security/c14n/Canonicalizer;->a:Z
const/4 v0, 0x0
sput-object v0, Lorg/apache/xml/security/c14n/Canonicalizer;->b:Ljava/util/Map;
return-void
.end method
.method private constructor <init>(Ljava/lang/String;)V
.registers 5
const/4 v2, 0x1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, 0x0
iput-object v0, p0, Lorg/apache/xml/security/c14n/Canonicalizer;->c:Lorg/apache/xml/security/c14n/CanonicalizerSpi;
:try_start_7
invoke-static {p1}, Lorg/apache/xml/security/c14n/Canonicalizer;->b(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lorg/apache/xml/security/c14n/CanonicalizerSpi;
iput-object v0, p0, Lorg/apache/xml/security/c14n/Canonicalizer;->c:Lorg/apache/xml/security/c14n/CanonicalizerSpi;
iget-object v0, p0, Lorg/apache/xml/security/c14n/Canonicalizer;->c:Lorg/apache/xml/security/c14n/CanonicalizerSpi;
const/4 v1, 0x1
iput-boolean v1, v0, Lorg/apache/xml/security/c14n/CanonicalizerSpi;->a:Z
:try_end_18
.catch Ljava/lang/Exception; {:try_start_7 .. :try_end_18} :catch_19
return-void
:catch_19
move-exception v0
new-array v0, v2, [Ljava/lang/Object;
const/4 v1, 0x0
aput-object p1, v0, v1
new-instance v1, Lorg/apache/xml/security/c14n/InvalidCanonicalizerException;
const-string v2, "signature.Canonicalizer.UnknownCanonicalizer"
invoke-direct {v1, v2, v0}, Lorg/apache/xml/security/c14n/InvalidCanonicalizerException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v1
.end method
.method public static final a(Ljava/lang/String;)Lorg/apache/xml/security/c14n/Canonicalizer;
.registers 2
new-instance v0, Lorg/apache/xml/security/c14n/Canonicalizer;
invoke-direct {v0, p0}, Lorg/apache/xml/security/c14n/Canonicalizer;-><init>(Ljava/lang/String;)V
return-object v0
.end method
.method public static a()V
.registers 2
sget-boolean v0, Lorg/apache/xml/security/c14n/Canonicalizer;->a:Z
if-nez v0, :cond_10
new-instance v0, Ljava/util/HashMap;
const/16 v1, 0xa
invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V
sput-object v0, Lorg/apache/xml/security/c14n/Canonicalizer;->b:Ljava/util/Map;
const/4 v0, 0x1
sput-boolean v0, Lorg/apache/xml/security/c14n/Canonicalizer;->a:Z
:cond_10
return-void
.end method
.method public static a(Ljava/lang/String;Ljava/lang/String;)V
.registers 5
invoke-static {p0}, Lorg/apache/xml/security/c14n/Canonicalizer;->b(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v0
if-eqz v0, :cond_17
const/4 v1, 0x2
new-array v1, v1, [Ljava/lang/Object;
const/4 v2, 0x0
aput-object p0, v1, v2
const/4 v2, 0x1
aput-object v0, v1, v2
new-instance v0, Lorg/apache/xml/security/exceptions/AlgorithmAlreadyRegisteredException;
const-string v2, "algorithm.alreadyRegistered"
invoke-direct {v0, v2, v1}, Lorg/apache/xml/security/exceptions/AlgorithmAlreadyRegisteredException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V
throw v0
:try_start_17
:cond_17
sget-object v0, Lorg/apache/xml/security/c14n/Canonicalizer;->b:Ljava/util/Map;
invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v1
invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:try_end_20
.catch Ljava/lang/ClassNotFoundException; {:try_start_17 .. :try_end_20} :catch_21
return-void
:catch_21
move-exception v0
new-instance v0, Ljava/lang/RuntimeException;
const-string v1, "c14n class not found"
invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V
throw v0
.end method
.method private static b(Ljava/lang/String;)Ljava/lang/Class;
.registers 2
sget-object v0, Lorg/apache/xml/security/c14n/Canonicalizer;->b:Ljava/util/Map;
invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Class;
return-object v0
.end method
.method public a(Ljava/io/OutputStream;)V
.registers 3
iget-object v0, p0, Lorg/apache/xml/security/c14n/Canonicalizer;->c:Lorg/apache/xml/security/c14n/CanonicalizerSpi;
invoke-virtual {v0, p1}, Lorg/apache/xml/security/c14n/CanonicalizerSpi;->a(Ljava/io/OutputStream;)V
return-void
.end method
.method public a(Lorg/w3c/dom/Node;)[B
.registers 3
iget-object v0, p0, Lorg/apache/xml/security/c14n/Canonicalizer;->c:Lorg/apache/xml/security/c14n/CanonicalizerSpi;
invoke-virtual {v0, p1}, Lorg/apache/xml/security/c14n/CanonicalizerSpi;->a(Lorg/w3c/dom/Node;)[B
move-result-object v0
return-object v0
.end method
.method public a(Lorg/w3c/dom/Node;Ljava/lang/String;)[B
.registers 4
iget-object v0, p0, Lorg/apache/xml/security/c14n/Canonicalizer;->c:Lorg/apache/xml/security/c14n/CanonicalizerSpi;
invoke-virtual {v0, p1, p2}, Lorg/apache/xml/security/c14n/CanonicalizerSpi;->a(Lorg/w3c/dom/Node;Ljava/lang/String;)[B
move-result-object v0
return-object v0
.end method