.class public Lorg/apache/xml/security/c14n/helper/C14nHelper;
.super Ljava/lang/Object;
.method private constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public static a(Ljava/lang/String;)Z
.registers 2
invoke-static {p0}, Lorg/apache/xml/security/c14n/helper/C14nHelper;->b(Ljava/lang/String;)Z
move-result v0
if-nez v0, :cond_8
const/4 v0, 0x1
:goto_7
return v0
:cond_8
const/4 v0, 0x0
goto :goto_7
.end method
.method public static a(Lorg/w3c/dom/Attr;)Z
.registers 2
invoke-static {p0}, Lorg/apache/xml/security/c14n/helper/C14nHelper;->b(Lorg/w3c/dom/Attr;)Z
move-result v0
if-nez v0, :cond_8
const/4 v0, 0x1
:goto_7
return v0
:cond_8
const/4 v0, 0x0
goto :goto_7
.end method
.method public static b(Ljava/lang/String;)Z
.registers 3
const/4 v0, 0x1
invoke-virtual {p0}, Ljava/lang/String;->length()I
move-result v1
if-nez v1, :cond_8
:cond_7
:goto_7
return v0
:cond_8
const/16 v1, 0x3a
invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I
move-result v1
if-gtz v1, :cond_7
const/4 v0, 0x0
goto :goto_7
.end method
.method public static b(Lorg/w3c/dom/Attr;)Z
.registers 2
invoke-interface {p0}, Lorg/w3c/dom/Attr;->getValue()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/xml/security/c14n/helper/C14nHelper;->b(Ljava/lang/String;)Z
move-result v0
return v0
.end method