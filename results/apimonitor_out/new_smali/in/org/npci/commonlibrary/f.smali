.class public Lin/org/npci/commonlibrary/f;
.super Ljava/lang/Exception;
.field private a:I
.field private b:Ljava/lang/String;
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Exception;-><init>()V
return-void
.end method
.method public constructor <init>(Lin/org/npci/commonlibrary/g;)V
.registers 3
invoke-virtual {p1}, Lin/org/npci/commonlibrary/g;->a()Ljava/lang/String;
move-result-object v0
invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V
invoke-virtual {p1}, Lin/org/npci/commonlibrary/g;->b()I
move-result v0
iput v0, p0, Lin/org/npci/commonlibrary/f;->a:I
invoke-virtual {p1}, Lin/org/npci/commonlibrary/g;->a()Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lin/org/npci/commonlibrary/f;->b:Ljava/lang/String;
return-void
.end method
.method public toString()Ljava/lang/String;
.registers 3
new-instance v0, Ljava/lang/StringBuilder;
invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
const-string v1, "Error "
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
iget v1, p0, Lin/org/npci/commonlibrary/f;->a:I
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, " : "
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
iget-object v1, p0, Lin/org/npci/commonlibrary/f;->b:Ljava/lang/String;
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
return-object v0
.end method