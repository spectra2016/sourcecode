.class public Lin/org/npci/commonlibrary/l;
.super Ljava/lang/Object;
.field private static a:Ljava/security/cert/Certificate;
.method static constructor <clinit>()V
.registers 1
:try_start_0
const-string v0, "signer.crt"
invoke-static {v0}, Lin/org/npci/commonlibrary/h;->a(Ljava/lang/String;)Ljava/security/cert/Certificate;
move-result-object v0
sput-object v0, Lin/org/npci/commonlibrary/l;->a:Ljava/security/cert/Certificate;
:try_end_8
.catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_8} :catch_9
:goto_8
return-void
:catch_9
move-exception v0
invoke-virtual {v0}, Ljava/security/cert/CertificateException;->printStackTrace()V
goto :goto_8
.end method
.method public static a()Ljava/security/PublicKey;
.registers 1
sget-object v0, Lin/org/npci/commonlibrary/l;->a:Ljava/security/cert/Certificate;
if-eqz v0, :cond_b
sget-object v0, Lin/org/npci/commonlibrary/l;->a:Ljava/security/cert/Certificate;
invoke-virtual {v0}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;
move-result-object v0
:goto_a
return-object v0
:cond_b
const/4 v0, 0x0
goto :goto_a
.end method