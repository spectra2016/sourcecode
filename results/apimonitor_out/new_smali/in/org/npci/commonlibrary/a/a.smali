.class public Lin/org/npci/commonlibrary/a/a;
.super Ljava/lang/Object;
.method public static a(Ljava/lang/String;)Lorg/w3c/dom/Document;
.registers 4
invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;
move-result-object v0
const/4 v1, 0x1
invoke-virtual {v0, v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->setNamespaceAware(Z)V
invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
move-result-object v0
new-instance v1, Lorg/xml/sax/InputSource;
new-instance v2, Ljava/io/StringReader;
invoke-direct {v2, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V
invoke-direct {v1, v2}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V
invoke-virtual {v0, v1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;
move-result-object v0
return-object v0
.end method
.method public static a(Lorg/w3c/dom/Document;Ljava/security/PublicKey;)Z
.registers 5
const-string v0, "http://www.w3.org/2000/09/xmldsig#"
const-string v1, "Signature"
invoke-interface {p0, v0, v1}, Lorg/w3c/dom/Document;->getElementsByTagNameNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/NodeList;
move-result-object v0
invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I
move-result v1
if-nez v1, :cond_16
new-instance v0, Ljava/lang/Exception;
const-string v1, "Signature not found"
invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V
throw v0
:cond_16
new-instance v1, Lorg/apache/xml/security/signature/XMLSignature;
const/4 v2, 0x0
invoke-interface {v0, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;
move-result-object v0
check-cast v0, Lorg/w3c/dom/Element;
const-string v2, ""
invoke-direct {v1, v0, v2}, Lorg/apache/xml/security/signature/XMLSignature;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;)V
invoke-virtual {v1, p1}, Lorg/apache/xml/security/signature/XMLSignature;->a(Ljava/security/Key;)Z
move-result v0
return v0
.end method