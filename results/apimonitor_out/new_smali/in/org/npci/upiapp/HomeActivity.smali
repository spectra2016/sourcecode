.class public Lin/org/npci/upiapp/HomeActivity;
.super Landroid/support/v7/a/e;
.source "HomeActivity.java"
.field private static n:Ljava/lang/String;
.field private m:Lin/org/npci/upiapp/HomeActivity$a;
.field private o:Lin/juspay/mystique/d;
.field private p:Lin/org/npci/upiapp/core/JsInterface;
.field private q:Lin/org/npci/upiapp/core/b;
.field private r:Lin/org/npci/upiapp/core/NPCIJSInterface;
.field private s:Lin/org/npci/upiapp/core/QRScannerInterface;
.method static constructor <clinit>()V
.registers 1
const-string v0, ""
sput-object v0, Lin/org/npci/upiapp/HomeActivity;->n:Ljava/lang/String;
return-void
.end method
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Landroid/support/v7/a/e;-><init>()V
return-void
.end method
.method public static a(Landroid/graphics/BitmapFactory$Options;I)I
.registers 5
iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I
const/4 v0, 0x1
if-le v1, p1, :cond_e
div-int/lit8 v1, v1, 0x2
:goto_7
div-int v2, v1, v0
if-lt v2, p1, :cond_e
mul-int/lit8 v0, v0, 0x2
goto :goto_7
:cond_e
return v0
.end method
.method static synthetic a(Lin/org/npci/upiapp/HomeActivity;)Lin/juspay/mystique/d;
.registers 2
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
return-object v0
.end method
.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;)Ljava/lang/String;
.registers 7
new-instance v0, Landroid/content/ContextWrapper;
invoke-virtual {p0}, Lin/org/npci/upiapp/HomeActivity;->getApplicationContext()Landroid/content/Context;
move-result-object v1
invoke-direct {v0, v1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V
const-string v1, "imageDir"
const/4 v2, 0x0
invoke-virtual {v0, v1, v2}, Landroid/content/ContextWrapper;->getDir(Ljava/lang/String;I)Ljava/io/File;
move-result-object v0
new-instance v3, Ljava/io/File;
invoke-direct {v3, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
const/4 v2, 0x0
:try_start_16
new-instance v1, Ljava/io/FileOutputStream;
invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
:try_end_1b
.catchall {:try_start_16 .. :try_end_1b} :catchall_41
.catch Ljava/lang/Exception; {:try_start_16 .. :try_end_1b} :catch_31
:try_start_1b
sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;
const/16 v2, 0x64
invoke-virtual {p2, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
:try_end_22
.catchall {:try_start_1b .. :try_end_22} :catchall_4e
.catch Ljava/lang/Exception; {:try_start_1b .. :try_end_22} :catch_50
if-eqz v1, :cond_27
:try_start_24
invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
:try_end_27
.catch Ljava/io/IOException; {:try_start_24 .. :try_end_27} :catch_2c
:goto_27
:cond_27
invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
move-result-object v0
return-object v0
:catch_2c
move-exception v0
invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
goto :goto_27
:catch_31
move-exception v0
move-object v1, v2
:goto_33
:try_start_33
invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
:try_end_36
.catchall {:try_start_33 .. :try_end_36} :catchall_4e
if-eqz v1, :cond_27
:try_start_38
invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
:try_end_3b
.catch Ljava/io/IOException; {:try_start_38 .. :try_end_3b} :catch_3c
goto :goto_27
:catch_3c
move-exception v0
invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
goto :goto_27
:catchall_41
move-exception v0
move-object v1, v2
:goto_43
if-eqz v1, :cond_48
:try_start_45
invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
:goto_48
:try_end_48
.catch Ljava/io/IOException; {:try_start_45 .. :try_end_48} :catch_49
:cond_48
throw v0
:catch_49
move-exception v1
invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
goto :goto_48
:catchall_4e
move-exception v0
goto :goto_43
:catch_50
move-exception v0
goto :goto_33
.end method
.method private a(Landroid/content/BroadcastReceiver;)V
.registers 4
:try_start_0
new-instance v0, Landroid/content/IntentFilter;
invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V
const-string v1, "in.org.npci.upiapp.uibroadcastreceiver"
invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
invoke-static {p0}, Landroid/support/v4/a/i;->a(Landroid/content/Context;)Landroid/support/v4/a/i;
move-result-object v1
invoke-virtual {v1, p1, v0}, Landroid/support/v4/a/i;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V
:try_end_11
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_11} :catch_12
:goto_11
return-void
:catch_12
move-exception v0
goto :goto_11
.end method
.method private a(Landroid/os/Bundle;)V
.registers 6
const v0, 0x7f0d0065
invoke-virtual {p0, v0}, Lin/org/npci/upiapp/HomeActivity;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/FrameLayout;
new-instance v1, Lin/juspay/mystique/d;
const/4 v2, 0x0
new-instance v3, Lin/org/npci/upiapp/HomeActivity$1;
invoke-direct {v3, p0}, Lin/org/npci/upiapp/HomeActivity$1;-><init>(Lin/org/npci/upiapp/HomeActivity;)V
invoke-direct {v1, p0, v0, v2, v3}, Lin/juspay/mystique/d;-><init>(Landroid/app/Activity;Landroid/widget/FrameLayout;Landroid/os/Bundle;Lin/juspay/mystique/e;)V
iput-object v1, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
new-instance v0, Lin/org/npci/upiapp/core/JsInterface;
iget-object v1, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
invoke-direct {v0, p0, v1}, Lin/org/npci/upiapp/core/JsInterface;-><init>(Landroid/app/Activity;Lin/juspay/mystique/d;)V
iput-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->p:Lin/org/npci/upiapp/core/JsInterface;
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
iget-object v1, p0, Lin/org/npci/upiapp/HomeActivity;->p:Lin/org/npci/upiapp/core/JsInterface;
const-string v2, "JBridge"
invoke-virtual {v0, v1, v2}, Lin/juspay/mystique/d;->a(Ljava/lang/Object;Ljava/lang/String;)V
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
iget-object v1, p0, Lin/org/npci/upiapp/HomeActivity;->q:Lin/org/npci/upiapp/core/b;
const-string v2, "Tracker"
invoke-virtual {v0, v1, v2}, Lin/juspay/mystique/d;->a(Ljava/lang/Object;Ljava/lang/String;)V
if-eqz p1, :cond_3e
:try_start_33
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
const-string v1, "currentAppState"
invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->c(Ljava/lang/String;)V
:cond_3e
new-instance v0, Lin/org/npci/upiapp/core/JsInterface;
iget-object v1, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
invoke-direct {v0, p0, v1}, Lin/org/npci/upiapp/core/JsInterface;-><init>(Landroid/app/Activity;Lin/juspay/mystique/d;)V
iput-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->p:Lin/org/npci/upiapp/core/JsInterface;
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
iget-object v1, p0, Lin/org/npci/upiapp/HomeActivity;->p:Lin/org/npci/upiapp/core/JsInterface;
const-string v2, "JBridge"
invoke-virtual {v0, v1, v2}, Lin/juspay/mystique/d;->a(Ljava/lang/Object;Ljava/lang/String;)V
new-instance v0, Lin/org/npci/upiapp/core/NPCIJSInterface;
iget-object v1, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
invoke-direct {v0, p0, v1}, Lin/org/npci/upiapp/core/NPCIJSInterface;-><init>(Landroid/app/Activity;Lin/juspay/mystique/d;)V
iput-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->r:Lin/org/npci/upiapp/core/NPCIJSInterface;
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
iget-object v1, p0, Lin/org/npci/upiapp/HomeActivity;->r:Lin/org/npci/upiapp/core/NPCIJSInterface;
const-string v2, "NPCICL"
invoke-virtual {v0, v1, v2}, Lin/juspay/mystique/d;->a(Ljava/lang/Object;Ljava/lang/String;)V
new-instance v0, Lin/org/npci/upiapp/core/QRScannerInterface;
iget-object v1, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
invoke-direct {v0, p0, v1}, Lin/org/npci/upiapp/core/QRScannerInterface;-><init>(Landroid/app/Activity;Lin/juspay/mystique/d;)V
iput-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->s:Lin/org/npci/upiapp/core/QRScannerInterface;
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
iget-object v1, p0, Lin/org/npci/upiapp/HomeActivity;->s:Lin/org/npci/upiapp/core/QRScannerInterface;
const-string v2, "QRScanner"
invoke-virtual {v0, v1, v2}, Lin/juspay/mystique/d;->a(Ljava/lang/Object;Ljava/lang/String;)V
invoke-virtual {p0}, Lin/org/npci/upiapp/HomeActivity;->getResources()Landroid/content/res/Resources;
move-result-object v0
const v1, 0x7f080008
invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
move-result v0
if-eqz v0, :cond_8b
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0x13
if-lt v0, v1, :cond_8b
const/4 v0, 0x1
invoke-static {v0}, Landroid/webkit/WebView;->setWebContentsDebuggingEnabled(Z)V
:goto_8b
:try_end_8b
.catch Ljava/lang/Exception; {:try_start_33 .. :try_end_8b} :catch_8c
:cond_8b
return-void
:catch_8c
move-exception v0
invoke-static {v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V
goto :goto_8b
.end method
.method private b(Ljava/lang/String;)Landroid/graphics/Bitmap;
.registers 5
:try_start_0
new-instance v0, Landroid/content/ContextWrapper;
invoke-direct {v0, p0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V
const-string v1, "imageDir"
const/4 v2, 0x0
invoke-virtual {v0, v1, v2}, Landroid/content/ContextWrapper;->getDir(Ljava/lang/String;I)Ljava/io/File;
move-result-object v0
new-instance v1, Ljava/io/File;
invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
new-instance v0, Ljava/io/FileInputStream;
invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
:try_end_19
.catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_19} :catch_1b
move-result-object v0
:goto_1a
return-object v0
:catch_1b
move-exception v0
invoke-static {v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V
const/4 v0, 0x0
goto :goto_1a
.end method
.method private b(Landroid/content/BroadcastReceiver;)V
.registers 3
:try_start_0
invoke-static {p0}, Landroid/support/v4/a/i;->a(Landroid/content/Context;)Landroid/support/v4/a/i;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/support/v4/a/i;->a(Landroid/content/BroadcastReceiver;)V
:goto_7
:try_end_7
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_8
return-void
:catch_8
move-exception v0
goto :goto_7
.end method
.method private l()V
.registers 8
const/4 v4, -0x1
const/4 v6, 0x5
const/4 v0, 0x0
:try_start_3
const-string v1, "NPCI"
const/4 v2, 0x0
invoke-virtual {p0, v1, v2}, Lin/org/npci/upiapp/HomeActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
move-result-object v1
if-eqz v1, :cond_4d
const-string v2, "LAST_APP_VERSION"
const-string v2, "LAST_APP_VERSION"
const/4 v3, -0x1
invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
move-result v2
if-ne v2, v4, :cond_25
invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;
move-result-object v3
const-string v4, "LAST_APP_VERSION"
const/4 v5, 0x5
invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
move-result-object v3
invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V
:cond_25
if-le v6, v2, :cond_4d
const-string v2, "bhim"
const/4 v3, 0x0
invoke-virtual {p0, v2, v3}, Lin/org/npci/upiapp/HomeActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;
move-result-object v2
invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;
move-result-object v2
:goto_32
if-eqz v2, :cond_3f
array-length v3, v2
if-ge v0, v3, :cond_3f
aget-object v3, v2, v0
invoke-virtual {v3}, Ljava/io/File;->delete()Z
add-int/lit8 v0, v0, 0x1
goto :goto_32
:cond_3f
invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;
move-result-object v0
const-string v1, "LAST_APP_VERSION"
const/4 v2, 0x5
invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
move-result-object v0
invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
:try_end_4d
.catch Ljava/lang/Exception; {:try_start_3 .. :try_end_4d} :catch_4e
:goto_4d
:cond_4d
return-void
:catch_4e
move-exception v0
const-string v1, "HomeActivity"
const-string v2, "Exception: resetInternalStorageIfAppUpdated"
invoke-static {v1, v2, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
goto :goto_4d
.end method
.method private m()V
.registers 3
invoke-direct {p0}, Lin/org/npci/upiapp/HomeActivity;->q()Z
move-result v0
if-eqz v0, :cond_10
new-instance v0, Landroid/content/Intent;
const-class v1, Lin/org/npci/upiapp/gcm/RegistrationIntentService;
invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V
invoke-virtual {p0, v0}, Lin/org/npci/upiapp/HomeActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
:cond_10
return-void
.end method
.method private n()V
.registers 3
const-string v0, "PRODUCTION"
const-string v1, "PRODUCTION"
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_18
invoke-direct {p0}, Lin/org/npci/upiapp/HomeActivity;->p()Z
move-result v0
if-eqz v0, :cond_18
new-instance v0, Ljava/lang/RuntimeException;
const-string v1, "Debuggable mode wont work. Please download app from playstore"
invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V
throw v0
:cond_18
return-void
.end method
.method private o()V
.registers 2
const v0, 0x7f070037
invoke-virtual {p0, v0}, Lin/org/npci/upiapp/HomeActivity;->getString(I)Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lcom/crashlytics/android/Crashlytics;->setUserIdentifier(Ljava/lang/String;)V
const-string v0, "test@juspay.in"
invoke-static {v0}, Lcom/crashlytics/android/Crashlytics;->setUserEmail(Ljava/lang/String;)V
const-string v0, "Test"
invoke-static {v0}, Lcom/crashlytics/android/Crashlytics;->setUserName(Ljava/lang/String;)V
return-void
.end method
.method private p()Z
.registers 2
invoke-virtual {p0}, Lin/org/npci/upiapp/HomeActivity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;
move-result-object v0
iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I
and-int/lit8 v0, v0, 0x2
if-eqz v0, :cond_c
const/4 v0, 0x1
:goto_b
return v0
:cond_c
const/4 v0, 0x0
goto :goto_b
.end method
.method private q()Z
.registers 4
invoke-static {}, Lcom/google/android/gms/common/b;->a()Lcom/google/android/gms/common/b;
move-result-object v0
invoke-virtual {v0, p0}, Lcom/google/android/gms/common/b;->a(Landroid/content/Context;)I
move-result v1
if-eqz v1, :cond_23
invoke-virtual {v0, v1}, Lcom/google/android/gms/common/b;->a(I)Z
move-result v2
if-eqz v2, :cond_1b
const/16 v2, 0x2328
invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/common/b;->a(Landroid/app/Activity;II)Landroid/app/Dialog;
move-result-object v0
invoke-virtual {v0}, Landroid/app/Dialog;->show()V
:goto_19
const/4 v0, 0x0
:goto_1a
return v0
:cond_1b
const-string v0, "HomeActivity"
const-string v1, "This device is not supported for GCM"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_19
:cond_23
const/4 v0, 0x1
goto :goto_1a
.end method
.method public a(F)I
.registers 3
invoke-virtual {p0}, Lin/org/npci/upiapp/HomeActivity;->getResources()Landroid/content/res/Resources;
move-result-object v0
invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
move-result-object v0
iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I
div-int/lit16 v0, v0, 0xa0
int-to-float v0, v0
mul-float/2addr v0, p1
float-to-int v0, v0
return v0
.end method
.method public a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/graphics/Bitmap;
.registers 13
const/4 v0, 0x1
const/4 v7, 0x0
const/4 v6, 0x0
invoke-direct {p0, p1}, Lin/org/npci/upiapp/HomeActivity;->b(Ljava/lang/String;)Landroid/graphics/Bitmap;
move-result-object v1
const-string v2, "HomeActivity"
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "Saved Bitmap ? "
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
if-eqz v1, :cond_25
:goto_16
invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-static {v2, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
if-eqz v1, :cond_27
move-object v0, v1
:goto_24
:cond_24
return-object v0
:cond_25
move v0, v6
goto :goto_16
:cond_27
new-instance v5, Ljava/util/HashMap;
invoke-direct {v5}, Ljava/util/HashMap;-><init>()V
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/e;
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;->d:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;
invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b;
invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b;-><init>()V
:try_start_38
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->l:Lin/juspay/widget/qrscanner/com/google/zxing/a;
const/16 v3, 0x200
const/16 v4, 0x200
move-object v1, p2
invoke-virtual/range {v0 .. v5}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b;->a(Ljava/lang/String;Lin/juspay/widget/qrscanner/com/google/zxing/a;IILjava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
move-result-object v4
invoke-virtual {v4}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c()I
move-result v5
invoke-virtual {v4}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v8
sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;
invoke-static {v5, v8, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
move-result-object v0
move v3, v6
:goto_52
if-ge v3, v5, :cond_6c
move v2, v6
:goto_55
if-ge v2, v8, :cond_68
invoke-virtual {v4, v3, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v1
if-eqz v1, :cond_66
const/high16 v1, -0x100
:goto_5f
invoke-virtual {v0, v3, v2, v1}, Landroid/graphics/Bitmap;->setPixel(III)V
add-int/lit8 v1, v2, 0x1
move v2, v1
goto :goto_55
:cond_66
const/4 v1, -0x1
goto :goto_5f
:cond_68
add-int/lit8 v1, v3, 0x1
move v3, v1
goto :goto_52
:cond_6c
new-instance v1, Landroid/graphics/BitmapFactory$Options;
invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V
const/4 v2, 0x1
iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z
invoke-virtual {p0}, Lin/org/npci/upiapp/HomeActivity;->getResources()Landroid/content/res/Resources;
move-result-object v2
const v3, 0x7f020109
invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
const/4 v2, 0x0
iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z
const/high16 v2, 0x41a0
invoke-virtual {p0, v2}, Lin/org/npci/upiapp/HomeActivity;->a(F)I
move-result v2
invoke-static {v1, v2}, Lin/org/npci/upiapp/HomeActivity;->a(Landroid/graphics/BitmapFactory$Options;I)I
move-result v2
iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I
invoke-virtual {p0}, Lin/org/npci/upiapp/HomeActivity;->getResources()Landroid/content/res/Resources;
move-result-object v2
const v3, 0x7f020109
invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
move-result-object v1
new-instance v2, Landroid/graphics/Canvas;
invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V
invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I
move-result v3
invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I
move-result v4
new-instance v5, Landroid/graphics/Matrix;
invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V
const/4 v6, 0x0
invoke-virtual {v2, v0, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I
move-result v5
sub-int/2addr v3, v5
div-int/lit8 v3, v3, 0x2
invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I
move-result v5
sub-int/2addr v4, v5
div-int/lit8 v4, v4, 0x2
int-to-float v3, v3
int-to-float v4, v4
const/4 v5, 0x0
invoke-virtual {v2, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
if-eqz p3, :cond_24
invoke-direct {p0, p1, v0}, Lin/org/npci/upiapp/HomeActivity;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)Ljava/lang/String;
:try_end_c7
.catch Lin/juspay/widget/qrscanner/com/google/zxing/WriterException; {:try_start_38 .. :try_end_c7} :catch_c9
goto/16 :goto_24
:catch_c9
move-exception v0
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/WriterException;->printStackTrace()V
invoke-static {v0}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V
move-object v0, v7
goto/16 :goto_24
.end method
.method public a(Ljava/lang/String;)V
.registers 8
new-instance v2, Landroid/content/Intent;
invoke-direct {v2}, Landroid/content/Intent;-><init>()V
new-instance v3, Landroid/os/Bundle;
invoke-direct {v3}, Landroid/os/Bundle;-><init>()V
:try_start_a
new-instance v4, Lorg/json/JSONObject;
invoke-direct {v4, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
invoke-virtual {v4}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
move-result-object v5
:goto_13
invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_2d
invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
invoke-virtual {v4, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v1
check-cast v1, Ljava/lang/String;
invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
:try_end_28
.catch Ljava/lang/Exception; {:try_start_a .. :try_end_28} :catch_29
goto :goto_13
:catch_29
move-exception v0
invoke-static {v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V
:cond_2d
invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
const/4 v0, 0x0
invoke-virtual {p0, v0, v2}, Lin/org/npci/upiapp/HomeActivity;->setResult(ILandroid/content/Intent;)V
invoke-virtual {p0}, Lin/org/npci/upiapp/HomeActivity;->finish()V
return-void
.end method
.method public k()Ljava/lang/String;
.registers 5
invoke-virtual {p0}, Lin/org/npci/upiapp/HomeActivity;->getIntent()Landroid/content/Intent;
move-result-object v0
invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;
move-result-object v0
const-string v2, "android.intent.action.VIEW"
invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_37
if-eqz v0, :cond_37
const-string v1, "HomeActivity"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "DATA_FROM_MERCHANT - "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;
move-result-object v0
:goto_36
return-object v0
:cond_37
const/4 v0, 0x0
goto :goto_36
.end method
.method protected onActivityResult(IILandroid/content/Intent;)V
.registers 7
invoke-super {p0, p1, p2, p3}, Landroid/support/v7/a/e;->onActivityResult(IILandroid/content/Intent;)V
const-string v0, "HomeActivity"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Activity Result is "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
return-void
.end method
.method public onBackPressed()V
.registers 3
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
const-string v1, "window.onBackpressed()"
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
return-void
.end method
.method protected onCreate(Landroid/os/Bundle;)V
.registers 4
invoke-super {p0, p1}, Landroid/support/v7/a/e;->onCreate(Landroid/os/Bundle;)V
invoke-direct {p0}, Lin/org/npci/upiapp/HomeActivity;->n()V
invoke-direct {p0}, Lin/org/npci/upiapp/HomeActivity;->l()V
invoke-direct {p0}, Lin/org/npci/upiapp/HomeActivity;->m()V
new-instance v0, Lin/org/npci/upiapp/core/b;
invoke-direct {v0, p0}, Lin/org/npci/upiapp/core/b;-><init>(Landroid/app/Activity;)V
iput-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->q:Lin/org/npci/upiapp/core/b;
invoke-direct {p0}, Lin/org/npci/upiapp/HomeActivity;->o()V
const v0, 0x7f040019
invoke-virtual {p0, v0}, Lin/org/npci/upiapp/HomeActivity;->setContentView(I)V
const v0, 0x7f07004e
invoke-virtual {p0, v0}, Lin/org/npci/upiapp/HomeActivity;->getString(I)Ljava/lang/String;
move-result-object v0
sput-object v0, Lin/org/npci/upiapp/HomeActivity;->n:Ljava/lang/String;
invoke-direct {p0, p1}, Lin/org/npci/upiapp/HomeActivity;->a(Landroid/os/Bundle;)V
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
sget-object v1, Lin/org/npci/upiapp/HomeActivity;->n:Ljava/lang/String;
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->b(Ljava/lang/String;)V
return-void
.end method
.method protected onDestroy()V
.registers 3
invoke-super {p0}, Landroid/support/v7/a/e;->onDestroy()V
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->m:Lin/org/npci/upiapp/HomeActivity$a;
invoke-direct {p0, v0}, Lin/org/npci/upiapp/HomeActivity;->b(Landroid/content/BroadcastReceiver;)V
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
const-string v1, "onDestroy"
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->d(Ljava/lang/String;)V
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
invoke-virtual {v0}, Lin/juspay/mystique/d;->a()V
return-void
.end method
.method protected onPause()V
.registers 3
invoke-super {p0}, Landroid/support/v7/a/e;->onPause()V
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->m:Lin/org/npci/upiapp/HomeActivity$a;
invoke-direct {p0, v0}, Lin/org/npci/upiapp/HomeActivity;->b(Landroid/content/BroadcastReceiver;)V
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
const-string v1, "onPause"
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->d(Ljava/lang/String;)V
return-void
.end method
.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
.registers 5
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->p:Lin/org/npci/upiapp/core/JsInterface;
invoke-virtual {v0, p1, p2, p3}, Lin/org/npci/upiapp/core/JsInterface;->a(I[Ljava/lang/String;[I)V
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->s:Lin/org/npci/upiapp/core/QRScannerInterface;
if-eqz v0, :cond_e
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->s:Lin/org/npci/upiapp/core/QRScannerInterface;
invoke-virtual {v0, p1, p2, p3}, Lin/org/npci/upiapp/core/QRScannerInterface;->a(I[Ljava/lang/String;[I)V
:cond_e
return-void
.end method
.method protected onResume()V
.registers 3
invoke-super {p0}, Landroid/support/v7/a/e;->onResume()V
new-instance v0, Lin/org/npci/upiapp/HomeActivity$a;
const/4 v1, 0x0
invoke-direct {v0, p0, v1}, Lin/org/npci/upiapp/HomeActivity$a;-><init>(Lin/org/npci/upiapp/HomeActivity;Lin/org/npci/upiapp/HomeActivity$1;)V
iput-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->m:Lin/org/npci/upiapp/HomeActivity$a;
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->m:Lin/org/npci/upiapp/HomeActivity$a;
invoke-direct {p0, v0}, Lin/org/npci/upiapp/HomeActivity;->a(Landroid/content/BroadcastReceiver;)V
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->r:Lin/org/npci/upiapp/core/NPCIJSInterface;
if-eqz v0, :cond_20
const-string v0, "HomeActivity"
const-string v1, "Lifecycle - onResume Called"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->r:Lin/org/npci/upiapp/core/NPCIJSInterface;
invoke-virtual {v0}, Lin/org/npci/upiapp/core/NPCIJSInterface;->a()V
:cond_20
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity;->o:Lin/juspay/mystique/d;
const-string v1, "onResume"
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->d(Ljava/lang/String;)V
return-void
.end method