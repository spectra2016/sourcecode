.class public Lin/org/npci/upiapp/utils/c;
.super Ljava/lang/Object;
.source "KeyValueStore.java"
.field private final a:Landroid/content/SharedPreferences;
.method public constructor <init>(Landroid/content/Context;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const-string v0, "BHIMPreferences"
const/4 v1, 0x0
invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
move-result-object v0
iput-object v0, p0, Lin/org/npci/upiapp/utils/c;->a:Landroid/content/SharedPreferences;
return-void
.end method
.method public a(Landroid/content/Context;)V
.registers 4
const-string v0, "BHIMPreferences"
const/4 v1, 0x0
invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
move-result-object v0
invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;
move-result-object v0
invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;
move-result-object v0
invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
return-void
.end method
.method public a(Ljava/lang/String;Ljava/lang/String;)V
.registers 4
iget-object v0, p0, Lin/org/npci/upiapp/utils/c;->a:Landroid/content/SharedPreferences;
invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;
move-result-object v0
invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
return-void
.end method
.method public b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.registers 4
iget-object v0, p0, Lin/org/npci/upiapp/utils/c;->a:Landroid/content/SharedPreferences;
invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method