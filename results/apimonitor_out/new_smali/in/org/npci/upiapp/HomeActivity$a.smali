.class  Lin/org/npci/upiapp/HomeActivity$a;
.super Landroid/content/BroadcastReceiver;
.source "HomeActivity.java"
.field final synthetic a:Lin/org/npci/upiapp/HomeActivity;
.method private constructor <init>(Lin/org/npci/upiapp/HomeActivity;)V
.registers 2
iput-object p1, p0, Lin/org/npci/upiapp/HomeActivity$a;->a:Lin/org/npci/upiapp/HomeActivity;
invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V
return-void
.end method
.method synthetic constructor <init>(Lin/org/npci/upiapp/HomeActivity;Lin/org/npci/upiapp/HomeActivity$1;)V
.registers 3
invoke-direct {p0, p1}, Lin/org/npci/upiapp/HomeActivity$a;-><init>(Lin/org/npci/upiapp/HomeActivity;)V
return-void
.end method
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.registers 9
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity$a;->a:Lin/org/npci/upiapp/HomeActivity;
invoke-static {v0}, Lin/org/npci/upiapp/HomeActivity;->a(Lin/org/npci/upiapp/HomeActivity;)Lin/juspay/mystique/d;
move-result-object v0
if-eqz v0, :cond_86
:try_start_8
const-string v0, "onNotificationReceived"
invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
const-string v1, "onTokenReceived"
invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
const-string v2, "onTokenRefreshed"
invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
move-result-object v2
if-eqz v0, :cond_3e
iget-object v3, p0, Lin/org/npci/upiapp/HomeActivity$a;->a:Lin/org/npci/upiapp/HomeActivity;
invoke-static {v3}, Lin/org/npci/upiapp/HomeActivity;->a(Lin/org/npci/upiapp/HomeActivity;)Lin/juspay/mystique/d;
move-result-object v3
new-instance v4, Ljava/lang/StringBuilder;
invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
const-string v5, "javascript: window.onNotificationReceived("
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v4, ");"
invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-virtual {v3, v0}, Lin/juspay/mystique/d;->b(Ljava/lang/String;)V
:cond_3e
if-eqz v1, :cond_62
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity$a;->a:Lin/org/npci/upiapp/HomeActivity;
invoke-static {v0}, Lin/org/npci/upiapp/HomeActivity;->a(Lin/org/npci/upiapp/HomeActivity;)Lin/juspay/mystique/d;
move-result-object v0
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "javascript: window.onTokenReceived(\'"
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v3, "\');"
invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->b(Ljava/lang/String;)V
:cond_62
if-eqz v2, :cond_86
iget-object v0, p0, Lin/org/npci/upiapp/HomeActivity$a;->a:Lin/org/npci/upiapp/HomeActivity;
invoke-static {v0}, Lin/org/npci/upiapp/HomeActivity;->a(Lin/org/npci/upiapp/HomeActivity;)Lin/juspay/mystique/d;
move-result-object v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "javascript: window.onTokenRefreshed(\'"
invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "\');"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->b(Ljava/lang/String;)V
:try_end_86
.catch Ljava/lang/Exception; {:try_start_8 .. :try_end_86} :catch_87
:cond_86
:goto_86
return-void
:catch_87
move-exception v0
goto :goto_86
.end method