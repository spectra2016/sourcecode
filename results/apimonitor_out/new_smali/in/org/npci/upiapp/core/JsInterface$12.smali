.class  Lin/org/npci/upiapp/core/JsInterface$12;
.super Landroid/os/AsyncTask;
.source "JsInterface.java"
.field final synthetic a:Ljava/lang/String;
.field final synthetic b:Ljava/lang/String;
.field final synthetic c:Ljava/lang/String;
.field final synthetic d:Ljava/lang/String;
.field final synthetic e:Ljava/lang/String;
.field final synthetic f:Lin/org/npci/upiapp/core/JsInterface;
.method constructor <init>(Lin/org/npci/upiapp/core/JsInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.registers 7
iput-object p1, p0, Lin/org/npci/upiapp/core/JsInterface$12;->f:Lin/org/npci/upiapp/core/JsInterface;
iput-object p2, p0, Lin/org/npci/upiapp/core/JsInterface$12;->a:Ljava/lang/String;
iput-object p3, p0, Lin/org/npci/upiapp/core/JsInterface$12;->b:Ljava/lang/String;
iput-object p4, p0, Lin/org/npci/upiapp/core/JsInterface$12;->c:Ljava/lang/String;
iput-object p5, p0, Lin/org/npci/upiapp/core/JsInterface$12;->d:Ljava/lang/String;
iput-object p6, p0, Lin/org/npci/upiapp/core/JsInterface$12;->e:Ljava/lang/String;
invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V
return-void
.end method
.method protected doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
.registers 7
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Now calling API :"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$12;->b:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "API Request parameter : URL ->"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$12;->b:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "- Method -> "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$12;->c:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "API Request parameter : Data ->"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$12;->d:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "- Header -> "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$12;->e:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
new-instance v1, Ljava/util/HashMap;
invoke-direct {v1}, Ljava/util/HashMap;-><init>()V
:try_start_71
new-instance v2, Lorg/json/JSONObject;
iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$12;->e:Ljava/lang/String;
invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
move-result-object v3
:goto_7c
invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_af
invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v4
invoke-virtual {v1, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:try_end_8f
.catch Lorg/json/JSONException; {:try_start_71 .. :try_end_8f} :catch_90
.catch Ljava/lang/Exception; {:try_start_71 .. :try_end_8f} :catch_150
goto :goto_7c
:catch_90
move-exception v0
invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
invoke-static {v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "ERR: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v0}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;
move-result-object v0
invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
:goto_ae
return-object v0
:cond_af
:try_start_af
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "API Request Header : Header -> "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v0, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$12;->c:Ljava/lang/String;
const-string v2, "POST"
invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_16a
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "METHOD - "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$12;->c:Ljava/lang/String;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v0, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "URL - "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$12;->b:Ljava/lang/String;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v0, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "REQUEST-BODY - "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$12;->d:Ljava/lang/String;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v0, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "HEADER - "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v0, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
new-instance v0, Ljava/lang/String;
iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$12;->b:Ljava/lang/String;
iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$12;->d:Ljava/lang/String;
invoke-static {v2, v3, v1}, Lin/org/npci/upiapp/utils/RestClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)[B
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
:try_end_14e
.catch Lorg/json/JSONException; {:try_start_af .. :try_end_14e} :catch_90
.catch Ljava/lang/Exception; {:try_start_af .. :try_end_14e} :catch_150
goto/16 :goto_ae
:catch_150
move-exception v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "ERR: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;
move-result-object v0
invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
goto/16 :goto_ae
:cond_16a
:try_start_16a
iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$12;->c:Ljava/lang/String;
const-string v2, "GET"
invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_1be
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "METHOD:  - "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$12;->c:Ljava/lang/String;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v0, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "URL - "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$12;->b:Ljava/lang/String;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v0, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
new-instance v0, Ljava/lang/String;
iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$12;->b:Ljava/lang/String;
new-instance v3, Ljava/util/HashMap;
invoke-direct {v3}, Ljava/util/HashMap;-><init>()V
invoke-static {v2, v3, v1}, Lin/org/npci/upiapp/utils/RestClient;->a(Ljava/lang/String;Ljava/util/Map;Ljava/util/HashMap;)[B
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
goto/16 :goto_ae
:cond_1be
iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$12;->c:Ljava/lang/String;
const-string v2, "DELETE"
invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_212
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "METHOD:  - "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$12;->c:Ljava/lang/String;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v0, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "URL - "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$12;->b:Ljava/lang/String;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v0, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
new-instance v0, Ljava/lang/String;
iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$12;->b:Ljava/lang/String;
new-instance v3, Ljava/util/HashMap;
invoke-direct {v3}, Ljava/util/HashMap;-><init>()V
invoke-static {v2, v3, v1}, Lin/org/npci/upiapp/utils/RestClient;->a(Ljava/lang/String;Ljava/util/Map;Ljava/util/HashMap;)[B
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
:try_end_210
.catch Lorg/json/JSONException; {:try_start_16a .. :try_end_210} :catch_90
.catch Ljava/lang/Exception; {:try_start_16a .. :try_end_210} :catch_150
goto/16 :goto_ae
:cond_212
const/4 v0, 0x0
goto/16 :goto_ae
.end method
.method protected onPostExecute(Ljava/lang/Object;)V
.registers 9
const/4 v3, 0x3
const/4 v6, 0x1
const/4 v5, 0x0
const/4 v4, 0x2
if-eqz p1, :cond_73
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Response of API: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
move-object v0, p1
check-cast v0, Ljava/lang/String;
const-string v1, "ERR:"
invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_74
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Response Error: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$12;->a:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
check-cast p1, Ljava/lang/String;
invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
move-result-object v0
invoke-static {v0, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
move-result-object v0
const-string v1, "window.callJSCallback(\'%s\',\'%s\',\'%s\');"
new-array v2, v3, [Ljava/lang/Object;
iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$12;->a:Ljava/lang/String;
aput-object v3, v2, v5
const-string v3, "success"
aput-object v3, v2, v6
aput-object v0, v2, v4
invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
move-result-object v0
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v1
invoke-static {v1, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$12;->f:Lin/org/npci/upiapp/core/JsInterface;
invoke-static {v1}, Lin/org/npci/upiapp/core/JsInterface;->c(Lin/org/npci/upiapp/core/JsInterface;)Lin/juspay/mystique/d;
move-result-object v1
invoke-virtual {v1, v0}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
:cond_73
:goto_73
return-void
:cond_74
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Response Success: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p0, Lin/org/npci/upiapp/core/JsInterface$12;->a:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
check-cast p1, Ljava/lang/String;
invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
move-result-object v0
invoke-static {v0, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
move-result-object v0
const-string v1, "window.callJSCallback(\'%s\',\'%s\',\'%s\');"
new-array v2, v3, [Ljava/lang/Object;
iget-object v3, p0, Lin/org/npci/upiapp/core/JsInterface$12;->a:Ljava/lang/String;
aput-object v3, v2, v5
const-string v3, "success"
aput-object v3, v2, v6
aput-object v0, v2, v4
invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
move-result-object v0
invoke-static {}, Lin/org/npci/upiapp/core/JsInterface;->a()Ljava/lang/String;
move-result-object v1
invoke-static {v1, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$12;->f:Lin/org/npci/upiapp/core/JsInterface;
invoke-static {v1}, Lin/org/npci/upiapp/core/JsInterface;->c(Lin/org/npci/upiapp/core/JsInterface;)Lin/juspay/mystique/d;
move-result-object v1
invoke-virtual {v1, v0}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
goto :goto_73
.end method