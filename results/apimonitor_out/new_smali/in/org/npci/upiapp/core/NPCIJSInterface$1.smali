.class  Lin/org/npci/upiapp/core/NPCIJSInterface$1;
.super Ljava/lang/Object;
.source "NPCIJSInterface.java"
.implements Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;
.field final synthetic a:Ljava/lang/String;
.field final synthetic b:Lin/org/npci/upiapp/core/NPCIJSInterface;
.method constructor <init>(Lin/org/npci/upiapp/core/NPCIJSInterface;Ljava/lang/String;)V
.registers 3
iput-object p1, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$1;->b:Lin/org/npci/upiapp/core/NPCIJSInterface;
iput-object p2, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$1;->a:Ljava/lang/String;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public serviceConnected(Lorg/npci/upi/security/services/CLServices;)V
.registers 5
invoke-static {p1}, Lin/org/npci/upiapp/core/NPCIJSInterface;->a(Lorg/npci/upi/security/services/CLServices;)Lorg/npci/upi/security/services/CLServices;
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$1;->b:Lin/org/npci/upiapp/core/NPCIJSInterface;
invoke-static {v0}, Lin/org/npci/upiapp/core/NPCIJSInterface;->a(Lin/org/npci/upiapp/core/NPCIJSInterface;)Lin/juspay/mystique/d;
move-result-object v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "window.callUICallback(\""
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$1;->a:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "\")"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
return-void
.end method
.method public serviceDisconnected()V
.registers 3
const-string v0, "NPCIJSInterface"
const-string v1, "Error Code : 843 - Description : NPCI Service Disconnected"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V
return-void
.end method