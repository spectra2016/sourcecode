.class  Lin/org/npci/upiapp/core/JsInterface$6;
.super Ljava/lang/Object;
.source "JsInterface.java"
.implements Ljava/lang/Runnable;
.field final synthetic a:Ljava/lang/String;
.field final synthetic b:I
.field final synthetic c:I
.field final synthetic d:Lin/org/npci/upiapp/core/JsInterface;
.method constructor <init>(Lin/org/npci/upiapp/core/JsInterface;Ljava/lang/String;II)V
.registers 5
iput-object p1, p0, Lin/org/npci/upiapp/core/JsInterface$6;->d:Lin/org/npci/upiapp/core/JsInterface;
iput-object p2, p0, Lin/org/npci/upiapp/core/JsInterface$6;->a:Ljava/lang/String;
iput p3, p0, Lin/org/npci/upiapp/core/JsInterface$6;->b:I
iput p4, p0, Lin/org/npci/upiapp/core/JsInterface$6;->c:I
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public run()V
.registers 11
const/4 v9, 0x1
const/4 v8, 0x0
const/4 v7, 0x0
iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$6;->d:Lin/org/npci/upiapp/core/JsInterface;
invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->b(Lin/org/npci/upiapp/core/JsInterface;)Landroid/app/Activity;
move-result-object v0
iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$6;->a:Ljava/lang/String;
invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
move-result v1
invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;
move-result-object v0
iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$6;->d:Lin/org/npci/upiapp/core/JsInterface;
iget v2, p0, Lin/org/npci/upiapp/core/JsInterface$6;->b:I
int-to-float v2, v2
invoke-virtual {v1, v2}, Lin/org/npci/upiapp/core/JsInterface;->a(F)I
move-result v1
sget-object v2, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;
const/16 v3, 0x8
new-array v3, v3, [Landroid/animation/Keyframe;
invoke-static {v7, v7}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v3, v8
const v4, 0x3dcccccd
neg-int v5, v1
int-to-float v5, v5
invoke-static {v4, v5}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v3, v9
const/4 v4, 0x2
const v5, 0x3e851eb8
int-to-float v6, v1
invoke-static {v5, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/4 v4, 0x3
const v5, 0x3ed70a3d
neg-int v6, v1
int-to-float v6, v6
invoke-static {v5, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/4 v4, 0x4
const v5, 0x3f147ae1
int-to-float v6, v1
invoke-static {v5, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/4 v4, 0x5
const v5, 0x3f3d70a4
neg-int v6, v1
int-to-float v6, v6
invoke-static {v5, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/4 v4, 0x6
const v5, 0x3f666666
int-to-float v1, v1
invoke-static {v5, v1}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v1
aput-object v1, v3, v4
const/4 v1, 0x7
const/high16 v4, 0x3f80
invoke-static {v4, v7}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v3, v1
invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Landroid/util/Property;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;
move-result-object v1
new-array v2, v9, [Landroid/animation/PropertyValuesHolder;
aput-object v1, v2, v8
invoke-static {v0, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;
move-result-object v0
iget v1, p0, Lin/org/npci/upiapp/core/JsInterface$6;->c:I
int-to-long v2, v1
invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;
move-result-object v0
invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V
return-void
.end method