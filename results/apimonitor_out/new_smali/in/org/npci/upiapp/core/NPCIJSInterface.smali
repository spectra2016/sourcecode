.class public Lin/org/npci/upiapp/core/NPCIJSInterface;
.super Ljava/lang/Object;
.source "NPCIJSInterface.java"
.field private static b:Lorg/npci/upi/security/services/CLServices;
.field private a:Landroid/app/Activity;
.field private c:Lin/juspay/mystique/d;
.field private d:Ljava/lang/String;
.method public constructor <init>(Landroid/app/Activity;Lin/juspay/mystique/d;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->a:Landroid/app/Activity;
iput-object p2, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->c:Lin/juspay/mystique/d;
return-void
.end method
.method static synthetic a(Lin/org/npci/upiapp/core/NPCIJSInterface;)Lin/juspay/mystique/d;
.registers 2
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->c:Lin/juspay/mystique/d;
return-object v0
.end method
.method static synthetic a(Lin/org/npci/upiapp/core/NPCIJSInterface;Ljava/lang/Object;)Ljava/lang/Object;
.registers 3
invoke-direct {p0, p1}, Lin/org/npci/upiapp/core/NPCIJSInterface;->a(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
return-object v0
.end method
.method private a(Ljava/lang/Object;)Ljava/lang/Object;
.registers 4
if-nez p1, :cond_5
sget-object p1, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;
:goto_4
:cond_4
return-object p1
:cond_5
instance-of v0, p1, Lorg/json/JSONArray;
if-nez v0, :cond_4
instance-of v0, p1, Lorg/json/JSONObject;
if-nez v0, :cond_4
sget-object v0, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;
invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
move-result v0
if-nez v0, :cond_4
:try_start_15
instance-of v0, p1, Ljava/util/Collection;
if-eqz v0, :cond_22
new-instance v0, Lorg/json/JSONArray;
check-cast p1, Ljava/util/Collection;
invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V
move-object p1, v0
goto :goto_4
:cond_22
invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z
move-result v0
if-eqz v0, :cond_31
invoke-direct {p0, p1}, Lin/org/npci/upiapp/core/NPCIJSInterface;->b(Ljava/lang/Object;)Lorg/json/JSONArray;
move-result-object p1
goto :goto_4
:cond_31
instance-of v0, p1, Ljava/util/Map;
if-eqz v0, :cond_3e
new-instance v0, Lorg/json/JSONObject;
check-cast p1, Ljava/util/Map;
invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V
move-object p1, v0
goto :goto_4
:cond_3e
instance-of v0, p1, Ljava/lang/Boolean;
if-nez v0, :cond_4
instance-of v0, p1, Ljava/lang/Byte;
if-nez v0, :cond_4
instance-of v0, p1, Ljava/lang/Character;
if-nez v0, :cond_4
instance-of v0, p1, Ljava/lang/Double;
if-nez v0, :cond_4
instance-of v0, p1, Ljava/lang/Float;
if-nez v0, :cond_4
instance-of v0, p1, Ljava/lang/Integer;
if-nez v0, :cond_4
instance-of v0, p1, Ljava/lang/Long;
if-nez v0, :cond_4
instance-of v0, p1, Ljava/lang/Short;
if-nez v0, :cond_4
instance-of v0, p1, Ljava/lang/String;
if-nez v0, :cond_4
invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Package;->getName()Ljava/lang/String;
move-result-object v0
const-string v1, "java."
invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_7c
invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
:try_end_79
.catch Ljava/lang/Exception; {:try_start_15 .. :try_end_79} :catch_7b
move-result-object p1
goto :goto_4
:catch_7b
move-exception v0
:cond_7c
const/4 p1, 0x0
goto :goto_4
.end method
.method static synthetic a(Lin/org/npci/upiapp/core/NPCIJSInterface;Ljava/lang/String;)Ljava/lang/String;
.registers 2
iput-object p1, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->d:Ljava/lang/String;
return-object p1
.end method
.method static synthetic a(Lorg/npci/upi/security/services/CLServices;)Lorg/npci/upi/security/services/CLServices;
.registers 1
sput-object p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->b:Lorg/npci/upi/security/services/CLServices;
return-object p0
.end method
.method static synthetic b(Lin/org/npci/upiapp/core/NPCIJSInterface;)Landroid/app/Activity;
.registers 2
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->a:Landroid/app/Activity;
return-object v0
.end method
.method private b(Ljava/lang/Object;)Lorg/json/JSONArray;
.registers 6
new-instance v1, Lorg/json/JSONArray;
invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V
invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z
move-result v0
if-nez v0, :cond_2c
new-instance v0, Lorg/json/JSONException;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Not a primitive array: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V
throw v0
:cond_2c
invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I
move-result v2
const/4 v0, 0x0
:goto_31
if-ge v0, v2, :cond_41
invoke-static {p1, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;
move-result-object v3
invoke-direct {p0, v3}, Lin/org/npci/upiapp/core/NPCIJSInterface;->a(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v3
invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
add-int/lit8 v0, v0, 0x1
goto :goto_31
:cond_41
return-object v1
.end method
.method static synthetic c()Lorg/npci/upi/security/services/CLServices;
.registers 1
sget-object v0, Lin/org/npci/upiapp/core/NPCIJSInterface;->b:Lorg/npci/upi/security/services/CLServices;
return-object v0
.end method
.method public a()V
.registers 5
const-string v0, "NPCIJSInterface"
const-string v1, "Lifecycle - onResume Called"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
:try_start_7
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->d:Ljava/lang/String;
if-eqz v0, :cond_4f
new-instance v1, Lorg/json/JSONObject;
invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V
:try_start_10
:try_end_10
.catch Ljava/lang/Exception; {:try_start_7 .. :try_end_10} :catch_55
const-string v0, "NPCIJSInterface"
const-string v2, "BACK_PRESSED - CALLBACK CALLED"
invoke-static {v0, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
const-string v0, "event"
const-string v2, "back_pressed"
invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
:try_start_1e
:try_end_1e
.catch Ljava/lang/Exception; {:try_start_10 .. :try_end_1e} :catch_50
:goto_1e
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->c:Lin/juspay/mystique/d;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "window.callUICallback(\""
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->d:Ljava/lang/String;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, "\", "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, ");"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
const/4 v0, 0x0
iput-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->d:Ljava/lang/String;
:goto_4f
:cond_4f
return-void
:catch_50
move-exception v0
invoke-static {v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V
:try_end_54
.catch Ljava/lang/Exception; {:try_start_1e .. :try_end_54} :catch_55
goto :goto_1e
:catch_55
move-exception v0
invoke-static {v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V
goto :goto_4f
.end method
.method public b()V
.registers 3
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->a:Landroid/app/Activity;
new-instance v1, Lin/org/npci/upiapp/core/NPCIJSInterface$7;
invoke-direct {v1, p0}, Lin/org/npci/upiapp/core/NPCIJSInterface$7;-><init>(Lin/org/npci/upiapp/core/NPCIJSInterface;)V
invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
return-void
.end method
.method public decodeNPCIXmlKeys(Ljava/lang/String;)Ljava/lang/String;
.registers 4
new-instance v0, Ljava/lang/String;
const/4 v1, 0x2
invoke-static {p1, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
return-object v0
.end method
.method public fetchData(Ljava/lang/String;)Ljava/lang/String;
.registers 5
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->a:Landroid/app/Activity;
const-string v1, "NPCI"
const/4 v2, 0x0
invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
move-result-object v0
const-string v1, "NOT_FOUND"
invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public getChallenge(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.registers 6
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->a:Landroid/app/Activity;
new-instance v1, Lin/org/npci/upiapp/core/NPCIJSInterface$3;
invoke-direct {v1, p0, p3, p1, p2}, Lin/org/npci/upiapp/core/NPCIJSInterface$3;-><init>(Lin/org/npci/upiapp/core/NPCIJSInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
return-void
.end method
.method public getCredentials(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.registers 24
move-object/from16 v0, p9
iput-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->d:Ljava/lang/String;
new-instance v4, Lorg/npci/upi/security/services/CLRemoteResultReceiver;
new-instance v1, Lin/org/npci/upiapp/core/NPCIJSInterface$5;
new-instance v2, Landroid/os/Handler;
invoke-direct {v2}, Landroid/os/Handler;-><init>()V
move-object/from16 v0, p9
invoke-direct {v1, p0, v2, v0}, Lin/org/npci/upiapp/core/NPCIJSInterface$5;-><init>(Lin/org/npci/upiapp/core/NPCIJSInterface;Landroid/os/Handler;Ljava/lang/String;)V
invoke-direct {v4, v1}, Lorg/npci/upi/security/services/CLRemoteResultReceiver;-><init>(Landroid/os/ResultReceiver;)V
iget-object v13, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->a:Landroid/app/Activity;
new-instance v1, Lin/org/npci/upiapp/core/NPCIJSInterface$6;
move-object v2, p0
move-object/from16 v3, p9
move-object v5, p1
move-object/from16 v6, p2
move-object/from16 v7, p3
move-object/from16 v8, p4
move-object/from16 v9, p5
move-object/from16 v10, p6
move-object/from16 v11, p7
move-object/from16 v12, p8
invoke-direct/range {v1 .. v12}, Lin/org/npci/upiapp/core/NPCIJSInterface$6;-><init>(Lin/org/npci/upiapp/core/NPCIJSInterface;Ljava/lang/String;Lorg/npci/upi/security/services/CLRemoteResultReceiver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
invoke-virtual {v13, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
return-void
.end method
.method public varargs handleInit(Ljava/lang/String;Ljava/lang/String;Lorg/npci/upi/security/services/CLRemoteResultReceiver;[Ljava/lang/String;)V
.registers 16
const/4 v9, 0x3
const/4 v8, 0x2
const/4 v7, 0x1
const/4 v6, 0x0
:try_start_4
iget-object v10, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->a:Landroid/app/Activity;
new-instance v0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;
move-object v1, p0
move-object v2, p2
move-object v3, p4
move-object v4, p1
move-object v5, p3
invoke-direct/range {v0 .. v5}, Lin/org/npci/upiapp/core/NPCIJSInterface$2;-><init>(Lin/org/npci/upiapp/core/NPCIJSInterface;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lorg/npci/upi/security/services/CLRemoteResultReceiver;)V
invoke-static {v10, v0}, Lorg/npci/upi/security/services/CLServices;->initService(Landroid/content/Context;Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;)V
:goto_13
:try_end_13
.catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_13} :catch_14
return-void
:catch_14
move-exception v0
const/4 v0, -0x1
invoke-virtual {p2}, Ljava/lang/String;->hashCode()I
move-result v1
sparse-switch v1, :sswitch_data_13e
:goto_1d
:cond_1d
packed-switch v0, :pswitch_data_150
goto :goto_13
:pswitch_21
sget-object v0, Lin/org/npci/upiapp/core/NPCIJSInterface;->b:Lorg/npci/upi/security/services/CLServices;
aget-object v1, p4, v6
aget-object v2, p4, v7
invoke-virtual {v0, v1, v2}, Lorg/npci/upi/security/services/CLServices;->getChallenge(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
iget-object v1, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->c:Lin/juspay/mystique/d;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "window.callUICallback(\""
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, "\",\""
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v2, "\")"
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-virtual {v1, v0}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
goto :goto_13
:sswitch_54
const-string v1, "getChallenge"
invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_1d
move v0, v6
goto :goto_1d
:sswitch_5e
const-string v1, "registerApp"
invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_1d
move v0, v7
goto :goto_1d
:sswitch_68
const-string v1, "getCredential"
invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_1d
move v0, v8
goto :goto_1d
:sswitch_72
const-string v1, "unbindService"
invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_1d
move v0, v9
goto :goto_1d
:pswitch_7c
sget-object v0, Lin/org/npci/upiapp/core/NPCIJSInterface;->b:Lorg/npci/upi/security/services/CLServices;
aget-object v1, p4, v6
aget-object v2, p4, v7
aget-object v3, p4, v8
aget-object v4, p4, v9
invoke-virtual {v0, v1, v2, v3, v4}, Lorg/npci/upi/security/services/CLServices;->registerApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
move-result v0
invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v0
iget-object v1, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->c:Lin/juspay/mystique/d;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "window.callUICallback(\""
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, "\",\""
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v2, "\")"
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-virtual {v1, v0}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
goto/16 :goto_13
:pswitch_b8
array-length v1, p4
move v0, v6
:goto_ba
if-ge v0, v1, :cond_d9
aget-object v2, p4, v0
const-string v3, "NPCIJSInterface"
new-instance v4, Ljava/lang/StringBuilder;
invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
const-string v5, "getCredentials - "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v3, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
add-int/lit8 v0, v0, 0x1
goto :goto_ba
:cond_d9
sget-object v0, Lin/org/npci/upiapp/core/NPCIJSInterface;->b:Lorg/npci/upi/security/services/CLServices;
aget-object v1, p4, v6
aget-object v2, p4, v7
aget-object v3, p4, v8
aget-object v4, p4, v9
const/4 v5, 0x4
aget-object v5, p4, v5
const/4 v6, 0x5
aget-object v6, p4, v6
const/4 v7, 0x6
aget-object v7, p4, v7
const/4 v8, 0x7
aget-object v8, p4, v8
move-object v9, p3
invoke-virtual/range {v0 .. v9}, Lorg/npci/upi/security/services/CLServices;->getCredential(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/npci/upi/security/services/CLRemoteResultReceiver;)V
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->c:Lin/juspay/mystique/d;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "window.callUICallback(\""
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "\")"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
goto/16 :goto_13
:try_start_113
:pswitch_113
sget-object v0, Lin/org/npci/upiapp/core/NPCIJSInterface;->b:Lorg/npci/upi/security/services/CLServices;
invoke-virtual {v0}, Lorg/npci/upi/security/services/CLServices;->unbindService()V
:try_end_118
.catch Ljava/lang/Exception; {:try_start_113 .. :try_end_118} :catch_138
:goto_118
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->c:Lin/juspay/mystique/d;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "window.callUICallback(\""
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "\")"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
goto/16 :goto_13
:catch_138
move-exception v0
invoke-static {v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V
goto :goto_118
nop
:pswitch_data_150
.packed-switch 0x0
:pswitch_21
:pswitch_7c
:pswitch_b8
:pswitch_113
.end packed-switch
:sswitch_data_13e
.sparse-switch
-0x7f27d382 -> :sswitch_5e
-0x5b438c81 -> :sswitch_72
-0x3a7b5fb3 -> :sswitch_68
0x5307edad -> :sswitch_54
.end sparse-switch
.end method
.method public initialiseNPCICL(Ljava/lang/String;)V
.registers 5
:try_start_0
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->a:Landroid/app/Activity;
new-instance v1, Lin/org/npci/upiapp/core/NPCIJSInterface$1;
invoke-direct {v1, p0, p1}, Lin/org/npci/upiapp/core/NPCIJSInterface$1;-><init>(Lin/org/npci/upiapp/core/NPCIJSInterface;Ljava/lang/String;)V
invoke-static {v0, v1}, Lorg/npci/upi/security/services/CLServices;->initService(Landroid/content/Context;Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;)V
:try_end_a
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_a} :catch_b
:goto_a
return-void
:catch_b
move-exception v0
const-string v1, "Service already initiated"
invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_37
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->c:Lin/juspay/mystique/d;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "window.callUICallback(\""
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "\")"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
goto :goto_a
:cond_37
const-string v1, "NPCIJSInterface"
const-string v2, "intialiseNPCICL"
invoke-static {v1, v2, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
goto :goto_a
.end method
.method public populateHMAC(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.registers 10
invoke-virtual {p0}, Lin/org/npci/upiapp/core/NPCIJSInterface;->b()V
const/4 v0, 0x0
:try_start_4
new-instance v1, Lin/org/npci/upiapp/utils/a;
invoke-direct {v1}, Lin/org/npci/upiapp/utils/a;-><init>()V
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "|"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "|"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
const-string v2, "NPCIJSInterface"
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "PSP Hmac Msg - "
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v3
invoke-static {v2, v3}, Lin/org/npci/upiapp/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V
const/4 v2, 0x2
invoke-static {p3, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
move-result-object v2
invoke-static {v1}, Lin/org/npci/upiapp/utils/a;->a(Ljava/lang/String;)[B
move-result-object v1
invoke-static {v1, v2}, Lin/org/npci/upiapp/utils/a;->a([B[B)[B
move-result-object v1
const/4 v2, 0x0
invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
:try_end_53
.catch Ljava/lang/Exception; {:try_start_4 .. :try_end_53} :catch_55
move-result-object v0
:goto_54
return-object v0
:catch_55
move-exception v1
const-string v2, "NPCIJSInterface"
const-string v3, "populateHMAC "
invoke-static {v2, v3, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
goto :goto_54
.end method
.method public registerApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.registers 14
iget-object v7, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->a:Landroid/app/Activity;
new-instance v0, Lin/org/npci/upiapp/core/NPCIJSInterface$4;
move-object v1, p0
move-object v2, p5
move-object v3, p1
move-object v4, p2
move-object v5, p3
move-object v6, p4
invoke-direct/range {v0 .. v6}, Lin/org/npci/upiapp/core/NPCIJSInterface$4;-><init>(Lin/org/npci/upiapp/core/NPCIJSInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
invoke-virtual {v7, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
return-void
.end method
.method public saveData(Ljava/lang/String;Ljava/lang/String;)V
.registers 6
const-string v0, "NPCIJSInterface"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Saving to local store : "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, ", "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface;->a:Landroid/app/Activity;
const-string v1, "NPCI"
const/4 v2, 0x0
invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
move-result-object v0
invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;
move-result-object v0
invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
move-result-object v0
invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
return-void
.end method
.method public trustCred(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.registers 6
const/4 v0, 0x0
:try_start_1
new-instance v1, Lin/org/npci/upiapp/utils/a;
invoke-direct {v1}, Lin/org/npci/upiapp/utils/a;-><init>()V
invoke-static {p1}, Lin/org/npci/upiapp/utils/a;->a(Ljava/lang/String;)[B
move-result-object v1
const/4 v2, 0x2
invoke-static {p2, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
move-result-object v2
invoke-static {v1, v2}, Lin/org/npci/upiapp/utils/a;->a([B[B)[B
move-result-object v1
const/4 v2, 0x2
invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
:try_end_17
.catch Ljava/lang/Exception; {:try_start_1 .. :try_end_17} :catch_19
move-result-object v0
:goto_18
return-object v0
:catch_19
move-exception v1
invoke-static {v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V
goto :goto_18
.end method
.method public unbindNPCICL(Ljava/lang/String;)V
.registers 5
const-string v0, "unbindService"
const/4 v1, 0x0
const/4 v2, 0x0
new-array v2, v2, [Ljava/lang/String;
invoke-virtual {p0, p1, v0, v1, v2}, Lin/org/npci/upiapp/core/NPCIJSInterface;->handleInit(Ljava/lang/String;Ljava/lang/String;Lorg/npci/upi/security/services/CLRemoteResultReceiver;[Ljava/lang/String;)V
return-void
.end method