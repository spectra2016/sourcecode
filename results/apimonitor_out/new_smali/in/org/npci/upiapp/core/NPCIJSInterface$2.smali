.class  Lin/org/npci/upiapp/core/NPCIJSInterface$2;
.super Ljava/lang/Object;
.source "NPCIJSInterface.java"
.implements Lorg/npci/upi/security/services/ServiceConnectionStatusNotifier;
.field final synthetic a:Ljava/lang/String;
.field final synthetic b:[Ljava/lang/String;
.field final synthetic c:Ljava/lang/String;
.field final synthetic d:Lorg/npci/upi/security/services/CLRemoteResultReceiver;
.field final synthetic e:Lin/org/npci/upiapp/core/NPCIJSInterface;
.method constructor <init>(Lin/org/npci/upiapp/core/NPCIJSInterface;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lorg/npci/upi/security/services/CLRemoteResultReceiver;)V
.registers 6
iput-object p1, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->e:Lin/org/npci/upiapp/core/NPCIJSInterface;
iput-object p2, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->a:Ljava/lang/String;
iput-object p3, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
iput-object p4, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->c:Ljava/lang/String;
iput-object p5, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->d:Lorg/npci/upi/security/services/CLRemoteResultReceiver;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public serviceConnected(Lorg/npci/upi/security/services/CLServices;)V
.registers 12
const/4 v4, 0x3
const/4 v3, 0x2
const/4 v2, 0x1
const/4 v1, 0x0
invoke-static {p1}, Lin/org/npci/upiapp/core/NPCIJSInterface;->a(Lorg/npci/upi/security/services/CLServices;)Lorg/npci/upi/security/services/CLServices;
iget-object v5, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->a:Ljava/lang/String;
const/4 v0, -0x1
invoke-virtual {v5}, Ljava/lang/String;->hashCode()I
move-result v6
sparse-switch v6, :sswitch_data_130
:cond_11
:goto_11
packed-switch v0, :pswitch_data_142
:goto_14
return-void
:sswitch_15
const-string v6, "getChallenge"
invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v5
if-eqz v5, :cond_11
move v0, v1
goto :goto_11
:sswitch_1f
const-string v6, "registerApp"
invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v5
if-eqz v5, :cond_11
move v0, v2
goto :goto_11
:sswitch_29
const-string v6, "getCredential"
invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v5
if-eqz v5, :cond_11
move v0, v3
goto :goto_11
:sswitch_33
const-string v6, "unbindService"
invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v5
if-eqz v5, :cond_11
move v0, v4
goto :goto_11
:pswitch_3d
invoke-static {}, Lin/org/npci/upiapp/core/NPCIJSInterface;->c()Lorg/npci/upi/security/services/CLServices;
move-result-object v0
iget-object v3, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
aget-object v1, v3, v1
iget-object v3, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
aget-object v2, v3, v2
invoke-virtual {v0, v1, v2}, Lorg/npci/upi/security/services/CLServices;->getChallenge(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
iget-object v1, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->e:Lin/org/npci/upiapp/core/NPCIJSInterface;
invoke-static {v1}, Lin/org/npci/upiapp/core/NPCIJSInterface;->a(Lin/org/npci/upiapp/core/NPCIJSInterface;)Lin/juspay/mystique/d;
move-result-object v1
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "window.callUICallback(\""
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->c:Ljava/lang/String;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, "\",\""
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v2, "\")"
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-virtual {v1, v0}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
goto :goto_14
:pswitch_7c
invoke-static {}, Lin/org/npci/upiapp/core/NPCIJSInterface;->c()Lorg/npci/upi/security/services/CLServices;
move-result-object v0
iget-object v5, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
aget-object v1, v5, v1
iget-object v5, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
aget-object v2, v5, v2
iget-object v5, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
aget-object v3, v5, v3
iget-object v5, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
aget-object v4, v5, v4
invoke-virtual {v0, v1, v2, v3, v4}, Lorg/npci/upi/security/services/CLServices;->registerApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
move-result v0
invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v0
iget-object v1, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->e:Lin/org/npci/upiapp/core/NPCIJSInterface;
invoke-static {v1}, Lin/org/npci/upiapp/core/NPCIJSInterface;->a(Lin/org/npci/upiapp/core/NPCIJSInterface;)Lin/juspay/mystique/d;
move-result-object v1
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "window.callUICallback(\""
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->c:Ljava/lang/String;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, "\",\""
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v2, "\")"
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-virtual {v1, v0}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
goto/16 :goto_14
:pswitch_c8
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->e:Lin/org/npci/upiapp/core/NPCIJSInterface;
iget-object v5, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->c:Ljava/lang/String;
invoke-static {v0, v5}, Lin/org/npci/upiapp/core/NPCIJSInterface;->a(Lin/org/npci/upiapp/core/NPCIJSInterface;Ljava/lang/String;)Ljava/lang/String;
invoke-static {}, Lin/org/npci/upiapp/core/NPCIJSInterface;->c()Lorg/npci/upi/security/services/CLServices;
move-result-object v0
iget-object v5, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
aget-object v1, v5, v1
iget-object v5, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
aget-object v2, v5, v2
iget-object v5, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
aget-object v3, v5, v3
iget-object v5, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
aget-object v4, v5, v4
iget-object v5, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
const/4 v6, 0x4
aget-object v5, v5, v6
iget-object v6, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
const/4 v7, 0x5
aget-object v6, v6, v7
iget-object v7, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
const/4 v8, 0x6
aget-object v7, v7, v8
iget-object v8, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->b:[Ljava/lang/String;
const/4 v9, 0x7
aget-object v8, v8, v9
iget-object v9, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->d:Lorg/npci/upi/security/services/CLRemoteResultReceiver;
invoke-virtual/range {v0 .. v9}, Lorg/npci/upi/security/services/CLServices;->getCredential(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/npci/upi/security/services/CLRemoteResultReceiver;)V
goto/16 :goto_14
:try_start_fe
:pswitch_fe
invoke-static {}, Lin/org/npci/upiapp/core/NPCIJSInterface;->c()Lorg/npci/upi/security/services/CLServices;
move-result-object v0
invoke-virtual {v0}, Lorg/npci/upi/security/services/CLServices;->unbindService()V
:goto_105
:try_end_105
.catch Ljava/lang/Exception; {:try_start_fe .. :try_end_105} :catch_12b
iget-object v0, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->e:Lin/org/npci/upiapp/core/NPCIJSInterface;
invoke-static {v0}, Lin/org/npci/upiapp/core/NPCIJSInterface;->a(Lin/org/npci/upiapp/core/NPCIJSInterface;)Lin/juspay/mystique/d;
move-result-object v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "window.callUICallback(\""
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p0, Lin/org/npci/upiapp/core/NPCIJSInterface$2;->c:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "\")"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
goto/16 :goto_14
:catch_12b
move-exception v0
invoke-static {v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V
goto :goto_105
:pswitch_data_142
.packed-switch 0x0
:pswitch_3d
:pswitch_7c
:pswitch_c8
:pswitch_fe
.end packed-switch
:sswitch_data_130
.sparse-switch
-0x7f27d382 -> :sswitch_1f
-0x5b438c81 -> :sswitch_33
-0x3a7b5fb3 -> :sswitch_29
0x5307edad -> :sswitch_15
.end sparse-switch
.end method
.method public serviceDisconnected()V
.registers 3
const-string v0, "NPCIJSInterface"
const-string v1, "Error Code : 843 - Description : NPCI Service Disconnected"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V
return-void
.end method