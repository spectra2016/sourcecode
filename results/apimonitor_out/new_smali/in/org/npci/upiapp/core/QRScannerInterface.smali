.class public Lin/org/npci/upiapp/core/QRScannerInterface;
.super Ljava/lang/Object;
.source "QRScannerInterface.java"
.field private static final a:Ljava/lang/String;
.field private b:Landroid/app/Activity;
.field private c:Lin/juspay/mystique/d;
.field private d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;
.field private e:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;
.field private f:Ljava/lang/String;
.method static constructor <clinit>()V
.registers 1
const-class v0, Lin/org/npci/upiapp/core/QRScannerInterface;
invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v0
sput-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
return-void
.end method
.method public constructor <init>(Landroid/app/Activity;Lin/juspay/mystique/d;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->b:Landroid/app/Activity;
iput-object p2, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->c:Lin/juspay/mystique/d;
return-void
.end method
.method static synthetic a(Lin/org/npci/upiapp/core/QRScannerInterface;Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;)Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;
.registers 2
iput-object p1, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->e:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;
return-object p1
.end method
.method static synthetic a(Lin/org/npci/upiapp/core/QRScannerInterface;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;
.registers 2
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;
return-object v0
.end method
.method static synthetic a(Lin/org/npci/upiapp/core/QRScannerInterface;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;
.registers 2
iput-object p1, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;
return-object p1
.end method
.method static synthetic a()Ljava/lang/String;
.registers 1
sget-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
return-object v0
.end method
.method static synthetic b(Lin/org/npci/upiapp/core/QRScannerInterface;)Landroid/app/Activity;
.registers 2
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->b:Landroid/app/Activity;
return-object v0
.end method
.method private b()V
.registers 5
sget-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
const-string v1, "openQRScanner Invoked!!"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->f:Ljava/lang/String;
invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-nez v0, :cond_38
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->f:Ljava/lang/String;
invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
move-result v0
sget-object v1, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Opening QR Scanner inside Frame with ID :"
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
iget-object v1, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->b:Landroid/app/Activity;
new-instance v2, Lin/org/npci/upiapp/core/QRScannerInterface$3;
invoke-direct {v2, p0, v0}, Lin/org/npci/upiapp/core/QRScannerInterface$3;-><init>(Lin/org/npci/upiapp/core/QRScannerInterface;I)V
invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
:goto_37
return-void
:cond_38
sget-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
const-string v1, "ERROR: Frame ID null!!"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V
goto :goto_37
.end method
.method static synthetic c(Lin/org/npci/upiapp/core/QRScannerInterface;)Ljava/lang/String;
.registers 2
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->f:Ljava/lang/String;
return-object v0
.end method
.method private c()Z
.registers 3
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->b:Landroid/app/Activity;
const-string v1, "android.permission.CAMERA"
invoke-static {v0, v1}, Landroid/support/v4/a/a;->a(Landroid/content/Context;Ljava/lang/String;)I
move-result v0
if-nez v0, :cond_c
const/4 v0, 0x1
:goto_b
return v0
:cond_c
const/4 v0, 0x0
goto :goto_b
.end method
.method static synthetic d(Lin/org/npci/upiapp/core/QRScannerInterface;)Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;
.registers 2
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->e:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;
return-object v0
.end method
.method static synthetic e(Lin/org/npci/upiapp/core/QRScannerInterface;)Lin/juspay/mystique/d;
.registers 2
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->c:Lin/juspay/mystique/d;
return-object v0
.end method
.method public a(I[Ljava/lang/String;[I)V
.registers 6
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->c:Lin/juspay/mystique/d;
if-nez v0, :cond_c
sget-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
const-string v1, "ERROR: Empty Dynamic UI!!"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V
:goto_b
return-void
:cond_c
packed-switch p1, :pswitch_data_2a
goto :goto_b
:pswitch_10
invoke-direct {p0}, Lin/org/npci/upiapp/core/QRScannerInterface;->c()Z
move-result v0
if-eqz v0, :cond_21
sget-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
const-string v1, "Camera Access Permission Granted."
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-direct {p0}, Lin/org/npci/upiapp/core/QRScannerInterface;->b()V
goto :goto_b
:cond_21
sget-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
const-string v1, "ERROR: Camera Access Permission Not Granted!!"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V
goto :goto_b
nop
:pswitch_data_2a
.packed-switch 0x65
:pswitch_10
.end packed-switch
.end method
.method public captureManager(Ljava/lang/String;)V
.registers 4
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->b:Landroid/app/Activity;
new-instance v1, Lin/org/npci/upiapp/core/QRScannerInterface$2;
invoke-direct {v1, p0, p1}, Lin/org/npci/upiapp/core/QRScannerInterface$2;-><init>(Lin/org/npci/upiapp/core/QRScannerInterface;Ljava/lang/String;)V
invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
return-void
.end method
.method public closeQRScanner()V
.registers 3
sget-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
const-string v1, "JSInterface closeQRScanner Called"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;
if-eqz v0, :cond_1d
sget-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
const-string v1, "JSInterface closeQRScanner 2 Called"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->b:Landroid/app/Activity;
new-instance v1, Lin/org/npci/upiapp/core/QRScannerInterface$1;
invoke-direct {v1, p0}, Lin/org/npci/upiapp/core/QRScannerInterface$1;-><init>(Lin/org/npci/upiapp/core/QRScannerInterface;)V
invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
:goto_1c
return-void
:cond_1d
sget-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
const-string v1, "JSInterface closeQRScanner 3 Called"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
sget-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
const-string v1, "ERROR: CaptureManager NULL!!"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V
goto :goto_1c
.end method
.method public openQRScanner(Ljava/lang/String;)V
.registers 4
sget-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
const-string v1, "JSInterface openQRScanner Called"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
iput-object p1, p0, Lin/org/npci/upiapp/core/QRScannerInterface;->f:Ljava/lang/String;
invoke-direct {p0}, Lin/org/npci/upiapp/core/QRScannerInterface;->c()Z
move-result v0
if-nez v0, :cond_1e
sget-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
const-string v1, "Requesting for Camera Access Permission....."
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
sget-object v0, Lin/org/npci/upiapp/core/QRScannerInterface;->a:Ljava/lang/String;
const-string v1, "Requesting for Camera Access Permission."
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
:goto_1d
return-void
:cond_1e
invoke-direct {p0}, Lin/org/npci/upiapp/core/QRScannerInterface;->b()V
goto :goto_1d
.end method