.class  Lin/org/npci/upiapp/core/QRScannerInterface$3;
.super Ljava/lang/Object;
.source "QRScannerInterface.java"
.implements Ljava/lang/Runnable;
.field final synthetic a:I
.field final synthetic b:Lin/org/npci/upiapp/core/QRScannerInterface;
.method constructor <init>(Lin/org/npci/upiapp/core/QRScannerInterface;I)V
.registers 3
iput-object p1, p0, Lin/org/npci/upiapp/core/QRScannerInterface$3;->b:Lin/org/npci/upiapp/core/QRScannerInterface;
iput p2, p0, Lin/org/npci/upiapp/core/QRScannerInterface$3;->a:I
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public run()V
.registers 5
const/4 v2, -0x1
new-instance v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface$3;->b:Lin/org/npci/upiapp/core/QRScannerInterface;
invoke-static {v0}, Lin/org/npci/upiapp/core/QRScannerInterface;->b(Lin/org/npci/upiapp/core/QRScannerInterface;)Landroid/app/Activity;
move-result-object v0
invoke-direct {v1, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;-><init>(Landroid/content/Context;)V
new-instance v0, Landroid/widget/FrameLayout$LayoutParams;
invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V
invoke-virtual {v1, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface$3;->b:Lin/org/npci/upiapp/core/QRScannerInterface;
invoke-static {v0}, Lin/org/npci/upiapp/core/QRScannerInterface;->b(Lin/org/npci/upiapp/core/QRScannerInterface;)Landroid/app/Activity;
move-result-object v0
iget v2, p0, Lin/org/npci/upiapp/core/QRScannerInterface$3;->a:I
invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;
move-result-object v0
check-cast v0, Landroid/widget/FrameLayout;
if-nez v0, :cond_51
invoke-static {}, Lin/org/npci/upiapp/core/QRScannerInterface;->a()Ljava/lang/String;
move-result-object v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Unable to find view with resID - "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p0, Lin/org/npci/upiapp/core/QRScannerInterface$3;->b:Lin/org/npci/upiapp/core/QRScannerInterface;
invoke-static {v2}, Lin/org/npci/upiapp/core/QRScannerInterface;->c(Lin/org/npci/upiapp/core/QRScannerInterface;)Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, " : "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget v2, p0, Lin/org/npci/upiapp/core/QRScannerInterface$3;->a:I
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V
:goto_50
return-void
:cond_51
invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface$3;->b:Lin/org/npci/upiapp/core/QRScannerInterface;
new-instance v2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;
iget-object v3, p0, Lin/org/npci/upiapp/core/QRScannerInterface$3;->b:Lin/org/npci/upiapp/core/QRScannerInterface;
invoke-static {v3}, Lin/org/npci/upiapp/core/QRScannerInterface;->b(Lin/org/npci/upiapp/core/QRScannerInterface;)Landroid/app/Activity;
move-result-object v3
invoke-direct {v2, v3, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;-><init>(Landroid/app/Activity;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;)V
invoke-static {v0, v2}, Lin/org/npci/upiapp/core/QRScannerInterface;->a(Lin/org/npci/upiapp/core/QRScannerInterface;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface$3;->b:Lin/org/npci/upiapp/core/QRScannerInterface;
invoke-static {v0}, Lin/org/npci/upiapp/core/QRScannerInterface;->a(Lin/org/npci/upiapp/core/QRScannerInterface;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;
move-result-object v0
new-instance v1, Lin/org/npci/upiapp/core/QRScannerInterface$3$1;
invoke-direct {v1, p0}, Lin/org/npci/upiapp/core/QRScannerInterface$3$1;-><init>(Lin/org/npci/upiapp/core/QRScannerInterface$3;)V
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;)V
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface$3;->b:Lin/org/npci/upiapp/core/QRScannerInterface;
invoke-static {v0}, Lin/org/npci/upiapp/core/QRScannerInterface;->a(Lin/org/npci/upiapp/core/QRScannerInterface;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;
move-result-object v0
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->b()V
iget-object v0, p0, Lin/org/npci/upiapp/core/QRScannerInterface$3;->b:Lin/org/npci/upiapp/core/QRScannerInterface;
invoke-static {v0}, Lin/org/npci/upiapp/core/QRScannerInterface;->a(Lin/org/npci/upiapp/core/QRScannerInterface;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;
move-result-object v0
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->a()V
goto :goto_50
.end method