.class public Lin/org/npci/upiapp/core/a;
.super Ljava/lang/Object;
.source "SimUtil.java"
.method public static a(Landroid/content/Context;)Z
.registers 5
const/4 v0, 0x1
const/4 v1, 0x0
sget v2, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v3, 0x16
if-lt v2, v3, :cond_15
invoke-static {p0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;
move-result-object v2
invoke-virtual {v2}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoCount()I
move-result v2
if-le v2, v0, :cond_13
:goto_12
return v0
:cond_13
move v0, v1
goto :goto_12
:cond_15
move v0, v1
goto :goto_12
.end method
.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Z
.registers 15
const/4 v6, 0x0
const/4 v7, 0x1
if-nez p1, :cond_b5
:try_start_4
sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;
const-string v1, "Philips T939"
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_b1
const-string v0, "isms0"
:goto_10
const-string v1, "android.os.ServiceManager"
invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v1
const-string v2, "getService"
const/4 v3, 0x1
new-array v3, v3, [Ljava/lang/Class;
const/4 v4, 0x0
const-class v5, Ljava/lang/String;
aput-object v5, v3, v4
invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
move-result-object v1
const/4 v2, 0x1
invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
const/4 v2, 0x0
const/4 v3, 0x1
new-array v3, v3, [Ljava/lang/Object;
const/4 v4, 0x0
aput-object v0, v3, v4
invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
const-string v1, "com.android.internal.telephony.ISms$Stub"
invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
move-result-object v1
const-string v2, "asInterface"
const/4 v3, 0x1
new-array v3, v3, [Ljava/lang/Class;
const/4 v4, 0x0
const-class v5, Landroid/os/IBinder;
aput-object v5, v3, v4
invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
move-result-object v1
const/4 v2, 0x1
invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
const/4 v2, 0x0
const/4 v3, 0x1
new-array v3, v3, [Ljava/lang/Object;
const/4 v4, 0x0
aput-object v0, v3, v4
invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
const-string v1, "SimUtil"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "send msg - "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v2, 0x12
if-ge v1, v2, :cond_e0
invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v1
const-string v2, "sendText"
const/4 v3, 0x5
new-array v3, v3, [Ljava/lang/Class;
const/4 v4, 0x0
const-class v5, Ljava/lang/String;
aput-object v5, v3, v4
const/4 v4, 0x1
const-class v5, Ljava/lang/String;
aput-object v5, v3, v4
const/4 v4, 0x2
const-class v5, Ljava/lang/String;
aput-object v5, v3, v4
const/4 v4, 0x3
const-class v5, Landroid/app/PendingIntent;
aput-object v5, v3, v4
const/4 v4, 0x4
const-class v5, Landroid/app/PendingIntent;
aput-object v5, v3, v4
invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
move-result-object v1
const/4 v2, 0x5
new-array v2, v2, [Ljava/lang/Object;
const/4 v3, 0x0
aput-object p2, v2, v3
const/4 v3, 0x1
aput-object p3, v2, v3
const/4 v3, 0x2
aput-object p4, v2, v3
const/4 v3, 0x3
aput-object p5, v2, v3
const/4 v3, 0x4
aput-object p6, v2, v3
invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
:goto_af
move v0, v7
:goto_b0
return v0
:cond_b1
const-string v0, "isms"
goto/16 :goto_10
:cond_b5
if-ne p1, v7, :cond_bb
const-string v0, "isms2"
goto/16 :goto_10
:cond_bb
new-instance v0, Ljava/lang/Exception;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "can not get service which for sim \'"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "\', only 0,1 accepted as values"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V
throw v0
:try_end_da
.catch Ljava/lang/Exception; {:try_start_4 .. :try_end_da} :catch_da
:catch_da
move-exception v0
invoke-static {v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/Throwable;)V
move v0, v6
goto :goto_b0
:cond_e0
:try_start_e0
invoke-static {p0}, Lin/org/npci/upiapp/core/a;->a(Landroid/content/Context;)Z
move-result v0
if-eqz v0, :cond_13f
new-instance v1, Ljava/util/ArrayList;
invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V
invoke-static {p0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;
move-result-object v0
invoke-virtual {v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoList()Ljava/util/List;
move-result-object v0
invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v2
:goto_f7
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_127
invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/telephony/SubscriptionInfo;
invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I
move-result v0
invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v3
invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
const-string v3, "SimUtil"
new-instance v4, Ljava/lang/StringBuilder;
invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
const-string v5, "SmsManager - subscriptionId: "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-static {v3, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V
goto :goto_f7
:cond_127
invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Integer;
invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
move-result v0
invoke-static {v0}, Landroid/telephony/SmsManager;->getSmsManagerForSubscriptionId(I)Landroid/telephony/SmsManager;
move-result-object v0
const/4 v2, 0x0
move-object v1, p2
move-object v3, p4
move-object v4, p5
move-object v5, p6
invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
goto/16 :goto_af
:cond_13f
invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;
move-result-object v0
const/4 v2, 0x0
move-object v1, p2
move-object v3, p4
move-object v4, p5
move-object v5, p6
invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
:try_end_14b
.catch Ljava/lang/Exception; {:try_start_e0 .. :try_end_14b} :catch_da
goto/16 :goto_af
.end method
.method public static b(Landroid/content/Context;)Z
.registers 3
const/4 v1, 0x1
const-string v0, "phone"
invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/telephony/TelephonyManager;
invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I
move-result v0
if-eq v0, v1, :cond_11
move v0, v1
:goto_10
return v0
:cond_11
const/4 v0, 0x0
goto :goto_10
.end method
.method public static c(Landroid/content/Context;)Ljava/lang/String;
.registers 7
:try_start_0
new-instance v1, Lorg/json/JSONArray;
invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v2, 0x16
if-lt v0, v2, :cond_75
invoke-static {p0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;
move-result-object v0
invoke-virtual {v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoList()Ljava/util/List;
move-result-object v0
invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v2
:goto_17
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_bd
invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/telephony/SubscriptionInfo;
:try_start_23
:try_end_23
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_23} :catch_6b
new-instance v3, Lorg/json/JSONObject;
invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V
const-string v4, "slotId"
invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSimSlotIndex()I
move-result v5
invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
const-string v4, "subscriptionId"
invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I
move-result v5
invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
const-string v4, "displayName"
invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;
move-result-object v5
invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
const-string v4, "carrierName"
invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getCarrierName()Ljava/lang/CharSequence;
move-result-object v5
invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
const-string v4, "phoneNumber"
invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getNumber()Ljava/lang/String;
move-result-object v5
invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
const-string v4, "simId"
invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getIccId()Ljava/lang/String;
move-result-object v0
invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
:try_end_61
.catch Ljava/lang/Exception; {:try_start_23 .. :try_end_61} :catch_62
goto :goto_17
:catch_62
move-exception v0
:try_start_63
const-string v3, "SimUtil"
const-string v4, "Exception getting sim details for SDK >= 22"
invoke-static {v3, v4, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
:try_end_6a
.catch Ljava/lang/Exception; {:try_start_63 .. :try_end_6a} :catch_6b
goto :goto_17
:catch_6b
move-exception v0
const-string v0, "SimUtil"
const-string v1, "Not able to fetch Sim Cards"
invoke-static {v0, v1}, Lin/org/npci/upiapp/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V
const/4 v0, 0x0
:goto_74
return-object v0
:try_start_75
:cond_75
const-string v0, "phone"
invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/telephony/TelephonyManager;
:try_end_7d
.catch Ljava/lang/Exception; {:try_start_75 .. :try_end_7d} :catch_6b
if-eqz v0, :cond_bd
:try_start_7f
new-instance v2, Lorg/json/JSONObject;
invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V
const-string v3, "slotId"
invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I
move-result v4
invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
const-string v3, "subscriptionId"
invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;
move-result-object v4
invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
const-string v3, "displayName"
invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;
move-result-object v4
invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
const-string v3, "carrierName"
invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;
move-result-object v4
invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
const-string v3, "phoneNumber"
invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;
move-result-object v4
invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
const-string v3, "simId"
invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;
move-result-object v0
invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
:goto_bd
:cond_bd
:try_end_bd
.catch Ljava/lang/Exception; {:try_start_7f .. :try_end_bd} :catch_c2
:try_start_bd
invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
move-result-object v0
goto :goto_74
:catch_c2
move-exception v0
const-string v2, "SimUtil"
const-string v3, "Exception getting sim details for SDK < 22"
invoke-static {v2, v3, v0}, Lin/org/npci/upiapp/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
:try_end_ca
.catch Ljava/lang/Exception; {:try_start_bd .. :try_end_ca} :catch_6b
goto :goto_bd
.end method