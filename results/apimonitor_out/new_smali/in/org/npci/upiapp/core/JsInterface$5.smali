.class  Lin/org/npci/upiapp/core/JsInterface$5;
.super Ljava/lang/Object;
.source "JsInterface.java"
.implements Ljava/lang/Runnable;
.field final synthetic a:Ljava/lang/String;
.field final synthetic b:F
.field final synthetic c:I
.field final synthetic d:Lin/org/npci/upiapp/core/JsInterface;
.method constructor <init>(Lin/org/npci/upiapp/core/JsInterface;Ljava/lang/String;FI)V
.registers 5
iput-object p1, p0, Lin/org/npci/upiapp/core/JsInterface$5;->d:Lin/org/npci/upiapp/core/JsInterface;
iput-object p2, p0, Lin/org/npci/upiapp/core/JsInterface$5;->a:Ljava/lang/String;
iput p3, p0, Lin/org/npci/upiapp/core/JsInterface$5;->b:F
iput p4, p0, Lin/org/npci/upiapp/core/JsInterface$5;->c:I
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public run()V
.registers 14
const/4 v12, 0x0
const/high16 v11, -0x3fc0
const/high16 v10, 0x3f80
const v9, 0x3f666666
const v6, 0x3f8ccccd
iget-object v0, p0, Lin/org/npci/upiapp/core/JsInterface$5;->d:Lin/org/npci/upiapp/core/JsInterface;
invoke-static {v0}, Lin/org/npci/upiapp/core/JsInterface;->b(Lin/org/npci/upiapp/core/JsInterface;)Landroid/app/Activity;
move-result-object v0
iget-object v1, p0, Lin/org/npci/upiapp/core/JsInterface$5;->a:Ljava/lang/String;
invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
move-result v1
invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;
move-result-object v0
sget-object v1, Landroid/view/View;->SCALE_X:Landroid/util/Property;
const/16 v2, 0xb
new-array v2, v2, [Landroid/animation/Keyframe;
const/4 v3, 0x0
invoke-static {v12, v10}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v2, v3
const/4 v3, 0x1
const v4, 0x3dcccccd
invoke-static {v4, v9}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v2, v3
const/4 v3, 0x2
const v4, 0x3e4ccccd
invoke-static {v4, v9}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v2, v3
const/4 v3, 0x3
const v4, 0x3e99999a
invoke-static {v4, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v2, v3
const/4 v3, 0x4
const v4, 0x3ecccccd
invoke-static {v4, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v2, v3
const/4 v3, 0x5
const/high16 v4, 0x3f00
invoke-static {v4, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v2, v3
const/4 v3, 0x6
const v4, 0x3f19999a
invoke-static {v4, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v2, v3
const/4 v3, 0x7
const v4, 0x3f333333
invoke-static {v4, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v2, v3
const/16 v3, 0x8
const v4, 0x3f4ccccd
invoke-static {v4, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v2, v3
const/16 v3, 0x9
invoke-static {v9, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v2, v3
const/16 v3, 0xa
invoke-static {v10, v10}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v4
aput-object v4, v2, v3
invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Landroid/util/Property;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;
move-result-object v1
sget-object v2, Landroid/view/View;->SCALE_Y:Landroid/util/Property;
const/16 v3, 0xb
new-array v3, v3, [Landroid/animation/Keyframe;
const/4 v4, 0x0
invoke-static {v12, v10}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/4 v4, 0x1
const v5, 0x3dcccccd
invoke-static {v5, v9}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/4 v4, 0x2
const v5, 0x3e4ccccd
invoke-static {v5, v9}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/4 v4, 0x3
const v5, 0x3e99999a
invoke-static {v5, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/4 v4, 0x4
const v5, 0x3ecccccd
invoke-static {v5, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/4 v4, 0x5
const/high16 v5, 0x3f00
invoke-static {v5, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/4 v4, 0x6
const v5, 0x3f19999a
invoke-static {v5, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/4 v4, 0x7
const v5, 0x3f333333
invoke-static {v5, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/16 v4, 0x8
const v5, 0x3f4ccccd
invoke-static {v5, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/16 v4, 0x9
invoke-static {v9, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
const/16 v4, 0xa
invoke-static {v10, v10}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v5
aput-object v5, v3, v4
invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Landroid/util/Property;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;
move-result-object v2
sget-object v3, Landroid/view/View;->ROTATION:Landroid/util/Property;
const/16 v4, 0xb
new-array v4, v4, [Landroid/animation/Keyframe;
const/4 v5, 0x0
invoke-static {v12, v12}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v6
aput-object v6, v4, v5
const/4 v5, 0x1
const v6, 0x3dcccccd
iget v7, p0, Lin/org/npci/upiapp/core/JsInterface$5;->b:F
mul-float/2addr v7, v11
invoke-static {v6, v7}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v6
aput-object v6, v4, v5
const/4 v5, 0x2
const v6, 0x3e4ccccd
iget v7, p0, Lin/org/npci/upiapp/core/JsInterface$5;->b:F
mul-float/2addr v7, v11
invoke-static {v6, v7}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v6
aput-object v6, v4, v5
const/4 v5, 0x3
const v6, 0x3e99999a
const/high16 v7, 0x4040
iget v8, p0, Lin/org/npci/upiapp/core/JsInterface$5;->b:F
mul-float/2addr v7, v8
invoke-static {v6, v7}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v6
aput-object v6, v4, v5
const/4 v5, 0x4
const v6, 0x3ecccccd
iget v7, p0, Lin/org/npci/upiapp/core/JsInterface$5;->b:F
mul-float/2addr v7, v11
invoke-static {v6, v7}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v6
aput-object v6, v4, v5
const/4 v5, 0x5
const/high16 v6, 0x3f00
const/high16 v7, 0x4040
iget v8, p0, Lin/org/npci/upiapp/core/JsInterface$5;->b:F
mul-float/2addr v7, v8
invoke-static {v6, v7}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v6
aput-object v6, v4, v5
const/4 v5, 0x6
const v6, 0x3f19999a
iget v7, p0, Lin/org/npci/upiapp/core/JsInterface$5;->b:F
mul-float/2addr v7, v11
invoke-static {v6, v7}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v6
aput-object v6, v4, v5
const/4 v5, 0x7
const v6, 0x3f333333
const/high16 v7, 0x4040
iget v8, p0, Lin/org/npci/upiapp/core/JsInterface$5;->b:F
mul-float/2addr v7, v8
invoke-static {v6, v7}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v6
aput-object v6, v4, v5
const/16 v5, 0x8
const v6, 0x3f4ccccd
iget v7, p0, Lin/org/npci/upiapp/core/JsInterface$5;->b:F
mul-float/2addr v7, v11
invoke-static {v6, v7}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v6
aput-object v6, v4, v5
const/16 v5, 0x9
const/high16 v6, 0x4040
iget v7, p0, Lin/org/npci/upiapp/core/JsInterface$5;->b:F
mul-float/2addr v6, v7
invoke-static {v9, v6}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v6
aput-object v6, v4, v5
const/16 v5, 0xa
invoke-static {v10, v12}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;
move-result-object v6
aput-object v6, v4, v5
invoke-static {v3, v4}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Landroid/util/Property;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;
move-result-object v3
const/4 v4, 0x3
new-array v4, v4, [Landroid/animation/PropertyValuesHolder;
const/4 v5, 0x0
aput-object v1, v4, v5
const/4 v1, 0x1
aput-object v2, v4, v1
const/4 v1, 0x2
aput-object v3, v4, v1
invoke-static {v0, v4}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;
move-result-object v0
iget v1, p0, Lin/org/npci/upiapp/core/JsInterface$5;->c:I
int-to-long v2, v1
invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;
move-result-object v0
invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V
return-void
.end method