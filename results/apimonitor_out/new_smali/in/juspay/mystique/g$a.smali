.class  Lin/juspay/mystique/g$a;
.super Ljava/lang/Object;
.source "SourceFile"
.field  a:Ljava/lang/Class;
.field  b:Ljava/lang/String;
.field  c:[Ljava/lang/Class;
.method public constructor <init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lin/juspay/mystique/g$a;->a:Ljava/lang/Class;
iput-object p2, p0, Lin/juspay/mystique/g$a;->b:Ljava/lang/String;
iput-object p3, p0, Lin/juspay/mystique/g$a;->c:[Ljava/lang/Class;
return-void
.end method
.method public equals(Ljava/lang/Object;)Z
.registers 5
const/4 v0, 0x0
if-ne p0, p1, :cond_5
const/4 v0, 0x1
:goto_4
:cond_4
return v0
:cond_5
if-eqz p1, :cond_4
invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v1
invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v2
if-ne v1, v2, :cond_4
check-cast p1, Lin/juspay/mystique/g$a;
iget-object v1, p0, Lin/juspay/mystique/g$a;->a:Ljava/lang/Class;
iget-object v2, p1, Lin/juspay/mystique/g$a;->a:Ljava/lang/Class;
invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_4
iget-object v1, p0, Lin/juspay/mystique/g$a;->b:Ljava/lang/String;
iget-object v2, p1, Lin/juspay/mystique/g$a;->b:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_4
iget-object v0, p0, Lin/juspay/mystique/g$a;->c:[Ljava/lang/Class;
iget-object v1, p1, Lin/juspay/mystique/g$a;->c:[Ljava/lang/Class;
invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z
move-result v0
goto :goto_4
.end method
.method public hashCode()I
.registers 3
iget-object v0, p0, Lin/juspay/mystique/g$a;->a:Ljava/lang/Class;
invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I
move-result v0
mul-int/lit8 v0, v0, 0x1f
iget-object v1, p0, Lin/juspay/mystique/g$a;->b:Ljava/lang/String;
invoke-virtual {v1}, Ljava/lang/String;->hashCode()I
move-result v1
add-int/2addr v0, v1
mul-int/lit8 v1, v0, 0x1f
iget-object v0, p0, Lin/juspay/mystique/g$a;->c:[Ljava/lang/Class;
if-eqz v0, :cond_1d
iget-object v0, p0, Lin/juspay/mystique/g$a;->c:[Ljava/lang/Class;
invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I
move-result v0
:goto_1b
add-int/2addr v0, v1
return v0
:cond_1d
const/4 v0, 0x0
goto :goto_1b
.end method