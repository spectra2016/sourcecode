.class  Lin/juspay/mystique/g$2;
.super Ljava/lang/Object;
.source "SourceFile"
.implements Landroid/view/View$OnTouchListener;
.field final synthetic a:Ljava/lang/String;
.field final synthetic b:Lin/juspay/mystique/g;
.method constructor <init>(Lin/juspay/mystique/g;Ljava/lang/String;)V
.registers 3
iput-object p1, p0, Lin/juspay/mystique/g$2;->b:Lin/juspay/mystique/g;
iput-object p2, p0, Lin/juspay/mystique/g$2;->a:Ljava/lang/String;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
.registers 11
const/high16 v2, 0x42c8
const-string v0, "0"
invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I
move-result v1
packed-switch v1, :pswitch_data_da
:cond_b
:goto_b
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "window.callUICallback(\'"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p0, Lin/juspay/mystique/g$2;->a:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "\',\'"
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, "\');"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
iget-object v1, p0, Lin/juspay/mystique/g$2;->b:Lin/juspay/mystique/g;
invoke-static {v1}, Lin/juspay/mystique/g;->a(Lin/juspay/mystique/g;)Lin/juspay/mystique/d;
move-result-object v1
invoke-virtual {v1, v0}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
const/4 v0, 0x1
return v0
:pswitch_3b
invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F
move-result v1
invoke-static {v1}, Lin/juspay/mystique/g;->b(F)F
invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F
move-result v1
invoke-static {v1}, Lin/juspay/mystique/g;->c(F)F
goto :goto_b
:pswitch_4a
invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F
move-result v1
invoke-static {v1}, Lin/juspay/mystique/g;->d(F)F
invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F
move-result v1
invoke-static {v1}, Lin/juspay/mystique/g;->e(F)F
invoke-static {}, Lin/juspay/mystique/g;->b()F
move-result v1
invoke-static {}, Lin/juspay/mystique/g;->c()F
move-result v3
sub-float/2addr v1, v3
invoke-static {}, Lin/juspay/mystique/g;->d()F
move-result v3
invoke-static {}, Lin/juspay/mystique/g;->e()F
move-result v4
sub-float/2addr v3, v4
float-to-double v4, v3
float-to-double v6, v1
invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D
move-result-wide v4
invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D
move-result-wide v4
double-to-float v1, v4
const/4 v3, 0x0
cmpg-float v3, v1, v3
if-gez v3, :cond_7d
const/high16 v3, 0x43b4
add-float/2addr v1, v3
:cond_7d
const/high16 v3, 0x4234
cmpl-float v3, v1, v3
if-ltz v3, :cond_89
const/high16 v3, 0x4307
cmpg-float v3, v1, v3
if-lez v3, :cond_96
:cond_89
const/high16 v3, 0x4361
cmpl-float v3, v1, v3
if-ltz v3, :cond_b8
const v3, 0x439d8000
cmpg-float v1, v1, v3
if-gtz v1, :cond_b8
:cond_96
invoke-static {}, Lin/juspay/mystique/g;->d()F
move-result v1
invoke-static {}, Lin/juspay/mystique/g;->e()F
move-result v3
sub-float/2addr v1, v3
cmpl-float v1, v1, v2
if-lez v1, :cond_a7
const-string v0, "2"
goto/16 :goto_b
:cond_a7
invoke-static {}, Lin/juspay/mystique/g;->e()F
move-result v1
invoke-static {}, Lin/juspay/mystique/g;->d()F
move-result v3
sub-float/2addr v1, v3
cmpl-float v1, v1, v2
if-lez v1, :cond_b
const-string v0, "-2"
goto/16 :goto_b
:cond_b8
invoke-static {}, Lin/juspay/mystique/g;->b()F
move-result v1
invoke-static {}, Lin/juspay/mystique/g;->c()F
move-result v3
sub-float/2addr v1, v3
cmpl-float v1, v1, v2
if-lez v1, :cond_c9
const-string v0, "1"
goto/16 :goto_b
:cond_c9
invoke-static {}, Lin/juspay/mystique/g;->c()F
move-result v1
invoke-static {}, Lin/juspay/mystique/g;->b()F
move-result v3
sub-float/2addr v1, v3
cmpl-float v1, v1, v2
if-lez v1, :cond_b
const-string v0, "-1"
goto/16 :goto_b
:pswitch_data_da
.packed-switch 0x0
:pswitch_3b
:pswitch_4a
.end packed-switch
.end method