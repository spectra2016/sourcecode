.class public Lin/juspay/mystique/b;
.super Ljava/lang/Object;
.source "SourceFile"
.implements Ljava/lang/reflect/InvocationHandler;
.field private a:Ljava/lang/Object;
.field private b:Z
.field private c:Lin/juspay/mystique/d;
.method constructor <init>(Ljava/lang/Object;Lin/juspay/mystique/d;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, 0x1
iput-boolean v0, p0, Lin/juspay/mystique/b;->b:Z
iput-object p1, p0, Lin/juspay/mystique/b;->a:Ljava/lang/Object;
iput-object p2, p0, Lin/juspay/mystique/b;->c:Lin/juspay/mystique/d;
return-void
.end method
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
.registers 11
const/4 v1, 0x0
:try_start_1
iget-boolean v0, p0, Lin/juspay/mystique/b;->b:Z
if-eqz v0, :cond_b
iget-object v0, p0, Lin/juspay/mystique/b;->a:Ljava/lang/Object;
invoke-virtual {p2, v0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
:try_end_a
.catch Ljava/lang/Exception; {:try_start_1 .. :try_end_a} :catch_a0
move-result-object v1
:cond_b
if-nez v1, :cond_3d
:try_start_d
const-string v0, "null"
:try_end_f
.catch Ljava/lang/Exception; {:try_start_d .. :try_end_f} :catch_f0
:goto_f
if-eqz p3, :cond_9d
:try_start_11
const-string v3, "\""
const-string v2, ","
const/4 v1, 0x0
move v6, v1
move-object v1, v2
move v2, v6
:goto_19
array-length v4, p3
if-ge v2, v4, :cond_3f
array-length v4, p3
add-int/lit8 v4, v4, -0x1
if-ne v2, v4, :cond_23
const-string v1, ""
:cond_23
new-instance v4, Ljava/lang/StringBuilder;
invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
aget-object v4, p3, v2
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v3
add-int/lit8 v2, v2, 0x1
goto :goto_19
:cond_3d
move-object v0, v1
goto :goto_f
:cond_3f
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, "\""
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
:goto_52
iget-object v2, p0, Lin/juspay/mystique/b;->c:Lin/juspay/mystique/d;
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "window.duiProxyCallback(\'"
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v3
const-string v4, "\',\'"
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
iget-object v4, p0, Lin/juspay/mystique/b;->a:Ljava/lang/Object;
invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v4
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
const-string v4, "\',\'"
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;
move-result-object v4
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
const-string v4, "\',"
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v3, ");"
invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v2, v1}, Lin/juspay/mystique/d;->a(Ljava/lang/String;)V
:goto_9c
return-object v0
:cond_9d
const-string v1, "\'\'"
:try_end_9f
.catch Ljava/lang/Exception; {:try_start_11 .. :try_end_9f} :catch_f5
goto :goto_52
:catch_a0
move-exception v0
move-object v6, v0
move-object v0, v1
move-object v1, v6
:goto_a4
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
iget-object v3, p0, Lin/juspay/mystique/b;->a:Ljava/lang/Object;
invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, "-"
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
iget-object v3, p0, Lin/juspay/mystique/b;->c:Lin/juspay/mystique/d;
invoke-virtual {v3}, Lin/juspay/mystique/d;->c()Lin/juspay/mystique/e;
move-result-object v3
const-string v4, "InvocationHandler"
new-instance v5, Ljava/lang/StringBuilder;
invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
move-result-object v1
invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v5, "-"
invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-interface {v3, v4, v1}, Lin/juspay/mystique/e;->a(Ljava/lang/String;Ljava/lang/String;)V
goto :goto_9c
:catch_f0
move-exception v0
move-object v6, v0
move-object v0, v1
move-object v1, v6
goto :goto_a4
:catch_f5
move-exception v1
goto :goto_a4
.end method