.class  Lin/juspay/tracker/e$b;
.super Ljava/util/TimerTask;
.source "GodelTracker.java"
.field final synthetic a:Lin/juspay/tracker/e;
.method constructor <init>(Lin/juspay/tracker/e;)V
.registers 2
iput-object p1, p0, Lin/juspay/tracker/e$b;->a:Lin/juspay/tracker/e;
invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V
return-void
.end method
.method static synthetic a(Lin/juspay/tracker/e$b;Ljava/io/File;)Ljava/lang/StringBuilder;
.registers 3
invoke-direct {p0, p1}, Lin/juspay/tracker/e$b;->a(Ljava/io/File;)Ljava/lang/StringBuilder;
move-result-object v0
return-object v0
.end method
.method private a(Ljava/io/File;)Ljava/lang/StringBuilder;
.registers 6
new-instance v0, Ljava/lang/StringBuilder;
const-string v1, ""
invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V
invoke-virtual {p1}, Ljava/io/File;->exists()Z
move-result v1
if-eqz v1, :cond_22
:try_start_d
new-instance v1, Ljava/io/FileInputStream;
invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
:goto_12
invoke-virtual {v1}, Ljava/io/FileInputStream;->read()I
move-result v2
const/4 v3, -0x1
if-eq v2, v3, :cond_22
int-to-char v2, v2
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
:try_end_1d
.catch Ljava/io/IOException; {:try_start_d .. :try_end_1d} :catch_1e
goto :goto_12
:catch_1e
move-exception v1
invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
:cond_22
return-object v0
.end method
.method public run()V
.registers 3
invoke-static {}, Lin/juspay/tracker/e;->j()Z
move-result v0
if-nez v0, :cond_1f
iget-object v0, p0, Lin/juspay/tracker/e$b;->a:Lin/juspay/tracker/e;
invoke-virtual {v0}, Lin/juspay/tracker/e;->c()Z
move-result v0
if-nez v0, :cond_1f
invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;
move-result-object v0
new-instance v1, Landroid/os/Handler;
invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V
new-instance v0, Lin/juspay/tracker/e$b$1;
invoke-direct {v0, p0}, Lin/juspay/tracker/e$b$1;-><init>(Lin/juspay/tracker/e$b;)V
invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
:cond_1f
return-void
.end method