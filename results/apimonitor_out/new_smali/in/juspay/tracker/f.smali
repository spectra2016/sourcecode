.class public Lin/juspay/tracker/f;
.super Ljava/lang/Object;
.source "JuspayLogger.java"
.field public static a:Z
.field private static b:Z
.method static constructor <clinit>()V
.registers 1
const/4 v0, 0x0
sput-boolean v0, Lin/juspay/tracker/f;->a:Z
sput-boolean v0, Lin/juspay/tracker/f;->b:Z
return-void
.end method
.method public static a(Ljava/lang/String;Ljava/lang/String;)V
.registers 3
sget-boolean v0, Lin/juspay/tracker/f;->a:Z
if-eqz v0, :cond_7
invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
:cond_7
return-void
.end method
.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.registers 5
invoke-static {p0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
new-instance v0, Lin/juspay/tracker/c;
invoke-direct {v0}, Lin/juspay/tracker/c;-><init>()V
invoke-virtual {v0, p1}, Lin/juspay/tracker/c;->a(Ljava/lang/String;)Lin/juspay/tracker/c;
move-result-object v0
invoke-virtual {v0, p2}, Lin/juspay/tracker/c;->a(Ljava/lang/Throwable;)Lin/juspay/tracker/c;
move-result-object v0
invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;
move-result-object v1
invoke-virtual {v1, v0}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/c;)V
return-void
.end method
.method public static b(Ljava/lang/String;Ljava/lang/String;)V
.registers 3
sget-boolean v0, Lin/juspay/tracker/f;->a:Z
if-eqz v0, :cond_b
sget-boolean v0, Lin/juspay/tracker/f;->b:Z
if-eqz v0, :cond_b
invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
:cond_b
return-void
.end method