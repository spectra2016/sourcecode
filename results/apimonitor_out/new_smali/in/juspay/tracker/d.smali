.class public Lin/juspay/tracker/d;
.super Ljava/lang/Object;
.source "FileUtil.java"
.field private static a:Ljava/lang/String;
.method static constructor <clinit>()V
.registers 1
const-class v0, Lin/juspay/tracker/d;
invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;
move-result-object v0
sput-object v0, Lin/juspay/tracker/d;->a:Ljava/lang/String;
return-void
.end method
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public static a([B)[B
.registers 4
:try_start_0
new-instance v0, Ljava/io/ByteArrayOutputStream;
array-length v1, p0
invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V
new-instance v1, Ljava/util/zip/GZIPOutputStream;
invoke-direct {v1, v0}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V
invoke-virtual {v1, p0}, Ljava/util/zip/GZIPOutputStream;->write([B)V
invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->close()V
sget-object v1, Lin/juspay/tracker/d;->a:Ljava/lang/String;
const-string v2, "Gzipping complete"
invoke-static {v1, v2}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
:try_end_1e
.catch Ljava/io/IOException; {:try_start_0 .. :try_end_1e} :catch_20
move-result-object v0
return-object v0
:catch_20
move-exception v0
sget-object v1, Lin/juspay/tracker/d;->a:Ljava/lang/String;
const-string v2, "Could not gzip"
invoke-static {v1, v2, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
new-instance v1, Ljava/lang/RuntimeException;
invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V
throw v1
.end method