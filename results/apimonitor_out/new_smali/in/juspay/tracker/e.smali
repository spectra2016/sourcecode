.class public Lin/juspay/tracker/e;
.super Ljava/lang/Object;
.source "GodelTracker.java"
.field private static final a:Ljava/lang/String;
.field private static b:J
.field private static c:J
.field private static d:J
.field private static e:J
.field private static f:Ljava/util/List;
.field private static g:Ljava/util/Map;
.field private static p:Z
.field private static w:Lin/juspay/tracker/e;
.field private h:Ljava/util/Timer;
.field private i:Ljava/util/TimerTask;
.field private j:Lin/juspay/tracker/e$c;
.field private k:I
.field private l:Ljava/lang/String;
.field private m:J
.field private n:Ljava/lang/String;
.field private o:Ljava/lang/String;
.field private q:Z
.field private r:J
.field private s:Ljava/util/Timer;
.field private volatile t:Z
.field private u:Lin/juspay/tracker/e$a;
.field private v:Ljava/lang/String;
.field private x:Landroid/content/Context;
.field private y:Ljava/lang/String;
.method static constructor <clinit>()V
.registers 4
const-wide/16 v2, 0xbb8
const-class v0, Lin/juspay/tracker/e;
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
sput-object v0, Lin/juspay/tracker/e;->a:Ljava/lang/String;
sput-wide v2, Lin/juspay/tracker/e;->b:J
const-wide/16 v0, 0x1388
sput-wide v0, Lin/juspay/tracker/e;->c:J
const-wide/16 v0, 0x2710
sput-wide v0, Lin/juspay/tracker/e;->d:J
sput-wide v2, Lin/juspay/tracker/e;->e:J
new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;
invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V
sput-object v0, Lin/juspay/tracker/e;->f:Ljava/util/List;
new-instance v0, Ljava/util/HashMap;
invoke-direct {v0}, Ljava/util/HashMap;-><init>()V
sput-object v0, Lin/juspay/tracker/e;->g:Ljava/util/Map;
const/4 v0, 0x0
sput-boolean v0, Lin/juspay/tracker/e;->p:Z
const/4 v0, 0x0
sput-object v0, Lin/juspay/tracker/e;->w:Lin/juspay/tracker/e;
return-void
.end method
.method private constructor <init>()V
.registers 4
const/4 v2, 0x0
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput v2, p0, Lin/juspay/tracker/e;->k:I
const/4 v0, 0x1
iput-boolean v0, p0, Lin/juspay/tracker/e;->q:Z
const-wide/16 v0, 0x0
iput-wide v0, p0, Lin/juspay/tracker/e;->r:J
const/4 v0, 0x0
iput-object v0, p0, Lin/juspay/tracker/e;->s:Ljava/util/Timer;
iput-boolean v2, p0, Lin/juspay/tracker/e;->t:Z
sget-object v0, Lin/juspay/tracker/e$a;->a:Lin/juspay/tracker/e$a;
iput-object v0, p0, Lin/juspay/tracker/e;->u:Lin/juspay/tracker/e$a;
invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;
move-result-object v0
invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lin/juspay/tracker/e;->l:Ljava/lang/String;
sget-object v0, Lin/juspay/tracker/e;->a:Ljava/lang/String;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Godel Session Id - "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget-object v2, p0, Lin/juspay/tracker/e;->l:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
new-instance v0, Ljava/util/Date;
invoke-direct {v0}, Ljava/util/Date;-><init>()V
invoke-virtual {v0}, Ljava/util/Date;->getTime()J
move-result-wide v0
iput-wide v0, p0, Lin/juspay/tracker/e;->m:J
new-instance v0, Lin/juspay/tracker/e$b;
invoke-direct {v0, p0}, Lin/juspay/tracker/e$b;-><init>(Lin/juspay/tracker/e;)V
iput-object v0, p0, Lin/juspay/tracker/e;->i:Ljava/util/TimerTask;
new-instance v0, Lin/juspay/tracker/e$c;
invoke-direct {v0, p0}, Lin/juspay/tracker/e$c;-><init>(Lin/juspay/tracker/e;)V
iput-object v0, p0, Lin/juspay/tracker/e;->j:Lin/juspay/tracker/e$c;
return-void
.end method
.method static synthetic a(Lin/juspay/tracker/e;)Landroid/content/Context;
.registers 2
iget-object v0, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
return-object v0
.end method
.method public static a()Lin/juspay/tracker/e;
.registers 2
const-class v1, Lin/juspay/tracker/e;
monitor-enter v1
:try_start_3
sget-object v0, Lin/juspay/tracker/e;->w:Lin/juspay/tracker/e;
if-nez v0, :cond_13
new-instance v0, Lin/juspay/tracker/e;
invoke-direct {v0}, Lin/juspay/tracker/e;-><init>()V
sput-object v0, Lin/juspay/tracker/e;->w:Lin/juspay/tracker/e;
sget-object v0, Lin/juspay/tracker/e;->w:Lin/juspay/tracker/e;
invoke-direct {v0}, Lin/juspay/tracker/e;->l()V
:cond_13
sget-object v0, Lin/juspay/tracker/e;->w:Lin/juspay/tracker/e;
monitor-exit v1
return-object v0
:catchall_17
move-exception v0
monitor-exit v1
:try_end_19
.catchall {:try_start_3 .. :try_end_19} :catchall_17
throw v0
.end method
.method public static a(Lin/juspay/tracker/e$a;)V
.registers 2
const/4 v0, 0x0
sput-object v0, Lin/juspay/tracker/e;->w:Lin/juspay/tracker/e;
invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;
move-result-object v0
sput-object v0, Lin/juspay/tracker/e;->w:Lin/juspay/tracker/e;
sget-object v0, Lin/juspay/tracker/e;->w:Lin/juspay/tracker/e;
iput-object p0, v0, Lin/juspay/tracker/e;->u:Lin/juspay/tracker/e$a;
return-void
.end method
.method private a(Ljava/util/Map;Lorg/json/JSONArray;)Z
.registers 9
const/4 v3, 0x0
move v2, v3
:goto_2
invoke-virtual {p2}, Lorg/json/JSONArray;->length()I
move-result v0
if-ge v2, v0, :cond_33
:try_start_8
invoke-virtual {p2, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
move-result-object v4
invoke-virtual {v4}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
move-result-object v5
:cond_10
invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_38
invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_10
invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v1
check-cast v1, Ljava/lang/String;
invoke-virtual {v4, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
:try_end_2f
.catch Lorg/json/JSONException; {:try_start_8 .. :try_end_2f} :catch_34
move-result v0
if-eqz v0, :cond_10
const/4 v3, 0x1
:cond_33
return v3
:catch_34
move-exception v0
invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
:cond_38
add-int/lit8 v0, v2, 0x1
move v2, v0
goto :goto_2
.end method
.method private b(Ljava/util/Map;)V
.registers 5
const-string v0, "session_id"
iget-object v1, p0, Lin/juspay/tracker/e;->l:Ljava/lang/String;
invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "bank"
iget-object v1, p0, Lin/juspay/tracker/e;->n:Ljava/lang/String;
invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "sn"
iget v1, p0, Lin/juspay/tracker/e;->k:I
add-int/lit8 v1, v1, 0x1
iput v1, p0, Lin/juspay/tracker/e;->k:I
invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
move-result-object v1
invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
sget-object v0, Lin/juspay/tracker/e;->a:Ljava/lang/String;
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Analytics: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Lin/juspay/tracker/f;->b(Ljava/lang/String;Ljava/lang/String;)V
invoke-direct {p0, p1}, Lin/juspay/tracker/e;->c(Ljava/util/Map;)Z
move-result v0
if-eqz v0, :cond_44
sget-object v0, Lin/juspay/tracker/e;->f:Ljava/util/List;
invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
:cond_44
return-void
.end method
.method static synthetic b(Lin/juspay/tracker/e;)Z
.registers 2
iget-boolean v0, p0, Lin/juspay/tracker/e;->t:Z
return v0
.end method
.method static synthetic b(Z)Z
.registers 1
sput-boolean p0, Lin/juspay/tracker/e;->p:Z
return p0
.end method
.method static synthetic c(Lin/juspay/tracker/e;)Ljava/util/TimerTask;
.registers 2
iget-object v0, p0, Lin/juspay/tracker/e;->i:Ljava/util/TimerTask;
return-object v0
.end method
.method private c(Ljava/util/Map;)Z
.registers 6
const/4 v2, 0x1
const/4 v1, 0x0
const-string v0, "log_level"
invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_48
const-string v0, "log_level"
invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
move-result v0
:goto_1a
invoke-static {}, Lin/juspay/tracker/i;->a()Lin/juspay/tracker/i;
move-result-object v3
invoke-virtual {v3}, Lin/juspay/tracker/i;->e()I
move-result v3
if-ge v0, v3, :cond_46
move v0, v1
:goto_25
invoke-static {}, Lin/juspay/tracker/i;->a()Lin/juspay/tracker/i;
move-result-object v3
invoke-virtual {v3}, Lin/juspay/tracker/i;->d()Lorg/json/JSONArray;
move-result-object v3
invoke-direct {p0, p1, v3}, Lin/juspay/tracker/e;->a(Ljava/util/Map;Lorg/json/JSONArray;)Z
move-result v3
if-eqz v3, :cond_44
:goto_33
invoke-static {}, Lin/juspay/tracker/i;->a()Lin/juspay/tracker/i;
move-result-object v0
invoke-virtual {v0}, Lin/juspay/tracker/i;->c()Lorg/json/JSONArray;
move-result-object v0
invoke-direct {p0, p1, v0}, Lin/juspay/tracker/e;->a(Ljava/util/Map;Lorg/json/JSONArray;)Z
move-result v0
if-eqz v0, :cond_42
:goto_41
return v2
:cond_42
move v2, v1
goto :goto_41
:cond_44
move v1, v0
goto :goto_33
:cond_46
move v0, v2
goto :goto_25
:cond_48
move v0, v1
goto :goto_1a
.end method
.method static synthetic d(Lin/juspay/tracker/e;)Lin/juspay/tracker/e$a;
.registers 2
iget-object v0, p0, Lin/juspay/tracker/e;->u:Lin/juspay/tracker/e$a;
return-object v0
.end method
.method public static e()Ljava/lang/String;
.registers 4
:try_start_0
invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;
move-result-object v0
invoke-static {v0}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;
move-result-object v0
invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v1
:cond_c
invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_4d
invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/net/NetworkInterface;
invoke-virtual {v0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;
move-result-object v0
invoke-static {v0}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;
move-result-object v0
invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v2
:cond_24
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_c
invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/net/InetAddress;
invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z
move-result v3
if-nez v3, :cond_24
invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
move-result-object v0
invoke-static {v0}, Lorg/apache/http/conn/util/InetAddressUtils;->isIPv4Address(Ljava/lang/String;)Z
:try_end_41
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_41} :catch_45
move-result v3
if-eqz v3, :cond_24
:goto_44
return-object v0
:catch_45
move-exception v0
sget-object v1, Lin/juspay/tracker/e;->a:Ljava/lang/String;
const-string v2, "Failed to Retreive IP address"
invoke-static {v1, v2, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
:cond_4d
const-string v0, ""
goto :goto_44
.end method
.method static synthetic h()Ljava/util/List;
.registers 1
sget-object v0, Lin/juspay/tracker/e;->f:Ljava/util/List;
return-object v0
.end method
.method static synthetic i()Ljava/lang/String;
.registers 1
sget-object v0, Lin/juspay/tracker/e;->a:Ljava/lang/String;
return-object v0
.end method
.method static synthetic j()Z
.registers 1
sget-boolean v0, Lin/juspay/tracker/e;->p:Z
return v0
.end method
.method private k()V
.registers 7
iget-object v0, p0, Lin/juspay/tracker/e;->h:Ljava/util/Timer;
if-nez v0, :cond_16
new-instance v0, Ljava/util/Timer;
invoke-direct {v0}, Ljava/util/Timer;-><init>()V
iput-object v0, p0, Lin/juspay/tracker/e;->h:Ljava/util/Timer;
iget-object v0, p0, Lin/juspay/tracker/e;->h:Ljava/util/Timer;
iget-object v1, p0, Lin/juspay/tracker/e;->i:Ljava/util/TimerTask;
sget-wide v2, Lin/juspay/tracker/e;->b:J
sget-wide v4, Lin/juspay/tracker/e;->c:J
invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
:cond_16
iget-object v0, p0, Lin/juspay/tracker/e;->s:Ljava/util/Timer;
if-nez v0, :cond_2c
new-instance v0, Ljava/util/Timer;
invoke-direct {v0}, Ljava/util/Timer;-><init>()V
iput-object v0, p0, Lin/juspay/tracker/e;->s:Ljava/util/Timer;
iget-object v0, p0, Lin/juspay/tracker/e;->s:Ljava/util/Timer;
iget-object v1, p0, Lin/juspay/tracker/e;->j:Lin/juspay/tracker/e$c;
sget-wide v2, Lin/juspay/tracker/e;->b:J
sget-wide v4, Lin/juspay/tracker/e;->e:J
invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
:cond_2c
return-void
.end method
.method private l()V
.registers 5
sget-object v0, Lin/juspay/tracker/e;->w:Lin/juspay/tracker/e;
if-eqz v0, :cond_4c
sget-object v0, Lin/juspay/tracker/e;->g:Ljava/util/Map;
invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;
move-result-object v0
invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
move-result-object v2
:goto_e
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_47
invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/util/Map$Entry;
new-instance v1, Lin/juspay/tracker/b;
invoke-direct {v1}, Lin/juspay/tracker/b;-><init>()V
sget-object v3, Lin/juspay/tracker/b$b;->b:Lin/juspay/tracker/b$b;
invoke-virtual {v1, v3}, Lin/juspay/tracker/b;->a(Lin/juspay/tracker/b$b;)Lin/juspay/tracker/b;
move-result-object v1
sget-object v3, Lin/juspay/tracker/b$a;->h:Lin/juspay/tracker/b$a;
invoke-virtual {v1, v3}, Lin/juspay/tracker/b;->a(Lin/juspay/tracker/b$a;)Lin/juspay/tracker/b;
move-result-object v3
invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;
move-result-object v1
check-cast v1, Ljava/lang/String;
invoke-virtual {v3, v1}, Lin/juspay/tracker/b;->b(Ljava/lang/String;)Lin/juspay/tracker/b;
move-result-object v1
invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
invoke-virtual {v1, v0}, Lin/juspay/tracker/b;->c(Ljava/lang/String;)Lin/juspay/tracker/b;
move-result-object v0
invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;
move-result-object v1
invoke-virtual {v1, v0}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/b;)V
goto :goto_e
:cond_47
sget-object v0, Lin/juspay/tracker/e;->g:Ljava/util/Map;
invoke-interface {v0}, Ljava/util/Map;->clear()V
:cond_4c
return-void
.end method
.method public a(Landroid/content/Context;)V
.registers 2
iput-object p1, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
return-void
.end method
.method public a(Lin/juspay/tracker/b;)V
.registers 8
invoke-static {}, Lin/juspay/tracker/g;->a()Lin/juspay/tracker/g;
move-result-object v0
invoke-virtual {v0, p1}, Lin/juspay/tracker/g;->a(Lin/juspay/tracker/b;)Lin/juspay/tracker/b;
move-result-object v0
if-nez v0, :cond_b
:goto_a
return-void
:cond_b
new-instance v1, Ljava/util/HashMap;
invoke-direct {v1}, Ljava/util/HashMap;-><init>()V
const-string v2, "type"
const-string v3, "event"
invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v2, "at"
iget-object v3, v0, Lin/juspay/tracker/b;->a:Ljava/util/Date;
invoke-virtual {v3}, Ljava/util/Date;->getTime()J
move-result-wide v4
invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;
move-result-object v3
invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v2, "category"
iget-object v3, v0, Lin/juspay/tracker/b;->b:Ljava/lang/String;
invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v2, "action"
iget-object v3, v0, Lin/juspay/tracker/b;->c:Ljava/lang/String;
invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v2, "label"
iget-object v3, v0, Lin/juspay/tracker/b;->d:Ljava/lang/String;
invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v2, "value"
iget-object v0, v0, Lin/juspay/tracker/b;->e:Ljava/lang/String;
invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "pageId"
sget v2, Lin/juspay/tracker/h;->h:I
invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
invoke-direct {p0, v1}, Lin/juspay/tracker/e;->b(Ljava/util/Map;)V
goto :goto_a
.end method
.method public a(Lin/juspay/tracker/c;)V
.registers 6
new-instance v0, Ljava/util/HashMap;
invoke-direct {v0}, Ljava/util/HashMap;-><init>()V
const-string v1, "type"
const-string v2, "Exception"
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v1, "at"
invoke-virtual {p1}, Lin/juspay/tracker/c;->c()Ljava/util/Date;
move-result-object v2
invoke-virtual {v2}, Ljava/util/Date;->getTime()J
move-result-wide v2
invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;
move-result-object v2
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v1, "message"
invoke-virtual {p1}, Lin/juspay/tracker/c;->b()Ljava/lang/Throwable;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;
move-result-object v2
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v1, "stackTrace"
invoke-virtual {p1}, Lin/juspay/tracker/c;->b()Ljava/lang/Throwable;
move-result-object v2
invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;
move-result-object v2
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v1, "description"
invoke-virtual {p1}, Lin/juspay/tracker/c;->a()Ljava/lang/String;
move-result-object v2
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v1, "pageId"
sget v2, Lin/juspay/tracker/h;->h:I
invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
move-result-object v2
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v1, "log_level"
const/4 v2, 0x2
invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
move-result-object v2
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
invoke-direct {p0, v0}, Lin/juspay/tracker/e;->b(Ljava/util/Map;)V
return-void
.end method
.method public a(Ljava/lang/String;)V
.registers 2
iput-object p1, p0, Lin/juspay/tracker/e;->v:Ljava/lang/String;
return-void
.end method
.method public a(Ljava/util/Date;Ljava/lang/String;)V
.registers 7
new-instance v0, Ljava/util/HashMap;
invoke-direct {v0}, Ljava/util/HashMap;-><init>()V
const-string v1, "type"
const-string v2, "jsError"
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v1, "at"
invoke-virtual {p1}, Ljava/util/Date;->getTime()J
move-result-wide v2
invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;
move-result-object v2
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v1, "stackTrace"
invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v1, "pageId"
sget v2, Lin/juspay/tracker/h;->h:I
invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
move-result-object v2
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
invoke-direct {p0, v0}, Lin/juspay/tracker/e;->b(Ljava/util/Map;)V
return-void
.end method
.method public a(Ljava/util/Map;)V
.registers 2
invoke-direct {p0, p1}, Lin/juspay/tracker/e;->b(Ljava/util/Map;)V
return-void
.end method
.method public a(Z)V
.registers 4
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v0
iput-wide v0, p0, Lin/juspay/tracker/e;->r:J
iput-boolean p1, p0, Lin/juspay/tracker/e;->q:Z
return-void
.end method
.method public b()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lin/juspay/tracker/e;->l:Ljava/lang/String;
return-object v0
.end method
.method public b(Ljava/lang/String;)V
.registers 2
iput-object p1, p0, Lin/juspay/tracker/e;->n:Ljava/lang/String;
return-void
.end method
.method public c(Ljava/lang/String;)V
.registers 4
new-instance v0, Lin/juspay/tracker/b;
invoke-direct {v0}, Lin/juspay/tracker/b;-><init>()V
sget-object v1, Lin/juspay/tracker/b$b;->b:Lin/juspay/tracker/b$b;
invoke-virtual {v0, v1}, Lin/juspay/tracker/b;->a(Lin/juspay/tracker/b$b;)Lin/juspay/tracker/b;
move-result-object v0
sget-object v1, Lin/juspay/tracker/b$a;->h:Lin/juspay/tracker/b$a;
invoke-virtual {v0, v1}, Lin/juspay/tracker/b;->a(Lin/juspay/tracker/b$a;)Lin/juspay/tracker/b;
move-result-object v0
invoke-virtual {v0, p1}, Lin/juspay/tracker/b;->b(Ljava/lang/String;)Lin/juspay/tracker/b;
move-result-object v0
invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;
move-result-object v1
invoke-virtual {v1, v0}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/b;)V
return-void
.end method
.method public c()Z
.registers 7
const/4 v0, 0x0
iget-wide v2, p0, Lin/juspay/tracker/e;->r:J
sget-wide v4, Lin/juspay/tracker/e;->d:J
add-long/2addr v2, v4
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v4
cmp-long v1, v2, v4
if-lez v1, :cond_11
iget-boolean v0, p0, Lin/juspay/tracker/e;->q:Z
:goto_10
return v0
:cond_11
invoke-virtual {p0, v0}, Lin/juspay/tracker/e;->a(Z)V
goto :goto_10
.end method
.method public d()V
.registers 5
:try_start_0
invoke-static {}, Lin/juspay/tracker/i;->a()Lin/juspay/tracker/i;
move-result-object v0
invoke-virtual {v0}, Lin/juspay/tracker/i;->f()Lorg/json/JSONObject;
move-result-object v0
const-string v1, "interval_start"
sget-wide v2, Lin/juspay/tracker/e;->b:J
invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J
move-result-wide v2
sput-wide v2, Lin/juspay/tracker/e;->b:J
const-string v1, "interval_batch"
sget-wide v2, Lin/juspay/tracker/e;->c:J
invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J
move-result-wide v0
sput-wide v0, Lin/juspay/tracker/e;->c:J
new-instance v0, Lin/juspay/tracker/b;
invoke-direct {v0}, Lin/juspay/tracker/b;-><init>()V
sget-object v1, Lin/juspay/tracker/b$b;->d:Lin/juspay/tracker/b$b;
invoke-virtual {v0, v1}, Lin/juspay/tracker/b;->a(Lin/juspay/tracker/b$b;)Lin/juspay/tracker/b;
move-result-object v0
sget-object v1, Lin/juspay/tracker/b$a;->h:Lin/juspay/tracker/b$a;
invoke-virtual {v0, v1}, Lin/juspay/tracker/b;->a(Lin/juspay/tracker/b$a;)Lin/juspay/tracker/b;
move-result-object v0
const-string v1, "log_push_config"
invoke-virtual {v0, v1}, Lin/juspay/tracker/b;->b(Ljava/lang/String;)Lin/juspay/tracker/b;
move-result-object v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "START_INTERVAL = "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
sget-wide v2, Lin/juspay/tracker/e;->b:J
invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, " BATCH_INTERVAL = "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
sget-wide v2, Lin/juspay/tracker/e;->c:J
invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Lin/juspay/tracker/b;->c(Ljava/lang/String;)Lin/juspay/tracker/b;
move-result-object v0
invoke-static {}, Lin/juspay/tracker/e;->a()Lin/juspay/tracker/e;
move-result-object v1
invoke-virtual {v1, v0}, Lin/juspay/tracker/e;->a(Lin/juspay/tracker/b;)V
:try_end_5f
.catchall {:try_start_0 .. :try_end_5f} :catchall_6f
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_5f} :catch_63
invoke-direct {p0}, Lin/juspay/tracker/e;->k()V
:goto_62
return-void
:catch_63
move-exception v0
:try_start_64
sget-object v1, Lin/juspay/tracker/e;->a:Ljava/lang/String;
const-string v2, "Exception while setting timer interval"
invoke-static {v1, v2, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
:try_end_6b
.catchall {:try_start_64 .. :try_end_6b} :catchall_6f
invoke-direct {p0}, Lin/juspay/tracker/e;->k()V
goto :goto_62
:catchall_6f
move-exception v0
invoke-direct {p0}, Lin/juspay/tracker/e;->k()V
throw v0
.end method
.method public f()Ljava/util/Map;
.registers 5
new-instance v1, Ljava/util/HashMap;
invoke-direct {v1}, Ljava/util/HashMap;-><init>()V
:try_start_5
const-string v0, "at"
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v2
invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "brand"
sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;
invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "model"
sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;
invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "manufacturer"
sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "os"
const-string v2, "android"
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "os_version"
sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;
invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "locale"
invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;
move-result-object v2
invoke-virtual {v2}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "app_name"
invoke-virtual {p0}, Lin/juspay/tracker/e;->g()Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "client_id"
iget-object v2, p0, Lin/juspay/tracker/e;->v:Ljava/lang/String;
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "godel_version"
invoke-static {}, Lin/juspay/tracker/j;->a()Lin/juspay/tracker/j;
move-result-object v2
invoke-virtual {v2}, Lin/juspay/tracker/j;->b()Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "godel_build_version"
invoke-static {}, Lin/juspay/tracker/j;->a()Lin/juspay/tracker/j;
move-result-object v2
invoke-virtual {v2}, Lin/juspay/tracker/j;->d()Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "godel_remotes_version"
invoke-static {}, Lin/juspay/tracker/j;->a()Lin/juspay/tracker/j;
move-result-object v2
invoke-virtual {v2}, Lin/juspay/tracker/j;->c()Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "invocation_type"
iget-object v2, p0, Lin/juspay/tracker/e;->y:Ljava/lang/String;
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "ip_address"
invoke-static {}, Lin/juspay/tracker/e;->e()Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
iget-object v0, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
if-eqz v0, :cond_12f
const-string v0, "device_id"
invoke-static {}, Lin/juspay/tracker/j;->a()Lin/juspay/tracker/j;
move-result-object v2
iget-object v3, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
invoke-virtual {v2, v3}, Lin/juspay/tracker/j;->g(Landroid/content/Context;)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "screen_width"
invoke-static {}, Lin/juspay/tracker/j;->a()Lin/juspay/tracker/j;
move-result-object v2
iget-object v3, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
invoke-virtual {v2, v3}, Lin/juspay/tracker/j;->e(Landroid/content/Context;)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "screen_height"
invoke-static {}, Lin/juspay/tracker/j;->a()Lin/juspay/tracker/j;
move-result-object v2
iget-object v3, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
invoke-virtual {v2, v3}, Lin/juspay/tracker/j;->d(Landroid/content/Context;)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "screen_ppi"
invoke-static {}, Lin/juspay/tracker/j;->a()Lin/juspay/tracker/j;
move-result-object v2
iget-object v3, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
invoke-virtual {v2, v3}, Lin/juspay/tracker/j;->f(Landroid/content/Context;)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "network_info"
invoke-static {}, Lin/juspay/tracker/j;->a()Lin/juspay/tracker/j;
move-result-object v2
iget-object v3, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
invoke-virtual {v2, v3}, Lin/juspay/tracker/j;->a(Landroid/content/Context;)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "network_type"
invoke-static {}, Lin/juspay/tracker/j;->a()Lin/juspay/tracker/j;
move-result-object v2
iget-object v3, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
invoke-virtual {v2, v3}, Lin/juspay/tracker/j;->b(Landroid/content/Context;)I
move-result v2
invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "app_version"
invoke-static {}, Lin/juspay/tracker/j;->a()Lin/juspay/tracker/j;
move-result-object v2
iget-object v3, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
invoke-virtual {v2, v3}, Lin/juspay/tracker/j;->c(Landroid/content/Context;)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
iget-object v0, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;
move-result-object v0
iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I
and-int/lit8 v0, v0, 0x2
if-eqz v0, :cond_14a
const/4 v0, 0x1
:goto_113
const-string v2, "app_debuggable"
invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;
move-result-object v0
invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "dev_options_enabled"
invoke-static {}, Lin/juspay/tracker/j;->a()Lin/juspay/tracker/j;
move-result-object v2
iget-object v3, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
invoke-virtual {v2, v3}, Lin/juspay/tracker/j;->h(Landroid/content/Context;)Z
move-result v2
invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_12f
const-string v0, "is_rooted"
invoke-static {}, Lin/juspay/tracker/j;->a()Lin/juspay/tracker/j;
invoke-static {}, Lin/juspay/tracker/j;->e()Z
move-result v2
invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "log_level"
const/4 v2, 0x2
invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
move-result-object v2
invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:goto_149
:try_end_149
.catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_149} :catch_14c
return-object v1
:cond_14a
const/4 v0, 0x0
goto :goto_113
:catch_14c
move-exception v0
sget-object v2, Lin/juspay/tracker/e;->a:Ljava/lang/String;
const-string v3, "Exception while creatingSession Data Map"
invoke-static {v2, v3, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
goto :goto_149
.end method
.method public g()Ljava/lang/String;
.registers 3
iget-object v0, p0, Lin/juspay/tracker/e;->o:Ljava/lang/String;
if-nez v0, :cond_1a
iget-object v0, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;
move-result-object v0
iget-object v1, p0, Lin/juspay/tracker/e;->x:Landroid/content/Context;
invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
move-result-object v0
invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lin/juspay/tracker/e;->o:Ljava/lang/String;
:cond_1a
iget-object v0, p0, Lin/juspay/tracker/e;->o:Ljava/lang/String;
return-object v0
.end method