.class public Lin/juspay/tracker/i;
.super Ljava/lang/Object;
.source "Properties.java"
.field private static a:Lin/juspay/tracker/i;
.field private b:Lorg/json/JSONObject;
.field private c:Lorg/json/JSONObject;
.field private d:I
.field private e:Lorg/json/JSONArray;
.field private f:Lorg/json/JSONArray;
.field private g:Ljava/lang/String;
.method public constructor <init>()V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
new-instance v0, Lorg/json/JSONObject;
invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V
iput-object v0, p0, Lin/juspay/tracker/i;->b:Lorg/json/JSONObject;
new-instance v0, Lorg/json/JSONObject;
invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V
iput-object v0, p0, Lin/juspay/tracker/i;->c:Lorg/json/JSONObject;
const/4 v0, 0x0
iput v0, p0, Lin/juspay/tracker/i;->d:I
new-instance v0, Lorg/json/JSONArray;
invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V
iput-object v0, p0, Lin/juspay/tracker/i;->e:Lorg/json/JSONArray;
new-instance v0, Lorg/json/JSONArray;
invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V
iput-object v0, p0, Lin/juspay/tracker/i;->f:Lorg/json/JSONArray;
const-string v0, ""
iput-object v0, p0, Lin/juspay/tracker/i;->g:Ljava/lang/String;
sput-object p0, Lin/juspay/tracker/i;->a:Lin/juspay/tracker/i;
return-void
.end method
.method public static a()Lin/juspay/tracker/i;
.registers 1
sget-object v0, Lin/juspay/tracker/i;->a:Lin/juspay/tracker/i;
if-nez v0, :cond_b
new-instance v0, Lin/juspay/tracker/i;
invoke-direct {v0}, Lin/juspay/tracker/i;-><init>()V
sput-object v0, Lin/juspay/tracker/i;->a:Lin/juspay/tracker/i;
:cond_b
sget-object v0, Lin/juspay/tracker/i;->a:Lin/juspay/tracker/i;
return-object v0
.end method
.method public a(I)V
.registers 2
iput p1, p0, Lin/juspay/tracker/i;->d:I
return-void
.end method
.method public a(Ljava/lang/String;)V
.registers 2
iput-object p1, p0, Lin/juspay/tracker/i;->g:Ljava/lang/String;
return-void
.end method
.method public a(Lorg/json/JSONArray;)V
.registers 2
iput-object p1, p0, Lin/juspay/tracker/i;->f:Lorg/json/JSONArray;
return-void
.end method
.method public a(Lorg/json/JSONObject;)V
.registers 2
iput-object p1, p0, Lin/juspay/tracker/i;->c:Lorg/json/JSONObject;
return-void
.end method
.method public b()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lin/juspay/tracker/i;->g:Ljava/lang/String;
return-object v0
.end method
.method public b(Lorg/json/JSONArray;)V
.registers 2
iput-object p1, p0, Lin/juspay/tracker/i;->e:Lorg/json/JSONArray;
return-void
.end method
.method public b(Lorg/json/JSONObject;)V
.registers 2
iput-object p1, p0, Lin/juspay/tracker/i;->b:Lorg/json/JSONObject;
return-void
.end method
.method public c()Lorg/json/JSONArray;
.registers 2
iget-object v0, p0, Lin/juspay/tracker/i;->f:Lorg/json/JSONArray;
return-object v0
.end method
.method public d()Lorg/json/JSONArray;
.registers 2
iget-object v0, p0, Lin/juspay/tracker/i;->e:Lorg/json/JSONArray;
return-object v0
.end method
.method public e()I
.registers 2
iget v0, p0, Lin/juspay/tracker/i;->d:I
return v0
.end method
.method public f()Lorg/json/JSONObject;
.registers 2
iget-object v0, p0, Lin/juspay/tracker/i;->c:Lorg/json/JSONObject;
return-object v0
.end method
.method public g()Lorg/json/JSONObject;
.registers 2
iget-object v0, p0, Lin/juspay/tracker/i;->b:Lorg/json/JSONObject;
return-object v0
.end method