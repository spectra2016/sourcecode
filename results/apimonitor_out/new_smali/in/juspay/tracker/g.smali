.class public Lin/juspay/tracker/g;
.super Ljava/lang/Object;
.source "LoggingUtil.java"
.field private static b:Lin/juspay/tracker/g;
.field private final a:Ljava/lang/String;
.field private c:Lorg/json/JSONObject;
.method constructor <init>()V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const-class v0, Lin/juspay/tracker/g;
invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lin/juspay/tracker/g;->a:Ljava/lang/String;
const/4 v0, 0x0
iput-object v0, p0, Lin/juspay/tracker/g;->c:Lorg/json/JSONObject;
return-void
.end method
.method public static a()Lin/juspay/tracker/g;
.registers 1
sget-object v0, Lin/juspay/tracker/g;->b:Lin/juspay/tracker/g;
if-nez v0, :cond_b
new-instance v0, Lin/juspay/tracker/g;
invoke-direct {v0}, Lin/juspay/tracker/g;-><init>()V
sput-object v0, Lin/juspay/tracker/g;->b:Lin/juspay/tracker/g;
:cond_b
sget-object v0, Lin/juspay/tracker/g;->b:Lin/juspay/tracker/g;
return-object v0
.end method
.method private a(Ljava/lang/String;)Lorg/json/JSONObject;
.registers 7
const/4 v1, 0x0
:try_start_1
invoke-static {}, Lin/juspay/tracker/i;->a()Lin/juspay/tracker/i;
move-result-object v0
invoke-virtual {v0}, Lin/juspay/tracker/i;->g()Lorg/json/JSONObject;
move-result-object v2
if-nez v2, :cond_d
move-object v0, v1
:goto_c
return-object v0
:cond_d
invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;
move-result-object v3
:cond_11
invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_28
invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
move-result v4
if-eqz v4, :cond_11
invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
move-result-object v0
goto :goto_c
:cond_28
invoke-direct {p0, p1}, Lin/juspay/tracker/g;->b(Ljava/lang/String;)Lorg/json/JSONObject;
:try_end_2b
.catch Lorg/json/JSONException; {:try_start_1 .. :try_end_2b} :catch_2d
move-result-object v0
goto :goto_c
:catch_2d
move-exception v0
iget-object v2, p0, Lin/juspay/tracker/g;->a:Ljava/lang/String;
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "Error while getting rules for event "
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v3
invoke-static {v2, v3, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
move-object v0, v1
goto :goto_c
.end method
.method private a(D)Z
.registers 10
const/4 v0, 0x1
const/4 v1, 0x0
const-wide/high16 v2, 0x3ff0
cmpl-double v2, p1, v2
if-nez v2, :cond_9
:goto_8
:cond_8
return v0
:cond_9
const-wide/16 v2, 0x0
cmpl-double v2, p1, v2
if-nez v2, :cond_11
move v0, v1
goto :goto_8
:cond_11
new-instance v2, Ljava/security/SecureRandom;
invoke-static {}, Ldroidbox/java/security/SecureRandom;->droidbox_cons()V
invoke-direct {v2}, Ljava/security/SecureRandom;-><init>()V
const/16 v3, 0x64
invoke-virtual {v2, v3}, Ljava/security/SecureRandom;->nextInt(I)I
move-result v2
int-to-double v2, v2
const-wide/high16 v4, 0x4059
mul-double/2addr v4, p1
cmpg-double v2, v2, v4
if-ltz v2, :cond_8
move v0, v1
goto :goto_8
.end method
.method private b(Ljava/lang/String;)Lorg/json/JSONObject;
.registers 6
:try_start_0
const-string v0, "account_info"
invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
move-result v0
if-eqz v0, :cond_1b
new-instance v0, Lorg/json/JSONObject;
invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V
const-string v1, "logHashed"
const/4 v2, 0x1
invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
const-string v1, "value"
const-wide/16 v2, 0x0
invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;
:goto_1a
return-object v0
:cond_1b
new-instance v0, Lorg/json/JSONObject;
invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V
const-string v1, "logHashed"
const/4 v2, 0x0
invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
const-string v1, "value"
const/4 v2, 0x1
invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
:try_end_2c
.catch Lorg/json/JSONException; {:try_start_0 .. :try_end_2c} :catch_2d
goto :goto_1a
:catch_2d
move-exception v0
iget-object v1, p0, Lin/juspay/tracker/g;->a:Ljava/lang/String;
const-string v2, "Error while getting default rules "
invoke-static {v1, v2, v0}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
const/4 v0, 0x0
goto :goto_1a
.end method
.method public a(Lin/juspay/tracker/b;)Lin/juspay/tracker/b;
.registers 8
const/4 v0, 0x0
:try_start_1
invoke-virtual {p1}, Lin/juspay/tracker/b;->a()Ljava/lang/String;
move-result-object v1
if-eqz v1, :cond_3d
invoke-virtual {p1}, Lin/juspay/tracker/b;->a()Ljava/lang/String;
move-result-object v1
invoke-direct {p0, v1}, Lin/juspay/tracker/g;->a(Ljava/lang/String;)Lorg/json/JSONObject;
move-result-object v1
if-eqz v1, :cond_3d
const-string v2, "logHashed"
const/4 v3, 0x0
invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z
move-result v2
if-eqz v2, :cond_2f
invoke-virtual {p1}, Lin/juspay/tracker/b;->b()Ljava/lang/String;
move-result-object v2
if-eqz v2, :cond_2f
invoke-static {}, Lin/juspay/tracker/a;->a()Lin/juspay/tracker/a;
move-result-object v2
invoke-virtual {p1}, Lin/juspay/tracker/b;->b()Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Lin/juspay/tracker/a;->b(Ljava/lang/String;)Ljava/lang/String;
move-result-object v2
invoke-virtual {p1, v2}, Lin/juspay/tracker/b;->c(Ljava/lang/String;)Lin/juspay/tracker/b;
:cond_2f
const-string v2, "value"
const-wide/16 v4, 0x0
invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D
move-result-wide v2
invoke-direct {p0, v2, v3}, Lin/juspay/tracker/g;->a(D)Z
:try_end_3a
.catch Ljava/lang/Exception; {:try_start_1 .. :try_end_3a} :catch_40
move-result v1
if-eqz v1, :cond_3e
:goto_3d
:cond_3d
return-object p1
:cond_3e
move-object p1, v0
goto :goto_3d
:catch_40
move-exception v1
iget-object v2, p0, Lin/juspay/tracker/g;->a:Ljava/lang/String;
const-string v3, "Error in getLogEvent "
invoke-static {v2, v3, v1}, Lin/juspay/tracker/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
move-object p1, v0
goto :goto_3d
.end method