.class public final enum Lin/juspay/tracker/e$a;
.super Ljava/lang/Enum;
.source "GodelTracker.java"
.field public static final enum a:Lin/juspay/tracker/e$a;
.field public static final enum b:Lin/juspay/tracker/e$a;
.field public static final enum c:Lin/juspay/tracker/e$a;
.field private static final synthetic d:[Lin/juspay/tracker/e$a;
.method static constructor <clinit>()V
.registers 5
const/4 v4, 0x2
const/4 v3, 0x1
const/4 v2, 0x0
new-instance v0, Lin/juspay/tracker/e$a;
const-string v1, "ENCRYPT"
invoke-direct {v0, v1, v2}, Lin/juspay/tracker/e$a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/tracker/e$a;->a:Lin/juspay/tracker/e$a;
new-instance v0, Lin/juspay/tracker/e$a;
const-string v1, "OBFUSCATE"
invoke-direct {v0, v1, v3}, Lin/juspay/tracker/e$a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/tracker/e$a;->b:Lin/juspay/tracker/e$a;
new-instance v0, Lin/juspay/tracker/e$a;
const-string v1, "GZIP"
invoke-direct {v0, v1, v4}, Lin/juspay/tracker/e$a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/tracker/e$a;->c:Lin/juspay/tracker/e$a;
const/4 v0, 0x3
new-array v0, v0, [Lin/juspay/tracker/e$a;
sget-object v1, Lin/juspay/tracker/e$a;->a:Lin/juspay/tracker/e$a;
aput-object v1, v0, v2
sget-object v1, Lin/juspay/tracker/e$a;->b:Lin/juspay/tracker/e$a;
aput-object v1, v0, v3
sget-object v1, Lin/juspay/tracker/e$a;->c:Lin/juspay/tracker/e$a;
aput-object v1, v0, v4
sput-object v0, Lin/juspay/tracker/e$a;->d:[Lin/juspay/tracker/e$a;
return-void
.end method
.method private constructor <init>(Ljava/lang/String;I)V
.registers 3
invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V
return-void
.end method
.method public static valueOf(Ljava/lang/String;)Lin/juspay/tracker/e$a;
.registers 2
const-class v0, Lin/juspay/tracker/e$a;
invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
move-result-object v0
check-cast v0, Lin/juspay/tracker/e$a;
return-object v0
.end method
.method public static values()[Lin/juspay/tracker/e$a;
.registers 1
sget-object v0, Lin/juspay/tracker/e$a;->d:[Lin/juspay/tracker/e$a;
invoke-virtual {v0}, [Lin/juspay/tracker/e$a;->clone()Ljava/lang/Object;
move-result-object v0
check-cast v0, [Lin/juspay/tracker/e$a;
return-object v0
.end method