.class public Lin/juspay/widget/qrscanner/com/google/zxing/b/a;
.super Ljava/lang/Object;
.source "QRCodeReader.java"
.implements Lin/juspay/widget/qrscanner/com/google/zxing/i;
.field private static final a:[Lin/juspay/widget/qrscanner/com/google/zxing/l;
.field private final b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;
.method static constructor <clinit>()V
.registers 1
const/4 v0, 0x0
new-array v0, v0, [Lin/juspay/widget/qrscanner/com/google/zxing/l;
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a;->a:[Lin/juspay/widget/qrscanner/com/google/zxing/l;
return-void
.end method
.method public constructor <init>()V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;
invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;-><init>()V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;
return-void
.end method
.method private static a([ILin/juspay/widget/qrscanner/com/google/zxing/common/b;)F
.registers 12
const/4 v1, 0x1
const/4 v2, 0x0
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v7
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c()I
move-result v8
aget v3, p0, v2
aget v0, p0, v1
move v4, v1
move v5, v0
move v6, v3
move v0, v2
:goto_12
if-ge v6, v8, :cond_21
if-ge v5, v7, :cond_21
invoke-virtual {p1, v6, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v3
if-eq v4, v3, :cond_42
add-int/lit8 v3, v0, 0x1
const/4 v0, 0x5
if-ne v3, v0, :cond_2a
:cond_21
if-eq v6, v8, :cond_25
if-ne v5, v7, :cond_39
:cond_25
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_2a
if-nez v4, :cond_37
move v0, v1
:goto_2d
move v9, v3
move v3, v0
move v0, v9
:goto_30
add-int/lit8 v6, v6, 0x1
add-int/lit8 v4, v5, 0x1
move v5, v4
move v4, v3
goto :goto_12
:cond_37
move v0, v2
goto :goto_2d
:cond_39
aget v0, p0, v2
sub-int v0, v6, v0
int-to-float v0, v0
const/high16 v1, 0x40e0
div-float/2addr v0, v1
return v0
:cond_42
move v3, v4
goto :goto_30
.end method
.method private static a(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;)Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
.registers 12
const/4 v4, 0x1
const/4 v2, 0x0
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a()[I
move-result-object v0
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->b()[I
move-result-object v1
if-eqz v0, :cond_e
if-nez v1, :cond_13
:cond_e
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_13
invoke-static {v0, p0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a;->a([ILin/juspay/widget/qrscanner/com/google/zxing/common/b;)F
move-result v5
aget v3, v0, v4
aget v6, v1, v4
aget v4, v0, v2
aget v0, v1, v2
if-ge v4, v0, :cond_23
if-lt v3, v6, :cond_28
:cond_23
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_28
sub-int v1, v6, v3
sub-int v7, v0, v4
if-eq v1, v7, :cond_3c
sub-int v0, v6, v3
add-int/2addr v0, v4
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c()I
move-result v1
if-lt v0, v1, :cond_3c
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_3c
sub-int v1, v0, v4
add-int/lit8 v1, v1, 0x1
int-to-float v1, v1
div-float/2addr v1, v5
invoke-static {v1}, Ljava/lang/Math;->round(F)I
move-result v7
sub-int v1, v6, v3
add-int/lit8 v1, v1, 0x1
int-to-float v1, v1
div-float/2addr v1, v5
invoke-static {v1}, Ljava/lang/Math;->round(F)I
move-result v8
if-lez v7, :cond_54
if-gtz v8, :cond_59
:cond_54
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_59
if-eq v8, v7, :cond_60
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_60
const/high16 v1, 0x4000
div-float v1, v5, v1
float-to-int v9, v1
add-int v1, v3, v9
add-int v3, v4, v9
add-int/lit8 v4, v7, -0x1
int-to-float v4, v4
mul-float/2addr v4, v5
float-to-int v4, v4
add-int/2addr v4, v3
sub-int v0, v4, v0
if-lez v0, :cond_b6
if-le v0, v9, :cond_7a
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_7a
sub-int v0, v3, v0
move v4, v0
:goto_7d
add-int/lit8 v0, v8, -0x1
int-to-float v0, v0
mul-float/2addr v0, v5
float-to-int v0, v0
add-int/2addr v0, v1
sub-int/2addr v0, v6
if-lez v0, :cond_b4
if-le v0, v9, :cond_8d
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_8d
sub-int v0, v1, v0
:goto_8f
new-instance v6, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-direct {v6, v7, v8}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;-><init>(II)V
move v3, v2
:goto_95
if-ge v3, v8, :cond_b3
int-to-float v1, v3
mul-float/2addr v1, v5
float-to-int v1, v1
add-int v9, v0, v1
move v1, v2
:goto_9d
if-ge v1, v7, :cond_af
int-to-float v10, v1
mul-float/2addr v10, v5
float-to-int v10, v10
add-int/2addr v10, v4
invoke-virtual {p0, v10, v9}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v10
if-eqz v10, :cond_ac
invoke-virtual {v6, v1, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->b(II)V
:cond_ac
add-int/lit8 v1, v1, 0x1
goto :goto_9d
:cond_af
add-int/lit8 v1, v3, 0x1
move v3, v1
goto :goto_95
:cond_b3
return-object v6
:cond_b4
move v0, v1
goto :goto_8f
:cond_b6
move v4, v3
goto :goto_7d
.end method
.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/c;)Lin/juspay/widget/qrscanner/com/google/zxing/j;
.registers 3
const/4 v0, 0x0
invoke-virtual {p0, p1, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a;->a(Lin/juspay/widget/qrscanner/com/google/zxing/c;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/j;
move-result-object v0
return-object v0
.end method
.method public final a(Lin/juspay/widget/qrscanner/com/google/zxing/c;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/j;
.registers 9
if-eqz p2, :cond_73
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->b:Lin/juspay/widget/qrscanner/com/google/zxing/d;
invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_73
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/c;->a()Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
move-result-object v0
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;)Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
move-result-object v0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;
invoke-virtual {v1, v0, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/common/e;
move-result-object v1
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a;->a:[Lin/juspay/widget/qrscanner/com/google/zxing/l;
move-object v2, v1
move-object v1, v0
:goto_1c
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->e()Ljava/lang/Object;
move-result-object v0
instance-of v0, v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/i;
if-eqz v0, :cond_2d
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->e()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/i;
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/i;->a([Lin/juspay/widget/qrscanner/com/google/zxing/l;)V
:cond_2d
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/j;
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->b()Ljava/lang/String;
move-result-object v3
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->a()[B
move-result-object v4
sget-object v5, Lin/juspay/widget/qrscanner/com/google/zxing/a;->l:Lin/juspay/widget/qrscanner/com/google/zxing/a;
invoke-direct {v0, v3, v4, v1, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/j;-><init>(Ljava/lang/String;[B[Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/a;)V
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->c()Ljava/util/List;
move-result-object v1
if-eqz v1, :cond_47
sget-object v3, Lin/juspay/widget/qrscanner/com/google/zxing/k;->c:Lin/juspay/widget/qrscanner/com/google/zxing/k;
invoke-virtual {v0, v3, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/j;->a(Lin/juspay/widget/qrscanner/com/google/zxing/k;Ljava/lang/Object;)V
:cond_47
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->d()Ljava/lang/String;
move-result-object v1
if-eqz v1, :cond_52
sget-object v3, Lin/juspay/widget/qrscanner/com/google/zxing/k;->d:Lin/juspay/widget/qrscanner/com/google/zxing/k;
invoke-virtual {v0, v3, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/j;->a(Lin/juspay/widget/qrscanner/com/google/zxing/k;Ljava/lang/Object;)V
:cond_52
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->f()Z
move-result v1
if-eqz v1, :cond_72
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/k;->j:Lin/juspay/widget/qrscanner/com/google/zxing/k;
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->h()I
move-result v3
invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v3
invoke-virtual {v0, v1, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/j;->a(Lin/juspay/widget/qrscanner/com/google/zxing/k;Ljava/lang/Object;)V
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/k;->k:Lin/juspay/widget/qrscanner/com/google/zxing/k;
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/e;->g()I
move-result v2
invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v2
invoke-virtual {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/j;->a(Lin/juspay/widget/qrscanner/com/google/zxing/k;Ljava/lang/Object;)V
:cond_72
return-object v0
:cond_73
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/c;->a()Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
move-result-object v1
invoke-direct {v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;)V
invoke-virtual {v0, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a(Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/common/g;
move-result-object v0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/g;->a()Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
move-result-object v2
invoke-virtual {v1, v2, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/e;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/common/e;
move-result-object v1
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/g;->b()[Lin/juspay/widget/qrscanner/com/google/zxing/l;
move-result-object v0
move-object v2, v1
move-object v1, v0
goto :goto_1c
.end method
.method public a()V
.registers 1
return-void
.end method