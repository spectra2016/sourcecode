.class public final enum Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.super Ljava/lang/Enum;
.source "Mode.java"
.field public static final enum a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.field public static final enum b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.field public static final enum c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.field public static final enum d:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.field public static final enum e:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.field public static final enum f:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.field public static final enum g:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.field public static final enum h:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.field public static final enum i:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.field public static final enum j:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.field private static final synthetic m:[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.field private final k:[I
.field private final l:I
.method static constructor <clinit>()V
.registers 10
const/4 v9, 0x4
const/4 v8, 0x2
const/4 v7, 0x1
const/4 v6, 0x0
const/4 v5, 0x3
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
const-string v1, "TERMINATOR"
new-array v2, v5, [I
fill-array-data v2, :array_d6
invoke-direct {v0, v1, v6, v2, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;-><init>(Ljava/lang/String;I[II)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
const-string v1, "NUMERIC"
new-array v2, v5, [I
fill-array-data v2, :array_e0
invoke-direct {v0, v1, v7, v2, v7}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;-><init>(Ljava/lang/String;I[II)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
const-string v1, "ALPHANUMERIC"
new-array v2, v5, [I
fill-array-data v2, :array_ea
invoke-direct {v0, v1, v8, v2, v8}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;-><init>(Ljava/lang/String;I[II)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
const-string v1, "STRUCTURED_APPEND"
new-array v2, v5, [I
fill-array-data v2, :array_f4
invoke-direct {v0, v1, v5, v2, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;-><init>(Ljava/lang/String;I[II)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->d:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
const-string v1, "BYTE"
new-array v2, v5, [I
fill-array-data v2, :array_fe
invoke-direct {v0, v1, v9, v2, v9}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;-><init>(Ljava/lang/String;I[II)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->e:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
const-string v1, "ECI"
const/4 v2, 0x5
new-array v3, v5, [I
fill-array-data v3, :array_108
const/4 v4, 0x7
invoke-direct {v0, v1, v2, v3, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;-><init>(Ljava/lang/String;I[II)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->f:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
const-string v1, "KANJI"
const/4 v2, 0x6
new-array v3, v5, [I
fill-array-data v3, :array_112
const/16 v4, 0x8
invoke-direct {v0, v1, v2, v3, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;-><init>(Ljava/lang/String;I[II)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->g:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
const-string v1, "FNC1_FIRST_POSITION"
const/4 v2, 0x7
new-array v3, v5, [I
fill-array-data v3, :array_11c
const/4 v4, 0x5
invoke-direct {v0, v1, v2, v3, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;-><init>(Ljava/lang/String;I[II)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->h:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
const-string v1, "FNC1_SECOND_POSITION"
const/16 v2, 0x8
new-array v3, v5, [I
fill-array-data v3, :array_126
const/16 v4, 0x9
invoke-direct {v0, v1, v2, v3, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;-><init>(Ljava/lang/String;I[II)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->i:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
const-string v1, "HANZI"
const/16 v2, 0x9
new-array v3, v5, [I
fill-array-data v3, :array_130
const/16 v4, 0xd
invoke-direct {v0, v1, v2, v3, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;-><init>(Ljava/lang/String;I[II)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->j:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
const/16 v0, 0xa
new-array v0, v0, [Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
aput-object v1, v0, v6
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
aput-object v1, v0, v7
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
aput-object v1, v0, v8
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->d:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
aput-object v1, v0, v5
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->e:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
aput-object v1, v0, v9
const/4 v1, 0x5
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->f:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
aput-object v2, v0, v1
const/4 v1, 0x6
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->g:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
aput-object v2, v0, v1
const/4 v1, 0x7
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->h:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
aput-object v2, v0, v1
const/16 v1, 0x8
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->i:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
aput-object v2, v0, v1
const/16 v1, 0x9
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->j:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
aput-object v2, v0, v1
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->m:[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
return-void
:array_ea
.array-data 0x4
0x9t 0x0t 0x0t 0x0t
0xbt 0x0t 0x0t 0x0t
0xdt 0x0t 0x0t 0x0t
.end array-data
:array_112
.array-data 0x4
0x8t 0x0t 0x0t 0x0t
0xat 0x0t 0x0t 0x0t
0xct 0x0t 0x0t 0x0t
.end array-data
:array_fe
.array-data 0x4
0x8t 0x0t 0x0t 0x0t
0x10t 0x0t 0x0t 0x0t
0x10t 0x0t 0x0t 0x0t
.end array-data
:array_108
.array-data 0x4
0x0t 0x0t 0x0t 0x0t
0x0t 0x0t 0x0t 0x0t
0x0t 0x0t 0x0t 0x0t
.end array-data
:array_e0
.array-data 0x4
0xat 0x0t 0x0t 0x0t
0xct 0x0t 0x0t 0x0t
0xet 0x0t 0x0t 0x0t
.end array-data
:array_11c
.array-data 0x4
0x0t 0x0t 0x0t 0x0t
0x0t 0x0t 0x0t 0x0t
0x0t 0x0t 0x0t 0x0t
.end array-data
:array_d6
.array-data 0x4
0x0t 0x0t 0x0t 0x0t
0x0t 0x0t 0x0t 0x0t
0x0t 0x0t 0x0t 0x0t
.end array-data
:array_126
.array-data 0x4
0x0t 0x0t 0x0t 0x0t
0x0t 0x0t 0x0t 0x0t
0x0t 0x0t 0x0t 0x0t
.end array-data
:array_130
.array-data 0x4
0x8t 0x0t 0x0t 0x0t
0xat 0x0t 0x0t 0x0t
0xct 0x0t 0x0t 0x0t
.end array-data
:array_f4
.array-data 0x4
0x0t 0x0t 0x0t 0x0t
0x0t 0x0t 0x0t 0x0t
0x0t 0x0t 0x0t 0x0t
.end array-data
.end method
.method private constructor <init>(Ljava/lang/String;I[II)V
.registers 5
invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V
iput-object p3, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->k:[I
iput p4, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->l:I
return-void
.end method
.method public static a(I)Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.registers 2
packed-switch p0, :pswitch_data_28
:pswitch_3
new-instance v0, Ljava/lang/IllegalArgumentException;
invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V
throw v0
:pswitch_9
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
:goto_b
return-object v0
:pswitch_c
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
goto :goto_b
:pswitch_f
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
goto :goto_b
:pswitch_12
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->d:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
goto :goto_b
:pswitch_15
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->e:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
goto :goto_b
:pswitch_18
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->h:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
goto :goto_b
:pswitch_1b
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->f:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
goto :goto_b
:pswitch_1e
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->g:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
goto :goto_b
:pswitch_21
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->i:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
goto :goto_b
:pswitch_24
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->j:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
goto :goto_b
nop
:pswitch_data_28
.packed-switch 0x0
:pswitch_9
:pswitch_c
:pswitch_f
:pswitch_12
:pswitch_15
:pswitch_18
:pswitch_3
:pswitch_1b
:pswitch_1e
:pswitch_21
:pswitch_3
:pswitch_3
:pswitch_3
:pswitch_24
.end packed-switch
.end method
.method public static valueOf(Ljava/lang/String;)Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.registers 2
const-class v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
return-object v0
.end method
.method public static values()[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
.registers 1
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->m:[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
invoke-virtual {v0}, [Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->clone()Ljava/lang/Object;
move-result-object v0
check-cast v0, [Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;
return-object v0
.end method
.method public a()I
.registers 2
iget v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->l:I
return v0
.end method
.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;)I
.registers 4
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->a()I
move-result v0
const/16 v1, 0x9
if-gt v0, v1, :cond_e
const/4 v0, 0x0
:goto_9
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/h;->k:[I
aget v0, v1, v0
return v0
:cond_e
const/16 v1, 0x1a
if-gt v0, v1, :cond_14
const/4 v0, 0x1
goto :goto_9
:cond_14
const/4 v0, 0x2
goto :goto_9
.end method