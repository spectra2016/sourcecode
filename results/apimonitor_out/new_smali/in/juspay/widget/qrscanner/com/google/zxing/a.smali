.class public final enum Lin/juspay/widget/qrscanner/com/google/zxing/a;
.super Ljava/lang/Enum;
.source "BarcodeFormat.java"
.field public static final enum a:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum b:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum c:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum d:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum e:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum f:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum g:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum h:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum i:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum j:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum k:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum l:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum m:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum n:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum o:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum p:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field public static final enum q:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field private static final synthetic r:[Lin/juspay/widget/qrscanner/com/google/zxing/a;
.method static constructor <clinit>()V
.registers 8
const/4 v7, 0x4
const/4 v6, 0x3
const/4 v5, 0x2
const/4 v4, 0x1
const/4 v3, 0x0
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "AZTEC"
invoke-direct {v0, v1, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "CODABAR"
invoke-direct {v0, v1, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->b:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "CODE_39"
invoke-direct {v0, v1, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->c:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "CODE_93"
invoke-direct {v0, v1, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->d:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "CODE_128"
invoke-direct {v0, v1, v7}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->e:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "DATA_MATRIX"
const/4 v2, 0x5
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->f:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "EAN_8"
const/4 v2, 0x6
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->g:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "EAN_13"
const/4 v2, 0x7
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->h:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "ITF"
const/16 v2, 0x8
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->i:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "MAXICODE"
const/16 v2, 0x9
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->j:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "PDF_417"
const/16 v2, 0xa
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->k:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "QR_CODE"
const/16 v2, 0xb
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->l:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "RSS_14"
const/16 v2, 0xc
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->m:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "RSS_EXPANDED"
const/16 v2, 0xd
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->n:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "UPC_A"
const/16 v2, 0xe
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->o:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "UPC_E"
const/16 v2, 0xf
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->p:Lin/juspay/widget/qrscanner/com/google/zxing/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
const-string v1, "UPC_EAN_EXTENSION"
const/16 v2, 0x10
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->q:Lin/juspay/widget/qrscanner/com/google/zxing/a;
const/16 v0, 0x11
new-array v0, v0, [Lin/juspay/widget/qrscanner/com/google/zxing/a;
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v1, v0, v3
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/a;->b:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v1, v0, v4
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/a;->c:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v1, v0, v5
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/a;->d:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v1, v0, v6
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/a;->e:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v1, v0, v7
const/4 v1, 0x5
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->f:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v2, v0, v1
const/4 v1, 0x6
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->g:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v2, v0, v1
const/4 v1, 0x7
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->h:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v2, v0, v1
const/16 v1, 0x8
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->i:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v2, v0, v1
const/16 v1, 0x9
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->j:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v2, v0, v1
const/16 v1, 0xa
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->k:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v2, v0, v1
const/16 v1, 0xb
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->l:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v2, v0, v1
const/16 v1, 0xc
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->m:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v2, v0, v1
const/16 v1, 0xd
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->n:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v2, v0, v1
const/16 v1, 0xe
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->o:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v2, v0, v1
const/16 v1, 0xf
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->p:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v2, v0, v1
const/16 v1, 0x10
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/a;->q:Lin/juspay/widget/qrscanner/com/google/zxing/a;
aput-object v2, v0, v1
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->r:[Lin/juspay/widget/qrscanner/com/google/zxing/a;
return-void
.end method
.method private constructor <init>(Ljava/lang/String;I)V
.registers 3
invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V
return-void
.end method
.method public static valueOf(Ljava/lang/String;)Lin/juspay/widget/qrscanner/com/google/zxing/a;
.registers 2
const-class v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;
return-object v0
.end method
.method public static values()[Lin/juspay/widget/qrscanner/com/google/zxing/a;
.registers 1
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a;->r:[Lin/juspay/widget/qrscanner/com/google/zxing/a;
invoke-virtual {v0}, [Lin/juspay/widget/qrscanner/com/google/zxing/a;->clone()Ljava/lang/Object;
move-result-object v0
check-cast v0, [Lin/juspay/widget/qrscanner/com/google/zxing/a;
return-object v0
.end method