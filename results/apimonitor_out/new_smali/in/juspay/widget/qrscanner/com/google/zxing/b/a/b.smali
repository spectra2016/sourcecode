.class final Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;
.super Ljava/lang/Object;
.source "DataBlock.java"
.field private final a:I
.field private final b:[B
.method private constructor <init>(I[B)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->a:I
iput-object p2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->b:[B
return-void
.end method
.method static a([BLin/juspay/widget/qrscanner/com/google/zxing/b/a/j;Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;)[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;
.registers 16
const/4 v1, 0x0
array-length v0, p0
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->c()I
move-result v2
if-eq v0, v2, :cond_e
new-instance v0, Ljava/lang/IllegalArgumentException;
invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V
throw v0
:cond_e
invoke-virtual {p1, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->a(Lin/juspay/widget/qrscanner/com/google/zxing/b/a/f;)Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j$b;
move-result-object v5
invoke-virtual {v5}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j$b;->d()[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j$a;
move-result-object v6
array-length v3, v6
move v0, v1
move v2, v1
:goto_19
if-ge v0, v3, :cond_25
aget-object v4, v6, v0
invoke-virtual {v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j$a;->a()I
move-result v4
add-int/2addr v2, v4
add-int/lit8 v0, v0, 0x1
goto :goto_19
:cond_25
new-array v8, v2, [Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;
array-length v7, v6
move v4, v1
move v0, v1
:goto_2a
if-ge v4, v7, :cond_54
aget-object v9, v6, v4
move v2, v0
move v0, v1
:goto_30
invoke-virtual {v9}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j$a;->a()I
move-result v3
if-ge v0, v3, :cond_4f
invoke-virtual {v9}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j$a;->b()I
move-result v10
invoke-virtual {v5}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j$b;->a()I
move-result v3
add-int v11, v3, v10
add-int/lit8 v3, v2, 0x1
new-instance v12, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;
new-array v11, v11, [B
invoke-direct {v12, v10, v11}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;-><init>(I[B)V
aput-object v12, v8, v2
add-int/lit8 v0, v0, 0x1
move v2, v3
goto :goto_30
:cond_4f
add-int/lit8 v0, v4, 0x1
move v4, v0
move v0, v2
goto :goto_2a
:cond_54
aget-object v2, v8, v1
iget-object v2, v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->b:[B
array-length v3, v2
array-length v2, v8
add-int/lit8 v2, v2, -0x1
:goto_5c
if-ltz v2, :cond_65
aget-object v4, v8, v2
iget-object v4, v4, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->b:[B
array-length v4, v4
if-ne v4, v3, :cond_82
:cond_65
add-int/lit8 v7, v2, 0x1
invoke-virtual {v5}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j$b;->a()I
move-result v2
sub-int/2addr v3, v2
move v6, v1
move v2, v1
:goto_6e
if-ge v6, v3, :cond_8a
move v4, v2
move v2, v1
:goto_72
if-ge v2, v0, :cond_85
aget-object v5, v8, v2
iget-object v9, v5, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->b:[B
add-int/lit8 v5, v4, 0x1
aget-byte v4, p0, v4
aput-byte v4, v9, v6
add-int/lit8 v2, v2, 0x1
move v4, v5
goto :goto_72
:cond_82
add-int/lit8 v2, v2, -0x1
goto :goto_5c
:cond_85
add-int/lit8 v2, v6, 0x1
move v6, v2
move v2, v4
goto :goto_6e
:cond_8a
move v4, v7
:goto_8b
if-ge v4, v0, :cond_9c
aget-object v5, v8, v4
iget-object v6, v5, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->b:[B
add-int/lit8 v5, v2, 0x1
aget-byte v2, p0, v2
aput-byte v2, v6, v3
add-int/lit8 v2, v4, 0x1
move v4, v2
move v2, v5
goto :goto_8b
:cond_9c
aget-object v4, v8, v1
iget-object v4, v4, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->b:[B
array-length v9, v4
:goto_a1
if-ge v3, v9, :cond_c0
move v4, v1
move v5, v2
:goto_a5
if-ge v4, v0, :cond_bc
if-ge v4, v7, :cond_b9
move v2, v3
:goto_aa
aget-object v6, v8, v4
iget-object v10, v6, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->b:[B
add-int/lit8 v6, v5, 0x1
aget-byte v5, p0, v5
aput-byte v5, v10, v2
add-int/lit8 v2, v4, 0x1
move v4, v2
move v5, v6
goto :goto_a5
:cond_b9
add-int/lit8 v2, v3, 0x1
goto :goto_aa
:cond_bc
add-int/lit8 v3, v3, 0x1
move v2, v5
goto :goto_a1
:cond_c0
return-object v8
.end method
.method  a()I
.registers 2
iget v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->a:I
return v0
.end method
.method  b()[B
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/b;->b:[B
return-object v0
.end method