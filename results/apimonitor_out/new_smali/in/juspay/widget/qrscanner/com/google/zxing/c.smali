.class public final Lin/juspay/widget/qrscanner/com/google/zxing/c;
.super Ljava/lang/Object;
.source "BinaryBitmap.java"
.field private final a:Lin/juspay/widget/qrscanner/com/google/zxing/b;
.field private b:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
.method public constructor <init>(Lin/juspay/widget/qrscanner/com/google/zxing/b;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
if-nez p1, :cond_d
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "Binarizer must be non-null."
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_d
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b;
return-void
.end method
.method public a()Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/c;->b:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
if-nez v0, :cond_c
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/b;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b;->b()Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
move-result-object v0
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/c;->b:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
:cond_c
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/c;->b:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
return-object v0
.end method
.method public toString()Ljava/lang/String;
.registers 2
:try_start_0
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/c;->a()Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
move-result-object v0
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->toString()Ljava/lang/String;
:try_end_7
.catch Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException; {:try_start_0 .. :try_end_7} :catch_9
move-result-object v0
:goto_8
return-object v0
:catch_9
move-exception v0
const-string v0, ""
goto :goto_8
.end method