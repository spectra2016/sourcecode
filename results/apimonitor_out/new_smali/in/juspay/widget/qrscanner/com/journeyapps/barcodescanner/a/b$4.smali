.class  Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$4;
.super Ljava/lang/Object;
.source "CameraInstance.java"
.implements Ljava/lang/Runnable;
.field final synthetic a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
.method constructor <init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)V
.registers 2
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$4;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public run()V
.registers 4
:try_start_0
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g()Ljava/lang/String;
move-result-object v0
const-string v1, "Configuring camera"
invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$4;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
move-result-object v0
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b()V
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$4;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Landroid/os/Handler;
move-result-object v0
if-eqz v0, :cond_2f
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$4;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Landroid/os/Handler;
move-result-object v0
sget v1, Lin/juspay/widget/qrscanner/a$b;->zxing_prewiew_size_ready:I
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$4;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
invoke-static {v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->c(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
move-result-object v2
invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
move-result-object v0
invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
:try_end_2f
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_2f} :catch_30
:goto_2f
:cond_2f
return-void
:catch_30
move-exception v0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$4;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
invoke-static {v1, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;Ljava/lang/Exception;)V
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g()Ljava/lang/String;
move-result-object v1
const-string v2, "Failed to configure camera"
invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
goto :goto_2f
.end method