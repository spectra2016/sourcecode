.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/i;
.super Ljava/lang/Object;
.source "DefaultDecoderFactory.java"
.implements Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/f;
.field private a:Ljava/util/Collection;
.field private b:Ljava/util/Map;
.field private c:Ljava/lang/String;
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public a(Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;
.registers 5
new-instance v0, Ljava/util/EnumMap;
const-class v1, Lin/juspay/widget/qrscanner/com/google/zxing/d;
invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V
invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/i;->b:Ljava/util/Map;
if-eqz v1, :cond_13
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/i;->b:Ljava/util/Map;
invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V
:cond_13
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/i;->a:Ljava/util/Collection;
if-eqz v1, :cond_1e
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/d;->c:Lin/juspay/widget/qrscanner/com/google/zxing/d;
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/i;->a:Ljava/util/Collection;
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_1e
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/i;->c:Ljava/lang/String;
if-eqz v1, :cond_29
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/d;->e:Lin/juspay/widget/qrscanner/com/google/zxing/d;
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/i;->c:Ljava/lang/String;
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_29
new-instance v1, Lin/juspay/widget/qrscanner/com/google/zxing/g;
invoke-direct {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/g;-><init>()V
invoke-virtual {v1, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/g;->a(Ljava/util/Map;)V
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;
invoke-direct {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/i;)V
return-object v0
.end method