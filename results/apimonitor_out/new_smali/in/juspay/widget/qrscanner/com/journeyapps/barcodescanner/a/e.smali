.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;
.super Ljava/lang/Object;
.source "CameraSurface.java"
.field private a:Landroid/view/SurfaceHolder;
.field private b:Landroid/graphics/SurfaceTexture;
.method public constructor <init>(Landroid/graphics/SurfaceTexture;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
if-nez p1, :cond_d
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "surfaceTexture may not be null"
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_d
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;->b:Landroid/graphics/SurfaceTexture;
return-void
.end method
.method public constructor <init>(Landroid/view/SurfaceHolder;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
if-nez p1, :cond_d
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "surfaceHolder may not be null"
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_d
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;->a:Landroid/view/SurfaceHolder;
return-void
.end method
.method public a(Landroid/hardware/Camera;)V
.registers 4
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;->a:Landroid/view/SurfaceHolder;
if-eqz v0, :cond_a
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;->a:Landroid/view/SurfaceHolder;
invoke-virtual {p1, v0}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
:goto_9
return-void
:cond_a
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0xb
if-lt v0, v1, :cond_16
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;->b:Landroid/graphics/SurfaceTexture;
invoke-virtual {p1, v0}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
goto :goto_9
:cond_16
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "SurfaceTexture not supported."
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
.end method