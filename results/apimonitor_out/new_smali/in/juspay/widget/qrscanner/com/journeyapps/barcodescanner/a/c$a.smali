.class final Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;
.super Ljava/lang/Object;
.source "CameraManager.java"
.implements Landroid/hardware/Camera$PreviewCallback;
.field final synthetic a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
.field private b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;
.field private c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
.method public constructor <init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;)V
.registers 2
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;)V
.registers 2
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;
return-void
.end method
.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)V
.registers 2
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
return-void
.end method
.method public onPreviewFrame([BLandroid/hardware/Camera;)V
.registers 10
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
iget-object v6, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;
if-eqz v1, :cond_32
if-eqz v6, :cond_32
invoke-virtual {p2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;
move-result-object v0
invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewFormat()I
move-result v4
:try_start_10
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;
iget v2, v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I
iget v3, v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->g()I
move-result v5
move-object v1, p1
invoke-direct/range {v0 .. v5}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;-><init>([BIIII)V
invoke-interface {v6, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;)V
:goto_23
:cond_23
:try_end_23
.catch Ljava/lang/IllegalArgumentException; {:try_start_10 .. :try_end_23} :catch_24
return-void
:catch_24
move-exception v0
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->j()Ljava/lang/String;
move-result-object v1
const-string v2, "Camera preview failed"
invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
invoke-interface {v6, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;->a(Ljava/lang/Exception;)V
goto :goto_23
:cond_32
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->j()Ljava/lang/String;
move-result-object v0
const-string v1, "Got preview callback, but no handler or resolution available"
invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
if-eqz v6, :cond_23
new-instance v0, Ljava/lang/Exception;
const-string v1, "No resolution available"
invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V
invoke-interface {v6, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;->a(Ljava/lang/Exception;)V
goto :goto_23
.end method