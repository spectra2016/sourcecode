.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;
.super Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;
.source "BarcodeView.java"
.field private a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
.field private b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;
.field private c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;
.field private d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/f;
.field private e:Landroid/os/Handler;
.field private final f:Landroid/os/Handler$Callback;
.method public constructor <init>(Landroid/content/Context;)V
.registers 4
const/4 v1, 0x0
invoke-direct {p0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;-><init>(Landroid/content/Context;)V
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;
invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->f:Landroid/os/Handler$Callback;
invoke-direct {p0, p1, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 4
invoke-direct {p0, p1, p2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
const/4 v0, 0x0
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;
invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->f:Landroid/os/Handler$Callback;
invoke-direct {p0, p1, p2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.registers 5
invoke-direct {p0, p1, p2, p3}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
const/4 v0, 0x0
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;
invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->f:Landroid/os/Handler$Callback;
invoke-direct {p0, p1, p2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V
return-void
.end method
.method static synthetic a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;
return-object v0
.end method
.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
.registers 5
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/i;
invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/i;-><init>()V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/f;
new-instance v0, Landroid/os/Handler;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->f:Landroid/os/Handler$Callback;
invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->e:Landroid/os/Handler;
return-void
.end method
.method static synthetic b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
return-object v0
.end method
.method private j()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;
.registers 4
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/f;
if-nez v0, :cond_a
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->b()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/f;
move-result-object v0
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/f;
:cond_a
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/g;
invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/g;-><init>()V
new-instance v1, Ljava/util/HashMap;
invoke-direct {v1}, Ljava/util/HashMap;-><init>()V
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/d;->j:Lin/juspay/widget/qrscanner/com/google/zxing/d;
invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/f;
invoke-interface {v2, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/f;->a(Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;
move-result-object v1
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/g;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;)V
return-object v1
.end method
.method private k()V
.registers 5
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->l()V
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
if-eq v0, v1, :cond_2e
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->h()Z
move-result v0
if-eqz v0, :cond_2e
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->getCameraInstance()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
move-result-object v1
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->j()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;
move-result-object v2
iget-object v3, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->e:Landroid/os/Handler;
invoke-direct {v0, v1, v2, v3}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;Landroid/os/Handler;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->getPreviewFramingRect()Landroid/graphics/Rect;
move-result-object v1
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->a(Landroid/graphics/Rect;)V
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->a()V
:cond_2e
return-void
.end method
.method private l()V
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;
if-eqz v0, :cond_c
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->b()V
const/4 v0, 0x0
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;
:cond_c
return-void
.end method
.method public a()V
.registers 2
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
const/4 v0, 0x0
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->l()V
return-void
.end method
.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;)V
.registers 3
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$a;
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->k()V
return-void
.end method
.method protected b()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/f;
.registers 2
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/i;
invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/i;-><init>()V
return-object v0
.end method
.method protected c()V
.registers 1
invoke-super {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->c()V
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->k()V
return-void
.end method
.method public d()V
.registers 1
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->l()V
invoke-super {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c;->d()V
return-void
.end method
.method public getDecoderFactory()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/f;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/f;
return-object v0
.end method
.method public setDecoderFactory(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/f;)V
.registers 4
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/n;->a()V
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/f;
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;
if-eqz v0, :cond_12
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->j()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;
move-result-object v1
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;)V
:cond_12
return-void
.end method