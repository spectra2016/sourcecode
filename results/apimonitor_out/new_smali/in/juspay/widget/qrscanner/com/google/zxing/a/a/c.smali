.class public final Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;
.super Ljava/lang/Object;
.source "InactivityTimer.java"
.field private static final a:Ljava/lang/String;
.field private final b:Landroid/content/Context;
.field private final c:Landroid/content/BroadcastReceiver;
.field private d:Z
.field private e:Landroid/os/Handler;
.field private f:Ljava/lang/Runnable;
.field private g:Z
.method static constructor <clinit>()V
.registers 1
const-class v0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;
invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v0
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->a:Ljava/lang/String;
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Runnable;)V
.registers 5
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, 0x0
iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->d:Z
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->b:Landroid/content/Context;
iput-object p2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->f:Ljava/lang/Runnable;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c$a;
const/4 v1, 0x0
invoke-direct {v0, p0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c$a;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c$1;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->c:Landroid/content/BroadcastReceiver;
new-instance v0, Landroid/os/Handler;
invoke-direct {v0}, Landroid/os/Handler;-><init>()V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->e:Landroid/os/Handler;
return-void
.end method
.method static synthetic a(Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;)Landroid/os/Handler;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->e:Landroid/os/Handler;
return-object v0
.end method
.method static synthetic a(Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;Z)V
.registers 2
invoke-direct {p0, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->a(Z)V
return-void
.end method
.method private a(Z)V
.registers 3
iput-boolean p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->g:Z
iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->d:Z
if-eqz v0, :cond_9
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->a()V
:cond_9
return-void
.end method
.method private d()V
.registers 3
iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->d:Z
if-eqz v0, :cond_e
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->b:Landroid/content/Context;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->c:Landroid/content/BroadcastReceiver;
invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
const/4 v0, 0x0
iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->d:Z
:cond_e
return-void
.end method
.method private e()V
.registers 5
iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->d:Z
if-nez v0, :cond_15
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->b:Landroid/content/Context;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->c:Landroid/content/BroadcastReceiver;
new-instance v2, Landroid/content/IntentFilter;
const-string v3, "android.intent.action.BATTERY_CHANGED"
invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V
invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
const/4 v0, 0x1
iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->d:Z
:cond_15
return-void
.end method
.method private f()V
.registers 3
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->e:Landroid/os/Handler;
const/4 v1, 0x0
invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
return-void
.end method
.method public a()V
.registers 5
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->f()V
iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->g:Z
if-eqz v0, :cond_11
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->e:Landroid/os/Handler;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->f:Ljava/lang/Runnable;
const-wide/32 v2, 0x493e0
invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
:cond_11
return-void
.end method
.method public b()V
.registers 1
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->e()V
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->a()V
return-void
.end method
.method public c()V
.registers 1
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->f()V
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->d()V
return-void
.end method