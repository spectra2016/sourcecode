.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;
.super Ljava/lang/Object;
.source "Decoder.java"
.implements Lin/juspay/widget/qrscanner/com/google/zxing/m;
.field private a:Lin/juspay/widget/qrscanner/com/google/zxing/i;
.field private b:Ljava/util/List;
.method public constructor <init>(Lin/juspay/widget/qrscanner/com/google/zxing/i;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->b:Ljava/util/List;
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/i;
return-void
.end method
.method protected a(Lin/juspay/widget/qrscanner/com/google/zxing/c;)Lin/juspay/widget/qrscanner/com/google/zxing/j;
.registers 4
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->clear()V
:try_start_5
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/i;
instance-of v0, v0, Lin/juspay/widget/qrscanner/com/google/zxing/g;
if-eqz v0, :cond_19
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/i;
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/g;
invoke-virtual {v0, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/g;->b(Lin/juspay/widget/qrscanner/com/google/zxing/c;)Lin/juspay/widget/qrscanner/com/google/zxing/j;
:try_end_12
.catchall {:try_start_5 .. :try_end_12} :catchall_2d
.catch Ljava/lang/Exception; {:try_start_5 .. :try_end_12} :catch_25
move-result-object v0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/i;
invoke-interface {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/i;->a()V
:goto_18
return-object v0
:cond_19
:try_start_19
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/i;
invoke-interface {v0, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/i;->a(Lin/juspay/widget/qrscanner/com/google/zxing/c;)Lin/juspay/widget/qrscanner/com/google/zxing/j;
:try_end_1e
.catchall {:try_start_19 .. :try_end_1e} :catchall_2d
.catch Ljava/lang/Exception; {:try_start_19 .. :try_end_1e} :catch_25
move-result-object v0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/i;
invoke-interface {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/i;->a()V
goto :goto_18
:catch_25
move-exception v0
const/4 v0, 0x0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/i;
invoke-interface {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/i;->a()V
goto :goto_18
:catchall_2d
move-exception v0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/i;
invoke-interface {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/i;->a()V
throw v0
.end method
.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/f;)Lin/juspay/widget/qrscanner/com/google/zxing/j;
.registers 3
invoke-virtual {p0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->b(Lin/juspay/widget/qrscanner/com/google/zxing/f;)Lin/juspay/widget/qrscanner/com/google/zxing/c;
move-result-object v0
invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->a(Lin/juspay/widget/qrscanner/com/google/zxing/c;)Lin/juspay/widget/qrscanner/com/google/zxing/j;
move-result-object v0
return-object v0
.end method
.method public a()Ljava/util/List;
.registers 3
new-instance v0, Ljava/util/ArrayList;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->b:Ljava/util/List;
invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
return-object v0
.end method
.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/l;)V
.registers 3
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/e;->b:Ljava/util/List;
invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
return-void
.end method
.method protected b(Lin/juspay/widget/qrscanner/com/google/zxing/f;)Lin/juspay/widget/qrscanner/com/google/zxing/c;
.registers 4
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/c;
new-instance v1, Lin/juspay/widget/qrscanner/com/google/zxing/common/j;
invoke-direct {v1, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/j;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/f;)V
invoke-direct {v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/c;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/b;)V
return-object v0
.end method