.class public final Lin/juspay/widget/qrscanner/com/google/zxing/common/f;
.super Lin/juspay/widget/qrscanner/com/google/zxing/common/i;
.source "DefaultGridSampler.java"
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/i;-><init>()V
return-void
.end method
.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;IILin/juspay/widget/qrscanner/com/google/zxing/common/k;)Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
.registers 14
const/high16 v8, 0x3f00
const/4 v1, 0x0
if-lez p2, :cond_7
if-gtz p3, :cond_c
:cond_7
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_c
new-instance v3, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-direct {v3, p2, p3}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;-><init>(II)V
mul-int/lit8 v0, p2, 0x2
new-array v4, v0, [F
move v2, v1
:goto_16
if-ge v2, p3, :cond_55
array-length v5, v4
int-to-float v0, v2
add-float v6, v0, v8
move v0, v1
:goto_1d
if-ge v0, v5, :cond_2c
div-int/lit8 v7, v0, 0x2
int-to-float v7, v7
add-float/2addr v7, v8
aput v7, v4, v0
add-int/lit8 v7, v0, 0x1
aput v6, v4, v7
add-int/lit8 v0, v0, 0x2
goto :goto_1d
:cond_2c
invoke-virtual {p4, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/common/k;->a([F)V
invoke-static {p1, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/common/f;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;[F)V
move v0, v1
:goto_33
if-ge v0, v5, :cond_51
:try_start_35
aget v6, v4, v0
float-to-int v6, v6
add-int/lit8 v7, v0, 0x1
aget v7, v4, v7
float-to-int v7, v7
invoke-virtual {p1, v6, v7}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v6
if-eqz v6, :cond_48
div-int/lit8 v6, v0, 0x2
invoke-virtual {v3, v6, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->b(II)V
:cond_48
:try_end_48
.catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_35 .. :try_end_48} :catch_4b
add-int/lit8 v0, v0, 0x2
goto :goto_33
:catch_4b
move-exception v0
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_51
add-int/lit8 v0, v2, 0x1
move v2, v0
goto :goto_16
:cond_55
return-object v3
.end method