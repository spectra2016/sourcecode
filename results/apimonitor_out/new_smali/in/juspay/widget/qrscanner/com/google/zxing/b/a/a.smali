.class final Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;
.super Ljava/lang/Object;
.source "BitMatrixParser.java"
.field private final a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
.field private b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;
.field private c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;
.field private d:Z
.method constructor <init>(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v0
const/16 v1, 0x15
if-lt v0, v1, :cond_10
and-int/lit8 v0, v0, 0x3
const/4 v1, 0x1
if-eq v0, v1, :cond_15
:cond_10
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;
move-result-object v0
throw v0
:cond_15
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
return-void
.end method
.method private a(III)I
.registers 5
iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->d:Z
if-eqz v0, :cond_11
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v0, p2, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v0
:goto_a
if-eqz v0, :cond_18
shl-int/lit8 v0, p3, 0x1
or-int/lit8 v0, v0, 0x1
:goto_10
return v0
:cond_11
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v0, p1, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v0
goto :goto_a
:cond_18
shl-int/lit8 v0, p3, 0x1
goto :goto_10
.end method
.method  a()Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;
.registers 7
const/4 v4, 0x7
const/4 v1, 0x0
const/16 v5, 0x8
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;
if-eqz v0, :cond_b
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;
:goto_a
return-object v0
:cond_b
move v0, v1
move v2, v1
:goto_d
const/4 v3, 0x6
if-ge v0, v3, :cond_17
invoke-direct {p0, v0, v5, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a(III)I
move-result v2
add-int/lit8 v0, v0, 0x1
goto :goto_d
:cond_17
invoke-direct {p0, v4, v5, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a(III)I
move-result v0
invoke-direct {p0, v5, v5, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a(III)I
move-result v0
invoke-direct {p0, v5, v4, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a(III)I
move-result v2
const/4 v0, 0x5
:goto_24
if-ltz v0, :cond_2d
invoke-direct {p0, v5, v0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a(III)I
move-result v2
add-int/lit8 v0, v0, -0x1
goto :goto_24
:cond_2d
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v3
add-int/lit8 v4, v3, -0x7
add-int/lit8 v0, v3, -0x1
:goto_37
if-lt v0, v4, :cond_40
invoke-direct {p0, v5, v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a(III)I
move-result v1
add-int/lit8 v0, v0, -0x1
goto :goto_37
:cond_40
add-int/lit8 v0, v3, -0x8
:goto_42
if-ge v0, v3, :cond_4b
invoke-direct {p0, v0, v5, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a(III)I
move-result v1
add-int/lit8 v0, v0, 0x1
goto :goto_42
:cond_4b
invoke-static {v2, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;->b(II)Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;
move-result-object v0
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;
if-eqz v0, :cond_58
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;
goto :goto_a
:cond_58
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;
move-result-object v0
throw v0
.end method
.method  a(Z)V
.registers 3
const/4 v0, 0x0
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;
iput-boolean p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->d:Z
return-void
.end method
.method  b()Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;
.registers 9
const/4 v1, 0x5
const/4 v2, 0x0
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;
if-eqz v0, :cond_9
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;
:goto_8
return-object v0
:cond_9
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v5
add-int/lit8 v0, v5, -0x11
div-int/lit8 v0, v0, 0x4
const/4 v3, 0x6
if-gt v0, v3, :cond_1b
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->b(I)Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;
move-result-object v0
goto :goto_8
:cond_1b
add-int/lit8 v6, v5, -0xb
move v4, v1
move v3, v2
:goto_1f
if-ltz v4, :cond_30
add-int/lit8 v0, v5, -0x9
:goto_23
if-lt v0, v6, :cond_2c
invoke-direct {p0, v0, v4, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a(III)I
move-result v3
add-int/lit8 v0, v0, -0x1
goto :goto_23
:cond_2c
add-int/lit8 v0, v4, -0x1
move v4, v0
goto :goto_1f
:cond_30
invoke-static {v3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->c(I)Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;
move-result-object v0
if-eqz v0, :cond_3f
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->d()I
move-result v3
if-ne v3, v5, :cond_3f
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;
goto :goto_8
:cond_3f
move v7, v1
move v1, v2
move v2, v7
:goto_42
if-ltz v2, :cond_53
add-int/lit8 v0, v5, -0x9
:goto_46
if-lt v0, v6, :cond_4f
invoke-direct {p0, v2, v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a(III)I
move-result v1
add-int/lit8 v0, v0, -0x1
goto :goto_46
:cond_4f
add-int/lit8 v0, v2, -0x1
move v2, v0
goto :goto_42
:cond_53
invoke-static {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->c(I)Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;
move-result-object v0
if-eqz v0, :cond_62
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->d()I
move-result v1
if-ne v1, v5, :cond_62
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->b:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;
goto :goto_8
:cond_62
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;
move-result-object v0
throw v0
.end method
.method  c()[B
.registers 16
const/4 v4, 0x0
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a()Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;
move-result-object v0
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->b()Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;
move-result-object v9
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->values()[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;
move-result-object v1
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;->b()B
move-result v0
aget-object v0, v1, v0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v10
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v0, v1, v10}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;I)V
invoke-virtual {v9}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->e()Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
move-result-object v11
const/4 v1, 0x1
invoke-virtual {v9}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->c()I
move-result v0
new-array v12, v0, [B
add-int/lit8 v0, v10, -0x1
move v3, v4
move v5, v4
move v6, v4
move v8, v1
:goto_2f
if-lez v0, :cond_74
const/4 v1, 0x6
if-ne v0, v1, :cond_36
add-int/lit8 v0, v0, -0x1
:cond_36
move v2, v4
:goto_37
if-ge v2, v10, :cond_6e
if-eqz v8, :cond_69
add-int/lit8 v1, v10, -0x1
sub-int/2addr v1, v2
:goto_3e
move v7, v4
:goto_3f
const/4 v13, 0x2
if-ge v7, v13, :cond_6b
sub-int v13, v0, v7
invoke-virtual {v11, v13, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v13
if-nez v13, :cond_66
add-int/lit8 v3, v3, 0x1
shl-int/lit8 v5, v5, 0x1
iget-object v13, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
sub-int v14, v0, v7
invoke-virtual {v13, v14, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v13
if-eqz v13, :cond_5a
or-int/lit8 v5, v5, 0x1
:cond_5a
const/16 v13, 0x8
if-ne v3, v13, :cond_66
add-int/lit8 v3, v6, 0x1
int-to-byte v5, v5
aput-byte v5, v12, v6
move v5, v4
move v6, v3
move v3, v4
:cond_66
add-int/lit8 v7, v7, 0x1
goto :goto_3f
:cond_69
move v1, v2
goto :goto_3e
:cond_6b
add-int/lit8 v2, v2, 0x1
goto :goto_37
:cond_6e
xor-int/lit8 v1, v8, 0x1
add-int/lit8 v0, v0, -0x2
move v8, v1
goto :goto_2f
:cond_74
invoke-virtual {v9}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->c()I
move-result v0
if-eq v6, v0, :cond_7f
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/FormatException;
move-result-object v0
throw v0
:cond_7f
return-object v12
.end method
.method  d()V
.registers 4
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;
if-nez v0, :cond_5
:goto_4
return-void
:cond_5
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->values()[Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;
move-result-object v0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->c:Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;->b()B
move-result v1
aget-object v0, v0, v1
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v1
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v0, v2, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/c;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;I)V
goto :goto_4
.end method
.method  e()V
.registers 5
const/4 v0, 0x0
:goto_1
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c()I
move-result v1
if-ge v0, v1, :cond_31
add-int/lit8 v1, v0, 0x1
:goto_b
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v2
if-ge v1, v2, :cond_2e
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v2, v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v2
iget-object v3, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v3, v1, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v3
if-eq v2, v3, :cond_2b
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v2, v1, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c(II)V
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v2, v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c(II)V
:cond_2b
add-int/lit8 v1, v1, 0x1
goto :goto_b
:cond_2e
add-int/lit8 v0, v0, 0x1
goto :goto_1
:cond_31
return-void
.end method