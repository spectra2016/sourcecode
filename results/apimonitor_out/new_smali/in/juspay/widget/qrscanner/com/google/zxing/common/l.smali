.class public final Lin/juspay/widget/qrscanner/com/google/zxing/common/l;
.super Ljava/lang/Object;
.source "StringUtils.java"
.field private static final a:Ljava/lang/String;
.field private static final b:Z
.method static constructor <clinit>()V
.registers 2
invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;
move-result-object v0
invoke-virtual {v0}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
move-result-object v0
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/common/l;->a:Ljava/lang/String;
const-string v0, "SJIS"
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/common/l;->a:Ljava/lang/String;
invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
move-result v0
if-nez v0, :cond_1e
const-string v0, "EUC_JP"
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/common/l;->a:Ljava/lang/String;
invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_22
:cond_1e
const/4 v0, 0x1
:goto_1f
sput-boolean v0, Lin/juspay/widget/qrscanner/com/google/zxing/common/l;->b:Z
return-void
:cond_22
const/4 v0, 0x0
goto :goto_1f
.end method
.method public static a([BLjava/util/Map;)Ljava/lang/String;
.registers 25
if-eqz p1, :cond_19
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/d;->e:Lin/juspay/widget/qrscanner/com/google/zxing/d;
move-object/from16 v0, p1
invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
move-result v2
if-eqz v2, :cond_19
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/d;->e:Lin/juspay/widget/qrscanner/com/google/zxing/d;
move-object/from16 v0, p1
invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;
move-result-object v2
:goto_18
return-object v2
:cond_19
move-object/from16 v0, p0
array-length v0, v0
move/from16 v19, v0
const/4 v12, 0x1
const/4 v10, 0x1
const/16 v17, 0x1
const/16 v16, 0x0
const/4 v15, 0x0
const/4 v14, 0x0
const/4 v13, 0x0
const/4 v9, 0x0
const/4 v8, 0x0
const/4 v7, 0x0
const/4 v6, 0x0
const/4 v5, 0x0
const/4 v4, 0x0
const/4 v11, 0x0
move-object/from16 v0, p0
array-length v2, v0
const/4 v3, 0x3
if-le v2, v3, :cond_b7
const/4 v2, 0x0
aget-byte v2, p0, v2
const/16 v3, -0x11
if-ne v2, v3, :cond_b7
const/4 v2, 0x1
aget-byte v2, p0, v2
const/16 v3, -0x45
if-ne v2, v3, :cond_b7
const/4 v2, 0x2
aget-byte v2, p0, v2
const/16 v3, -0x41
if-ne v2, v3, :cond_b7
const/4 v2, 0x1
:goto_4a
const/4 v3, 0x0
move/from16 v18, v3
move v3, v4
move v4, v6
move/from16 v6, v17
:goto_51
move/from16 v0, v18
move/from16 v1, v19
if-ge v0, v1, :cond_181
if-nez v12, :cond_5d
if-nez v10, :cond_5d
if-eqz v6, :cond_181
:cond_5d
aget-byte v17, p0, v18
move/from16 v0, v17
and-int/lit16 v0, v0, 0xff
move/from16 v20, v0
if-eqz v6, :cond_1fb
if-lez v16, :cond_be
move/from16 v0, v20
and-int/lit16 v0, v0, 0x80
move/from16 v17, v0
if-nez v17, :cond_b9
const/4 v6, 0x0
move/from16 v17, v6
:goto_74
if-eqz v12, :cond_84
const/16 v6, 0x7f
move/from16 v0, v20
if-le v0, v6, :cond_f3
const/16 v6, 0xa0
move/from16 v0, v20
if-ge v0, v6, :cond_f3
const/4 v6, 0x0
move v12, v6
:goto_84
:cond_84
if-eqz v10, :cond_1f0
if-lez v9, :cond_11f
const/16 v6, 0x40
move/from16 v0, v20
if-lt v0, v6, :cond_9a
const/16 v6, 0x7f
move/from16 v0, v20
if-eq v0, v6, :cond_9a
const/16 v6, 0xfc
move/from16 v0, v20
if-le v0, v6, :cond_110
:cond_9a
const/4 v6, 0x0
move/from16 v21, v5
move v5, v4
move/from16 v4, v21
move/from16 v22, v7
move v7, v8
move v8, v9
move v9, v6
move/from16 v6, v22
:goto_a7
add-int/lit8 v10, v18, 0x1
move/from16 v18, v10
move v10, v9
move v9, v8
move v8, v7
move v7, v6
move/from16 v6, v17
move/from16 v21, v5
move v5, v4
move/from16 v4, v21
goto :goto_51
:cond_b7
const/4 v2, 0x0
goto :goto_4a
:cond_b9
add-int/lit8 v16, v16, -0x1
move/from16 v17, v6
goto :goto_74
:cond_be
move/from16 v0, v20
and-int/lit16 v0, v0, 0x80
move/from16 v17, v0
if-eqz v17, :cond_1fb
and-int/lit8 v17, v20, 0x40
if-nez v17, :cond_ce
const/4 v6, 0x0
move/from16 v17, v6
goto :goto_74
:cond_ce
add-int/lit8 v16, v16, 0x1
and-int/lit8 v17, v20, 0x20
if-nez v17, :cond_d9
add-int/lit8 v15, v15, 0x1
move/from16 v17, v6
goto :goto_74
:cond_d9
add-int/lit8 v16, v16, 0x1
and-int/lit8 v17, v20, 0x10
if-nez v17, :cond_e4
add-int/lit8 v14, v14, 0x1
move/from16 v17, v6
goto :goto_74
:cond_e4
add-int/lit8 v16, v16, 0x1
and-int/lit8 v17, v20, 0x8
if-nez v17, :cond_ef
add-int/lit8 v13, v13, 0x1
move/from16 v17, v6
goto :goto_74
:cond_ef
const/4 v6, 0x0
move/from16 v17, v6
goto :goto_74
:cond_f3
const/16 v6, 0x9f
move/from16 v0, v20
if-le v0, v6, :cond_84
const/16 v6, 0xc0
move/from16 v0, v20
if-lt v0, v6, :cond_10b
const/16 v6, 0xd7
move/from16 v0, v20
if-eq v0, v6, :cond_10b
const/16 v6, 0xf7
move/from16 v0, v20
if-ne v0, v6, :cond_84
:cond_10b
add-int/lit8 v6, v11, 0x1
move v11, v6
goto/16 :goto_84
:cond_110
add-int/lit8 v6, v9, -0x1
move v9, v10
move/from16 v21, v4
move v4, v5
move/from16 v5, v21
move/from16 v22, v8
move v8, v6
move v6, v7
move/from16 v7, v22
goto :goto_a7
:cond_11f
const/16 v6, 0x80
move/from16 v0, v20
if-eq v0, v6, :cond_131
const/16 v6, 0xa0
move/from16 v0, v20
if-eq v0, v6, :cond_131
const/16 v6, 0xef
move/from16 v0, v20
if-le v0, v6, :cond_140
:cond_131
const/4 v6, 0x0
move/from16 v21, v5
move v5, v4
move/from16 v4, v21
move/from16 v22, v7
move v7, v8
move v8, v9
move v9, v6
move/from16 v6, v22
goto/16 :goto_a7
:cond_140
const/16 v6, 0xa0
move/from16 v0, v20
if-le v0, v6, :cond_15a
const/16 v6, 0xe0
move/from16 v0, v20
if-ge v0, v6, :cond_15a
add-int/lit8 v8, v8, 0x1
const/4 v6, 0x0
add-int/lit8 v4, v7, 0x1
if-le v4, v5, :cond_1e5
move v5, v6
move v7, v8
move v6, v4
move v8, v9
move v9, v10
goto/16 :goto_a7
:cond_15a
const/16 v6, 0x7f
move/from16 v0, v20
if-le v0, v6, :cond_175
add-int/lit8 v7, v9, 0x1
const/4 v6, 0x0
add-int/lit8 v4, v4, 0x1
if-le v4, v3, :cond_1d8
move v3, v4
move v9, v10
move/from16 v21, v4
move v4, v5
move/from16 v5, v21
move/from16 v22, v7
move v7, v8
move/from16 v8, v22
goto/16 :goto_a7
:cond_175
const/4 v6, 0x0
const/4 v4, 0x0
move v7, v8
move v8, v9
move v9, v10
move/from16 v21, v4
move v4, v5
move/from16 v5, v21
goto/16 :goto_a7
:cond_181
if-eqz v6, :cond_1d6
if-lez v16, :cond_1d6
const/4 v4, 0x0
:goto_186
if-eqz v10, :cond_18b
if-lez v9, :cond_18b
const/4 v10, 0x0
:cond_18b
if-eqz v4, :cond_198
if-nez v2, :cond_194
add-int v2, v15, v14
add-int/2addr v2, v13
if-lez v2, :cond_198
:cond_194
const-string v2, "UTF8"
goto/16 :goto_18
:cond_198
if-eqz v10, :cond_1a8
sget-boolean v2, Lin/juspay/widget/qrscanner/com/google/zxing/common/l;->b:Z
if-nez v2, :cond_1a4
const/4 v2, 0x3
if-ge v5, v2, :cond_1a4
const/4 v2, 0x3
if-lt v3, v2, :cond_1a8
:cond_1a4
const-string v2, "SJIS"
goto/16 :goto_18
:cond_1a8
if-eqz v12, :cond_1c0
if-eqz v10, :cond_1c0
const/4 v2, 0x2
if-ne v5, v2, :cond_1b2
const/4 v2, 0x2
if-eq v8, v2, :cond_1b8
:cond_1b2
mul-int/lit8 v2, v11, 0xa
move/from16 v0, v19
if-lt v2, v0, :cond_1bc
:cond_1b8
const-string v2, "SJIS"
goto/16 :goto_18
:cond_1bc
const-string v2, "ISO8859_1"
goto/16 :goto_18
:cond_1c0
if-eqz v12, :cond_1c6
const-string v2, "ISO8859_1"
goto/16 :goto_18
:cond_1c6
if-eqz v10, :cond_1cc
const-string v2, "SJIS"
goto/16 :goto_18
:cond_1cc
if-eqz v4, :cond_1d2
const-string v2, "UTF8"
goto/16 :goto_18
:cond_1d2
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/common/l;->a:Ljava/lang/String;
goto/16 :goto_18
:cond_1d6
move v4, v6
goto :goto_186
:cond_1d8
move v9, v10
move/from16 v21, v4
move v4, v5
move/from16 v5, v21
move/from16 v22, v7
move v7, v8
move/from16 v8, v22
goto/16 :goto_a7
:cond_1e5
move v7, v8
move v8, v9
move v9, v10
move/from16 v21, v5
move v5, v6
move v6, v4
move/from16 v4, v21
goto/16 :goto_a7
:cond_1f0
move v6, v7
move v7, v8
move v8, v9
move v9, v10
move/from16 v21, v5
move v5, v4
move/from16 v4, v21
goto/16 :goto_a7
:cond_1fb
move/from16 v17, v6
goto/16 :goto_74
.end method