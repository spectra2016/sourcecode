.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
.super Ljava/lang/Object;
.source "CameraInstance.java"
.field private static final a:Ljava/lang/String;
.field private b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
.field private c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;
.field private d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
.field private e:Landroid/os/Handler;
.field private f:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;
.field private g:Z
.field private h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
.field private i:Ljava/lang/Runnable;
.field private j:Ljava/lang/Runnable;
.field private k:Ljava/lang/Runnable;
.field private l:Ljava/lang/Runnable;
.method static constructor <clinit>()V
.registers 1
const-class v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v0
sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a:Ljava/lang/String;
return-void
.end method
.method public constructor <init>(Landroid/content/Context;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, 0x0
iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g:Z
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;-><init>()V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$3;
invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$3;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->i:Ljava/lang/Runnable;
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$4;
invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$4;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->j:Ljava/lang/Runnable;
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$5;
invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$5;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->k:Ljava/lang/Runnable;
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$6;
invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$6;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->l:Ljava/lang/Runnable;
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/n;->a()V
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->a()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
move-result-object v0
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
invoke-direct {v0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;-><init>(Landroid/content/Context;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;)V
return-void
.end method
.method static synthetic a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
return-object v0
.end method
.method static synthetic a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;Ljava/lang/Exception;)V
.registers 2
invoke-direct {p0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Ljava/lang/Exception;)V
return-void
.end method
.method private a(Ljava/lang/Exception;)V
.registers 4
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->e:Landroid/os/Handler;
if-eqz v0, :cond_f
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->e:Landroid/os/Handler;
sget v1, Lin/juspay/widget/qrscanner/a$b;->zxing_camera_error:I
invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
move-result-object v0
invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
:cond_f
return-void
.end method
.method static synthetic b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Landroid/os/Handler;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->e:Landroid/os/Handler;
return-object v0
.end method
.method static synthetic c(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
.registers 2
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->h()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
move-result-object v0
return-object v0
.end method
.method static synthetic d(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;
return-object v0
.end method
.method static synthetic e(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
return-object v0
.end method
.method static synthetic g()Ljava/lang/String;
.registers 1
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a:Ljava/lang/String;
return-object v0
.end method
.method private h()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->h()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
move-result-object v0
return-object v0
.end method
.method private i()V
.registers 3
iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g:Z
if-nez v0, :cond_c
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "CameraInstance is not open"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_c
return-void
.end method
.method public a()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->f:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;
return-object v0
.end method
.method public a(Landroid/os/Handler;)V
.registers 2
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->e:Landroid/os/Handler;
return-void
.end method
.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;)V
.registers 3
iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g:Z
if-nez v0, :cond_b
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
invoke-virtual {v0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;)V
:cond_b
return-void
.end method
.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;)V
.registers 2
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;
return-void
.end method
.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;)V
.registers 3
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->f:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
invoke-virtual {v0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;)V
return-void
.end method
.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;)V
.registers 4
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->i()V
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
new-instance v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$2;
invoke-direct {v1, p0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$2;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;)V
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->a(Ljava/lang/Runnable;)V
return-void
.end method
.method public a(Z)V
.registers 4
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/n;->a()V
iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g:Z
if-eqz v0, :cond_11
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
new-instance v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$1;
invoke-direct {v1, p0, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$1;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;Z)V
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->a(Ljava/lang/Runnable;)V
:cond_11
return-void
.end method
.method public b()V
.registers 3
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/n;->a()V
const/4 v0, 0x1
iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g:Z
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->i:Ljava/lang/Runnable;
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->b(Ljava/lang/Runnable;)V
return-void
.end method
.method public c()V
.registers 3
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/n;->a()V
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->i()V
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->j:Ljava/lang/Runnable;
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->a(Ljava/lang/Runnable;)V
return-void
.end method
.method public d()V
.registers 3
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/n;->a()V
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->i()V
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->k:Ljava/lang/Runnable;
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->a(Ljava/lang/Runnable;)V
return-void
.end method
.method public e()V
.registers 3
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/n;->a()V
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a:Ljava/lang/String;
const-string v1, "Inside CameraInstance Close"
invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g:Z
if-eqz v0, :cond_15
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->l:Ljava/lang/Runnable;
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->a(Ljava/lang/Runnable;)V
:cond_15
const/4 v0, 0x0
iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g:Z
return-void
.end method
.method public f()Z
.registers 2
iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g:Z
return v0
.end method