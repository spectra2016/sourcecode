.class public final enum Lin/juspay/widget/qrscanner/com/google/zxing/e;
.super Ljava/lang/Enum;
.source "EncodeHintType.java"
.field public static final enum a:Lin/juspay/widget/qrscanner/com/google/zxing/e;
.field public static final enum b:Lin/juspay/widget/qrscanner/com/google/zxing/e;
.field public static final enum c:Lin/juspay/widget/qrscanner/com/google/zxing/e;
.field public static final enum d:Lin/juspay/widget/qrscanner/com/google/zxing/e;
.field public static final enum e:Lin/juspay/widget/qrscanner/com/google/zxing/e;
.field public static final enum f:Lin/juspay/widget/qrscanner/com/google/zxing/e;
.field private static final synthetic g:[Lin/juspay/widget/qrscanner/com/google/zxing/e;
.method static constructor <clinit>()V
.registers 8
const/4 v7, 0x4
const/4 v6, 0x3
const/4 v5, 0x2
const/4 v4, 0x1
const/4 v3, 0x0
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;
const-string v1, "ERROR_CORRECTION"
invoke-direct {v0, v1, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/e;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/e;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;
const-string v1, "CHARACTER_SET"
invoke-direct {v0, v1, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/e;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;->b:Lin/juspay/widget/qrscanner/com/google/zxing/e;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;
const-string v1, "MIN_SIZE"
invoke-direct {v0, v1, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/e;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;->c:Lin/juspay/widget/qrscanner/com/google/zxing/e;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;
const-string v1, "MAX_SIZE"
invoke-direct {v0, v1, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/e;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;->d:Lin/juspay/widget/qrscanner/com/google/zxing/e;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;
const-string v1, "MARGIN"
invoke-direct {v0, v1, v7}, Lin/juspay/widget/qrscanner/com/google/zxing/e;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;->e:Lin/juspay/widget/qrscanner/com/google/zxing/e;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;
const-string v1, "QR_VERSION"
const/4 v2, 0x5
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/e;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;->f:Lin/juspay/widget/qrscanner/com/google/zxing/e;
const/4 v0, 0x6
new-array v0, v0, [Lin/juspay/widget/qrscanner/com/google/zxing/e;
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/e;
aput-object v1, v0, v3
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/e;->b:Lin/juspay/widget/qrscanner/com/google/zxing/e;
aput-object v1, v0, v4
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/e;->c:Lin/juspay/widget/qrscanner/com/google/zxing/e;
aput-object v1, v0, v5
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/e;->d:Lin/juspay/widget/qrscanner/com/google/zxing/e;
aput-object v1, v0, v6
sget-object v1, Lin/juspay/widget/qrscanner/com/google/zxing/e;->e:Lin/juspay/widget/qrscanner/com/google/zxing/e;
aput-object v1, v0, v7
const/4 v1, 0x5
sget-object v2, Lin/juspay/widget/qrscanner/com/google/zxing/e;->f:Lin/juspay/widget/qrscanner/com/google/zxing/e;
aput-object v2, v0, v1
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;->g:[Lin/juspay/widget/qrscanner/com/google/zxing/e;
return-void
.end method
.method private constructor <init>(Ljava/lang/String;I)V
.registers 3
invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V
return-void
.end method
.method public static valueOf(Ljava/lang/String;)Lin/juspay/widget/qrscanner/com/google/zxing/e;
.registers 2
const-class v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;
invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;
return-object v0
.end method
.method public static values()[Lin/juspay/widget/qrscanner/com/google/zxing/e;
.registers 1
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/e;->g:[Lin/juspay/widget/qrscanner/com/google/zxing/e;
invoke-virtual {v0}, [Lin/juspay/widget/qrscanner/com/google/zxing/e;->clone()Ljava/lang/Object;
move-result-object v0
check-cast v0, [Lin/juspay/widget/qrscanner/com/google/zxing/e;
return-object v0
.end method