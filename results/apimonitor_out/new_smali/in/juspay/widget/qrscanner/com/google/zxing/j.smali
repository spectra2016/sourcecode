.class public final Lin/juspay/widget/qrscanner/com/google/zxing/j;
.super Ljava/lang/Object;
.source "Result.java"
.field private final a:Ljava/lang/String;
.field private final b:[B
.field private final c:I
.field private d:[Lin/juspay/widget/qrscanner/com/google/zxing/l;
.field private final e:Lin/juspay/widget/qrscanner/com/google/zxing/a;
.field private f:Ljava/util/Map;
.field private final g:J
.method public constructor <init>(Ljava/lang/String;[BI[Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/a;J)V
.registers 10
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/j;->a:Ljava/lang/String;
iput-object p2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/j;->b:[B
iput p3, p0, Lin/juspay/widget/qrscanner/com/google/zxing/j;->c:I
iput-object p4, p0, Lin/juspay/widget/qrscanner/com/google/zxing/j;->d:[Lin/juspay/widget/qrscanner/com/google/zxing/l;
iput-object p5, p0, Lin/juspay/widget/qrscanner/com/google/zxing/j;->e:Lin/juspay/widget/qrscanner/com/google/zxing/a;
const/4 v0, 0x0
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/j;->f:Ljava/util/Map;
iput-wide p6, p0, Lin/juspay/widget/qrscanner/com/google/zxing/j;->g:J
return-void
.end method
.method public constructor <init>(Ljava/lang/String;[B[Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/a;)V
.registers 13
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v6
move-object v1, p0
move-object v2, p1
move-object v3, p2
move-object v4, p3
move-object v5, p4
invoke-direct/range {v1 .. v7}, Lin/juspay/widget/qrscanner/com/google/zxing/j;-><init>(Ljava/lang/String;[B[Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/a;J)V
return-void
.end method
.method public constructor <init>(Ljava/lang/String;[B[Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/a;J)V
.registers 16
if-nez p2, :cond_d
const/4 v3, 0x0
:goto_3
move-object v0, p0
move-object v1, p1
move-object v2, p2
move-object v4, p3
move-object v5, p4
move-wide v6, p5
invoke-direct/range {v0 .. v7}, Lin/juspay/widget/qrscanner/com/google/zxing/j;-><init>(Ljava/lang/String;[BI[Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/a;J)V
return-void
:cond_d
array-length v0, p2
mul-int/lit8 v3, v0, 0x8
goto :goto_3
.end method
.method public a()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/j;->a:Ljava/lang/String;
return-object v0
.end method
.method public a(Lin/juspay/widget/qrscanner/com/google/zxing/k;Ljava/lang/Object;)V
.registers 5
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/j;->f:Ljava/util/Map;
if-nez v0, :cond_d
new-instance v0, Ljava/util/EnumMap;
const-class v1, Lin/juspay/widget/qrscanner/com/google/zxing/k;
invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/j;->f:Ljava/util/Map;
:cond_d
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/j;->f:Ljava/util/Map;
invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
return-void
.end method
.method public toString()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/j;->a:Ljava/lang/String;
return-object v0
.end method