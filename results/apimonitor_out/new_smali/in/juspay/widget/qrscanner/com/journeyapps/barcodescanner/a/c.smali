.class public final Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
.super Ljava/lang/Object;
.source "CameraManager.java"
.field private static final a:Ljava/lang/String;
.field private b:Landroid/hardware/Camera;
.field private c:Landroid/hardware/Camera$CameraInfo;
.field private d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;
.field private e:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;
.field private f:Z
.field private g:Ljava/lang/String;
.field private h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
.field private i:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;
.field private j:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
.field private k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
.field private l:I
.field private m:Landroid/content/Context;
.field private final n:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;
.method static constructor <clinit>()V
.registers 1
const-class v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v0
sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a:Ljava/lang/String;
return-void
.end method
.method public constructor <init>(Landroid/content/Context;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
invoke-direct {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;-><init>()V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
const/4 v0, -0x1
iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->l:I
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->m:Landroid/content/Context;
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;
invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->n:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;
return-void
.end method
.method private static a(Landroid/hardware/Camera$Parameters;)Ljava/util/List;
.registers 6
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;
move-result-object v0
new-instance v1, Ljava/util/ArrayList;
invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V
if-nez v0, :cond_1f
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;
move-result-object v0
if-eqz v0, :cond_1d
new-instance v2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
iget v3, v0, Landroid/hardware/Camera$Size;->width:I
iget v0, v0, Landroid/hardware/Camera$Size;->height:I
invoke-direct {v2, v3, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;-><init>(II)V
invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
:cond_1d
move-object v0, v1
:goto_1e
return-object v0
:cond_1f
invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v2
:goto_23
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_3c
invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/hardware/Camera$Size;
new-instance v3, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
iget v4, v0, Landroid/hardware/Camera$Size;->width:I
iget v0, v0, Landroid/hardware/Camera$Size;->height:I
invoke-direct {v3, v4, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;-><init>(II)V
invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
goto :goto_23
:cond_3c
move-object v0, v1
goto :goto_1e
.end method
.method private a(I)V
.registers 3
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setDisplayOrientation(I)V
return-void
.end method
.method private b(Z)V
.registers 6
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->k()Landroid/hardware/Camera$Parameters;
move-result-object v0
if-nez v0, :cond_e
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a:Ljava/lang/String;
const-string v1, "Device error: no camera parameters are available. Proceeding without configuration."
invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
:goto_d
return-void
:cond_e
sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a:Ljava/lang/String;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Initial camera parameters: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->flatten()Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
if-eqz p1, :cond_33
sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a:Ljava/lang/String;
const-string v2, "In camera config safe mode -- most settings will not be honored"
invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
:cond_33
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->g()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
move-result-object v1
invoke-static {v0, v1, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Landroid/hardware/Camera$Parameters;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;Z)V
if-nez p1, :cond_6f
const/4 v1, 0x0
invoke-static {v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Landroid/hardware/Camera$Parameters;Z)V
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->b()Z
move-result v1
if-eqz v1, :cond_4d
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->f(Landroid/hardware/Camera$Parameters;)V
:cond_4d
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->c()Z
move-result v1
if-eqz v1, :cond_58
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->e(Landroid/hardware/Camera$Parameters;)V
:cond_58
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->e()Z
move-result v1
if-eqz v1, :cond_6f
sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v2, 0xf
if-lt v1, v2, :cond_6f
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->d(Landroid/hardware/Camera$Parameters;)V
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->b(Landroid/hardware/Camera$Parameters;)V
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->c(Landroid/hardware/Camera$Parameters;)V
:cond_6f
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a(Landroid/hardware/Camera$Parameters;)Ljava/util/List;
move-result-object v1
invoke-interface {v1}, Ljava/util/List;->size()I
move-result v2
if-nez v2, :cond_ac
const/4 v1, 0x0
iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->j:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
:goto_7c
sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;
const-string v2, "glass-1"
invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_89
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Landroid/hardware/Camera$Parameters;)V
:cond_89
sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a:Ljava/lang/String;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Final camera parameters: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->flatten()Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
goto/16 :goto_d
:cond_ac
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->i:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->f()Z
move-result v3
invoke-virtual {v2, v1, v3}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->a(Ljava/util/List;Z)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
move-result-object v1
iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->j:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->j:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
iget v1, v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->j:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
iget v2, v2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I
invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V
goto :goto_7c
.end method
.method static synthetic j()Ljava/lang/String;
.registers 1
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a:Ljava/lang/String;
return-object v0
.end method
.method private k()Landroid/hardware/Camera$Parameters;
.registers 3
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;
move-result-object v0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->g:Ljava/lang/String;
if-nez v1, :cond_11
invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->flatten()Ljava/lang/String;
move-result-object v1
iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->g:Ljava/lang/String;
:goto_10
return-object v0
:cond_11
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->g:Ljava/lang/String;
invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->unflatten(Ljava/lang/String;)V
goto :goto_10
.end method
.method private l()I
.registers 5
const/4 v0, 0x0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->i:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;->a()I
move-result v1
packed-switch v1, :pswitch_data_4a
:pswitch_a
:goto_a
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->c:Landroid/hardware/Camera$CameraInfo;
iget v1, v1, Landroid/hardware/Camera$CameraInfo;->facing:I
const/4 v2, 0x1
if-ne v1, v2, :cond_3e
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->c:Landroid/hardware/Camera$CameraInfo;
iget v1, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I
add-int/2addr v0, v1
rem-int/lit16 v0, v0, 0x168
rsub-int v0, v0, 0x168
rem-int/lit16 v0, v0, 0x168
:goto_1c
sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a:Ljava/lang/String;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Camera Display Orientation: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
return v0
:pswitch_35
const/16 v0, 0x5a
goto :goto_a
:pswitch_38
const/16 v0, 0xb4
goto :goto_a
:pswitch_3b
const/16 v0, 0x10e
goto :goto_a
:cond_3e
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->c:Landroid/hardware/Camera$CameraInfo;
iget v1, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I
sub-int v0, v1, v0
add-int/lit16 v0, v0, 0x168
rem-int/lit16 v0, v0, 0x168
goto :goto_1c
nop
:pswitch_data_4a
.packed-switch 0x0
:pswitch_a
:pswitch_35
:pswitch_38
:pswitch_3b
.end packed-switch
.end method
.method private m()V
.registers 4
:try_start_0
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->l()I
move-result v0
iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->l:I
iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->l:I
invoke-direct {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a(I)V
:try_end_b
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_b} :catch_27
:goto_b
const/4 v0, 0x0
:try_start_c
invoke-direct {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b(Z)V
:try_end_f
.catch Ljava/lang/Exception; {:try_start_c .. :try_end_f} :catch_30
:goto_f
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;
move-result-object v0
invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;
move-result-object v0
if-nez v0, :cond_3f
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->j:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
:goto_1f
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->n:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)V
return-void
:catch_27
move-exception v0
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a:Ljava/lang/String;
const-string v1, "Failed to set rotation."
invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_b
:catch_30
move-exception v0
const/4 v0, 0x1
:try_start_32
invoke-direct {p0, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b(Z)V
:try_end_35
.catch Ljava/lang/Exception; {:try_start_32 .. :try_end_35} :catch_36
goto :goto_f
:catch_36
move-exception v0
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a:Ljava/lang/String;
const-string v1, "Camera rejected even safe-mode parameters! No configuration"
invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_f
:cond_3f
new-instance v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
iget v2, v0, Landroid/hardware/Camera$Size;->width:I
iget v0, v0, Landroid/hardware/Camera$Size;->height:I
invoke-direct {v1, v2, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;-><init>(II)V
iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
goto :goto_1f
.end method
.method public a()V
.registers 3
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->a()I
move-result v0
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a/a;->b(I)Landroid/hardware/Camera;
move-result-object v0
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
if-nez v0, :cond_29
new-instance v0, Ljava/lang/RuntimeException;
const-string v1, "Failed to open camera"
invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V
throw v0
:cond_29
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->a()I
move-result v0
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a/a;->a(I)I
move-result v0
new-instance v1, Landroid/hardware/Camera$CameraInfo;
invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V
iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->c:Landroid/hardware/Camera$CameraInfo;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->c:Landroid/hardware/Camera$CameraInfo;
invoke-static {v0, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V
return-void
.end method
.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;)V
.registers 2
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
return-void
.end method
.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;)V
.registers 3
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
invoke-virtual {p1, v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;->a(Landroid/hardware/Camera;)V
return-void
.end method
.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;)V
.registers 2
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->i:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;
return-void
.end method
.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;)V
.registers 4
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
if-eqz v0, :cond_12
iget-boolean v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->f:Z
if-eqz v1, :cond_12
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->n:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;
invoke-virtual {v1, p1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;)V
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->n:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;
invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setOneShotPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
:cond_12
return-void
.end method
.method public a(Z)V
.registers 4
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
if-eqz v0, :cond_35
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->i()Z
move-result v0
if-eq p1, v0, :cond_35
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;
if-eqz v0, :cond_13
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;->b()V
:cond_13
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;
move-result-object v0
invoke-static {v0, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Landroid/hardware/Camera$Parameters;Z)V
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;->d()Z
move-result v1
if-eqz v1, :cond_27
invoke-static {v0, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->b(Landroid/hardware/Camera$Parameters;Z)V
:cond_27
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;
if-eqz v0, :cond_35
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;->a()V
:cond_35
return-void
.end method
.method public b()V
.registers 3
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
if-nez v0, :cond_c
new-instance v0, Ljava/lang/RuntimeException;
const-string v1, "Camera not open"
invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V
throw v0
:cond_c
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->m()V
return-void
.end method
.method public c()V
.registers 4
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
if-eqz v0, :cond_29
iget-boolean v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->f:Z
if-nez v1, :cond_29
invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V
const/4 v0, 0x1
iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->f:Z
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;-><init>(Landroid/hardware/Camera;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->m:Landroid/content/Context;
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->h:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
invoke-direct {v0, v1, p0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;-><init>(Landroid/content/Context;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->e:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->e:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->a()V
:cond_29
return-void
.end method
.method public d()V
.registers 3
const/4 v1, 0x0
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;
if-eqz v0, :cond_c
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;->b()V
iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;
:cond_c
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->e:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;
if-eqz v0, :cond_17
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->e:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;->b()V
iput-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->e:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a;
:cond_17
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
if-eqz v0, :cond_2c
iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->f:Z
if-eqz v0, :cond_2c
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->n:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/k;)V
const/4 v0, 0x0
iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->f:Z
:cond_2c
return-void
.end method
.method public e()V
.registers 3
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
if-eqz v0, :cond_1a
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a:Ljava/lang/String;
const-string v1, "before Release"
invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
invoke-virtual {v0}, Landroid/hardware/Camera;->release()V
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->a:Ljava/lang/String;
const-string v1, "after Release"
invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
const/4 v0, 0x0
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
:cond_1a
return-void
.end method
.method public f()Z
.registers 3
iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->l:I
const/4 v1, -0x1
if-ne v0, v1, :cond_d
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "Rotation not calculated yet. Call configure() first."
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_d
iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->l:I
rem-int/lit16 v0, v0, 0xb4
if-eqz v0, :cond_15
const/4 v0, 0x1
:goto_14
return v0
:cond_15
const/4 v0, 0x0
goto :goto_14
.end method
.method public g()I
.registers 2
iget v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->l:I
return v0
.end method
.method public h()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
if-nez v0, :cond_6
const/4 v0, 0x0
:goto_5
return-object v0
:cond_6
invoke-virtual {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->f()Z
move-result v0
if-eqz v0, :cond_13
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
move-result-object v0
goto :goto_5
:cond_13
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
goto :goto_5
.end method
.method public i()Z
.registers 4
const/4 v0, 0x0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->b:Landroid/hardware/Camera;
invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;
move-result-object v1
if-eqz v1, :cond_20
invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;
move-result-object v1
if-eqz v1, :cond_20
const-string v2, "on"
invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v2
if-nez v2, :cond_1f
const-string v2, "torch"
invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_20
:cond_1f
const/4 v0, 0x1
:cond_20
return v0
.end method