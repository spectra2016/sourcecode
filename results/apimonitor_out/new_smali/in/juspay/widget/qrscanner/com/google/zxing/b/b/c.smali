.class public Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;
.super Ljava/lang/Object;
.source "Detector.java"
.field private final a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
.field private b:Lin/juspay/widget/qrscanner/com/google/zxing/m;
.method public constructor <init>(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;)V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
return-void
.end method
.method private a(IIII)F
.registers 13
const/4 v0, 0x0
const/high16 v2, 0x3f80
invoke-direct {p0, p1, p2, p3, p4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->b(IIII)F
move-result v4
sub-int v1, p3, p1
sub-int v1, p1, v1
if-gez v1, :cond_31
int-to-float v3, p1
sub-int v1, p1, v1
int-to-float v1, v1
div-float v1, v3, v1
move v3, v0
:goto_14
int-to-float v5, p2
sub-int v6, p4, p2
int-to-float v6, v6
mul-float/2addr v1, v6
sub-float v1, v5, v1
float-to-int v1, v1
if-gez v1, :cond_52
int-to-float v5, p2
sub-int v1, p2, v1
int-to-float v1, v1
div-float v1, v5, v1
:goto_24
int-to-float v5, p1
sub-int/2addr v3, p1
int-to-float v3, v3
mul-float/2addr v1, v3
add-float/2addr v1, v5
float-to-int v1, v1
invoke-direct {p0, p1, p2, v1, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->b(IIII)F
move-result v0
add-float/2addr v0, v4
sub-float/2addr v0, v2
return v0
:cond_31
iget-object v3, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v3}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c()I
move-result v3
if-lt v1, v3, :cond_74
iget-object v3, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v3}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c()I
move-result v3
add-int/lit8 v3, v3, -0x1
sub-int/2addr v3, p1
int-to-float v3, v3
sub-int/2addr v1, p1
int-to-float v1, v1
div-float/2addr v3, v1
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c()I
move-result v1
add-int/lit8 v1, v1, -0x1
move v7, v1
move v1, v3
move v3, v7
goto :goto_14
:cond_52
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v0
if-lt v1, v0, :cond_71
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v0
add-int/lit8 v0, v0, -0x1
sub-int/2addr v0, p2
int-to-float v0, v0
sub-int/2addr v1, p2
int-to-float v1, v1
div-float v1, v0, v1
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v0
add-int/lit8 v0, v0, -0x1
goto :goto_24
:cond_71
move v0, v1
move v1, v2
goto :goto_24
:cond_74
move v3, v1
move v1, v2
goto :goto_14
.end method
.method private a(Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;)F
.registers 9
const/high16 v5, 0x40e0
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F
move-result v0
float-to-int v0, v0
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F
move-result v1
float-to-int v1, v1
invoke-virtual {p2}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F
move-result v2
float-to-int v2, v2
invoke-virtual {p2}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F
move-result v3
float-to-int v3, v3
invoke-direct {p0, v0, v1, v2, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a(IIII)F
move-result v0
invoke-virtual {p2}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F
move-result v1
float-to-int v1, v1
invoke-virtual {p2}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F
move-result v2
float-to-int v2, v2
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F
move-result v3
float-to-int v3, v3
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F
move-result v4
float-to-int v4, v4
invoke-direct {p0, v1, v2, v3, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a(IIII)F
move-result v1
invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z
move-result v2
if-eqz v2, :cond_3b
div-float v0, v1, v5
:goto_3a
return v0
:cond_3b
invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z
move-result v2
if-eqz v2, :cond_43
div-float/2addr v0, v5
goto :goto_3a
:cond_43
add-float/2addr v0, v1
const/high16 v1, 0x4160
div-float/2addr v0, v1
goto :goto_3a
.end method
.method private static a(Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;F)I
.registers 6
invoke-static {p0, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a(Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;)F
move-result v0
div-float/2addr v0, p3
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/a/a;->a(F)I
move-result v0
invoke-static {p0, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a(Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;)F
move-result v1
div-float/2addr v1, p3
invoke-static {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/a/a;->a(F)I
move-result v1
add-int/2addr v0, v1
div-int/lit8 v0, v0, 0x2
add-int/lit8 v0, v0, 0x7
and-int/lit8 v1, v0, 0x3
packed-switch v1, :pswitch_data_28
:goto_1c
:pswitch_1c
return v0
:pswitch_1d
add-int/lit8 v0, v0, 0x1
goto :goto_1c
:pswitch_20
add-int/lit8 v0, v0, -0x1
goto :goto_1c
:pswitch_23
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:pswitch_data_28
.packed-switch 0x0
:pswitch_1d
:pswitch_1c
:pswitch_20
:pswitch_23
.end packed-switch
.end method
.method private static a(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;Lin/juspay/widget/qrscanner/com/google/zxing/common/k;I)Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
.registers 4
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/common/i;->a()Lin/juspay/widget/qrscanner/com/google/zxing/common/i;
move-result-object v0
invoke-virtual {v0, p0, p2, p2, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/i;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;IILin/juspay/widget/qrscanner/com/google/zxing/common/k;)Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
move-result-object v0
return-object v0
.end method
.method private static a(Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;I)Lin/juspay/widget/qrscanner/com/google/zxing/common/k;
.registers 22
move/from16 v0, p4
int-to-float v1, v0
const/high16 v2, 0x4060
sub-float v3, v1, v2
if-eqz p3, :cond_3c
invoke-virtual/range {p3 .. p3}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F
move-result v13
invoke-virtual/range {p3 .. p3}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F
move-result v14
const/high16 v1, 0x4040
sub-float v6, v3, v1
move v5, v6
:goto_16
const/high16 v1, 0x4060
const/high16 v2, 0x4060
const/high16 v4, 0x4060
const/high16 v7, 0x4060
invoke-virtual/range {p0 .. p0}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F
move-result v9
invoke-virtual/range {p0 .. p0}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F
move-result v10
invoke-virtual/range {p1 .. p1}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F
move-result v11
invoke-virtual/range {p1 .. p1}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F
move-result v12
invoke-virtual/range {p2 .. p2}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F
move-result v15
invoke-virtual/range {p2 .. p2}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F
move-result v16
move v8, v3
invoke-static/range {v1 .. v16}, Lin/juspay/widget/qrscanner/com/google/zxing/common/k;->a(FFFFFFFFFFFFFFFF)Lin/juspay/widget/qrscanner/com/google/zxing/common/k;
move-result-object v1
return-object v1
:cond_3c
invoke-virtual/range {p1 .. p1}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F
move-result v1
invoke-virtual/range {p0 .. p0}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F
move-result v2
sub-float/2addr v1, v2
invoke-virtual/range {p2 .. p2}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F
move-result v2
add-float v13, v1, v2
invoke-virtual/range {p1 .. p1}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F
move-result v1
invoke-virtual/range {p0 .. p0}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F
move-result v2
sub-float/2addr v1, v2
invoke-virtual/range {p2 .. p2}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F
move-result v2
add-float v14, v1, v2
move v6, v3
move v5, v3
goto :goto_16
.end method
.method private b(IIII)F
.registers 24
sub-int v3, p4, p2
invoke-static {v3}, Ljava/lang/Math;->abs(I)I
move-result v3
sub-int v4, p3, p1
invoke-static {v4}, Ljava/lang/Math;->abs(I)I
move-result v4
if-le v3, v4, :cond_5e
const/4 v3, 0x1
move v12, v3
:goto_10
if-eqz v12, :cond_9a
:goto_12
sub-int v3, p4, p2
invoke-static {v3}, Ljava/lang/Math;->abs(I)I
move-result v13
sub-int v3, p3, p1
invoke-static {v3}, Ljava/lang/Math;->abs(I)I
move-result v14
neg-int v3, v13
div-int/lit8 v5, v3, 0x2
move/from16 v0, p2
move/from16 v1, p4
if-ge v0, v1, :cond_61
const/4 v3, 0x1
move v11, v3
:goto_29
move/from16 v0, p1
move/from16 v1, p3
if-ge v0, v1, :cond_64
const/4 v3, 0x1
:goto_30
const/4 v6, 0x0
add-int v15, p4, v11
move/from16 v8, p2
move v10, v5
move/from16 v5, p1
:goto_38
if-eq v8, v15, :cond_98
if-eqz v12, :cond_66
move v9, v5
:goto_3d
if-eqz v12, :cond_68
move v7, v8
:goto_40
const/4 v4, 0x1
if-ne v6, v4, :cond_6a
const/4 v4, 0x1
:goto_44
move-object/from16 v0, p0
iget-object v0, v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
move-object/from16 v16, v0
move-object/from16 v0, v16
invoke-virtual {v0, v9, v7}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v7
if-ne v4, v7, :cond_96
const/4 v4, 0x2
if-ne v6, v4, :cond_6c
move/from16 v0, p2
move/from16 v1, p1
invoke-static {v8, v5, v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/a/a;->a(IIII)F
move-result v3
:goto_5d
return v3
:cond_5e
const/4 v3, 0x0
move v12, v3
goto :goto_10
:cond_61
const/4 v3, -0x1
move v11, v3
goto :goto_29
:cond_64
const/4 v3, -0x1
goto :goto_30
:cond_66
move v9, v8
goto :goto_3d
:cond_68
move v7, v5
goto :goto_40
:cond_6a
const/4 v4, 0x0
goto :goto_44
:cond_6c
add-int/lit8 v7, v6, 0x1
:goto_6e
add-int v6, v10, v14
if-lez v6, :cond_93
move/from16 v0, p3
if-ne v5, v0, :cond_87
move v3, v7
:goto_77
const/4 v4, 0x2
if-ne v3, v4, :cond_90
add-int v3, p4, v11
move/from16 v0, p3
move/from16 v1, p2
move/from16 v2, p1
invoke-static {v3, v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/a/a;->a(IIII)F
move-result v3
goto :goto_5d
:cond_87
add-int v4, v5, v3
sub-int v5, v6, v13
:goto_8b
add-int/2addr v8, v11
move v6, v7
move v10, v5
move v5, v4
goto :goto_38
:cond_90
const/high16 v3, 0x7fc0
goto :goto_5d
:cond_93
move v4, v5
move v5, v6
goto :goto_8b
:cond_96
move v7, v6
goto :goto_6e
:cond_98
move v3, v6
goto :goto_77
:cond_9a
move/from16 v17, p4
move/from16 p4, p3
move/from16 p3, v17
move/from16 v18, p2
move/from16 p2, p1
move/from16 p1, v18
goto/16 :goto_12
.end method
.method protected final a(Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;)F
.registers 6
invoke-direct {p0, p1, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a(Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;)F
move-result v0
invoke-direct {p0, p1, p3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a(Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;)F
move-result v1
add-float/2addr v0, v1
const/high16 v1, 0x4000
div-float/2addr v0, v1
return v0
.end method
.method protected final a(FIIF)Lin/juspay/widget/qrscanner/com/google/zxing/b/b/a;
.registers 13
const/4 v5, 0x0
const/high16 v6, 0x4040
mul-float v0, p4, p1
float-to-int v0, v0
sub-int v1, p2, v0
invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I
move-result v2
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c()I
move-result v1
add-int/lit8 v1, v1, -0x1
add-int v3, p2, v0
invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I
move-result v4
sub-int v1, v4, v2
int-to-float v1, v1
mul-float v3, p1, v6
cmpg-float v1, v1, v3
if-gez v1, :cond_28
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_28
sub-int v1, p3, v0
invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I
move-result v3
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v1
add-int/lit8 v1, v1, -0x1
add-int/2addr v0, p3
invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I
move-result v5
sub-int v0, v5, v3
int-to-float v0, v0
mul-float v1, p1, v6
cmpg-float v0, v0, v1
if-gez v0, :cond_49
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_49
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/b;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
sub-int/2addr v4, v2
sub-int/2addr v5, v3
iget-object v7, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->b:Lin/juspay/widget/qrscanner/com/google/zxing/m;
move v6, p1
invoke-direct/range {v0 .. v7}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/b;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;IIIIFLin/juspay/widget/qrscanner/com/google/zxing/m;)V
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/b;->a()Lin/juspay/widget/qrscanner/com/google/zxing/b/b/a;
move-result-object v0
return-object v0
.end method
.method protected final a(Lin/juspay/widget/qrscanner/com/google/zxing/b/b/f;)Lin/juspay/widget/qrscanner/com/google/zxing/common/g;
.registers 13
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/f;->b()Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
move-result-object v2
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/f;->c()Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
move-result-object v3
invoke-virtual {p1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/f;->a()Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
move-result-object v4
invoke-virtual {p0, v2, v3, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a(Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;)F
move-result v5
const/high16 v0, 0x3f80
cmpg-float v0, v5, v0
if-gez v0, :cond_1b
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_1b
invoke-static {v2, v3, v4, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a(Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;F)I
move-result v6
invoke-static {v6}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->a(I)Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;
move-result-object v1
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->d()I
move-result v0
add-int/lit8 v7, v0, -0x7
const/4 v0, 0x0
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;->b()[I
move-result-object v1
array-length v1, v1
if-lez v1, :cond_78
invoke-virtual {v3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->a()F
move-result v1
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->a()F
move-result v8
sub-float/2addr v1, v8
invoke-virtual {v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->a()F
move-result v8
add-float/2addr v1, v8
invoke-virtual {v3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->b()F
move-result v8
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->b()F
move-result v9
sub-float/2addr v8, v9
invoke-virtual {v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->b()F
move-result v9
add-float/2addr v8, v9
const/high16 v9, 0x3f80
const/high16 v10, 0x4040
int-to-float v7, v7
div-float v7, v10, v7
sub-float v7, v9, v7
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->a()F
move-result v9
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->a()F
move-result v10
sub-float/2addr v1, v10
mul-float/2addr v1, v7
add-float/2addr v1, v9
float-to-int v9, v1
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->b()F
move-result v1
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->b()F
move-result v10
sub-float/2addr v8, v10
mul-float/2addr v7, v8
add-float/2addr v1, v7
float-to-int v7, v1
const/4 v1, 0x4
:goto_6f
const/16 v8, 0x10
if-gt v1, v8, :cond_78
int-to-float v8, v1
:try_start_74
invoke-virtual {p0, v5, v9, v7, v8}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a(FIIF)Lin/juspay/widget/qrscanner/com/google/zxing/b/b/a;
:try_end_77
.catch Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException; {:try_start_74 .. :try_end_77} :catch_96
move-result-object v0
:cond_78
invoke-static {v2, v3, v4, v0, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a(Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;Lin/juspay/widget/qrscanner/com/google/zxing/l;I)Lin/juspay/widget/qrscanner/com/google/zxing/common/k;
move-result-object v1
iget-object v5, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-static {v5, v1, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;Lin/juspay/widget/qrscanner/com/google/zxing/common/k;I)Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
move-result-object v5
if-nez v0, :cond_9a
const/4 v0, 0x3
new-array v0, v0, [Lin/juspay/widget/qrscanner/com/google/zxing/l;
const/4 v1, 0x0
aput-object v4, v0, v1
const/4 v1, 0x1
aput-object v2, v0, v1
const/4 v1, 0x2
aput-object v3, v0, v1
:goto_90
new-instance v1, Lin/juspay/widget/qrscanner/com/google/zxing/common/g;
invoke-direct {v1, v5, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/g;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;[Lin/juspay/widget/qrscanner/com/google/zxing/l;)V
return-object v1
:catch_96
move-exception v8
shl-int/lit8 v1, v1, 0x1
goto :goto_6f
:cond_9a
const/4 v1, 0x4
new-array v1, v1, [Lin/juspay/widget/qrscanner/com/google/zxing/l;
const/4 v6, 0x0
aput-object v4, v1, v6
const/4 v4, 0x1
aput-object v2, v1, v4
const/4 v2, 0x2
aput-object v3, v1, v2
const/4 v2, 0x3
aput-object v0, v1, v2
move-object v0, v1
goto :goto_90
.end method
.method public final a(Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/common/g;
.registers 5
if-nez p1, :cond_17
const/4 v0, 0x0
:goto_3
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->b:Lin/juspay/widget/qrscanner/com/google/zxing/m;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->b:Lin/juspay/widget/qrscanner/com/google/zxing/m;
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;-><init>(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;Lin/juspay/widget/qrscanner/com/google/zxing/m;)V
invoke-virtual {v0, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a(Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/b/b/f;
move-result-object v0
invoke-virtual {p0, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/c;->a(Lin/juspay/widget/qrscanner/com/google/zxing/b/b/f;)Lin/juspay/widget/qrscanner/com/google/zxing/common/g;
move-result-object v0
return-object v0
:cond_17
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->j:Lin/juspay/widget/qrscanner/com/google/zxing/d;
invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/m;
goto :goto_3
.end method