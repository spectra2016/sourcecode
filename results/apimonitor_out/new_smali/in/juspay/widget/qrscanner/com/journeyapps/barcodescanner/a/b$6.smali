.class  Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$6;
.super Ljava/lang/Object;
.source "CameraInstance.java"
.implements Ljava/lang/Runnable;
.field final synthetic a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
.method constructor <init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)V
.registers 2
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$6;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public run()V
.registers 4
:try_start_0
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g()Ljava/lang/String;
move-result-object v0
const-string v1, "Closing camera"
invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$6;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
move-result-object v0
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->d()V
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$6;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
move-result-object v0
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;->e()V
:goto_1b
:try_end_1b
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1b} :catch_25
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$6;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->e(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
move-result-object v0
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;->b()V
return-void
:catch_25
move-exception v0
invoke-static {}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;->g()Ljava/lang/String;
move-result-object v1
const-string v2, "Failed to close camera"
invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
goto :goto_1b
.end method