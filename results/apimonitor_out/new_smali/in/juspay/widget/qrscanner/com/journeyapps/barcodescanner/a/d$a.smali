.class public final enum Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
.super Ljava/lang/Enum;
.source "CameraSettings.java"
.field public static final enum a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
.field public static final enum b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
.field public static final enum c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
.field public static final enum d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
.field private static final synthetic e:[Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
.method static constructor <clinit>()V
.registers 6
const/4 v5, 0x3
const/4 v4, 0x2
const/4 v3, 0x1
const/4 v2, 0x0
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
const-string v1, "AUTO"
invoke-direct {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
const-string v1, "CONTINUOUS"
invoke-direct {v0, v1, v3}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
const-string v1, "INFINITY"
invoke-direct {v0, v1, v4}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
const-string v1, "MACRO"
invoke-direct {v0, v1, v5}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;-><init>(Ljava/lang/String;I)V
sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
const/4 v0, 0x4
new-array v0, v0, [Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
aput-object v1, v0, v2
sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
aput-object v1, v0, v3
sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
aput-object v1, v0, v4
sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
aput-object v1, v0, v5
sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->e:[Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
return-void
.end method
.method private constructor <init>(Ljava/lang/String;I)V
.registers 3
invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V
return-void
.end method
.method public static valueOf(Ljava/lang/String;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
.registers 2
const-class v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
return-object v0
.end method
.method public static values()[Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
.registers 1
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->e:[Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
invoke-virtual {v0}, [Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->clone()Ljava/lang/Object;
move-result-object v0
check-cast v0, [Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
return-object v0
.end method