.class public final Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;
.super Ljava/lang/Object;
.source "CameraConfigurationUtils.java"
.field private static final a:Ljava/util/regex/Pattern;
.method static constructor <clinit>()V
.registers 1
const-string v0, ";"
invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
move-result-object v0
sput-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a:Ljava/util/regex/Pattern;
return-void
.end method
.method private static a(Ljava/lang/Iterable;)Ljava/lang/String;
.registers 6
if-nez p0, :cond_4
const/4 v0, 0x0
:goto_3
return-object v0
:cond_4
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;
move-result-object v2
:goto_d
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_31
invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/hardware/Camera$Area;
iget-object v3, v0, Landroid/hardware/Camera$Area;->rect:Landroid/graphics/Rect;
invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v3
const/16 v4, 0x3a
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
move-result-object v3
iget v0, v0, Landroid/hardware/Camera$Area;->weight:I
invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v0
const/16 v3, 0x20
invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
goto :goto_d
:cond_31
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
goto :goto_3
.end method
.method private static varargs a(Ljava/lang/String;Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;
.registers 7
const-string v0, "CameraConfiguration"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Requesting "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, " value from among: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CameraConfiguration"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Supported "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, " values: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
if-eqz p1, :cond_7e
array-length v2, p2
const/4 v0, 0x0
move v1, v0
:goto_4d
if-ge v1, v2, :cond_7e
aget-object v0, p2, v1
invoke-interface {p1, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z
move-result v3
if-eqz v3, :cond_7a
const-string v1, "CameraConfiguration"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Can set "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, " to: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
:goto_79
return-object v0
:cond_7a
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_4d
:cond_7e
const-string v0, "CameraConfiguration"
const-string v1, "No supported values match"
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
const/4 v0, 0x0
goto :goto_79
.end method
.method private static a(Ljava/util/Collection;)Ljava/lang/String;
.registers 4
if-eqz p0, :cond_8
invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z
move-result v0
if-eqz v0, :cond_b
:cond_8
const-string v0, "[]"
:goto_a
return-object v0
:cond_b
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const/16 v0, 0x5b
invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;
move-result-object v2
:cond_19
:goto_19
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_38
invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, [I
invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;
move-result-object v0
invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_19
const-string v0, ", "
invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
goto :goto_19
:cond_38
const/16 v0, 0x5d
invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
goto :goto_a
.end method
.method private static a(I)Ljava/util/List;
.registers 5
new-instance v0, Landroid/hardware/Camera$Area;
new-instance v1, Landroid/graphics/Rect;
neg-int v2, p0
neg-int v3, p0
invoke-direct {v1, v2, v3, p0, p0}, Landroid/graphics/Rect;-><init>(IIII)V
const/4 v2, 0x1
invoke-direct {v0, v1, v2}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V
invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;
move-result-object v0
return-object v0
.end method
.method public static a(Landroid/hardware/Camera$Parameters;)V
.registers 3
const/16 v0, 0xa
const/16 v1, 0x14
invoke-static {p0, v0, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Landroid/hardware/Camera$Parameters;II)V
return-void
.end method
.method public static a(Landroid/hardware/Camera$Parameters;II)V
.registers 11
const/4 v7, 0x1
const/4 v6, 0x0
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;
move-result-object v0
const-string v1, "CameraConfiguration"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Supported FPS ranges: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Ljava/util/Collection;)Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
if-eqz v0, :cond_50
invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
move-result v1
if-nez v1, :cond_50
const/4 v1, 0x0
invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v2
:cond_2f
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_9e
invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, [I
aget v3, v0, v6
aget v4, v0, v7
mul-int/lit16 v5, p1, 0x3e8
if-lt v3, v5, :cond_2f
mul-int/lit16 v3, p2, 0x3e8
if-gt v4, v3, :cond_2f
:goto_47
if-nez v0, :cond_51
const-string v0, "CameraConfiguration"
const-string v1, "No suitable FPS range?"
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
:goto_50
:cond_50
return-void
:cond_51
const/4 v1, 0x2
new-array v1, v1, [I
invoke-virtual {p0, v1}, Landroid/hardware/Camera$Parameters;->getPreviewFpsRange([I)V
invoke-static {v1, v0}, Ljava/util/Arrays;->equals([I[I)Z
move-result v1
if-eqz v1, :cond_7a
const-string v1, "CameraConfiguration"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "FPS range already set to "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;
move-result-object v0
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_50
:cond_7a
const-string v1, "CameraConfiguration"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Setting FPS range to "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
aget v1, v0, v6
aget v0, v0, v7
invoke-virtual {p0, v1, v0}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V
goto :goto_50
:cond_9e
move-object v0, v1
goto :goto_47
.end method
.method public static a(Landroid/hardware/Camera$Parameters;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;Z)V
.registers 10
const/4 v6, 0x2
const/4 v5, 0x1
const/4 v4, 0x0
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;
move-result-object v1
const/4 v0, 0x0
if-nez p2, :cond_e
sget-object v2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->a:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
if-ne p1, v2, :cond_53
:cond_e
const-string v0, "focus mode"
new-array v2, v5, [Ljava/lang/String;
const-string v3, "auto"
aput-object v3, v2, v4
invoke-static {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Ljava/lang/String;Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
:goto_1a
:cond_1a
if-nez p2, :cond_2e
if-nez v0, :cond_2e
const-string v0, "focus mode"
new-array v2, v6, [Ljava/lang/String;
const-string v3, "macro"
aput-object v3, v2, v4
const-string v3, "edof"
aput-object v3, v2, v5
invoke-static {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Ljava/lang/String;Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
:cond_2e
if-eqz v0, :cond_52
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_8f
const-string v1, "CameraConfiguration"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Focus mode already set to "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
:goto_52
:cond_52
return-void
:cond_53
sget-object v2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->b:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
if-ne p1, v2, :cond_6d
const-string v0, "focus mode"
const/4 v2, 0x3
new-array v2, v2, [Ljava/lang/String;
const-string v3, "continuous-picture"
aput-object v3, v2, v4
const-string v3, "continuous-video"
aput-object v3, v2, v5
const-string v3, "auto"
aput-object v3, v2, v6
invoke-static {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Ljava/lang/String;Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
goto :goto_1a
:cond_6d
sget-object v2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->c:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
if-ne p1, v2, :cond_7e
const-string v0, "focus mode"
new-array v2, v5, [Ljava/lang/String;
const-string v3, "infinity"
aput-object v3, v2, v4
invoke-static {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Ljava/lang/String;Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
goto :goto_1a
:cond_7e
sget-object v2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d$a;
if-ne p1, v2, :cond_1a
const-string v0, "focus mode"
new-array v2, v5, [Ljava/lang/String;
const-string v3, "macro"
aput-object v3, v2, v4
invoke-static {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Ljava/lang/String;Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
goto :goto_1a
:cond_8f
invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V
goto :goto_52
.end method
.method public static a(Landroid/hardware/Camera$Parameters;Z)V
.registers 8
const/4 v5, 0x1
const/4 v4, 0x0
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;
move-result-object v0
if-eqz p1, :cond_3e
const-string v1, "flash mode"
const/4 v2, 0x2
new-array v2, v2, [Ljava/lang/String;
const-string v3, "torch"
aput-object v3, v2, v4
const-string v3, "on"
aput-object v3, v2, v5
invoke-static {v1, v0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Ljava/lang/String;Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
:goto_19
if-eqz v0, :cond_3d
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_4b
const-string v1, "CameraConfiguration"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Flash mode already set to "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
:goto_3d
:cond_3d
return-void
:cond_3e
const-string v1, "flash mode"
new-array v2, v5, [Ljava/lang/String;
const-string v3, "off"
aput-object v3, v2, v4
invoke-static {v1, v0, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Ljava/lang/String;Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
goto :goto_19
:cond_4b
const-string v1, "CameraConfiguration"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Setting flash mode to "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V
goto :goto_3d
.end method
.method public static b(Landroid/hardware/Camera$Parameters;)V
.registers 5
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getMaxNumFocusAreas()I
move-result v0
if-lez v0, :cond_4c
const-string v0, "CameraConfiguration"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Old focus areas: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getFocusAreas()Ljava/util/List;
move-result-object v2
invoke-static {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Ljava/lang/Iterable;)Ljava/lang/String;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
const/16 v0, 0x190
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(I)Ljava/util/List;
move-result-object v0
const-string v1, "CameraConfiguration"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Setting focus area to : "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Ljava/lang/Iterable;)Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->setFocusAreas(Ljava/util/List;)V
:goto_4b
return-void
:cond_4c
const-string v0, "CameraConfiguration"
const-string v1, "Device does not support focus areas"
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_4b
.end method
.method public static b(Landroid/hardware/Camera$Parameters;Z)V
.registers 7
const/4 v0, 0x0
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getMinExposureCompensation()I
move-result v1
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getMaxExposureCompensation()I
move-result v2
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getExposureCompensationStep()F
move-result v3
if-nez v1, :cond_11
if-eqz v2, :cond_78
:cond_11
cmpl-float v4, v3, v0
if-lez v4, :cond_78
if-eqz p1, :cond_4f
:goto_17
div-float/2addr v0, v3
invoke-static {v0}, Ljava/lang/Math;->round(F)I
move-result v0
int-to-float v4, v0
mul-float/2addr v3, v4
invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I
move-result v0
invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I
move-result v0
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getExposureCompensation()I
move-result v1
if-ne v1, v0, :cond_52
const-string v1, "CameraConfiguration"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "Exposure compensation already set to "
invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v0
const-string v2, " / "
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
:goto_4e
return-void
:cond_4f
const/high16 v0, 0x3fc0
goto :goto_17
:cond_52
const-string v1, "CameraConfiguration"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "Setting exposure compensation to "
invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v2
const-string v4, " / "
invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->setExposureCompensation(I)V
goto :goto_4e
:cond_78
const-string v0, "CameraConfiguration"
const-string v1, "Camera does not support exposure compensation"
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_4e
.end method
.method public static c(Landroid/hardware/Camera$Parameters;)V
.registers 5
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getMaxNumMeteringAreas()I
move-result v0
if-lez v0, :cond_48
const-string v0, "CameraConfiguration"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Old metering areas: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getMeteringAreas()Ljava/util/List;
move-result-object v2
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
const/16 v0, 0x190
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(I)Ljava/util/List;
move-result-object v0
const-string v1, "CameraConfiguration"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Setting metering area to : "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Ljava/lang/Iterable;)Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->setMeteringAreas(Ljava/util/List;)V
:goto_47
return-void
:cond_48
const-string v0, "CameraConfiguration"
const-string v1, "Device does not support metering areas"
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_47
.end method
.method public static d(Landroid/hardware/Camera$Parameters;)V
.registers 3
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->isVideoStabilizationSupported()Z
move-result v0
if-eqz v0, :cond_20
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getVideoStabilization()Z
move-result v0
if-eqz v0, :cond_14
const-string v0, "CameraConfiguration"
const-string v1, "Video stabilization already enabled"
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
:goto_13
return-void
:cond_14
const-string v0, "CameraConfiguration"
const-string v1, "Enabling video stabilization..."
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
const/4 v0, 0x1
invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->setVideoStabilization(Z)V
goto :goto_13
:cond_20
const-string v0, "CameraConfiguration"
const-string v1, "This device does not support video stabilization"
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_13
.end method
.method public static e(Landroid/hardware/Camera$Parameters;)V
.registers 6
const-string v0, "barcode"
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getSceneMode()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_14
const-string v0, "CameraConfiguration"
const-string v1, "Barcode scene mode already set"
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
:cond_13
:goto_13
return-void
:cond_14
const-string v0, "scene mode"
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getSupportedSceneModes()Ljava/util/List;
move-result-object v1
const/4 v2, 0x1
new-array v2, v2, [Ljava/lang/String;
const/4 v3, 0x0
const-string v4, "barcode"
aput-object v4, v2, v3
invoke-static {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Ljava/lang/String;Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_13
invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->setSceneMode(Ljava/lang/String;)V
goto :goto_13
.end method
.method public static f(Landroid/hardware/Camera$Parameters;)V
.registers 6
const-string v0, "negative"
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getColorEffect()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_14
const-string v0, "CameraConfiguration"
const-string v1, "Negative effect already set"
invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
:cond_13
:goto_13
return-void
:cond_14
const-string v0, "color effect"
invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getSupportedColorEffects()Ljava/util/List;
move-result-object v1
const/4 v2, 0x1
new-array v2, v2, [Ljava/lang/String;
const/4 v3, 0x0
const-string v4, "negative"
aput-object v4, v2, v3
invoke-static {v0, v1, v2}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/a/a;->a(Ljava/lang/String;Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
if-eqz v0, :cond_13
invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->setColorEffect(Ljava/lang/String;)V
goto :goto_13
.end method