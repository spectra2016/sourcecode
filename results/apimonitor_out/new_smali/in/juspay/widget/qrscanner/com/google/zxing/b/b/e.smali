.class public Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;
.super Ljava/lang/Object;
.source "FinderPatternFinder.java"
.field private final a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
.field private final b:Ljava/util/List;
.field private c:Z
.field private final d:[I
.field private final e:Lin/juspay/widget/qrscanner/com/google/zxing/m;
.method public constructor <init>(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;Lin/juspay/widget/qrscanner/com/google/zxing/m;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
const/4 v0, 0x5
new-array v0, v0, [I
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->d:[I
iput-object p2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->e:Lin/juspay/widget/qrscanner/com/google/zxing/m;
return-void
.end method
.method private static a([II)F
.registers 5
const/4 v0, 0x4
aget v0, p0, v0
sub-int v0, p1, v0
const/4 v1, 0x3
aget v1, p0, v1
sub-int/2addr v0, v1
int-to-float v0, v0
const/4 v1, 0x2
aget v1, p0, v1
int-to-float v1, v1
const/high16 v2, 0x4000
div-float/2addr v1, v2
sub-float/2addr v0, v1
return v0
.end method
.method private a(IIII)Z
.registers 12
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a()[I
move-result-object v1
const/4 v0, 0x0
:goto_5
if-lt p1, v0, :cond_1f
if-lt p2, v0, :cond_1f
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
sub-int v3, p2, v0
sub-int v4, p1, v0
invoke-virtual {v2, v3, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v2
if-eqz v2, :cond_1f
const/4 v2, 0x2
aget v3, v1, v2
add-int/lit8 v3, v3, 0x1
aput v3, v1, v2
add-int/lit8 v0, v0, 0x1
goto :goto_5
:cond_1f
if-lt p1, v0, :cond_23
if-ge p2, v0, :cond_25
:cond_23
const/4 v0, 0x0
:goto_24
return v0
:goto_25
:cond_25
if-lt p1, v0, :cond_44
if-lt p2, v0, :cond_44
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
sub-int v3, p2, v0
sub-int v4, p1, v0
invoke-virtual {v2, v3, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v2
if-nez v2, :cond_44
const/4 v2, 0x1
aget v2, v1, v2
if-gt v2, p3, :cond_44
const/4 v2, 0x1
aget v3, v1, v2
add-int/lit8 v3, v3, 0x1
aput v3, v1, v2
add-int/lit8 v0, v0, 0x1
goto :goto_25
:cond_44
if-lt p1, v0, :cond_4d
if-lt p2, v0, :cond_4d
const/4 v2, 0x1
aget v2, v1, v2
if-le v2, p3, :cond_4f
:cond_4d
const/4 v0, 0x0
goto :goto_24
:goto_4f
:cond_4f
if-lt p1, v0, :cond_6e
if-lt p2, v0, :cond_6e
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
sub-int v3, p2, v0
sub-int v4, p1, v0
invoke-virtual {v2, v3, v4}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v2
if-eqz v2, :cond_6e
const/4 v2, 0x0
aget v2, v1, v2
if-gt v2, p3, :cond_6e
const/4 v2, 0x0
aget v3, v1, v2
add-int/lit8 v3, v3, 0x1
aput v3, v1, v2
add-int/lit8 v0, v0, 0x1
goto :goto_4f
:cond_6e
const/4 v0, 0x0
aget v0, v1, v0
if-le v0, p3, :cond_75
const/4 v0, 0x0
goto :goto_24
:cond_75
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c()I
move-result v3
const/4 v0, 0x1
:goto_82
add-int v4, p1, v0
if-ge v4, v2, :cond_a0
add-int v4, p2, v0
if-ge v4, v3, :cond_a0
iget-object v4, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
add-int v5, p2, v0
add-int v6, p1, v0
invoke-virtual {v4, v5, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v4
if-eqz v4, :cond_a0
const/4 v4, 0x2
aget v5, v1, v4
add-int/lit8 v5, v5, 0x1
aput v5, v1, v4
add-int/lit8 v0, v0, 0x1
goto :goto_82
:cond_a0
add-int v4, p1, v0
if-ge v4, v2, :cond_a8
add-int v4, p2, v0
if-lt v4, v3, :cond_ab
:cond_a8
const/4 v0, 0x0
goto/16 :goto_24
:cond_ab
:goto_ab
add-int v4, p1, v0
if-ge v4, v2, :cond_ce
add-int v4, p2, v0
if-ge v4, v3, :cond_ce
iget-object v4, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
add-int v5, p2, v0
add-int v6, p1, v0
invoke-virtual {v4, v5, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v4
if-nez v4, :cond_ce
const/4 v4, 0x3
aget v4, v1, v4
if-ge v4, p3, :cond_ce
const/4 v4, 0x3
aget v5, v1, v4
add-int/lit8 v5, v5, 0x1
aput v5, v1, v4
add-int/lit8 v0, v0, 0x1
goto :goto_ab
:cond_ce
add-int v4, p1, v0
if-ge v4, v2, :cond_db
add-int v4, p2, v0
if-ge v4, v3, :cond_db
const/4 v4, 0x3
aget v4, v1, v4
if-lt v4, p3, :cond_de
:cond_db
const/4 v0, 0x0
goto/16 :goto_24
:cond_de
:goto_de
add-int v4, p1, v0
if-ge v4, v2, :cond_101
add-int v4, p2, v0
if-ge v4, v3, :cond_101
iget-object v4, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
add-int v5, p2, v0
add-int v6, p1, v0
invoke-virtual {v4, v5, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v4
if-eqz v4, :cond_101
const/4 v4, 0x4
aget v4, v1, v4
if-ge v4, p3, :cond_101
const/4 v4, 0x4
aget v5, v1, v4
add-int/lit8 v5, v5, 0x1
aput v5, v1, v4
add-int/lit8 v0, v0, 0x1
goto :goto_de
:cond_101
const/4 v0, 0x4
aget v0, v1, v0
if-lt v0, p3, :cond_109
const/4 v0, 0x0
goto/16 :goto_24
:cond_109
const/4 v0, 0x0
aget v0, v1, v0
const/4 v2, 0x1
aget v2, v1, v2
add-int/2addr v0, v2
const/4 v2, 0x2
aget v2, v1, v2
add-int/2addr v0, v2
const/4 v2, 0x3
aget v2, v1, v2
add-int/2addr v0, v2
const/4 v2, 0x4
aget v2, v1, v2
add-int/2addr v0, v2
sub-int/2addr v0, p4
invoke-static {v0}, Ljava/lang/Math;->abs(I)I
move-result v0
mul-int/lit8 v2, p4, 0x2
if-ge v0, v2, :cond_12e
invoke-static {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a([I)Z
move-result v0
if-eqz v0, :cond_12e
const/4 v0, 0x1
goto/16 :goto_24
:cond_12e
const/4 v0, 0x0
goto/16 :goto_24
.end method
.method protected static a([I)Z
.registers 8
const/4 v0, 0x1
const/high16 v6, 0x4040
const/4 v1, 0x0
move v2, v1
move v3, v1
:goto_6
const/4 v4, 0x5
if-ge v2, v4, :cond_12
aget v4, p0, v2
if-nez v4, :cond_e
:cond_d
:goto_d
return v1
:cond_e
add-int/2addr v3, v4
add-int/lit8 v2, v2, 0x1
goto :goto_6
:cond_12
const/4 v2, 0x7
if-lt v3, v2, :cond_d
int-to-float v2, v3
const/high16 v3, 0x40e0
div-float/2addr v2, v3
const/high16 v3, 0x4000
div-float v3, v2, v3
aget v4, p0, v1
int-to-float v4, v4
sub-float v4, v2, v4
invoke-static {v4}, Ljava/lang/Math;->abs(F)F
move-result v4
cmpg-float v4, v4, v3
if-gez v4, :cond_65
aget v4, p0, v0
int-to-float v4, v4
sub-float v4, v2, v4
invoke-static {v4}, Ljava/lang/Math;->abs(F)F
move-result v4
cmpg-float v4, v4, v3
if-gez v4, :cond_65
mul-float v4, v6, v2
const/4 v5, 0x2
aget v5, p0, v5
int-to-float v5, v5
sub-float/2addr v4, v5
invoke-static {v4}, Ljava/lang/Math;->abs(F)F
move-result v4
mul-float v5, v6, v3
cmpg-float v4, v4, v5
if-gez v4, :cond_65
const/4 v4, 0x3
aget v4, p0, v4
int-to-float v4, v4
sub-float v4, v2, v4
invoke-static {v4}, Ljava/lang/Math;->abs(F)F
move-result v4
cmpg-float v4, v4, v3
if-gez v4, :cond_65
const/4 v4, 0x4
aget v4, p0, v4
int-to-float v4, v4
sub-float/2addr v2, v4
invoke-static {v2}, Ljava/lang/Math;->abs(F)F
move-result v2
cmpg-float v2, v2, v3
if-gez v2, :cond_65
:goto_63
move v1, v0
goto :goto_d
:cond_65
move v0, v1
goto :goto_63
.end method
.method private a()[I
.registers 4
const/4 v2, 0x0
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->d:[I
aput v2, v0, v2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->d:[I
const/4 v1, 0x1
aput v2, v0, v1
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->d:[I
const/4 v1, 0x2
aput v2, v0, v1
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->d:[I
const/4 v1, 0x3
aput v2, v0, v1
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->d:[I
const/4 v1, 0x4
aput v2, v0, v1
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->d:[I
return-object v0
.end method
.method private b(IIII)F
.registers 16
const/4 v10, 0x4
const/4 v9, 0x3
const/4 v8, 0x1
const/4 v7, 0x0
const/high16 v0, 0x7fc0
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v3
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a()[I
move-result-object v4
move v1, p1
:goto_11
if-ltz v1, :cond_23
invoke-virtual {v2, p2, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v5
if-eqz v5, :cond_23
const/4 v5, 0x2
aget v6, v4, v5
add-int/lit8 v6, v6, 0x1
aput v6, v4, v5
add-int/lit8 v1, v1, -0x1
goto :goto_11
:cond_23
if-gez v1, :cond_26
:goto_25
:cond_25
return v0
:goto_26
:cond_26
if-ltz v1, :cond_3b
invoke-virtual {v2, p2, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v5
if-nez v5, :cond_3b
aget v5, v4, v8
if-gt v5, p3, :cond_3b
aget v5, v4, v8
add-int/lit8 v5, v5, 0x1
aput v5, v4, v8
add-int/lit8 v1, v1, -0x1
goto :goto_26
:cond_3b
if-ltz v1, :cond_25
aget v5, v4, v8
if-gt v5, p3, :cond_25
:goto_41
if-ltz v1, :cond_56
invoke-virtual {v2, p2, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v5
if-eqz v5, :cond_56
aget v5, v4, v7
if-gt v5, p3, :cond_56
aget v5, v4, v7
add-int/lit8 v5, v5, 0x1
aput v5, v4, v7
add-int/lit8 v1, v1, -0x1
goto :goto_41
:cond_56
aget v1, v4, v7
if-gt v1, p3, :cond_25
add-int/lit8 v1, p1, 0x1
:goto_5c
if-ge v1, v3, :cond_6e
invoke-virtual {v2, p2, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v5
if-eqz v5, :cond_6e
const/4 v5, 0x2
aget v6, v4, v5
add-int/lit8 v6, v6, 0x1
aput v6, v4, v5
add-int/lit8 v1, v1, 0x1
goto :goto_5c
:cond_6e
if-eq v1, v3, :cond_25
:goto_70
if-ge v1, v3, :cond_85
invoke-virtual {v2, p2, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v5
if-nez v5, :cond_85
aget v5, v4, v9
if-ge v5, p3, :cond_85
aget v5, v4, v9
add-int/lit8 v5, v5, 0x1
aput v5, v4, v9
add-int/lit8 v1, v1, 0x1
goto :goto_70
:cond_85
if-eq v1, v3, :cond_25
aget v5, v4, v9
if-ge v5, p3, :cond_25
:goto_8b
if-ge v1, v3, :cond_a0
invoke-virtual {v2, p2, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v5
if-eqz v5, :cond_a0
aget v5, v4, v10
if-ge v5, p3, :cond_a0
aget v5, v4, v10
add-int/lit8 v5, v5, 0x1
aput v5, v4, v10
add-int/lit8 v1, v1, 0x1
goto :goto_8b
:cond_a0
aget v2, v4, v10
if-ge v2, p3, :cond_25
aget v2, v4, v7
aget v3, v4, v8
add-int/2addr v2, v3
const/4 v3, 0x2
aget v3, v4, v3
add-int/2addr v2, v3
aget v3, v4, v9
add-int/2addr v2, v3
aget v3, v4, v10
add-int/2addr v2, v3
sub-int/2addr v2, p4
invoke-static {v2}, Ljava/lang/Math;->abs(I)I
move-result v2
mul-int/lit8 v2, v2, 0x5
mul-int/lit8 v3, p4, 0x2
if-ge v2, v3, :cond_25
invoke-static {v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a([I)Z
move-result v2
if-eqz v2, :cond_25
invoke-static {v4, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a([II)F
move-result v0
goto/16 :goto_25
.end method
.method private b()I
.registers 8
const/4 v6, 0x1
const/4 v2, 0x0
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->size()I
move-result v0
if-gt v0, v6, :cond_c
move v0, v2
:goto_b
return v0
:cond_c
const/4 v1, 0x0
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v3
:goto_13
invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_4d
invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->d()I
move-result v4
const/4 v5, 0x2
if-lt v4, v5, :cond_4f
if-nez v1, :cond_2a
:goto_28
move-object v1, v0
goto :goto_13
:cond_2a
iput-boolean v6, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->c:Z
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a()F
move-result v2
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->a()F
move-result v3
sub-float/2addr v2, v3
invoke-static {v2}, Ljava/lang/Math;->abs(F)F
move-result v2
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->b()F
move-result v1
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->b()F
move-result v0
sub-float v0, v1, v0
invoke-static {v0}, Ljava/lang/Math;->abs(F)F
move-result v0
sub-float v0, v2, v0
float-to-int v0, v0
div-int/lit8 v0, v0, 0x2
goto :goto_b
:cond_4d
move v0, v2
goto :goto_b
:cond_4f
move-object v0, v1
goto :goto_28
.end method
.method private c(IIII)F
.registers 16
const/4 v10, 0x4
const/4 v9, 0x3
const/4 v8, 0x1
const/4 v7, 0x0
const/high16 v0, 0x7fc0
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c()I
move-result v3
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a()[I
move-result-object v4
move v1, p1
:goto_11
if-ltz v1, :cond_23
invoke-virtual {v2, v1, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v5
if-eqz v5, :cond_23
const/4 v5, 0x2
aget v6, v4, v5
add-int/lit8 v6, v6, 0x1
aput v6, v4, v5
add-int/lit8 v1, v1, -0x1
goto :goto_11
:cond_23
if-gez v1, :cond_26
:goto_25
:cond_25
return v0
:goto_26
:cond_26
if-ltz v1, :cond_3b
invoke-virtual {v2, v1, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v5
if-nez v5, :cond_3b
aget v5, v4, v8
if-gt v5, p3, :cond_3b
aget v5, v4, v8
add-int/lit8 v5, v5, 0x1
aput v5, v4, v8
add-int/lit8 v1, v1, -0x1
goto :goto_26
:cond_3b
if-ltz v1, :cond_25
aget v5, v4, v8
if-gt v5, p3, :cond_25
:goto_41
if-ltz v1, :cond_56
invoke-virtual {v2, v1, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v5
if-eqz v5, :cond_56
aget v5, v4, v7
if-gt v5, p3, :cond_56
aget v5, v4, v7
add-int/lit8 v5, v5, 0x1
aput v5, v4, v7
add-int/lit8 v1, v1, -0x1
goto :goto_41
:cond_56
aget v1, v4, v7
if-gt v1, p3, :cond_25
add-int/lit8 v1, p1, 0x1
:goto_5c
if-ge v1, v3, :cond_6e
invoke-virtual {v2, v1, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v5
if-eqz v5, :cond_6e
const/4 v5, 0x2
aget v6, v4, v5
add-int/lit8 v6, v6, 0x1
aput v6, v4, v5
add-int/lit8 v1, v1, 0x1
goto :goto_5c
:cond_6e
if-eq v1, v3, :cond_25
:goto_70
if-ge v1, v3, :cond_85
invoke-virtual {v2, v1, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v5
if-nez v5, :cond_85
aget v5, v4, v9
if-ge v5, p3, :cond_85
aget v5, v4, v9
add-int/lit8 v5, v5, 0x1
aput v5, v4, v9
add-int/lit8 v1, v1, 0x1
goto :goto_70
:cond_85
if-eq v1, v3, :cond_25
aget v5, v4, v9
if-ge v5, p3, :cond_25
:goto_8b
if-ge v1, v3, :cond_a0
invoke-virtual {v2, v1, p2}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v5
if-eqz v5, :cond_a0
aget v5, v4, v10
if-ge v5, p3, :cond_a0
aget v5, v4, v10
add-int/lit8 v5, v5, 0x1
aput v5, v4, v10
add-int/lit8 v1, v1, 0x1
goto :goto_8b
:cond_a0
aget v2, v4, v10
if-ge v2, p3, :cond_25
aget v2, v4, v7
aget v3, v4, v8
add-int/2addr v2, v3
const/4 v3, 0x2
aget v3, v4, v3
add-int/2addr v2, v3
aget v3, v4, v9
add-int/2addr v2, v3
aget v3, v4, v10
add-int/2addr v2, v3
sub-int/2addr v2, p4
invoke-static {v2}, Ljava/lang/Math;->abs(I)I
move-result v2
mul-int/lit8 v2, v2, 0x5
if-ge v2, p4, :cond_25
invoke-static {v4}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a([I)Z
move-result v2
if-eqz v2, :cond_25
invoke-static {v4, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a([II)F
move-result v0
goto/16 :goto_25
.end method
.method private c()Z
.registers 10
const/4 v2, 0x0
const/4 v4, 0x0
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->size()I
move-result v5
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v6
move v1, v2
move v3, v4
:goto_10
invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_2e
invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->d()I
move-result v7
const/4 v8, 0x2
if-lt v7, v8, :cond_5c
add-int/lit8 v3, v3, 0x1
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->c()F
move-result v0
add-float/2addr v0, v1
move v1, v3
:goto_2b
move v3, v1
move v1, v0
goto :goto_10
:cond_2e
const/4 v0, 0x3
if-ge v3, v0, :cond_32
:goto_31
:cond_31
return v4
:cond_32
int-to-float v0, v5
div-float v3, v1, v0
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v5
:goto_3b
invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_52
invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->c()F
move-result v0
sub-float/2addr v0, v3
invoke-static {v0}, Ljava/lang/Math;->abs(F)F
move-result v0
add-float/2addr v2, v0
goto :goto_3b
:cond_52
const v0, 0x3d4ccccd
mul-float/2addr v0, v1
cmpg-float v0, v2, v0
if-gtz v0, :cond_31
const/4 v4, 0x1
goto :goto_31
:cond_5c
move v0, v1
move v1, v3
goto :goto_2b
.end method
.method private d()[Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
.registers 11
const/4 v9, 0x2
const/4 v8, 0x1
const/4 v4, 0x0
const/4 v2, 0x0
const/4 v7, 0x3
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->size()I
move-result v5
if-ge v5, v7, :cond_12
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;->a()Lin/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
move-result-object v0
throw v0
:cond_12
if-le v5, v7, :cond_83
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v6
move v1, v2
move v3, v2
:goto_1c
invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_31
invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->c()F
move-result v0
add-float/2addr v3, v0
mul-float/2addr v0, v0
add-float/2addr v0, v1
move v1, v0
goto :goto_1c
:cond_31
int-to-float v0, v5
div-float/2addr v3, v0
int-to-float v0, v5
div-float v0, v1, v0
mul-float v1, v3, v3
sub-float/2addr v0, v1
float-to-double v0, v0
invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D
move-result-wide v0
double-to-float v0, v0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
new-instance v5, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e$b;
const/4 v6, 0x0
invoke-direct {v5, v3, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e$b;-><init>(FLin/juspay/widget/qrscanner/com/google/zxing/b/b/e$1;)V
invoke-static {v1, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
const v1, 0x3e4ccccd
mul-float/2addr v1, v3
invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F
move-result v5
move v1, v4
:goto_53
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->size()I
move-result v0
if-ge v1, v0, :cond_83
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->size()I
move-result v0
if-le v0, v7, :cond_83
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->c()F
move-result v0
sub-float/2addr v0, v3
invoke-static {v0}, Ljava/lang/Math;->abs(F)F
move-result v0
cmpl-float v0, v0, v5
if-lez v0, :cond_7f
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;
add-int/lit8 v1, v1, -0x1
:cond_7f
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_53
:cond_83
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->size()I
move-result v0
if-le v0, v7, :cond_c6
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v1
:goto_91
invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_a3
invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->c()F
move-result v0
add-float/2addr v2, v0
goto :goto_91
:cond_a3
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->size()I
move-result v0
int-to-float v0, v0
div-float v0, v2, v0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
new-instance v2, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e$a;
const/4 v3, 0x0
invoke-direct {v2, v0, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e$a;-><init>(FLin/juspay/widget/qrscanner/com/google/zxing/b/b/e$1;)V
invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v1}, Ljava/util/List;->size()I
move-result v1
invoke-interface {v0, v7, v1}, Ljava/util/List;->subList(II)Ljava/util/List;
move-result-object v0
invoke-interface {v0}, Ljava/util/List;->clear()V
:cond_c6
new-array v1, v7, [Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
aput-object v0, v1, v4
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
aput-object v0, v1, v8
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
aput-object v0, v1, v9
return-object v1
.end method
.method final a(Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/b/b/f;
.registers 14
if-eqz p1, :cond_68
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->d:Lin/juspay/widget/qrscanner/com/google/zxing/d;
invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_68
const/4 v0, 0x1
move v2, v0
:goto_c
if-eqz p1, :cond_6b
sget-object v0, Lin/juspay/widget/qrscanner/com/google/zxing/d;->b:Lin/juspay/widget/qrscanner/com/google/zxing/d;
invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_6b
const/4 v0, 0x1
:goto_17
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->d()I
move-result v6
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v1}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->c()I
move-result v7
mul-int/lit8 v1, v6, 0x3
div-int/lit16 v1, v1, 0xe4
const/4 v3, 0x3
if-lt v1, v3, :cond_2c
if-eqz v2, :cond_2d
:cond_2c
const/4 v1, 0x3
:cond_2d
const/4 v4, 0x0
const/4 v2, 0x5
new-array v8, v2, [I
add-int/lit8 v3, v1, -0x1
move v5, v1
:goto_34
if-ge v3, v6, :cond_11d
if-nez v4, :cond_11d
const/4 v1, 0x0
const/4 v2, 0x0
aput v2, v8, v1
const/4 v1, 0x1
const/4 v2, 0x0
aput v2, v8, v1
const/4 v1, 0x2
const/4 v2, 0x0
aput v2, v8, v1
const/4 v1, 0x3
const/4 v2, 0x0
aput v2, v8, v1
const/4 v1, 0x4
const/4 v2, 0x0
aput v2, v8, v1
const/4 v1, 0x0
const/4 v2, 0x0
:goto_4e
if-ge v2, v7, :cond_103
iget-object v9, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a:Lin/juspay/widget/qrscanner/com/google/zxing/common/b;
invoke-virtual {v9, v2, v3}, Lin/juspay/widget/qrscanner/com/google/zxing/common/b;->a(II)Z
move-result v9
if-eqz v9, :cond_6d
and-int/lit8 v9, v1, 0x1
const/4 v10, 0x1
if-ne v9, v10, :cond_5f
add-int/lit8 v1, v1, 0x1
:cond_5f
aget v9, v8, v1
add-int/lit8 v9, v9, 0x1
aput v9, v8, v1
:goto_65
add-int/lit8 v2, v2, 0x1
goto :goto_4e
:cond_68
const/4 v0, 0x0
move v2, v0
goto :goto_c
:cond_6b
const/4 v0, 0x0
goto :goto_17
:cond_6d
and-int/lit8 v9, v1, 0x1
if-nez v9, :cond_fb
const/4 v9, 0x4
if-ne v1, v9, :cond_f1
invoke-static {v8}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a([I)Z
move-result v1
if-eqz v1, :cond_d4
invoke-virtual {p0, v8, v3, v2, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a([IIIZ)Z
move-result v1
if-eqz v1, :cond_b8
const/4 v5, 0x2
iget-boolean v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->c:Z
if-eqz v1, :cond_a2
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->c()Z
move-result v1
:goto_89
const/4 v4, 0x0
const/4 v9, 0x0
const/4 v10, 0x0
aput v10, v8, v9
const/4 v9, 0x1
const/4 v10, 0x0
aput v10, v8, v9
const/4 v9, 0x2
const/4 v10, 0x0
aput v10, v8, v9
const/4 v9, 0x3
const/4 v10, 0x0
aput v10, v8, v9
const/4 v9, 0x4
const/4 v10, 0x0
aput v10, v8, v9
move v11, v4
move v4, v1
move v1, v11
goto :goto_65
:cond_a2
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b()I
move-result v1
const/4 v9, 0x2
aget v9, v8, v9
if-le v1, v9, :cond_12a
const/4 v2, 0x2
aget v2, v8, v2
sub-int/2addr v1, v2
sub-int/2addr v1, v5
add-int v2, v3, v1
add-int/lit8 v1, v7, -0x1
:goto_b4
move v3, v2
move v2, v1
move v1, v4
goto :goto_89
:cond_b8
const/4 v1, 0x0
const/4 v9, 0x2
aget v9, v8, v9
aput v9, v8, v1
const/4 v1, 0x1
const/4 v9, 0x3
aget v9, v8, v9
aput v9, v8, v1
const/4 v1, 0x2
const/4 v9, 0x4
aget v9, v8, v9
aput v9, v8, v1
const/4 v1, 0x3
const/4 v9, 0x1
aput v9, v8, v1
const/4 v1, 0x4
const/4 v9, 0x0
aput v9, v8, v1
const/4 v1, 0x3
goto :goto_65
:cond_d4
const/4 v1, 0x0
const/4 v9, 0x2
aget v9, v8, v9
aput v9, v8, v1
const/4 v1, 0x1
const/4 v9, 0x3
aget v9, v8, v9
aput v9, v8, v1
const/4 v1, 0x2
const/4 v9, 0x4
aget v9, v8, v9
aput v9, v8, v1
const/4 v1, 0x3
const/4 v9, 0x1
aput v9, v8, v1
const/4 v1, 0x4
const/4 v9, 0x0
aput v9, v8, v1
const/4 v1, 0x3
goto/16 :goto_65
:cond_f1
add-int/lit8 v1, v1, 0x1
aget v9, v8, v1
add-int/lit8 v9, v9, 0x1
aput v9, v8, v1
goto/16 :goto_65
:cond_fb
aget v9, v8, v1
add-int/lit8 v9, v9, 0x1
aput v9, v8, v1
goto/16 :goto_65
:cond_103
invoke-static {v8}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a([I)Z
move-result v1
if-eqz v1, :cond_11a
invoke-virtual {p0, v8, v3, v7, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a([IIIZ)Z
move-result v1
if-eqz v1, :cond_11a
const/4 v1, 0x0
aget v5, v8, v1
iget-boolean v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->c:Z
if-eqz v1, :cond_11a
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->c()Z
move-result v4
:cond_11a
add-int/2addr v3, v5
goto/16 :goto_34
:cond_11d
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->d()[Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
move-result-object v0
invoke-static {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/l;->a([Lin/juspay/widget/qrscanner/com/google/zxing/l;)V
new-instance v1, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/f;
invoke-direct {v1, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/f;-><init>([Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;)V
return-object v1
:cond_12a
move v1, v2
move v2, v3
goto :goto_b4
.end method
.method protected final a([IIIZ)Z
.registers 13
const/4 v3, 0x1
const/4 v7, 0x2
const/4 v2, 0x0
aget v0, p1, v2
aget v1, p1, v3
add-int/2addr v0, v1
aget v1, p1, v7
add-int/2addr v0, v1
const/4 v1, 0x3
aget v1, p1, v1
add-int/2addr v0, v1
const/4 v1, 0x4
aget v1, p1, v1
add-int/2addr v0, v1
invoke-static {p1, p3}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a([II)F
move-result v1
float-to-int v4, v1
aget v5, p1, v7
invoke-direct {p0, p2, v4, v5, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b(IIII)F
move-result v4
invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z
move-result v5
if-nez v5, :cond_7a
float-to-int v1, v1
float-to-int v5, v4
aget v6, p1, v7
invoke-direct {p0, v1, v5, v6, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->c(IIII)F
move-result v5
invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z
move-result v1
if-nez v1, :cond_7a
if-eqz p4, :cond_3e
float-to-int v1, v4
float-to-int v6, v5
aget v7, p1, v7
invoke-direct {p0, v1, v6, v7, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->a(IIII)Z
move-result v1
if-eqz v1, :cond_7a
:cond_3e
int-to-float v0, v0
const/high16 v1, 0x40e0
div-float v6, v0, v1
move v1, v2
:goto_44
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0}, Ljava/util/List;->size()I
move-result v0
if-ge v1, v0, :cond_64
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
invoke-virtual {v0, v6, v4, v5}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->a(FFF)Z
move-result v7
if-eqz v7, :cond_7b
iget-object v2, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-virtual {v0, v4, v5, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;->b(FFF)Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
move-result-object v0
invoke-interface {v2, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
move v2, v3
:cond_64
if-nez v2, :cond_79
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;
invoke-direct {v0, v5, v4, v6}, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/d;-><init>(FFF)V
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->b:Ljava/util/List;
invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->e:Lin/juspay/widget/qrscanner/com/google/zxing/m;
if-eqz v1, :cond_79
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/b/b/e;->e:Lin/juspay/widget/qrscanner/com/google/zxing/m;
invoke-interface {v1, v0}, Lin/juspay/widget/qrscanner/com/google/zxing/m;->a(Lin/juspay/widget/qrscanner/com/google/zxing/l;)V
:cond_79
move v2, v3
:cond_7a
return v2
:cond_7b
add-int/lit8 v0, v1, 0x1
move v1, v0
goto :goto_44
.end method