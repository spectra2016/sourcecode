.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;
.super Ljava/lang/Object;
.source "CaptureManager.java"
.field private static final a:Ljava/lang/String;
.field private static b:I
.field private c:Landroid/app/Activity;
.field private d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;
.field private e:I
.field private f:Z
.field private g:Z
.field private h:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;
.field private i:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;
.field private j:Landroid/os/Handler;
.field private k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;
.field private final l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;
.field private m:Z
.method static constructor <clinit>()V
.registers 1
const-class v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;
invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v0
sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->a:Ljava/lang/String;
const/16 v0, 0xfa
sput v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->b:I
return-void
.end method
.method public constructor <init>(Landroid/app/Activity;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;)V
.registers 5
const/4 v1, 0x0
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, -0x1
iput v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->e:I
iput-boolean v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->f:Z
iput-boolean v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->g:Z
const/4 v0, 0x0
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;
new-instance v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d$1;
invoke-direct {v0, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d$1;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;
iput-boolean v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->m:Z
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->c:Landroid/app/Activity;
iput-object p2, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;
invoke-virtual {p2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->getBarcodeView()Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;
move-result-object v0
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->l:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$a;)V
new-instance v0, Landroid/os/Handler;
invoke-direct {v0}, Landroid/os/Handler;-><init>()V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->j:Landroid/os/Handler;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;
new-instance v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d$2;
invoke-direct {v1, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d$2;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;)V
invoke-direct {v0, p1, v1}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->h:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;
new-instance v0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;
invoke-direct {v0, p1}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;-><init>(Landroid/app/Activity;)V
iput-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->i:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;
return-void
.end method
.method static synthetic a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;)V
.registers 1
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->g()V
return-void
.end method
.method static synthetic f()Ljava/lang/String;
.registers 1
sget-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->a:Ljava/lang/String;
return-object v0
.end method
.method private g()V
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->c:Landroid/app/Activity;
invoke-virtual {v0}, Landroid/app/Activity;->finish()V
return-void
.end method
.method public a()V
.registers 3
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;
invoke-virtual {v0, v1}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;)V
return-void
.end method
.method public a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;)V
.registers 2
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->k:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a;
return-void
.end method
.method public b()V
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->b()V
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->h:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->b()V
return-void
.end method
.method public c()V
.registers 2
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->d:Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView;->a()V
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->h:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->c()V
return-void
.end method
.method public d()V
.registers 2
const/4 v0, 0x1
iput-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->g:Z
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->h:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;
invoke-virtual {v0}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/c;->c()V
return-void
.end method
.method protected e()V
.registers 4
iget-object v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->c:Landroid/app/Activity;
invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z
move-result v0
if-nez v0, :cond_c
iget-boolean v0, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->g:Z
if-eqz v0, :cond_d
:cond_c
:goto_c
return-void
:cond_d
new-instance v0, Landroid/app/AlertDialog$Builder;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->c:Landroid/app/Activity;
invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->c:Landroid/app/Activity;
sget v2, Lin/juspay/widget/qrscanner/a$e;->zxing_app_name:I
invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
iget-object v1, p0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;->c:Landroid/app/Activity;
sget v2, Lin/juspay/widget/qrscanner/a$e;->zxing_msg_camera_framework_bug:I
invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
sget v1, Lin/juspay/widget/qrscanner/a$e;->zxing_button_ok:I
new-instance v2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d$3;
invoke-direct {v2, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d$3;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;)V
invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
new-instance v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d$4;
invoke-direct {v1, p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d$4;-><init>(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d;)V
invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;
invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
goto :goto_c
.end method