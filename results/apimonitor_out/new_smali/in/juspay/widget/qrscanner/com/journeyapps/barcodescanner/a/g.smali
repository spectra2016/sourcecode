.class public Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/g;
.super Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;
.source "CenterCropStrategy.java"
.field private static final a:Ljava/lang/String;
.method static constructor <clinit>()V
.registers 1
const-class v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/g;
invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
move-result-object v0
sput-object v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/g;->a:Ljava/lang/String;
return-void
.end method
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/l;-><init>()V
return-void
.end method
.method protected a(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)F
.registers 10
const/high16 v6, 0x3f80
iget v0, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I
if-lez v0, :cond_a
iget v0, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I
if-gtz v0, :cond_c
:cond_a
const/4 v0, 0x0
:goto_b
return v0
:cond_c
invoke-virtual {p1, p2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
move-result-object v1
iget v0, v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I
int-to-float v0, v0
mul-float/2addr v0, v6
iget v2, p1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I
int-to-float v2, v2
div-float/2addr v0, v2
cmpl-float v2, v0, v6
if-lez v2, :cond_29
div-float v0, v6, v0
float-to-double v2, v0
const-wide v4, 0x3ff199999999999aL
invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D
move-result-wide v2
double-to-float v0, v2
:cond_29
iget v2, v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I
int-to-float v2, v2
mul-float/2addr v2, v6
iget v3, p2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I
int-to-float v3, v3
div-float/2addr v2, v3
iget v1, v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I
int-to-float v1, v1
mul-float/2addr v1, v6
iget v3, p2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I
int-to-float v3, v3
div-float/2addr v1, v3
add-float/2addr v1, v2
div-float v2, v6, v1
div-float v1, v2, v1
mul-float/2addr v0, v1
goto :goto_b
.end method
.method public b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Landroid/graphics/Rect;
.registers 10
invoke-virtual {p1, p2}, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b(Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;)Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
move-result-object v0
sget-object v1, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/g;->a:Ljava/lang/String;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Preview: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, "; Scaled: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, "; Want: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
iget v1, v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I
iget v2, p2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I
sub-int/2addr v1, v2
div-int/lit8 v1, v1, 0x2
iget v2, v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I
iget v3, p2, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I
sub-int/2addr v2, v3
div-int/lit8 v2, v2, 0x2
new-instance v3, Landroid/graphics/Rect;
neg-int v4, v1
neg-int v5, v2
iget v6, v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->a:I
sub-int v1, v6, v1
iget v0, v0, Lin/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;->b:I
sub-int/2addr v0, v2
invoke-direct {v3, v4, v5, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V
return-object v3
.end method