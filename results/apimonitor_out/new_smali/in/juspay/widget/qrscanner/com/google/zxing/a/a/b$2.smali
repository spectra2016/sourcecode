.class  Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b$2;
.super Ljava/lang/Object;
.source "BeepManager.java"
.implements Landroid/media/MediaPlayer$OnErrorListener;
.field final synthetic a:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;
.method constructor <init>(Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;)V
.registers 2
iput-object p1, p0, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b$2;->a:Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public onError(Landroid/media/MediaPlayer;II)Z
.registers 7
invoke-static {}, Lin/juspay/widget/qrscanner/com/google/zxing/a/a/b;->b()Ljava/lang/String;
move-result-object v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Failed to beep "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, ", "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
invoke-virtual {p1}, Landroid/media/MediaPlayer;->stop()V
invoke-virtual {p1}, Landroid/media/MediaPlayer;->release()V
const/4 v0, 0x1
return v0
.end method