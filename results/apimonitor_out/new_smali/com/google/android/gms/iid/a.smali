.class public Lcom/google/android/gms/iid/a;
.super Ljava/lang/Object;
.field static a:Ljava/util/Map;
.field static f:Ljava/lang/String;
.field private static g:Lcom/google/android/gms/iid/f;
.field private static h:Lcom/google/android/gms/iid/e;
.field  b:Landroid/content/Context;
.field  c:Ljava/security/KeyPair;
.field  d:Ljava/lang/String;
.field  e:J
.method static constructor <clinit>()V
.registers 1
new-instance v0, Ljava/util/HashMap;
invoke-direct {v0}, Ljava/util/HashMap;-><init>()V
sput-object v0, Lcom/google/android/gms/iid/a;->a:Ljava/util/Map;
return-void
.end method
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V
.registers 5
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const-string v0, ""
iput-object v0, p0, Lcom/google/android/gms/iid/a;->d:Ljava/lang/String;
invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
move-result-object v0
iput-object v0, p0, Lcom/google/android/gms/iid/a;->b:Landroid/content/Context;
iput-object p2, p0, Lcom/google/android/gms/iid/a;->d:Ljava/lang/String;
return-void
.end method
.method static a(Landroid/content/Context;)I
.registers 6
const/4 v0, 0x0
:try_start_1
invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v1
invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v2
const/4 v3, 0x0
invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
move-result-object v1
iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
:goto_10
:try_end_10
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_10} :catch_11
return v0
:catch_11
move-exception v1
const-string v2, "InstanceID"
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "Never happens: can\'t find own package "
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_10
.end method
.method public static declared-synchronized a(Landroid/content/Context;Landroid/os/Bundle;)Lcom/google/android/gms/iid/a;
.registers 6
const-class v2, Lcom/google/android/gms/iid/a;
monitor-enter v2
if-nez p1, :cond_42
:try_start_5
const-string v0, ""
:goto_7
if-nez v0, :cond_4c
const-string v0, ""
move-object v1, v0
:goto_c
invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
move-result-object v3
sget-object v0, Lcom/google/android/gms/iid/a;->g:Lcom/google/android/gms/iid/f;
if-nez v0, :cond_22
new-instance v0, Lcom/google/android/gms/iid/f;
invoke-direct {v0, v3}, Lcom/google/android/gms/iid/f;-><init>(Landroid/content/Context;)V
sput-object v0, Lcom/google/android/gms/iid/a;->g:Lcom/google/android/gms/iid/f;
new-instance v0, Lcom/google/android/gms/iid/e;
invoke-direct {v0, v3}, Lcom/google/android/gms/iid/e;-><init>(Landroid/content/Context;)V
sput-object v0, Lcom/google/android/gms/iid/a;->h:Lcom/google/android/gms/iid/e;
:cond_22
invoke-static {v3}, Lcom/google/android/gms/iid/a;->a(Landroid/content/Context;)I
move-result v0
invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
move-result-object v0
sput-object v0, Lcom/google/android/gms/iid/a;->f:Ljava/lang/String;
sget-object v0, Lcom/google/android/gms/iid/a;->a:Ljava/util/Map;
invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Lcom/google/android/gms/iid/a;
if-nez v0, :cond_40
new-instance v0, Lcom/google/android/gms/iid/a;
invoke-direct {v0, v3, v1, p1}, Lcom/google/android/gms/iid/a;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V
sget-object v3, Lcom/google/android/gms/iid/a;->a:Ljava/util/Map;
invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:try_end_40
.catchall {:try_start_5 .. :try_end_40} :catchall_49
:cond_40
monitor-exit v2
return-object v0
:cond_42
:try_start_42
const-string v0, "subtype"
invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
:try_end_47
.catchall {:try_start_42 .. :try_end_47} :catchall_49
move-result-object v0
goto :goto_7
:catchall_49
move-exception v0
monitor-exit v2
throw v0
:cond_4c
move-object v1, v0
goto :goto_c
.end method
.method static a(Ljava/security/KeyPair;)Ljava/lang/String;
.registers 5
invoke-virtual {p0}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;
move-result-object v0
invoke-interface {v0}, Ljava/security/PublicKey;->getEncoded()[B
move-result-object v0
:try_start_8
const-string v1, "SHA1"
invoke-static {v1}, Ldroidbox/java/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
move-result-object v1
invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->digest([B)[B
move-result-object v0
const/4 v1, 0x0
aget-byte v1, v0, v1
and-int/lit8 v1, v1, 0xf
add-int/lit8 v1, v1, 0x70
const/4 v2, 0x0
and-int/lit16 v1, v1, 0xff
int-to-byte v1, v1
aput-byte v1, v0, v2
const/4 v1, 0x0
const/16 v2, 0x8
const/16 v3, 0xb
invoke-static {v0, v1, v2, v3}, Landroid/util/Base64;->encodeToString([BIII)Ljava/lang/String;
:try_end_27
.catch Ljava/security/NoSuchAlgorithmException; {:try_start_8 .. :try_end_27} :catch_29
move-result-object v0
:goto_28
return-object v0
:catch_29
move-exception v0
const-string v0, "InstanceID"
const-string v1, "Unexpected error, device missing required alghorithms"
invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
const/4 v0, 0x0
goto :goto_28
.end method
.method static a([B)Ljava/lang/String;
.registers 2
const/16 v0, 0xb
invoke-static {p0, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public static b(Landroid/content/Context;)Lcom/google/android/gms/iid/a;
.registers 2
const/4 v0, 0x0
invoke-static {p0, v0}, Lcom/google/android/gms/iid/a;->a(Landroid/content/Context;Landroid/os/Bundle;)Lcom/google/android/gms/iid/a;
move-result-object v0
return-object v0
.end method
.method public a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
.registers 10
const/4 v1, 0x0
invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;
move-result-object v0
invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;
move-result-object v2
if-ne v0, v2, :cond_13
new-instance v0, Ljava/io/IOException;
const-string v1, "MAIN_THREAD"
invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V
throw v0
:cond_13
const/4 v0, 0x1
invoke-virtual {p0}, Lcom/google/android/gms/iid/a;->e()Z
move-result v2
if-eqz v2, :cond_1e
const/4 v4, 0x0
:goto_1b
if-eqz v4, :cond_27
:goto_1d
:cond_1d
return-object v4
:cond_1e
sget-object v2, Lcom/google/android/gms/iid/a;->g:Lcom/google/android/gms/iid/f;
iget-object v3, p0, Lcom/google/android/gms/iid/a;->d:Ljava/lang/String;
invoke-virtual {v2, v3, p1, p2}, Lcom/google/android/gms/iid/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v4
goto :goto_1b
:cond_27
if-nez p3, :cond_2e
new-instance p3, Landroid/os/Bundle;
invoke-direct {p3}, Landroid/os/Bundle;-><init>()V
:cond_2e
const-string v2, "ttl"
invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v2
if-eqz v2, :cond_37
move v0, v1
:cond_37
const-string v2, "jwt"
const-string v3, "type"
invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v2
if-eqz v2, :cond_71
:goto_45
invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/iid/a;->b(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
move-result-object v4
const-string v0, "InstanceID"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "token: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
if-eqz v4, :cond_1d
if-eqz v1, :cond_1d
sget-object v0, Lcom/google/android/gms/iid/a;->g:Lcom/google/android/gms/iid/f;
iget-object v1, p0, Lcom/google/android/gms/iid/a;->d:Ljava/lang/String;
sget-object v5, Lcom/google/android/gms/iid/a;->f:Ljava/lang/String;
move-object v2, p1
move-object v3, p2
invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/iid/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
goto :goto_1d
:cond_71
move v1, v0
goto :goto_45
.end method
.method  a()Ljava/security/KeyPair;
.registers 5
iget-object v0, p0, Lcom/google/android/gms/iid/a;->c:Ljava/security/KeyPair;
if-nez v0, :cond_e
sget-object v0, Lcom/google/android/gms/iid/a;->g:Lcom/google/android/gms/iid/f;
iget-object v1, p0, Lcom/google/android/gms/iid/a;->d:Ljava/lang/String;
invoke-virtual {v0, v1}, Lcom/google/android/gms/iid/f;->c(Ljava/lang/String;)Ljava/security/KeyPair;
move-result-object v0
iput-object v0, p0, Lcom/google/android/gms/iid/a;->c:Ljava/security/KeyPair;
:cond_e
iget-object v0, p0, Lcom/google/android/gms/iid/a;->c:Ljava/security/KeyPair;
if-nez v0, :cond_24
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v0
iput-wide v0, p0, Lcom/google/android/gms/iid/a;->e:J
sget-object v0, Lcom/google/android/gms/iid/a;->g:Lcom/google/android/gms/iid/f;
iget-object v1, p0, Lcom/google/android/gms/iid/a;->d:Ljava/lang/String;
iget-wide v2, p0, Lcom/google/android/gms/iid/a;->e:J
invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/iid/f;->a(Ljava/lang/String;J)Ljava/security/KeyPair;
move-result-object v0
iput-object v0, p0, Lcom/google/android/gms/iid/a;->c:Ljava/security/KeyPair;
:cond_24
iget-object v0, p0, Lcom/google/android/gms/iid/a;->c:Ljava/security/KeyPair;
return-object v0
.end method
.method public b(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
.registers 6
if-eqz p2, :cond_7
const-string v0, "scope"
invoke-virtual {p3, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
:cond_7
const-string v0, "sender"
invoke-virtual {p3, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
const-string v0, ""
iget-object v1, p0, Lcom/google/android/gms/iid/a;->d:Ljava/lang/String;
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_44
move-object v0, p1
:goto_17
const-string v1, "legacy.register"
invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z
move-result v1
if-nez v1, :cond_33
const-string v1, "subscription"
invoke-virtual {p3, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "subtype"
invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "X-subscription"
invoke-virtual {p3, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "X-subtype"
invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
:cond_33
sget-object v0, Lcom/google/android/gms/iid/a;->h:Lcom/google/android/gms/iid/e;
invoke-virtual {p0}, Lcom/google/android/gms/iid/a;->a()Ljava/security/KeyPair;
move-result-object v1
invoke-virtual {v0, p3, v1}, Lcom/google/android/gms/iid/e;->a(Landroid/os/Bundle;Ljava/security/KeyPair;)Landroid/content/Intent;
move-result-object v0
sget-object v1, Lcom/google/android/gms/iid/a;->h:Lcom/google/android/gms/iid/e;
invoke-virtual {v1, v0}, Lcom/google/android/gms/iid/e;->b(Landroid/content/Intent;)Ljava/lang/String;
move-result-object v0
return-object v0
:cond_44
iget-object v0, p0, Lcom/google/android/gms/iid/a;->d:Ljava/lang/String;
goto :goto_17
.end method
.method  b()V
.registers 3
const-wide/16 v0, 0x0
iput-wide v0, p0, Lcom/google/android/gms/iid/a;->e:J
sget-object v0, Lcom/google/android/gms/iid/a;->g:Lcom/google/android/gms/iid/f;
iget-object v1, p0, Lcom/google/android/gms/iid/a;->d:Ljava/lang/String;
invoke-virtual {v0, v1}, Lcom/google/android/gms/iid/f;->d(Ljava/lang/String;)V
const/4 v0, 0x0
iput-object v0, p0, Lcom/google/android/gms/iid/a;->c:Ljava/security/KeyPair;
return-void
.end method
.method  c()Lcom/google/android/gms/iid/f;
.registers 2
sget-object v0, Lcom/google/android/gms/iid/a;->g:Lcom/google/android/gms/iid/f;
return-object v0
.end method
.method  d()Lcom/google/android/gms/iid/e;
.registers 2
sget-object v0, Lcom/google/android/gms/iid/a;->h:Lcom/google/android/gms/iid/e;
return-object v0
.end method
.method  e()Z
.registers 7
const/4 v0, 0x1
sget-object v1, Lcom/google/android/gms/iid/a;->g:Lcom/google/android/gms/iid/f;
const-string v2, "appVersion"
invoke-virtual {v1, v2}, Lcom/google/android/gms/iid/f;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
if-eqz v1, :cond_13
sget-object v2, Lcom/google/android/gms/iid/a;->f:Ljava/lang/String;
invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_14
:cond_13
:goto_13
return v0
:cond_14
sget-object v1, Lcom/google/android/gms/iid/a;->g:Lcom/google/android/gms/iid/f;
const-string v2, "lastToken"
invoke-virtual {v1, v2}, Lcom/google/android/gms/iid/f;->a(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
if-eqz v1, :cond_13
invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
move-result-wide v2
invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v1
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v2
const-wide/16 v4, 0x3e8
div-long/2addr v2, v4
invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
move-result-wide v4
sub-long/2addr v2, v4
const-wide/32 v4, 0x93a80
cmp-long v1, v2, v4
if-gtz v1, :cond_13
const/4 v0, 0x0
goto :goto_13
.end method