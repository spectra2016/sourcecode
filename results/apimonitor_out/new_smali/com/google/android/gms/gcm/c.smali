.class public Lcom/google/android/gms/gcm/c;
.super Ljava/lang/Object;
.field public static a:I
.field public static b:I
.field public static c:I
.field static d:Lcom/google/android/gms/gcm/c;
.field private static final i:Ljava/util/concurrent/atomic/AtomicInteger;
.field final e:Landroid/os/Messenger;
.field private f:Landroid/content/Context;
.field private g:Landroid/app/PendingIntent;
.field private h:Ljava/util/Map;
.field private final j:Ljava/util/concurrent/BlockingQueue;
.method static constructor <clinit>()V
.registers 2
const v0, 0x4c4b40
sput v0, Lcom/google/android/gms/gcm/c;->a:I
const v0, 0x632ea0
sput v0, Lcom/google/android/gms/gcm/c;->b:I
const v0, 0x6acfc0
sput v0, Lcom/google/android/gms/gcm/c;->c:I
new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;
const/4 v1, 0x1
invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V
sput-object v0, Lcom/google/android/gms/gcm/c;->i:Ljava/util/concurrent/atomic/AtomicInteger;
return-void
.end method
.method public constructor <init>()V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;
invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V
iput-object v0, p0, Lcom/google/android/gms/gcm/c;->j:Ljava/util/concurrent/BlockingQueue;
new-instance v0, Ljava/util/HashMap;
invoke-direct {v0}, Ljava/util/HashMap;-><init>()V
invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;
move-result-object v0
iput-object v0, p0, Lcom/google/android/gms/gcm/c;->h:Ljava/util/Map;
new-instance v0, Landroid/os/Messenger;
new-instance v1, Lcom/google/android/gms/gcm/c$1;
invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;
move-result-object v2
invoke-direct {v1, p0, v2}, Lcom/google/android/gms/gcm/c$1;-><init>(Lcom/google/android/gms/gcm/c;Landroid/os/Looper;)V
invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V
iput-object v0, p0, Lcom/google/android/gms/gcm/c;->e:Landroid/os/Messenger;
return-void
.end method
.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/gcm/c;
.registers 4
const-class v1, Lcom/google/android/gms/gcm/c;
monitor-enter v1
:try_start_3
sget-object v0, Lcom/google/android/gms/gcm/c;->d:Lcom/google/android/gms/gcm/c;
if-nez v0, :cond_16
new-instance v0, Lcom/google/android/gms/gcm/c;
invoke-direct {v0}, Lcom/google/android/gms/gcm/c;-><init>()V
sput-object v0, Lcom/google/android/gms/gcm/c;->d:Lcom/google/android/gms/gcm/c;
sget-object v0, Lcom/google/android/gms/gcm/c;->d:Lcom/google/android/gms/gcm/c;
invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
move-result-object v2
iput-object v2, v0, Lcom/google/android/gms/gcm/c;->f:Landroid/content/Context;
:cond_16
sget-object v0, Lcom/google/android/gms/gcm/c;->d:Lcom/google/android/gms/gcm/c;
:try_end_18
.catchall {:try_start_3 .. :try_end_18} :catchall_1a
monitor-exit v1
return-object v0
:catchall_1a
move-exception v0
monitor-exit v1
throw v0
.end method
.method static synthetic a(Lcom/google/android/gms/gcm/c;)Ljava/util/concurrent/BlockingQueue;
.registers 2
iget-object v0, p0, Lcom/google/android/gms/gcm/c;->j:Ljava/util/concurrent/BlockingQueue;
return-object v0
.end method
.method private a(Ljava/lang/String;Ljava/lang/String;JILandroid/os/Bundle;)V
.registers 14
if-nez p1, :cond_a
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "Missing \'to\'"
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_a
new-instance v0, Landroid/content/Intent;
const-string v1, "com.google.android.gcm.intent.SEND"
invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
if-eqz p6, :cond_16
invoke-virtual {v0, p6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
:cond_16
invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/c;->a(Landroid/content/Intent;)V
iget-object v1, p0, Lcom/google/android/gms/gcm/c;->f:Landroid/content/Context;
invoke-static {v1}, Lcom/google/android/gms/gcm/c;->b(Landroid/content/Context;)Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
const-string v1, "google.to"
invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
const-string v1, "google.message_id"
invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
const-string v1, "google.ttl"
invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;
move-result-object v2
invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
const-string v1, "google.delay"
invoke-static {p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
move-result-object v2
invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
iget-object v1, p0, Lcom/google/android/gms/gcm/c;->f:Landroid/content/Context;
invoke-static {v1}, Lcom/google/android/gms/gcm/c;->b(Landroid/content/Context;)Ljava/lang/String;
move-result-object v1
const-string v2, ".gsf"
invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
move-result v1
if-eqz v1, :cond_9f
new-instance v2, Landroid/os/Bundle;
invoke-direct {v2}, Landroid/os/Bundle;-><init>()V
invoke-virtual {p6}, Landroid/os/Bundle;->keySet()Ljava/util/Set;
move-result-object v0
invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
move-result-object v3
:cond_59
:goto_59
invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_87
invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
invoke-virtual {p6, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v1
instance-of v4, v1, Ljava/lang/String;
if-eqz v4, :cond_59
new-instance v4, Ljava/lang/StringBuilder;
invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
const-string v5, "gcm."
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v4
move-object v0, v1
check-cast v0, Ljava/lang/String;
invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
goto :goto_59
:cond_87
const-string v0, "google.to"
invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
const-string v0, "google.message_id"
invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
iget-object v0, p0, Lcom/google/android/gms/gcm/c;->f:Landroid/content/Context;
invoke-static {v0}, Lcom/google/android/gms/iid/a;->b(Landroid/content/Context;)Lcom/google/android/gms/iid/a;
move-result-object v0
const-string v1, "GCM"
const-string v3, "upstream"
invoke-virtual {v0, v1, v3, v2}, Lcom/google/android/gms/iid/a;->b(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
:goto_9e
return-void
:cond_9f
iget-object v1, p0, Lcom/google/android/gms/gcm/c;->f:Landroid/content/Context;
const-string v2, "com.google.android.gtalkservice.permission.GTALK_SERVICE"
invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
goto :goto_9e
.end method
.method static synthetic a(Lcom/google/android/gms/gcm/c;Landroid/content/Intent;)Z
.registers 3
invoke-direct {p0, p1}, Lcom/google/android/gms/gcm/c;->b(Landroid/content/Intent;)Z
move-result v0
return v0
.end method
.method static synthetic b(Lcom/google/android/gms/gcm/c;)Landroid/content/Context;
.registers 2
iget-object v0, p0, Lcom/google/android/gms/gcm/c;->f:Landroid/content/Context;
return-object v0
.end method
.method public static b(Landroid/content/Context;)Ljava/lang/String;
.registers 2
invoke-static {p0}, Lcom/google/android/gms/iid/e;->a(Landroid/content/Context;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method private b(Landroid/content/Intent;)Z
.registers 4
const-string v0, "In-Reply-To"
invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
if-nez v0, :cond_16
const-string v1, "error"
invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z
move-result v1
if-eqz v1, :cond_16
const-string v0, "google.message_id"
invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
:cond_16
if-eqz v0, :cond_2d
iget-object v1, p0, Lcom/google/android/gms/gcm/c;->h:Ljava/util/Map;
invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/os/Handler;
if-eqz v0, :cond_2d
invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;
move-result-object v1
iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;
invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
move-result v0
:goto_2c
return v0
:cond_2d
const/4 v0, 0x0
goto :goto_2c
.end method
.method public static c(Landroid/content/Context;)I
.registers 4
invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v0
:try_start_4
invoke-static {p0}, Lcom/google/android/gms/gcm/c;->b(Landroid/content/Context;)Ljava/lang/String;
move-result-object v1
const/4 v2, 0x0
invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
move-result-object v0
iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
:goto_f
:try_end_f
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_f} :catch_10
return v0
:catch_10
move-exception v0
const/4 v0, -0x1
goto :goto_f
.end method
.method declared-synchronized a(Landroid/content/Intent;)V
.registers 6
monitor-enter p0
:try_start_1
iget-object v0, p0, Lcom/google/android/gms/gcm/c;->g:Landroid/app/PendingIntent;
if-nez v0, :cond_19
new-instance v0, Landroid/content/Intent;
invoke-direct {v0}, Landroid/content/Intent;-><init>()V
const-string v1, "com.google.example.invalidpackage"
invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
iget-object v1, p0, Lcom/google/android/gms/gcm/c;->f:Landroid/content/Context;
const/4 v2, 0x0
const/4 v3, 0x0
invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
move-result-object v0
iput-object v0, p0, Lcom/google/android/gms/gcm/c;->g:Landroid/app/PendingIntent;
:cond_19
const-string v0, "app"
iget-object v1, p0, Lcom/google/android/gms/gcm/c;->g:Landroid/app/PendingIntent;
invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
:try_end_20
.catchall {:try_start_1 .. :try_end_20} :catchall_22
monitor-exit p0
return-void
:catchall_22
move-exception v0
monitor-exit p0
throw v0
.end method
.method public a(Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;)V
.registers 15
const/4 v6, -0x1
move-object v1, p0
move-object v2, p1
move-object v3, p2
move-wide v4, p3
move-object v7, p5
invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/gcm/c;->a(Ljava/lang/String;Ljava/lang/String;JILandroid/os/Bundle;)V
return-void
.end method