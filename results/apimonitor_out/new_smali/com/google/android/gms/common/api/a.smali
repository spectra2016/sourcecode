.class public final Lcom/google/android/gms/common/api/a;
.super Ljava/lang/Object;
.field private final a:Lcom/google/android/gms/common/api/a$a;
.field private final b:Lcom/google/android/gms/common/api/a$c;
.field private final c:Lcom/google/android/gms/common/api/a$b;
.field private final d:Lcom/google/android/gms/common/api/a$d;
.field private final e:Ljava/lang/String;
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/common/api/a$a;Lcom/google/android/gms/common/api/a$b;)V
.registers 6
const/4 v1, 0x0
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const-string v0, "Cannot construct an Api with a null ClientBuilder"
invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
const-string v0, "Cannot construct an Api with a null ClientKey"
invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
iput-object p1, p0, Lcom/google/android/gms/common/api/a;->e:Ljava/lang/String;
iput-object p2, p0, Lcom/google/android/gms/common/api/a;->a:Lcom/google/android/gms/common/api/a$a;
iput-object v1, p0, Lcom/google/android/gms/common/api/a;->b:Lcom/google/android/gms/common/api/a$c;
iput-object p3, p0, Lcom/google/android/gms/common/api/a;->c:Lcom/google/android/gms/common/api/a$b;
iput-object v1, p0, Lcom/google/android/gms/common/api/a;->d:Lcom/google/android/gms/common/api/a$d;
return-void
.end method