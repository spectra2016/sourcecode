.class final Lcom/google/android/gms/common/internal/j$b;
.super Ljava/lang/Object;
.field final synthetic a:Lcom/google/android/gms/common/internal/j;
.field private final b:Lcom/google/android/gms/common/internal/j$b$a;
.field private final c:Ljava/util/Set;
.field private d:I
.field private e:Z
.field private f:Landroid/os/IBinder;
.field private final g:Lcom/google/android/gms/common/internal/j$a;
.field private h:Landroid/content/ComponentName;
.method public constructor <init>(Lcom/google/android/gms/common/internal/j;Lcom/google/android/gms/common/internal/j$a;)V
.registers 4
iput-object p1, p0, Lcom/google/android/gms/common/internal/j$b;->a:Lcom/google/android/gms/common/internal/j;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p2, p0, Lcom/google/android/gms/common/internal/j$b;->g:Lcom/google/android/gms/common/internal/j$a;
new-instance v0, Lcom/google/android/gms/common/internal/j$b$a;
invoke-direct {v0, p0}, Lcom/google/android/gms/common/internal/j$b$a;-><init>(Lcom/google/android/gms/common/internal/j$b;)V
iput-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->b:Lcom/google/android/gms/common/internal/j$b$a;
new-instance v0, Ljava/util/HashSet;
invoke-direct {v0}, Ljava/util/HashSet;-><init>()V
iput-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->c:Ljava/util/Set;
const/4 v0, 0x2
iput v0, p0, Lcom/google/android/gms/common/internal/j$b;->d:I
return-void
.end method
.method static synthetic a(Lcom/google/android/gms/common/internal/j$b;I)I
.registers 2
iput p1, p0, Lcom/google/android/gms/common/internal/j$b;->d:I
return p1
.end method
.method static synthetic a(Lcom/google/android/gms/common/internal/j$b;Landroid/content/ComponentName;)Landroid/content/ComponentName;
.registers 2
iput-object p1, p0, Lcom/google/android/gms/common/internal/j$b;->h:Landroid/content/ComponentName;
return-object p1
.end method
.method static synthetic a(Lcom/google/android/gms/common/internal/j$b;Landroid/os/IBinder;)Landroid/os/IBinder;
.registers 2
iput-object p1, p0, Lcom/google/android/gms/common/internal/j$b;->f:Landroid/os/IBinder;
return-object p1
.end method
.method static synthetic a(Lcom/google/android/gms/common/internal/j$b;)Lcom/google/android/gms/common/internal/j$a;
.registers 2
iget-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->g:Lcom/google/android/gms/common/internal/j$a;
return-object v0
.end method
.method static synthetic b(Lcom/google/android/gms/common/internal/j$b;)Ljava/util/Set;
.registers 2
iget-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->c:Ljava/util/Set;
return-object v0
.end method
.method public a(Landroid/content/ServiceConnection;Ljava/lang/String;)V
.registers 6
iget-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->a:Lcom/google/android/gms/common/internal/j;
invoke-static {v0}, Lcom/google/android/gms/common/internal/j;->c(Lcom/google/android/gms/common/internal/j;)Lcom/google/android/gms/common/stats/b;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/common/internal/j$b;->a:Lcom/google/android/gms/common/internal/j;
invoke-static {v1}, Lcom/google/android/gms/common/internal/j;->b(Lcom/google/android/gms/common/internal/j;)Landroid/content/Context;
move-result-object v1
iget-object v2, p0, Lcom/google/android/gms/common/internal/j$b;->g:Lcom/google/android/gms/common/internal/j$a;
invoke-virtual {v2}, Lcom/google/android/gms/common/internal/j$a;->a()Landroid/content/Intent;
move-result-object v2
invoke-virtual {v0, v1, p1, p2, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;Ljava/lang/String;Landroid/content/Intent;)V
iget-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->c:Ljava/util/Set;
invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
return-void
.end method
.method public a(Ljava/lang/String;)V
.registers 8
const/4 v0, 0x3
iput v0, p0, Lcom/google/android/gms/common/internal/j$b;->d:I
iget-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->a:Lcom/google/android/gms/common/internal/j;
invoke-static {v0}, Lcom/google/android/gms/common/internal/j;->c(Lcom/google/android/gms/common/internal/j;)Lcom/google/android/gms/common/stats/b;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/common/internal/j$b;->a:Lcom/google/android/gms/common/internal/j;
invoke-static {v1}, Lcom/google/android/gms/common/internal/j;->b(Lcom/google/android/gms/common/internal/j;)Landroid/content/Context;
move-result-object v1
iget-object v2, p0, Lcom/google/android/gms/common/internal/j$b;->g:Lcom/google/android/gms/common/internal/j$a;
invoke-virtual {v2}, Lcom/google/android/gms/common/internal/j$a;->a()Landroid/content/Intent;
move-result-object v3
iget-object v4, p0, Lcom/google/android/gms/common/internal/j$b;->b:Lcom/google/android/gms/common/internal/j$b$a;
const/16 v5, 0x81
move-object v2, p1
invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
move-result v0
iput-boolean v0, p0, Lcom/google/android/gms/common/internal/j$b;->e:Z
iget-boolean v0, p0, Lcom/google/android/gms/common/internal/j$b;->e:Z
if-nez v0, :cond_38
const/4 v0, 0x2
iput v0, p0, Lcom/google/android/gms/common/internal/j$b;->d:I
:try_start_27
iget-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->a:Lcom/google/android/gms/common/internal/j;
invoke-static {v0}, Lcom/google/android/gms/common/internal/j;->c(Lcom/google/android/gms/common/internal/j;)Lcom/google/android/gms/common/stats/b;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/common/internal/j$b;->a:Lcom/google/android/gms/common/internal/j;
invoke-static {v1}, Lcom/google/android/gms/common/internal/j;->b(Lcom/google/android/gms/common/internal/j;)Landroid/content/Context;
move-result-object v1
iget-object v2, p0, Lcom/google/android/gms/common/internal/j$b;->b:Lcom/google/android/gms/common/internal/j$b$a;
invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
:goto_38
:try_end_38
.catch Ljava/lang/IllegalArgumentException; {:try_start_27 .. :try_end_38} :catch_39
:cond_38
return-void
:catch_39
move-exception v0
goto :goto_38
.end method
.method public a()Z
.registers 2
iget-boolean v0, p0, Lcom/google/android/gms/common/internal/j$b;->e:Z
return v0
.end method
.method public a(Landroid/content/ServiceConnection;)Z
.registers 3
iget-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->c:Ljava/util/Set;
invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
move-result v0
return v0
.end method
.method public b()I
.registers 2
iget v0, p0, Lcom/google/android/gms/common/internal/j$b;->d:I
return v0
.end method
.method public b(Landroid/content/ServiceConnection;Ljava/lang/String;)V
.registers 5
iget-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->a:Lcom/google/android/gms/common/internal/j;
invoke-static {v0}, Lcom/google/android/gms/common/internal/j;->c(Lcom/google/android/gms/common/internal/j;)Lcom/google/android/gms/common/stats/b;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/common/internal/j$b;->a:Lcom/google/android/gms/common/internal/j;
invoke-static {v1}, Lcom/google/android/gms/common/internal/j;->b(Lcom/google/android/gms/common/internal/j;)Landroid/content/Context;
move-result-object v1
invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/stats/b;->b(Landroid/content/Context;Landroid/content/ServiceConnection;)V
iget-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->c:Ljava/util/Set;
invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
return-void
.end method
.method public b(Ljava/lang/String;)V
.registers 5
iget-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->a:Lcom/google/android/gms/common/internal/j;
invoke-static {v0}, Lcom/google/android/gms/common/internal/j;->c(Lcom/google/android/gms/common/internal/j;)Lcom/google/android/gms/common/stats/b;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/common/internal/j$b;->a:Lcom/google/android/gms/common/internal/j;
invoke-static {v1}, Lcom/google/android/gms/common/internal/j;->b(Lcom/google/android/gms/common/internal/j;)Landroid/content/Context;
move-result-object v1
iget-object v2, p0, Lcom/google/android/gms/common/internal/j$b;->b:Lcom/google/android/gms/common/internal/j$b$a;
invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
const/4 v0, 0x0
iput-boolean v0, p0, Lcom/google/android/gms/common/internal/j$b;->e:Z
const/4 v0, 0x2
iput v0, p0, Lcom/google/android/gms/common/internal/j$b;->d:I
return-void
.end method
.method public c()Z
.registers 2
iget-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->c:Ljava/util/Set;
invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z
move-result v0
return v0
.end method
.method public d()Landroid/os/IBinder;
.registers 2
iget-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->f:Landroid/os/IBinder;
return-object v0
.end method
.method public e()Landroid/content/ComponentName;
.registers 2
iget-object v0, p0, Lcom/google/android/gms/common/internal/j$b;->h:Landroid/content/ComponentName;
return-object v0
.end method