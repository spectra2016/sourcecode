.class public final Lcom/google/android/gms/common/internal/h$g;
.super Lcom/google/android/gms/common/internal/h$a;
.field public final e:Landroid/os/IBinder;
.field final synthetic f:Lcom/google/android/gms/common/internal/h;
.method public constructor <init>(Lcom/google/android/gms/common/internal/h;ILandroid/os/IBinder;Landroid/os/Bundle;)V
.registers 5
iput-object p1, p0, Lcom/google/android/gms/common/internal/h$g;->f:Lcom/google/android/gms/common/internal/h;
invoke-direct {p0, p1, p2, p4}, Lcom/google/android/gms/common/internal/h$a;-><init>(Lcom/google/android/gms/common/internal/h;ILandroid/os/Bundle;)V
iput-object p3, p0, Lcom/google/android/gms/common/internal/h$g;->e:Landroid/os/IBinder;
return-void
.end method
.method protected a(Lcom/google/android/gms/common/ConnectionResult;)V
.registers 3
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$g;->f:Lcom/google/android/gms/common/internal/h;
invoke-static {v0}, Lcom/google/android/gms/common/internal/h;->e(Lcom/google/android/gms/common/internal/h;)Lcom/google/android/gms/common/api/c$c;
move-result-object v0
if-eqz v0, :cond_11
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$g;->f:Lcom/google/android/gms/common/internal/h;
invoke-static {v0}, Lcom/google/android/gms/common/internal/h;->e(Lcom/google/android/gms/common/internal/h;)Lcom/google/android/gms/common/api/c$c;
move-result-object v0
invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/c$c;->a(Lcom/google/android/gms/common/ConnectionResult;)V
:cond_11
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$g;->f:Lcom/google/android/gms/common/internal/h;
invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/h;->a(Lcom/google/android/gms/common/ConnectionResult;)V
return-void
.end method
.method protected a()Z
.registers 6
const/4 v0, 0x0
:try_start_1
iget-object v1, p0, Lcom/google/android/gms/common/internal/h$g;->e:Landroid/os/IBinder;
invoke-interface {v1}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;
:try_end_6
.catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_6} :catch_3c
move-result-object v1
iget-object v2, p0, Lcom/google/android/gms/common/internal/h$g;->f:Lcom/google/android/gms/common/internal/h;
invoke-virtual {v2}, Lcom/google/android/gms/common/internal/h;->b()Ljava/lang/String;
move-result-object v2
invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v2
if-nez v2, :cond_45
const-string v2, "GmsClient"
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "service descriptor mismatch: "
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
iget-object v4, p0, Lcom/google/android/gms/common/internal/h$g;->f:Lcom/google/android/gms/common/internal/h;
invoke-virtual {v4}, Lcom/google/android/gms/common/internal/h;->b()Ljava/lang/String;
move-result-object v4
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
const-string v4, " vs. "
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
:goto_3b
:cond_3b
return v0
:catch_3c
move-exception v1
const-string v1, "GmsClient"
const-string v2, "service probably died"
invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_3b
:cond_45
iget-object v1, p0, Lcom/google/android/gms/common/internal/h$g;->f:Lcom/google/android/gms/common/internal/h;
iget-object v2, p0, Lcom/google/android/gms/common/internal/h$g;->e:Landroid/os/IBinder;
invoke-virtual {v1, v2}, Lcom/google/android/gms/common/internal/h;->a(Landroid/os/IBinder;)Landroid/os/IInterface;
move-result-object v1
if-eqz v1, :cond_3b
iget-object v2, p0, Lcom/google/android/gms/common/internal/h$g;->f:Lcom/google/android/gms/common/internal/h;
const/4 v3, 0x2
const/4 v4, 0x3
invoke-static {v2, v3, v4, v1}, Lcom/google/android/gms/common/internal/h;->a(Lcom/google/android/gms/common/internal/h;IILandroid/os/IInterface;)Z
move-result v1
if-eqz v1, :cond_3b
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$g;->f:Lcom/google/android/gms/common/internal/h;
invoke-virtual {v0}, Lcom/google/android/gms/common/internal/h;->k()Landroid/os/Bundle;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/common/internal/h$g;->f:Lcom/google/android/gms/common/internal/h;
invoke-static {v1}, Lcom/google/android/gms/common/internal/h;->b(Lcom/google/android/gms/common/internal/h;)Lcom/google/android/gms/common/api/c$b;
move-result-object v1
if-eqz v1, :cond_70
iget-object v1, p0, Lcom/google/android/gms/common/internal/h$g;->f:Lcom/google/android/gms/common/internal/h;
invoke-static {v1}, Lcom/google/android/gms/common/internal/h;->b(Lcom/google/android/gms/common/internal/h;)Lcom/google/android/gms/common/api/c$b;
move-result-object v1
invoke-interface {v1, v0}, Lcom/google/android/gms/common/api/c$b;->a(Landroid/os/Bundle;)V
:cond_70
const/4 v0, 0x1
goto :goto_3b
.end method