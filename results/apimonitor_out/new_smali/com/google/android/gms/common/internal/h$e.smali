.class public final Lcom/google/android/gms/common/internal/h$e;
.super Ljava/lang/Object;
.implements Landroid/content/ServiceConnection;
.field final synthetic a:Lcom/google/android/gms/common/internal/h;
.field private final b:I
.method public constructor <init>(Lcom/google/android/gms/common/internal/h;I)V
.registers 3
iput-object p1, p0, Lcom/google/android/gms/common/internal/h$e;->a:Lcom/google/android/gms/common/internal/h;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput p2, p0, Lcom/google/android/gms/common/internal/h$e;->b:I
return-void
.end method
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.registers 5
const-string v0, "Expecting a valid IBinder"
invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$e;->a:Lcom/google/android/gms/common/internal/h;
invoke-static {p2}, Lcom/google/android/gms/common/internal/n$a;->a(Landroid/os/IBinder;)Lcom/google/android/gms/common/internal/n;
move-result-object v1
invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/h;->a(Lcom/google/android/gms/common/internal/h;Lcom/google/android/gms/common/internal/n;)Lcom/google/android/gms/common/internal/n;
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$e;->a:Lcom/google/android/gms/common/internal/h;
iget v1, p0, Lcom/google/android/gms/common/internal/h$e;->b:I
invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/h;->c(I)V
return-void
.end method
.method public onServiceDisconnected(Landroid/content/ComponentName;)V
.registers 7
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$e;->a:Lcom/google/android/gms/common/internal/h;
iget-object v0, v0, Lcom/google/android/gms/common/internal/h;->a:Landroid/os/Handler;
iget-object v1, p0, Lcom/google/android/gms/common/internal/h$e;->a:Lcom/google/android/gms/common/internal/h;
iget-object v1, v1, Lcom/google/android/gms/common/internal/h;->a:Landroid/os/Handler;
const/4 v2, 0x4
iget v3, p0, Lcom/google/android/gms/common/internal/h$e;->b:I
const/4 v4, 0x1
invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
return-void
.end method