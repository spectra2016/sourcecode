.class public final Lcom/google/android/gms/common/GooglePlayServicesUtil;
.super Ljava/lang/Object;
.field public static final GMS_ERROR_DIALOG:Ljava/lang/String; = "GooglePlayServicesErrorDialog"
.field public static final GOOGLE_PLAY_SERVICES_PACKAGE:Ljava/lang/String; = "com.google.android.gms"
.field public static final GOOGLE_PLAY_SERVICES_VERSION_CODE:I = 0x0
.field public static final GOOGLE_PLAY_STORE_PACKAGE:Ljava/lang/String; = "com.android.vending"
.field public static zzaee:Z
.field public static zzaef:Z
.field private static zzaeg:I
.field private static zzaeh:Ljava/lang/String;
.field private static zzaei:Ljava/lang/Integer;
.field static final zzaej:Ljava/util/concurrent/atomic/AtomicBoolean;
.field private static final zzaek:Ljava/util/concurrent/atomic/AtomicBoolean;
.field private static final zzqf:Ljava/lang/Object;
.method static constructor <clinit>()V
.registers 3
const/4 v2, 0x0
const/4 v1, 0x0
invoke-static {}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzov()I
move-result v0
sput v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->GOOGLE_PLAY_SERVICES_VERSION_CODE:I
sput-boolean v1, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaee:Z
sput-boolean v1, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaef:Z
const/4 v0, -0x1
sput v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaeg:I
new-instance v0, Ljava/lang/Object;
invoke-direct {v0}, Ljava/lang/Object;-><init>()V
sput-object v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzqf:Ljava/lang/Object;
sput-object v2, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaeh:Ljava/lang/String;
sput-object v2, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaei:Ljava/lang/Integer;
new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;
invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V
sput-object v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaej:Ljava/util/concurrent/atomic/AtomicBoolean;
new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;
invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V
sput-object v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaek:Ljava/util/concurrent/atomic/AtomicBoolean;
return-void
.end method
.method private constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public static getErrorDialog(ILandroid/app/Activity;I)Landroid/app/Dialog;
.registers 4
const/4 v0, 0x0
invoke-static {p0, p1, p2, v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getErrorDialog(ILandroid/app/Activity;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/app/Dialog;
move-result-object v0
return-object v0
.end method
.method public static getErrorDialog(ILandroid/app/Activity;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/app/Dialog;
.registers 5
const/4 v0, 0x0
invoke-static {p0, p1, v0, p2, p3}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zza(ILandroid/app/Activity;Landroid/support/v4/app/Fragment;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/app/Dialog;
move-result-object v0
return-object v0
.end method
.method public static getErrorPendingIntent(ILandroid/content/Context;I)Landroid/app/PendingIntent;
.registers 4
invoke-static {}, Lcom/google/android/gms/common/b;->a()Lcom/google/android/gms/common/b;
move-result-object v0
invoke-virtual {v0, p1, p0, p2}, Lcom/google/android/gms/common/b;->a(Landroid/content/Context;II)Landroid/app/PendingIntent;
move-result-object v0
return-object v0
.end method
.method public static getErrorString(I)Ljava/lang/String;
.registers 2
invoke-static {p0}, Lcom/google/android/gms/common/ConnectionResult;->a(I)Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public static getOpenSourceSoftwareLicenseInfo(Landroid/content/Context;)Ljava/lang/String;
.registers 5
const/4 v1, 0x0
new-instance v0, Landroid/net/Uri$Builder;
invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V
const-string v2, "android.resource"
invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;
move-result-object v0
const-string v2, "com.google.android.gms"
invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;
move-result-object v0
const-string v2, "raw"
invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
move-result-object v0
const-string v2, "oss_notice"
invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
move-result-object v0
invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
move-result-object v0
:try_start_22
invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
move-result-object v2
invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
:try_end_29
.catch Ljava/lang/Exception; {:try_start_22 .. :try_end_29} :catch_4e
move-result-object v2
:try_start_2a
new-instance v0, Ljava/util/Scanner;
invoke-direct {v0, v2}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;)V
const-string v3, "\\A"
invoke-virtual {v0, v3}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;
move-result-object v0
invoke-virtual {v0}, Ljava/util/Scanner;->next()Ljava/lang/String;
:try_end_38
.catchall {:try_start_2a .. :try_end_38} :catchall_47
.catch Ljava/util/NoSuchElementException; {:try_start_2a .. :try_end_38} :catch_3f
move-result-object v0
if-eqz v2, :cond_3e
:try_start_3b
invoke-virtual {v2}, Ljava/io/InputStream;->close()V
:goto_3e
:cond_3e
return-object v0
:catch_3f
move-exception v0
if-eqz v2, :cond_45
invoke-virtual {v2}, Ljava/io/InputStream;->close()V
:cond_45
move-object v0, v1
goto :goto_3e
:catchall_47
move-exception v0
if-eqz v2, :cond_4d
invoke-virtual {v2}, Ljava/io/InputStream;->close()V
:cond_4d
throw v0
:try_end_4e
.catch Ljava/lang/Exception; {:try_start_3b .. :try_end_4e} :catch_4e
:catch_4e
move-exception v0
move-object v0, v1
goto :goto_3e
.end method
.method public static getRemoteContext(Landroid/content/Context;)Landroid/content/Context;
.registers 3
:try_start_0
const-string v0, "com.google.android.gms"
const/4 v1, 0x3
invoke-virtual {p0, v0, v1}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
:try_end_6
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_6} :catch_8
move-result-object v0
:goto_7
return-object v0
:catch_8
move-exception v0
const/4 v0, 0x0
goto :goto_7
.end method
.method public static getRemoteResource(Landroid/content/Context;)Landroid/content/res/Resources;
.registers 3
:try_start_0
invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v0
const-string v1, "com.google.android.gms"
invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
:try_end_9
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_9} :catch_b
move-result-object v0
:goto_a
return-object v0
:catch_b
move-exception v0
const/4 v0, 0x0
goto :goto_a
.end method
.method public static isGooglePlayServicesAvailable(Landroid/content/Context;)I
.registers 10
const/4 v2, 0x1
const/16 v0, 0x9
const/4 v1, 0x0
sget-boolean v3, Lcom/google/android/gms/common/internal/c;->a:Z
if-eqz v3, :cond_a
move v0, v1
:goto_9
return v0
:cond_a
invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v3
:try_start_e
invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v4
sget v5, Lcom/google/android/gms/a$b;->common_google_play_services_unknown_issue:I
invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
:goto_17
:try_end_17
.catch Ljava/lang/Throwable; {:try_start_e .. :try_end_17} :catch_48
const-string v4, "com.google.android.gms"
invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v5
invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v4
if-nez v4, :cond_26
invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzak(Landroid/content/Context;)V
:cond_26
:try_start_26
const-string v4, "com.google.android.gms"
const/16 v5, 0x40
invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
:try_end_2d
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_26 .. :try_end_2d} :catch_51
move-result-object v4
invoke-static {}, Lcom/google/android/gms/common/g;->a()Lcom/google/android/gms/common/g;
move-result-object v5
invoke-static {p0}, Lcom/google/android/gms/internal/c;->a(Landroid/content/Context;)Z
move-result v6
if-eqz v6, :cond_5b
sget-object v6, Lcom/google/android/gms/common/f$bj;->a:[Lcom/google/android/gms/common/f$a;
invoke-virtual {v5, v4, v6}, Lcom/google/android/gms/common/g;->a(Landroid/content/pm/PackageInfo;[Lcom/google/android/gms/common/f$a;)Lcom/google/android/gms/common/f$a;
move-result-object v5
if-nez v5, :cond_91
const-string v1, "GooglePlayServicesUtil"
const-string v2, "Google Play services signature invalid."
invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_9
:catch_48
move-exception v4
const-string v4, "GooglePlayServicesUtil"
const-string v5, "The Google Play services resources were not found. Check your project configuration to ensure that the resources are included."
invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_17
:catch_51
move-exception v0
const-string v0, "GooglePlayServicesUtil"
const-string v1, "Google Play services is missing."
invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
move v0, v2
goto :goto_9
:try_start_5b
:cond_5b
const-string v6, "com.android.vending"
const/16 v7, 0x2040
invoke-virtual {v3, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
move-result-object v6
sget-object v7, Lcom/google/android/gms/common/f$bj;->a:[Lcom/google/android/gms/common/f$a;
invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/common/g;->a(Landroid/content/pm/PackageInfo;[Lcom/google/android/gms/common/f$a;)Lcom/google/android/gms/common/f$a;
move-result-object v6
if-nez v6, :cond_7c
const-string v1, "GooglePlayServicesUtil"
const-string v2, "Google Play Store signature invalid."
invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
:try_end_72
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5b .. :try_end_72} :catch_73
goto :goto_9
:catch_73
move-exception v1
const-string v1, "GooglePlayServicesUtil"
const-string v2, "Google Play Store is neither installed nor updating."
invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_9
:cond_7c
const/4 v7, 0x1
:try_start_7d
new-array v7, v7, [Lcom/google/android/gms/common/f$a;
const/4 v8, 0x0
aput-object v6, v7, v8
invoke-virtual {v5, v4, v7}, Lcom/google/android/gms/common/g;->a(Landroid/content/pm/PackageInfo;[Lcom/google/android/gms/common/f$a;)Lcom/google/android/gms/common/f$a;
move-result-object v5
if-nez v5, :cond_91
const-string v1, "GooglePlayServicesUtil"
const-string v2, "Google Play services signature invalid."
invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
:try_end_8f
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7d .. :try_end_8f} :catch_73
goto/16 :goto_9
:cond_91
sget v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->GOOGLE_PLAY_SERVICES_VERSION_CODE:I
invoke-static {v0}, Lcom/google/android/gms/internal/c;->a(I)I
move-result v0
iget v5, v4, Landroid/content/pm/PackageInfo;->versionCode:I
invoke-static {v5}, Lcom/google/android/gms/internal/c;->a(I)I
move-result v5
if-ge v5, v0, :cond_c8
const-string v0, "GooglePlayServicesUtil"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Google Play services out of date.  Requires "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
sget v2, Lcom/google/android/gms/common/GooglePlayServicesUtil;->GOOGLE_PLAY_SERVICES_VERSION_CODE:I
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
const-string v2, " but found "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget v2, v4, Landroid/content/pm/PackageInfo;->versionCode:I
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
const/4 v0, 0x2
goto/16 :goto_9
:cond_c8
iget-object v0, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;
if-nez v0, :cond_d3
:try_start_cc
const-string v0, "com.google.android.gms"
const/4 v4, 0x0
invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
:try_end_d2
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_cc .. :try_end_d2} :catch_da
move-result-object v0
:cond_d3
iget-boolean v0, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z
if-nez v0, :cond_e5
const/4 v0, 0x3
goto/16 :goto_9
:catch_da
move-exception v0
const-string v1, "GooglePlayServicesUtil"
const-string v3, "Google Play services missing when getting application info."
invoke-static {v1, v3, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
move v0, v2
goto/16 :goto_9
:cond_e5
move v0, v1
goto/16 :goto_9
.end method
.method public static isUserRecoverableError(I)Z
.registers 2
packed-switch p0, :pswitch_data_8
:pswitch_3
const/4 v0, 0x0
:goto_4
return v0
:pswitch_5
const/4 v0, 0x1
goto :goto_4
nop
:pswitch_data_8
.packed-switch 0x1
:pswitch_5
:pswitch_5
:pswitch_5
:pswitch_3
:pswitch_3
:pswitch_3
:pswitch_3
:pswitch_3
:pswitch_5
.end packed-switch
.end method
.method public static showErrorDialogFragment(ILandroid/app/Activity;I)Z
.registers 4
const/4 v0, 0x0
invoke-static {p0, p1, p2, v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->showErrorDialogFragment(ILandroid/app/Activity;ILandroid/content/DialogInterface$OnCancelListener;)Z
move-result v0
return v0
.end method
.method public static showErrorDialogFragment(ILandroid/app/Activity;ILandroid/content/DialogInterface$OnCancelListener;)Z
.registers 5
const/4 v0, 0x0
invoke-static {p0, p1, v0, p2, p3}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->showErrorDialogFragment(ILandroid/app/Activity;Landroid/support/v4/app/Fragment;ILandroid/content/DialogInterface$OnCancelListener;)Z
move-result v0
return v0
.end method
.method public static showErrorDialogFragment(ILandroid/app/Activity;Landroid/support/v4/app/Fragment;ILandroid/content/DialogInterface$OnCancelListener;)Z
.registers 7
invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zza(ILandroid/app/Activity;Landroid/support/v4/app/Fragment;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/app/Dialog;
move-result-object v0
if-nez v0, :cond_8
const/4 v0, 0x0
:goto_7
return v0
:cond_8
const-string v1, "GooglePlayServicesErrorDialog"
invoke-static {p1, p4, v1, v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zza(Landroid/app/Activity;Landroid/content/DialogInterface$OnCancelListener;Ljava/lang/String;Landroid/app/Dialog;)V
const/4 v0, 0x1
goto :goto_7
.end method
.method public static showErrorNotification(ILandroid/content/Context;)V
.registers 3
invoke-static {p1}, Lcom/google/android/gms/internal/c;->a(Landroid/content/Context;)Z
move-result v0
if-eqz v0, :cond_b
const/4 v0, 0x2
if-ne p0, v0, :cond_b
const/16 p0, 0x2a
:cond_b
invoke-static {p1, p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzd(Landroid/content/Context;I)Z
move-result v0
if-nez v0, :cond_17
invoke-static {p1, p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzf(Landroid/content/Context;I)Z
move-result v0
if-eqz v0, :cond_1b
:cond_17
invoke-static {p1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzal(Landroid/content/Context;)V
:goto_1a
return-void
:cond_1b
invoke-static {p0, p1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zza(ILandroid/content/Context;)V
goto :goto_1a
.end method
.method private static zza(ILandroid/app/Activity;Landroid/support/v4/app/Fragment;ILandroid/content/DialogInterface$OnCancelListener;)Landroid/app/Dialog;
.registers 10
const/4 v0, 0x0
if-nez p0, :cond_4
:goto_3
return-object v0
:cond_4
invoke-static {p1}, Lcom/google/android/gms/internal/c;->a(Landroid/content/Context;)Z
move-result v1
if-eqz v1, :cond_f
const/4 v1, 0x2
if-ne p0, v1, :cond_f
const/16 p0, 0x2a
:cond_f
invoke-static {}, Lcom/google/android/gms/internal/h;->c()Z
move-result v1
if-eqz v1, :cond_3d
new-instance v1, Landroid/util/TypedValue;
invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V
invoke-virtual {p1}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;
move-result-object v2
const v3, 0x1010309
const/4 v4, 0x1
invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z
invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;
move-result-object v2
iget v1, v1, Landroid/util/TypedValue;->resourceId:I
invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;
move-result-object v1
const-string v2, "Theme.Dialog.Alert"
invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-eqz v1, :cond_3d
new-instance v0, Landroid/app/AlertDialog$Builder;
const/4 v1, 0x5
invoke-direct {v0, p1, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V
:cond_3d
if-nez v0, :cond_44
new-instance v0, Landroid/app/AlertDialog$Builder;
invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V
:cond_44
invoke-static {p1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzam(Landroid/content/Context;)Ljava/lang/String;
move-result-object v1
invoke-static {p1, p0, v1}, Lcom/google/android/gms/common/internal/e;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
if-eqz p4, :cond_54
invoke-virtual {v0, p4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;
:cond_54
invoke-static {}, Lcom/google/android/gms/common/b;->a()Lcom/google/android/gms/common/b;
move-result-object v1
const-string v2, "d"
invoke-virtual {v1, p1, p0, v2}, Lcom/google/android/gms/common/b;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;
move-result-object v2
if-nez p2, :cond_7c
new-instance v1, Lcom/google/android/gms/common/internal/f;
invoke-direct {v1, p1, v2, p3}, Lcom/google/android/gms/common/internal/f;-><init>(Landroid/app/Activity;Landroid/content/Intent;I)V
:goto_65
invoke-static {p1, p0}, Lcom/google/android/gms/common/internal/e;->b(Landroid/content/Context;I)Ljava/lang/String;
move-result-object v2
if-eqz v2, :cond_6e
invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
:cond_6e
invoke-static {p1, p0}, Lcom/google/android/gms/common/internal/e;->a(Landroid/content/Context;I)Ljava/lang/String;
move-result-object v1
if-eqz v1, :cond_77
invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
:cond_77
invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;
move-result-object v0
goto :goto_3
:cond_7c
new-instance v1, Lcom/google/android/gms/common/internal/f;
invoke-direct {v1, p2, v2, p3}, Lcom/google/android/gms/common/internal/f;-><init>(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
goto :goto_65
.end method
.method private static zza(ILandroid/content/Context;)V
.registers 3
const/4 v0, 0x0
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zza(ILandroid/content/Context;Ljava/lang/String;)V
return-void
.end method
.method private static zza(ILandroid/content/Context;Ljava/lang/String;)V
.registers 11
const v5, 0x108008a
const/4 v7, 0x0
const/4 v6, 0x1
invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
move-result-object v1
invoke-static {p1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzam(Landroid/content/Context;)Ljava/lang/String;
move-result-object v2
invoke-static {p1, p0}, Lcom/google/android/gms/common/internal/e;->c(Landroid/content/Context;I)Ljava/lang/String;
move-result-object v0
if-nez v0, :cond_19
sget v0, Lcom/google/android/gms/a$b;->common_google_play_services_notification_ticker:I
invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
move-result-object v0
:cond_19
invoke-static {p1, p0, v2}, Lcom/google/android/gms/common/internal/e;->b(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
move-result-object v2
invoke-static {}, Lcom/google/android/gms/common/b;->a()Lcom/google/android/gms/common/b;
move-result-object v3
const-string v4, "n"
invoke-virtual {v3, p1, p0, v7, v4}, Lcom/google/android/gms/common/b;->a(Landroid/content/Context;IILjava/lang/String;)Landroid/app/PendingIntent;
move-result-object v3
invoke-static {p1}, Lcom/google/android/gms/internal/c;->a(Landroid/content/Context;)Z
move-result v4
if-eqz v4, :cond_99
invoke-static {}, Lcom/google/android/gms/internal/h;->d()Z
move-result v4
invoke-static {v4}, Lcom/google/android/gms/common/internal/p;->a(Z)V
new-instance v4, Landroid/app/Notification$Builder;
invoke-direct {v4, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V
sget v5, Lcom/google/android/gms/a$a;->common_ic_googleplayservices:I
invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;
move-result-object v4
const/4 v5, 0x2
invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;
move-result-object v4
invoke-virtual {v4, v6}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;
move-result-object v4
new-instance v5, Landroid/app/Notification$BigTextStyle;
invoke-direct {v5}, Landroid/app/Notification$BigTextStyle;-><init>()V
new-instance v6, Ljava/lang/StringBuilder;
invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v6, " "
invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-virtual {v5, v0}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;
move-result-object v0
invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;
move-result-object v0
sget v2, Lcom/google/android/gms/a$a;->common_full_open_on_phone:I
sget v4, Lcom/google/android/gms/a$b;->common_open_on_phone:I
invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v2, v1, v3}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
move-result-object v0
invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;
move-result-object v0
move-object v1, v0
:goto_7d
invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzbw(I)Z
move-result v0
if-eqz v0, :cond_121
const/16 v0, 0x28c4
sget-object v2, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaej:Ljava/util/concurrent/atomic/AtomicBoolean;
invoke-virtual {v2, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
move v2, v0
:goto_8b
const-string v0, "notification"
invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/app/NotificationManager;
if-eqz p2, :cond_127
invoke-virtual {v0, p2, v2, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V
:goto_98
return-void
:cond_99
sget v4, Lcom/google/android/gms/a$b;->common_google_play_services_notification_ticker:I
invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
move-result-object v1
invoke-static {}, Lcom/google/android/gms/internal/h;->a()Z
move-result v4
if-eqz v4, :cond_f5
new-instance v4, Landroid/app/Notification$Builder;
invoke-direct {v4, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V
invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;
move-result-object v4
invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
move-result-object v0
invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
move-result-object v0
invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
move-result-object v0
invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
move-result-object v0
invoke-virtual {v0, v6}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;
move-result-object v0
invoke-static {}, Lcom/google/android/gms/internal/h;->g()Z
move-result v1
if-eqz v1, :cond_cb
invoke-virtual {v0, v6}, Landroid/app/Notification$Builder;->setLocalOnly(Z)Landroid/app/Notification$Builder;
:cond_cb
invoke-static {}, Lcom/google/android/gms/internal/h;->d()Z
move-result v1
if-eqz v1, :cond_f0
new-instance v1, Landroid/app/Notification$BigTextStyle;
invoke-direct {v1}, Landroid/app/Notification$BigTextStyle;-><init>()V
invoke-virtual {v1, v2}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;
invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;
move-result-object v0
:goto_e1
sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v2, 0x13
if-ne v1, v2, :cond_ee
iget-object v1, v0, Landroid/app/Notification;->extras:Landroid/os/Bundle;
const-string v2, "android.support.localOnly"
invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
:cond_ee
move-object v1, v0
goto :goto_7d
:cond_f0
invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;
move-result-object v0
goto :goto_e1
:cond_f5
new-instance v4, Landroid/support/v4/app/aa$d;
invoke-direct {v4, p1}, Landroid/support/v4/app/aa$d;-><init>(Landroid/content/Context;)V
invoke-virtual {v4, v5}, Landroid/support/v4/app/aa$d;->a(I)Landroid/support/v4/app/aa$d;
move-result-object v4
invoke-virtual {v4, v1}, Landroid/support/v4/app/aa$d;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/aa$d;
move-result-object v1
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v4
invoke-virtual {v1, v4, v5}, Landroid/support/v4/app/aa$d;->a(J)Landroid/support/v4/app/aa$d;
move-result-object v1
invoke-virtual {v1, v6}, Landroid/support/v4/app/aa$d;->a(Z)Landroid/support/v4/app/aa$d;
move-result-object v1
invoke-virtual {v1, v3}, Landroid/support/v4/app/aa$d;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/aa$d;
move-result-object v1
invoke-virtual {v1, v0}, Landroid/support/v4/app/aa$d;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/aa$d;
move-result-object v0
invoke-virtual {v0, v2}, Landroid/support/v4/app/aa$d;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/aa$d;
move-result-object v0
invoke-virtual {v0}, Landroid/support/v4/app/aa$d;->a()Landroid/app/Notification;
move-result-object v0
move-object v1, v0
goto/16 :goto_7d
:cond_121
const v0, 0x9b6d
move v2, v0
goto/16 :goto_8b
:cond_127
invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
goto/16 :goto_98
.end method
.method public static zza(Landroid/app/Activity;Landroid/content/DialogInterface$OnCancelListener;Ljava/lang/String;Landroid/app/Dialog;)V
.registers 6
:try_start_0
instance-of v0, p0, Landroid/support/v4/app/l;
:goto_2
:try_end_2
.catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_2} :catch_12
if-eqz v0, :cond_15
check-cast p0, Landroid/support/v4/app/l;
invoke-virtual {p0}, Landroid/support/v4/app/l;->f()Landroid/support/v4/app/p;
move-result-object v0
invoke-static {p3, p1}, Lcom/google/android/gms/common/c;->a(Landroid/app/Dialog;Landroid/content/DialogInterface$OnCancelListener;)Lcom/google/android/gms/common/c;
move-result-object v1
invoke-virtual {v1, v0, p2}, Lcom/google/android/gms/common/c;->a(Landroid/support/v4/app/p;Ljava/lang/String;)V
:goto_11
return-void
:catch_12
move-exception v0
const/4 v0, 0x0
goto :goto_2
:cond_15
invoke-static {}, Lcom/google/android/gms/internal/h;->a()Z
move-result v0
if-eqz v0, :cond_27
invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;
move-result-object v0
invoke-static {p3, p1}, Lcom/google/android/gms/common/a;->a(Landroid/app/Dialog;Landroid/content/DialogInterface$OnCancelListener;)Lcom/google/android/gms/common/a;
move-result-object v1
invoke-virtual {v1, v0, p2}, Lcom/google/android/gms/common/a;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
goto :goto_11
:cond_27
new-instance v0, Ljava/lang/RuntimeException;
const-string v1, "This Activity does not support Fragments."
invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V
throw v0
.end method
.method public static zzac(Landroid/content/Context;)V
.registers 6
invoke-static {}, Lcom/google/android/gms/common/b;->a()Lcom/google/android/gms/common/b;
move-result-object v0
invoke-virtual {v0, p0}, Lcom/google/android/gms/common/b;->a(Landroid/content/Context;)I
move-result v0
if-eqz v0, :cond_3c
invoke-static {}, Lcom/google/android/gms/common/b;->a()Lcom/google/android/gms/common/b;
move-result-object v1
const-string v2, "e"
invoke-virtual {v1, p0, v0, v2}, Lcom/google/android/gms/common/b;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;
move-result-object v1
const-string v2, "GooglePlayServicesUtil"
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "GooglePlayServices not available due to error "
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v3
invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
if-nez v1, :cond_34
new-instance v1, Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;
invoke-direct {v1, v0}, Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;-><init>(I)V
throw v1
:cond_34
new-instance v2, Lcom/google/android/gms/common/GooglePlayServicesRepairableException;
const-string v3, "Google Play Services not available"
invoke-direct {v2, v0, v3, v1}, Lcom/google/android/gms/common/GooglePlayServicesRepairableException;-><init>(ILjava/lang/String;Landroid/content/Intent;)V
throw v2
:cond_3c
return-void
.end method
.method public static zzaj(Landroid/content/Context;)V
.registers 3
sget-object v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaej:Ljava/util/concurrent/atomic/AtomicBoolean;
const/4 v1, 0x1
invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z
move-result v0
if-eqz v0, :cond_a
:goto_9
return-void
:cond_a
:try_start_a
const-string v0, "notification"
invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/app/NotificationManager;
const/16 v1, 0x28c4
invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V
:try_end_17
.catch Ljava/lang/SecurityException; {:try_start_a .. :try_end_17} :catch_18
goto :goto_9
:catch_18
move-exception v0
goto :goto_9
.end method
.method private static zzak(Landroid/content/Context;)V
.registers 5
sget-object v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaek:Ljava/util/concurrent/atomic/AtomicBoolean;
invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
move-result v0
if-eqz v0, :cond_9
:cond_8
return-void
:cond_9
sget-object v1, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzqf:Ljava/lang/Object;
monitor-enter v1
:try_start_c
sget-object v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaeh:Ljava/lang/String;
if-nez v0, :cond_51
invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v0
sput-object v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaeh:Ljava/lang/String;
:try_end_16
.catchall {:try_start_c .. :try_end_16} :catchall_4e
:try_start_16
invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v0
invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v2
const/16 v3, 0x80
invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
move-result-object v0
iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;
if-eqz v0, :cond_41
const-string v2, "com.google.android.gms.version"
invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
move-result v0
invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v0
sput-object v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaei:Ljava/lang/Integer;
:goto_34
:cond_34
:try_end_34
.catchall {:try_start_16 .. :try_end_34} :catchall_4e
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_16 .. :try_end_34} :catch_45
:try_start_34
sget-object v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaei:Ljava/lang/Integer;
monitor-exit v1
:try_end_37
.catchall {:try_start_34 .. :try_end_37} :catchall_4e
if-nez v0, :cond_8c
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "A required meta-data tag in your app\'s AndroidManifest.xml does not exist.  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_41
const/4 v0, 0x0
:try_start_42
sput-object v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaei:Ljava/lang/Integer;
:try_end_44
.catchall {:try_start_42 .. :try_end_44} :catchall_4e
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_42 .. :try_end_44} :catch_45
goto :goto_34
:catch_45
move-exception v0
:try_start_46
const-string v2, "GooglePlayServicesUtil"
const-string v3, "This should never happen."
invoke-static {v2, v3, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
goto :goto_34
:catchall_4e
move-exception v0
monitor-exit v1
:try_end_50
.catchall {:try_start_46 .. :try_end_50} :catchall_4e
throw v0
:try_start_51
:cond_51
sget-object v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaeh:Ljava/lang/String;
invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v2
invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-nez v0, :cond_34
new-instance v0, Ljava/lang/IllegalArgumentException;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "isGooglePlayServicesAvailable should only be called with Context from your application\'s package. A previous call used package \'"
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
sget-object v3, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaeh:Ljava/lang/String;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, "\' and this call used package \'"
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, "\'."
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:try_end_8c
.catchall {:try_start_51 .. :try_end_8c} :catchall_4e
:cond_8c
invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
move-result v1
sget v2, Lcom/google/android/gms/common/GooglePlayServicesUtil;->GOOGLE_PLAY_SERVICES_VERSION_CODE:I
if-eq v1, v2, :cond_8
new-instance v1, Ljava/lang/IllegalStateException;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "The meta-data tag in your app\'s AndroidManifest.xml does not have the right value.  Expected "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
sget v3, Lcom/google/android/gms/common/GooglePlayServicesUtil;->GOOGLE_PLAY_SERVICES_VERSION_CODE:I
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, " but"
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, " found "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v2, ".  You must have the"
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v2, " following declaration within the <application> element: "
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v2, "    <meta-data android:name=\""
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v2, "com.google.android.gms.version"
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v2, "\" android:value=\"@integer/google_play_services_version\" />"
invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v1
.end method
.method private static zzal(Landroid/content/Context;)V
.registers 5
new-instance v0, Lcom/google/android/gms/common/GooglePlayServicesUtil$a;
invoke-direct {v0, p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil$a;-><init>(Landroid/content/Context;)V
const/4 v1, 0x1
invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
move-result-object v1
const-wide/32 v2, 0x1d4c0
invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
return-void
.end method
.method public static zzam(Landroid/content/Context;)Ljava/lang/String;
.registers 5
invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;
move-result-object v0
iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->name:Ljava/lang/String;
invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v1
if-eqz v1, :cond_2b
invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v0
invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
move-result-object v1
invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v2
:try_start_18
invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v1
const/4 v3, 0x0
invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
:try_end_20
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_18 .. :try_end_20} :catch_2c
move-result-object v1
:goto_21
if-eqz v1, :cond_2b
invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
move-result-object v0
invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
move-result-object v0
:cond_2b
return-object v0
:catch_2c
move-exception v1
const/4 v1, 0x0
goto :goto_21
.end method
.method public static zzan(Landroid/content/Context;)I
.registers 5
const/4 v0, 0x0
:try_start_1
invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v1
const-string v2, "com.google.android.gms"
const/4 v3, 0x0
invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
:try_end_b
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_b} :catch_f
move-result-object v0
iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
:goto_e
return v0
:catch_f
move-exception v1
const-string v1, "GooglePlayServicesUtil"
const-string v2, "Google Play services is missing."
invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
goto :goto_e
.end method
.method public static zzao(Landroid/content/Context;)Z
.registers 3
invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v0
invoke-static {}, Lcom/google/android/gms/internal/h;->h()Z
move-result v1
if-eqz v1, :cond_14
const-string v1, "cn.google"
invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_14
const/4 v0, 0x1
:goto_13
return v0
:cond_14
const/4 v0, 0x0
goto :goto_13
.end method
.method public static zzap(Landroid/content/Context;)Z
.registers 4
invoke-static {}, Lcom/google/android/gms/internal/h;->e()Z
move-result v0
if-eqz v0, :cond_28
const-string v0, "user"
invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/os/UserManager;
invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/os/UserManager;->getApplicationRestrictions(Ljava/lang/String;)Landroid/os/Bundle;
move-result-object v0
if-eqz v0, :cond_28
const-string v1, "true"
const-string v2, "restricted_profile"
invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_28
const/4 v0, 0x1
:goto_27
return v0
:cond_28
const/4 v0, 0x0
goto :goto_27
.end method
.method static synthetic zzb(ILandroid/content/Context;)V
.registers 2
invoke-static {p0, p1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zza(ILandroid/content/Context;)V
return-void
.end method
.method public static zzb(Landroid/content/Context;ILjava/lang/String;)Z
.registers 8
const/4 v2, 0x1
const/4 v1, 0x0
invoke-static {}, Lcom/google/android/gms/internal/h;->f()Z
move-result v0
if-eqz v0, :cond_15
const-string v0, "appops"
invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/app/AppOpsManager;
:try_start_10
invoke-virtual {v0, p1, p2}, Landroid/app/AppOpsManager;->checkPackage(ILjava/lang/String;)V
:try_end_13
.catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_13} :catch_32
move v1, v2
:cond_14
:goto_14
return v1
:cond_15
invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v0
invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;
move-result-object v3
if-eqz p2, :cond_14
if-eqz v3, :cond_14
move v0, v1
:goto_22
array-length v4, v3
if-ge v0, v4, :cond_14
aget-object v4, v3, v0
invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v4
if-eqz v4, :cond_2f
move v1, v2
goto :goto_14
:cond_2f
add-int/lit8 v0, v0, 0x1
goto :goto_22
:catch_32
move-exception v0
goto :goto_14
.end method
.method public static zzb(Landroid/content/pm/PackageManager;)Z
.registers 10
const/4 v0, 0x1
const/4 v1, 0x0
sget-object v2, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzqf:Ljava/lang/Object;
monitor-enter v2
:try_start_5
sget v3, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaeg:I
:try_end_7
.catchall {:try_start_5 .. :try_end_7} :catchall_39
const/4 v4, -0x1
if-ne v3, v4, :cond_2a
:try_start_a
const-string v3, "com.google.android.gms"
const/16 v4, 0x40
invoke-virtual {p0, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
move-result-object v3
invoke-static {}, Lcom/google/android/gms/common/g;->a()Lcom/google/android/gms/common/g;
move-result-object v4
const/4 v5, 0x1
new-array v5, v5, [Lcom/google/android/gms/common/f$a;
const/4 v6, 0x0
sget-object v7, Lcom/google/android/gms/common/f;->b:[Lcom/google/android/gms/common/f$a;
const/4 v8, 0x1
aget-object v7, v7, v8
aput-object v7, v5, v6
invoke-virtual {v4, v3, v5}, Lcom/google/android/gms/common/g;->a(Landroid/content/pm/PackageInfo;[Lcom/google/android/gms/common/f$a;)Lcom/google/android/gms/common/f$a;
move-result-object v3
if-eqz v3, :cond_30
const/4 v3, 0x1
sput v3, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaeg:I
:try_start_2a
:try_end_2a
.catchall {:try_start_a .. :try_end_2a} :catchall_39
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_a .. :try_end_2a} :catch_34
:goto_2a
:cond_2a
sget v3, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaeg:I
if-eqz v3, :cond_3c
:goto_2e
monitor-exit v2
:try_end_2f
.catchall {:try_start_2a .. :try_end_2f} :catchall_39
return v0
:cond_30
const/4 v3, 0x0
:try_start_31
sput v3, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaeg:I
:try_end_33
.catchall {:try_start_31 .. :try_end_33} :catchall_39
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_31 .. :try_end_33} :catch_34
goto :goto_2a
:catch_34
move-exception v3
const/4 v3, 0x0
:try_start_36
sput v3, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaeg:I
goto :goto_2a
:catchall_39
move-exception v0
monitor-exit v2
:try_end_3b
.catchall {:try_start_36 .. :try_end_3b} :catchall_39
throw v0
:cond_3c
move v0, v1
goto :goto_2e
.end method
.method public static zzb(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
.registers 3
invoke-static {}, Lcom/google/android/gms/common/g;->a()Lcom/google/android/gms/common/g;
move-result-object v0
invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/common/g;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
move-result v0
return v0
.end method
.method public static zzbv(I)Landroid/content/Intent;
.registers 3
const/4 v1, 0x0
invoke-static {}, Lcom/google/android/gms/common/b;->a()Lcom/google/android/gms/common/b;
move-result-object v0
invoke-virtual {v0, v1, p0, v1}, Lcom/google/android/gms/common/b;->a(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;
move-result-object v0
return-object v0
.end method
.method private static zzbw(I)Z
.registers 2
sparse-switch p0, :sswitch_data_8
const/4 v0, 0x0
:goto_4
return v0
:sswitch_5
const/4 v0, 0x1
goto :goto_4
nop
:sswitch_data_8
.sparse-switch
0x1 -> :sswitch_5
0x2 -> :sswitch_5
0x3 -> :sswitch_5
0x12 -> :sswitch_5
0x2a -> :sswitch_5
.end sparse-switch
.end method
.method public static zzc(Landroid/content/pm/PackageManager;)Z
.registers 2
invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzb(Landroid/content/pm/PackageManager;)Z
move-result v0
if-nez v0, :cond_c
invoke-static {}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzow()Z
move-result v0
if-nez v0, :cond_e
:cond_c
const/4 v0, 0x1
:goto_d
return v0
:cond_e
const/4 v0, 0x0
goto :goto_d
.end method
.method public static zzd(Landroid/content/Context;I)Z
.registers 4
const/4 v0, 0x1
const/16 v1, 0x12
if-ne p1, v1, :cond_6
:goto_5
return v0
:cond_6
if-ne p1, v0, :cond_f
const-string v0, "com.google.android.gms"
invoke-static {p0, v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzh(Landroid/content/Context;Ljava/lang/String;)Z
move-result v0
goto :goto_5
:cond_f
const/4 v0, 0x0
goto :goto_5
.end method
.method public static zze(Landroid/content/Context;I)Z
.registers 4
const-string v0, "com.google.android.gms"
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzb(Landroid/content/Context;ILjava/lang/String;)Z
move-result v0
if-eqz v0, :cond_16
invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v0
const-string v1, "com.google.android.gms"
invoke-static {v0, v1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzb(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_16
const/4 v0, 0x1
:goto_15
return v0
:cond_16
const/4 v0, 0x0
goto :goto_15
.end method
.method public static zzf(Landroid/content/Context;I)Z
.registers 3
const/16 v0, 0x9
if-ne p1, v0, :cond_b
const-string v0, "com.android.vending"
invoke-static {p0, v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzh(Landroid/content/Context;Ljava/lang/String;)Z
move-result v0
:goto_a
return v0
:cond_b
const/4 v0, 0x0
goto :goto_a
.end method
.method static zzh(Landroid/content/Context;Ljava/lang/String;)Z
.registers 5
const/4 v1, 0x0
invoke-static {}, Lcom/google/android/gms/internal/h;->h()Z
move-result v0
if-eqz v0, :cond_2f
invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v0
invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;
move-result-object v0
invoke-virtual {v0}, Landroid/content/pm/PackageInstaller;->getAllSessions()Ljava/util/List;
move-result-object v0
invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v2
:cond_17
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_2f
invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/content/pm/PackageInstaller$SessionInfo;
invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;
move-result-object v0
invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_17
const/4 v0, 0x1
:goto_2e
return v0
:cond_2f
invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzap(Landroid/content/Context;)Z
move-result v0
if-eqz v0, :cond_37
move v0, v1
goto :goto_2e
:cond_37
invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v0
const/16 v2, 0x2000
:try_start_3d
invoke-virtual {v0, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
move-result-object v0
iget-boolean v0, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z
:try_end_43
.catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3d .. :try_end_43} :catch_44
goto :goto_2e
:catch_44
move-exception v0
move v0, v1
goto :goto_2e
.end method
.method private static zzov()I
.registers 1
const v0, 0x7e9e10
return v0
.end method
.method public static zzow()Z
.registers 2
sget-boolean v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaee:Z
if-eqz v0, :cond_7
sget-boolean v0, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzaef:Z
:goto_6
return v0
:cond_7
const-string v0, "user"
sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
goto :goto_6
.end method