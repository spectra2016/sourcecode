.class public Lcom/google/android/gms/common/internal/safeparcel/a;
.super Ljava/lang/Object;
.method public static a(Landroid/os/Parcel;)I
.registers 2
const/16 v0, 0x4f45
invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;I)I
move-result v0
return v0
.end method
.method public static a(Landroid/os/Parcel;I)V
.registers 2
invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/a;->c(Landroid/os/Parcel;I)V
return-void
.end method
.method public static a(Landroid/os/Parcel;II)V
.registers 4
const/4 v0, 0x4
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;II)V
invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeInt(I)V
return-void
.end method
.method public static a(Landroid/os/Parcel;IJ)V
.registers 6
const/16 v0, 0x8
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;II)V
invoke-virtual {p0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V
return-void
.end method
.method public static a(Landroid/os/Parcel;ILandroid/os/Bundle;Z)V
.registers 5
if-nez p2, :cond_9
if-eqz p3, :cond_8
const/4 v0, 0x0
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;II)V
:goto_8
:cond_8
return-void
:cond_9
invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;I)I
move-result v0
invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V
invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->c(Landroid/os/Parcel;I)V
goto :goto_8
.end method
.method public static a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V
.registers 5
if-nez p2, :cond_9
if-eqz p3, :cond_8
const/4 v0, 0x0
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;II)V
:goto_8
:cond_8
return-void
:cond_9
invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;I)I
move-result v0
invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V
invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->c(Landroid/os/Parcel;I)V
goto :goto_8
.end method
.method public static a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V
.registers 6
if-nez p2, :cond_9
if-eqz p4, :cond_8
const/4 v0, 0x0
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;II)V
:goto_8
:cond_8
return-void
:cond_9
invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;I)I
move-result v0
invoke-interface {p2, p0, p3}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V
invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->c(Landroid/os/Parcel;I)V
goto :goto_8
.end method
.method public static a(Landroid/os/Parcel;ILjava/lang/Float;Z)V
.registers 5
if-nez p2, :cond_9
if-eqz p3, :cond_8
const/4 v0, 0x0
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;II)V
:goto_8
:cond_8
return-void
:cond_9
const/4 v0, 0x4
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;II)V
invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F
move-result v0
invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeFloat(F)V
goto :goto_8
.end method
.method public static a(Landroid/os/Parcel;ILjava/lang/Long;Z)V
.registers 6
if-nez p2, :cond_9
if-eqz p3, :cond_8
const/4 v0, 0x0
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;II)V
:goto_8
:cond_8
return-void
:cond_9
const/16 v0, 0x8
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;II)V
invoke-virtual {p2}, Ljava/lang/Long;->longValue()J
move-result-wide v0
invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->writeLong(J)V
goto :goto_8
.end method
.method public static a(Landroid/os/Parcel;ILjava/lang/String;Z)V
.registers 5
if-nez p2, :cond_9
if-eqz p3, :cond_8
const/4 v0, 0x0
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;II)V
:goto_8
:cond_8
return-void
:cond_9
invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;I)I
move-result v0
invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->c(Landroid/os/Parcel;I)V
goto :goto_8
.end method
.method public static a(Landroid/os/Parcel;IZ)V
.registers 4
const/4 v0, 0x4
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;II)V
if-eqz p2, :cond_b
const/4 v0, 0x1
:goto_7
invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V
return-void
:cond_b
const/4 v0, 0x0
goto :goto_7
.end method
.method public static a(Landroid/os/Parcel;I[Landroid/os/Parcelable;IZ)V
.registers 10
const/4 v1, 0x0
if-nez p2, :cond_9
if-eqz p4, :cond_8
invoke-static {p0, p1, v1}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;II)V
:goto_8
:cond_8
return-void
:cond_9
invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;I)I
move-result v2
array-length v3, p2
invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V
move v0, v1
:goto_12
if-ge v0, v3, :cond_22
aget-object v4, p2, v0
if-nez v4, :cond_1e
invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V
:goto_1b
add-int/lit8 v0, v0, 0x1
goto :goto_12
:cond_1e
invoke-static {p0, v4, p3}, Lcom/google/android/gms/common/internal/safeparcel/a;->a(Landroid/os/Parcel;Landroid/os/Parcelable;I)V
goto :goto_1b
:cond_22
invoke-static {p0, v2}, Lcom/google/android/gms/common/internal/safeparcel/a;->c(Landroid/os/Parcel;I)V
goto :goto_8
.end method
.method public static a(Landroid/os/Parcel;I[Ljava/lang/String;Z)V
.registers 5
if-nez p2, :cond_9
if-eqz p3, :cond_8
const/4 v0, 0x0
invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;II)V
:goto_8
:cond_8
return-void
:cond_9
invoke-static {p0, p1}, Lcom/google/android/gms/common/internal/safeparcel/a;->b(Landroid/os/Parcel;I)I
move-result v0
invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V
invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/safeparcel/a;->c(Landroid/os/Parcel;I)V
goto :goto_8
.end method
.method private static a(Landroid/os/Parcel;Landroid/os/Parcelable;I)V
.registers 6
invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I
move-result v0
const/4 v1, 0x1
invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V
invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I
move-result v1
invoke-interface {p1, p0, p2}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V
invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I
move-result v2
invoke-virtual {p0, v0}, Landroid/os/Parcel;->setDataPosition(I)V
sub-int v0, v2, v1
invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V
invoke-virtual {p0, v2}, Landroid/os/Parcel;->setDataPosition(I)V
return-void
.end method
.method private static b(Landroid/os/Parcel;I)I
.registers 3
const/high16 v0, -0x1
or-int/2addr v0, p1
invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V
const/4 v0, 0x0
invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V
invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I
move-result v0
return v0
.end method
.method private static b(Landroid/os/Parcel;II)V
.registers 4
const v0, 0xffff
if-lt p2, v0, :cond_f
const/high16 v0, -0x1
or-int/2addr v0, p1
invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V
invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeInt(I)V
:goto_e
return-void
:cond_f
shl-int/lit8 v0, p2, 0x10
or-int/2addr v0, p1
invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V
goto :goto_e
.end method
.method private static c(Landroid/os/Parcel;I)V
.registers 5
invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I
move-result v0
sub-int v1, v0, p1
add-int/lit8 v2, p1, -0x4
invoke-virtual {p0, v2}, Landroid/os/Parcel;->setDataPosition(I)V
invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V
invoke-virtual {p0, v0}, Landroid/os/Parcel;->setDataPosition(I)V
return-void
.end method