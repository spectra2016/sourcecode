.class final Lcom/google/android/gms/common/internal/h$b;
.super Landroid/os/Handler;
.field final synthetic a:Lcom/google/android/gms/common/internal/h;
.method public constructor <init>(Lcom/google/android/gms/common/internal/h;Landroid/os/Looper;)V
.registers 3
iput-object p1, p0, Lcom/google/android/gms/common/internal/h$b;->a:Lcom/google/android/gms/common/internal/h;
invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V
return-void
.end method
.method private a(Landroid/os/Message;)V
.registers 3
iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;
check-cast v0, Lcom/google/android/gms/common/internal/h$c;
invoke-virtual {v0}, Lcom/google/android/gms/common/internal/h$c;->b()V
invoke-virtual {v0}, Lcom/google/android/gms/common/internal/h$c;->d()V
return-void
.end method
.method private b(Landroid/os/Message;)Z
.registers 5
const/4 v0, 0x1
iget v1, p1, Landroid/os/Message;->what:I
const/4 v2, 0x2
if-eq v1, v2, :cond_14
iget v1, p1, Landroid/os/Message;->what:I
if-eq v1, v0, :cond_14
iget v1, p1, Landroid/os/Message;->what:I
const/4 v2, 0x5
if-eq v1, v2, :cond_14
iget v1, p1, Landroid/os/Message;->what:I
const/4 v2, 0x6
if-ne v1, v2, :cond_15
:goto_14
:cond_14
return v0
:cond_15
const/4 v0, 0x0
goto :goto_14
.end method
.method public handleMessage(Landroid/os/Message;)V
.registers 7
const/4 v4, 0x1
const/4 v3, 0x0
const/4 v2, 0x4
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$b;->a:Lcom/google/android/gms/common/internal/h;
iget-object v0, v0, Lcom/google/android/gms/common/internal/h;->b:Ljava/util/concurrent/atomic/AtomicInteger;
invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
move-result v0
iget v1, p1, Landroid/os/Message;->arg1:I
if-eq v0, v1, :cond_19
invoke-direct {p0, p1}, Lcom/google/android/gms/common/internal/h$b;->b(Landroid/os/Message;)Z
move-result v0
if-eqz v0, :cond_18
invoke-direct {p0, p1}, Lcom/google/android/gms/common/internal/h$b;->a(Landroid/os/Message;)V
:cond_18
:goto_18
return-void
:cond_19
iget v0, p1, Landroid/os/Message;->what:I
if-eq v0, v4, :cond_27
iget v0, p1, Landroid/os/Message;->what:I
const/4 v1, 0x5
if-eq v0, v1, :cond_27
iget v0, p1, Landroid/os/Message;->what:I
const/4 v1, 0x6
if-ne v0, v1, :cond_33
:cond_27
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$b;->a:Lcom/google/android/gms/common/internal/h;
invoke-virtual {v0}, Lcom/google/android/gms/common/internal/h;->g()Z
move-result v0
if-nez v0, :cond_33
invoke-direct {p0, p1}, Lcom/google/android/gms/common/internal/h$b;->a(Landroid/os/Message;)V
goto :goto_18
:cond_33
iget v0, p1, Landroid/os/Message;->what:I
const/4 v1, 0x3
if-ne v0, v1, :cond_4e
new-instance v0, Lcom/google/android/gms/common/ConnectionResult;
iget v1, p1, Landroid/os/Message;->arg2:I
invoke-direct {v0, v1, v3}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V
iget-object v1, p0, Lcom/google/android/gms/common/internal/h$b;->a:Lcom/google/android/gms/common/internal/h;
invoke-static {v1}, Lcom/google/android/gms/common/internal/h;->a(Lcom/google/android/gms/common/internal/h;)Lcom/google/android/gms/common/api/c$e;
move-result-object v1
invoke-interface {v1, v0}, Lcom/google/android/gms/common/api/c$e;->a(Lcom/google/android/gms/common/ConnectionResult;)V
iget-object v1, p0, Lcom/google/android/gms/common/internal/h$b;->a:Lcom/google/android/gms/common/internal/h;
invoke-virtual {v1, v0}, Lcom/google/android/gms/common/internal/h;->a(Lcom/google/android/gms/common/ConnectionResult;)V
goto :goto_18
:cond_4e
iget v0, p1, Landroid/os/Message;->what:I
if-ne v0, v2, :cond_77
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$b;->a:Lcom/google/android/gms/common/internal/h;
invoke-static {v0, v2, v3}, Lcom/google/android/gms/common/internal/h;->a(Lcom/google/android/gms/common/internal/h;ILandroid/os/IInterface;)V
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$b;->a:Lcom/google/android/gms/common/internal/h;
invoke-static {v0}, Lcom/google/android/gms/common/internal/h;->b(Lcom/google/android/gms/common/internal/h;)Lcom/google/android/gms/common/api/c$b;
move-result-object v0
if-eqz v0, :cond_6a
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$b;->a:Lcom/google/android/gms/common/internal/h;
invoke-static {v0}, Lcom/google/android/gms/common/internal/h;->b(Lcom/google/android/gms/common/internal/h;)Lcom/google/android/gms/common/api/c$b;
move-result-object v0
iget v1, p1, Landroid/os/Message;->arg2:I
invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/c$b;->a(I)V
:cond_6a
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$b;->a:Lcom/google/android/gms/common/internal/h;
iget v1, p1, Landroid/os/Message;->arg2:I
invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/h;->a(I)V
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$b;->a:Lcom/google/android/gms/common/internal/h;
invoke-static {v0, v2, v4, v3}, Lcom/google/android/gms/common/internal/h;->a(Lcom/google/android/gms/common/internal/h;IILandroid/os/IInterface;)Z
goto :goto_18
:cond_77
iget v0, p1, Landroid/os/Message;->what:I
const/4 v1, 0x2
if-ne v0, v1, :cond_88
iget-object v0, p0, Lcom/google/android/gms/common/internal/h$b;->a:Lcom/google/android/gms/common/internal/h;
invoke-virtual {v0}, Lcom/google/android/gms/common/internal/h;->f()Z
move-result v0
if-nez v0, :cond_88
invoke-direct {p0, p1}, Lcom/google/android/gms/common/internal/h$b;->a(Landroid/os/Message;)V
goto :goto_18
:cond_88
invoke-direct {p0, p1}, Lcom/google/android/gms/common/internal/h$b;->b(Landroid/os/Message;)Z
move-result v0
if-eqz v0, :cond_96
iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;
check-cast v0, Lcom/google/android/gms/common/internal/h$c;
invoke-virtual {v0}, Lcom/google/android/gms/common/internal/h$c;->c()V
goto :goto_18
:cond_96
const-string v0, "GmsClient"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Don\'t know how to handle message: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
iget v2, p1, Landroid/os/Message;->what:I
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
new-instance v2, Ljava/lang/Exception;
invoke-direct {v2}, Ljava/lang/Exception;-><init>()V
invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
goto/16 :goto_18
.end method