.class  Lcom/google/android/gms/measurement/internal/m;
.super Ljava/lang/Object;
.field final a:Ljava/lang/String;
.field final b:Ljava/lang/String;
.field final c:J
.field final d:J
.field final e:J
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JJJ)V
.registers 16
const-wide/16 v4, 0x0
const/4 v1, 0x1
const/4 v2, 0x0
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;
invoke-static {p2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;
cmp-long v0, p3, v4
if-ltz v0, :cond_27
move v0, v1
:goto_12
invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->b(Z)V
cmp-long v0, p5, v4
if-ltz v0, :cond_29
:goto_19
invoke-static {v1}, Lcom/google/android/gms/common/internal/p;->b(Z)V
iput-object p1, p0, Lcom/google/android/gms/measurement/internal/m;->a:Ljava/lang/String;
iput-object p2, p0, Lcom/google/android/gms/measurement/internal/m;->b:Ljava/lang/String;
iput-wide p3, p0, Lcom/google/android/gms/measurement/internal/m;->c:J
iput-wide p5, p0, Lcom/google/android/gms/measurement/internal/m;->d:J
iput-wide p7, p0, Lcom/google/android/gms/measurement/internal/m;->e:J
return-void
:cond_27
move v0, v2
goto :goto_12
:cond_29
move v1, v2
goto :goto_19
.end method
.method  a(J)Lcom/google/android/gms/measurement/internal/m;
.registers 14
const-wide/16 v8, 0x1
new-instance v1, Lcom/google/android/gms/measurement/internal/m;
iget-object v2, p0, Lcom/google/android/gms/measurement/internal/m;->a:Ljava/lang/String;
iget-object v3, p0, Lcom/google/android/gms/measurement/internal/m;->b:Ljava/lang/String;
iget-wide v4, p0, Lcom/google/android/gms/measurement/internal/m;->c:J
add-long/2addr v4, v8
iget-wide v6, p0, Lcom/google/android/gms/measurement/internal/m;->d:J
add-long/2addr v6, v8
move-wide v8, p1
invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/measurement/internal/m;-><init>(Ljava/lang/String;Ljava/lang/String;JJJ)V
return-object v1
.end method