.class public Lcom/google/android/gms/measurement/internal/c;
.super Lcom/google/android/gms/measurement/internal/ab;
.field private a:Z
.field private final b:Landroid/app/AlarmManager;
.method protected constructor <init>(Lcom/google/android/gms/measurement/internal/y;)V
.registers 4
invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/ab;-><init>(Lcom/google/android/gms/measurement/internal/y;)V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/c;->i()Landroid/content/Context;
move-result-object v0
const-string v1, "alarm"
invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/app/AlarmManager;
iput-object v0, p0, Lcom/google/android/gms/measurement/internal/c;->b:Landroid/app/AlarmManager;
return-void
.end method
.method private o()Landroid/app/PendingIntent;
.registers 5
const/4 v3, 0x0
new-instance v0, Landroid/content/Intent;
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/c;->i()Landroid/content/Context;
move-result-object v1
const-class v2, Lcom/google/android/gms/measurement/AppMeasurementReceiver;
invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V
const-string v1, "com.google.android.gms.measurement.UPLOAD"
invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/c;->i()Landroid/content/Context;
move-result-object v1
invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
move-result-object v0
return-object v0
.end method
.method protected a()V
.registers 3
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/c;->b:Landroid/app/AlarmManager;
invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/c;->o()Landroid/app/PendingIntent;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
return-void
.end method
.method public a(J)V
.registers 10
const/4 v1, 0x1
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/c;->y()V
const-wide/16 v2, 0x0
cmp-long v0, p1, v2
if-lez v0, :cond_4d
move v0, v1
:goto_b
invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->b(Z)V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/c;->i()Landroid/content/Context;
move-result-object v0
invoke-static {v0}, Lcom/google/android/gms/measurement/AppMeasurementReceiver;->a(Landroid/content/Context;)Z
move-result v0
const-string v2, "Receiver not registered/enabled"
invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/p;->a(ZLjava/lang/Object;)V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/c;->i()Landroid/content/Context;
move-result-object v0
invoke-static {v0}, Lcom/google/android/gms/measurement/AppMeasurementService;->a(Landroid/content/Context;)Z
move-result v0
const-string v2, "Service not registered/enabled"
invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/p;->a(ZLjava/lang/Object;)V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/c;->b()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/c;->h()Lcom/google/android/gms/internal/e;
move-result-object v0
invoke-interface {v0}, Lcom/google/android/gms/internal/e;->b()J
move-result-wide v2
add-long/2addr v2, p1
iput-boolean v1, p0, Lcom/google/android/gms/measurement/internal/c;->a:Z
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/c;->b:Landroid/app/AlarmManager;
const/4 v1, 0x2
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/c;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v4
invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/h;->N()J
move-result-wide v4
invoke-static {v4, v5, p1, p2}, Ljava/lang/Math;->max(JJ)J
move-result-wide v4
invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/c;->o()Landroid/app/PendingIntent;
move-result-object v6
invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V
return-void
:cond_4d
const/4 v0, 0x0
goto :goto_b
.end method
.method public b()V
.registers 3
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/c;->y()V
const/4 v0, 0x0
iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/c;->a:Z
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/c;->b:Landroid/app/AlarmManager;
invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/c;->o()Landroid/app/PendingIntent;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
return-void
.end method
.method public bridge synthetic c()V
.registers 1
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->c()V
return-void
.end method
.method public bridge synthetic d()V
.registers 1
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->d()V
return-void
.end method
.method public bridge synthetic e()V
.registers 1
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->e()V
return-void
.end method
.method public bridge synthetic f()Lcom/google/android/gms/measurement/internal/r;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->f()Lcom/google/android/gms/measurement/internal/r;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic g()Lcom/google/android/gms/measurement/internal/ae;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->g()Lcom/google/android/gms/measurement/internal/ae;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic h()Lcom/google/android/gms/internal/e;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->h()Lcom/google/android/gms/internal/e;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic i()Landroid/content/Context;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->i()Landroid/content/Context;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic j()Lcom/google/android/gms/measurement/internal/f;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->j()Lcom/google/android/gms/measurement/internal/f;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic k()Lcom/google/android/gms/measurement/internal/x;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->k()Lcom/google/android/gms/measurement/internal/x;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic l()Lcom/google/android/gms/measurement/internal/t;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic m()Lcom/google/android/gms/measurement/internal/w;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->m()Lcom/google/android/gms/measurement/internal/w;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic n()Lcom/google/android/gms/measurement/internal/h;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v0
return-object v0
.end method