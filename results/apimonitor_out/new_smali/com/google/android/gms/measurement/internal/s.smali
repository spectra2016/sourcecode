.class public Lcom/google/android/gms/measurement/internal/s;
.super Lcom/google/android/gms/common/internal/h;
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/d;Lcom/google/android/gms/common/api/c$b;Lcom/google/android/gms/common/api/c$c;)V
.registers 13
const/16 v3, 0x5d
move-object v0, p0
move-object v1, p1
move-object v2, p2
move-object v4, p3
move-object v5, p4
move-object v6, p5
invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/internal/h;-><init>(Landroid/content/Context;Landroid/os/Looper;ILcom/google/android/gms/common/internal/d;Lcom/google/android/gms/common/api/c$b;Lcom/google/android/gms/common/api/c$c;)V
return-void
.end method
.method public synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
.registers 3
invoke-virtual {p0, p1}, Lcom/google/android/gms/measurement/internal/s;->b(Landroid/os/IBinder;)Lcom/google/android/gms/measurement/internal/q;
move-result-object v0
return-object v0
.end method
.method protected a()Ljava/lang/String;
.registers 2
const-string v0, "com.google.android.gms.measurement.START"
return-object v0
.end method
.method public b(Landroid/os/IBinder;)Lcom/google/android/gms/measurement/internal/q;
.registers 3
invoke-static {p1}, Lcom/google/android/gms/measurement/internal/q$a;->a(Landroid/os/IBinder;)Lcom/google/android/gms/measurement/internal/q;
move-result-object v0
return-object v0
.end method
.method protected b()Ljava/lang/String;
.registers 2
const-string v0, "com.google.android.gms.measurement.internal.IMeasurementService"
return-object v0
.end method