.class  Lcom/google/android/gms/measurement/internal/ae$5;
.super Ljava/lang/Object;
.implements Ljava/lang/Runnable;
.field final synthetic a:Lcom/google/android/gms/measurement/internal/UserAttributeParcel;
.field final synthetic b:Lcom/google/android/gms/measurement/internal/ae;
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ae;Lcom/google/android/gms/measurement/internal/UserAttributeParcel;)V
.registers 3
iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ae$5;->b:Lcom/google/android/gms/measurement/internal/ae;
iput-object p2, p0, Lcom/google/android/gms/measurement/internal/ae$5;->a:Lcom/google/android/gms/measurement/internal/UserAttributeParcel;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public run()V
.registers 5
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$5;->b:Lcom/google/android/gms/measurement/internal/ae;
invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ae;->c(Lcom/google/android/gms/measurement/internal/ae;)Lcom/google/android/gms/measurement/internal/q;
move-result-object v0
if-nez v0, :cond_18
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$5;->b:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Discarding data. Failed to set user attribute"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:goto_17
return-void
:try_start_18
:cond_18
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$5;->a:Lcom/google/android/gms/measurement/internal/UserAttributeParcel;
iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ae$5;->b:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ae;->f()Lcom/google/android/gms/measurement/internal/r;
move-result-object v2
iget-object v3, p0, Lcom/google/android/gms/measurement/internal/ae$5;->b:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v3
invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/t;->u()Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Lcom/google/android/gms/measurement/internal/r;->a(Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/AppMetadata;
move-result-object v2
invoke-interface {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/q;->a(Lcom/google/android/gms/measurement/internal/UserAttributeParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$5;->b:Lcom/google/android/gms/measurement/internal/ae;
invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ae;->d(Lcom/google/android/gms/measurement/internal/ae;)V
:try_end_36
.catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_36} :catch_37
goto :goto_17
:catch_37
move-exception v0
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$5;->b:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Failed to send attribute to AppMeasurementService"
invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_17
.end method