.class public Lcom/google/android/gms/measurement/internal/ae$a;
.super Ljava/lang/Object;
.implements Landroid/content/ServiceConnection;
.implements Lcom/google/android/gms/common/api/c$b;
.implements Lcom/google/android/gms/common/api/c$c;
.field final synthetic a:Lcom/google/android/gms/measurement/internal/ae;
.field private volatile b:Z
.field private volatile c:Lcom/google/android/gms/measurement/internal/s;
.method protected constructor <init>(Lcom/google/android/gms/measurement/internal/ae;)V
.registers 2
iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public a()V
.registers 7
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->e()V
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->i()Landroid/content/Context;
move-result-object v1
monitor-enter p0
:try_start_c
iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->b:Z
if-eqz v0, :cond_21
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Connection attempt already in progress"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
monitor-exit p0
:goto_20
return-void
:cond_21
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->c:Lcom/google/android/gms/measurement/internal/s;
if-eqz v0, :cond_39
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Already awaiting connection attempt"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
monitor-exit p0
goto :goto_20
:catchall_36
move-exception v0
monitor-exit p0
:try_end_38
.catchall {:try_start_c .. :try_end_38} :catchall_36
throw v0
:cond_39
:try_start_39
new-instance v0, Lcom/google/android/gms/measurement/internal/s;
invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;
move-result-object v2
invoke-static {v1}, Lcom/google/android/gms/common/internal/d;->a(Landroid/content/Context;)Lcom/google/android/gms/common/internal/d;
move-result-object v3
move-object v4, p0
move-object v5, p0
invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/measurement/internal/s;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/d;Lcom/google/android/gms/common/api/c$b;Lcom/google/android/gms/common/api/c$c;)V
iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->c:Lcom/google/android/gms/measurement/internal/s;
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Connecting to remote service"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
const/4 v0, 0x1
iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->b:Z
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->c:Lcom/google/android/gms/measurement/internal/s;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/s;->e()V
monitor-exit p0
:try_end_62
.catchall {:try_start_39 .. :try_end_62} :catchall_36
goto :goto_20
.end method
.method public a(I)V
.registers 4
const-string v0, "MeasurementServiceConnection.onConnectionSuspended"
invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->b(Ljava/lang/String;)V
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->s()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Service connection suspended"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->k()Lcom/google/android/gms/measurement/internal/x;
move-result-object v0
new-instance v1, Lcom/google/android/gms/measurement/internal/ae$a$4;
invoke-direct {v1, p0}, Lcom/google/android/gms/measurement/internal/ae$a$4;-><init>(Lcom/google/android/gms/measurement/internal/ae$a;)V
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V
return-void
.end method
.method public a(Landroid/content/Intent;)V
.registers 6
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->e()V
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->i()Landroid/content/Context;
move-result-object v0
invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;
move-result-object v1
monitor-enter p0
:try_start_10
iget-boolean v2, p0, Lcom/google/android/gms/measurement/internal/ae$a;->b:Z
if-eqz v2, :cond_25
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Connection attempt already in progress"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
monitor-exit p0
:goto_24
return-void
:cond_25
const/4 v2, 0x1
iput-boolean v2, p0, Lcom/google/android/gms/measurement/internal/ae$a;->b:Z
iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-static {v2}, Lcom/google/android/gms/measurement/internal/ae;->a(Lcom/google/android/gms/measurement/internal/ae;)Lcom/google/android/gms/measurement/internal/ae$a;
move-result-object v2
const/16 v3, 0x81
invoke-virtual {v1, v0, p1, v2, v3}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
monitor-exit p0
goto :goto_24
:catchall_35
move-exception v0
monitor-exit p0
:try_end_37
.catchall {:try_start_10 .. :try_end_37} :catchall_35
throw v0
.end method
.method public a(Landroid/os/Bundle;)V
.registers 5
const-string v0, "MeasurementServiceConnection.onConnected"
invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->b(Ljava/lang/String;)V
monitor-enter p0
const/4 v0, 0x0
:try_start_7
iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->b:Z
:try_end_9
.catchall {:try_start_7 .. :try_end_9} :catchall_29
:try_start_9
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->c:Lcom/google/android/gms/measurement/internal/s;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/s;->l()Landroid/os/IInterface;
move-result-object v0
check-cast v0, Lcom/google/android/gms/measurement/internal/q;
const/4 v1, 0x0
iput-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$a;->c:Lcom/google/android/gms/measurement/internal/s;
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ae;->k()Lcom/google/android/gms/measurement/internal/x;
move-result-object v1
new-instance v2, Lcom/google/android/gms/measurement/internal/ae$a$3;
invoke-direct {v2, p0, v0}, Lcom/google/android/gms/measurement/internal/ae$a$3;-><init>(Lcom/google/android/gms/measurement/internal/ae$a;Lcom/google/android/gms/measurement/internal/q;)V
invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V
:goto_22
:try_start_22
:try_end_22
.catchall {:try_start_9 .. :try_end_22} :catchall_29
.catch Landroid/os/DeadObjectException; {:try_start_9 .. :try_end_22} :catch_2c
.catch Ljava/lang/IllegalStateException; {:try_start_9 .. :try_end_22} :catch_24
monitor-exit p0
return-void
:catch_24
move-exception v0
:goto_25
const/4 v0, 0x0
iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->c:Lcom/google/android/gms/measurement/internal/s;
goto :goto_22
:catchall_29
move-exception v0
monitor-exit p0
:try_end_2b
.catchall {:try_start_22 .. :try_end_2b} :catchall_29
throw v0
:catch_2c
move-exception v0
goto :goto_25
.end method
.method public a(Lcom/google/android/gms/common/ConnectionResult;)V
.registers 4
const-string v0, "MeasurementServiceConnection.onConnectionFailed"
invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->b(Ljava/lang/String;)V
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->o()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Service connection failed"
invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
monitor-enter p0
const/4 v0, 0x0
:try_start_16
iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->b:Z
const/4 v0, 0x0
iput-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->c:Lcom/google/android/gms/measurement/internal/s;
monitor-exit p0
return-void
:catchall_1d
move-exception v0
monitor-exit p0
:try_end_1f
.catchall {:try_start_16 .. :try_end_1f} :catchall_1d
throw v0
.end method
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.registers 7
const-string v0, "MeasurementServiceConnection.onServiceConnected"
invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->b(Ljava/lang/String;)V
monitor-enter p0
const/4 v0, 0x0
:try_start_7
iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->b:Z
if-nez p2, :cond_1c
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Service connected with null binder"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
monitor-exit p0
:goto_1b
:try_end_1b
.catchall {:try_start_7 .. :try_end_1b} :catchall_53
return-void
:cond_1c
const/4 v0, 0x0
:try_start_1d
invoke-interface {p2}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;
move-result-object v1
const-string v2, "com.google.android.gms.measurement.internal.IMeasurementService"
invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v2
if-eqz v2, :cond_56
invoke-static {p2}, Lcom/google/android/gms/measurement/internal/q$a;->a(Landroid/os/IBinder;)Lcom/google/android/gms/measurement/internal/q;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Bound to IMeasurementService interface"
invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:try_end_3c
.catchall {:try_start_1d .. :try_end_3c} :catchall_53
.catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_3c} :catch_66
:goto_3c
if-nez v0, :cond_77
:try_start_3e
invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ae;->i()Landroid/content/Context;
move-result-object v1
iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-static {v2}, Lcom/google/android/gms/measurement/internal/ae;->a(Lcom/google/android/gms/measurement/internal/ae;)Lcom/google/android/gms/measurement/internal/ae$a;
move-result-object v2
invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
:try_start_51
:try_end_51
.catchall {:try_start_3e .. :try_end_51} :catchall_53
.catch Ljava/lang/IllegalArgumentException; {:try_start_3e .. :try_end_51} :catch_86
:goto_51
monitor-exit p0
goto :goto_1b
:catchall_53
move-exception v0
monitor-exit p0
:try_end_55
.catchall {:try_start_51 .. :try_end_55} :catchall_53
throw v0
:try_start_56
:cond_56
iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v2
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v2
const-string v3, "Got binder with a wrong descriptor"
invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
:try_end_65
.catchall {:try_start_56 .. :try_end_65} :catchall_53
.catch Landroid/os/RemoteException; {:try_start_56 .. :try_end_65} :catch_66
goto :goto_3c
:catch_66
move-exception v1
:try_start_67
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Service connect failed to get IMeasurementService"
invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
goto :goto_3c
:cond_77
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ae;->k()Lcom/google/android/gms/measurement/internal/x;
move-result-object v1
new-instance v2, Lcom/google/android/gms/measurement/internal/ae$a$1;
invoke-direct {v2, p0, v0}, Lcom/google/android/gms/measurement/internal/ae$a$1;-><init>(Lcom/google/android/gms/measurement/internal/ae$a;Lcom/google/android/gms/measurement/internal/q;)V
invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V
:try_end_85
.catchall {:try_start_67 .. :try_end_85} :catchall_53
goto :goto_51
:catch_86
move-exception v0
goto :goto_51
.end method
.method public onServiceDisconnected(Landroid/content/ComponentName;)V
.registers 4
const-string v0, "MeasurementServiceConnection.onServiceDisconnected"
invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->b(Ljava/lang/String;)V
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->s()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Service disconnected"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$a;->a:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->k()Lcom/google/android/gms/measurement/internal/x;
move-result-object v0
new-instance v1, Lcom/google/android/gms/measurement/internal/ae$a$2;
invoke-direct {v1, p0, p1}, Lcom/google/android/gms/measurement/internal/ae$a$2;-><init>(Lcom/google/android/gms/measurement/internal/ae$a;Landroid/content/ComponentName;)V
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V
return-void
.end method