.class  Lcom/google/android/gms/measurement/internal/ae$4;
.super Ljava/lang/Object;
.implements Ljava/lang/Runnable;
.field final synthetic a:Ljava/lang/String;
.field final synthetic b:Lcom/google/android/gms/measurement/internal/EventParcel;
.field final synthetic c:Lcom/google/android/gms/measurement/internal/ae;
.method constructor <init>(Lcom/google/android/gms/measurement/internal/ae;Ljava/lang/String;Lcom/google/android/gms/measurement/internal/EventParcel;)V
.registers 4
iput-object p1, p0, Lcom/google/android/gms/measurement/internal/ae$4;->c:Lcom/google/android/gms/measurement/internal/ae;
iput-object p2, p0, Lcom/google/android/gms/measurement/internal/ae$4;->a:Ljava/lang/String;
iput-object p3, p0, Lcom/google/android/gms/measurement/internal/ae$4;->b:Lcom/google/android/gms/measurement/internal/EventParcel;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public run()V
.registers 5
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$4;->c:Lcom/google/android/gms/measurement/internal/ae;
invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ae;->c(Lcom/google/android/gms/measurement/internal/ae;)Lcom/google/android/gms/measurement/internal/q;
move-result-object v0
if-nez v0, :cond_18
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$4;->c:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Discarding data. Failed to send event to service"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:goto_17
return-void
:cond_18
:try_start_18
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$4;->a:Ljava/lang/String;
invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v1
if-eqz v1, :cond_50
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$4;->b:Lcom/google/android/gms/measurement/internal/EventParcel;
iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ae$4;->c:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/ae;->f()Lcom/google/android/gms/measurement/internal/r;
move-result-object v2
iget-object v3, p0, Lcom/google/android/gms/measurement/internal/ae$4;->c:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v3
invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/t;->u()Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v3}, Lcom/google/android/gms/measurement/internal/r;->a(Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/AppMetadata;
move-result-object v2
invoke-interface {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/q;->a(Lcom/google/android/gms/measurement/internal/EventParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
:goto_39
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/ae$4;->c:Lcom/google/android/gms/measurement/internal/ae;
invoke-static {v0}, Lcom/google/android/gms/measurement/internal/ae;->d(Lcom/google/android/gms/measurement/internal/ae;)V
:try_end_3e
.catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_3e} :catch_3f
goto :goto_17
:catch_3f
move-exception v0
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$4;->c:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Failed to send event to AppMeasurementService"
invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_17
:try_start_50
:cond_50
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/ae$4;->b:Lcom/google/android/gms/measurement/internal/EventParcel;
iget-object v2, p0, Lcom/google/android/gms/measurement/internal/ae$4;->a:Ljava/lang/String;
iget-object v3, p0, Lcom/google/android/gms/measurement/internal/ae$4;->c:Lcom/google/android/gms/measurement/internal/ae;
invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/ae;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v3
invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/t;->u()Ljava/lang/String;
move-result-object v3
invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/measurement/internal/q;->a(Lcom/google/android/gms/measurement/internal/EventParcel;Ljava/lang/String;Ljava/lang/String;)V
:try_end_61
.catch Landroid/os/RemoteException; {:try_start_50 .. :try_end_61} :catch_3f
goto :goto_39
.end method