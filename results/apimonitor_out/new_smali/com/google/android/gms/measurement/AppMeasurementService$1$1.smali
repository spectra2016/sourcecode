.class  Lcom/google/android/gms/measurement/AppMeasurementService$1$1;
.super Ljava/lang/Object;
.implements Ljava/lang/Runnable;
.field final synthetic a:Lcom/google/android/gms/measurement/AppMeasurementService$1;
.method constructor <init>(Lcom/google/android/gms/measurement/AppMeasurementService$1;)V
.registers 2
iput-object p1, p0, Lcom/google/android/gms/measurement/AppMeasurementService$1$1;->a:Lcom/google/android/gms/measurement/AppMeasurementService$1;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public run()V
.registers 3
iget-object v0, p0, Lcom/google/android/gms/measurement/AppMeasurementService$1$1;->a:Lcom/google/android/gms/measurement/AppMeasurementService$1;
iget-object v0, v0, Lcom/google/android/gms/measurement/AppMeasurementService$1;->d:Lcom/google/android/gms/measurement/AppMeasurementService;
iget-object v1, p0, Lcom/google/android/gms/measurement/AppMeasurementService$1$1;->a:Lcom/google/android/gms/measurement/AppMeasurementService$1;
iget v1, v1, Lcom/google/android/gms/measurement/AppMeasurementService$1;->b:I
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/AppMeasurementService;->stopSelfResult(I)Z
move-result v0
if-eqz v0, :cond_29
iget-object v0, p0, Lcom/google/android/gms/measurement/AppMeasurementService$1$1;->a:Lcom/google/android/gms/measurement/AppMeasurementService$1;
iget-object v0, v0, Lcom/google/android/gms/measurement/AppMeasurementService$1;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->C()Z
move-result v0
if-eqz v0, :cond_2a
iget-object v0, p0, Lcom/google/android/gms/measurement/AppMeasurementService$1$1;->a:Lcom/google/android/gms/measurement/AppMeasurementService$1;
iget-object v0, v0, Lcom/google/android/gms/measurement/AppMeasurementService$1;->c:Lcom/google/android/gms/measurement/internal/t;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Device AppMeasurementService processed last upload request"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:goto_29
:cond_29
return-void
:cond_2a
iget-object v0, p0, Lcom/google/android/gms/measurement/AppMeasurementService$1$1;->a:Lcom/google/android/gms/measurement/AppMeasurementService$1;
iget-object v0, v0, Lcom/google/android/gms/measurement/AppMeasurementService$1;->c:Lcom/google/android/gms/measurement/internal/t;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Local AppMeasurementService processed last upload request"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
goto :goto_29
.end method