.class abstract Lcom/google/android/gms/measurement/internal/j;
.super Ljava/lang/Object;
.field private static volatile b:Landroid/os/Handler;
.field private final a:Lcom/google/android/gms/measurement/internal/y;
.field private final c:Ljava/lang/Runnable;
.field private volatile d:J
.field private e:Z
.method constructor <init>(Lcom/google/android/gms/measurement/internal/y;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
iput-object p1, p0, Lcom/google/android/gms/measurement/internal/j;->a:Lcom/google/android/gms/measurement/internal/y;
const/4 v0, 0x1
iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/j;->e:Z
new-instance v0, Lcom/google/android/gms/measurement/internal/j$1;
invoke-direct {v0, p0}, Lcom/google/android/gms/measurement/internal/j$1;-><init>(Lcom/google/android/gms/measurement/internal/j;)V
iput-object v0, p0, Lcom/google/android/gms/measurement/internal/j;->c:Ljava/lang/Runnable;
return-void
.end method
.method static synthetic a(Lcom/google/android/gms/measurement/internal/j;J)J
.registers 4
iput-wide p1, p0, Lcom/google/android/gms/measurement/internal/j;->d:J
return-wide p1
.end method
.method static synthetic a(Lcom/google/android/gms/measurement/internal/j;)Lcom/google/android/gms/measurement/internal/y;
.registers 2
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/j;->a:Lcom/google/android/gms/measurement/internal/y;
return-object v0
.end method
.method static synthetic b(Lcom/google/android/gms/measurement/internal/j;)Z
.registers 2
iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/j;->e:Z
return v0
.end method
.method private d()Landroid/os/Handler;
.registers 4
sget-object v0, Lcom/google/android/gms/measurement/internal/j;->b:Landroid/os/Handler;
if-eqz v0, :cond_7
sget-object v0, Lcom/google/android/gms/measurement/internal/j;->b:Landroid/os/Handler;
:goto_6
return-object v0
:cond_7
const-class v1, Lcom/google/android/gms/measurement/internal/j;
monitor-enter v1
:try_start_a
sget-object v0, Lcom/google/android/gms/measurement/internal/j;->b:Landroid/os/Handler;
if-nez v0, :cond_1f
new-instance v0, Landroid/os/Handler;
iget-object v2, p0, Lcom/google/android/gms/measurement/internal/j;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/y;->n()Landroid/content/Context;
move-result-object v2
invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;
move-result-object v2
invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V
sput-object v0, Lcom/google/android/gms/measurement/internal/j;->b:Landroid/os/Handler;
:cond_1f
sget-object v0, Lcom/google/android/gms/measurement/internal/j;->b:Landroid/os/Handler;
monitor-exit v1
goto :goto_6
:catchall_23
move-exception v0
monitor-exit v1
:try_end_25
.catchall {:try_start_a .. :try_end_25} :catchall_23
throw v0
.end method
.method public abstract a()V
.end method
.method public a(J)V
.registers 6
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/j;->c()V
const-wide/16 v0, 0x0
cmp-long v0, p1, v0
if-ltz v0, :cond_34
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/j;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->o()Lcom/google/android/gms/internal/e;
move-result-object v0
invoke-interface {v0}, Lcom/google/android/gms/internal/e;->a()J
move-result-wide v0
iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/j;->d:J
invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/j;->d()Landroid/os/Handler;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/j;->c:Ljava/lang/Runnable;
invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
move-result v0
if-nez v0, :cond_34
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/j;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Failed to schedule delayed post. time"
invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v2
invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
:cond_34
return-void
.end method
.method public b()Z
.registers 5
iget-wide v0, p0, Lcom/google/android/gms/measurement/internal/j;->d:J
const-wide/16 v2, 0x0
cmp-long v0, v0, v2
if-eqz v0, :cond_a
const/4 v0, 0x1
:goto_9
return v0
:cond_a
const/4 v0, 0x0
goto :goto_9
.end method
.method public c()V
.registers 3
const-wide/16 v0, 0x0
iput-wide v0, p0, Lcom/google/android/gms/measurement/internal/j;->d:J
invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/j;->d()Landroid/os/Handler;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/j;->c:Ljava/lang/Runnable;
invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
return-void
.end method