.class public Lcom/google/android/gms/measurement/internal/EventParams;
.super Ljava/lang/Object;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Ljava/lang/Iterable;
.field public static final CREATOR:Lcom/google/android/gms/measurement/internal/n;
.field public final a:I
.field private final b:Landroid/os/Bundle;
.method static constructor <clinit>()V
.registers 1
new-instance v0, Lcom/google/android/gms/measurement/internal/n;
invoke-direct {v0}, Lcom/google/android/gms/measurement/internal/n;-><init>()V
sput-object v0, Lcom/google/android/gms/measurement/internal/EventParams;->CREATOR:Lcom/google/android/gms/measurement/internal/n;
return-void
.end method
.method constructor <init>(ILandroid/os/Bundle;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput p1, p0, Lcom/google/android/gms/measurement/internal/EventParams;->a:I
iput-object p2, p0, Lcom/google/android/gms/measurement/internal/EventParams;->b:Landroid/os/Bundle;
return-void
.end method
.method constructor <init>(Landroid/os/Bundle;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
iput-object p1, p0, Lcom/google/android/gms/measurement/internal/EventParams;->b:Landroid/os/Bundle;
const/4 v0, 0x1
iput v0, p0, Lcom/google/android/gms/measurement/internal/EventParams;->a:I
return-void
.end method
.method static synthetic a(Lcom/google/android/gms/measurement/internal/EventParams;)Landroid/os/Bundle;
.registers 2
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/EventParams;->b:Landroid/os/Bundle;
return-object v0
.end method
.method public a()I
.registers 2
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/EventParams;->b:Landroid/os/Bundle;
invoke-virtual {v0}, Landroid/os/Bundle;->size()I
move-result v0
return v0
.end method
.method  a(Ljava/lang/String;)Ljava/lang/Object;
.registers 3
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/EventParams;->b:Landroid/os/Bundle;
invoke-virtual {v0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
return-object v0
.end method
.method public b()Landroid/os/Bundle;
.registers 3
new-instance v0, Landroid/os/Bundle;
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/EventParams;->b:Landroid/os/Bundle;
invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V
return-object v0
.end method
.method public describeContents()I
.registers 2
const/4 v0, 0x0
return v0
.end method
.method public iterator()Ljava/util/Iterator;
.registers 2
new-instance v0, Lcom/google/android/gms/measurement/internal/EventParams$1;
invoke-direct {v0, p0}, Lcom/google/android/gms/measurement/internal/EventParams$1;-><init>(Lcom/google/android/gms/measurement/internal/EventParams;)V
return-object v0
.end method
.method public toString()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/EventParams;->b:Landroid/os/Bundle;
invoke-virtual {v0}, Landroid/os/Bundle;->toString()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public writeToParcel(Landroid/os/Parcel;I)V
.registers 3
invoke-static {p0, p1, p2}, Lcom/google/android/gms/measurement/internal/n;->a(Lcom/google/android/gms/measurement/internal/EventParams;Landroid/os/Parcel;I)V
return-void
.end method