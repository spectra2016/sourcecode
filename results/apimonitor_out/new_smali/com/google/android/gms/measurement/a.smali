.class public Lcom/google/android/gms/measurement/a;
.super Ljava/lang/Object;
.field private final a:Lcom/google/android/gms/measurement/internal/y;
.method public constructor <init>(Lcom/google/android/gms/measurement/internal/y;)V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
iput-object p1, p0, Lcom/google/android/gms/measurement/a;->a:Lcom/google/android/gms/measurement/internal/y;
return-void
.end method
.method public static a(Landroid/content/Context;)Lcom/google/android/gms/measurement/a;
.registers 2
invoke-static {p0}, Lcom/google/android/gms/measurement/internal/y;->a(Landroid/content/Context;)Lcom/google/android/gms/measurement/internal/y;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->j()Lcom/google/android/gms/measurement/a;
move-result-object v0
return-object v0
.end method
.method public a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
.registers 5
if-nez p3, :cond_7
new-instance p3, Landroid/os/Bundle;
invoke-direct {p3}, Landroid/os/Bundle;-><init>()V
:cond_7
iget-object v0, p0, Lcom/google/android/gms/measurement/a;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->i()Lcom/google/android/gms/measurement/internal/ad;
move-result-object v0
invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/measurement/internal/ad;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
return-void
.end method