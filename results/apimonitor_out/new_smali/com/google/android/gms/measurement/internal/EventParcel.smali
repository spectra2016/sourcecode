.class public final Lcom/google/android/gms/measurement/internal/EventParcel;
.super Ljava/lang/Object;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.field public static final CREATOR:Lcom/google/android/gms/measurement/internal/o;
.field public final a:I
.field public final b:Ljava/lang/String;
.field public final c:Lcom/google/android/gms/measurement/internal/EventParams;
.field public final d:Ljava/lang/String;
.field public final e:J
.method static constructor <clinit>()V
.registers 1
new-instance v0, Lcom/google/android/gms/measurement/internal/o;
invoke-direct {v0}, Lcom/google/android/gms/measurement/internal/o;-><init>()V
sput-object v0, Lcom/google/android/gms/measurement/internal/EventParcel;->CREATOR:Lcom/google/android/gms/measurement/internal/o;
return-void
.end method
.method constructor <init>(ILjava/lang/String;Lcom/google/android/gms/measurement/internal/EventParams;Ljava/lang/String;J)V
.registers 8
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput p1, p0, Lcom/google/android/gms/measurement/internal/EventParcel;->a:I
iput-object p2, p0, Lcom/google/android/gms/measurement/internal/EventParcel;->b:Ljava/lang/String;
iput-object p3, p0, Lcom/google/android/gms/measurement/internal/EventParcel;->c:Lcom/google/android/gms/measurement/internal/EventParams;
iput-object p4, p0, Lcom/google/android/gms/measurement/internal/EventParcel;->d:Ljava/lang/String;
iput-wide p5, p0, Lcom/google/android/gms/measurement/internal/EventParcel;->e:J
return-void
.end method
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/EventParams;Ljava/lang/String;J)V
.registers 8
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, 0x1
iput v0, p0, Lcom/google/android/gms/measurement/internal/EventParcel;->a:I
iput-object p1, p0, Lcom/google/android/gms/measurement/internal/EventParcel;->b:Ljava/lang/String;
iput-object p2, p0, Lcom/google/android/gms/measurement/internal/EventParcel;->c:Lcom/google/android/gms/measurement/internal/EventParams;
iput-object p3, p0, Lcom/google/android/gms/measurement/internal/EventParcel;->d:Ljava/lang/String;
iput-wide p4, p0, Lcom/google/android/gms/measurement/internal/EventParcel;->e:J
return-void
.end method
.method public describeContents()I
.registers 2
const/4 v0, 0x0
return v0
.end method
.method public toString()Ljava/lang/String;
.registers 3
new-instance v0, Ljava/lang/StringBuilder;
invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
const-string v1, "origin="
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/EventParcel;->d:Ljava/lang/String;
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, ",name="
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/EventParcel;->b:Ljava/lang/String;
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, ",params="
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/EventParcel;->c:Lcom/google/android/gms/measurement/internal/EventParams;
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public writeToParcel(Landroid/os/Parcel;I)V
.registers 3
invoke-static {p0, p1, p2}, Lcom/google/android/gms/measurement/internal/o;->a(Lcom/google/android/gms/measurement/internal/EventParcel;Landroid/os/Parcel;I)V
return-void
.end method