.class public Lcom/google/android/gms/measurement/internal/z;
.super Lcom/google/android/gms/measurement/internal/q$a;
.field private final a:Lcom/google/android/gms/measurement/internal/y;
.field private final b:Z
.method public constructor <init>(Lcom/google/android/gms/measurement/internal/y;)V
.registers 3
invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/q$a;-><init>()V
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
iput-object p1, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
const/4 v0, 0x0
iput-boolean v0, p0, Lcom/google/android/gms/measurement/internal/z;->b:Z
return-void
.end method
.method public constructor <init>(Lcom/google/android/gms/measurement/internal/y;Z)V
.registers 3
invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/q$a;-><init>()V
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
iput-object p1, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
iput-boolean p2, p0, Lcom/google/android/gms/measurement/internal/z;->b:Z
return-void
.end method
.method static synthetic a(Lcom/google/android/gms/measurement/internal/z;)Lcom/google/android/gms/measurement/internal/y;
.registers 2
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
return-object v0
.end method
.method private b(Ljava/lang/String;)V
.registers 5
invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-eqz v0, :cond_1d
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Measurement Service called without app package"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
new-instance v0, Ljava/lang/SecurityException;
const-string v1, "Measurement Service called without app package"
invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V
throw v0
:try_start_1d
:cond_1d
invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/z;->c(Ljava/lang/String;)V
:try_end_20
.catch Ljava/lang/SecurityException; {:try_start_1d .. :try_end_20} :catch_21
return-void
:catch_21
move-exception v0
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Measurement Service called with invalid calling package"
invoke-virtual {v1, v2, p1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
throw v0
.end method
.method private c(Ljava/lang/String;)V
.registers 6
iget-boolean v0, p0, Lcom/google/android/gms/measurement/internal/z;->b:Z
if-eqz v0, :cond_15
invoke-static {}, Landroid/os/Process;->myUid()I
move-result v0
:goto_8
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/y;->n()Landroid/content/Context;
move-result-object v1
invoke-static {v1, v0, p1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zzb(Landroid/content/Context;ILjava/lang/String;)Z
move-result v1
if-eqz v1, :cond_1a
:cond_14
return-void
:cond_15
invoke-static {}, Landroid/os/Binder;->getCallingUid()I
move-result v0
goto :goto_8
:cond_1a
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/y;->n()Landroid/content/Context;
move-result-object v1
invoke-static {v1, v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->zze(Landroid/content/Context;I)Z
move-result v0
if-eqz v0, :cond_2e
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->v()Z
move-result v0
if-eqz v0, :cond_14
:cond_2e
new-instance v0, Ljava/lang/SecurityException;
const-string v1, "Unknown calling package name \'%s\'."
const/4 v2, 0x1
new-array v2, v2, [Ljava/lang/Object;
const/4 v3, 0x0
aput-object p1, v2, v3
invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
move-result-object v1
invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V
throw v0
.end method
.method public a(Lcom/google/android/gms/measurement/internal/AppMetadata;)V
.registers 4
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
iget-object v0, p1, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;
invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/z;->b(Ljava/lang/String;)V
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->g()Lcom/google/android/gms/measurement/internal/x;
move-result-object v0
new-instance v1, Lcom/google/android/gms/measurement/internal/z$6;
invoke-direct {v1, p0, p1}, Lcom/google/android/gms/measurement/internal/z$6;-><init>(Lcom/google/android/gms/measurement/internal/z;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V
return-void
.end method
.method public a(Lcom/google/android/gms/measurement/internal/EventParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
.registers 5
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
invoke-static {p2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
iget-object v0, p2, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;
invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/z;->b(Ljava/lang/String;)V
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->g()Lcom/google/android/gms/measurement/internal/x;
move-result-object v0
new-instance v1, Lcom/google/android/gms/measurement/internal/z$2;
invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/measurement/internal/z$2;-><init>(Lcom/google/android/gms/measurement/internal/z;Lcom/google/android/gms/measurement/internal/AppMetadata;Lcom/google/android/gms/measurement/internal/EventParcel;)V
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V
return-void
.end method
.method public a(Lcom/google/android/gms/measurement/internal/EventParcel;Ljava/lang/String;Ljava/lang/String;)V
.registers 6
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
invoke-static {p2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;
invoke-direct {p0, p2}, Lcom/google/android/gms/measurement/internal/z;->b(Ljava/lang/String;)V
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->g()Lcom/google/android/gms/measurement/internal/x;
move-result-object v0
new-instance v1, Lcom/google/android/gms/measurement/internal/z$3;
invoke-direct {v1, p0, p3, p1, p2}, Lcom/google/android/gms/measurement/internal/z$3;-><init>(Lcom/google/android/gms/measurement/internal/z;Ljava/lang/String;Lcom/google/android/gms/measurement/internal/EventParcel;Ljava/lang/String;)V
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V
return-void
.end method
.method public a(Lcom/google/android/gms/measurement/internal/UserAttributeParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
.registers 5
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
invoke-static {p2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
iget-object v0, p2, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;
invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/z;->b(Ljava/lang/String;)V
invoke-virtual {p1}, Lcom/google/android/gms/measurement/internal/UserAttributeParcel;->a()Ljava/lang/Object;
move-result-object v0
if-nez v0, :cond_20
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->g()Lcom/google/android/gms/measurement/internal/x;
move-result-object v0
new-instance v1, Lcom/google/android/gms/measurement/internal/z$4;
invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/measurement/internal/z$4;-><init>(Lcom/google/android/gms/measurement/internal/z;Lcom/google/android/gms/measurement/internal/AppMetadata;Lcom/google/android/gms/measurement/internal/UserAttributeParcel;)V
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V
:goto_1f
return-void
:cond_20
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->g()Lcom/google/android/gms/measurement/internal/x;
move-result-object v0
new-instance v1, Lcom/google/android/gms/measurement/internal/z$5;
invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/measurement/internal/z$5;-><init>(Lcom/google/android/gms/measurement/internal/z;Lcom/google/android/gms/measurement/internal/AppMetadata;Lcom/google/android/gms/measurement/internal/UserAttributeParcel;)V
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V
goto :goto_1f
.end method
.method  a(Ljava/lang/String;)V
.registers 8
const/4 v2, 0x2
const/4 v3, 0x0
invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
move-result v0
if-nez v0, :cond_30
const-string v0, ":"
invoke-virtual {p1, v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;
move-result-object v0
array-length v1, v0
if-ne v1, v2, :cond_30
const/4 v1, 0x0
:try_start_12
aget-object v1, v0, v1
invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
:try_end_1b
.catch Ljava/lang/NumberFormatException; {:try_start_12 .. :try_end_1b} :catch_31
move-result-wide v2
const-wide/16 v4, 0x0
cmp-long v1, v2, v4
if-lez v1, :cond_44
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/y;->e()Lcom/google/android/gms/measurement/internal/w;
move-result-object v1
iget-object v1, v1, Lcom/google/android/gms/measurement/internal/w;->b:Lcom/google/android/gms/measurement/internal/w$b;
const/4 v4, 0x1
aget-object v0, v0, v4
invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/measurement/internal/w$b;->a(Ljava/lang/String;J)V
:goto_30
:cond_30
return-void
:catch_31
move-exception v1
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->o()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Combining sample with a non-number weight"
aget-object v0, v0, v3
invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_30
:cond_44
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->o()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Combining sample with a non-positive weight"
invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v2
invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_30
.end method
.method public b(Lcom/google/android/gms/measurement/internal/AppMetadata;)V
.registers 4
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
iget-object v0, p1, Lcom/google/android/gms/measurement/internal/AppMetadata;->b:Ljava/lang/String;
invoke-direct {p0, v0}, Lcom/google/android/gms/measurement/internal/z;->b(Ljava/lang/String;)V
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/z;->a:Lcom/google/android/gms/measurement/internal/y;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->g()Lcom/google/android/gms/measurement/internal/x;
move-result-object v0
new-instance v1, Lcom/google/android/gms/measurement/internal/z$1;
invoke-direct {v1, p0, p1}, Lcom/google/android/gms/measurement/internal/z$1;-><init>(Lcom/google/android/gms/measurement/internal/z;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V
return-void
.end method