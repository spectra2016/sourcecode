.class public Lcom/google/android/gms/measurement/internal/h;
.super Lcom/google/android/gms/measurement/internal/aa;
.field static final a:Ljava/lang/String;
.field private b:Ljava/lang/Boolean;
.method static constructor <clinit>()V
.registers 3
sget v0, Lcom/google/android/gms/common/b;->a:I
div-int/lit16 v0, v0, 0x3e8
invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
move-result-object v0
const-string v1, "(\\d+)(\\d)(\\d\\d)"
const-string v2, "$1.$2.$3"
invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
sput-object v0, Lcom/google/android/gms/measurement/internal/h;->a:Ljava/lang/String;
return-void
.end method
.method constructor <init>(Lcom/google/android/gms/measurement/internal/y;)V
.registers 2
invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/aa;-><init>(Lcom/google/android/gms/measurement/internal/y;)V
return-void
.end method
.method public A()Ljava/lang/String;
.registers 2
const-string v0, "google_app_measurement2.db"
return-object v0
.end method
.method public B()J
.registers 3
sget v0, Lcom/google/android/gms/common/b;->a:I
div-int/lit16 v0, v0, 0x3e8
int-to-long v0, v0
return-wide v0
.end method
.method public C()Z
.registers 2
sget-boolean v0, Lcom/google/android/gms/common/internal/c;->a:Z
return v0
.end method
.method public D()Z
.registers 4
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/h;->b:Ljava/lang/Boolean;
if-nez v0, :cond_46
monitor-enter p0
:try_start_5
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/h;->b:Ljava/lang/Boolean;
if-nez v0, :cond_45
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/h;->i()Landroid/content/Context;
move-result-object v0
invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;
move-result-object v0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/h;->i()Landroid/content/Context;
move-result-object v1
invoke-static {}, Landroid/os/Process;->myPid()I
move-result v2
invoke-static {v1, v2}, Lcom/google/android/gms/internal/i;->a(Landroid/content/Context;I)Ljava/lang/String;
move-result-object v1
if-eqz v0, :cond_30
iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;
if-eqz v0, :cond_4d
invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_4d
const/4 v0, 0x1
:goto_2a
invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v0
iput-object v0, p0, Lcom/google/android/gms/measurement/internal/h;->b:Ljava/lang/Boolean;
:cond_30
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/h;->b:Ljava/lang/Boolean;
if-nez v0, :cond_45
sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
iput-object v0, p0, Lcom/google/android/gms/measurement/internal/h;->b:Ljava/lang/Boolean;
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/h;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "My process not in the list of running processes"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:cond_45
monitor-exit p0
:try_end_46
.catchall {:try_start_5 .. :try_end_46} :catchall_4f
:cond_46
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/h;->b:Ljava/lang/Boolean;
invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
move-result v0
return v0
:cond_4d
const/4 v0, 0x0
goto :goto_2a
:catchall_4f
move-exception v0
:try_start_50
monitor-exit p0
:try_end_51
.catchall {:try_start_50 .. :try_end_51} :catchall_4f
throw v0
.end method
.method public E()J
.registers 3
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->p:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Long;
invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
move-result-wide v0
return-wide v0
.end method
.method public F()J
.registers 3
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->l:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Long;
invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
move-result-wide v0
return-wide v0
.end method
.method public G()J
.registers 3
const-wide/16 v0, 0x14
return-wide v0
.end method
.method public H()J
.registers 5
const-wide/16 v2, 0x0
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->e:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Long;
invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
move-result-wide v0
invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J
move-result-wide v0
return-wide v0
.end method
.method public I()I
.registers 2
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->f:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Integer;
invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
move-result v0
return v0
.end method
.method public J()I
.registers 3
const/4 v1, 0x0
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->g:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Integer;
invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
move-result v0
invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I
move-result v0
return v0
.end method
.method public K()Ljava/lang/String;
.registers 2
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->h:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
return-object v0
.end method
.method public L()J
.registers 5
const-wide/16 v2, 0x0
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->i:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Long;
invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
move-result-wide v0
invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J
move-result-wide v0
return-wide v0
.end method
.method public M()J
.registers 5
const-wide/16 v2, 0x0
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->k:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Long;
invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
move-result-wide v0
invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J
move-result-wide v0
return-wide v0
.end method
.method public N()J
.registers 3
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->j:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Long;
invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
move-result-wide v0
return-wide v0
.end method
.method public O()J
.registers 5
const-wide/16 v2, 0x0
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->m:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Long;
invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
move-result-wide v0
invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J
move-result-wide v0
return-wide v0
.end method
.method public P()J
.registers 5
const-wide/16 v2, 0x0
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->n:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Long;
invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
move-result-wide v0
invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J
move-result-wide v0
return-wide v0
.end method
.method public Q()I
.registers 4
const/16 v1, 0x14
const/4 v2, 0x0
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->o:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Integer;
invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
move-result v0
invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I
move-result v0
invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I
move-result v0
return v0
.end method
.method  a()Ljava/lang/String;
.registers 2
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->c:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/String;
return-object v0
.end method
.method  b()I
.registers 2
const/16 v0, 0x20
return v0
.end method
.method public bridge synthetic c()V
.registers 1
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->c()V
return-void
.end method
.method public bridge synthetic d()V
.registers 1
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->d()V
return-void
.end method
.method public bridge synthetic e()V
.registers 1
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->e()V
return-void
.end method
.method public bridge synthetic f()Lcom/google/android/gms/measurement/internal/r;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->f()Lcom/google/android/gms/measurement/internal/r;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic g()Lcom/google/android/gms/measurement/internal/ae;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->g()Lcom/google/android/gms/measurement/internal/ae;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic h()Lcom/google/android/gms/internal/e;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->h()Lcom/google/android/gms/internal/e;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic i()Landroid/content/Context;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->i()Landroid/content/Context;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic j()Lcom/google/android/gms/measurement/internal/f;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->j()Lcom/google/android/gms/measurement/internal/f;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic k()Lcom/google/android/gms/measurement/internal/x;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->k()Lcom/google/android/gms/measurement/internal/x;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic l()Lcom/google/android/gms/measurement/internal/t;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic m()Lcom/google/android/gms/measurement/internal/w;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->m()Lcom/google/android/gms/measurement/internal/w;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic n()Lcom/google/android/gms/measurement/internal/h;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/aa;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v0
return-object v0
.end method
.method public o()I
.registers 2
const/16 v0, 0x18
return v0
.end method
.method  p()I
.registers 2
const/16 v0, 0x24
return v0
.end method
.method  q()I
.registers 2
const/16 v0, 0x100
return v0
.end method
.method  r()I
.registers 2
const/16 v0, 0x24
return v0
.end method
.method  s()I
.registers 2
const/16 v0, 0x800
return v0
.end method
.method  t()I
.registers 2
const/16 v0, 0x14
return v0
.end method
.method  u()J
.registers 3
const-wide/32 v0, 0x36ee80
return-wide v0
.end method
.method  v()J
.registers 3
const-wide/32 v0, 0xea60
return-wide v0
.end method
.method  w()J
.registers 3
const-wide/32 v0, 0xee48
return-wide v0
.end method
.method  x()J
.registers 3
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->d:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Long;
invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
move-result-wide v0
return-wide v0
.end method
.method  y()J
.registers 3
sget-object v0, Lcom/google/android/gms/measurement/internal/p;->q:Lcom/google/android/gms/measurement/internal/p$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/p$a;->a()Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Long;
invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
move-result-wide v0
return-wide v0
.end method
.method public z()Ljava/lang/String;
.registers 2
const-string v0, "google_app_measurement.db"
return-object v0
.end method