.class  Lcom/google/android/gms/measurement/internal/i;
.super Lcom/google/android/gms/measurement/internal/ab;
.field private static final a:Ljava/util/Map;
.field private final b:Lcom/google/android/gms/measurement/internal/i$a;
.field private final c:Lcom/google/android/gms/measurement/internal/b;
.method static constructor <clinit>()V
.registers 3
new-instance v0, Landroid/support/v4/e/a;
const/4 v1, 0x5
invoke-direct {v0, v1}, Landroid/support/v4/e/a;-><init>(I)V
sput-object v0, Lcom/google/android/gms/measurement/internal/i;->a:Ljava/util/Map;
sget-object v0, Lcom/google/android/gms/measurement/internal/i;->a:Ljava/util/Map;
const-string v1, "app_version"
const-string v2, "ALTER TABLE apps ADD COLUMN app_version TEXT;"
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
sget-object v0, Lcom/google/android/gms/measurement/internal/i;->a:Ljava/util/Map;
const-string v1, "app_store"
const-string v2, "ALTER TABLE apps ADD COLUMN app_store TEXT;"
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
sget-object v0, Lcom/google/android/gms/measurement/internal/i;->a:Ljava/util/Map;
const-string v1, "gmp_version"
const-string v2, "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;"
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
sget-object v0, Lcom/google/android/gms/measurement/internal/i;->a:Ljava/util/Map;
const-string v1, "dev_cert_hash"
const-string v2, "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;"
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
sget-object v0, Lcom/google/android/gms/measurement/internal/i;->a:Ljava/util/Map;
const-string v1, "measurement_enabled"
const-string v2, "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;"
invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
return-void
.end method
.method constructor <init>(Lcom/google/android/gms/measurement/internal/y;)V
.registers 5
invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/ab;-><init>(Lcom/google/android/gms/measurement/internal/y;)V
new-instance v0, Lcom/google/android/gms/measurement/internal/b;
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->h()Lcom/google/android/gms/internal/e;
move-result-object v1
invoke-direct {v0, v1}, Lcom/google/android/gms/measurement/internal/b;-><init>(Lcom/google/android/gms/internal/e;)V
iput-object v0, p0, Lcom/google/android/gms/measurement/internal/i;->c:Lcom/google/android/gms/measurement/internal/b;
invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/i;->A()Ljava/lang/String;
move-result-object v0
new-instance v1, Lcom/google/android/gms/measurement/internal/i$a;
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->i()Landroid/content/Context;
move-result-object v2
invoke-direct {v1, p0, v2, v0}, Lcom/google/android/gms/measurement/internal/i$a;-><init>(Lcom/google/android/gms/measurement/internal/i;Landroid/content/Context;Ljava/lang/String;)V
iput-object v1, p0, Lcom/google/android/gms/measurement/internal/i;->b:Lcom/google/android/gms/measurement/internal/i$a;
return-void
.end method
.method private A()Ljava/lang/String;
.registers 3
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->C()Z
move-result v0
if-nez v0, :cond_13
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->z()Ljava/lang/String;
move-result-object v0
:goto_12
return-object v0
:cond_13
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->D()Z
move-result v0
if-eqz v0, :cond_26
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->z()Ljava/lang/String;
move-result-object v0
goto :goto_12
:cond_26
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->p()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Using secondary database"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->A()Ljava/lang/String;
move-result-object v0
goto :goto_12
.end method
.method private B()Z
.registers 3
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->i()Landroid/content/Context;
move-result-object v0
invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/i;->A()Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;
move-result-object v0
invoke-virtual {v0}, Ljava/io/File;->exists()Z
move-result v0
return v0
.end method
.method static a(Landroid/database/Cursor;I)I
.registers 5
sget v0, Landroid/os/Build$VERSION;->SDK_INT:I
const/16 v1, 0xb
if-lt v0, v1, :cond_b
invoke-interface {p0, p1}, Landroid/database/Cursor;->getType(I)I
move-result v0
:goto_a
return v0
:cond_b
move-object v0, p0
check-cast v0, Landroid/database/sqlite/SQLiteCursor;
invoke-virtual {v0}, Landroid/database/sqlite/SQLiteCursor;->getWindow()Landroid/database/CursorWindow;
move-result-object v0
invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I
move-result v1
invoke-virtual {v0, v1, p1}, Landroid/database/CursorWindow;->isNull(II)Z
move-result v2
if-eqz v2, :cond_1e
const/4 v0, 0x0
goto :goto_a
:cond_1e
invoke-virtual {v0, v1, p1}, Landroid/database/CursorWindow;->isLong(II)Z
move-result v2
if-eqz v2, :cond_26
const/4 v0, 0x1
goto :goto_a
:cond_26
invoke-virtual {v0, v1, p1}, Landroid/database/CursorWindow;->isFloat(II)Z
move-result v2
if-eqz v2, :cond_2e
const/4 v0, 0x2
goto :goto_a
:cond_2e
invoke-virtual {v0, v1, p1}, Landroid/database/CursorWindow;->isString(II)Z
move-result v2
if-eqz v2, :cond_36
const/4 v0, 0x3
goto :goto_a
:cond_36
invoke-virtual {v0, v1, p1}, Landroid/database/CursorWindow;->isBlob(II)Z
move-result v0
if-eqz v0, :cond_3e
const/4 v0, 0x4
goto :goto_a
:cond_3e
const/4 v0, -0x1
goto :goto_a
.end method
.method private a(Ljava/lang/String;[Ljava/lang/String;J)J
.registers 10
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
const/4 v1, 0x0
:try_start_5
invoke-virtual {v0, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
move-result-object v1
invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
move-result v0
if-eqz v0, :cond_1a
const/4 v0, 0x0
invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
:try_end_13
.catchall {:try_start_5 .. :try_end_13} :catchall_2f
.catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_13} :catch_20
move-result-wide p3
if-eqz v1, :cond_19
invoke-interface {v1}, Landroid/database/Cursor;->close()V
:cond_19
:goto_19
return-wide p3
:cond_1a
if-eqz v1, :cond_19
invoke-interface {v1}, Landroid/database/Cursor;->close()V
goto :goto_19
:catch_20
move-exception v0
:try_start_21
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v2
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v2
const-string v3, "Database error"
invoke-virtual {v2, v3, p1, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
throw v0
:catchall_2f
:try_end_2f
.catchall {:try_start_21 .. :try_end_2f} :catchall_2f
move-exception v0
if-eqz v1, :cond_35
invoke-interface {v1}, Landroid/database/Cursor;->close()V
:cond_35
throw v0
.end method
.method static synthetic a(Lcom/google/android/gms/measurement/internal/i;)Lcom/google/android/gms/measurement/internal/b;
.registers 2
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/i;->c:Lcom/google/android/gms/measurement/internal/b;
return-object v0
.end method
.method static synthetic b(Lcom/google/android/gms/measurement/internal/i;)Ljava/lang/String;
.registers 2
invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/i;->A()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method static synthetic v()Ljava/util/Map;
.registers 1
sget-object v0, Lcom/google/android/gms/measurement/internal/i;->a:Ljava/util/Map;
return-object v0
.end method
.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/m;
.registers 15
const/4 v10, 0x0
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;
invoke-static {p2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->e()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
:try_start_d
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
const-string v1, "events"
const/4 v2, 0x3
new-array v2, v2, [Ljava/lang/String;
const/4 v3, 0x0
const-string v4, "lifetime_count"
aput-object v4, v2, v3
const/4 v3, 0x1
const-string v4, "current_bundle_count"
aput-object v4, v2, v3
const/4 v3, 0x2
const-string v4, "last_fire_timestamp"
aput-object v4, v2, v3
const-string v3, "app_id=? and name=?"
const/4 v4, 0x2
new-array v4, v4, [Ljava/lang/String;
const/4 v5, 0x0
aput-object p1, v4, v5
const/4 v5, 0x1
aput-object p2, v4, v5
const/4 v5, 0x0
const/4 v6, 0x0
const/4 v7, 0x0
invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
:try_end_36
.catchall {:try_start_d .. :try_end_36} :catchall_89
.catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_36} :catch_73
move-result-object v11
:try_start_37
invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z
:try_end_3a
.catchall {:try_start_37 .. :try_end_3a} :catchall_90
.catch Landroid/database/sqlite/SQLiteException; {:try_start_37 .. :try_end_3a} :catch_96
move-result v0
if-nez v0, :cond_44
if-eqz v11, :cond_42
invoke-interface {v11}, Landroid/database/Cursor;->close()V
:cond_42
move-object v1, v10
:goto_43
:cond_43
return-object v1
:cond_44
const/4 v0, 0x0
:try_start_45
invoke-interface {v11, v0}, Landroid/database/Cursor;->getLong(I)J
move-result-wide v4
const/4 v0, 0x1
invoke-interface {v11, v0}, Landroid/database/Cursor;->getLong(I)J
move-result-wide v6
const/4 v0, 0x2
invoke-interface {v11, v0}, Landroid/database/Cursor;->getLong(I)J
move-result-wide v8
new-instance v1, Lcom/google/android/gms/measurement/internal/m;
move-object v2, p1
move-object v3, p2
invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/measurement/internal/m;-><init>(Ljava/lang/String;Ljava/lang/String;JJJ)V
invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
move-result v0
if-eqz v0, :cond_6d
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v2, "Got multiple records for event aggregates, expected one"
invoke-virtual {v0, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:cond_6d
:try_end_6d
.catchall {:try_start_45 .. :try_end_6d} :catchall_90
.catch Landroid/database/sqlite/SQLiteException; {:try_start_45 .. :try_end_6d} :catch_96
if-eqz v11, :cond_43
invoke-interface {v11}, Landroid/database/Cursor;->close()V
goto :goto_43
:catch_73
move-exception v0
move-object v1, v10
:goto_75
:try_start_75
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v2
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v2
const-string v3, "Error querying events"
invoke-virtual {v2, v3, p1, p2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
:try_end_82
.catchall {:try_start_75 .. :try_end_82} :catchall_93
if-eqz v1, :cond_87
invoke-interface {v1}, Landroid/database/Cursor;->close()V
:cond_87
move-object v1, v10
goto :goto_43
:catchall_89
move-exception v0
:goto_8a
if-eqz v10, :cond_8f
invoke-interface {v10}, Landroid/database/Cursor;->close()V
:cond_8f
throw v0
:catchall_90
move-exception v0
move-object v10, v11
goto :goto_8a
:catchall_93
move-exception v0
move-object v10, v1
goto :goto_8a
:catch_96
move-exception v0
move-object v1, v11
goto :goto_75
.end method
.method public a(Ljava/lang/String;)Ljava/util/List;
.registers 13
const/4 v10, 0x0
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->e()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
new-instance v9, Ljava/util/ArrayList;
invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V
:try_start_f
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
const-string v1, "user_attributes"
const/4 v2, 0x3
new-array v2, v2, [Ljava/lang/String;
const/4 v3, 0x0
const-string v4, "name"
aput-object v4, v2, v3
const/4 v3, 0x1
const-string v4, "set_timestamp"
aput-object v4, v2, v3
const/4 v3, 0x2
const-string v4, "value"
aput-object v4, v2, v3
const-string v3, "app_id=?"
const/4 v4, 0x1
new-array v4, v4, [Ljava/lang/String;
const/4 v5, 0x0
aput-object p1, v4, v5
const/4 v5, 0x0
const/4 v6, 0x0
const-string v7, "rowid"
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v8
invoke-virtual {v8}, Lcom/google/android/gms/measurement/internal/h;->t()I
move-result v8
add-int/lit8 v8, v8, 0x1
invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
move-result-object v8
invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
:try_end_44
.catchall {:try_start_f .. :try_end_44} :catchall_c3
.catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_44} :catch_d0
move-result-object v7
:try_start_45
invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z
:try_end_48
.catchall {:try_start_45 .. :try_end_48} :catchall_cb
.catch Landroid/database/sqlite/SQLiteException; {:try_start_45 .. :try_end_48} :catch_ad
move-result v0
if-nez v0, :cond_52
if-eqz v7, :cond_50
invoke-interface {v7}, Landroid/database/Cursor;->close()V
:cond_50
move-object v0, v9
:goto_51
return-object v0
:cond_52
const/4 v0, 0x0
:try_start_53
invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
move-result-object v3
const/4 v0, 0x1
invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J
move-result-wide v4
const/4 v0, 0x2
invoke-virtual {p0, v7, v0}, Lcom/google/android/gms/measurement/internal/i;->b(Landroid/database/Cursor;I)Ljava/lang/Object;
move-result-object v6
if-nez v6, :cond_a3
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Read invalid user attribute value, ignoring it"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:goto_70
invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
move-result v0
if-nez v0, :cond_52
invoke-interface {v9}, Ljava/util/List;->size()I
move-result v0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/h;->t()I
move-result v1
if-le v0, v1, :cond_9c
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Loaded too many user attributes"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->t()I
move-result v0
invoke-interface {v9, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;
:try_end_9c
.catchall {:try_start_53 .. :try_end_9c} :catchall_cb
.catch Landroid/database/sqlite/SQLiteException; {:try_start_53 .. :try_end_9c} :catch_ad
:cond_9c
if-eqz v7, :cond_a1
invoke-interface {v7}, Landroid/database/Cursor;->close()V
:cond_a1
move-object v0, v9
goto :goto_51
:cond_a3
:try_start_a3
new-instance v1, Lcom/google/android/gms/measurement/internal/d;
move-object v2, p1
invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/measurement/internal/d;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Object;)V
invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
:try_end_ac
.catchall {:try_start_a3 .. :try_end_ac} :catchall_cb
.catch Landroid/database/sqlite/SQLiteException; {:try_start_a3 .. :try_end_ac} :catch_ad
goto :goto_70
:catch_ad
move-exception v0
move-object v1, v7
:try_start_af
:goto_af
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v2
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v2
const-string v3, "Error querying user attributes"
invoke-virtual {v2, v3, p1, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
:try_end_bc
.catchall {:try_start_af .. :try_end_bc} :catchall_cd
if-eqz v1, :cond_c1
invoke-interface {v1}, Landroid/database/Cursor;->close()V
:cond_c1
move-object v0, v10
goto :goto_51
:catchall_c3
move-exception v0
move-object v7, v10
:goto_c5
if-eqz v7, :cond_ca
invoke-interface {v7}, Landroid/database/Cursor;->close()V
:cond_ca
throw v0
:catchall_cb
move-exception v0
goto :goto_c5
:catchall_cd
move-exception v0
move-object v7, v1
goto :goto_c5
:catch_d0
move-exception v0
move-object v1, v10
goto :goto_af
.end method
.method public a(Ljava/lang/String;II)Ljava/util/List;
.registers 15
const/4 v10, 0x0
const/4 v1, 0x1
const/4 v9, 0x0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->e()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
if-lez p2, :cond_4e
move v0, v1
:goto_c
invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->b(Z)V
if-lez p3, :cond_50
:goto_11
invoke-static {v1}, Lcom/google/android/gms/common/internal/p;->b(Z)V
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;
:try_start_17
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
const-string v1, "queue"
const/4 v2, 0x2
new-array v2, v2, [Ljava/lang/String;
const/4 v3, 0x0
const-string v4, "rowid"
aput-object v4, v2, v3
const/4 v3, 0x1
const-string v4, "data"
aput-object v4, v2, v3
const-string v3, "app_id=?"
const/4 v4, 0x1
new-array v4, v4, [Ljava/lang/String;
const/4 v5, 0x0
aput-object p1, v4, v5
const/4 v5, 0x0
const/4 v6, 0x0
const-string v7, "rowid"
invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
move-result-object v8
invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
:try_end_3d
.catchall {:try_start_17 .. :try_end_3d} :catchall_d6
.catch Landroid/database/sqlite/SQLiteException; {:try_start_17 .. :try_end_3d} :catch_e3
move-result-object v2
:try_start_3e
invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
move-result v0
if-nez v0, :cond_52
invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
:try_end_47
.catchall {:try_start_3e .. :try_end_47} :catchall_de
.catch Landroid/database/sqlite/SQLiteException; {:try_start_3e .. :try_end_47} :catch_ad
move-result-object v0
if-eqz v2, :cond_4d
invoke-interface {v2}, Landroid/database/Cursor;->close()V
:goto_4d
:cond_4d
return-object v0
:cond_4e
move v0, v9
goto :goto_c
:cond_50
move v1, v9
goto :goto_11
:cond_52
:try_start_52
new-instance v0, Ljava/util/ArrayList;
invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
move v3, v9
:goto_58
const/4 v1, 0x0
invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J
:try_end_5c
.catchall {:try_start_52 .. :try_end_5c} :catchall_de
.catch Landroid/database/sqlite/SQLiteException; {:try_start_52 .. :try_end_5c} :catch_ad
move-result-wide v4
const/4 v1, 0x1
:try_start_5e
invoke-interface {v2, v1}, Landroid/database/Cursor;->getBlob(I)[B
move-result-object v1
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->j()Lcom/google/android/gms/measurement/internal/f;
move-result-object v6
invoke-virtual {v6, v1}, Lcom/google/android/gms/measurement/internal/f;->b([B)[B
:try_end_69
.catchall {:try_start_5e .. :try_end_69} :catchall_de
.catch Ljava/io/IOException; {:try_start_5e .. :try_end_69} :catch_7a
.catch Landroid/database/sqlite/SQLiteException; {:try_start_5e .. :try_end_69} :catch_ad
move-result-object v1
:try_start_6a
invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
move-result v6
if-nez v6, :cond_93
array-length v6, v1
:try_end_71
.catchall {:try_start_6a .. :try_end_71} :catchall_de
.catch Landroid/database/sqlite/SQLiteException; {:try_start_6a .. :try_end_71} :catch_ad
add-int/2addr v6, v3
if-le v6, p3, :cond_93
:cond_74
if-eqz v2, :cond_4d
invoke-interface {v2}, Landroid/database/Cursor;->close()V
goto :goto_4d
:catch_7a
move-exception v1
:try_start_7b
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v4
invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v4
const-string v5, "Failed to unzip queued bundle"
invoke-virtual {v4, v5, p1, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
move v1, v3
:goto_89
invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
move-result v3
if-eqz v3, :cond_74
if-gt v1, p3, :cond_74
move v3, v1
goto :goto_58
:cond_93
invoke-static {v1}, Lcom/google/android/gms/internal/n;->a([B)Lcom/google/android/gms/internal/n;
move-result-object v6
new-instance v7, Lcom/google/android/gms/internal/j$d;
invoke-direct {v7}, Lcom/google/android/gms/internal/j$d;-><init>()V
:try_start_9c
:try_end_9c
.catchall {:try_start_7b .. :try_end_9c} :catchall_de
.catch Landroid/database/sqlite/SQLiteException; {:try_start_7b .. :try_end_9c} :catch_ad
invoke-virtual {v7, v6}, Lcom/google/android/gms/internal/j$d;->a(Lcom/google/android/gms/internal/n;)Lcom/google/android/gms/internal/j$d;
:try_end_9f
.catchall {:try_start_9c .. :try_end_9f} :catchall_de
.catch Ljava/io/IOException; {:try_start_9c .. :try_end_9f} :catch_c6
.catch Landroid/database/sqlite/SQLiteException; {:try_start_9c .. :try_end_9f} :catch_ad
:try_start_9f
array-length v1, v1
add-int/2addr v1, v3
invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v3
invoke-static {v7, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
move-result-object v3
invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
:try_end_ac
.catchall {:try_start_9f .. :try_end_ac} :catchall_de
.catch Landroid/database/sqlite/SQLiteException; {:try_start_9f .. :try_end_ac} :catch_ad
goto :goto_89
:catch_ad
move-exception v0
move-object v1, v2
:try_start_af
:goto_af
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v2
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v2
const-string v3, "Error querying bundles"
invoke-virtual {v2, v3, p1, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
:try_end_bf
.catchall {:try_start_af .. :try_end_bf} :catchall_e0
move-result-object v0
if-eqz v1, :cond_4d
invoke-interface {v1}, Landroid/database/Cursor;->close()V
goto :goto_4d
:catch_c6
move-exception v1
:try_start_c7
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v4
invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v4
const-string v5, "Failed to merge queued bundle"
invoke-virtual {v4, v5, p1, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
:try_end_d4
.catchall {:try_start_c7 .. :try_end_d4} :catchall_de
.catch Landroid/database/sqlite/SQLiteException; {:try_start_c7 .. :try_end_d4} :catch_ad
move v1, v3
goto :goto_89
:catchall_d6
move-exception v0
move-object v2, v10
:goto_d8
if-eqz v2, :cond_dd
invoke-interface {v2}, Landroid/database/Cursor;->close()V
:cond_dd
throw v0
:catchall_de
move-exception v0
goto :goto_d8
:catchall_e0
move-exception v0
move-object v2, v1
goto :goto_d8
:catch_e3
move-exception v0
move-object v1, v10
goto :goto_af
.end method
.method protected a()V
.registers 1
return-void
.end method
.method public a(J)V
.registers 8
const/4 v4, 0x1
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->e()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
new-array v1, v4, [Ljava/lang/String;
const/4 v2, 0x0
invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;
move-result-object v3
aput-object v3, v1, v2
const-string v2, "queue"
const-string v3, "rowid=?"
invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
move-result v0
if-eq v0, v4, :cond_2b
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Deleted fewer rows from queue than expected"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:cond_2b
return-void
.end method
.method  a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Object;)V
.registers 6
invoke-static {p2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;
invoke-static {p3}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
instance-of v0, p3, Ljava/lang/String;
if-eqz v0, :cond_10
check-cast p3, Ljava/lang/String;
invoke-virtual {p1, p2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
:goto_f
return-void
:cond_10
instance-of v0, p3, Ljava/lang/Long;
if-eqz v0, :cond_1a
check-cast p3, Ljava/lang/Long;
invoke-virtual {p1, p2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
goto :goto_f
:cond_1a
instance-of v0, p3, Ljava/lang/Float;
if-eqz v0, :cond_24
check-cast p3, Ljava/lang/Float;
invoke-virtual {p1, p2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V
goto :goto_f
:cond_24
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "Invalid value type"
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
.end method
.method public a(Lcom/google/android/gms/internal/j$d;)V
.registers 8
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->e()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
iget-object v0, p1, Lcom/google/android/gms/internal/j$d;->o:Ljava/lang/String;
invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;
iget-object v0, p1, Lcom/google/android/gms/internal/j$d;->f:Ljava/lang/Long;
invoke-static {v0}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->s()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->h()Lcom/google/android/gms/internal/e;
move-result-object v0
invoke-interface {v0}, Lcom/google/android/gms/internal/e;->a()J
move-result-wide v0
iget-object v2, p1, Lcom/google/android/gms/internal/j$d;->f:Ljava/lang/Long;
invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
move-result-wide v2
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v4
invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/h;->E()J
move-result-wide v4
sub-long v4, v0, v4
cmp-long v2, v2, v4
if-ltz v2, :cond_45
iget-object v2, p1, Lcom/google/android/gms/internal/j$d;->f:Ljava/lang/Long;
invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
move-result-wide v2
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v4
invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/h;->E()J
move-result-wide v4
add-long/2addr v4, v0
cmp-long v2, v2, v4
if-lez v2, :cond_58
:cond_45
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v2
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/t;->o()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v2
const-string v3, "Storing bundle outside of the max uploading time span. now, timestamp"
invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v0
iget-object v1, p1, Lcom/google/android/gms/internal/j$d;->f:Ljava/lang/Long;
invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
:try_start_58
:cond_58
invoke-virtual {p1}, Lcom/google/android/gms/internal/j$d;->e()I
move-result v0
new-array v0, v0, [B
invoke-static {v0}, Lcom/google/android/gms/internal/zztd;->a([B)Lcom/google/android/gms/internal/zztd;
move-result-object v1
invoke-virtual {p1, v1}, Lcom/google/android/gms/internal/j$d;->a(Lcom/google/android/gms/internal/zztd;)V
invoke-virtual {v1}, Lcom/google/android/gms/internal/zztd;->b()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->j()Lcom/google/android/gms/measurement/internal/f;
move-result-object v1
invoke-virtual {v1, v0}, Lcom/google/android/gms/measurement/internal/f;->a([B)[B
:try_end_6f
.catch Ljava/io/IOException; {:try_start_58 .. :try_end_6f} :catch_b9
move-result-object v0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Saving bundle, size"
array-length v3, v0
invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v3
invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
new-instance v1, Landroid/content/ContentValues;
invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V
const-string v2, "app_id"
iget-object v3, p1, Lcom/google/android/gms/internal/j$d;->o:Ljava/lang/String;
invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v2, "bundle_end_timestamp"
iget-object v3, p1, Lcom/google/android/gms/internal/j$d;->f:Ljava/lang/Long;
invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
const-string v2, "data"
invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
:try_start_9a
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
const-string v2, "queue"
const/4 v3, 0x0
invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
move-result-wide v0
const-wide/16 v2, -0x1
cmp-long v0, v0, v2
if-nez v0, :cond_b8
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Failed to insert bundle (got -1)"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:try_end_b8
.catch Landroid/database/sqlite/SQLiteException; {:try_start_9a .. :try_end_b8} :catch_c8
:cond_b8
:goto_b8
return-void
:catch_b9
move-exception v0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Data loss. Failed to serialize bundle"
invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_b8
:catch_c8
move-exception v0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Error storing bundle"
invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_b8
.end method
.method public a(Lcom/google/android/gms/measurement/internal/a;)V
.registers 7
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->e()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
new-instance v0, Landroid/content/ContentValues;
invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V
const-string v1, "app_id"
iget-object v2, p1, Lcom/google/android/gms/measurement/internal/a;->a:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "app_instance_id"
iget-object v2, p1, Lcom/google/android/gms/measurement/internal/a;->b:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "gmp_app_id"
iget-object v2, p1, Lcom/google/android/gms/measurement/internal/a;->c:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "resettable_device_id_hash"
iget-object v2, p1, Lcom/google/android/gms/measurement/internal/a;->d:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "last_bundle_index"
iget-wide v2, p1, Lcom/google/android/gms/measurement/internal/a;->e:J
invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v2
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
const-string v1, "last_bundle_end_timestamp"
iget-wide v2, p1, Lcom/google/android/gms/measurement/internal/a;->f:J
invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v2
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
const-string v1, "app_version"
iget-object v2, p1, Lcom/google/android/gms/measurement/internal/a;->g:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "app_store"
iget-object v2, p1, Lcom/google/android/gms/measurement/internal/a;->h:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "gmp_version"
iget-wide v2, p1, Lcom/google/android/gms/measurement/internal/a;->i:J
invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v2
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
const-string v1, "dev_cert_hash"
iget-wide v2, p1, Lcom/google/android/gms/measurement/internal/a;->j:J
invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v2
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
const-string v1, "measurement_enabled"
iget-boolean v2, p1, Lcom/google/android/gms/measurement/internal/a;->k:Z
invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v2
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V
:try_start_6f
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v1
const-string v2, "apps"
const/4 v3, 0x0
const/4 v4, 0x5
invoke-virtual {v1, v2, v3, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
move-result-wide v0
const-wide/16 v2, -0x1
cmp-long v0, v0, v2
if-nez v0, :cond_8e
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Failed to insert/update app (got -1)"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:goto_8e
:cond_8e
:try_end_8e
.catch Landroid/database/sqlite/SQLiteException; {:try_start_6f .. :try_end_8e} :catch_8f
return-void
:catch_8f
move-exception v0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Error storing app"
invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_8e
.end method
.method public a(Lcom/google/android/gms/measurement/internal/d;)V
.registers 7
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->e()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
new-instance v0, Landroid/content/ContentValues;
invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V
const-string v1, "app_id"
iget-object v2, p1, Lcom/google/android/gms/measurement/internal/d;->a:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "name"
iget-object v2, p1, Lcom/google/android/gms/measurement/internal/d;->b:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "set_timestamp"
iget-wide v2, p1, Lcom/google/android/gms/measurement/internal/d;->c:J
invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v2
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
const-string v1, "value"
iget-object v2, p1, Lcom/google/android/gms/measurement/internal/d;->d:Ljava/lang/Object;
invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/measurement/internal/i;->a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Object;)V
:try_start_2e
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v1
const-string v2, "user_attributes"
const/4 v3, 0x0
const/4 v4, 0x5
invoke-virtual {v1, v2, v3, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
move-result-wide v0
const-wide/16 v2, -0x1
cmp-long v0, v0, v2
if-nez v0, :cond_4d
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Failed to insert/update user attribute (got -1)"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:cond_4d
:try_end_4d
.catch Landroid/database/sqlite/SQLiteException; {:try_start_2e .. :try_end_4d} :catch_4e
:goto_4d
return-void
:catch_4e
move-exception v0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Error storing user attribute"
invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_4d
.end method
.method public a(Lcom/google/android/gms/measurement/internal/m;)V
.registers 7
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->e()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
new-instance v0, Landroid/content/ContentValues;
invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V
const-string v1, "app_id"
iget-object v2, p1, Lcom/google/android/gms/measurement/internal/m;->a:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "name"
iget-object v2, p1, Lcom/google/android/gms/measurement/internal/m;->b:Ljava/lang/String;
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
const-string v1, "lifetime_count"
iget-wide v2, p1, Lcom/google/android/gms/measurement/internal/m;->c:J
invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v2
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
const-string v1, "current_bundle_count"
iget-wide v2, p1, Lcom/google/android/gms/measurement/internal/m;->d:J
invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v2
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
const-string v1, "last_fire_timestamp"
iget-wide v2, p1, Lcom/google/android/gms/measurement/internal/m;->e:J
invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v2
invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
:try_start_3d
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v1
const-string v2, "events"
const/4 v3, 0x0
const/4 v4, 0x5
invoke-virtual {v1, v2, v3, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
move-result-wide v0
const-wide/16 v2, -0x1
cmp-long v0, v0, v2
if-nez v0, :cond_5c
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "Failed to insert/update event aggregates (got -1)"
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:cond_5c
:try_end_5c
.catch Landroid/database/sqlite/SQLiteException; {:try_start_3d .. :try_end_5c} :catch_5d
:goto_5c
return-void
:catch_5d
move-exception v0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Error storing event aggregates"
invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_5c
.end method
.method public b(Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/a;
.registers 22
invoke-static/range {p1 .. p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;
invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/i;->e()V
invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
const/4 v10, 0x0
:try_start_a
invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v2
const-string v3, "apps"
const/16 v4, 0xa
new-array v4, v4, [Ljava/lang/String;
const/4 v5, 0x0
const-string v6, "app_instance_id"
aput-object v6, v4, v5
const/4 v5, 0x1
const-string v6, "gmp_app_id"
aput-object v6, v4, v5
const/4 v5, 0x2
const-string v6, "resettable_device_id_hash"
aput-object v6, v4, v5
const/4 v5, 0x3
const-string v6, "last_bundle_index"
aput-object v6, v4, v5
const/4 v5, 0x4
const-string v6, "last_bundle_end_timestamp"
aput-object v6, v4, v5
const/4 v5, 0x5
const-string v6, "app_version"
aput-object v6, v4, v5
const/4 v5, 0x6
const-string v6, "app_store"
aput-object v6, v4, v5
const/4 v5, 0x7
const-string v6, "gmp_version"
aput-object v6, v4, v5
const/16 v5, 0x8
const-string v6, "dev_cert_hash"
aput-object v6, v4, v5
const/16 v5, 0x9
const-string v6, "measurement_enabled"
aput-object v6, v4, v5
const-string v5, "app_id=?"
const/4 v6, 0x1
new-array v6, v6, [Ljava/lang/String;
const/4 v7, 0x0
aput-object p1, v6, v7
const/4 v7, 0x0
const/4 v8, 0x0
const/4 v9, 0x0
invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
:try_end_56
.catchall {:try_start_a .. :try_end_56} :catchall_f8
.catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_56} :catch_df
move-result-object v19
:try_start_57
invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z
:try_end_5a
.catchall {:try_start_57 .. :try_end_5a} :catchall_ff
.catch Landroid/database/sqlite/SQLiteException; {:try_start_57 .. :try_end_5a} :catch_106
move-result v2
if-nez v2, :cond_64
const/4 v3, 0x0
if-eqz v19, :cond_63
invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V
:cond_63
:goto_63
return-object v3
:cond_64
const/4 v2, 0x0
:try_start_65
move-object/from16 v0, v19
invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
move-result-object v5
const/4 v2, 0x1
move-object/from16 v0, v19
invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
move-result-object v6
const/4 v2, 0x2
move-object/from16 v0, v19
invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
move-result-object v7
const/4 v2, 0x3
move-object/from16 v0, v19
invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J
move-result-wide v8
const/4 v2, 0x4
move-object/from16 v0, v19
invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J
move-result-wide v10
const/4 v2, 0x5
move-object/from16 v0, v19
invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
move-result-object v12
const/4 v2, 0x6
move-object/from16 v0, v19
invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
move-result-object v13
const/4 v2, 0x7
move-object/from16 v0, v19
invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J
move-result-wide v14
const/16 v2, 0x8
move-object/from16 v0, v19
invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J
move-result-wide v16
const/16 v2, 0x9
move-object/from16 v0, v19
invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z
move-result v2
if-eqz v2, :cond_d3
const/4 v2, 0x1
:goto_af
new-instance v3, Lcom/google/android/gms/measurement/internal/a;
if-eqz v2, :cond_dc
const/16 v18, 0x1
:goto_b5
move-object/from16 v4, p1
invoke-direct/range {v3 .. v18}, Lcom/google/android/gms/measurement/internal/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JJZ)V
invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z
move-result v2
if-eqz v2, :cond_cd
invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v2
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v2
const-string v4, "Got multiple records for app, expected one"
invoke-virtual {v2, v4}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:cond_cd
:try_end_cd
.catchall {:try_start_65 .. :try_end_cd} :catchall_ff
.catch Landroid/database/sqlite/SQLiteException; {:try_start_65 .. :try_end_cd} :catch_106
if-eqz v19, :cond_63
invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V
goto :goto_63
:cond_d3
const/16 v2, 0x9
:try_start_d5
move-object/from16 v0, v19
invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I
:try_end_da
.catchall {:try_start_d5 .. :try_end_da} :catchall_ff
.catch Landroid/database/sqlite/SQLiteException; {:try_start_d5 .. :try_end_da} :catch_106
move-result v2
goto :goto_af
:cond_dc
const/16 v18, 0x0
goto :goto_b5
:catch_df
move-exception v2
move-object v4, v10
:goto_e1
:try_start_e1
invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v3
invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v3
const-string v5, "Error querying app"
move-object/from16 v0, p1
invoke-virtual {v3, v5, v0, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
:try_end_f0
.catchall {:try_start_e1 .. :try_end_f0} :catchall_103
const/4 v3, 0x0
if-eqz v4, :cond_63
invoke-interface {v4}, Landroid/database/Cursor;->close()V
goto/16 :goto_63
:catchall_f8
move-exception v2
:goto_f9
if-eqz v10, :cond_fe
invoke-interface {v10}, Landroid/database/Cursor;->close()V
:cond_fe
throw v2
:catchall_ff
move-exception v2
move-object/from16 v10, v19
goto :goto_f9
:catchall_103
move-exception v2
move-object v10, v4
goto :goto_f9
:catch_106
move-exception v2
move-object/from16 v4, v19
goto :goto_e1
.end method
.method  b(Landroid/database/Cursor;I)Ljava/lang/Object;
.registers 7
const/4 v0, 0x0
invoke-static {p1, p2}, Lcom/google/android/gms/measurement/internal/i;->a(Landroid/database/Cursor;I)I
move-result v1
packed-switch v1, :pswitch_data_4e
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v2
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v2
const-string v3, "Loaded invalid unknown value type, ignoring it"
invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v1
invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
:goto_19
return-object v0
:pswitch_1a
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Loaded invalid null value from database"
invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
goto :goto_19
:pswitch_28
invoke-interface {p1, p2}, Landroid/database/Cursor;->getLong(I)J
move-result-wide v0
invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v0
goto :goto_19
:pswitch_31
invoke-interface {p1, p2}, Landroid/database/Cursor;->getFloat(I)F
move-result v0
invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;
move-result-object v0
goto :goto_19
:pswitch_3a
invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
move-result-object v0
goto :goto_19
:pswitch_3f
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Loaded invalid blob type value, ignoring it"
invoke-virtual {v1, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
goto :goto_19
nop
:pswitch_data_4e
.packed-switch 0x0
:pswitch_1a
:pswitch_28
:pswitch_31
:pswitch_3a
:pswitch_3f
.end packed-switch
.end method
.method public b()V
.registers 2
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
return-void
.end method
.method public b(Ljava/lang/String;Ljava/lang/String;)V
.registers 8
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;
invoke-static {p2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/String;)Ljava/lang/String;
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->e()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
:try_start_c
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
const-string v1, "user_attributes"
const-string v2, "app_id=? and name=?"
const/4 v3, 0x2
new-array v3, v3, [Ljava/lang/String;
const/4 v4, 0x0
aput-object p1, v3, v4
const/4 v4, 0x1
aput-object p2, v3, v4
invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
move-result v0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Deleted user attribute rows:"
invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v0
invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
:goto_32
:try_end_32
.catch Landroid/database/sqlite/SQLiteException; {:try_start_c .. :try_end_32} :catch_33
return-void
:catch_33
move-exception v0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Error deleting user attribute"
invoke-virtual {v1, v2, p1, p2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
goto :goto_32
.end method
.method public o()V
.registers 2
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
return-void
.end method
.method public p()V
.registers 2
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
return-void
.end method
.method  q()Landroid/database/sqlite/SQLiteDatabase;
.registers 4
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->e()V
:try_start_3
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/i;->b:Lcom/google/android/gms/measurement/internal/i$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/i$a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
:try_end_8
.catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_8} :catch_a
move-result-object v0
return-object v0
:catch_a
move-exception v0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->o()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Error opening database"
invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
throw v0
.end method
.method public r()Ljava/lang/String;
.registers 6
const/4 v0, 0x0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v1
:try_start_5
const-string v2, "SELECT q.app_id FROM queue q JOIN apps a ON a.app_id=q.app_id WHERE a.measurement_enabled!=0 ORDER BY q.rowid LIMIT 1;"
const/4 v3, 0x0
invoke-virtual {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
:try_end_b
.catchall {:try_start_5 .. :try_end_b} :catchall_38
.catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_b} :catch_23
move-result-object v2
:try_start_c
invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
move-result v1
if-eqz v1, :cond_1d
const/4 v1, 0x0
invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
:try_end_16
.catchall {:try_start_c .. :try_end_16} :catchall_41
.catch Landroid/database/sqlite/SQLiteException; {:try_start_c .. :try_end_16} :catch_43
move-result-object v0
if-eqz v2, :cond_1c
invoke-interface {v2}, Landroid/database/Cursor;->close()V
:goto_1c
:cond_1c
return-object v0
:cond_1d
if-eqz v2, :cond_1c
invoke-interface {v2}, Landroid/database/Cursor;->close()V
goto :goto_1c
:catch_23
move-exception v1
move-object v2, v0
:goto_25
:try_start_25
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v3
invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v3
const-string v4, "Database error getting next bundle app id"
invoke-virtual {v3, v4, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
:try_end_32
.catchall {:try_start_25 .. :try_end_32} :catchall_41
if-eqz v2, :cond_1c
invoke-interface {v2}, Landroid/database/Cursor;->close()V
goto :goto_1c
:catchall_38
move-exception v1
move-object v2, v0
move-object v0, v1
:goto_3b
if-eqz v2, :cond_40
invoke-interface {v2}, Landroid/database/Cursor;->close()V
:cond_40
throw v0
:catchall_41
move-exception v0
goto :goto_3b
:catch_43
move-exception v1
goto :goto_25
.end method
.method  s()V
.registers 7
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->e()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/i;->B()Z
move-result v0
if-nez v0, :cond_d
:cond_c
:goto_c
return-void
:cond_d
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->m()Lcom/google/android/gms/measurement/internal/w;
move-result-object v0
iget-object v0, v0, Lcom/google/android/gms/measurement/internal/w;->f:Lcom/google/android/gms/measurement/internal/w$a;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/w$a;->a()J
move-result-wide v0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->h()Lcom/google/android/gms/internal/e;
move-result-object v2
invoke-interface {v2}, Lcom/google/android/gms/internal/e;->b()J
move-result-wide v2
sub-long v0, v2, v0
invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J
move-result-wide v0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v4
invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/h;->F()J
move-result-wide v4
cmp-long v0, v0, v4
if-lez v0, :cond_c
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->m()Lcom/google/android/gms/measurement/internal/w;
move-result-object v0
iget-object v0, v0, Lcom/google/android/gms/measurement/internal/w;->f:Lcom/google/android/gms/measurement/internal/w$a;
invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/measurement/internal/w$a;->a(J)V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->t()V
goto :goto_c
.end method
.method  t()V
.registers 7
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->e()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->y()V
invoke-direct {p0}, Lcom/google/android/gms/measurement/internal/i;->B()Z
move-result v0
if-nez v0, :cond_d
:cond_c
:goto_c
return-void
:cond_d
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->q()Landroid/database/sqlite/SQLiteDatabase;
move-result-object v0
const/4 v1, 0x2
new-array v1, v1, [Ljava/lang/String;
const/4 v2, 0x0
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->h()Lcom/google/android/gms/internal/e;
move-result-object v3
invoke-interface {v3}, Lcom/google/android/gms/internal/e;->a()J
move-result-wide v4
invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;
move-result-object v3
aput-object v3, v1, v2
const/4 v2, 0x1
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v3
invoke-virtual {v3}, Lcom/google/android/gms/measurement/internal/h;->E()J
move-result-wide v4
invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;
move-result-object v3
aput-object v3, v1, v2
const-string v2, "queue"
const-string v3, "abs(bundle_end_timestamp - ?) > cast(? as integer)"
invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
move-result v0
if-lez v0, :cond_c
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/i;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v1
const-string v2, "Deleted stale rows. rowsDeleted"
invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v0
invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_c
.end method
.method public u()J
.registers 5
const-string v0, "select max(bundle_end_timestamp) from queue"
const/4 v1, 0x0
const-wide/16 v2, 0x0
invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/measurement/internal/i;->a(Ljava/lang/String;[Ljava/lang/String;J)J
move-result-wide v0
return-wide v0
.end method