.class public final Lcom/google/android/gms/measurement/internal/p$a;
.super Ljava/lang/Object;
.field private final a:Ljava/lang/Object;
.field private final b:Lcom/google/android/gms/internal/b;
.field private c:Ljava/lang/Object;
.method private constructor <init>(Lcom/google/android/gms/internal/b;Ljava/lang/Object;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
iput-object p1, p0, Lcom/google/android/gms/measurement/internal/p$a;->b:Lcom/google/android/gms/internal/b;
iput-object p2, p0, Lcom/google/android/gms/measurement/internal/p$a;->a:Ljava/lang/Object;
return-void
.end method
.method static a(Ljava/lang/String;I)Lcom/google/android/gms/measurement/internal/p$a;
.registers 3
invoke-static {p0, p1, p1}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;II)Lcom/google/android/gms/measurement/internal/p$a;
move-result-object v0
return-object v0
.end method
.method static a(Ljava/lang/String;II)Lcom/google/android/gms/measurement/internal/p$a;
.registers 6
new-instance v0, Lcom/google/android/gms/measurement/internal/p$a;
invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v1
invoke-static {p0, v1}, Lcom/google/android/gms/internal/b;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/internal/b;
move-result-object v1
invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
move-result-object v2
invoke-direct {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/p$a;-><init>(Lcom/google/android/gms/internal/b;Ljava/lang/Object;)V
return-object v0
.end method
.method static a(Ljava/lang/String;J)Lcom/google/android/gms/measurement/internal/p$a;
.registers 4
invoke-static {p0, p1, p2, p1, p2}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;JJ)Lcom/google/android/gms/measurement/internal/p$a;
move-result-object v0
return-object v0
.end method
.method static a(Ljava/lang/String;JJ)Lcom/google/android/gms/measurement/internal/p$a;
.registers 8
new-instance v0, Lcom/google/android/gms/measurement/internal/p$a;
invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v1
invoke-static {p0, v1}, Lcom/google/android/gms/internal/b;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/internal/b;
move-result-object v1
invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
move-result-object v2
invoke-direct {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/p$a;-><init>(Lcom/google/android/gms/internal/b;Ljava/lang/Object;)V
return-object v0
.end method
.method static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/p$a;
.registers 3
invoke-static {p0, p1, p1}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/p$a;
move-result-object v0
return-object v0
.end method
.method static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/p$a;
.registers 5
new-instance v0, Lcom/google/android/gms/measurement/internal/p$a;
invoke-static {p0, p2}, Lcom/google/android/gms/internal/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/b;
move-result-object v1
invoke-direct {v0, v1, p1}, Lcom/google/android/gms/measurement/internal/p$a;-><init>(Lcom/google/android/gms/internal/b;Ljava/lang/Object;)V
return-object v0
.end method
.method static a(Ljava/lang/String;Z)Lcom/google/android/gms/measurement/internal/p$a;
.registers 3
invoke-static {p0, p1, p1}, Lcom/google/android/gms/measurement/internal/p$a;->a(Ljava/lang/String;ZZ)Lcom/google/android/gms/measurement/internal/p$a;
move-result-object v0
return-object v0
.end method
.method static a(Ljava/lang/String;ZZ)Lcom/google/android/gms/measurement/internal/p$a;
.registers 6
new-instance v0, Lcom/google/android/gms/measurement/internal/p$a;
invoke-static {p0, p2}, Lcom/google/android/gms/internal/b;->a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/b;
move-result-object v1
invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v2
invoke-direct {v0, v1, v2}, Lcom/google/android/gms/measurement/internal/p$a;-><init>(Lcom/google/android/gms/internal/b;Ljava/lang/Object;)V
return-object v0
.end method
.method public a()Ljava/lang/Object;
.registers 2
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/p$a;->c:Ljava/lang/Object;
if-eqz v0, :cond_7
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/p$a;->c:Ljava/lang/Object;
:goto_6
return-object v0
:cond_7
sget-boolean v0, Lcom/google/android/gms/common/internal/c;->a:Z
if-eqz v0, :cond_18
invoke-static {}, Lcom/google/android/gms/internal/b;->b()Z
move-result v0
if-eqz v0, :cond_18
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/p$a;->b:Lcom/google/android/gms/internal/b;
invoke-virtual {v0}, Lcom/google/android/gms/internal/b;->d()Ljava/lang/Object;
move-result-object v0
goto :goto_6
:cond_18
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/p$a;->a:Ljava/lang/Object;
goto :goto_6
.end method