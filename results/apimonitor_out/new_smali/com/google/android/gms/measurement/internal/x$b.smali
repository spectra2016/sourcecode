.class final Lcom/google/android/gms/measurement/internal/x$b;
.super Ljava/lang/Object;
.implements Ljava/lang/Thread$UncaughtExceptionHandler;
.field final synthetic a:Lcom/google/android/gms/measurement/internal/x;
.field private final b:Ljava/lang/String;
.method public constructor <init>(Lcom/google/android/gms/measurement/internal/x;Ljava/lang/String;)V
.registers 3
iput-object p1, p0, Lcom/google/android/gms/measurement/internal/x$b;->a:Lcom/google/android/gms/measurement/internal/x;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
invoke-static {p2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
iput-object p2, p0, Lcom/google/android/gms/measurement/internal/x$b;->b:Ljava/lang/String;
return-void
.end method
.method public declared-synchronized uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
.registers 5
monitor-enter p0
:try_start_1
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/x$b;->a:Lcom/google/android/gms/measurement/internal/x;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/x;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/x$b;->b:Ljava/lang/String;
invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
:try_end_10
.catchall {:try_start_1 .. :try_end_10} :catchall_12
monitor-exit p0
return-void
:catchall_12
move-exception v0
monitor-exit p0
throw v0
.end method