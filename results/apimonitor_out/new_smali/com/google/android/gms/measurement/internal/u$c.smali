.class  Lcom/google/android/gms/measurement/internal/u$c;
.super Ljava/lang/Object;
.implements Ljava/lang/Runnable;
.field final synthetic a:Lcom/google/android/gms/measurement/internal/u;
.field private final b:Ljava/net/URL;
.field private final c:[B
.field private final d:Lcom/google/android/gms/measurement/internal/u$a;
.method public constructor <init>(Lcom/google/android/gms/measurement/internal/u;Ljava/net/URL;[BLcom/google/android/gms/measurement/internal/u$a;)V
.registers 5
iput-object p1, p0, Lcom/google/android/gms/measurement/internal/u$c;->a:Lcom/google/android/gms/measurement/internal/u;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p2, p0, Lcom/google/android/gms/measurement/internal/u$c;->b:Ljava/net/URL;
iput-object p3, p0, Lcom/google/android/gms/measurement/internal/u$c;->c:[B
iput-object p4, p0, Lcom/google/android/gms/measurement/internal/u$c;->d:Lcom/google/android/gms/measurement/internal/u$a;
return-void
.end method
.method public run()V
.registers 11
const/4 v3, 0x0
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/u$c;->a:Lcom/google/android/gms/measurement/internal/u;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/u;->d()V
const/4 v2, 0x0
:try_start_7
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/u$c;->a:Lcom/google/android/gms/measurement/internal/u;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/u;->j()Lcom/google/android/gms/measurement/internal/f;
move-result-object v0
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/u$c;->c:[B
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/f;->a([B)[B
move-result-object v4
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/u$c;->a:Lcom/google/android/gms/measurement/internal/u;
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/u$c;->b:Ljava/net/URL;
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/u;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
:try_end_1a
.catchall {:try_start_7 .. :try_end_1a} :catchall_a1
.catch Ljava/io/IOException; {:try_start_7 .. :try_end_1a} :catch_6f
move-result-object v1
const/4 v0, 0x1
:try_start_1c
invoke-virtual {v1, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V
const-string v0, "Content-Encoding"
const-string v5, "gzip"
invoke-virtual {v1, v0, v5}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
array-length v0, v4
invoke-virtual {v1, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V
invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V
invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;
:try_end_30
.catchall {:try_start_1c .. :try_end_30} :catchall_d3
.catch Ljava/io/IOException; {:try_start_1c .. :try_end_30} :catch_da
move-result-object v0
:try_start_31
invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write([B)V
invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
:try_end_37
.catchall {:try_start_31 .. :try_end_37} :catchall_d7
.catch Ljava/io/IOException; {:try_start_31 .. :try_end_37} :catch_de
const/4 v0, 0x0
:try_start_38
invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I
move-result v2
iget-object v4, p0, Lcom/google/android/gms/measurement/internal/u$c;->a:Lcom/google/android/gms/measurement/internal/u;
invoke-static {v4, v1}, Lcom/google/android/gms/measurement/internal/u;->a(Lcom/google/android/gms/measurement/internal/u;Ljava/net/HttpURLConnection;)[B
:try_end_41
.catchall {:try_start_38 .. :try_end_41} :catchall_d3
.catch Ljava/io/IOException; {:try_start_38 .. :try_end_41} :catch_da
move-result-object v4
if-eqz v3, :cond_47
:try_start_44
invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
:try_end_47
.catch Ljava/io/IOException; {:try_start_44 .. :try_end_47} :catch_5e
:goto_47
:cond_47
if-eqz v1, :cond_4c
invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V
:cond_4c
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/u$c;->a:Lcom/google/android/gms/measurement/internal/u;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/u;->k()Lcom/google/android/gms/measurement/internal/x;
move-result-object v6
new-instance v0, Lcom/google/android/gms/measurement/internal/u$b;
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/u$c;->d:Lcom/google/android/gms/measurement/internal/u$a;
move-object v5, v3
invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/measurement/internal/u$b;-><init>(Lcom/google/android/gms/measurement/internal/u$a;ILjava/lang/Throwable;[BLcom/google/android/gms/measurement/internal/u$1;)V
invoke-virtual {v6, v0}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V
:goto_5d
return-void
:catch_5e
move-exception v0
iget-object v5, p0, Lcom/google/android/gms/measurement/internal/u$c;->a:Lcom/google/android/gms/measurement/internal/u;
invoke-virtual {v5}, Lcom/google/android/gms/measurement/internal/u;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v5
invoke-virtual {v5}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v5
const-string v6, "Error closing HTTP compressed POST connection output stream"
invoke-virtual {v5, v6, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_47
:catch_6f
move-exception v7
move v6, v2
move-object v0, v3
move-object v1, v3
:goto_73
if-eqz v0, :cond_78
:try_start_75
invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
:cond_78
:goto_78
:try_end_78
.catch Ljava/io/IOException; {:try_start_75 .. :try_end_78} :catch_90
if-eqz v1, :cond_7d
invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V
:cond_7d
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/u$c;->a:Lcom/google/android/gms/measurement/internal/u;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/u;->k()Lcom/google/android/gms/measurement/internal/x;
move-result-object v0
new-instance v4, Lcom/google/android/gms/measurement/internal/u$b;
iget-object v5, p0, Lcom/google/android/gms/measurement/internal/u$c;->d:Lcom/google/android/gms/measurement/internal/u$a;
move-object v8, v3
move-object v9, v3
invoke-direct/range {v4 .. v9}, Lcom/google/android/gms/measurement/internal/u$b;-><init>(Lcom/google/android/gms/measurement/internal/u$a;ILjava/lang/Throwable;[BLcom/google/android/gms/measurement/internal/u$1;)V
invoke-virtual {v0, v4}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V
goto :goto_5d
:catch_90
move-exception v0
iget-object v2, p0, Lcom/google/android/gms/measurement/internal/u$c;->a:Lcom/google/android/gms/measurement/internal/u;
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/u;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v2
invoke-virtual {v2}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v2
const-string v4, "Error closing HTTP compressed POST connection output stream"
invoke-virtual {v2, v4, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_78
:catchall_a1
move-exception v0
move-object v6, v0
move-object v1, v3
move-object v0, v3
:goto_a5
if-eqz v0, :cond_aa
:try_start_a7
invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
:cond_aa
:try_end_aa
.catch Ljava/io/IOException; {:try_start_a7 .. :try_end_aa} :catch_c2
:goto_aa
if-eqz v1, :cond_af
invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V
:cond_af
iget-object v0, p0, Lcom/google/android/gms/measurement/internal/u$c;->a:Lcom/google/android/gms/measurement/internal/u;
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/u;->k()Lcom/google/android/gms/measurement/internal/x;
move-result-object v7
new-instance v0, Lcom/google/android/gms/measurement/internal/u$b;
iget-object v1, p0, Lcom/google/android/gms/measurement/internal/u$c;->d:Lcom/google/android/gms/measurement/internal/u$a;
move-object v4, v3
move-object v5, v3
invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/measurement/internal/u$b;-><init>(Lcom/google/android/gms/measurement/internal/u$a;ILjava/lang/Throwable;[BLcom/google/android/gms/measurement/internal/u$1;)V
invoke-virtual {v7, v0}, Lcom/google/android/gms/measurement/internal/x;->a(Ljava/lang/Runnable;)V
throw v6
:catch_c2
move-exception v0
iget-object v4, p0, Lcom/google/android/gms/measurement/internal/u$c;->a:Lcom/google/android/gms/measurement/internal/u;
invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/u;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v4
invoke-virtual {v4}, Lcom/google/android/gms/measurement/internal/t;->b()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v4
const-string v5, "Error closing HTTP compressed POST connection output stream"
invoke-virtual {v4, v5, v0}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_aa
:catchall_d3
move-exception v0
move-object v6, v0
move-object v0, v3
goto :goto_a5
:catchall_d7
move-exception v4
move-object v6, v4
goto :goto_a5
:catch_da
move-exception v7
move v6, v2
move-object v0, v3
goto :goto_73
:catch_de
move-exception v7
move v6, v2
goto :goto_73
.end method