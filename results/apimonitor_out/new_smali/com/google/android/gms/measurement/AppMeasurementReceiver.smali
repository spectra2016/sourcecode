.class public final Lcom/google/android/gms/measurement/AppMeasurementReceiver;
.super Landroid/content/BroadcastReceiver;
.field static final a:Ljava/lang/Object;
.field static b:Landroid/os/PowerManager$WakeLock;
.field static c:Ljava/lang/Boolean;
.method static constructor <clinit>()V
.registers 1
new-instance v0, Ljava/lang/Object;
invoke-direct {v0}, Ljava/lang/Object;-><init>()V
sput-object v0, Lcom/google/android/gms/measurement/AppMeasurementReceiver;->a:Ljava/lang/Object;
return-void
.end method
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V
return-void
.end method
.method public static a(Landroid/content/Context;)Z
.registers 3
invoke-static {p0}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
sget-object v0, Lcom/google/android/gms/measurement/AppMeasurementReceiver;->c:Ljava/lang/Boolean;
if-eqz v0, :cond_e
sget-object v0, Lcom/google/android/gms/measurement/AppMeasurementReceiver;->c:Ljava/lang/Boolean;
invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
move-result v0
:goto_d
return v0
:cond_e
const-class v0, Lcom/google/android/gms/measurement/AppMeasurementReceiver;
const/4 v1, 0x0
invoke-static {p0, v0, v1}, Lcom/google/android/gms/measurement/internal/f;->a(Landroid/content/Context;Ljava/lang/Class;Z)Z
move-result v0
invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v1
sput-object v1, Lcom/google/android/gms/measurement/AppMeasurementReceiver;->c:Ljava/lang/Boolean;
goto :goto_d
.end method
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.registers 9
invoke-static {p1}, Lcom/google/android/gms/measurement/internal/y;->a(Landroid/content/Context;)Lcom/google/android/gms/measurement/internal/y;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->f()Lcom/google/android/gms/measurement/internal/t;
move-result-object v1
invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
move-result-object v2
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/y;->d()Lcom/google/android/gms/measurement/internal/h;
move-result-object v0
invoke-virtual {v0}, Lcom/google/android/gms/measurement/internal/h;->C()Z
move-result v0
if-eqz v0, :cond_41
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v3, "Device AppMeasurementReceiver got"
invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
:goto_1f
const-string v0, "com.google.android.gms.measurement.UPLOAD"
invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v0
if-eqz v0, :cond_40
invoke-static {p1}, Lcom/google/android/gms/measurement/AppMeasurementService;->a(Landroid/content/Context;)Z
move-result v0
new-instance v2, Landroid/content/Intent;
const-class v3, Lcom/google/android/gms/measurement/AppMeasurementService;
invoke-direct {v2, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V
const-string v3, "com.google.android.gms.measurement.UPLOAD"
invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
sget-object v3, Lcom/google/android/gms/measurement/AppMeasurementReceiver;->a:Ljava/lang/Object;
monitor-enter v3
:try_start_3a
invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
if-nez v0, :cond_4b
monitor-exit v3
:cond_40
:goto_40
:try_end_40
.catchall {:try_start_3a .. :try_end_40} :catchall_6f
return-void
:cond_41
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->t()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v3, "Local AppMeasurementReceiver got"
invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_1f
:try_start_4b
:cond_4b
const-string v0, "power"
invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/os/PowerManager;
sget-object v2, Lcom/google/android/gms/measurement/AppMeasurementReceiver;->b:Landroid/os/PowerManager$WakeLock;
if-nez v2, :cond_66
const/4 v2, 0x1
const-string v4, "AppMeasurement WakeLock"
invoke-virtual {v0, v2, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
move-result-object v0
sput-object v0, Lcom/google/android/gms/measurement/AppMeasurementReceiver;->b:Landroid/os/PowerManager$WakeLock;
sget-object v0, Lcom/google/android/gms/measurement/AppMeasurementReceiver;->b:Landroid/os/PowerManager$WakeLock;
const/4 v2, 0x0
invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V
:cond_66
sget-object v0, Lcom/google/android/gms/measurement/AppMeasurementReceiver;->b:Landroid/os/PowerManager$WakeLock;
const-wide/16 v4, 0x3e8
invoke-virtual {v0, v4, v5}, Landroid/os/PowerManager$WakeLock;->acquire(J)V
:goto_6d
:try_end_6d
.catchall {:try_start_4b .. :try_end_6d} :catchall_6f
.catch Ljava/lang/SecurityException; {:try_start_4b .. :try_end_6d} :catch_72
:try_start_6d
monitor-exit v3
goto :goto_40
:catchall_6f
move-exception v0
monitor-exit v3
:try_end_71
.catchall {:try_start_6d .. :try_end_71} :catchall_6f
throw v0
:catch_72
move-exception v0
:try_start_73
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/t;->o()Lcom/google/android/gms/measurement/internal/t$a;
move-result-object v0
const-string v1, "AppMeasurementService at risk of not starting. For more reliable app measurements, add the WAKE_LOCK permission to your manifest."
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/t$a;->a(Ljava/lang/String;)V
:try_end_7c
.catchall {:try_start_73 .. :try_end_7c} :catchall_6f
goto :goto_6d
.end method