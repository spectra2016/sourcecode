.class public Lcom/google/android/gms/measurement/internal/u;
.super Lcom/google/android/gms/measurement/internal/ab;
.method public constructor <init>(Lcom/google/android/gms/measurement/internal/y;)V
.registers 2
invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/ab;-><init>(Lcom/google/android/gms/measurement/internal/y;)V
return-void
.end method
.method static synthetic a(Lcom/google/android/gms/measurement/internal/u;Ljava/net/HttpURLConnection;)[B
.registers 3
invoke-direct {p0, p1}, Lcom/google/android/gms/measurement/internal/u;->a(Ljava/net/HttpURLConnection;)[B
move-result-object v0
return-object v0
.end method
.method private a(Ljava/net/HttpURLConnection;)[B
.registers 7
const/4 v1, 0x0
:try_start_1
new-instance v0, Ljava/io/ByteArrayOutputStream;
invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V
invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
move-result-object v1
const/16 v2, 0x400
new-array v2, v2, [B
:goto_e
invoke-virtual {v1, v2}, Ljava/io/InputStream;->read([B)I
move-result v3
if-lez v3, :cond_20
const/4 v4, 0x0
invoke-virtual {v0, v2, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
:try_end_18
.catchall {:try_start_1 .. :try_end_18} :catchall_19
goto :goto_e
:catchall_19
move-exception v0
if-eqz v1, :cond_1f
invoke-virtual {v1}, Ljava/io/InputStream;->close()V
:cond_1f
throw v0
:cond_20
:try_start_20
invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
:try_end_23
.catchall {:try_start_20 .. :try_end_23} :catchall_19
move-result-object v0
if-eqz v1, :cond_29
invoke-virtual {v1}, Ljava/io/InputStream;->close()V
:cond_29
return-object v0
.end method
.method protected a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
.registers 7
const/4 v4, 0x0
invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;
move-result-object v0
instance-of v1, v0, Ljava/net/HttpURLConnection;
if-nez v1, :cond_11
new-instance v0, Ljava/io/IOException;
const-string v1, "Failed to obtain HTTP connection"
invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V
throw v0
:cond_11
check-cast v0, Ljava/net/HttpURLConnection;
invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setDefaultUseCaches(Z)V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/u;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/h;->v()J
move-result-wide v2
long-to-int v1, v2
invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/u;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v1
invoke-virtual {v1}, Lcom/google/android/gms/measurement/internal/h;->w()J
move-result-wide v2
long-to-int v1, v2
invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V
invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V
const/4 v1, 0x1
invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V
return-object v0
.end method
.method protected a()V
.registers 1
return-void
.end method
.method public a(Ljava/net/URL;[BLcom/google/android/gms/measurement/internal/u$a;)V
.registers 6
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/u;->e()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/u;->y()V
invoke-static {p1}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
invoke-static {p2}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
invoke-static {p3}, Lcom/google/android/gms/common/internal/p;->a(Ljava/lang/Object;)Ljava/lang/Object;
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/u;->k()Lcom/google/android/gms/measurement/internal/x;
move-result-object v0
new-instance v1, Lcom/google/android/gms/measurement/internal/u$c;
invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/gms/measurement/internal/u$c;-><init>(Lcom/google/android/gms/measurement/internal/u;Ljava/net/URL;[BLcom/google/android/gms/measurement/internal/u$a;)V
invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/internal/x;->b(Ljava/lang/Runnable;)V
return-void
.end method
.method public b()Z
.registers 3
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/u;->y()V
invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/u;->i()Landroid/content/Context;
move-result-object v0
const-string v1, "connectivity"
invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/net/ConnectivityManager;
const/4 v1, 0x0
:try_start_10
invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;
:try_end_13
.catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_13} :catch_1e
move-result-object v0
:goto_14
if-eqz v0, :cond_21
invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z
move-result v0
if-eqz v0, :cond_21
const/4 v0, 0x1
:goto_1d
return v0
:catch_1e
move-exception v0
move-object v0, v1
goto :goto_14
:cond_21
const/4 v0, 0x0
goto :goto_1d
.end method
.method public bridge synthetic c()V
.registers 1
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->c()V
return-void
.end method
.method public bridge synthetic d()V
.registers 1
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->d()V
return-void
.end method
.method public bridge synthetic e()V
.registers 1
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->e()V
return-void
.end method
.method public bridge synthetic f()Lcom/google/android/gms/measurement/internal/r;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->f()Lcom/google/android/gms/measurement/internal/r;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic g()Lcom/google/android/gms/measurement/internal/ae;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->g()Lcom/google/android/gms/measurement/internal/ae;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic h()Lcom/google/android/gms/internal/e;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->h()Lcom/google/android/gms/internal/e;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic i()Landroid/content/Context;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->i()Landroid/content/Context;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic j()Lcom/google/android/gms/measurement/internal/f;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->j()Lcom/google/android/gms/measurement/internal/f;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic k()Lcom/google/android/gms/measurement/internal/x;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->k()Lcom/google/android/gms/measurement/internal/x;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic l()Lcom/google/android/gms/measurement/internal/t;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->l()Lcom/google/android/gms/measurement/internal/t;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic m()Lcom/google/android/gms/measurement/internal/w;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->m()Lcom/google/android/gms/measurement/internal/w;
move-result-object v0
return-object v0
.end method
.method public bridge synthetic n()Lcom/google/android/gms/measurement/internal/h;
.registers 2
invoke-super {p0}, Lcom/google/android/gms/measurement/internal/ab;->n()Lcom/google/android/gms/measurement/internal/h;
move-result-object v0
return-object v0
.end method