.class public abstract Lcom/google/android/gms/measurement/internal/q$a;
.super Landroid/os/Binder;
.implements Lcom/google/android/gms/measurement/internal/q;
.method public constructor <init>()V
.registers 2
invoke-direct {p0}, Landroid/os/Binder;-><init>()V
const-string v0, "com.google.android.gms.measurement.internal.IMeasurementService"
invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/measurement/internal/q$a;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return-void
.end method
.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/measurement/internal/q;
.registers 3
if-nez p0, :cond_4
const/4 v0, 0x0
:goto_3
return-object v0
:cond_4
const-string v0, "com.google.android.gms.measurement.internal.IMeasurementService"
invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;
move-result-object v0
if-eqz v0, :cond_13
instance-of v1, v0, Lcom/google/android/gms/measurement/internal/q;
if-eqz v1, :cond_13
check-cast v0, Lcom/google/android/gms/measurement/internal/q;
goto :goto_3
:cond_13
new-instance v0, Lcom/google/android/gms/measurement/internal/q$a$a;
invoke-direct {v0, p0}, Lcom/google/android/gms/measurement/internal/q$a$a;-><init>(Landroid/os/IBinder;)V
goto :goto_3
.end method
.method public asBinder()Landroid/os/IBinder;
.registers 1
return-object p0
.end method
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.registers 9
const/4 v2, 0x1
const/4 v1, 0x0
sparse-switch p1, :sswitch_data_b4
invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
move-result v0
:goto_9
return v0
:sswitch_a
const-string v0, "com.google.android.gms.measurement.internal.IMeasurementService"
invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
move v0, v2
goto :goto_9
:sswitch_11
const-string v0, "com.google.android.gms.measurement.internal.IMeasurementService"
invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
move-result v0
if-eqz v0, :cond_36
sget-object v0, Lcom/google/android/gms/measurement/internal/EventParcel;->CREATOR:Lcom/google/android/gms/measurement/internal/o;
invoke-virtual {v0, p2}, Lcom/google/android/gms/measurement/internal/o;->a(Landroid/os/Parcel;)Lcom/google/android/gms/measurement/internal/EventParcel;
move-result-object v0
:goto_22
invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
move-result v3
if-eqz v3, :cond_2e
sget-object v1, Lcom/google/android/gms/measurement/internal/AppMetadata;->CREATOR:Lcom/google/android/gms/measurement/internal/g;
invoke-virtual {v1, p2}, Lcom/google/android/gms/measurement/internal/g;->a(Landroid/os/Parcel;)Lcom/google/android/gms/measurement/internal/AppMetadata;
move-result-object v1
:cond_2e
invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/measurement/internal/q$a;->a(Lcom/google/android/gms/measurement/internal/EventParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
move v0, v2
goto :goto_9
:cond_36
move-object v0, v1
goto :goto_22
:sswitch_38
const-string v0, "com.google.android.gms.measurement.internal.IMeasurementService"
invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
move-result v0
if-eqz v0, :cond_5d
sget-object v0, Lcom/google/android/gms/measurement/internal/UserAttributeParcel;->CREATOR:Lcom/google/android/gms/measurement/internal/e;
invoke-virtual {v0, p2}, Lcom/google/android/gms/measurement/internal/e;->a(Landroid/os/Parcel;)Lcom/google/android/gms/measurement/internal/UserAttributeParcel;
move-result-object v0
:goto_49
invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
move-result v3
if-eqz v3, :cond_55
sget-object v1, Lcom/google/android/gms/measurement/internal/AppMetadata;->CREATOR:Lcom/google/android/gms/measurement/internal/g;
invoke-virtual {v1, p2}, Lcom/google/android/gms/measurement/internal/g;->a(Landroid/os/Parcel;)Lcom/google/android/gms/measurement/internal/AppMetadata;
move-result-object v1
:cond_55
invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/measurement/internal/q$a;->a(Lcom/google/android/gms/measurement/internal/UserAttributeParcel;Lcom/google/android/gms/measurement/internal/AppMetadata;)V
invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
move v0, v2
goto :goto_9
:cond_5d
move-object v0, v1
goto :goto_49
:sswitch_5f
const-string v0, "com.google.android.gms.measurement.internal.IMeasurementService"
invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
move-result v0
if-eqz v0, :cond_70
sget-object v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->CREATOR:Lcom/google/android/gms/measurement/internal/g;
invoke-virtual {v0, p2}, Lcom/google/android/gms/measurement/internal/g;->a(Landroid/os/Parcel;)Lcom/google/android/gms/measurement/internal/AppMetadata;
move-result-object v1
:cond_70
invoke-virtual {p0, v1}, Lcom/google/android/gms/measurement/internal/q$a;->a(Lcom/google/android/gms/measurement/internal/AppMetadata;)V
invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
move v0, v2
goto :goto_9
:sswitch_78
const-string v0, "com.google.android.gms.measurement.internal.IMeasurementService"
invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
move-result v0
if-eqz v0, :cond_89
sget-object v0, Lcom/google/android/gms/measurement/internal/EventParcel;->CREATOR:Lcom/google/android/gms/measurement/internal/o;
invoke-virtual {v0, p2}, Lcom/google/android/gms/measurement/internal/o;->a(Landroid/os/Parcel;)Lcom/google/android/gms/measurement/internal/EventParcel;
move-result-object v1
:cond_89
invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
move-result-object v0
invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
move-result-object v3
invoke-virtual {p0, v1, v0, v3}, Lcom/google/android/gms/measurement/internal/q$a;->a(Lcom/google/android/gms/measurement/internal/EventParcel;Ljava/lang/String;Ljava/lang/String;)V
invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
move v0, v2
goto/16 :goto_9
:sswitch_9a
const-string v0, "com.google.android.gms.measurement.internal.IMeasurementService"
invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
move-result v0
if-eqz v0, :cond_ab
sget-object v0, Lcom/google/android/gms/measurement/internal/AppMetadata;->CREATOR:Lcom/google/android/gms/measurement/internal/g;
invoke-virtual {v0, p2}, Lcom/google/android/gms/measurement/internal/g;->a(Landroid/os/Parcel;)Lcom/google/android/gms/measurement/internal/AppMetadata;
move-result-object v1
:cond_ab
invoke-virtual {p0, v1}, Lcom/google/android/gms/measurement/internal/q$a;->b(Lcom/google/android/gms/measurement/internal/AppMetadata;)V
invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
move v0, v2
goto/16 :goto_9
:sswitch_data_b4
.sparse-switch
0x1 -> :sswitch_11
0x2 -> :sswitch_38
0x4 -> :sswitch_5f
0x5 -> :sswitch_78
0x6 -> :sswitch_9a
0x5f4e5446 -> :sswitch_a
.end sparse-switch
.end method