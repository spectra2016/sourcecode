.class public final Lcom/google/android/gms/internal/o;
.super Ljava/lang/Object;
.field public static final a:Ljava/lang/Object;
.method static constructor <clinit>()V
.registers 1
new-instance v0, Ljava/lang/Object;
invoke-direct {v0}, Ljava/lang/Object;-><init>()V
sput-object v0, Lcom/google/android/gms/internal/o;->a:Ljava/lang/Object;
return-void
.end method
.method public static a([Ljava/lang/Object;)I
.registers 5
const/4 v1, 0x0
if-nez p0, :cond_15
move v0, v1
:goto_4
move v2, v1
:goto_5
if-ge v2, v0, :cond_17
aget-object v3, p0, v2
if-eqz v3, :cond_12
mul-int/lit8 v1, v1, 0x1f
invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I
move-result v3
add-int/2addr v1, v3
:cond_12
add-int/lit8 v2, v2, 0x1
goto :goto_5
:cond_15
array-length v0, p0
goto :goto_4
:cond_17
return v1
.end method
.method public static a([Ljava/lang/Object;[Ljava/lang/Object;)Z
.registers 10
const/4 v2, 0x1
const/4 v1, 0x0
if-nez p0, :cond_14
move v7, v1
:goto_5
if-nez p1, :cond_17
move v0, v1
:goto_8
move v3, v1
move v6, v1
:goto_a
if-ge v6, v7, :cond_45
aget-object v4, p0, v6
if-nez v4, :cond_45
add-int/lit8 v4, v6, 0x1
move v6, v4
goto :goto_a
:cond_14
array-length v0, p0
move v7, v0
goto :goto_5
:cond_17
array-length v0, p1
goto :goto_8
:goto_19
if-ge v5, v0, :cond_23
aget-object v3, p1, v5
if-nez v3, :cond_23
add-int/lit8 v3, v5, 0x1
move v5, v3
goto :goto_19
:cond_23
if-lt v6, v7, :cond_2f
move v4, v2
:goto_26
if-lt v5, v0, :cond_31
move v3, v2
:goto_29
if-eqz v4, :cond_33
if-eqz v3, :cond_33
move v1, v2
:cond_2e
return v1
:cond_2f
move v4, v1
goto :goto_26
:cond_31
move v3, v1
goto :goto_29
:cond_33
if-ne v4, v3, :cond_2e
aget-object v3, p0, v6
aget-object v4, p1, v5
invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
move-result v3
if-eqz v3, :cond_2e
add-int/lit8 v4, v6, 0x1
add-int/lit8 v3, v5, 0x1
move v6, v4
goto :goto_a
:cond_45
move v5, v3
goto :goto_19
.end method