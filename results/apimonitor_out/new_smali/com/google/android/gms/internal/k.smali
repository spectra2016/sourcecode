.class public final Lcom/google/android/gms/internal/k;
.super Ljava/lang/Object;
.field public static final a:Lcom/google/android/gms/common/api/a$b;
.field public static final b:Lcom/google/android/gms/common/api/a$b;
.field public static final c:Lcom/google/android/gms/common/api/a$a;
.field static final d:Lcom/google/android/gms/common/api/a$a;
.field public static final e:Lcom/google/android/gms/common/api/Scope;
.field public static final f:Lcom/google/android/gms/common/api/Scope;
.field public static final g:Lcom/google/android/gms/common/api/a;
.field public static final h:Lcom/google/android/gms/common/api/a;
.field public static final i:Lcom/google/android/gms/internal/l;
.method static constructor <clinit>()V
.registers 4
new-instance v0, Lcom/google/android/gms/common/api/a$b;
invoke-direct {v0}, Lcom/google/android/gms/common/api/a$b;-><init>()V
sput-object v0, Lcom/google/android/gms/internal/k;->a:Lcom/google/android/gms/common/api/a$b;
new-instance v0, Lcom/google/android/gms/common/api/a$b;
invoke-direct {v0}, Lcom/google/android/gms/common/api/a$b;-><init>()V
sput-object v0, Lcom/google/android/gms/internal/k;->b:Lcom/google/android/gms/common/api/a$b;
new-instance v0, Lcom/google/android/gms/internal/k$1;
invoke-direct {v0}, Lcom/google/android/gms/internal/k$1;-><init>()V
sput-object v0, Lcom/google/android/gms/internal/k;->c:Lcom/google/android/gms/common/api/a$a;
new-instance v0, Lcom/google/android/gms/internal/k$2;
invoke-direct {v0}, Lcom/google/android/gms/internal/k$2;-><init>()V
sput-object v0, Lcom/google/android/gms/internal/k;->d:Lcom/google/android/gms/common/api/a$a;
new-instance v0, Lcom/google/android/gms/common/api/Scope;
const-string v1, "profile"
invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V
sput-object v0, Lcom/google/android/gms/internal/k;->e:Lcom/google/android/gms/common/api/Scope;
new-instance v0, Lcom/google/android/gms/common/api/Scope;
const-string v1, "email"
invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V
sput-object v0, Lcom/google/android/gms/internal/k;->f:Lcom/google/android/gms/common/api/Scope;
new-instance v0, Lcom/google/android/gms/common/api/a;
const-string v1, "SignIn.API"
sget-object v2, Lcom/google/android/gms/internal/k;->c:Lcom/google/android/gms/common/api/a$a;
sget-object v3, Lcom/google/android/gms/internal/k;->a:Lcom/google/android/gms/common/api/a$b;
invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/a;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/api/a$a;Lcom/google/android/gms/common/api/a$b;)V
sput-object v0, Lcom/google/android/gms/internal/k;->g:Lcom/google/android/gms/common/api/a;
new-instance v0, Lcom/google/android/gms/common/api/a;
const-string v1, "SignIn.INTERNAL_API"
sget-object v2, Lcom/google/android/gms/internal/k;->d:Lcom/google/android/gms/common/api/a$a;
sget-object v3, Lcom/google/android/gms/internal/k;->b:Lcom/google/android/gms/common/api/a$b;
invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/a;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/api/a$a;Lcom/google/android/gms/common/api/a$b;)V
sput-object v0, Lcom/google/android/gms/internal/k;->h:Lcom/google/android/gms/common/api/a;
new-instance v0, Lcom/google/android/gms/a/a/a;
invoke-direct {v0}, Lcom/google/android/gms/a/a/a;-><init>()V
sput-object v0, Lcom/google/android/gms/internal/k;->i:Lcom/google/android/gms/internal/l;
return-void
.end method