.class public abstract Lcom/google/android/gms/internal/b;
.super Ljava/lang/Object;
.field private static final c:Ljava/lang/Object;
.field private static d:Lcom/google/android/gms/internal/b$a;
.field private static e:I
.field private static f:Ljava/lang/String;
.field protected final a:Ljava/lang/String;
.field protected final b:Ljava/lang/Object;
.field private g:Ljava/lang/Object;
.method static constructor <clinit>()V
.registers 1
new-instance v0, Ljava/lang/Object;
invoke-direct {v0}, Ljava/lang/Object;-><init>()V
sput-object v0, Lcom/google/android/gms/internal/b;->c:Ljava/lang/Object;
const/4 v0, 0x0
sput-object v0, Lcom/google/android/gms/internal/b;->d:Lcom/google/android/gms/internal/b$a;
const/4 v0, 0x0
sput v0, Lcom/google/android/gms/internal/b;->e:I
const-string v0, "com.google.android.providers.gsf.permission.READ_GSERVICES"
sput-object v0, Lcom/google/android/gms/internal/b;->f:Ljava/lang/String;
return-void
.end method
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, 0x0
iput-object v0, p0, Lcom/google/android/gms/internal/b;->g:Ljava/lang/Object;
iput-object p1, p0, Lcom/google/android/gms/internal/b;->a:Ljava/lang/String;
iput-object p2, p0, Lcom/google/android/gms/internal/b;->b:Ljava/lang/Object;
return-void
.end method
.method public static a()I
.registers 1
sget v0, Lcom/google/android/gms/internal/b;->e:I
return v0
.end method
.method public static a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/internal/b;
.registers 3
new-instance v0, Lcom/google/android/gms/internal/b$3;
invoke-direct {v0, p0, p1}, Lcom/google/android/gms/internal/b$3;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V
return-object v0
.end method
.method public static a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/internal/b;
.registers 3
new-instance v0, Lcom/google/android/gms/internal/b$2;
invoke-direct {v0, p0, p1}, Lcom/google/android/gms/internal/b$2;-><init>(Ljava/lang/String;Ljava/lang/Long;)V
return-object v0
.end method
.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/internal/b;
.registers 3
new-instance v0, Lcom/google/android/gms/internal/b$4;
invoke-direct {v0, p0, p1}, Lcom/google/android/gms/internal/b$4;-><init>(Ljava/lang/String;Ljava/lang/String;)V
return-object v0
.end method
.method public static a(Ljava/lang/String;Z)Lcom/google/android/gms/internal/b;
.registers 4
new-instance v0, Lcom/google/android/gms/internal/b$1;
invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v1
invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/b$1;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V
return-object v0
.end method
.method public static b()Z
.registers 1
sget-object v0, Lcom/google/android/gms/internal/b;->d:Lcom/google/android/gms/internal/b$a;
if-eqz v0, :cond_6
const/4 v0, 0x1
:goto_5
return v0
:cond_6
const/4 v0, 0x0
goto :goto_5
.end method
.method static synthetic e()Lcom/google/android/gms/internal/b$a;
.registers 1
sget-object v0, Lcom/google/android/gms/internal/b;->d:Lcom/google/android/gms/internal/b$a;
return-object v0
.end method
.method protected abstract a(Ljava/lang/String;)Ljava/lang/Object;
.end method
.method public final c()Ljava/lang/Object;
.registers 2
iget-object v0, p0, Lcom/google/android/gms/internal/b;->g:Ljava/lang/Object;
if-eqz v0, :cond_7
iget-object v0, p0, Lcom/google/android/gms/internal/b;->g:Ljava/lang/Object;
:goto_6
return-object v0
:cond_7
iget-object v0, p0, Lcom/google/android/gms/internal/b;->a:Ljava/lang/String;
invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/b;->a(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
goto :goto_6
.end method
.method public final d()Ljava/lang/Object;
.registers 4
invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
move-result-wide v0
:try_start_4
invoke-virtual {p0}, Lcom/google/android/gms/internal/b;->c()Ljava/lang/Object;
:try_end_7
.catchall {:try_start_4 .. :try_end_7} :catchall_c
move-result-object v2
invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V
return-object v2
:catchall_c
move-exception v2
invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V
throw v2
.end method