.class public abstract Lcom/google/android/gms/internal/p;
.super Ljava/lang/Object;
.field protected volatile A:I
.method public constructor <init>()V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/4 v0, -0x1
iput v0, p0, Lcom/google/android/gms/internal/p;->A:I
return-void
.end method
.method public a(Lcom/google/android/gms/internal/zztd;)V
.registers 2
return-void
.end method
.method public abstract b(Lcom/google/android/gms/internal/n;)Lcom/google/android/gms/internal/p;
.end method
.method protected c()I
.registers 2
const/4 v0, 0x0
return v0
.end method
.method public synthetic clone()Ljava/lang/Object;
.registers 2
invoke-virtual {p0}, Lcom/google/android/gms/internal/p;->f()Lcom/google/android/gms/internal/p;
move-result-object v0
return-object v0
.end method
.method public d()I
.registers 2
iget v0, p0, Lcom/google/android/gms/internal/p;->A:I
if-gez v0, :cond_7
invoke-virtual {p0}, Lcom/google/android/gms/internal/p;->e()I
:cond_7
iget v0, p0, Lcom/google/android/gms/internal/p;->A:I
return v0
.end method
.method public e()I
.registers 2
invoke-virtual {p0}, Lcom/google/android/gms/internal/p;->c()I
move-result v0
iput v0, p0, Lcom/google/android/gms/internal/p;->A:I
return v0
.end method
.method public f()Lcom/google/android/gms/internal/p;
.registers 2
invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lcom/google/android/gms/internal/p;
return-object v0
.end method
.method public toString()Ljava/lang/String;
.registers 2
invoke-static {p0}, Lcom/google/android/gms/internal/q;->a(Lcom/google/android/gms/internal/p;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method