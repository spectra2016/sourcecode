.class public final Lcom/google/android/gms/internal/f;
.super Ljava/lang/Object;
.implements Lcom/google/android/gms/internal/e;
.field private static a:Lcom/google/android/gms/internal/f;
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public static declared-synchronized c()Lcom/google/android/gms/internal/e;
.registers 2
const-class v1, Lcom/google/android/gms/internal/f;
monitor-enter v1
:try_start_3
sget-object v0, Lcom/google/android/gms/internal/f;->a:Lcom/google/android/gms/internal/f;
if-nez v0, :cond_e
new-instance v0, Lcom/google/android/gms/internal/f;
invoke-direct {v0}, Lcom/google/android/gms/internal/f;-><init>()V
sput-object v0, Lcom/google/android/gms/internal/f;->a:Lcom/google/android/gms/internal/f;
:cond_e
sget-object v0, Lcom/google/android/gms/internal/f;->a:Lcom/google/android/gms/internal/f;
:try_end_10
.catchall {:try_start_3 .. :try_end_10} :catchall_12
monitor-exit v1
return-object v0
:catchall_12
move-exception v0
monitor-exit v1
throw v0
.end method
.method public a()J
.registers 3
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v0
return-wide v0
.end method
.method public b()J
.registers 3
invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
move-result-wide v0
return-wide v0
.end method