.class  Lcom/crashlytics/android/core/InvalidSessionReport;
.super Ljava/lang/Object;
.source "InvalidSessionReport.java"
.implements Lcom/crashlytics/android/core/Report;
.field private final customHeaders:Ljava/util/Map;
.field private final files:[Ljava/io/File;
.field private final identifier:Ljava/lang/String;
.method public constructor <init>(Ljava/lang/String;[Ljava/io/File;)V
.registers 5
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p2, p0, Lcom/crashlytics/android/core/InvalidSessionReport;->files:[Ljava/io/File;
new-instance v0, Ljava/util/HashMap;
sget-object v1, Lcom/crashlytics/android/core/ReportUploader;->HEADER_INVALID_CLS_FILE:Ljava/util/Map;
invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V
iput-object v0, p0, Lcom/crashlytics/android/core/InvalidSessionReport;->customHeaders:Ljava/util/Map;
iput-object p1, p0, Lcom/crashlytics/android/core/InvalidSessionReport;->identifier:Ljava/lang/String;
return-void
.end method
.method public getCustomHeaders()Ljava/util/Map;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/InvalidSessionReport;->customHeaders:Ljava/util/Map;
invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
move-result-object v0
return-object v0
.end method
.method public getFile()Ljava/io/File;
.registers 3
iget-object v0, p0, Lcom/crashlytics/android/core/InvalidSessionReport;->files:[Ljava/io/File;
const/4 v1, 0x0
aget-object v0, v0, v1
return-object v0
.end method
.method public getFileName()Ljava/lang/String;
.registers 3
iget-object v0, p0, Lcom/crashlytics/android/core/InvalidSessionReport;->files:[Ljava/io/File;
const/4 v1, 0x0
aget-object v0, v0, v1
invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public getFiles()[Ljava/io/File;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/InvalidSessionReport;->files:[Ljava/io/File;
return-object v0
.end method
.method public getIdentifier()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/InvalidSessionReport;->identifier:Ljava/lang/String;
return-object v0
.end method
.method public remove()V
.registers 9
iget-object v1, p0, Lcom/crashlytics/android/core/InvalidSessionReport;->files:[Ljava/io/File;
array-length v2, v1
const/4 v0, 0x0
:goto_4
if-ge v0, v2, :cond_2e
aget-object v3, v1, v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v4
const-string v5, "CrashlyticsCore"
new-instance v6, Ljava/lang/StringBuilder;
invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V
const-string v7, "Removing invalid report file at "
invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v6
invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;
move-result-object v7
invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v6
invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v6
invoke-interface {v4, v5, v6}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-virtual {v3}, Ljava/io/File;->delete()Z
add-int/lit8 v0, v0, 0x1
goto :goto_4
:cond_2e
return-void
.end method