.class public Lcom/crashlytics/android/core/CrashlyticsCore;
.super Lio/fabric/sdk/android/h;
.source "CrashlyticsCore.java"
.field static final CLS_DEFAULT_PROCESS_DELAY:F = 1.0f
.field static final COLLECT_CUSTOM_KEYS:Ljava/lang/String; = "com.crashlytics.CollectCustomKeys"
.field static final COLLECT_CUSTOM_LOGS:Ljava/lang/String; = "com.crashlytics.CollectCustomLogs"
.field static final CRASHLYTICS_API_ENDPOINT:Ljava/lang/String; = "com.crashlytics.ApiEndpoint"
.field static final CRASHLYTICS_REQUIRE_BUILD_ID:Ljava/lang/String; = "com.crashlytics.RequireBuildId"
.field static final CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT:Z = true
.field static final CRASH_MARKER_FILE_NAME:Ljava/lang/String; = "crash_marker"
.field static final DEFAULT_MAIN_HANDLER_TIMEOUT_SEC:I = 0x4
.field private static final INITIALIZATION_MARKER_FILE_NAME:Ljava/lang/String; = "initialization_marker"
.field static final MAX_ATTRIBUTES:I = 0x40
.field static final MAX_ATTRIBUTE_SIZE:I = 0x400
.field private static final MISSING_BUILD_ID_MSG:Ljava/lang/String; = "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app\'s organization."
.field private static final PREF_ALWAYS_SEND_REPORTS_KEY:Ljava/lang/String; = "always_send_reports_opt_in"
.field private static final SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT:Z = false
.field public static final TAG:Ljava/lang/String; = "CrashlyticsCore"
.field private apiKey:Ljava/lang/String;
.field private final attributes:Ljava/util/concurrent/ConcurrentHashMap;
.field private buildId:Ljava/lang/String;
.field private crashMarker:Lcom/crashlytics/android/core/CrashlyticsFileMarker;
.field private delay:F
.field private disabled:Z
.field private executorServiceWrapper:Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;
.field private externalCrashEventDataProvider:Lcom/crashlytics/android/core/internal/CrashEventDataProvider;
.field private fileStore:Lio/fabric/sdk/android/services/d/a;
.field private handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
.field private httpRequestFactory:Lio/fabric/sdk/android/services/network/d;
.field private initializationMarker:Lcom/crashlytics/android/core/CrashlyticsFileMarker;
.field private installerPackageName:Ljava/lang/String;
.field private listener:Lcom/crashlytics/android/core/CrashlyticsListener;
.field private packageName:Ljava/lang/String;
.field private final pinningInfo:Lcom/crashlytics/android/core/PinningInfoProvider;
.field private sdkDir:Ljava/io/File;
.field private final startTime:J
.field private userEmail:Ljava/lang/String;
.field private userId:Ljava/lang/String;
.field private userName:Ljava/lang/String;
.field private versionCode:Ljava/lang/String;
.field private versionName:Ljava/lang/String;
.method public constructor <init>()V
.registers 4
const/4 v2, 0x0
const/high16 v0, 0x3f80
const/4 v1, 0x0
invoke-direct {p0, v0, v2, v2, v1}, Lcom/crashlytics/android/core/CrashlyticsCore;-><init>(FLcom/crashlytics/android/core/CrashlyticsListener;Lcom/crashlytics/android/core/PinningInfoProvider;Z)V
return-void
.end method
.method constructor <init>(FLcom/crashlytics/android/core/CrashlyticsListener;Lcom/crashlytics/android/core/PinningInfoProvider;Z)V
.registers 11
const-string v0, "Crashlytics Exception Handler"
invoke-static {v0}, Lio/fabric/sdk/android/services/b/n;->a(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;
move-result-object v5
move-object v0, p0
move v1, p1
move-object v2, p2
move-object v3, p3
move v4, p4
invoke-direct/range {v0 .. v5}, Lcom/crashlytics/android/core/CrashlyticsCore;-><init>(FLcom/crashlytics/android/core/CrashlyticsListener;Lcom/crashlytics/android/core/PinningInfoProvider;ZLjava/util/concurrent/ExecutorService;)V
return-void
.end method
.method constructor <init>(FLcom/crashlytics/android/core/CrashlyticsListener;Lcom/crashlytics/android/core/PinningInfoProvider;ZLjava/util/concurrent/ExecutorService;)V
.registers 8
const/4 v0, 0x0
invoke-direct {p0}, Lio/fabric/sdk/android/h;-><init>()V
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userId:Ljava/lang/String;
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userEmail:Ljava/lang/String;
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userName:Ljava/lang/String;
iput p1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->delay:F
if-eqz p2, :cond_29
:goto_e
iput-object p2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->listener:Lcom/crashlytics/android/core/CrashlyticsListener;
iput-object p3, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->pinningInfo:Lcom/crashlytics/android/core/PinningInfoProvider;
iput-boolean p4, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->disabled:Z
new-instance v0, Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;
invoke-direct {v0, p5}, Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;-><init>(Ljava/util/concurrent/ExecutorService;)V
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->executorServiceWrapper:Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;
new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;
invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->attributes:Ljava/util/concurrent/ConcurrentHashMap;
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v0
iput-wide v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->startTime:J
return-void
:cond_29
new-instance p2, Lcom/crashlytics/android/core/CrashlyticsCore$NoOpListener;
invoke-direct {p2, v0}, Lcom/crashlytics/android/core/CrashlyticsCore$NoOpListener;-><init>(Lcom/crashlytics/android/core/CrashlyticsCore$1;)V
goto :goto_e
.end method
.method static synthetic access$100(Lcom/crashlytics/android/core/CrashlyticsCore;)Lcom/crashlytics/android/core/CrashlyticsFileMarker;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->initializationMarker:Lcom/crashlytics/android/core/CrashlyticsFileMarker;
return-object v0
.end method
.method static synthetic access$200(Lcom/crashlytics/android/core/CrashlyticsCore;Landroid/app/Activity;Lio/fabric/sdk/android/services/e/o;)Z
.registers 4
invoke-direct {p0, p1, p2}, Lcom/crashlytics/android/core/CrashlyticsCore;->getSendDecisionFromUser(Landroid/app/Activity;Lio/fabric/sdk/android/services/e/o;)Z
move-result v0
return v0
.end method
.method private checkForPreviousCrash()V
.registers 5
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->executorServiceWrapper:Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;
new-instance v1, Lcom/crashlytics/android/core/CrashlyticsCore$CrashMarkerCheck;
iget-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->crashMarker:Lcom/crashlytics/android/core/CrashlyticsFileMarker;
invoke-direct {v1, v2}, Lcom/crashlytics/android/core/CrashlyticsCore$CrashMarkerCheck;-><init>(Lcom/crashlytics/android/core/CrashlyticsFileMarker;)V
invoke-virtual {v0, v1}, Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;->executeSyncLoggingException(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Boolean;
sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
invoke-virtual {v1, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z
move-result v0
if-nez v0, :cond_18
:goto_17
return-void
:try_start_18
:cond_18
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->listener:Lcom/crashlytics/android/core/CrashlyticsListener;
invoke-interface {v0}, Lcom/crashlytics/android/core/CrashlyticsListener;->crashlyticsDidDetectCrashDuringPreviousExecution()V
:try_end_1d
.catch Ljava/lang/Exception; {:try_start_18 .. :try_end_1d} :catch_1e
goto :goto_17
:catch_1e
move-exception v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "CrashlyticsCore"
const-string v3, "Exception thrown by CrashlyticsListener while notifying of previous crash."
invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
goto :goto_17
.end method
.method private doLog(ILjava/lang/String;Ljava/lang/String;)V
.registers 8
iget-boolean v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->disabled:Z
if-eqz v0, :cond_5
:goto_4
:cond_4
return-void
:cond_5
const-string v0, "prior to logging messages."
invoke-static {v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->ensureFabricWithCalled(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_4
invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
move-result-wide v0
iget-wide v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->startTime:J
sub-long/2addr v0, v2
iget-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
invoke-static {p1, p2, p3}, Lcom/crashlytics/android/core/CrashlyticsCore;->formatLogMessage(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
move-result-object v3
invoke-virtual {v2, v0, v1, v3}, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;->writeToLog(JLjava/lang/String;)V
goto :goto_4
.end method
.method private static ensureFabricWithCalled(Ljava/lang/String;)Z
.registers 5
invoke-static {}, Lcom/crashlytics/android/core/CrashlyticsCore;->getInstance()Lcom/crashlytics/android/core/CrashlyticsCore;
move-result-object v0
if-eqz v0, :cond_a
iget-object v0, v0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
if-nez v0, :cond_29
:cond_a
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "CrashlyticsCore"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Crashlytics must be initialized by calling Fabric.with(Context) "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
const/4 v3, 0x0
invoke-interface {v0, v1, v2, v3}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
const/4 v0, 0x0
:goto_28
return v0
:cond_29
const/4 v0, 0x1
goto :goto_28
.end method
.method private finishInitSynchronously()V
.registers 5
new-instance v1, Lcom/crashlytics/android/core/CrashlyticsCore$1;
invoke-direct {v1, p0}, Lcom/crashlytics/android/core/CrashlyticsCore$1;-><init>(Lcom/crashlytics/android/core/CrashlyticsCore;)V
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getDependencies()Ljava/util/Collection;
move-result-object v0
invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;
move-result-object v2
:goto_d
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_1d
invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lio/fabric/sdk/android/services/concurrency/l;
invoke-virtual {v1, v0}, Lio/fabric/sdk/android/services/concurrency/g;->addDependency(Lio/fabric/sdk/android/services/concurrency/l;)V
goto :goto_d
:cond_1d
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getFabric()Lio/fabric/sdk/android/c;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/c;->f()Ljava/util/concurrent/ExecutorService;
move-result-object v0
invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
move-result-object v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "CrashlyticsCore"
const-string v3, "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously."
invoke-interface {v1, v2, v3}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
const-wide/16 v2, 0x4
:try_start_36
sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;
invoke-interface {v0, v2, v3, v1}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
:try_end_3b
.catch Ljava/lang/InterruptedException; {:try_start_36 .. :try_end_3b} :catch_3c
.catch Ljava/util/concurrent/ExecutionException; {:try_start_36 .. :try_end_3b} :catch_49
.catch Ljava/util/concurrent/TimeoutException; {:try_start_36 .. :try_end_3b} :catch_56
:goto_3b
return-void
:catch_3c
move-exception v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "CrashlyticsCore"
const-string v3, "Crashlytics was interrupted during initialization."
invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
goto :goto_3b
:catch_49
move-exception v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "CrashlyticsCore"
const-string v3, "Problem encountered during Crashlytics initialization."
invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
goto :goto_3b
:catch_56
move-exception v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "CrashlyticsCore"
const-string v3, "Crashlytics timed out during initialization."
invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
goto :goto_3b
.end method
.method private static formatLogMessage(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.registers 5
new-instance v0, Ljava/lang/StringBuilder;
invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
invoke-static {p0}, Lio/fabric/sdk/android/services/b/i;->b(I)Ljava/lang/String;
move-result-object v1
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, "/"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, " "
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method public static getInstance()Lcom/crashlytics/android/core/CrashlyticsCore;
.registers 1
const-class v0, Lcom/crashlytics/android/core/CrashlyticsCore;
invoke-static {v0}, Lio/fabric/sdk/android/c;->a(Ljava/lang/Class;)Lio/fabric/sdk/android/h;
move-result-object v0
check-cast v0, Lcom/crashlytics/android/core/CrashlyticsCore;
return-object v0
.end method
.method private getSendDecisionFromUser(Landroid/app/Activity;Lio/fabric/sdk/android/services/e/o;)Z
.registers 7
new-instance v0, Lcom/crashlytics/android/core/CrashlyticsCore$7;
invoke-direct {v0, p0}, Lcom/crashlytics/android/core/CrashlyticsCore$7;-><init>(Lcom/crashlytics/android/core/CrashlyticsCore;)V
invoke-static {p1, p2, v0}, Lcom/crashlytics/android/core/CrashPromptDialog;->create(Landroid/app/Activity;Lio/fabric/sdk/android/services/e/o;Lcom/crashlytics/android/core/CrashPromptDialog$AlwaysSendCallback;)Lcom/crashlytics/android/core/CrashPromptDialog;
move-result-object v0
new-instance v1, Lcom/crashlytics/android/core/CrashlyticsCore$8;
invoke-direct {v1, p0, v0}, Lcom/crashlytics/android/core/CrashlyticsCore$8;-><init>(Lcom/crashlytics/android/core/CrashlyticsCore;Lcom/crashlytics/android/core/CrashPromptDialog;)V
invoke-virtual {p1, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "CrashlyticsCore"
const-string v3, "Waiting for user opt-in."
invoke-interface {v1, v2, v3}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-virtual {v0}, Lcom/crashlytics/android/core/CrashPromptDialog;->await()V
invoke-virtual {v0}, Lcom/crashlytics/android/core/CrashPromptDialog;->getOptIn()Z
move-result v0
return v0
.end method
.method static getSessionSettingsData()Lio/fabric/sdk/android/services/e/p;
.registers 1
invoke-static {}, Lio/fabric/sdk/android/services/e/q;->a()Lio/fabric/sdk/android/services/e/q;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/services/e/q;->b()Lio/fabric/sdk/android/services/e/t;
move-result-object v0
if-nez v0, :cond_c
const/4 v0, 0x0
:goto_b
return-object v0
:cond_c
iget-object v0, v0, Lio/fabric/sdk/android/services/e/t;->b:Lio/fabric/sdk/android/services/e/p;
goto :goto_b
.end method
.method private installExceptionHandler(Lcom/crashlytics/android/core/UnityVersionProvider;)Z
.registers 9
:try_start_0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "CrashlyticsCore"
const-string v2, "Installing exception handler..."
invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
new-instance v0, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;
move-result-object v1
iget-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->executorServiceWrapper:Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getIdManager()Lio/fabric/sdk/android/services/b/o;
move-result-object v3
iget-object v5, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->fileStore:Lio/fabric/sdk/android/services/d/a;
move-object v4, p1
move-object v6, p0
invoke-direct/range {v0 .. v6}, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;-><init>(Ljava/lang/Thread$UncaughtExceptionHandler;Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;Lio/fabric/sdk/android/services/b/o;Lcom/crashlytics/android/core/UnityVersionProvider;Lio/fabric/sdk/android/services/d/a;Lcom/crashlytics/android/core/CrashlyticsCore;)V
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
invoke-virtual {v0}, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;->openSession()V
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "CrashlyticsCore"
const-string v2, "Successfully installed exception handler."
invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
:try_end_35
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_35} :catch_37
const/4 v0, 0x1
:goto_36
return v0
:catch_37
move-exception v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "CrashlyticsCore"
const-string v3, "There was a problem installing the exception handler."
invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
const/4 v0, 0x0
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
const/4 v0, 0x0
goto :goto_36
.end method
.method static isBuildIdValid(Ljava/lang/String;Z)Z
.registers 6
const/4 v0, 0x1
if-nez p1, :cond_f
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "CrashlyticsCore"
const-string v3, "Configured not to require a build ID."
invoke-interface {v1, v2, v3}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
:cond_e
:goto_e
return v0
:cond_f
invoke-static {p0}, Lio/fabric/sdk/android/services/b/i;->c(Ljava/lang/String;)Z
move-result v1
if-eqz v1, :cond_e
const-string v0, "CrashlyticsCore"
const-string v1, "."
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".     |  | "
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".     |  |"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".     |  |"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".   \\ |  | /"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".    \\    /"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".     \\  /"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".      \\/"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, "."
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app\'s organization."
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, "."
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".      /\\"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".     /  \\"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".    /    \\"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".   / |  | \\"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".     |  |"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".     |  |"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, ".     |  |"
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const-string v0, "CrashlyticsCore"
const-string v1, "."
invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
const/4 v0, 0x0
goto/16 :goto_e
.end method
.method static recordFatalExceptionEvent(Ljava/lang/String;Ljava/lang/String;)V
.registers 4
const-class v0, Lcom/crashlytics/android/answers/Answers;
invoke-static {v0}, Lio/fabric/sdk/android/c;->a(Ljava/lang/Class;)Lio/fabric/sdk/android/h;
move-result-object v0
check-cast v0, Lcom/crashlytics/android/answers/Answers;
if-eqz v0, :cond_12
new-instance v1, Lio/fabric/sdk/android/services/b/j$a;
invoke-direct {v1, p0, p1}, Lio/fabric/sdk/android/services/b/j$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V
invoke-virtual {v0, v1}, Lcom/crashlytics/android/answers/Answers;->onException(Lio/fabric/sdk/android/services/b/j$a;)V
:cond_12
return-void
.end method
.method static recordLoggedExceptionEvent(Ljava/lang/String;Ljava/lang/String;)V
.registers 4
const-class v0, Lcom/crashlytics/android/answers/Answers;
invoke-static {v0}, Lio/fabric/sdk/android/c;->a(Ljava/lang/Class;)Lio/fabric/sdk/android/h;
move-result-object v0
check-cast v0, Lcom/crashlytics/android/answers/Answers;
if-eqz v0, :cond_12
new-instance v1, Lio/fabric/sdk/android/services/b/j$b;
invoke-direct {v1, p0, p1}, Lio/fabric/sdk/android/services/b/j$b;-><init>(Ljava/lang/String;Ljava/lang/String;)V
invoke-virtual {v0, v1}, Lcom/crashlytics/android/answers/Answers;->onException(Lio/fabric/sdk/android/services/b/j$b;)V
:cond_12
return-void
.end method
.method private static sanitizeAttribute(Ljava/lang/String;)Ljava/lang/String;
.registers 3
const/16 v1, 0x400
if-eqz p0, :cond_13
invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;
move-result-object p0
invoke-virtual {p0}, Ljava/lang/String;->length()I
move-result v0
if-le v0, v1, :cond_13
const/4 v0, 0x0
invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
move-result-object p0
:cond_13
return-object p0
.end method
.method private setAndValidateKitProperties(Landroid/content/Context;)V
.registers 6
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->pinningInfo:Lcom/crashlytics/android/core/PinningInfoProvider;
if-eqz v0, :cond_65
new-instance v0, Lcom/crashlytics/android/core/CrashlyticsPinningInfoProvider;
iget-object v1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->pinningInfo:Lcom/crashlytics/android/core/PinningInfoProvider;
invoke-direct {v0, v1}, Lcom/crashlytics/android/core/CrashlyticsPinningInfoProvider;-><init>(Lcom/crashlytics/android/core/PinningInfoProvider;)V
:goto_b
new-instance v1, Lio/fabric/sdk/android/services/network/b;
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v2
invoke-direct {v1, v2}, Lio/fabric/sdk/android/services/network/b;-><init>(Lio/fabric/sdk/android/k;)V
iput-object v1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->httpRequestFactory:Lio/fabric/sdk/android/services/network/d;
iget-object v1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->httpRequestFactory:Lio/fabric/sdk/android/services/network/d;
invoke-interface {v1, v0}, Lio/fabric/sdk/android/services/network/d;->a(Lio/fabric/sdk/android/services/network/f;)V
invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->packageName:Ljava/lang/String;
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getIdManager()Lio/fabric/sdk/android/services/b/o;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/services/b/o;->j()Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->installerPackageName:Ljava/lang/String;
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "CrashlyticsCore"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Installer package name is: "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->installerPackageName:Ljava/lang/String;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v0
iget-object v1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->packageName:Ljava/lang/String;
const/4 v2, 0x0
invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
move-result-object v0
iget v1, v0, Landroid/content/pm/PackageInfo;->versionCode:I
invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
move-result-object v1
iput-object v1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->versionCode:Ljava/lang/String;
iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
if-nez v1, :cond_67
const-string v0, "0.0"
:goto_62
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->versionName:Ljava/lang/String;
return-void
:cond_65
const/4 v0, 0x0
goto :goto_b
:cond_67
iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
goto :goto_62
.end method
.method  canSendWithUserApproval()Z
.registers 4
invoke-static {}, Lio/fabric/sdk/android/services/e/q;->a()Lio/fabric/sdk/android/services/e/q;
move-result-object v0
new-instance v1, Lcom/crashlytics/android/core/CrashlyticsCore$6;
invoke-direct {v1, p0}, Lcom/crashlytics/android/core/CrashlyticsCore$6;-><init>(Lcom/crashlytics/android/core/CrashlyticsCore;)V
const/4 v2, 0x1
invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v2
invoke-virtual {v0, v1, v2}, Lio/fabric/sdk/android/services/e/q;->a(Lio/fabric/sdk/android/services/e/q$b;Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Boolean;
invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
move-result v0
return v0
.end method
.method public crash()V
.registers 2
new-instance v0, Lcom/crashlytics/android/core/CrashTest;
invoke-direct {v0}, Lcom/crashlytics/android/core/CrashTest;-><init>()V
invoke-virtual {v0}, Lcom/crashlytics/android/core/CrashTest;->indexOutOfBounds()V
return-void
.end method
.method  createCrashMarker()V
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->crashMarker:Lcom/crashlytics/android/core/CrashlyticsFileMarker;
invoke-virtual {v0}, Lcom/crashlytics/android/core/CrashlyticsFileMarker;->create()Z
return-void
.end method
.method  didPreviousInitializationFail()Z
.registers 3
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->executorServiceWrapper:Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;
new-instance v1, Lcom/crashlytics/android/core/CrashlyticsCore$4;
invoke-direct {v1, p0}, Lcom/crashlytics/android/core/CrashlyticsCore$4;-><init>(Lcom/crashlytics/android/core/CrashlyticsCore;)V
invoke-virtual {v0, v1}, Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;->executeSyncLoggingException(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Boolean;
invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
move-result v0
return v0
.end method
.method protected bridge synthetic doInBackground()Ljava/lang/Object;
.registers 2
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->doInBackground()Ljava/lang/Void;
move-result-object v0
return-object v0
.end method
.method protected doInBackground()Ljava/lang/Void;
.registers 6
const/4 v4, 0x0
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->markInitializationStarted()V
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getExternalCrashEventData()Lcom/crashlytics/android/core/internal/models/SessionEventData;
move-result-object v0
if-eqz v0, :cond_f
iget-object v1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
invoke-virtual {v1, v0}, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;->writeExternalCrashEvent(Lcom/crashlytics/android/core/internal/models/SessionEventData;)V
:cond_f
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
invoke-virtual {v0}, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;->cleanInvalidTempFiles()V
:try_start_14
invoke-static {}, Lio/fabric/sdk/android/services/e/q;->a()Lio/fabric/sdk/android/services/e/q;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/services/e/q;->b()Lio/fabric/sdk/android/services/e/t;
move-result-object v0
if-nez v0, :cond_2d
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "CrashlyticsCore"
const-string v2, "Received null settings, skipping initialization!"
invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->d(Ljava/lang/String;Ljava/lang/String;)V
:try_end_29
.catchall {:try_start_14 .. :try_end_29} :catchall_7c
.catch Ljava/lang/Exception; {:try_start_14 .. :try_end_29} :catch_6c
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->markInitializationComplete()V
:goto_2c
return-object v4
:cond_2d
:try_start_2d
iget-object v1, v0, Lio/fabric/sdk/android/services/e/t;->d:Lio/fabric/sdk/android/services/e/m;
iget-boolean v1, v1, Lio/fabric/sdk/android/services/e/m;->c:Z
if-nez v1, :cond_42
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "CrashlyticsCore"
const-string v2, "Collection of crash reports disabled in Crashlytics settings."
invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
:try_end_3e
.catchall {:try_start_2d .. :try_end_3e} :catchall_7c
.catch Ljava/lang/Exception; {:try_start_2d .. :try_end_3e} :catch_6c
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->markInitializationComplete()V
goto :goto_2c
:cond_42
:try_start_42
iget-object v1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
invoke-virtual {v1}, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;->finalizeSessions()Z
invoke-virtual {p0, v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getCreateReportSpiCall(Lio/fabric/sdk/android/services/e/t;)Lcom/crashlytics/android/core/CreateReportSpiCall;
move-result-object v0
if-nez v0, :cond_5c
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "CrashlyticsCore"
const-string v2, "Unable to create a call to upload reports."
invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->d(Ljava/lang/String;Ljava/lang/String;)V
:try_end_58
.catchall {:try_start_42 .. :try_end_58} :catchall_7c
.catch Ljava/lang/Exception; {:try_start_42 .. :try_end_58} :catch_6c
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->markInitializationComplete()V
goto :goto_2c
:cond_5c
:try_start_5c
new-instance v1, Lcom/crashlytics/android/core/ReportUploader;
iget-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->apiKey:Ljava/lang/String;
invoke-direct {v1, v2, v0}, Lcom/crashlytics/android/core/ReportUploader;-><init>(Ljava/lang/String;Lcom/crashlytics/android/core/CreateReportSpiCall;)V
iget v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->delay:F
invoke-virtual {v1, v0}, Lcom/crashlytics/android/core/ReportUploader;->uploadReports(F)V
:try_end_68
.catchall {:try_start_5c .. :try_end_68} :catchall_7c
.catch Ljava/lang/Exception; {:try_start_5c .. :try_end_68} :catch_6c
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->markInitializationComplete()V
goto :goto_2c
:catch_6c
move-exception v0
:try_start_6d
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "CrashlyticsCore"
const-string v3, "Crashlytics encountered a problem during asynchronous initialization."
invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
:try_end_78
.catchall {:try_start_6d .. :try_end_78} :catchall_7c
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->markInitializationComplete()V
goto :goto_2c
:catchall_7c
move-exception v0
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->markInitializationComplete()V
throw v0
.end method
.method  getApiKey()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->apiKey:Ljava/lang/String;
return-object v0
.end method
.method  getAttributes()Ljava/util/Map;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->attributes:Ljava/util/concurrent/ConcurrentHashMap;
invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
move-result-object v0
return-object v0
.end method
.method  getBuildId()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->buildId:Ljava/lang/String;
return-object v0
.end method
.method  getCreateReportSpiCall(Lio/fabric/sdk/android/services/e/t;)Lcom/crashlytics/android/core/CreateReportSpiCall;
.registers 6
if-eqz p1, :cond_12
new-instance v0, Lcom/crashlytics/android/core/DefaultCreateReportSpiCall;
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getOverridenSpiEndpoint()Ljava/lang/String;
move-result-object v1
iget-object v2, p1, Lio/fabric/sdk/android/services/e/t;->a:Lio/fabric/sdk/android/services/e/e;
iget-object v2, v2, Lio/fabric/sdk/android/services/e/e;->d:Ljava/lang/String;
iget-object v3, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->httpRequestFactory:Lio/fabric/sdk/android/services/network/d;
invoke-direct {v0, p0, v1, v2, v3}, Lcom/crashlytics/android/core/DefaultCreateReportSpiCall;-><init>(Lio/fabric/sdk/android/h;Ljava/lang/String;Ljava/lang/String;Lio/fabric/sdk/android/services/network/d;)V
:goto_11
return-object v0
:cond_12
const/4 v0, 0x0
goto :goto_11
.end method
.method  getExternalCrashEventData()Lcom/crashlytics/android/core/internal/models/SessionEventData;
.registers 3
const/4 v0, 0x0
iget-object v1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->externalCrashEventDataProvider:Lcom/crashlytics/android/core/internal/CrashEventDataProvider;
if-eqz v1, :cond_b
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->externalCrashEventDataProvider:Lcom/crashlytics/android/core/internal/CrashEventDataProvider;
invoke-interface {v0}, Lcom/crashlytics/android/core/internal/CrashEventDataProvider;->getCrashEventData()Lcom/crashlytics/android/core/internal/models/SessionEventData;
move-result-object v0
:cond_b
return-object v0
.end method
.method  getHandler()Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
return-object v0
.end method
.method public getIdentifier()Ljava/lang/String;
.registers 2
const-string v0, "com.crashlytics.sdk.android.crashlytics-core"
return-object v0
.end method
.method  getInstallerPackageName()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->installerPackageName:Ljava/lang/String;
return-object v0
.end method
.method  getOverridenSpiEndpoint()Ljava/lang/String;
.registers 3
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getContext()Landroid/content/Context;
move-result-object v0
const-string v1, "com.crashlytics.ApiEndpoint"
invoke-static {v0, v1}, Lio/fabric/sdk/android/services/b/i;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
return-object v0
.end method
.method  getPackageName()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->packageName:Ljava/lang/String;
return-object v0
.end method
.method public getPinningInfoProvider()Lcom/crashlytics/android/core/PinningInfoProvider;
.registers 2
iget-boolean v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->disabled:Z
if-nez v0, :cond_7
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->pinningInfo:Lcom/crashlytics/android/core/PinningInfoProvider;
:goto_6
return-object v0
:cond_7
const/4 v0, 0x0
goto :goto_6
.end method
.method  getSdkDirectory()Ljava/io/File;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->sdkDir:Ljava/io/File;
if-nez v0, :cond_f
new-instance v0, Lio/fabric/sdk/android/services/d/b;
invoke-direct {v0, p0}, Lio/fabric/sdk/android/services/d/b;-><init>(Lio/fabric/sdk/android/h;)V
invoke-virtual {v0}, Lio/fabric/sdk/android/services/d/b;->a()Ljava/io/File;
move-result-object v0
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->sdkDir:Ljava/io/File;
:cond_f
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->sdkDir:Ljava/io/File;
return-object v0
.end method
.method  getUserEmail()Ljava/lang/String;
.registers 2
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getIdManager()Lio/fabric/sdk/android/services/b/o;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/services/b/o;->a()Z
move-result v0
if-eqz v0, :cond_d
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userEmail:Ljava/lang/String;
:goto_c
return-object v0
:cond_d
const/4 v0, 0x0
goto :goto_c
.end method
.method  getUserIdentifier()Ljava/lang/String;
.registers 2
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getIdManager()Lio/fabric/sdk/android/services/b/o;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/services/b/o;->a()Z
move-result v0
if-eqz v0, :cond_d
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userId:Ljava/lang/String;
:goto_c
return-object v0
:cond_d
const/4 v0, 0x0
goto :goto_c
.end method
.method  getUserName()Ljava/lang/String;
.registers 2
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getIdManager()Lio/fabric/sdk/android/services/b/o;
move-result-object v0
invoke-virtual {v0}, Lio/fabric/sdk/android/services/b/o;->a()Z
move-result v0
if-eqz v0, :cond_d
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userName:Ljava/lang/String;
:goto_c
return-object v0
:cond_d
const/4 v0, 0x0
goto :goto_c
.end method
.method public getVersion()Ljava/lang/String;
.registers 2
const-string v0, "2.3.14.151"
return-object v0
.end method
.method  getVersionCode()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->versionCode:Ljava/lang/String;
return-object v0
.end method
.method  getVersionName()Ljava/lang/String;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->versionName:Ljava/lang/String;
return-object v0
.end method
.method  internalVerifyPinning(Ljava/net/URL;)Z
.registers 6
const/4 v1, 0x0
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getPinningInfoProvider()Lcom/crashlytics/android/core/PinningInfoProvider;
move-result-object v0
if-eqz v0, :cond_21
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->httpRequestFactory:Lio/fabric/sdk/android/services/network/d;
sget-object v2, Lio/fabric/sdk/android/services/network/c;->a:Lio/fabric/sdk/android/services/network/c;
invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;
move-result-object v3
invoke-interface {v0, v2, v3}, Lio/fabric/sdk/android/services/network/d;->a(Lio/fabric/sdk/android/services/network/c;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
move-result-object v2
invoke-virtual {v2}, Lio/fabric/sdk/android/services/network/HttpRequest;->a()Ljava/net/HttpURLConnection;
move-result-object v0
check-cast v0, Ljavax/net/ssl/HttpsURLConnection;
invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setInstanceFollowRedirects(Z)V
invoke-virtual {v2}, Lio/fabric/sdk/android/services/network/HttpRequest;->b()I
const/4 v0, 0x1
:goto_20
return v0
:cond_21
move v0, v1
goto :goto_20
.end method
.method public log(ILjava/lang/String;Ljava/lang/String;)V
.registers 8
invoke-direct {p0, p1, p2, p3}, Lcom/crashlytics/android/core/CrashlyticsCore;->doLog(ILjava/lang/String;Ljava/lang/String;)V
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, ""
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, ""
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
const/4 v3, 0x1
invoke-interface {v0, p1, v1, v2, v3}, Lio/fabric/sdk/android/k;->a(ILjava/lang/String;Ljava/lang/String;Z)V
return-void
.end method
.method public log(Ljava/lang/String;)V
.registers 4
const/4 v0, 0x3
const-string v1, "CrashlyticsCore"
invoke-direct {p0, v0, v1, p1}, Lcom/crashlytics/android/core/CrashlyticsCore;->doLog(ILjava/lang/String;Ljava/lang/String;)V
return-void
.end method
.method public logException(Ljava/lang/Throwable;)V
.registers 6
iget-boolean v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->disabled:Z
if-eqz v0, :cond_5
:goto_4
:cond_4
return-void
:cond_5
const-string v0, "prior to logging exceptions."
invoke-static {v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->ensureFabricWithCalled(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_4
if-nez p1, :cond_1c
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const/4 v1, 0x5
const-string v2, "CrashlyticsCore"
const-string v3, "Crashlytics is ignoring a request to log a null exception."
invoke-interface {v0, v1, v2, v3}, Lio/fabric/sdk/android/k;->a(ILjava/lang/String;Ljava/lang/String;)V
goto :goto_4
:cond_1c
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;
move-result-object v1
invoke-virtual {v0, v1, p1}, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;->writeNonFatalException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
goto :goto_4
.end method
.method  markInitializationComplete()V
.registers 3
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->executorServiceWrapper:Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;
new-instance v1, Lcom/crashlytics/android/core/CrashlyticsCore$3;
invoke-direct {v1, p0}, Lcom/crashlytics/android/core/CrashlyticsCore$3;-><init>(Lcom/crashlytics/android/core/CrashlyticsCore;)V
invoke-virtual {v0, v1}, Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;->executeAsync(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
return-void
.end method
.method  markInitializationStarted()V
.registers 3
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->executorServiceWrapper:Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;
new-instance v1, Lcom/crashlytics/android/core/CrashlyticsCore$2;
invoke-direct {v1, p0}, Lcom/crashlytics/android/core/CrashlyticsCore$2;-><init>(Lcom/crashlytics/android/core/CrashlyticsCore;)V
invoke-virtual {v0, v1}, Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;->executeSyncLoggingException(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
return-void
.end method
.method protected onPreExecute()Z
.registers 2
invoke-super {p0}, Lio/fabric/sdk/android/h;->getContext()Landroid/content/Context;
move-result-object v0
invoke-virtual {p0, v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->onPreExecute(Landroid/content/Context;)Z
move-result v0
return v0
.end method
.method  onPreExecute(Landroid/content/Context;)Z
.registers 8
const/4 v1, 0x1
const/4 v0, 0x0
iget-boolean v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->disabled:Z
if-eqz v2, :cond_7
:goto_6
:cond_6
return v0
:cond_7
new-instance v2, Lio/fabric/sdk/android/services/b/g;
invoke-direct {v2}, Lio/fabric/sdk/android/services/b/g;-><init>()V
invoke-virtual {v2, p1}, Lio/fabric/sdk/android/services/b/g;->a(Landroid/content/Context;)Ljava/lang/String;
move-result-object v2
iput-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->apiKey:Ljava/lang/String;
iget-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->apiKey:Ljava/lang/String;
if-eqz v2, :cond_6
invoke-static {p1}, Lio/fabric/sdk/android/services/b/i;->m(Landroid/content/Context;)Ljava/lang/String;
move-result-object v2
iput-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->buildId:Ljava/lang/String;
const-string v2, "com.crashlytics.RequireBuildId"
invoke-static {p1, v2, v1}, Lio/fabric/sdk/android/services/b/i;->a(Landroid/content/Context;Ljava/lang/String;Z)Z
move-result v2
iget-object v3, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->buildId:Ljava/lang/String;
invoke-static {v3, v2}, Lcom/crashlytics/android/core/CrashlyticsCore;->isBuildIdValid(Ljava/lang/String;Z)Z
move-result v2
if-nez v2, :cond_32
new-instance v0, Lio/fabric/sdk/android/services/concurrency/UnmetDependencyException;
const-string v1, "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app\'s organization."
invoke-direct {v0, v1}, Lio/fabric/sdk/android/services/concurrency/UnmetDependencyException;-><init>(Ljava/lang/String;)V
throw v0
:cond_32
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v2
const-string v3, "CrashlyticsCore"
new-instance v4, Ljava/lang/StringBuilder;
invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
const-string v5, "Initializing Crashlytics "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getVersion()Ljava/lang/String;
move-result-object v5
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v4
invoke-interface {v2, v3, v4}, Lio/fabric/sdk/android/k;->c(Ljava/lang/String;Ljava/lang/String;)V
new-instance v2, Lio/fabric/sdk/android/services/d/b;
invoke-direct {v2, p0}, Lio/fabric/sdk/android/services/d/b;-><init>(Lio/fabric/sdk/android/h;)V
iput-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->fileStore:Lio/fabric/sdk/android/services/d/a;
new-instance v2, Lcom/crashlytics/android/core/CrashlyticsFileMarker;
const-string v3, "crash_marker"
iget-object v4, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->fileStore:Lio/fabric/sdk/android/services/d/a;
invoke-direct {v2, v3, v4}, Lcom/crashlytics/android/core/CrashlyticsFileMarker;-><init>(Ljava/lang/String;Lio/fabric/sdk/android/services/d/a;)V
iput-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->crashMarker:Lcom/crashlytics/android/core/CrashlyticsFileMarker;
new-instance v2, Lcom/crashlytics/android/core/CrashlyticsFileMarker;
const-string v3, "initialization_marker"
iget-object v4, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->fileStore:Lio/fabric/sdk/android/services/d/a;
invoke-direct {v2, v3, v4}, Lcom/crashlytics/android/core/CrashlyticsFileMarker;-><init>(Ljava/lang/String;Lio/fabric/sdk/android/services/d/a;)V
iput-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->initializationMarker:Lcom/crashlytics/android/core/CrashlyticsFileMarker;
:try_start_6f
invoke-direct {p0, p1}, Lcom/crashlytics/android/core/CrashlyticsCore;->setAndValidateKitProperties(Landroid/content/Context;)V
new-instance v2, Lcom/crashlytics/android/core/ManifestUnityVersionProvider;
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getPackageName()Ljava/lang/String;
move-result-object v3
invoke-direct {v2, p1, v3}, Lcom/crashlytics/android/core/ManifestUnityVersionProvider;-><init>(Landroid/content/Context;Ljava/lang/String;)V
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->didPreviousInitializationFail()Z
move-result v3
invoke-direct {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->checkForPreviousCrash()V
invoke-direct {p0, v2}, Lcom/crashlytics/android/core/CrashlyticsCore;->installExceptionHandler(Lcom/crashlytics/android/core/UnityVersionProvider;)Z
move-result v2
if-eqz v2, :cond_6
if-eqz v3, :cond_a3
invoke-static {p1}, Lio/fabric/sdk/android/services/b/i;->n(Landroid/content/Context;)Z
move-result v2
if-eqz v2, :cond_a3
invoke-direct {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->finishInitSynchronously()V
:try_end_93
.catch Ljava/lang/Exception; {:try_start_6f .. :try_end_93} :catch_95
goto/16 :goto_6
:catch_95
move-exception v1
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v2
const-string v3, "CrashlyticsCore"
const-string v4, "Crashlytics was not started due to an exception during initialization"
invoke-interface {v2, v3, v4, v1}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
goto/16 :goto_6
:cond_a3
move v0, v1
goto/16 :goto_6
.end method
.method public setBool(Ljava/lang/String;Z)V
.registers 4
invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;
move-result-object v0
invoke-virtual {p0, p1, v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->setString(Ljava/lang/String;Ljava/lang/String;)V
return-void
.end method
.method public setDouble(Ljava/lang/String;D)V
.registers 6
invoke-static {p2, p3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;
move-result-object v0
invoke-virtual {p0, p1, v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->setString(Ljava/lang/String;Ljava/lang/String;)V
return-void
.end method
.method  setExternalCrashEventDataProvider(Lcom/crashlytics/android/core/internal/CrashEventDataProvider;)V
.registers 2
iput-object p1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->externalCrashEventDataProvider:Lcom/crashlytics/android/core/internal/CrashEventDataProvider;
return-void
.end method
.method public setFloat(Ljava/lang/String;F)V
.registers 4
invoke-static {p2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;
move-result-object v0
invoke-virtual {p0, p1, v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->setString(Ljava/lang/String;Ljava/lang/String;)V
return-void
.end method
.method public setInt(Ljava/lang/String;I)V
.registers 4
invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
move-result-object v0
invoke-virtual {p0, p1, v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->setString(Ljava/lang/String;Ljava/lang/String;)V
return-void
.end method
.method public declared-synchronized setListener(Lcom/crashlytics/android/core/CrashlyticsListener;)V
.registers 5
monitor-enter p0
:try_start_1
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "CrashlyticsCore"
const-string v2, "Use of setListener is deprecated."
invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->d(Ljava/lang/String;Ljava/lang/String;)V
if-nez p1, :cond_19
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "listener must not be null."
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:try_end_16
.catchall {:try_start_1 .. :try_end_16} :catchall_16
:catchall_16
move-exception v0
monitor-exit p0
throw v0
:cond_19
:try_start_19
iput-object p1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->listener:Lcom/crashlytics/android/core/CrashlyticsListener;
:try_end_1b
.catchall {:try_start_19 .. :try_end_1b} :catchall_16
monitor-exit p0
return-void
.end method
.method public setLong(Ljava/lang/String;J)V
.registers 6
invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;
move-result-object v0
invoke-virtual {p0, p1, v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->setString(Ljava/lang/String;Ljava/lang/String;)V
return-void
.end method
.method  setShouldSendUserReportsWithoutPrompting(Z)V
.registers 5
new-instance v0, Lio/fabric/sdk/android/services/d/d;
invoke-direct {v0, p0}, Lio/fabric/sdk/android/services/d/d;-><init>(Lio/fabric/sdk/android/h;)V
invoke-interface {v0}, Lio/fabric/sdk/android/services/d/c;->b()Landroid/content/SharedPreferences$Editor;
move-result-object v1
const-string v2, "always_send_reports_opt_in"
invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;
move-result-object v1
invoke-interface {v0, v1}, Lio/fabric/sdk/android/services/d/c;->a(Landroid/content/SharedPreferences$Editor;)Z
return-void
.end method
.method public setString(Ljava/lang/String;Ljava/lang/String;)V
.registers 7
iget-boolean v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->disabled:Z
if-eqz v0, :cond_5
:goto_4
:cond_4
return-void
:cond_5
const-string v0, "prior to setting keys."
invoke-static {v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->ensureFabricWithCalled(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_4
if-nez p1, :cond_30
invoke-virtual {p0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getContext()Landroid/content/Context;
move-result-object v0
if-eqz v0, :cond_23
invoke-static {v0}, Lio/fabric/sdk/android/services/b/i;->i(Landroid/content/Context;)Z
move-result v0
if-eqz v0, :cond_23
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "Custom attribute key must not be null."
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_23
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "CrashlyticsCore"
const-string v2, "Attempting to set custom attribute with null key, ignoring."
const/4 v3, 0x0
invoke-interface {v0, v1, v2, v3}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
goto :goto_4
:cond_30
invoke-static {p1}, Lcom/crashlytics/android/core/CrashlyticsCore;->sanitizeAttribute(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->attributes:Ljava/util/concurrent/ConcurrentHashMap;
invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I
move-result v0
const/16 v2, 0x40
if-lt v0, v2, :cond_52
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->attributes:Ljava/util/concurrent/ConcurrentHashMap;
invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
move-result v0
if-nez v0, :cond_52
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "CrashlyticsCore"
const-string v2, "Exceeded maximum number of custom attributes (64)"
invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
goto :goto_4
:cond_52
if-nez p2, :cond_63
const-string v0, ""
:goto_56
iget-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->attributes:Ljava/util/concurrent/ConcurrentHashMap;
invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
iget-object v1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->attributes:Ljava/util/concurrent/ConcurrentHashMap;
invoke-virtual {v0, v1}, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;->cacheKeyData(Ljava/util/Map;)V
goto :goto_4
:cond_63
invoke-static {p2}, Lcom/crashlytics/android/core/CrashlyticsCore;->sanitizeAttribute(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
goto :goto_56
.end method
.method public setUserEmail(Ljava/lang/String;)V
.registers 6
iget-boolean v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->disabled:Z
if-eqz v0, :cond_5
:goto_4
:cond_4
return-void
:cond_5
const-string v0, "prior to setting user data."
invoke-static {v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->ensureFabricWithCalled(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_4
invoke-static {p1}, Lcom/crashlytics/android/core/CrashlyticsCore;->sanitizeAttribute(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userEmail:Ljava/lang/String;
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
iget-object v1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userId:Ljava/lang/String;
iget-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userName:Ljava/lang/String;
iget-object v3, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userEmail:Ljava/lang/String;
invoke-virtual {v0, v1, v2, v3}, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;->cacheUserData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
goto :goto_4
.end method
.method public setUserIdentifier(Ljava/lang/String;)V
.registers 6
iget-boolean v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->disabled:Z
if-eqz v0, :cond_5
:goto_4
:cond_4
return-void
:cond_5
const-string v0, "prior to setting user data."
invoke-static {v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->ensureFabricWithCalled(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_4
invoke-static {p1}, Lcom/crashlytics/android/core/CrashlyticsCore;->sanitizeAttribute(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userId:Ljava/lang/String;
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
iget-object v1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userId:Ljava/lang/String;
iget-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userName:Ljava/lang/String;
iget-object v3, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userEmail:Ljava/lang/String;
invoke-virtual {v0, v1, v2, v3}, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;->cacheUserData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
goto :goto_4
.end method
.method public setUserName(Ljava/lang/String;)V
.registers 6
iget-boolean v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->disabled:Z
if-eqz v0, :cond_5
:goto_4
:cond_4
return-void
:cond_5
const-string v0, "prior to setting user data."
invoke-static {v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->ensureFabricWithCalled(Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_4
invoke-static {p1}, Lcom/crashlytics/android/core/CrashlyticsCore;->sanitizeAttribute(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
iput-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userName:Ljava/lang/String;
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->handler:Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
iget-object v1, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userId:Ljava/lang/String;
iget-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userName:Ljava/lang/String;
iget-object v3, p0, Lcom/crashlytics/android/core/CrashlyticsCore;->userEmail:Ljava/lang/String;
invoke-virtual {v0, v1, v2, v3}, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;->cacheUserData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
goto :goto_4
.end method
.method  shouldPromptUserBeforeSendingCrashReports()Z
.registers 4
invoke-static {}, Lio/fabric/sdk/android/services/e/q;->a()Lio/fabric/sdk/android/services/e/q;
move-result-object v0
new-instance v1, Lcom/crashlytics/android/core/CrashlyticsCore$5;
invoke-direct {v1, p0}, Lcom/crashlytics/android/core/CrashlyticsCore$5;-><init>(Lcom/crashlytics/android/core/CrashlyticsCore;)V
const/4 v2, 0x0
invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
move-result-object v2
invoke-virtual {v0, v1, v2}, Lio/fabric/sdk/android/services/e/q;->a(Lio/fabric/sdk/android/services/e/q$b;Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Ljava/lang/Boolean;
invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
move-result v0
return v0
.end method
.method  shouldSendReportsWithoutPrompting()Z
.registers 4
new-instance v0, Lio/fabric/sdk/android/services/d/d;
invoke-direct {v0, p0}, Lio/fabric/sdk/android/services/d/d;-><init>(Lio/fabric/sdk/android/h;)V
invoke-interface {v0}, Lio/fabric/sdk/android/services/d/c;->a()Landroid/content/SharedPreferences;
move-result-object v0
const-string v1, "always_send_reports_opt_in"
const/4 v2, 0x0
invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
move-result v0
return v0
.end method
.method public verifyPinning(Ljava/net/URL;)Z
.registers 6
:try_start_0
invoke-virtual {p0, p1}, Lcom/crashlytics/android/core/CrashlyticsCore;->internalVerifyPinning(Ljava/net/URL;)Z
:try_end_3
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3} :catch_5
move-result v0
:goto_4
return v0
:catch_5
move-exception v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "CrashlyticsCore"
const-string v3, "Could not verify SSL pinning"
invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
const/4 v0, 0x0
goto :goto_4
.end method