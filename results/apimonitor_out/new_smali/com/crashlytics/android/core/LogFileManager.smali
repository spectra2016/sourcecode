.class  Lcom/crashlytics/android/core/LogFileManager;
.super Ljava/lang/Object;
.source "LogFileManager.java"
.field private static final DIRECTORY_NAME:Ljava/lang/String; = "log-files"
.field private static final LOGFILE_EXT:Ljava/lang/String; = ".temp"
.field private static final LOGFILE_PREFIX:Ljava/lang/String; = "crashlytics-userlog-"
.field static final MAX_LOG_SIZE:I = 0x10000
.field private static final NOOP_LOG_STORE:Lcom/crashlytics/android/core/LogFileManager$NoopLogStore;
.field private final context:Landroid/content/Context;
.field private currentLog:Lcom/crashlytics/android/core/FileLogStore;
.field private final fileStore:Lio/fabric/sdk/android/services/d/a;
.method static constructor <clinit>()V
.registers 2
new-instance v0, Lcom/crashlytics/android/core/LogFileManager$NoopLogStore;
const/4 v1, 0x0
invoke-direct {v0, v1}, Lcom/crashlytics/android/core/LogFileManager$NoopLogStore;-><init>(Lcom/crashlytics/android/core/LogFileManager$1;)V
sput-object v0, Lcom/crashlytics/android/core/LogFileManager;->NOOP_LOG_STORE:Lcom/crashlytics/android/core/LogFileManager$NoopLogStore;
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Lio/fabric/sdk/android/services/d/a;)V
.registers 4
const/4 v0, 0x0
invoke-direct {p0, p1, p2, v0}, Lcom/crashlytics/android/core/LogFileManager;-><init>(Landroid/content/Context;Lio/fabric/sdk/android/services/d/a;Ljava/lang/String;)V
return-void
.end method
.method public constructor <init>(Landroid/content/Context;Lio/fabric/sdk/android/services/d/a;Ljava/lang/String;)V
.registers 5
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lcom/crashlytics/android/core/LogFileManager;->context:Landroid/content/Context;
iput-object p2, p0, Lcom/crashlytics/android/core/LogFileManager;->fileStore:Lio/fabric/sdk/android/services/d/a;
sget-object v0, Lcom/crashlytics/android/core/LogFileManager;->NOOP_LOG_STORE:Lcom/crashlytics/android/core/LogFileManager$NoopLogStore;
iput-object v0, p0, Lcom/crashlytics/android/core/LogFileManager;->currentLog:Lcom/crashlytics/android/core/FileLogStore;
invoke-virtual {p0, p3}, Lcom/crashlytics/android/core/LogFileManager;->setCurrentSession(Ljava/lang/String;)V
return-void
.end method
.method private getLogFileDir()Ljava/io/File;
.registers 4
new-instance v0, Ljava/io/File;
iget-object v1, p0, Lcom/crashlytics/android/core/LogFileManager;->fileStore:Lio/fabric/sdk/android/services/d/a;
invoke-interface {v1}, Lio/fabric/sdk/android/services/d/a;->a()Ljava/io/File;
move-result-object v1
const-string v2, "log-files"
invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
invoke-virtual {v0}, Ljava/io/File;->exists()Z
move-result v1
if-nez v1, :cond_16
invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
:cond_16
return-object v0
.end method
.method private getSessionIdForFile(Ljava/io/File;)Ljava/lang/String;
.registers 5
invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;
move-result-object v0
const-string v1, ".temp"
invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I
move-result v1
const/4 v2, -0x1
if-ne v1, v2, :cond_e
:goto_d
return-object v0
:cond_e
const-string v2, "crashlytics-userlog-"
invoke-virtual {v2}, Ljava/lang/String;->length()I
move-result v2
invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
move-result-object v0
goto :goto_d
.end method
.method private getWorkingFileForSession(Ljava/lang/String;)Ljava/io/File;
.registers 5
new-instance v0, Ljava/lang/StringBuilder;
invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
const-string v1, "crashlytics-userlog-"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
const-string v1, ".temp"
invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
new-instance v1, Ljava/io/File;
invoke-direct {p0}, Lcom/crashlytics/android/core/LogFileManager;->getLogFileDir()Ljava/io/File;
move-result-object v2
invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
return-object v1
.end method
.method private isLoggingEnabled()Z
.registers 4
iget-object v0, p0, Lcom/crashlytics/android/core/LogFileManager;->context:Landroid/content/Context;
const-string v1, "com.crashlytics.CollectCustomLogs"
const/4 v2, 0x1
invoke-static {v0, v1, v2}, Lio/fabric/sdk/android/services/b/i;->a(Landroid/content/Context;Ljava/lang/String;Z)Z
move-result v0
return v0
.end method
.method public clearLog()V
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/LogFileManager;->currentLog:Lcom/crashlytics/android/core/FileLogStore;
invoke-interface {v0}, Lcom/crashlytics/android/core/FileLogStore;->deleteLogFile()V
return-void
.end method
.method public discardOldLogFiles(Ljava/util/Set;)V
.registers 7
invoke-direct {p0}, Lcom/crashlytics/android/core/LogFileManager;->getLogFileDir()Ljava/io/File;
move-result-object v0
invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;
move-result-object v1
if-eqz v1, :cond_20
array-length v2, v1
const/4 v0, 0x0
:goto_c
if-ge v0, v2, :cond_20
aget-object v3, v1, v0
invoke-direct {p0, v3}, Lcom/crashlytics/android/core/LogFileManager;->getSessionIdForFile(Ljava/io/File;)Ljava/lang/String;
move-result-object v4
invoke-interface {p1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
move-result v4
if-nez v4, :cond_1d
invoke-virtual {v3}, Ljava/io/File;->delete()Z
:cond_1d
add-int/lit8 v0, v0, 0x1
goto :goto_c
:cond_20
return-void
.end method
.method public getByteStringForLog()Lcom/crashlytics/android/core/ByteString;
.registers 2
iget-object v0, p0, Lcom/crashlytics/android/core/LogFileManager;->currentLog:Lcom/crashlytics/android/core/FileLogStore;
invoke-interface {v0}, Lcom/crashlytics/android/core/FileLogStore;->getLogAsByteString()Lcom/crashlytics/android/core/ByteString;
move-result-object v0
return-object v0
.end method
.method public final setCurrentSession(Ljava/lang/String;)V
.registers 5
iget-object v0, p0, Lcom/crashlytics/android/core/LogFileManager;->currentLog:Lcom/crashlytics/android/core/FileLogStore;
invoke-interface {v0}, Lcom/crashlytics/android/core/FileLogStore;->closeLogFile()V
sget-object v0, Lcom/crashlytics/android/core/LogFileManager;->NOOP_LOG_STORE:Lcom/crashlytics/android/core/LogFileManager$NoopLogStore;
iput-object v0, p0, Lcom/crashlytics/android/core/LogFileManager;->currentLog:Lcom/crashlytics/android/core/FileLogStore;
if-nez p1, :cond_c
:goto_b
return-void
:cond_c
invoke-direct {p0}, Lcom/crashlytics/android/core/LogFileManager;->isLoggingEnabled()Z
move-result v0
if-nez v0, :cond_1e
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "CrashlyticsCore"
const-string v2, "Preferences requested no custom logs. Aborting log file creation."
invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
goto :goto_b
:cond_1e
invoke-direct {p0, p1}, Lcom/crashlytics/android/core/LogFileManager;->getWorkingFileForSession(Ljava/lang/String;)Ljava/io/File;
move-result-object v0
const/high16 v1, 0x1
invoke-virtual {p0, v0, v1}, Lcom/crashlytics/android/core/LogFileManager;->setLogFile(Ljava/io/File;I)V
goto :goto_b
.end method
.method  setLogFile(Ljava/io/File;I)V
.registers 4
new-instance v0, Lcom/crashlytics/android/core/QueueFileLogStore;
invoke-direct {v0, p1, p2}, Lcom/crashlytics/android/core/QueueFileLogStore;-><init>(Ljava/io/File;I)V
iput-object v0, p0, Lcom/crashlytics/android/core/LogFileManager;->currentLog:Lcom/crashlytics/android/core/FileLogStore;
return-void
.end method
.method public writeToLog(JLjava/lang/String;)V
.registers 5
iget-object v0, p0, Lcom/crashlytics/android/core/LogFileManager;->currentLog:Lcom/crashlytics/android/core/FileLogStore;
invoke-interface {v0, p1, p2, p3}, Lcom/crashlytics/android/core/FileLogStore;->writeToLog(JLjava/lang/String;)V
return-void
.end method