.class public Lcom/crashlytics/android/core/CrashlyticsCore$Builder;
.super Ljava/lang/Object;
.source "CrashlyticsCore.java"
.field private delay:F
.field private disabled:Z
.field private listener:Lcom/crashlytics/android/core/CrashlyticsListener;
.field private pinningInfoProvider:Lcom/crashlytics/android/core/PinningInfoProvider;
.method public constructor <init>()V
.registers 2
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
const/high16 v0, -0x4080
iput v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->delay:F
const/4 v0, 0x0
iput-boolean v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->disabled:Z
return-void
.end method
.method public build()Lcom/crashlytics/android/core/CrashlyticsCore;
.registers 6
iget v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->delay:F
const/4 v1, 0x0
cmpg-float v0, v0, v1
if-gez v0, :cond_b
const/high16 v0, 0x3f80
iput v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->delay:F
:cond_b
new-instance v0, Lcom/crashlytics/android/core/CrashlyticsCore;
iget v1, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->delay:F
iget-object v2, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->listener:Lcom/crashlytics/android/core/CrashlyticsListener;
iget-object v3, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->pinningInfoProvider:Lcom/crashlytics/android/core/PinningInfoProvider;
iget-boolean v4, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->disabled:Z
invoke-direct {v0, v1, v2, v3, v4}, Lcom/crashlytics/android/core/CrashlyticsCore;-><init>(FLcom/crashlytics/android/core/CrashlyticsListener;Lcom/crashlytics/android/core/PinningInfoProvider;Z)V
return-object v0
.end method
.method public delay(F)Lcom/crashlytics/android/core/CrashlyticsCore$Builder;
.registers 4
const/4 v1, 0x0
cmpg-float v0, p1, v1
if-gtz v0, :cond_d
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "delay must be greater than 0"
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_d
iget v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->delay:F
cmpl-float v0, v0, v1
if-lez v0, :cond_1b
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "delay already set."
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_1b
iput p1, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->delay:F
return-object p0
.end method
.method public disabled(Z)Lcom/crashlytics/android/core/CrashlyticsCore$Builder;
.registers 2
iput-boolean p1, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->disabled:Z
return-object p0
.end method
.method public listener(Lcom/crashlytics/android/core/CrashlyticsListener;)Lcom/crashlytics/android/core/CrashlyticsCore$Builder;
.registers 4
if-nez p1, :cond_a
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "listener must not be null."
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_a
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->listener:Lcom/crashlytics/android/core/CrashlyticsListener;
if-eqz v0, :cond_16
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "listener already set."
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_16
iput-object p1, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->listener:Lcom/crashlytics/android/core/CrashlyticsListener;
return-object p0
.end method
.method public pinningInfo(Lcom/crashlytics/android/core/PinningInfoProvider;)Lcom/crashlytics/android/core/CrashlyticsCore$Builder;
.registers 4
if-nez p1, :cond_a
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "pinningInfoProvider must not be null."
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_a
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->pinningInfoProvider:Lcom/crashlytics/android/core/PinningInfoProvider;
if-eqz v0, :cond_16
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "pinningInfoProvider already set."
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_16
iput-object p1, p0, Lcom/crashlytics/android/core/CrashlyticsCore$Builder;->pinningInfoProvider:Lcom/crashlytics/android/core/PinningInfoProvider;
return-object p0
.end method