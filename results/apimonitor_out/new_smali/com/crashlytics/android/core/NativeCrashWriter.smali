.class  Lcom/crashlytics/android/core/NativeCrashWriter;
.super Ljava/lang/Object;
.source "NativeCrashWriter.java"
.field private static final DEFAULT_SIGNAL:Lcom/crashlytics/android/core/internal/models/SignalData; = null
.field private static final EMPTY_BINARY_IMAGE_MESSAGES:[Lcom/crashlytics/android/core/NativeCrashWriter$BinaryImageMessage; = null
.field private static final EMPTY_CHILDREN:[Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage; = null
.field private static final EMPTY_CUSTOM_ATTRIBUTE_MESSAGES:[Lcom/crashlytics/android/core/NativeCrashWriter$CustomAttributeMessage; = null
.field private static final EMPTY_FRAME_MESSAGES:[Lcom/crashlytics/android/core/NativeCrashWriter$FrameMessage; = null
.field private static final EMPTY_THREAD_MESSAGES:[Lcom/crashlytics/android/core/NativeCrashWriter$ThreadMessage; = null
.field static final NDK_CRASH_TYPE:Ljava/lang/String; = "ndk-crash"
.method static constructor <clinit>()V
.registers 6
const/4 v3, 0x0
new-instance v0, Lcom/crashlytics/android/core/internal/models/SignalData;
const-string v1, ""
const-string v2, ""
const-wide/16 v4, 0x0
invoke-direct {v0, v1, v2, v4, v5}, Lcom/crashlytics/android/core/internal/models/SignalData;-><init>(Ljava/lang/String;Ljava/lang/String;J)V
sput-object v0, Lcom/crashlytics/android/core/NativeCrashWriter;->DEFAULT_SIGNAL:Lcom/crashlytics/android/core/internal/models/SignalData;
new-array v0, v3, [Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;
sput-object v0, Lcom/crashlytics/android/core/NativeCrashWriter;->EMPTY_CHILDREN:[Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;
new-array v0, v3, [Lcom/crashlytics/android/core/NativeCrashWriter$ThreadMessage;
sput-object v0, Lcom/crashlytics/android/core/NativeCrashWriter;->EMPTY_THREAD_MESSAGES:[Lcom/crashlytics/android/core/NativeCrashWriter$ThreadMessage;
new-array v0, v3, [Lcom/crashlytics/android/core/NativeCrashWriter$FrameMessage;
sput-object v0, Lcom/crashlytics/android/core/NativeCrashWriter;->EMPTY_FRAME_MESSAGES:[Lcom/crashlytics/android/core/NativeCrashWriter$FrameMessage;
new-array v0, v3, [Lcom/crashlytics/android/core/NativeCrashWriter$BinaryImageMessage;
sput-object v0, Lcom/crashlytics/android/core/NativeCrashWriter;->EMPTY_BINARY_IMAGE_MESSAGES:[Lcom/crashlytics/android/core/NativeCrashWriter$BinaryImageMessage;
new-array v0, v3, [Lcom/crashlytics/android/core/NativeCrashWriter$CustomAttributeMessage;
sput-object v0, Lcom/crashlytics/android/core/NativeCrashWriter;->EMPTY_CUSTOM_ATTRIBUTE_MESSAGES:[Lcom/crashlytics/android/core/NativeCrashWriter$CustomAttributeMessage;
return-void
.end method
.method constructor <init>()V
.registers 1
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method static synthetic access$000()[Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;
.registers 1
sget-object v0, Lcom/crashlytics/android/core/NativeCrashWriter;->EMPTY_CHILDREN:[Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;
return-object v0
.end method
.method private static createBinaryImagesMessage([Lcom/crashlytics/android/core/internal/models/BinaryImageData;)Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
.registers 5
if-eqz p0, :cond_15
array-length v0, p0
new-array v0, v0, [Lcom/crashlytics/android/core/NativeCrashWriter$BinaryImageMessage;
:goto_5
const/4 v1, 0x0
:goto_6
array-length v2, v0
if-ge v1, v2, :cond_18
new-instance v2, Lcom/crashlytics/android/core/NativeCrashWriter$BinaryImageMessage;
aget-object v3, p0, v1
invoke-direct {v2, v3}, Lcom/crashlytics/android/core/NativeCrashWriter$BinaryImageMessage;-><init>(Lcom/crashlytics/android/core/internal/models/BinaryImageData;)V
aput-object v2, v0, v1
add-int/lit8 v1, v1, 0x1
goto :goto_6
:cond_15
sget-object v0, Lcom/crashlytics/android/core/NativeCrashWriter;->EMPTY_BINARY_IMAGE_MESSAGES:[Lcom/crashlytics/android/core/NativeCrashWriter$BinaryImageMessage;
goto :goto_5
:cond_18
new-instance v1, Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
invoke-direct {v1, v0}, Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;-><init>([Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;)V
return-object v1
.end method
.method private static createCustomAttributesMessage([Lcom/crashlytics/android/core/internal/models/CustomAttributeData;)Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
.registers 5
if-eqz p0, :cond_15
array-length v0, p0
new-array v0, v0, [Lcom/crashlytics/android/core/NativeCrashWriter$CustomAttributeMessage;
:goto_5
const/4 v1, 0x0
:goto_6
array-length v2, v0
if-ge v1, v2, :cond_18
new-instance v2, Lcom/crashlytics/android/core/NativeCrashWriter$CustomAttributeMessage;
aget-object v3, p0, v1
invoke-direct {v2, v3}, Lcom/crashlytics/android/core/NativeCrashWriter$CustomAttributeMessage;-><init>(Lcom/crashlytics/android/core/internal/models/CustomAttributeData;)V
aput-object v2, v0, v1
add-int/lit8 v1, v1, 0x1
goto :goto_6
:cond_15
sget-object v0, Lcom/crashlytics/android/core/NativeCrashWriter;->EMPTY_CUSTOM_ATTRIBUTE_MESSAGES:[Lcom/crashlytics/android/core/NativeCrashWriter$CustomAttributeMessage;
goto :goto_5
:cond_18
new-instance v1, Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
invoke-direct {v1, v0}, Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;-><init>([Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;)V
return-object v1
.end method
.method private static createDeviceMessage(Lcom/crashlytics/android/core/internal/models/DeviceData;)Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;
.registers 13
if-nez p0, :cond_8
new-instance v1, Lcom/crashlytics/android/core/NativeCrashWriter$NullMessage;
invoke-direct {v1}, Lcom/crashlytics/android/core/NativeCrashWriter$NullMessage;-><init>()V
:goto_7
return-object v1
:cond_8
new-instance v1, Lcom/crashlytics/android/core/NativeCrashWriter$DeviceMessage;
iget v0, p0, Lcom/crashlytics/android/core/internal/models/DeviceData;->batteryCapacity:I
int-to-float v0, v0
const/high16 v2, 0x42c8
div-float v2, v0, v2
iget v3, p0, Lcom/crashlytics/android/core/internal/models/DeviceData;->batteryVelocity:I
iget-boolean v4, p0, Lcom/crashlytics/android/core/internal/models/DeviceData;->proximity:Z
iget v5, p0, Lcom/crashlytics/android/core/internal/models/DeviceData;->orientation:I
iget-wide v6, p0, Lcom/crashlytics/android/core/internal/models/DeviceData;->totalPhysicalMemory:J
iget-wide v8, p0, Lcom/crashlytics/android/core/internal/models/DeviceData;->availablePhysicalMemory:J
sub-long/2addr v6, v8
iget-wide v8, p0, Lcom/crashlytics/android/core/internal/models/DeviceData;->totalInternalStorage:J
iget-wide v10, p0, Lcom/crashlytics/android/core/internal/models/DeviceData;->availableInternalStorage:J
sub-long/2addr v8, v10
invoke-direct/range {v1 .. v9}, Lcom/crashlytics/android/core/NativeCrashWriter$DeviceMessage;-><init>(FIZIJJ)V
goto :goto_7
.end method
.method private static createEventMessage(Lcom/crashlytics/android/core/internal/models/SessionEventData;Lcom/crashlytics/android/core/LogFileManager;Ljava/util/Map;)Lcom/crashlytics/android/core/NativeCrashWriter$EventMessage;
.registers 12
iget-object v0, p0, Lcom/crashlytics/android/core/internal/models/SessionEventData;->signal:Lcom/crashlytics/android/core/internal/models/SignalData;
if-eqz v0, :cond_62
iget-object v0, p0, Lcom/crashlytics/android/core/internal/models/SessionEventData;->signal:Lcom/crashlytics/android/core/internal/models/SignalData;
:goto_6
new-instance v1, Lcom/crashlytics/android/core/NativeCrashWriter$SignalMessage;
invoke-direct {v1, v0}, Lcom/crashlytics/android/core/NativeCrashWriter$SignalMessage;-><init>(Lcom/crashlytics/android/core/internal/models/SignalData;)V
iget-object v0, p0, Lcom/crashlytics/android/core/internal/models/SessionEventData;->threads:[Lcom/crashlytics/android/core/internal/models/ThreadData;
invoke-static {v0}, Lcom/crashlytics/android/core/NativeCrashWriter;->createThreadsMessage([Lcom/crashlytics/android/core/internal/models/ThreadData;)Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
move-result-object v0
iget-object v2, p0, Lcom/crashlytics/android/core/internal/models/SessionEventData;->binaryImages:[Lcom/crashlytics/android/core/internal/models/BinaryImageData;
invoke-static {v2}, Lcom/crashlytics/android/core/NativeCrashWriter;->createBinaryImagesMessage([Lcom/crashlytics/android/core/internal/models/BinaryImageData;)Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
move-result-object v2
new-instance v3, Lcom/crashlytics/android/core/NativeCrashWriter$ExecutionMessage;
invoke-direct {v3, v1, v0, v2}, Lcom/crashlytics/android/core/NativeCrashWriter$ExecutionMessage;-><init>(Lcom/crashlytics/android/core/NativeCrashWriter$SignalMessage;Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;)V
iget-object v0, p0, Lcom/crashlytics/android/core/internal/models/SessionEventData;->customAttributes:[Lcom/crashlytics/android/core/internal/models/CustomAttributeData;
invoke-static {v0, p2}, Lcom/crashlytics/android/core/NativeCrashWriter;->mergeCustomAttributes([Lcom/crashlytics/android/core/internal/models/CustomAttributeData;Ljava/util/Map;)[Lcom/crashlytics/android/core/internal/models/CustomAttributeData;
move-result-object v0
invoke-static {v0}, Lcom/crashlytics/android/core/NativeCrashWriter;->createCustomAttributesMessage([Lcom/crashlytics/android/core/internal/models/CustomAttributeData;)Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
move-result-object v0
new-instance v1, Lcom/crashlytics/android/core/NativeCrashWriter$ApplicationMessage;
invoke-direct {v1, v3, v0}, Lcom/crashlytics/android/core/NativeCrashWriter$ApplicationMessage;-><init>(Lcom/crashlytics/android/core/NativeCrashWriter$ExecutionMessage;Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;)V
iget-object v0, p0, Lcom/crashlytics/android/core/internal/models/SessionEventData;->deviceData:Lcom/crashlytics/android/core/internal/models/DeviceData;
invoke-static {v0}, Lcom/crashlytics/android/core/NativeCrashWriter;->createDeviceMessage(Lcom/crashlytics/android/core/internal/models/DeviceData;)Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;
move-result-object v2
invoke-virtual {p1}, Lcom/crashlytics/android/core/LogFileManager;->getByteStringForLog()Lcom/crashlytics/android/core/ByteString;
move-result-object v3
if-nez v3, :cond_42
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v4, "CrashlyticsCore"
const-string v5, "No log data to include with this event."
invoke-interface {v0, v4, v5}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
:cond_42
invoke-virtual {p1}, Lcom/crashlytics/android/core/LogFileManager;->clearLog()V
if-eqz v3, :cond_65
new-instance v0, Lcom/crashlytics/android/core/NativeCrashWriter$LogMessage;
invoke-direct {v0, v3}, Lcom/crashlytics/android/core/NativeCrashWriter$LogMessage;-><init>(Lcom/crashlytics/android/core/ByteString;)V
:goto_4c
new-instance v3, Lcom/crashlytics/android/core/NativeCrashWriter$EventMessage;
iget-wide v4, p0, Lcom/crashlytics/android/core/internal/models/SessionEventData;->timestamp:J
const-string v6, "ndk-crash"
const/4 v7, 0x3
new-array v7, v7, [Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;
const/4 v8, 0x0
aput-object v1, v7, v8
const/4 v1, 0x1
aput-object v2, v7, v1
const/4 v1, 0x2
aput-object v0, v7, v1
invoke-direct {v3, v4, v5, v6, v7}, Lcom/crashlytics/android/core/NativeCrashWriter$EventMessage;-><init>(JLjava/lang/String;[Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;)V
return-object v3
:cond_62
sget-object v0, Lcom/crashlytics/android/core/NativeCrashWriter;->DEFAULT_SIGNAL:Lcom/crashlytics/android/core/internal/models/SignalData;
goto :goto_6
:cond_65
new-instance v0, Lcom/crashlytics/android/core/NativeCrashWriter$NullMessage;
invoke-direct {v0}, Lcom/crashlytics/android/core/NativeCrashWriter$NullMessage;-><init>()V
goto :goto_4c
.end method
.method private static createFramesMessage([Lcom/crashlytics/android/core/internal/models/ThreadData$FrameData;)Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
.registers 5
if-eqz p0, :cond_15
array-length v0, p0
new-array v0, v0, [Lcom/crashlytics/android/core/NativeCrashWriter$FrameMessage;
:goto_5
const/4 v1, 0x0
:goto_6
array-length v2, v0
if-ge v1, v2, :cond_18
new-instance v2, Lcom/crashlytics/android/core/NativeCrashWriter$FrameMessage;
aget-object v3, p0, v1
invoke-direct {v2, v3}, Lcom/crashlytics/android/core/NativeCrashWriter$FrameMessage;-><init>(Lcom/crashlytics/android/core/internal/models/ThreadData$FrameData;)V
aput-object v2, v0, v1
add-int/lit8 v1, v1, 0x1
goto :goto_6
:cond_15
sget-object v0, Lcom/crashlytics/android/core/NativeCrashWriter;->EMPTY_FRAME_MESSAGES:[Lcom/crashlytics/android/core/NativeCrashWriter$FrameMessage;
goto :goto_5
:cond_18
new-instance v1, Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
invoke-direct {v1, v0}, Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;-><init>([Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;)V
return-object v1
.end method
.method private static createThreadsMessage([Lcom/crashlytics/android/core/internal/models/ThreadData;)Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
.registers 6
if-eqz p0, :cond_1b
array-length v0, p0
new-array v0, v0, [Lcom/crashlytics/android/core/NativeCrashWriter$ThreadMessage;
:goto_5
const/4 v1, 0x0
:goto_6
array-length v2, v0
if-ge v1, v2, :cond_1e
aget-object v2, p0, v1
new-instance v3, Lcom/crashlytics/android/core/NativeCrashWriter$ThreadMessage;
iget-object v4, v2, Lcom/crashlytics/android/core/internal/models/ThreadData;->frames:[Lcom/crashlytics/android/core/internal/models/ThreadData$FrameData;
invoke-static {v4}, Lcom/crashlytics/android/core/NativeCrashWriter;->createFramesMessage([Lcom/crashlytics/android/core/internal/models/ThreadData$FrameData;)Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
move-result-object v4
invoke-direct {v3, v2, v4}, Lcom/crashlytics/android/core/NativeCrashWriter$ThreadMessage;-><init>(Lcom/crashlytics/android/core/internal/models/ThreadData;Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;)V
aput-object v3, v0, v1
add-int/lit8 v1, v1, 0x1
goto :goto_6
:cond_1b
sget-object v0, Lcom/crashlytics/android/core/NativeCrashWriter;->EMPTY_THREAD_MESSAGES:[Lcom/crashlytics/android/core/NativeCrashWriter$ThreadMessage;
goto :goto_5
:cond_1e
new-instance v1, Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
invoke-direct {v1, v0}, Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;-><init>([Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;)V
return-object v1
.end method
.method private static mergeCustomAttributes([Lcom/crashlytics/android/core/internal/models/CustomAttributeData;Ljava/util/Map;)[Lcom/crashlytics/android/core/internal/models/CustomAttributeData;
.registers 8
const/4 v1, 0x0
new-instance v2, Ljava/util/TreeMap;
invoke-direct {v2, p1}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V
if-eqz p0, :cond_18
array-length v3, p0
move v0, v1
:goto_a
if-ge v0, v3, :cond_18
aget-object v4, p0, v0
iget-object v5, v4, Lcom/crashlytics/android/core/internal/models/CustomAttributeData;->key:Ljava/lang/String;
iget-object v4, v4, Lcom/crashlytics/android/core/internal/models/CustomAttributeData;->value:Ljava/lang/String;
invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
add-int/lit8 v0, v0, 0x1
goto :goto_a
:cond_18
invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;
move-result-object v0
invoke-interface {v2}, Ljava/util/Map;->size()I
move-result v2
new-array v2, v2, [Ljava/util/Map$Entry;
invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
move-result-object v0
check-cast v0, [Ljava/util/Map$Entry;
array-length v2, v0
new-array v4, v2, [Lcom/crashlytics/android/core/internal/models/CustomAttributeData;
move v3, v1
:goto_2c
array-length v1, v4
if-ge v3, v1, :cond_4a
new-instance v5, Lcom/crashlytics/android/core/internal/models/CustomAttributeData;
aget-object v1, v0, v3
invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;
move-result-object v1
check-cast v1, Ljava/lang/String;
aget-object v2, v0, v3
invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;
move-result-object v2
check-cast v2, Ljava/lang/String;
invoke-direct {v5, v1, v2}, Lcom/crashlytics/android/core/internal/models/CustomAttributeData;-><init>(Ljava/lang/String;Ljava/lang/String;)V
aput-object v5, v4, v3
add-int/lit8 v1, v3, 0x1
move v3, v1
goto :goto_2c
:cond_4a
return-object v4
.end method
.method public static writeNativeCrash(Lcom/crashlytics/android/core/internal/models/SessionEventData;Lcom/crashlytics/android/core/LogFileManager;Ljava/util/Map;Lcom/crashlytics/android/core/CodedOutputStream;)V
.registers 5
invoke-static {p0, p1, p2}, Lcom/crashlytics/android/core/NativeCrashWriter;->createEventMessage(Lcom/crashlytics/android/core/internal/models/SessionEventData;Lcom/crashlytics/android/core/LogFileManager;Ljava/util/Map;)Lcom/crashlytics/android/core/NativeCrashWriter$EventMessage;
move-result-object v0
invoke-virtual {v0, p3}, Lcom/crashlytics/android/core/NativeCrashWriter$EventMessage;->write(Lcom/crashlytics/android/core/CodedOutputStream;)V
return-void
.end method