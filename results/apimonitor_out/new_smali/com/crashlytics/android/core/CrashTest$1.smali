.class  Lcom/crashlytics/android/core/CrashTest$1;
.super Landroid/os/AsyncTask;
.source "CrashTest.java"
.field final synthetic this$0:Lcom/crashlytics/android/core/CrashTest;
.field final synthetic val$delayMs:J
.method constructor <init>(Lcom/crashlytics/android/core/CrashTest;J)V
.registers 4
iput-object p1, p0, Lcom/crashlytics/android/core/CrashTest$1;->this$0:Lcom/crashlytics/android/core/CrashTest;
iput-wide p2, p0, Lcom/crashlytics/android/core/CrashTest$1;->val$delayMs:J
invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V
return-void
.end method
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
.registers 3
check-cast p1, [Ljava/lang/Void;
invoke-virtual {p0, p1}, Lcom/crashlytics/android/core/CrashTest$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
move-result-object v0
return-object v0
.end method
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
.registers 4
:try_start_0
iget-wide v0, p0, Lcom/crashlytics/android/core/CrashTest$1;->val$delayMs:J
invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
:try_end_5
.catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_5} :catch_e
:goto_5
iget-object v0, p0, Lcom/crashlytics/android/core/CrashTest$1;->this$0:Lcom/crashlytics/android/core/CrashTest;
const-string v1, "Background thread crash"
invoke-virtual {v0, v1}, Lcom/crashlytics/android/core/CrashTest;->throwRuntimeException(Ljava/lang/String;)V
const/4 v0, 0x0
return-object v0
:catch_e
move-exception v0
goto :goto_5
.end method