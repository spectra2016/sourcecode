.class final Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
.super Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;
.source "NativeCrashWriter.java"
.field private final messages:[Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;
.method public varargs constructor <init>([Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;)V
.registers 4
const/4 v1, 0x0
new-array v0, v1, [Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;
invoke-direct {p0, v1, v0}, Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;-><init>(I[Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;)V
iput-object p1, p0, Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;->messages:[Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;
return-void
.end method
.method public getSize()I
.registers 6
const/4 v0, 0x0
iget-object v2, p0, Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;->messages:[Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;
array-length v3, v2
move v1, v0
:goto_5
if-ge v0, v3, :cond_11
aget-object v4, v2, v0
invoke-virtual {v4}, Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;->getSize()I
move-result v4
add-int/2addr v1, v4
add-int/lit8 v0, v0, 0x1
goto :goto_5
:cond_11
return v1
.end method
.method public write(Lcom/crashlytics/android/core/CodedOutputStream;)V
.registers 6
iget-object v1, p0, Lcom/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;->messages:[Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;
array-length v2, v1
const/4 v0, 0x0
:goto_4
if-ge v0, v2, :cond_e
aget-object v3, v1, v0
invoke-virtual {v3, p1}, Lcom/crashlytics/android/core/NativeCrashWriter$ProtobufMessage;->write(Lcom/crashlytics/android/core/CodedOutputStream;)V
add-int/lit8 v0, v0, 0x1
goto :goto_4
:cond_e
return-void
.end method