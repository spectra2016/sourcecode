.class  Lcom/crashlytics/android/core/ReportUploader$Worker;
.super Lio/fabric/sdk/android/services/b/h;
.source "ReportUploader.java"
.field private final delay:F
.field final synthetic this$0:Lcom/crashlytics/android/core/ReportUploader;
.method constructor <init>(Lcom/crashlytics/android/core/ReportUploader;F)V
.registers 3
iput-object p1, p0, Lcom/crashlytics/android/core/ReportUploader$Worker;->this$0:Lcom/crashlytics/android/core/ReportUploader;
invoke-direct {p0}, Lio/fabric/sdk/android/services/b/h;-><init>()V
iput p2, p0, Lcom/crashlytics/android/core/ReportUploader$Worker;->delay:F
return-void
.end method
.method private attemptUploadWithRetry()V
.registers 10
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v1, "CrashlyticsCore"
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Starting report processing in "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget v3, p0, Lcom/crashlytics/android/core/ReportUploader$Worker;->delay:F
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, " second(s)..."
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
iget v0, p0, Lcom/crashlytics/android/core/ReportUploader$Worker;->delay:F
const/4 v1, 0x0
cmpl-float v0, v0, v1
if-lez v0, :cond_34
:try_start_2b
iget v0, p0, Lcom/crashlytics/android/core/ReportUploader$Worker;->delay:F
const/high16 v1, 0x447a
mul-float/2addr v0, v1
float-to-long v0, v0
invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
:cond_34
:try_end_34
.catch Ljava/lang/InterruptedException; {:try_start_2b .. :try_end_34} :catch_49
invoke-static {}, Lcom/crashlytics/android/core/CrashlyticsCore;->getInstance()Lcom/crashlytics/android/core/CrashlyticsCore;
move-result-object v0
invoke-virtual {v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->getHandler()Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
move-result-object v2
iget-object v1, p0, Lcom/crashlytics/android/core/ReportUploader$Worker;->this$0:Lcom/crashlytics/android/core/ReportUploader;
invoke-virtual {v1}, Lcom/crashlytics/android/core/ReportUploader;->findReports()Ljava/util/List;
move-result-object v1
invoke-virtual {v2}, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;->isHandlingException()Z
move-result v2
if-eqz v2, :cond_52
:cond_48
:goto_48
return-void
:catch_49
move-exception v0
invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
goto :goto_48
:cond_52
invoke-interface {v1}, Ljava/util/List;->isEmpty()Z
move-result v2
if-nez v2, :cond_98
invoke-virtual {v0}, Lcom/crashlytics/android/core/CrashlyticsCore;->canSendWithUserApproval()Z
move-result v0
if-nez v0, :cond_98
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v0
const-string v2, "CrashlyticsCore"
new-instance v3, Ljava/lang/StringBuilder;
invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
const-string v4, "User declined to send. Removing "
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-interface {v1}, Ljava/util/List;->size()I
move-result v4
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v3
const-string v4, " Report(s)."
invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v3
invoke-interface {v0, v2, v3}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v1
:goto_88
invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_48
invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lcom/crashlytics/android/core/Report;
invoke-interface {v0}, Lcom/crashlytics/android/core/Report;->remove()V
goto :goto_88
:cond_98
const/4 v0, 0x0
move v8, v0
move-object v0, v1
move v1, v8
:goto_9c
invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
move-result v2
if-nez v2, :cond_48
invoke-static {}, Lcom/crashlytics/android/core/CrashlyticsCore;->getInstance()Lcom/crashlytics/android/core/CrashlyticsCore;
move-result-object v2
invoke-virtual {v2}, Lcom/crashlytics/android/core/CrashlyticsCore;->getHandler()Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
move-result-object v2
invoke-virtual {v2}, Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;->isHandlingException()Z
move-result v2
if-nez v2, :cond_48
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v2
const-string v3, "CrashlyticsCore"
new-instance v4, Ljava/lang/StringBuilder;
invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
const-string v5, "Attempting to send "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-interface {v0}, Ljava/util/List;->size()I
move-result v5
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v4
const-string v5, " report(s)"
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v4
invoke-interface {v2, v3, v4}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;
move-result-object v2
:goto_da
invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
move-result v0
if-eqz v0, :cond_ec
invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;
move-result-object v0
check-cast v0, Lcom/crashlytics/android/core/Report;
iget-object v3, p0, Lcom/crashlytics/android/core/ReportUploader$Worker;->this$0:Lcom/crashlytics/android/core/ReportUploader;
invoke-virtual {v3, v0}, Lcom/crashlytics/android/core/ReportUploader;->forceUpload(Lcom/crashlytics/android/core/Report;)Z
goto :goto_da
:cond_ec
iget-object v0, p0, Lcom/crashlytics/android/core/ReportUploader$Worker;->this$0:Lcom/crashlytics/android/core/ReportUploader;
invoke-virtual {v0}, Lcom/crashlytics/android/core/ReportUploader;->findReports()Ljava/util/List;
move-result-object v2
invoke-interface {v2}, Ljava/util/List;->isEmpty()Z
move-result v0
if-nez v0, :cond_142
invoke-static {}, Lcom/crashlytics/android/core/ReportUploader;->access$100()[S
move-result-object v3
add-int/lit8 v0, v1, 0x1
invoke-static {}, Lcom/crashlytics/android/core/ReportUploader;->access$100()[S
move-result-object v4
array-length v4, v4
add-int/lit8 v4, v4, -0x1
invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I
move-result v1
aget-short v1, v3, v1
int-to-long v4, v1
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v3, "CrashlyticsCore"
new-instance v6, Ljava/lang/StringBuilder;
invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V
const-string v7, "Report submisson: scheduling delayed retry in "
invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v6
invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
move-result-object v6
const-string v7, " seconds"
invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v6
invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v6
invoke-interface {v1, v3, v6}, Lio/fabric/sdk/android/k;->a(Ljava/lang/String;Ljava/lang/String;)V
const-wide/16 v6, 0x3e8
mul-long/2addr v4, v6
:try_start_131
invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
:try_end_134
.catch Ljava/lang/InterruptedException; {:try_start_131 .. :try_end_134} :catch_138
move v1, v0
move-object v0, v2
goto/16 :goto_9c
:catch_138
move-exception v0
invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;
move-result-object v0
invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
goto/16 :goto_48
:cond_142
move-object v0, v2
goto/16 :goto_9c
.end method
.method public onRun()V
.registers 5
:try_start_0
invoke-direct {p0}, Lcom/crashlytics/android/core/ReportUploader$Worker;->attemptUploadWithRetry()V
:goto_3
:try_end_3
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3} :catch_a
iget-object v0, p0, Lcom/crashlytics/android/core/ReportUploader$Worker;->this$0:Lcom/crashlytics/android/core/ReportUploader;
const/4 v1, 0x0
#setter for: Lcom/crashlytics/android/core/ReportUploader;->uploadThread:Ljava/lang/Thread;
invoke-static {v0, v1}, Lcom/crashlytics/android/core/ReportUploader;->access$002(Lcom/crashlytics/android/core/ReportUploader;Ljava/lang/Thread;)Ljava/lang/Thread;
return-void
:catch_a
move-exception v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "CrashlyticsCore"
const-string v3, "An unexpected error occurred while attempting to upload crash reports."
invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
goto :goto_3
.end method