.class  Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper$2;
.super Ljava/lang/Object;
.source "CrashlyticsExecutorServiceWrapper.java"
.implements Ljava/util/concurrent/Callable;
.field final synthetic this$0:Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;
.field final synthetic val$callable:Ljava/util/concurrent/Callable;
.method constructor <init>(Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;Ljava/util/concurrent/Callable;)V
.registers 3
iput-object p1, p0, Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper$2;->this$0:Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;
iput-object p2, p0, Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper$2;->val$callable:Ljava/util/concurrent/Callable;
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
return-void
.end method
.method public call()Ljava/lang/Object;
.registers 5
:try_start_0
iget-object v0, p0, Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper$2;->val$callable:Ljava/util/concurrent/Callable;
invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
:try_end_5
.catch Ljava/lang/Exception; {:try_start_0 .. :try_end_5} :catch_7
move-result-object v0
:goto_6
return-object v0
:catch_7
move-exception v0
invoke-static {}, Lio/fabric/sdk/android/c;->h()Lio/fabric/sdk/android/k;
move-result-object v1
const-string v2, "CrashlyticsCore"
const-string v3, "Failed to execute task."
invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/k;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
const/4 v0, 0x0
goto :goto_6
.end method