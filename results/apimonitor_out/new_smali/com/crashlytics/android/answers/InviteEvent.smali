.class public Lcom/crashlytics/android/answers/InviteEvent;
.super Lcom/crashlytics/android/answers/PredefinedEvent;
.source "InviteEvent.java"
.field static final METHOD_ATTRIBUTE:Ljava/lang/String; = "method"
.field static final TYPE:Ljava/lang/String; = "invite"
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Lcom/crashlytics/android/answers/PredefinedEvent;-><init>()V
return-void
.end method
.method  getPredefinedType()Ljava/lang/String;
.registers 2
const-string v0, "invite"
return-object v0
.end method
.method public putMethod(Ljava/lang/String;)Lcom/crashlytics/android/answers/InviteEvent;
.registers 4
iget-object v0, p0, Lcom/crashlytics/android/answers/InviteEvent;->predefinedAttributes:Lcom/crashlytics/android/answers/AnswersAttributes;
const-string v1, "method"
invoke-virtual {v0, v1, p1}, Lcom/crashlytics/android/answers/AnswersAttributes;->put(Ljava/lang/String;Ljava/lang/String;)V
return-object p0
.end method