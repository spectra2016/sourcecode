.class  Lcom/crashlytics/android/answers/AnswersAttributes;
.super Ljava/lang/Object;
.source "AnswersAttributes.java"
.field final attributes:Ljava/util/Map;
.field final validator:Lcom/crashlytics/android/answers/AnswersEventValidator;
.method public constructor <init>(Lcom/crashlytics/android/answers/AnswersEventValidator;)V
.registers 3
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;
invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V
iput-object v0, p0, Lcom/crashlytics/android/answers/AnswersAttributes;->attributes:Ljava/util/Map;
iput-object p1, p0, Lcom/crashlytics/android/answers/AnswersAttributes;->validator:Lcom/crashlytics/android/answers/AnswersEventValidator;
return-void
.end method
.method  put(Ljava/lang/String;Ljava/lang/Number;)V
.registers 5
iget-object v0, p0, Lcom/crashlytics/android/answers/AnswersAttributes;->validator:Lcom/crashlytics/android/answers/AnswersEventValidator;
const-string v1, "key"
invoke-virtual {v0, p1, v1}, Lcom/crashlytics/android/answers/AnswersEventValidator;->isNull(Ljava/lang/Object;Ljava/lang/String;)Z
move-result v0
if-nez v0, :cond_14
iget-object v0, p0, Lcom/crashlytics/android/answers/AnswersAttributes;->validator:Lcom/crashlytics/android/answers/AnswersEventValidator;
const-string v1, "value"
invoke-virtual {v0, p2, v1}, Lcom/crashlytics/android/answers/AnswersEventValidator;->isNull(Ljava/lang/Object;Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_15
:goto_14
:cond_14
return-void
:cond_15
iget-object v0, p0, Lcom/crashlytics/android/answers/AnswersAttributes;->validator:Lcom/crashlytics/android/answers/AnswersEventValidator;
invoke-virtual {v0, p1}, Lcom/crashlytics/android/answers/AnswersEventValidator;->limitStringLength(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
invoke-virtual {p0, v0, p2}, Lcom/crashlytics/android/answers/AnswersAttributes;->putAttribute(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_14
.end method
.method  put(Ljava/lang/String;Ljava/lang/String;)V
.registers 5
iget-object v0, p0, Lcom/crashlytics/android/answers/AnswersAttributes;->validator:Lcom/crashlytics/android/answers/AnswersEventValidator;
const-string v1, "key"
invoke-virtual {v0, p1, v1}, Lcom/crashlytics/android/answers/AnswersEventValidator;->isNull(Ljava/lang/Object;Ljava/lang/String;)Z
move-result v0
if-nez v0, :cond_14
iget-object v0, p0, Lcom/crashlytics/android/answers/AnswersAttributes;->validator:Lcom/crashlytics/android/answers/AnswersEventValidator;
const-string v1, "value"
invoke-virtual {v0, p2, v1}, Lcom/crashlytics/android/answers/AnswersEventValidator;->isNull(Ljava/lang/Object;Ljava/lang/String;)Z
move-result v0
if-eqz v0, :cond_15
:goto_14
:cond_14
return-void
:cond_15
iget-object v0, p0, Lcom/crashlytics/android/answers/AnswersAttributes;->validator:Lcom/crashlytics/android/answers/AnswersEventValidator;
invoke-virtual {v0, p1}, Lcom/crashlytics/android/answers/AnswersEventValidator;->limitStringLength(Ljava/lang/String;)Ljava/lang/String;
move-result-object v0
iget-object v1, p0, Lcom/crashlytics/android/answers/AnswersAttributes;->validator:Lcom/crashlytics/android/answers/AnswersEventValidator;
invoke-virtual {v1, p2}, Lcom/crashlytics/android/answers/AnswersEventValidator;->limitStringLength(Ljava/lang/String;)Ljava/lang/String;
move-result-object v1
invoke-virtual {p0, v0, v1}, Lcom/crashlytics/android/answers/AnswersAttributes;->putAttribute(Ljava/lang/String;Ljava/lang/Object;)V
goto :goto_14
.end method
.method  putAttribute(Ljava/lang/String;Ljava/lang/Object;)V
.registers 5
iget-object v0, p0, Lcom/crashlytics/android/answers/AnswersAttributes;->validator:Lcom/crashlytics/android/answers/AnswersEventValidator;
iget-object v1, p0, Lcom/crashlytics/android/answers/AnswersAttributes;->attributes:Ljava/util/Map;
invoke-virtual {v0, v1, p1}, Lcom/crashlytics/android/answers/AnswersEventValidator;->isFullMap(Ljava/util/Map;Ljava/lang/String;)Z
move-result v0
if-nez v0, :cond_f
iget-object v0, p0, Lcom/crashlytics/android/answers/AnswersAttributes;->attributes:Ljava/util/Map;
invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
:cond_f
return-void
.end method
.method public toString()Ljava/lang/String;
.registers 3
new-instance v0, Lorg/json/JSONObject;
iget-object v1, p0, Lcom/crashlytics/android/answers/AnswersAttributes;->attributes:Ljava/util/Map;
invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V
invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
move-result-object v0
return-object v0
.end method