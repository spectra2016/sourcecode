.class  Lcom/crashlytics/android/answers/SessionMetadataCollector;
.super Ljava/lang/Object;
.source "SessionMetadataCollector.java"
.field private final context:Landroid/content/Context;
.field private final idManager:Lio/fabric/sdk/android/services/b/o;
.field private final versionCode:Ljava/lang/String;
.field private final versionName:Ljava/lang/String;
.method public constructor <init>(Landroid/content/Context;Lio/fabric/sdk/android/services/b/o;Ljava/lang/String;Ljava/lang/String;)V
.registers 5
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
iput-object p1, p0, Lcom/crashlytics/android/answers/SessionMetadataCollector;->context:Landroid/content/Context;
iput-object p2, p0, Lcom/crashlytics/android/answers/SessionMetadataCollector;->idManager:Lio/fabric/sdk/android/services/b/o;
iput-object p3, p0, Lcom/crashlytics/android/answers/SessionMetadataCollector;->versionCode:Ljava/lang/String;
iput-object p4, p0, Lcom/crashlytics/android/answers/SessionMetadataCollector;->versionName:Ljava/lang/String;
return-void
.end method
.method public getMetadata()Lcom/crashlytics/android/answers/SessionEventMetadata;
.registers 14
iget-object v0, p0, Lcom/crashlytics/android/answers/SessionMetadataCollector;->idManager:Lio/fabric/sdk/android/services/b/o;
invoke-virtual {v0}, Lio/fabric/sdk/android/services/b/o;->i()Ljava/util/Map;
move-result-object v0
iget-object v1, p0, Lcom/crashlytics/android/answers/SessionMetadataCollector;->idManager:Lio/fabric/sdk/android/services/b/o;
invoke-virtual {v1}, Lio/fabric/sdk/android/services/b/o;->c()Ljava/lang/String;
move-result-object v1
iget-object v2, p0, Lcom/crashlytics/android/answers/SessionMetadataCollector;->idManager:Lio/fabric/sdk/android/services/b/o;
invoke-virtual {v2}, Lio/fabric/sdk/android/services/b/o;->b()Ljava/lang/String;
move-result-object v3
sget-object v2, Lio/fabric/sdk/android/services/b/o$a;->d:Lio/fabric/sdk/android/services/b/o$a;
invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v4
check-cast v4, Ljava/lang/String;
sget-object v2, Lio/fabric/sdk/android/services/b/o$a;->g:Lio/fabric/sdk/android/services/b/o$a;
invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v5
check-cast v5, Ljava/lang/String;
iget-object v2, p0, Lcom/crashlytics/android/answers/SessionMetadataCollector;->idManager:Lio/fabric/sdk/android/services/b/o;
invoke-virtual {v2}, Lio/fabric/sdk/android/services/b/o;->l()Ljava/lang/Boolean;
move-result-object v6
sget-object v2, Lio/fabric/sdk/android/services/b/o$a;->c:Lio/fabric/sdk/android/services/b/o$a;
invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
move-result-object v7
check-cast v7, Ljava/lang/String;
iget-object v0, p0, Lcom/crashlytics/android/answers/SessionMetadataCollector;->context:Landroid/content/Context;
invoke-static {v0}, Lio/fabric/sdk/android/services/b/i;->m(Landroid/content/Context;)Ljava/lang/String;
move-result-object v8
iget-object v0, p0, Lcom/crashlytics/android/answers/SessionMetadataCollector;->idManager:Lio/fabric/sdk/android/services/b/o;
invoke-virtual {v0}, Lio/fabric/sdk/android/services/b/o;->d()Ljava/lang/String;
move-result-object v9
iget-object v0, p0, Lcom/crashlytics/android/answers/SessionMetadataCollector;->idManager:Lio/fabric/sdk/android/services/b/o;
invoke-virtual {v0}, Lio/fabric/sdk/android/services/b/o;->g()Ljava/lang/String;
move-result-object v10
invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;
move-result-object v0
invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;
move-result-object v2
new-instance v0, Lcom/crashlytics/android/answers/SessionEventMetadata;
iget-object v11, p0, Lcom/crashlytics/android/answers/SessionMetadataCollector;->versionCode:Ljava/lang/String;
iget-object v12, p0, Lcom/crashlytics/android/answers/SessionMetadataCollector;->versionName:Ljava/lang/String;
invoke-direct/range {v0 .. v12}, Lcom/crashlytics/android/answers/SessionEventMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
return-object v0
.end method