.class  Lcom/crashlytics/android/answers/RetryManager;
.super Ljava/lang/Object;
.source "RetryManager.java"
.field private static final NANOSECONDS_IN_MS:J = 0xf4240L
.field  lastRetry:J
.field private retryState:Lio/fabric/sdk/android/services/concurrency/a/e;
.method public constructor <init>(Lio/fabric/sdk/android/services/concurrency/a/e;)V
.registers 4
invoke-direct {p0}, Ljava/lang/Object;-><init>()V
if-nez p1, :cond_d
new-instance v0, Ljava/lang/NullPointerException;
const-string v1, "retryState must not be null"
invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V
throw v0
:cond_d
iput-object p1, p0, Lcom/crashlytics/android/answers/RetryManager;->retryState:Lio/fabric/sdk/android/services/concurrency/a/e;
return-void
.end method
.method public canRetry(J)Z
.registers 8
const-wide/32 v0, 0xf4240
iget-object v2, p0, Lcom/crashlytics/android/answers/RetryManager;->retryState:Lio/fabric/sdk/android/services/concurrency/a/e;
invoke-virtual {v2}, Lio/fabric/sdk/android/services/concurrency/a/e;->a()J
move-result-wide v2
mul-long/2addr v0, v2
iget-wide v2, p0, Lcom/crashlytics/android/answers/RetryManager;->lastRetry:J
sub-long v2, p1, v2
cmp-long v0, v2, v0
if-ltz v0, :cond_14
const/4 v0, 0x1
:goto_13
return v0
:cond_14
const/4 v0, 0x0
goto :goto_13
.end method
.method public recordRetry(J)V
.registers 4
iput-wide p1, p0, Lcom/crashlytics/android/answers/RetryManager;->lastRetry:J
iget-object v0, p0, Lcom/crashlytics/android/answers/RetryManager;->retryState:Lio/fabric/sdk/android/services/concurrency/a/e;
invoke-virtual {v0}, Lio/fabric/sdk/android/services/concurrency/a/e;->b()Lio/fabric/sdk/android/services/concurrency/a/e;
move-result-object v0
iput-object v0, p0, Lcom/crashlytics/android/answers/RetryManager;->retryState:Lio/fabric/sdk/android/services/concurrency/a/e;
return-void
.end method
.method public reset()V
.registers 3
const-wide/16 v0, 0x0
iput-wide v0, p0, Lcom/crashlytics/android/answers/RetryManager;->lastRetry:J
iget-object v0, p0, Lcom/crashlytics/android/answers/RetryManager;->retryState:Lio/fabric/sdk/android/services/concurrency/a/e;
invoke-virtual {v0}, Lio/fabric/sdk/android/services/concurrency/a/e;->c()Lio/fabric/sdk/android/services/concurrency/a/e;
move-result-object v0
iput-object v0, p0, Lcom/crashlytics/android/answers/RetryManager;->retryState:Lio/fabric/sdk/android/services/concurrency/a/e;
return-void
.end method