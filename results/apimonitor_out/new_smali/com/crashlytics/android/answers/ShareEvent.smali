.class public Lcom/crashlytics/android/answers/ShareEvent;
.super Lcom/crashlytics/android/answers/PredefinedEvent;
.source "ShareEvent.java"
.field static final CONTENT_ID_ATTRIBUTE:Ljava/lang/String; = "contentId"
.field static final CONTENT_NAME_ATTRIBUTE:Ljava/lang/String; = "contentName"
.field static final CONTENT_TYPE_ATTRIBUTE:Ljava/lang/String; = "contentType"
.field static final METHOD_ATTRIBUTE:Ljava/lang/String; = "method"
.field static final TYPE:Ljava/lang/String; = "share"
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Lcom/crashlytics/android/answers/PredefinedEvent;-><init>()V
return-void
.end method
.method  getPredefinedType()Ljava/lang/String;
.registers 2
const-string v0, "share"
return-object v0
.end method
.method public putContentId(Ljava/lang/String;)Lcom/crashlytics/android/answers/ShareEvent;
.registers 4
iget-object v0, p0, Lcom/crashlytics/android/answers/ShareEvent;->predefinedAttributes:Lcom/crashlytics/android/answers/AnswersAttributes;
const-string v1, "contentId"
invoke-virtual {v0, v1, p1}, Lcom/crashlytics/android/answers/AnswersAttributes;->put(Ljava/lang/String;Ljava/lang/String;)V
return-object p0
.end method
.method public putContentName(Ljava/lang/String;)Lcom/crashlytics/android/answers/ShareEvent;
.registers 4
iget-object v0, p0, Lcom/crashlytics/android/answers/ShareEvent;->predefinedAttributes:Lcom/crashlytics/android/answers/AnswersAttributes;
const-string v1, "contentName"
invoke-virtual {v0, v1, p1}, Lcom/crashlytics/android/answers/AnswersAttributes;->put(Ljava/lang/String;Ljava/lang/String;)V
return-object p0
.end method
.method public putContentType(Ljava/lang/String;)Lcom/crashlytics/android/answers/ShareEvent;
.registers 4
iget-object v0, p0, Lcom/crashlytics/android/answers/ShareEvent;->predefinedAttributes:Lcom/crashlytics/android/answers/AnswersAttributes;
const-string v1, "contentType"
invoke-virtual {v0, v1, p1}, Lcom/crashlytics/android/answers/AnswersAttributes;->put(Ljava/lang/String;Ljava/lang/String;)V
return-object p0
.end method
.method public putMethod(Ljava/lang/String;)Lcom/crashlytics/android/answers/ShareEvent;
.registers 4
iget-object v0, p0, Lcom/crashlytics/android/answers/ShareEvent;->predefinedAttributes:Lcom/crashlytics/android/answers/AnswersAttributes;
const-string v1, "method"
invoke-virtual {v0, v1, p1}, Lcom/crashlytics/android/answers/AnswersAttributes;->put(Ljava/lang/String;Ljava/lang/String;)V
return-object p0
.end method