#!/bin/bash
apkname=$1
pwddir="/home/jyoti/spectrafinal"
a=$pwddir
echo $a
#testpath is Robotium test project path 
testpath="$a/ExampleApplicationTesting2"
echo "testpath is:"
echo $testpath
echo "robotium shell script started with apk name " $apkname

new=`aapt list -a $apkname | sed -n "/^Package Group[^s]/s/.*name=//p"`
echo "Package = " $new
old=`grep -o -P '(?<=(targetPackage=")).*(?=")' $testpath/AndroidManifest.xml`
echo "Package = " $old
sed -i "s@$old@$new@" $testpath/AndroidManifest.xml
sed -i "s@$old@$new@" $testpath/bin/AndroidManifest.xml

sed -i "s@$old@$new@" $testpath/src/com/example/any/test/ExampleTest.java
 
olaun=`grep -o -P '(?<=(LAUNCHER_ACTIVITY_FULL_CLASSNAME = ")).*(?=")' $testpath/src/com/example/any/test/ExampleTest.java`
echo "old launcher = " $olaun
nlaun=`aapt d --values badging $apkname | grep "launchable-activity"|cut -d" " -f2|cut -c 7-|rev | cut -c 2- | rev`
echo "New launcher = " $nlaun
sed -i "s@$olaun@$nlaun@" $testpath/src/com/example/any/test/ExampleTest.java
echo "Successfully changed Manifest and script in Robotium Test"
dir=`pwd`
cd $testpath
echo "current directory : "
echo $testpath
echo "Building test project"
android update project --path .
echo "running ant"
ant clean debug
echo "ROBOTIUM BUILD SUCCESSFULLY"
cd $dir/results
echo "current directory is "
pwd
rm -rf signed_$apkname ||true
rm -rf ExampleApplicationTesting2-debug.apk|| true
rm -rf signed_ExampleApplicationTesting2-debug.apk ||true
cp $testpath/bin/ExampleApplicationTesting2-debug.apk .
echo "apkname is " $apkname
apkfile=`basename $apkname`
echo "apkfile before sign" 
echo $apkfile
echo "Signing APK to be tested......"
./signapk.sh $apkfile my-release-key.keystore gajrani alias_name
echo "Signing Robotest App......"
./signapk.sh ExampleApplicationTesting2-debug.apk my-release-key.keystore gajrani alias_name

echo "Installing test App......"
exist=`adb shell pm list packages| grep $new`
echo "checking it exists "
if [ -n "$exist" ]; then
   adb uninstall $new
fi


a1="signed"
a2="_"
a1="${a1}${a2}"
signapkfile="${a1}${apkfile}"

echo "${signapkfile}"

adb install $signapkfile

echo "Installing Robotium App......"

existt=`adb shell pm list packages| grep com.example.any.test`
if [ -n "$existt" ]; then
  adb uninstall com.example.any.test
fi

adb install signed_ExampleApplicationTesting2-debug.apk
echo "Starting App Testing......"
cd ..
echo "base directory"

